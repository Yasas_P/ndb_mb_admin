<%@page import="org.exolab.castor.mapping.xml.Param"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <link href="resouces/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link rel="stylesheet" href="resouces/css/font-awesome.min.css">
        <link rel="shortcut icon" href="resouces/images/favicon4.ico" />
        <title>NDB NEOS ADMIN</title>
        <!--<title>NDB MB Solution Login</title>-->

        <script type="text/javascript">
            function encryp() {
                if ($('#password').val() != "") {
                    var ps = $('#password').val();
                    $('#password').val(CryptoJS.MD5(ps));
                    //                    var value = '&lt;%= request.getMethod() %&gt;';
                    //                    alert(CryptoJS.MD5(ps));

                }
            }

        </script>     
        <style type="text/css">
            .main{
                position: absolute;
                margin: auto;
                top: 25%; left: 0; bottom: 0; right: 0;
            }
            .error-dis{
                text-align: center;                   
            }
            .copyright{
                bottom: 0;
                left: 0;
                position: fixed;
                right: 0;
                z-index: 1000;
                border-top-width: 1px;
                text-align: center;
            }
            .login-form{
                display: block;
                margin-left:20px;
                margin-bottom:20px;
                margin-right: 20px;
            }
            body{
                background-color: white;
            }

        </style>
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: LOGIN BOX -->         
        <div class="container">
            <div class="row">
                <div class="main">
                    <div class="col-md-4 col-md-offset-4">

                        <div>
                            <div class="panel panel-info" style="border-color: white;background-color: white" >
                                <div>
                                    <div class="row">
<!--                                        <img src="resouces/images/loginNDB.png" alt="NDB Bank Logo" style="margin-top: 0px;
                                             margin-bottom: -10px;margin-left: 47px; width: 357px;height:101px;">   -->
                                        <img src="resouces/images/loginNDB_Updated.png" alt="NDB Bank Logo" style="margin-top: 0px;
                                             margin-bottom: -10px;margin-left: 32px; width: 357px;height:101px;">   
                                             <!--margin-bottom: -10px;margin-left: 47px; width: 357px;height:101px;">-->   
                                    </div>						
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <form id="login-form" class="login-form" novalidate="novalidate" action="CheckUserLogin" method="post">
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon"><em class="glyphicon glyphicon-user" style="color:#999999;"></em></span>
                                                    <input type="text" class="form-control" placeholder="Username" name="username" autocomplete="off" >
                                                </div>
                                                <div class="input-group form-group">
                                                    <span class="input-group-addon"><em class="glyphicon glyphicon-lock" style="color:#999999;"></em></span>
                                                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" autocomplete="off" >
                                                </div>									
                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-sm form-control" value="LOGIN" style="background-color: #FF473A;color:white;font-weight:bold;border-radius: 20px;letter-spacing: 2px;font-size: 15px;">
                                                </div>									
                                            </form>								
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <s:if test="hasActionErrors()">
                                <div class="error-dis">
                                    <em class="fa fa-remove-sign" style="color: red ; font-family: sans-serif; font-weight: bold;text-shadow: 0px 0px 0px #000;"> <s:property default="errormessage" value="errormessage"></s:property></em>

                                    </div>
                            </s:if>
                            <s:if test="hasActionMessages()">
                                <!--<div class="errorHandler alert alert-danger no-display2222">-->
                                <div class="error-dis">
                                    <em class="fa fa-remove-sign" style="color: green; font-family: sans-serif">   <s:property default="errormessage" value="errormessage"></s:property></em>
                                        <!--</div>-->
                                    </div>  
                            </s:if>

                        </div>
                    </div>	
                </div>			
            </div>
        </div>


        <!-- start: COPYRIGHT -->
        <div class="copyright" style="background-color: #221F20">
            <!--<font id="versionno"></font>-->
            <span style="font-family: sans-serif; color: white; font-size: 12px ; text-shadow: 0 0 5px black;">
            <%                String param1 = application.getInitParameter("version");
                out.println(param1);
            %>

            Copyright � 2019 <a href="http://www.epiclanka.net/"><span style="color: #FF473A; text-shadow: 0 0 0 black;">Epic Lanka (pvt) Ltd.</a> All rights reserved.</span>
        </div>
        <!-- end: COPYRIGHT -->
    </body><!-- end: BODY -->
</html>