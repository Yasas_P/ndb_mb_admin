<%-- 
    Document   : scheduledpayment
    Created on : Sep 19, 2019, 2:32:32 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <title>Scheduled Payment</title>
        <script type="text/javascript">
            function setdate() {
                $("#fromDate").datepicker("setDate", new Date());
                $("#toDate").datepicker("setDate", new Date());
            }
            
            function resetAllData() {
                $('#fromDate').val("");
                $('#toDate').val("");
                $('#cif').val("");
                $('#payType').val("");
                $('#status').val("");
                setdate();

                $("#gridtable").jqGrid('setGridParam', {postData: {
                        fromDate: '',
                        toDate: '',
                        cif: '',
                        payType: '',
                        status: '',
                        search: false
                    }});

                jQuery("#gridtable").trigger("reloadGrid");
            }
            
            function search() {

                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                var cif = $('#cif').val();
                var payType = $('#payType').val();
                var status = $('#status').val();

                $("#gridtable").jqGrid('setGridParam', {postData: {
                        fromDate: fromDate,
                        toDate: toDate,
                        cif: cif,
                        payType: payType,
                        status: status,
                        search: true
                    }}
                );

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

            }

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });
            
            function subtodo() {
                $('#reporttype').val("pdf");
                form = document.getElementById('scheduledpaymentform');
                form.action = 'reportGenerateScheduledPayment';
                form.submit();
                $('#subview').button("disable");
            }
            
            $.subscribe('completetopics', function (event, data) {
                var isGenerate = <s:property value="vgenerate"/>;
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
              
                if (recors > 0 && isGenerate == false) {
                    $('#subview').button("enable");
                }else {
                    $('#subview').button("disable");
                }
            });
        </script>
    </head>
    <body onload="setdate()">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set id="vsearch" var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set id="vgenerate" var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>  

                            <div id="formstyle">

                                <s:form id="scheduledpaymentform" method="post" action="ScheduledPayment" theme="simple" cssClass="form">
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >From Date</label>
                                                <sj:datepicker cssClass="form-control" id="fromDate" name="fromDate" readonly="true" maxDate="d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >To Date</label>
                                                <sj:datepicker cssClass="form-control" id="toDate" name="toDate" readonly="true" maxDate="+1d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200"/>
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Payment Type</label>
                                                <s:select  cssClass="form-control" name="payType" id="payType" list="%{payTypeList}"   headerKey=""  headerValue="--Select Payment Type--" listKey="payType" listValue="description" value="%{payType}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Status</label>
                                                <s:select cssClass="form-control" name="status" id="status" headerValue="-- Select Status --" list="%{statusList}"   headerKey="" listKey="statuscode" listValue="description" />
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >CID</label>
                                                <s:textfield  name="cif" id="cif" 
                                                    maxLength="20" cssClass="form-control"
                                                    />
                                            </div>
                                        </div>
                                    </div> 

                                </s:form>
                                <div class="row row_1"></div>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <sj:submit  
                                                value="Search"
                                                button="true" 
                                                id="searchButton"
                                                onclick="search()"
                                                disabled="#vsearch" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                /> 
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View PDF" 
                                                name="subview" 
                                                disabled="#vgenerate"
                                                id="subview" 
                                                onClick="subtodo()" 
                                                /> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- space for dialog box -->
                        </div>

                        <div id="tablediv">

                            <s:url var="listurl" action="ListScheduledPayment"/>
                            <s:set var="pcaption">${CURRENTPAGE}</s:set>

                            <sjg:grid 
                                id="gridtable"
                                caption="%{pcaption}"
                                dataType="json"
                                href="%{listurl}"
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="10"
                                autowidth="true"
                                rownumbers="true"
                                onCompleteTopics="completetopics"
                                rowTotal="false"
                                viewrecords="true"
                                onErrorTopics="anyerrors" 
                                shrinkToFit="false" 
                                >                                    

                                <sjg:gridColumn name="id" index="u.id" title="Schedule ID"  sortable="true" width="250" />  
                                <sjg:gridColumn name="payType" index="u.schedulePayType.description" title="Payment Type" sortable="true"/>
                                <sjg:gridColumn name="frequency" index="u.swtScheduleFrequency.description" title="Frequency" sortable="true"/>
                                <sjg:gridColumn name="scheduleStatus" index="u.statusByScheduleStatus.description" title="Schedule Status" sortable="true"/>
                                <sjg:gridColumn name="status" index="u.statusByStatus.description" title="Status" sortable="true"/>
                                <sjg:gridColumn name="cif" index="u.swtMobileUser.cif" title="CID" sortable="true"/>
                                <sjg:gridColumn name="recurringDate" index="u.recurringDate" title="Recurring Date" sortable="true"/>
                                <sjg:gridColumn name="noOfInstallments" index="u.noOfInstallments" title="No Of Installments" sortable="true"/>
                                <sjg:gridColumn name="noOfRuns" index="u.noOfRuns" title="No Of Runs" sortable="true"/>
                                <sjg:gridColumn name="startDate" index="u.startDate" title="Start Date" sortable="true"/>
                                <sjg:gridColumn name="endDate" index="u.endDate" title="End Date" sortable="true"/>
                                <sjg:gridColumn name="lastRanDate" index="u.lastRanDate" title="Last Ran Date" sortable="true"/>
                                <sjg:gridColumn name="amount" index="u.amount" title="Amount" sortable="true"/>
                                <sjg:gridColumn name="fromAccount" index="u.fromAccount" title="From Account" sortable="true"/>
                                <sjg:gridColumn name="toAccount" index="u.toAccount" title="To Account" sortable="true"/>
                                <sjg:gridColumn name="createdDate" index="u.createdDate" title="Created Date And Time" sortable="true"/>
                                <sjg:gridColumn name="lastUpdatedOn" index="u.lastUpdatedOn" title="Last Updated Date And Time"  sortable="true"/>
                            </sjg:grid>
                        </div>

                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->

        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->

        <!-- end: BODY -->
    </body>
</html>
