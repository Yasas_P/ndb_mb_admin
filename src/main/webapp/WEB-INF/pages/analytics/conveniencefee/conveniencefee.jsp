<%-- 
    Document   : conveniencefee
    Created on : Sep 27, 2019, 9:30:34 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Convenience Fee</title>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;
                //  var isGenerate = false;

                if (recors > 0 && isGenerate == false) {
                    $('#view2').button("enable");
                } else {
                    $('#view2').button("disable");
                }
                
            });
            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('conveniencefeeform');
                form.action = 'reportGenerateConvenienceFee.action';
                form.submit();
                $('#view2').button("disable");
            }
            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function searchPage() {
                $('#message').empty();

                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                var cifSearch = $('#cifSearch').val();
                var usernameSearch = $('#usernameSearch').val();
                var statusSearch = $('#statusSearch').val();
                var mobilenoSearch = $('#mobilenoSearch').val();
                var segmentTypeSearch = $('#segmentTypeSearch').val();
                var onBoardChannelSearch = $('#onBoardChannelSearch').val();
                var onBoardTypeSearch = $('#onBoardTypeSearch').val();
                var customerCategorySearch = $('#customerCategorySearch').val();
                var defTypeSearch = $('#defTypeSearch').val();
                var feeStatusSearch = $('#feeStatusSearch').val();
                var defAccNoSearch = $('#defAccNoSearch').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        fromDate: fromDate,
                        toDate: toDate,
                        cifSearch: cifSearch,
                        usernameSearch: usernameSearch,
                        statusSearch: statusSearch,
                        mobilenoSearch: mobilenoSearch,
                        segmentTypeSearch: segmentTypeSearch,
                        onBoardChannelSearch: onBoardChannelSearch,
                        onBoardTypeSearch: onBoardTypeSearch,
                        customerCategorySearch: customerCategorySearch,
                        defTypeSearch: defTypeSearch,
                        defAccNoSearch: defAccNoSearch,
                        feeStatusSearch: feeStatusSearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetAllData() {

                $('#cifSearch').val("");
                $('#usernameSearch').val("");
                $('#statusSearch').val("");
                $('#mobilenoSearch').val("");
                $('#segmentTypeSearch').val("");
                $('#fromDate').val("");
                $('#toDate').val("");
                $('#onBoardChannelSearch').val("");
                $('#onBoardTypeSearch').val("");
                $('#customerCategorySearch').val("");
                $('#defTypeSearch').val("");
                $('#defAccNoSearch').val("");
                $('#feeStatusSearch').val("");

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        fromDate: '',
                        toDate: '',
                        cifSearch: '',
                        usernameSearch: '',
                        statusSearch: '',
                        mobilenoSearch: '',
                        segmentTypeSearch: '',
                        onBoardChannelSearch: '',
                        onBoardTypeSearch: '',
                        customerCategorySearch: '',
                        defTypeSearch: '',
                        defAccNoSearch: '',
                        feeStatusSearch: '',
                        search: false
                    }
                });

                setdate();

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

            }

            function setdate() {
                $("#fromDate").datepicker("setDate", new Date());
                $("#toDate").datepicker("setDate", new Date());
            }

        </script>
    </head>

    <body onload="setdate()">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form id="conveniencefeeform" method="post" action="ConvenienceFee" theme="simple" cssClass="form" >

                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>

                                        <div class="row row_1">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label >From Date</label>
                                                <sj:datepicker cssClass="form-control" id="fromDate" name="fromDate" readonly="true" maxDate="d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >To Date</label>
                                                <sj:datepicker cssClass="form-control" id="toDate" name="toDate" readonly="true" maxDate="+1d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>CID </label>
                                                <s:textfield cssClass="form-control" name="cifSearch" id="cifSearch" maxLength="30" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>User Name </label>
                                                <s:textfield cssClass="form-control" name="usernameSearch" id="usernameSearch" maxLength="35" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">        
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >User Segment</label>
                                                <s:select cssClass="form-control" name="segmentTypeSearch" id="segmentTypeSearch" headerValue="-- Select User Segment --" list="%{segmentTypeList}"   headerKey="" listKey="segmentcode" listValue="description" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >On Board Channel</label>
                                                <s:select cssClass="form-control" name="onBoardChannelSearch" id="onBoardChannelSearch" headerValue="-- Select On Board Channel --" list="%{onBoardChannelList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >On Board Type</label>
                                                <s:select cssClass="form-control" name="onBoardTypeSearch" id="onBoardTypeSearch" headerValue="-- Select On Board Type --" list="%{onBoardTypeList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Customer Category</label>
                                                <s:textfield cssClass="form-control" name="customerCategorySearch" id="customerCategorySearch" maxLength="100" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >User Status</label>
                                                <s:select cssClass="form-control" name="statusSearch" id="statusSearch" headerValue="-- Select Status --" list="%{statusList}"   headerKey="" listKey="statuscode" listValue="description" />
                                            </div>
                                        </div>
<!--                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Mobile</label>
                                                <s:textfield cssClass="form-control" name="mobilenoSearch" id="mobilenoSearch" maxLength="16" onkeyup="$(this).val($(this).val().replace(/[^0-9+ ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9+ ]/g,''))" />
                                            </div>
                                        </div>-->
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Primary Account Type</label>
                                                <s:select cssClass="form-control" name="defTypeSearch" id="defTypeSearch" headerValue="-- Select Primary Account Type --" list="%{defaultAccountTypeList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div>
                                         <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Primary Account/Card Number</label>
                                                <s:textfield cssClass="form-control" name="defAccNoSearch" id="defAccNoSearch" maxLength="16"  />
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Fee Deduct Status</label>
                                                <s:select cssClass="form-control" name="feeStatusSearch" id="feeStatusSearch" headerValue="-- Select Fee Deduct Status --" list="%{feeStatusList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div> 
                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                disabled="#vsearch"
                                                onclick="searchPage()"
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"

                                                />
                                        </div> 
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"

                                                />
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View CSV" 
                                                name="view2" 
                                                id="view2" 
                                                onClick="todocsv()" 
                                                disabled="#vgenerate"/> 
                                        </div>
                                    </div>  
                                </div>
                            </div>
                                        <!-- ----place for dialog box--- -->
                        </div>
                        <div id="tablediv">
                            <s:url var="listurl" action="listConvenienceFee"/>
                            <s:set var="pcaption">${CURRENTPAGE}</s:set>

                            <sjg:grid
                                id="gridtable"
                                caption="%{pcaption}"
                                dataType="json"
                                href="%{listurl}"
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="10"
                                autowidth="true"
                                rownumbers="true"
                                onCompleteTopics="completetopics"
                                rowTotal="false"
                                viewrecords="true"
                                onErrorTopics="anyerrors"
                                shrinkToFit="false"
                                >
                                <sjg:gridColumn name="userid" index="u.userid" title="Unique ID"  sortable="false" width="50"/>                             
                                <sjg:gridColumn name="cif" index="u.cif" title="CID"  sortable="false" width="50"/>
                                <sjg:gridColumn name="username" index="u.username" title="User Name"  sortable="false" width="70"/>
                                <sjg:gridColumn name="promotionMtSegmentType" index="u.USER_SEGMENT" title="User Segment"  sortable="false" width="70"/>
                                <sjg:gridColumn name="onBoardChannel" index="u.onBoardChannel" title="On Board Channel"  sortable="false" width="70"/>
                                <sjg:gridColumn name="onBoardType" index="u.onBoardType" title="On Board Type"  sortable="false" width="70"/>
                                <sjg:gridColumn name="customerCategory" index="u.customerCategory" title="Customer Category"  sortable="false" width="70"/>
                                <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="false" width="70" hidden="true"/>
                                <sjg:gridColumn name="statusDes" index="u.statusDes" title="User Status"  sortable="false" width="70"/>
                                <sjg:gridColumn name="loginStatus" index="u.loginStatus" title="Login Status"  sortable="false" width="70"/>
                                <sjg:gridColumn name="defType" index="u.defType" title="Primary Account Type"  sortable="false" width="70"/>
                                <sjg:gridColumn name="defAccNo" index="u.defAccNo" title="Primary Acc/Card Number"  sortable="false" width="70"/>
                                <sjg:gridColumn name="customerName" index="u.CUSTOMER_NAME" title="Customer Name"  sortable="false"  width="70"/>
                                <sjg:gridColumn name="mobileNumber" index="u.mobileNumber" title="Mobile"  sortable="false" width="70"/>   
                                <sjg:gridColumn name="chargeAmount" index="F.CHARGED_AMOUNT" title="Fee Amount"  sortable="false"/>    
                                <sjg:gridColumn name="date" index="F.TIMESTAMP" title="Fee Deduct Date"  sortable="false" width="70"/>      
                                <sjg:gridColumn name="feeDeductStatus" index="F.RESPONSE_CODE" title="Fee Deduct Status"  sortable="false" width="70"/>      
                                <sjg:gridColumn name="errorMessage" index="F.DESCRIPTION" title="Error Message"  sortable="false" width="70"/>      
                            </sjg:grid> 
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
        <!--</div>-->
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->



        <!-- end: BODY -->
    </body>
</html>
