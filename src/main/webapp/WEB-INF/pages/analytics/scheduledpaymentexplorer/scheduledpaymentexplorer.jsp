<%-- 
    Document   : scheduledpaymentexplorer
    Created on : Oct 23, 2019, 2:56:26 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <%@include file="/stylelinks.jspf" %>
        
        <title>Transaction Explorer</title> 
        <script type="text/javascript">

            function viewformatter(cellvalue) {
                return "<a href='#' title='View' onClick='javascript:viewTranInit(&#34;" + cellvalue + "&#34;)' title='View Transaction'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";

            }

            function viewTranInit(keyval) {
                $("#viewdialog").data('txnId', keyval).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#viewdialog");
                $led.html("Loading..");
                $led.load("viewDetailScheduledPaymentExp.action?txnId=" + $led.data('auditId'));
            });

            function searchTxn(param) {

                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                var nic = $('#nic').val();
                var cif = $('#cif').val();
                var txnType = $('#txnType').val();
                var responseCode = $('#responseCode').val();
                var customerCategory = $('#customerCategory').val();
                var channelType = $('#channelType').val();
                var currencyCode = $('#currencyCode').val();
                var tranRefNo = $('#tranRefNo').val();
                var iblRefNo = $('#iblRefNo').val();
                var billCategoryName = $('#billCategoryName').val();
                var billProviderName = $('#billProviderName').val();
                var status = $('#status').val();
                var staffStatus = $('#staffStatus').val();

                $("#gridtable").jqGrid('setGridParam', {postData: {
                        fromDate: fromDate,
                        toDate: toDate,
                        nic: nic,
                        cif: cif,
                        txnType: txnType,
                        responseCode: responseCode,
                        customerCategory: customerCategory,
                        channelType: channelType,
                        currencyCode: currencyCode,
                        tranRefNo: tranRefNo,
                        iblRefNo: iblRefNo,
                        billCategoryName: billCategoryName,
                        billProviderName: billProviderName,
                        status: status,
                        staffStatus: staffStatus,
                        search: param}}
                );

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

            }

            $.subscribe('completetopics', function (event, data) {
                var isGenerate = <s:property value="vgenerate"/>;
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                if (recors > 0 && isGenerate == false) {
                    $('#view2').button("enable");
                    
                } else {
                    $('#view2').button("disable");

                }
            });



            function setdate() {
                $("#fromDate").datepicker("setDate", new Date());
                $("#toDate").datepicker("setDate", new Date());
            }

            function resetAllData() {
                $('#fromDate').val("");
                $('#toDate').val("");
                $('#nic').val("");
                $('#cif').val("");
                $('#txnType').val("");
                $('#responseCode').val("");
                $('#customerCategory').val("");
                $('#channelType').val("");
                $('#currencyCode').val("");
                $('#tranRefNo').val("");
                $('#iblRefNo').val("");
                $('#billCategoryName').val("");
                $('#billProviderName').val("");
                $('#status').val("");
                $('#staffStatus').val("");
                //$('#subview').button("disable");
                //$('#subview1').button("disable");
                setdate();

                $("#gridtable").jqGrid('setGridParam', {postData: {
                        fromDate: '',
                        toDate: '',
                        nic: '',
                        cif: '',
                        txnType: '',
                        responseCode: '',
                        customerCategory: '',
                        channelType: '',
                        currencyCode: '',
                        tranRefNo: '',
                        iblRefNo: '',
                        billCategoryName: '',
                        billProviderName: '',
                        status: '',
                        staffStatus: '',
                        search: false
                    }});

                jQuery("#gridtable").trigger("reloadGrid");
            }

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function search(e) {
                var key = e.keyCode || e.which;
                if (key == 13) {
                    searchTxn(true);
                }
            }

            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('scheduledpaymentexpform');
                form.action = 'reportGenerateScheduledPaymentExp.action';
                form.submit();

                $('#view2').button("disable");
            }

        </script>

    </head>


    <body onload="setdate()">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->

                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">

                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set id="vsearch" var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set id="vgenerate" var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                            <s:set id="vviewlink" var="vviewlink"><s:property value="vviewlink" default="true"/></s:set>
                            <s:set id="vview" var="vview"><s:property value="vview" default="true"/></s:set>    

                                <div id="formstyle">

                                <s:form id="scheduledpaymentexpform" method="post" action="ScheduledPaymentExp" theme="simple" cssClass="form">
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                        <div class="row row_1">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label >From Date</label>
                                                <sj:datepicker cssClass="form-control" id="fromDate" name="fromDate" readonly="true" maxDate="d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >To Date</label>
                                                <sj:datepicker cssClass="form-control" id="toDate" name="toDate" readonly="true" maxDate="+1d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200"/>
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Transaction Type</label>
                                                <s:select  cssClass="form-control" name="txnType" id="txnType" list="%{txnTypeList}"   headerKey=""  headerValue="--Select Transaction Type--" listKey="typecode" listValue="description" value="%{txnType}" disabled="false"/>

                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >T24 Reference </label>
                                                <s:textfield  cssClass="form-control" name="tranRefNo" id="tranRefNo" maxLength="50" disabled="false"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >IBL Reference </label>
                                                <s:textfield  cssClass="form-control" name="iblRefNo" id="iblRefNo" maxLength="50" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Channel Type</label>
                                                <s:select  cssClass="form-control" name="channelType" id="channelType" list="%{channelTypeList}"   headerKey=""  headerValue="--Select Channel--" listKey="key" listValue="value" value="%{channelType}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Status</label>
                                                <s:select cssClass="form-control" name="status" id="status" headerValue="-- Select Status --" list="%{txnStatusList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div>
<!--                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Status</label>
                                                <%--<s:select cssClass="form-control" name="status" id="status" headerValue="-- Select Status --" list="%{statusList}"   headerKey="" listKey="statuscode" listValue="description" />--%>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Response</label>
                                                <%--<s:select  cssClass="form-control" name="responseCode" id="responseCode" list="%{responseList}"   headerKey=""  headerValue="--Select Response--" listKey="code" listValue="description" value="%{responseCode}" disabled="false"/>--%>
                                            </div>
                                        </div>    -->
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Currency Code</label>
                                                <s:select  cssClass="form-control" name="currencyCode" id="currencyCode" list="%{currencyList}"   headerKey=""  headerValue="--Select Currency--" listKey="currencyCode" listValue="currencyCode" disabled="false"/>
                                                <%--<s:textfield  cssClass="form-control" name="currencyCode" id="currencyCode" maxLength="10" disabled="false"/>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1"> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Customer CID</label>
                                                <s:textfield  name="cif" id="cif" 
                                                              maxLength="20" cssClass="form-control"
                                                              onkeypress="search(event)"
                                                              />
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Customer Category</label>
                                                <s:textfield  cssClass="form-control" name="customerCategory" id="customerCategory" maxLength="10" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Customer NIC</label>
                                                <s:textfield  name="nic" id="nic" 
                                                              maxLength="20" cssClass="form-control"
                                                              onkeypress="search(event)"
                                                              />

                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Bill Category Name</label>
                                                <s:select cssClass="form-control" name="billCategoryName" id="billCategoryName" headerValue="-- Select Bill Category Name --" list="%{billerCategoryList}"   headerKey="" listKey="billercatcode" listValue="name" />
                                                <%--<s:textfield  cssClass="form-control" name="billCategoryName" id="billCategoryName" maxLength="50" disabled="false"/>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">        
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Bill Provider Name</label>
                                                <s:select cssClass="form-control" name="billProviderName" id="billProviderName" headerValue="-- Select Bill Provider Name --" list="%{billServiceProviderList}"   headerKey="" listKey="providerId" listValue="providerName" />
                                                <%--<s:textfield  cssClass="form-control" name="billProviderName" id="billProviderName" maxLength="50" disabled="false"/>--%>
                                            </div>
                                        </div>
<!--                                    </div>
                                    <div class="row row_1"> -->
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Staff/Non Staff</label>
                                                <s:select cssClass="form-control" name="staffStatus" id="staffStatus" headerValue="-- Select Staff/Non Staff --" list="%{staffOrNotList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div>
                                    </div> 

                                </s:form>
                                <div class="row row_1"></div>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <sj:submit  
                                                value="Search"
                                                button="true" 
                                                id="searchButton"
                                                onclick="searchTxn(true)"
                                                disabled="#vsearch" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                /> 
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View CSV" 
                                                name="view2" 
                                                id="view2" 
                                                onClick="todocsv()" 
                                                disabled="#vgenerate"/> 
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Start update dialog box -->
                            <sj:dialog                                     
                                id="viewdialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="View Transaction"
                                onOpenTopics="openviewtasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="590"
                                dialogClass= "dialogclass"
                                />


                        </div>

                        <div id="tablediv">

                            <s:url var="listurl" action="ListScheduledPaymentExp"/>
                            <s:set var="pcaption">${CURRENTPAGE}</s:set>

                            <sjg:grid 
                                id="gridtable"
                                caption="%{pcaption}"
                                dataType="json"
                                href="%{listurl}"
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="10"
                                autowidth="true"
                                rownumbers="true"
                                onCompleteTopics="completetopics"
                                rowTotal="false"
                                viewrecords="true"
                                onErrorTopics="anyerrors" 
                                shrinkToFit="false" 
                                >                                    

                                <%--    <sjg:gridColumn name="txnid" index="g.TRANSACTION_ID" width="35" align="center" title="View" formatter="viewformatter" hidden="#vviewlink" sortable="false" frozen="true"/>  --%>
                                <sjg:gridColumn name="txnId" index="TRANSACTION_ID" title="Transaction ID"  sortable="true" width="250"/>                                               
                                <sjg:gridColumn name="scheduleId" index="U.SCHEDULE_ID" title="Schedule ID"  sortable="true" width="250"/>                                               
                                <sjg:gridColumn name="txnType" index="TRANSACTION_TYPE" title="Transaction Type"  sortable="true"/>
                                <sjg:gridColumn name="localTime" index="U.LOCAL_TIME" title="Transaction Time"  sortable="true"/>
                                <sjg:gridColumn name="txnMode" index="txnMode" title="Transaction Mode" sortable="true"/>
                                <sjg:gridColumn name="payTypeDes" index="PT.DESCRIPTION" title="Pay Type" sortable="true"/>
                                <sjg:gridColumn name="isPayToMobile" index="U.IS_PAYTO_MOBILE" title="Pay To Mobile" sortable="true"/>
                                <sjg:gridColumn name="isOneTime" index="isOneTime" title="One Time/Registered" sortable="true"/>
                                <sjg:gridColumn name="fromAccNo" index="FROM_ACCOUNT_NUMBER" title="From Account Number" sortable="true"/> 
                                <sjg:gridColumn name="toAccNo" index="TO_ACCOUNT_NUMBER" title="To Account Number" sortable="true"/> 
                                <sjg:gridColumn name="currencyCode" index="currencyCode" title="Currency Code" sortable="true"/>
                                <sjg:gridColumn name="amount" index="AMOUNT" title="Amount"  sortable="true" width="100" align="right"/>
                                <sjg:gridColumn name="channelType" index="channelType" title="Channel Type" sortable="true"/>
                                <sjg:gridColumn name="responseCodes" index="responseCodes" title="Status" sortable="true"/>
                                <sjg:gridColumn name="t24TranStatus" index="T24TranStatus" title="T24 Status" sortable="true"/>
                                <sjg:gridColumn name="t24TranReference" index="T24TranReference" title="T24 Reference" sortable="true"/>
                                <sjg:gridColumn name="IBLTranStatus" index="IBLTranStatus" title="IBL Status" sortable="true"/>
                                <sjg:gridColumn name="IBLReference" index="IBLReference" title="IBL Reference" sortable="true"/>
                                <sjg:gridColumn name="errDesc" index="U.ERR_DESC" title="Error Description" sortable="true"/>
                                <sjg:gridColumn name="userId" index="userId" title="Unique ID" sortable="true"/>
                                <sjg:gridColumn name="custCif" index="CUST_CIF" title="CID" sortable="true"/>
                                <sjg:gridColumn name="customerCategory" index="customerCategory" title="Customer Category" sortable="true"/>
                                <sjg:gridColumn name="staffStatus" index="staffStatus" title="Staff/Non Staff" sortable="true"/>
                                <sjg:gridColumn name="nic" index="CustNic" title="NIC" sortable="true"/>
                                <sjg:gridColumn name="userName" index="userName" title="User Name" sortable="true"/>
                                <sjg:gridColumn name="custName" index="custName" title="Customer Name" sortable="true"/>
                                <sjg:gridColumn name="custEmail" index="custEmail" title="Customer Email" sortable="true"/>
                                <sjg:gridColumn name="mobileNumber" index="mobileNumber" title="Mobile Number" sortable="true"/>
                                <sjg:gridColumn name="address1" index="address1" title="Address" sortable="true"/>
                                <sjg:gridColumn name="appId" index="appId" title="App ID" sortable="true"/>
                                <sjg:gridColumn name="deviceManufacture" index="deviceManufacture" title="Device Manufacture" sortable="true"/>
                                <sjg:gridColumn name="deviceBuildNumber" index="deviceBuildNumber" title="Device Build Number" sortable="true"/>
                                <%--<sjg:gridColumn name="deviceId" index="deviceId" title="Device ID" sortable="true"/>--%>
                                <sjg:gridColumn name="billRefNo" index="billRefNo" title="Bill Reference Number" sortable="true"/>
                                <sjg:gridColumn name="billCategoryName" index="billCategoryName" title="Bill Category Name" sortable="true"/>
                                <sjg:gridColumn name="billProviderName" index="billProviderName" title="Bill Provider Name" sortable="true"/>
                                <sjg:gridColumn name="billerName" index="billerName" title="Biller Name" sortable="true"/>
                                <sjg:gridColumn name="bankName" index="bankName" title="Bank Name" sortable="true"/>
                                <sjg:gridColumn name="branchName" index="branchName" title="Branch Name" sortable="true"/>
                                <sjg:gridColumn name="remarks" index="remarks" title="Remarks" sortable="true"/>
                                <sjg:gridColumn name="payeeName" index="payeeName" title="Payee Name" sortable="true"/>
                                <sjg:gridColumn name="serviceFee" index="serviceFee" title="Service Fee" sortable="true"/>
                            </sjg:grid>
                        </div>

                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->

        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->

        <!-- end: BODY -->
    </body>
</html>

