<%-- 
Document   : customersearchdetailview
Created on : Jun 20, 2019, 11:38:06 AM
Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>detail view block applications customer</title> 
    </head>
    <script>

        function resetAllStatus() {
            var userid = $('#useridstatus').val();
            viewStatus(userid);
        }

         function viewStatus(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findCustomerStatusCustomerSearch.action',
                    data: {userid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessagestatusupdate').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#statusmob').val("");
                            $('#statusib').val("");
                        } else {
                            $('#statusmob').val(data.statusByMbStatus);
                            $('#statusib').val(data.statusByIbStatus);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

    </script>
    <body>
        <s:div id="amessagestatusupdate">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>


        <s:form id="detailviewblockappscustomer" method="post" action="*CustomerSearch" theme="simple" cssClass="form" >

            <s:hidden id="useridstatus" name="userid"></s:hidden>

                <div class="row row_popup">
                    <div class="horizontal_line_popup"></div>
                </div>

                <div class="row row_popup">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer ID/CID</label>
                                <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="cif" cssClass="form-control" />
                            </div>  
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label >User Name</label>
                                <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="username" cssClass="form-control" />
                            </div>
                        </div>                
                    </div>
                </div>    
            </div>   
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Customer Status</label>
                                <s:select value="%{status}" headerValue="--Select Status--" headerKey="" cssClass="form-control"  id="statusapp" list="%{statusList}"  name="status" listKey="statuscode" listValue="description" disabled="true"/>                  
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Mobile Banking Status</label>
                                <s:select value="%{statusByMbStatus}" headerValue="-- Select Mobile Banking Status --" headerKey="" cssClass="form-control"  id="statusmob" list="%{statusList}"  name="statusByMbStatus" listKey="statuscode" listValue="description"/>                  
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Internet Banking Status</label>
                                <s:select value="%{statusByIbStatus}" headerValue="-- Select Internet Banking Status --" headerKey="" cssClass="form-control"  id="statusib" list="%{statusList}"  name="statusByIbStatus" listKey="statuscode" listValue="description"/>                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row_popup form-inline">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>    
                <div class="col-sm-6  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="blockApplicationsCustomerSearch" var="updatappstatusurl"/>
                        <sj:submit
                            button="true"
                            id ="update_status"
                            value="Update Status"
                            href="%{updatappstatusurl}"
                            onClickTopics=""
                            targets="amessagestatusupdate"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            disabled="false"
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            onClick="resetAllStatus()" 
                            cssClass="ui-button-reset"

                            />                            
                    </div>

                </div>
            </div>
        </s:form>
    </body>
</html>
