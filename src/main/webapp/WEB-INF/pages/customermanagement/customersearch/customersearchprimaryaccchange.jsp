<%-- 
    Document   : customersearchprimaryaccchange
    Created on : May 29, 2019, 4:16:23 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>primary account change customer</title> 
    </head>
    <script>
        function accInquiry() {

            var defaultAccountOld = $("#defaultAccountOld_pa").val();
            var userid = $("#userid_pa").val();
            var cif = $("#cif_pa").val();
            var defType = $("#deftype_pa").val();

            $.ajax({
                url: '${pageContext.request.contextPath}/cusAccountInqCustomerSearch.action',
                data: {defaultAccountOld: defaultAccountOld, cif: cif, defType:defType,userid: userid},
                dataType: "json",
                type: "POST",
                success: function (data) {
                    var msg = data.message;

                    if (msg) {
                        $('#amessageaccupdate').html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span>"
                                + "<span>" + msg
                                + "</span></p></div></div>");
                        $('#defaultAccountnew_pa').empty();
                        $('#deftype_pa').val("");
//                        $('#updateAccount').button("enable");
                        $('#defaultAccountnew_pa').append("<option value=''>-- Select Primary Account/Card Number --</option>");

                    } else {
                        $('#amessageaccupdate').text("");
                        $('#defaultAccountnew_pa').empty();
                        $('#updateAccount').button("enable");
                        $('#defaultAccountnew_pa').append("<option value=''>-- Select Primary Account/Card Number --</option>");

                        $.each(data.accountList, function (index, item) {
                            $('#defaultAccountnew_pa').append(
                                    $('<option></option>').val(item.accountNumber).html(item.accountMaskedNumber)
                                    );
                        });
                        $("#defTypeHidden_pa").val(defType);

                    }
                },
                error: function (data) {
                    window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                }
            });

        }
        $.subscribe('resetCustomerAccount', function (event, data) {
            $('#amessageaccupdate').text("");
            $('#defaultAccountnew_pa').empty();
            $('#deftype_pa').val("");
            $('#defaultAccountnew_pa').append("<option value=''>-- Select Primary Account/Card Number --</option>");

            $('#updateAccount').button("disable");
            ;
        });
    </script>
    <body>
        <s:div id="amessageaccupdate">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>

        <s:set var="vupdate"><s:property value="vupdate" default="true"/></s:set>

        <s:form id="accupdatecustomer" method="post" action="CustomerSearch" theme="simple" cssClass="form" >

            <s:hidden id="userid_pa" name="userid"></s:hidden>
            <s:hidden id="cif_pa" name="cif"></s:hidden>
            <s:hidden id="username_pa" name="username"></s:hidden>
            <s:hidden id="defaultAccountOld_pa" name="defaultAccountOld"></s:hidden>
            <s:hidden id="defTypeHidden_pa" name="defTypeHidden"></s:hidden>

                <div class="row row_popup"> 
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label >User Name</label>
                        <s:label disabled="false" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="username"  cssClass="form-control" />
                    </div>  
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label >CID</label>
                        <s:label disabled="false" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="cif"  cssClass="form-control" />
                    </div> 
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label >Primary Acc/Card Number</label>
                        <s:label disabled="false" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="defaultAccountOld" cssClass="form-control"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label >Primary Type</label>
                        <s:select cssClass="form-control" name="defType" id="deftype_pa" headerValue="-- Select Primary Type --" list="%{defaultAccountTypeList}"   headerKey="" listKey="key" listValue="value" />
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    <div class="form-group">
                        <sj:submit
                            button="true"
                            value="Check Accounts/Cards  "
                            onClick="accInquiry()" 
                            cssClass="form-control btn_normal"
                            cssStyle="border-radius: 12px;background-color:#969595;color:white;" 
                            />
                    </div>
                </div>
            </div>
<!--            <div class="row row_popup"> 
                <div class="col-sm-9">
                </div>
                <div class="col-sm-3 text-right">
                    <div class="form-group">
                        <%--<sj:submit--%>
                            button="true"
                            value="Check Accounts"
                            onClick="accInquiry()" 
                            cssClass="form-control btn_normal"
                            cssStyle="border-radius: 12px;background-color:#969595;color:white;" 
                            />
                    </div>
                </div>
            </div>-->
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4"> 
                    <div class="form-group">
                        <span style="color: red">*</span><label >New Primary Account/Card Number</label>
                        <s:select  cssClass="form-control" name="defaultAccountnew" id="defaultAccountnew_pa" list="%{accountList}"   headerKey=""  headerValue="-- Select Primary Account/Card Number --" listKey="accountNumber" listValue="accountMaskedNumber" />                  
                    </div>
                </div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>    
                <div class="col-sm-6  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="updateAccountCustomerSearch" var="updateaccounturl"/>
                        <sj:submit
                            button="true"
                            id ="updateAccount"
                            value="Update Accounts"
                            href="%{updateaccounturl}"
                            onClickTopics=""
                            targets="amessageaccupdate"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            disabled="true"
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetCustomerAccount"
                            />                          
                    </div>

                </div>
            </div>
        </div>
    </s:form> 
</body>
</html>
