<%-- 
    Document   : customersearchdetailview
    Created on : Jun 20, 2019, 11:38:06 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>detail view customer</title> 
    </head>
    <body>


        <s:form id="detailviewcustomer" method="post" action="*CustomerSearch" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>

            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Unique ID</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="userid" cssClass="form-control" />
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer ID/CID</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="cif" cssClass="form-control" />
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >User Name</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="username" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >User Segment</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="cusSegmentType" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >On Board Channel</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="onBoardChannel" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >On Board Type</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="onBoardType" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer Category</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="customerCategory" cssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer Status</label> 
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="status"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Waive Off Status</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="waveoff"  cssClass="form-control"/>
                                </div>  
                            </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Charge Amount</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="chargeAmount"  cssClass="form-control"/>
                                </div>  
                            </div>  
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Login Status</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="loginStatus" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Primary Account Type</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="defType" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Primary Acc/Card Number</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="defAccNo" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer Name</label>
                                    <td><s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="customerName" cssClass="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >NIC/Passport No</label>
                                    <td><s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="nic" cssClass="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >DOB</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="dob" cssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Gender</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="gender" cssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Mobile</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="mobileNumber"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Mobile(Secondary)</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="secondaryMobile"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Email</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="email" cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Email(Secondary)</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="secondaryEmail"  cssClass="form-control"/>
                                </div>  
                            </div>
<!--                            <div class="col-sm-4" >
                                <div class="form-group">
                                    <label >Address</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="permanentAdd" cssClass="form-control" />
                                </div>
                            </div>    -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Block Status</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="loginAttemptStatus"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Invalid Login Attempts</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="loginAttempts"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Last Password Update Time</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="lastPassUpdateTime"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Fee Cycle Start Date</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="finalFeeDudDay"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Account Officer</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="accountofficer"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Remark</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="remark"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Maker</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="maker"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Checker</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="checker"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Created Time</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="createdtime"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Last Updated Time</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="lastupdatedtime"  cssClass="form-control"/>
                                </div>  
                            </div>
                        </div>

<!--                        <div class="row row_popup"> 

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label >Address Information</label>
                                </div>
                                <div class="col-sm-4" style="background: #E4E4E4">
                                    <div class="form-group">
                                        <label >Permanent</label>
                                        <s:textarea rows="5" cols="4" disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="permAddress" cssClass="form-control" />
                                    </div>
                                </div>    
                                <div class="col-sm-4" style="background: #E4E4E4">
                                    <div class="form-group">
                                        <label>Correspondence</label>
                                        <td><s:textarea rows="5" cols="4" disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="correspondenceAdd" cssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-4" style="background: #E4E4E4">
                                    <div class="form-group">
                                        <label>Office</label>
                                        <s:textarea rows="5" cols="4" disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="officeAdd" cssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>-->
 
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label >Customer Last Login</label>
                                <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="lastlogindate" cssClass="form-control" />
                            </div>  
                        </div>
                    </div>
                </div>
            </s:form>
    </body>
</html>
