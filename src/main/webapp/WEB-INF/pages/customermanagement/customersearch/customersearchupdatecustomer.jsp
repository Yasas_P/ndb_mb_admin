<%-- 
    Document   : customersearchupdatecustomer
    Created on : Oct 2, 2019, 11:34:51 AM
    Author     : sivaganesan_t
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>primary account change customer</title> 
    </head>
    <script>
        function customerInquiry() {

            var userid = $("#userid_up").val();
            var cif = $("#cif_up").val();

            $.ajax({
                url: '${pageContext.request.contextPath}/customerInqCustomerSearch.action',
                data: {cif: cif,userid: userid},
                dataType: "json",
                type: "POST",
                success: function (data) {
                    var msg = data.message;

                    if (msg) {
                        $('#amessagecustomerdata').html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span>"
                                + "<span>" + msg
                                + "</span></p></div></div>");
                        
                        $('#updateCustomerDataButt').button("disable");
                        $('#custDetailArea').empty();
                    } else {
                        $('#amessagecustomerdata').text("");
                        $('#custDetailArea').empty();
                        var custDetailHtml = "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Field Name</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >Old Value</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >New Value</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Customer Name</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.customerName+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.customerName+"</label></div></div>" +
                                    "</div >" +
//                                    "<div class='row row_popup'>"+
//                                        "<div class='col-sm-3'><div class='form-group'> <label >Status Code</label></div></div>" +
//                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.status+"</label></div></div>" +
//                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.status+"</label></div></div>" +
//                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Segment Type</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.segmentType+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.segmentType+"</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Customer Category</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.customerCategory+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.customerCategory+"</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >NIC</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.nic+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.nic+"</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >DOB</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.dob+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.dob+"</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Gender</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.gender+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.gender+"</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Mobile Number</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.mobileNumber+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.mobileNumber+"</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Email</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label style='width:100%; word-wrap: break-word;' >"+data.oldCustomerData.email+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label style='width:100%; word-wrap: break-word;' >"+data.newCustomerData.email+"</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Secondary Mobile</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.secondaryMobile+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.secondaryMobile+"</label></div></div>" +
                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Secondary Email</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label style='width:100%; word-wrap: break-word;' >"+data.oldCustomerData.secondaryEmail+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label style='width:100%; word-wrap: break-word;' >"+data.newCustomerData.secondaryEmail+"</label></div></div>" +
                                    "</div >" +
//                                    "<div class='row row_popup'>"+
//                                        "<div class='col-sm-3'><div class='form-group'> <label >Address</label></div></div>" +
//                                        "<div class='col-sm-4'><div class='form-group'> <label style='width:100%; word-wrap: break-word;' >"+data.oldCustomerData.permanentAdd+"</label></div></div>" +
//                                        "<div class='col-sm-4'><div class='form-group'> <label style='width:100%; word-wrap: break-word;' >"+data.newCustomerData.permanentAdd+"</label></div></div>" +
//                                    "</div >" +
                                    "<div class='row row_popup'>"+
                                        "<div class='col-sm-3'><div class='form-group'> <label >Account Officer</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.oldCustomerData.accountofficer+"</label></div></div>" +
                                        "<div class='col-sm-4'><div class='form-group'> <label >"+data.newCustomerData.accountofficer+"</label></div></div>" +
                                    "</div >" +
                                    "";
                        $('#custDetailArea').html(custDetailHtml);    
                        $('#updateCustomerDataButt').button("enable");
                    }
                },
                error: function (data) {
                    window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                }
            });

        }
        $.subscribe('resetCustomerData', function (event, data) {
            $('#amessagecustomerdata').text("");
            $('#custDetailArea').empty();
            $('#updateCustomerDataButt').button("disable");
        });
    </script>
    <body>
        <s:div id="amessagecustomerdata">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>

        <s:set var="vupdate"><s:property value="vupdate" default="true"/></s:set>

        <s:form id="updatecustomerdata" method="post" action="CustomerSearch" theme="simple" cssClass="form" >

            <s:hidden id="userid_up" name="userid"></s:hidden>
            <s:hidden id="cif_up" name="cif"></s:hidden>
            <s:hidden id="username_up" name="username"></s:hidden>

                <div class="row row_popup"> 
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label >User Name</label>
                        <s:label disabled="false" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="username"  cssClass="form-control" />
                    </div>  
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label >CID</label>
                        <s:label disabled="false" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="cif"  cssClass="form-control" />
                    </div> 
                </div>
                <div class="col-sm-3 text-right">
                    <div class="form-group">
                        <sj:submit
                            button="true"
                            value="Check Customer  "
                            onClick="customerInquiry()" 
                            cssClass="form-control btn_normal"
                            cssStyle="border-radius: 12px;background-color:#969595;color:white;" 
                            />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup" id="custDetailArea">
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>    
                <div class="col-sm-6  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="updateCustomerCustomerSearch" var="updatecustomerdataurl"/>
                        <sj:submit
                            button="true"
                            id ="updateCustomerDataButt"
                            value="Update Customer"
                            href="%{updatecustomerdataurl}"
                            onClickTopics=""
                            targets="amessagecustomerdata"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            disabled="true"
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetCustomerData"
                            />                          
                    </div>

                </div>
            </div>
        </div>
    </s:form> 
</body>
</html>
