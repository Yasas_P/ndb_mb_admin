<%-- 
    Document   : customersearch
    Created on : Mar 22, 2019, 4:02:28 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Customer Search</title>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">

            function blockformatter(cellvalue, options, rowObject) {
                if (rowObject.status === "ACT") {
                    return "<a href='#' title='Block IB/UB' onClick='javascript:blockInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-locked' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }

            //----------------------------------------------------------------------------------------------
            function blockInit(keyval) {
                $("#blockdialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openblocktasktopage', function (event, data) {
                var $led = $("#blockdialog");
                $led.html("Loading..");
                $led.load("viewBlocAppCustomerSearch.action?userid=" + $led.data('id'));
            });
//----------------------------------------------------------------------------------------------

            function detailviewformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='View Customer' onClick='javascript:detailviewInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }

            function editformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit Customer' onClick='javascript:editInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }

            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmCustomer(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectCustomer(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function updatecustomerformatter(cellvalue, options, rowObject) {
                if (rowObject.status == "ACT") {
                    return "<a href='#' title='Update Customer' onClick='javascript:updatecustomerInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-refresh' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }

            function updateformatter(cellvalue, options, rowObject) {
                if (rowObject.status == "ACT") {
                    return "<a href='#' title='Device Info' onClick='javascript:updateInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-triangle-2-n-s' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }

            }

            function primaryaccchangeformatter(cellvalue, options, rowObject) {
                 if (rowObject.status == "ACT") {
                return "<a href='#' title='Primary Account Updation' onClick='javascript:primaryAccChangeInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-refresh' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            } else {
                    return "--";
                }
        }

            function attemptResetformatter(cellvalue, options, rowObject) {
                if (rowObject.status == "ACT") {
                    return "<a href='#' title='Attempt Reset' onClick='javascript:resAttemptInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.cif + "&#34;)'><img class='ui-icon ui-icon-arrowreturnthick-1-s' style='display:inline-table;border:none;'/></a>";
                } else {
                    return "--";
                }
            }

            function passwordResetformatter(cellvalue, options, rowObject) {
                if (rowObject.status == "ACT") {
                    return "<a href='#' title='Password Reset' onClick='javascript:restPasswordInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.cif + "&#34;)'><img class='ui-icon ui-icon-key' style='display:inline-table;border:none;'/></a>";
                } else {
                    return "--";
                }
            }

            function viewdownloadeformatter(cellvalue, options, rowObject) {
                if (rowObject.operationcode == "CUSC" || rowObject.operationcode == "UCUS" || rowObject.operationcode == "UCPA" || rowObject.operationcode == "CUUC" || rowObject.operationcode == "BLOCKA") {
                    return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }

            function viewPendInit(keyval) {
                $("#viewpenddialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openviewpendtasktopage', function (event, data) {
                var $led = $("#viewpenddialog");
                $led.html("Loading..");
                $led.load("viewPendCustomerSearch.action?id=" + $led.data('id'));
            });

            function resAttemptInit(keyval, cif) {

                $('#divmsg').empty();
                $("#attemptsresetdialog").data('keyval', keyval).dialog('open');
                $("#attemptsresetdialog").html('Are you sure you want to Attempts reset for CID : ' + cif + ' ?');
                return false;
            }

            function restPasswordInit(keyval, cif) {

                $('#divmsg').empty();
                $("#passwordresetdialog").data('keyval', keyval).dialog('open');
                $("#passwordresetdialog").html('Are you sure you want to reset password for CID : ' + cif + ' ?');
                return false;
            }

            function limitchangeformatter(cellvalue, options, rowObject) {
                if (rowObject.status == "ACT") {
                    return "<a href='#' title='Limit Change' onClick='javascript:limitChangeInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-triangle-2-e-w' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }

            function registerformatter(cellvalue, options, rowObject) {
                if (rowObject.statuscode === "ACT") {
                    return "--";
                } else {
                    return "<a href='#' title='Register Customer' onClick='javascript:registerInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                }
            }

            function reconfirmformatter(cellvalue, options, rowObject) {
                if (rowObject.statuscode == "ACT") {
                    return "<a href='#' title='Reconfirm Customer' onClick='javascript:reconfirmInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }

            function deleteformatter(cellvalue, options, rowObject) {
                if (rowObject.statuscode == "CINI") {
                    return "<a href='#/' title='Delete' onClick='javascript:deleteInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }

            function statuschangeformatter(cellvalue, options, rowObject) {
                if (rowObject.statuscode == "CINI") {
                    return "--";
                } else {
                    return "<a href='#/' title='Status Change' onClick='javascript:statusChangeInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-power' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                }
            }

            function passrestformatter(cellvalue, options, rowObject) {
                if (rowObject.statuscode == "ACT") {
                    return "<a href='#/' title='Password Reset' onClick='javascript:PasswordRestInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }

            function resetattemptsformatter(cellvalue, options, rowObject) {
                if (rowObject.attemptcount == "0") {
                    return "--";
                } else {
                    return "<a href='#/' title='Reset Attempt Count' onClick='javascript:ResetAttemptsInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-arrowreturnthick-1-n' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                }
            }

            function viewdualformatter(cellvalue, options, rowObject) {
                return "<a href='#/' title='View' onClick='javascript:viewDualInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.userid + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }
//----------------------------------------------------------------------------------------------          

            function confirmCustomer(keyval, popvar) {
                $('#divmsg').empty();

                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function rejectCustomer(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function confirmCS(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmCustomerSearch.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectCS(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectCustomerSearch.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
//----------------------------------------------------------------------------------------------
            function detailviewInit(keyval) {
                $("#viewdialog").data('userid', keyval).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#viewdialog");
                $led.html("Loading..");
                $led.load("detailviewCustomerSearch.action?userid=" + $led.data('userid'));
            });
//----------------------------------------------------------------------------------------------
            function editInit(keyval) {
                $("#editdialog").data('userid', keyval).dialog('open');
            }

            $.subscribe('openedittasktopage', function (event, data) {
                var $led = $("#editdialog");
                $led.html("Loading..");
                $led.load("detailCustomerSearch.action?userid=" + $led.data('userid'));
            });
//----------------------------------------------------------------------------------------------
            function updatecustomerInit(keyval) {
                $("#updatecustomerdialog").data('userid', keyval).dialog('open');
            }

            $.subscribe('openupdatecustomertasktopage', function (event, data) {
                var $led = $("#updatecustomerdialog");
                $led.html("Loading..");
                $led.load("customerDetailCustomerSearch.action?userid=" + $led.data('userid'));
            });
//----------------------------------------------------------------------------------------------
            function updateInit(keyval) {
                $("#updatedialog").data('userid', keyval).dialog('open');
            }

            $.subscribe('openupdatetasktopage', function (event, data) {
                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("deviceDetailUpdateCustomerSearch.action?userid=" + $led.data('userid'));
            });
//----------------------------------------------------------------------------------------------
            function primaryAccChangeInit(keyval) {
                $("#primaryaccchangedialog").data('userid', keyval).dialog('open');
            }

            $.subscribe('openprimaryaccchangetasktopage', function (event, data) {
                var $led = $("#primaryaccchangedialog");
                $led.html("Loading..");
                $led.load("primaryAccChangeCustomerSearch.action?userid=" + $led.data('userid'));
            });
//----------------------------------------------------------------------------------------------
            function limitChangeInit(keyval) {
                $("#limitchangedialog").data('userid', keyval).dialog('open');
            }

            $.subscribe('openlimittasktopage', function (event, data) {
                var $led = $("#limitchangedialog");
                $led.html("Loading..");
                $led.load("viewLimitChangeCustomerSearch.action?userid=" + $led.data('userid'));
            });
//----------------------------------------------------------------------------------------------
            function registerInit(keyval) {
                $("#registerdialog").data('userid', keyval).dialog('open');
                $('body').css({
                    overflow: 'hidden'
                });
                $('.ui-dialog-titlebar-close').on("click", function () {
                    $('body').css({
                        overflow: 'auto'
                    });
                });
            }

            $.subscribe('openregistertasktopage', function (event, data) {
                var $led = $("#registerdialog");
                $led.html("Loading..");
                $led.load("findCustomerSearch.action?userid=" + $led.data('userid'));
            });
//----------------------------------------------------------------------------------------------
            function reconfirmInit(keyval) {
                $('#divmsg').empty();

                $("#reconfirmdialog").data('keyval', keyval).dialog('open');
                $("#reconfirmdialog").html('Are you sure you want to reconfirm customer : ' + keyval + ' ?');
                return false;
            }

            function reconfirmCustomer(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/reconfirmCustomerSearch.action',
                    data: {userid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#reconfirmsuccdialog").dialog('open');
                        $("#reconfirmsuccdialog").html(data.message);
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }


//----------------------------------------------------------------------------------------------
//            function deleteInit(keyval) {
//                $('#divmsg').empty();
//
//                $("#deletedialog").data('keyval', keyval).dialog('open');
//                $("#deletedialog").html('Are you sure you want to delete customer : ' + keyval + ' ?');
//                return false;
//            }
//
//            function deleteCustomer(keyval) {
//                $.ajax({
//                    url: '${pageContext.request.contextPath}/deleteCustomerSearch.action',
//                    data: {userid: keyval},
//                    dataType: "json",
//                    type: "POST",
//                    success: function (data) {
//                        $("#deletesuccdialog").dialog('open');
//                        $("#deletesuccdialog").html(data.message);
//                    },
//                    error: function (data) {
//                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
//                    }
//                });
//            }
//----------------------------------------------------------------------------------------------
            function ResetAttemptsInit(keyval) {
                $('#divmsg').empty();

                $("#resetattemptsdialog").data('keyval', keyval).dialog('open');
                $("#resetattemptsdialog").html('Are you sure you want to reset the attempt count : ' + keyval + ' ?');
                return false;
            }

            function resetAttemptsCustomer(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/resetAttemptsCustomerSearch.action',
                    data: {userid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#resetattemptssuccdialog").dialog('open');
                        $("#resetattemptssuccdialog").html(data.message);
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

//----------------------------------------------------------------------------------------------------------------------------
            function statusChangeInit(keyval) {
                $('#divmsg').empty();

                $("#statuschangedialog").data('keyval', keyval).dialog('open');
                $("#statuschangedialog").html('Are you sure you want to change status : ' + keyval + ' ?');
                return false;
            }

            function statusChangeCustomer(keyval) {

                $.ajax({
                    url: '${pageContext.request.contextPath}/statusChangeCustomerSearch.action',
                    data: {userid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#statuschangesuccdialog").dialog('open');
                        $("#statuschangesuccdialog").html(data.message);
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }
//----------------------------------------------------------------------------------------------------------------------------
            function PasswordRestInit(keyval) {

                $('#divmsg').empty();

                $("#passwordresetdialog").data('keyval', keyval).dialog('open');
                $("#passwordresetdialog").html('Are you sure you want to reset customer password : ' + keyval + ' ?');
                return false;
            }

//            function passwordRestCustomer(keyval) {
//                $.ajax({
//                    url: '${pageContext.request.contextPath}/passwordRestCustomerSearch.action',
//                    data: {userid: keyval},
//                    dataType: "json",
//                    type: "POST",
//                    success: function (data) {
//                        $("#passwordresetsuccdialog").dialog('open');
//                        $("#passwordresetsuccdialog").html(data.message);
//                    },
//                    error: function (data) {
//                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
//                    }
//                });
//            }

            function attemptsRestCustomer(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/attemptResetCustomerSearch.action',
                    data: {userid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#attemptresetsuccdialog").dialog('open');
                        $("#attemptresetsuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }
            function passwordRestCustomer(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/passwordResetCustomerSearch.action',
                    data: {userid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#attemptresetsuccdialog").dialog('open');
                        $("#attemptresetsuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }
//----------------------------------------------------------------------------------------------------------------------------
            function viewDualInit(keyval) {
                $("#viewdualdialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openviewdualtasktopage', function (event, data) {
                var $led = $("#viewdualdialog");
                $led.html("Loading..");
                $led.load("detailDualCustomerSearch.action?id=" + $led.data('id'));
            });
//----------------------------------------------------------------------------------------------------------------------------

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function searchPage() {
                $('#message').empty();

                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                var cifSearch = $('#cifSearch').val();
                var usernameSearch = $('#usernameSearch').val();
                var nicSearch = $('#nicSearch').val();
                var statusSearch = $('#statusSearch').val();
                var firstnameSearch = $('#firstnameSearch').val();
                var lastnameSearch = $('#lastnameSearch').val();
                var mobilenoSearch = $('#mobilenoSearch').val();
                var emailSearch = $('#emailSearch').val();
                var segmentTypeSearch = $('#segmentTypeSearch').val();
                var onBoardChannelSearch = $('#onBoardChannelSearch').val();
                var onBoardTypeSearch = $('#onBoardTypeSearch').val();
                var customerCategorySearch = $('#customerCategorySearch').val();
                var waveoffSearch = $('#waveoffSearch').val();
                var defTypeSearch = $('#defTypeSearch').val();
                var defAccNoSearch = $('#defAccNoSearch').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        fromDate: fromDate,
                        toDate: toDate,
                        cifSearch: cifSearch,
                        usernameSearch: usernameSearch,
                        nicSearch: nicSearch,
                        statusSearch: statusSearch,
                        firstnameSearch: firstnameSearch,
                        lastnameSearch: lastnameSearch,
                        mobilenoSearch: mobilenoSearch,
                        emailSearch: emailSearch,
                        segmentTypeSearch: segmentTypeSearch,
                        onBoardChannelSearch: onBoardChannelSearch,
                        onBoardTypeSearch: onBoardTypeSearch,
                        customerCategorySearch: customerCategorySearch,
                        waveoffSearch: waveoffSearch,
                        defTypeSearch: defTypeSearch,
                        defAccNoSearch: defAccNoSearch,
                        search: true
                    }
                });


                $("#gridtabledual").jqGrid('setGridParam', {
                    postData: {
                        cifSearch: cifSearch,
                        usernameSearch: usernameSearch,
                        nicSearch: nicSearch,
                        statusSearch: statusSearch,
                        mobilenoSearch: mobilenoSearch,
                        emailSearch: emailSearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtabledual").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtabledual").trigger("reloadGrid");
            }

            function resetAllData() {

                $('#cifSearch').val("");
                $('#usernameSearch').val("");
                $('#nicSearch').val("");
                $('#statusSearch').val("");
                $('#firstnameSearch').val("");
                $('#lastnameSearch').val("");
                $('#mobilenoSearch').val("");
                $('#emailSearch').val("");
                $('#segmentTypeSearch').val("");
                $('#fromDate').val("");
                $('#toDate').val("");
                $('#onBoardChannelSearch').val("");
                $('#onBoardTypeSearch').val("");
                $('#customerCategorySearch').val("");
                $('#waveoffSearch').val("");
                $('#defTypeSearch').val("");
                $('#defAccNoSearch').val("");

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        fromDate: '',
                        toDate: '',
                        cifSearch: '',
                        usernameSearch: '',
                        nicSearch: '',
                        statusSearch: '',
                        firstnameSearch: '',
                        lastnameSearch: '',
                        mobilenoSearch: '',
                        emailSearch: '',
                        segmentTypeSearch: '',
                        onBoardChannelSearch: '',
                        onBoardTypeSearch: '',
                        customerCategorySearch: '',
                        waveoffSearch: '',
                        defTypeSearch: '',
                        defAccNoSearch: '',
                        search: false
                    }
                });

                $("#gridtabledual").jqGrid('setGridParam', {
                    postData: {
                        cifSearch: '',
                        usernameSearch: '',
                        nicSearch: '',
                        statusSearch: '',
                        mobilenoSearch: '',
                        emailSearch: '',
                        search: false
                    }
                });

                setdate();

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtabledual").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtabledual").trigger("reloadGrid");

            }

//            need for customer register
            function resetFieldData() {

//                $('#regiserbtn').attr('disabled', true);
//                $('#invalidsignupbtn').attr('disabled', true);

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }

            function todoexel() {
                $('#reporttype').val("exel");
                form = document.getElementById('customersearch');
                form.action = 'reportGenerateCustomerSearch';
                form.submit();
                $('#view1').button("disable");
                $('#view2').button("disable");
//                $('#view3').button("disable");
            }

            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('customersearch');
                form.action = 'reportGenerateCustomerSearch.action';
                form.submit();

                //    $('#view').button("disable");
                $('#view1').button("disable");
                $('#view2').button("disable");
//                $('#view3').button("disable");
            }

//            function todocsvFee() {
//                $('#reporttype').val("fee");
//                form = document.getElementById('customersearch');
//                form.action = 'convenienceFeeGenerateCustomerSearch.action';
//                form.submit();
//
//                //    $('#view').button("disable");
//                $('#view1').button("disable");
//                $('#view2').button("disable");
//                $('#view3').button("disable");
//            }

            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;
//                var isConfeegenerate = <s:property value="vconfeegenerate"/>;
                //  var isGenerate = false;

                if (recors > 0 && isGenerate == false) {
                    //    $('#view').button("enable");
                    $('#view1').button("enable");
                    $('#view2').button("enable");
                } else {
                    //    $('#view').button("disable");
                    $('#view1').button("disable");
                    $('#view2').button("disable");
                }
//                if (recors > 0 && isConfeegenerate == false) {
//                    $('#view3').button("enable");
//                } else {
//                    $('#view3').button("disable");
//                }
            });

            $.subscribe('onbeforetopics', function (event, data) {
                $("#gridtable").jqGrid('setGridParam', {postData: {
                        load: "yes"
                    }});
            });

            function setdate() {
                $("#fromDate").datepicker("setDate", new Date());
                $("#toDate").datepicker("setDate", new Date());
            }

            function validateCurrency(value) {
                //var value= $("#field1").val();
                var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
                var tempVal = value.val();
                if (regex.test(value.val()))
                {
                } else {
                    tempVal = "";
                }
                value.val(tempVal);
            }
            ;


        </script>
    </head>

    <body onload="setdate()">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                            <s:set var="vconfeegenerate"><s:property value="vconfeegenerate" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vstatuschange"><s:property value="vstatuschange" default="true"/></s:set>
                            <s:set var="vlimitchange"><s:property value="vlimitchange" default="true"/></s:set>
                            <s:set var="vreconfirm"><s:property value="vreconfirm" default="true"/></s:set>
                            <s:set var="vresetattempts"><s:property value="vresetattempts" default="true"/></s:set>
                            <s:set var="vinvalidsignup"><s:property value="vinvalidsignup" default="true"/></s:set>
                            <s:set var="vregister"><s:property value="vregister" default="true"/></s:set>
                            <s:set var="vviewcustomer"><s:property value="vviewcustomer" default="true"/></s:set>
                            <s:set var="vupdate"><s:property value="vupdate" default="true"/></s:set>
                            <s:set var="vviewreg" ><s:property value="vviewreg" default="true"/></s:set>
                            <s:set var="vprimaccupdate" ><s:property value="vprimaccupdate" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vattemptreset"><s:property value="vattemptreset" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>
                            <s:set var="vpasswordreset"><s:property value="vpasswordreset" default="true"/></s:set>
                            <s:set var="vblock"><s:property value="vblock" default="true"/></s:set>
                            <s:set var="vupdatecustomer"><s:property value="vupdatecustomer" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form id="customersearch" method="post" action="CustomerSearch" theme="simple" cssClass="form" >

                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>

                                        <div class="row row_1">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label >From Date</label>
                                                <sj:datepicker cssClass="form-control" id="fromDate" name="fromDate" readonly="true" maxDate="d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >To Date</label>
                                                <sj:datepicker cssClass="form-control" id="toDate" name="toDate" readonly="true" maxDate="+1d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>CID </label>
                                                <s:textfield cssClass="form-control" name="cifSearch" id="cifSearch" maxLength="30" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>User Name </label>
                                                <s:textfield cssClass="form-control" name="usernameSearch" id="usernameSearch" maxLength="35" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">        
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >User Segment</label>
                                                <s:select cssClass="form-control" name="segmentTypeSearch" id="segmentTypeSearch" headerValue="-- Select User Segment --" list="%{segmentTypeList}"   headerKey="" listKey="segmentcode" listValue="description" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >On Board Channel</label>
                                                <s:select cssClass="form-control" name="onBoardChannelSearch" id="onBoardChannelSearch" headerValue="-- Select On Board Channel --" list="%{onBoardChannelList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >On Board Type</label>
                                                <s:select cssClass="form-control" name="onBoardTypeSearch" id="onBoardTypeSearch" headerValue="-- Select On Board Type --" list="%{onBoardTypeList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Customer Category</label>
                                                <s:textfield cssClass="form-control" name="customerCategorySearch" id="customerCategorySearch" maxLength="100" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Status</label>
                                                <s:select cssClass="form-control" name="statusSearch" id="statusSearch" headerValue="-- Select Status --" list="%{statusList}"   headerKey="" listKey="statuscode" listValue="description" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>NIC</label>
                                                <s:textfield cssClass="form-control" name="nicSearch" id="nicSearch" maxLength="12" onkeyup="$(this).val($(this).val().replace(/[^0-9a-zA-Z]/g,'').toUpperCase())" onmouseout="$(this).val($(this).val().replace(/[^0-9a-zA-Z]/g,'').toUpperCase())"/>
                                            </div>
                                        </div>
                                        <!--                                        <div class="col-sm-3">
                                                                                    <div class="form-group">
                                                                                        <label>First Name</label>
                                        <s:textfield cssClass="form-control" name="firstnameSearch" id="firstnameSearch" maxLength="50" />
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <s:textfield cssClass="form-control" name="lastnameSearch" id="lastnameSearch" maxLength="50" />
                                    </div>
                                </div>-->
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Mobile</label>
                                                <s:textfield cssClass="form-control" name="mobilenoSearch" id="mobilenoSearch" maxLength="16" onkeyup="$(this).val($(this).val().replace(/[^0-9+ ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9+ ]/g,''))" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <s:textfield cssClass="form-control" name="emailSearch" id="emailSearch" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^0-9@_.a-zA-Z- ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9@_.a-zA-Z- ]/g,''))"  />
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="row row_1"> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Waive Off </label>
                                                <s:select cssClass="form-control" name="waveoffSearch" id="waveoffSearch" headerValue="-- Select Waive Off --" list="%{waveOffStatusList}"   headerKey="" listKey="statuscode" listValue="description" />
                                            </div>  
                                        </div> 
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Primary Account Type</label>
                                                <s:select cssClass="form-control" name="defTypeSearch" id="defTypeSearch" headerValue="-- Select Primary Account Type --" list="%{defaultAccountTypeList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Primary Acc/Card Number</label>
                                                <s:textfield cssClass="form-control" name="defAccNoSearch" id="defAccNoSearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"  />
                                            </div>
                                        </div>     

                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                disabled="#vsearch"
                                                onclick="searchPage()"
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"

                                                />
                                        </div> 
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"

                                                />
                                        </div>
                                        <!--                                        <div class="form-group">
                                        <%--<sj:submit--%> 
                                            cssClass="form-control btn_normal"
                                            cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                            button="true" 
                                            value="View Excel" 
                                            name="view1" 
                                            disabled="#vgenerate"
                                            id="view1" 
                                            onClick="todoexel()" 
                                            /> 
                                    </div>-->
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View CSV" 
                                                name="view2" 
                                                id="view2" 
                                                onClick="todocsv()" 
                                                disabled="#vgenerate"/> 
                                        </div>
                                        <!--                                        <div class="form-group">
                                        <sj:submit 
                                            cssClass="form-control btn_normal"
                                            cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                            button="true" 
                                            value="View Convenience Fee" 
                                            name="view3" 
                                            id="view3" 
                                            onClick="todocsvFee()" 
                                            disabled="#vconfeegenerate"/> 
                                    </div>-->
                                    </div>  
                                    <!--                                    <div class="col-sm-6 col-xs-6 text-right">
                                                                            <div class="form-group">
                                    <s:url var="regurl" action="ViewPopupRegCustomerSearch"/>
                                    <sj:submit 
                                        openDialog="remoteregdialog"
                                        button="true"
                                        href="%{regurl}"
                                        value="Customer Registration"
                                        id="regButton"   
                                        cssClass="ui-button-addtask"
                                        />
                                </div>

                            </div>-->

                                </div>

                                <div class="col-sm-8"></div>

                            </div>
                            <!--------------------------------------------------------------------------------->   
                            <sj:dialog                                     
                                id="remoteregdialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                title="Customer Registration"                            
                                loadingText="Loading .."                            
                                position="center"                            
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                />


                            <!--------------------------------------------------------------------------------->
                            <!-- Start view dialog box -->
                            <sj:dialog                                     
                                id="viewdualdialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="View Customer Confirmation"
                                onOpenTopics="openviewdualtasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                cssStyle="overflow:auto"
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start view dialog box -->
                            <sj:dialog                                     
                                id="viewdialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="View Customer"
                                onOpenTopics="openviewtasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                cssStyle="overflow:auto"
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start edit dialog box -->
                            <sj:dialog                                     
                                id="editdialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="Edit Customer"
                                onOpenTopics="openedittasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                cssStyle="overflow:auto"
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start update customer dialog box -->
                            <sj:dialog                                     
                                id="updatecustomerdialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="Update Customer Data"
                                onOpenTopics="openupdatecustomertasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                cssStyle="overflow:auto"
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start device info dialog box -->
                            <sj:dialog                                     
                                id="updatedialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="Device Info"
                                onOpenTopics="openupdatetasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                cssStyle="overflow:auto"
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start primary account dialog box -->
                            <sj:dialog                                     
                                id="primaryaccchangedialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="Primary Account Updation"
                                onOpenTopics="openprimaryaccchangetasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                cssStyle="overflow:auto"
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start register view dialog box -->
                            <sj:dialog                                     
                                id="registerdialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="Register Customer"
                                onOpenTopics="openregistertasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                cssStyle="overflow:scroll"
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start limit change view dialog box -->
                            <sj:dialog                                     
                                id="limitchangedialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="Limit Change"
                                onOpenTopics="openlimittasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                cssStyle="overflow:auto"
                                />
                            <!--block IB/MB -->
                            <sj:dialog                                     
                                id="blockdialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="Block Mobile Banking / Internet Banking "
                                onOpenTopics="openblocktasktopage" 
                                loadingText="Loading .."
                                width="1000"
                                height="550"
                                dialogClass= "new-fixed-dialog"
                                cssStyle="overflow:auto"
                                />
                            <sj:dialog 
                                id="attemptrestdialog" 
                                buttons="{ 
                                'OK':function() { resPassword($(this).data('keyval'),$(this).data('fname'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Password Reset"                            
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start delete confirm dialog box -->
                            <sj:dialog 
                                id="deletedialog" 
                                buttons="{                                    
                                'OK':function() { deleteCustomer($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Delete Customer"                            
                                />
                            <!-- Start delete process dialog box -->
                            <sj:dialog 
                                id="deletesuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                title="Deleting Process." 
                                />
                            <!-- Start delete error dialog box -->
                            <sj:dialog 
                                id="deleteerrordialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}                                    
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Delete error."
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start reset attempts count confirm dialog box -->
                            <sj:dialog 
                                id="resetattemptsdialog" 
                                buttons="{                                    
                                'OK':function() { resetAttemptsCustomer($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Reset Attempt Count"

                                />
                            <!-- Start delete process dialog box -->
                            <sj:dialog 
                                id="resetattemptssuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                title="Attempt count reseting Process." 
                                />
                            <!-- Start delete error dialog box -->
                            <sj:dialog 
                                id="resetattemptssuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}                                    
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Attempt count reset error."
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start reconfirm dialog box -->
                            <sj:dialog 
                                id="reconfirmdialog" 
                                buttons="{                                    
                                'OK':function() { reconfirmCustomer($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Reconfirm Customer"                            
                                />
                            <!-- Start delete process dialog box -->
                            <sj:dialog 
                                id="reconfirmsuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                title="Reconfirming Process." 
                                />
                            <!-- Start delete error dialog box -->
                            <sj:dialog 
                                id="reconfirmerrordialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}                                    
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Reconfirm error."
                                />
                            <!--------------------------------------------------------------------------------->
                            <!-- Start password reset confirm dialog box -->
                            <sj:dialog 
                                id="passwordresetdialog" 
                                buttons="{                                    
                                'OK':function() { passwordRestCustomer($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Customer Password Reset "                            
                                />
                            <!-- Start attempt reset confirm dialog box -->
                            <sj:dialog 
                                id="attemptsresetdialog" 
                                buttons="{                                    
                                'OK':function() { attemptsRestCustomer($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Customer Attempts Reset "                            
                                />
                            <!-- Start password reset confirm dialog box -->
                            <%--<sj:dialog
                                id="passwordresetdialog" 
                                buttons="{                                    
                                'OK':function() { passwordRestCustomer($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Customer Attempts Reset "                            
                                />--%> 
                            <!-- Start delete process dialog box -->
                            <sj:dialog 
                                id="passwordresetsuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                title="Password Resetting Process." 
                                />
                            <sj:dialog 
                                id="attemptresetsuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                title="Customer Attempts Resetting Process." 
                                />
                            <!-- Start delete error dialog box -->
                            <sj:dialog 
                                id="passwordreseterrordialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}                                    
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Password Reset Error."
                                />

                            <!--------------------------------------------------------------------------------------------->
                            <!-- Start status change confirm dialog box -->
                            <sj:dialog 
                                id="statuschangedialog" 
                                buttons="{                                    
                                'OK':function() { statusChangeCustomer($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Customer Status Change"                            
                                />
                            <!-- Start delete process dialog box -->
                            <sj:dialog 
                                id="statuschangesuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                title="Status Changing Process." 
                                />
                            <!-- Start delete error dialog box -->
                            <sj:dialog 
                                id="statuschangeerrordialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}                                    
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Status Change error."
                                />
                            <!------------------------------------------------------------------------------------------->
                            <!-- Start approve confirm dialog box -->
                            <sj:dialog 
                                id="confirmdialog" 
                                buttons="{ 
                                'OK':function() { confirmCS($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                dialogClass= "fixed-dialog"
                                width="600"
                                height="300"
                                title="Approve Requested Operation"                            
                                />
                            <!-- Start approve process dialog box -->
                            <sj:dialog 
                                id="confirmsuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                width="350"
                                title="Requested Operation Approving Process" 
                                />

                            <!-- Start reject confirm dialog box -->
                            <sj:dialog 
                                id="rejectdialog" 
                                buttons="{ 
                                'OK':function() { rejectCS($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                dialogClass= "fixed-dialog"
                                width="600"
                                height="300"
                                title="Reject Requested Operation"                            
                                />
                            <!-- Start reject process dialog box -->
                            <sj:dialog 
                                id="rejectsuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                dialogClass= "fixed-dialog"
                                width="350"
                                title="Requested Operation Rejecting Process" 
                                />
                            <!-- Start Pend view dialog box -->
                            <sj:dialog                                     
                                id="viewpenddialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="View Pending Customer Search"
                                onOpenTopics="openviewpendtasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                />
                        </div>
                        <div id="tablediv">
                            <s:url var="listurl" action="listCustomerSearch"/>
                            <s:set var="pcaption">${CURRENTPAGE}</s:set>

                            <sjg:grid
                                id="gridtable"
                                caption="%{pcaption}"
                                dataType="json"
                                href="%{listurl}"
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="10"
                                autowidth="true"
                                rownumbers="true"
                                onCompleteTopics="completetopics"
                                rowTotal="false"
                                viewrecords="true"
                                onErrorTopics="anyerrors"
                                shrinkToFit="false"
                                >
                                <sjg:gridColumn name="userid" index="u.userid" title="View Customer" width="45"  formatter="detailviewformatter" sortable="false" hidden="#vviewcustomer" align="center" />  
                                <sjg:gridColumn name="userid" index="u.userid" title="Edit Customer" width="45"  formatter="editformatter" sortable="false" hidden="#vstatuschange" align="center" />  
                                <sjg:gridColumn name="userid" index="u.userid" title="Update Customer" width="45"  formatter="updatecustomerformatter" sortable="false" hidden="#vupdatecustomer" align="center" />  
                                <sjg:gridColumn name="userid" index="u.userid" title="Device info" width="45"  formatter="updateformatter" sortable="false" hidden="#vupdate" align="center" />  
                                <%--<sjg:gridColumn name="userid" index="u.userid" title="Limit Change" width="45" formatter="limitchangeformatter" sortable="false" hidden="#vlimitchange" align="center" />--%>
                                <sjg:gridColumn name="userid" index="u.userid" title="Limit Change" width="45" formatter="limitchangeformatter" sortable="false" hidden="true" align="center" />
                                <sjg:gridColumn name="userid" index="u.userid" title="Primary Account Updation" width="45" formatter="primaryaccchangeformatter" sortable="false" hidden="#vprimaccupdate" align="center" />
                                <sjg:gridColumn name="userid" index="u.userid" title="Attempts Reset" width="45" formatter="attemptResetformatter" sortable="false" hidden="#vattemptreset" align="center" />
                                <sjg:gridColumn name="userid" index="u.userid" title="Password Reset" width="45" formatter="passwordResetformatter" sortable="false" hidden="#vpasswordreset" align="center" />                                                      
                                <sjg:gridColumn name="userid" index="u.userid" title="Block IB/MB" width="45" formatter="blockformatter" sortable="false" hidden="#vblock" align="center"  />

                                <sjg:gridColumn name="userid" index="u.userid" title="Unique ID"  sortable="false" width="50"/>    
                                <sjg:gridColumn name="cif" index="u.cif" title="CID"  sortable="false" width="50"/>
                                <sjg:gridColumn name="username" index="u.username" title="User Name"  sortable="false" width="70"/>
                                <sjg:gridColumn name="promotionMtSegmentType" index="u.USER_SEGMENT" title="User Segment"  sortable="false" width="70"/>
                                <sjg:gridColumn name="onBoardChannel" index="u.onBoardChannel" title="On Board Channel"  sortable="false" width="70"/>
                                <sjg:gridColumn name="onBoardType" index="u.onBoardType" title="On Board Type"  sortable="false" width="70"/>
                                <sjg:gridColumn name="customerCategory" index="u.customerCategory" title="Customer Category"  sortable="false" width="70"/>
                                <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="false" width="70" hidden="true"/>
                                <sjg:gridColumn name="statusDes" index="u.statusDes" title="Status"  sortable="false" width="70"/>
                                <sjg:gridColumn name="waveoff" index="u.waveoff" title="Waive Off"  sortable="false" width="70" />
                                <sjg:gridColumn name="chargeAmount" index="u.CHARGE_AMOUNT" title="Charge Amount"  sortable="false"/> 
                                <sjg:gridColumn name="loginStatus" index="u.loginStatus" title="Login Status"  sortable="false" width="70"/>
                                <sjg:gridColumn name="defType" index="u.defType" title="Primary Account Type"  sortable="false" width="70"/>
                                <sjg:gridColumn name="defAccNo" index="u.defAccNo" title="Primary Acc/Card Number"  sortable="false" width="70"/>
                                <sjg:gridColumn name="customerName" index="u.CUSTOMER_NAME" title="Customer Name"  sortable="false"  width="70"/>
                                <sjg:gridColumn name="nic" index="u.nic" title="NIC"  sortable="false" width="70"/>
                                <sjg:gridColumn name="dob" index="u.dob" title="DOB"  sortable="false"  width="70"/>
                                <sjg:gridColumn name="mobileNumber" index="u.mobileNumber" title="Mobile"  sortable="false" width="70"/>
                                <sjg:gridColumn name="secondaryMobile" index="u.secondaryMobile" title="Mobile(Secondary)"  sortable="false" width="70"/>
                                <sjg:gridColumn name="email" index="u.email" title="Email"  sortable="false"/>
                                <sjg:gridColumn name="secondaryEmail" index="u.secondaryEmail" title="Email(Secondary)"  sortable="false"/>
                                <%--<sjg:gridColumn name="permanentaddress" index="u.permanentaddress" title="Address"  sortable="false"/>--%>
                                <sjg:gridColumn name="loginAttemptStatus" index="u.loginAttempts" title="Block Status"  sortable="false"/>           
                                <sjg:gridColumn name="loginAttempts" index="u.loginAttempts" title="Invalid Login Attempts"  sortable="false"/>           
                                <sjg:gridColumn name="lastLoggedinDatetime" index="u.lastLoggedinDatetime" title="Last Logged In Date And Time"  sortable="false"/>           
                                <sjg:gridColumn name="lastPassUpdateTime" index="u.lastPassUpdateTime" title="Last Password Updated Date And Time"  sortable="false"/>           
                                <sjg:gridColumn name="finalFeeDudDay" index="u.finalFeeDudDay" title="Fee Cycle Start Date"  sortable="false"/>           
                                <sjg:gridColumn name="userTraceNumber" index="u.userTraceNumber" title="User Trace Number"  sortable="true" frozen="false" hidden="false"/>
                                <sjg:gridColumn name="accountofficer" index="u.accountofficer" title="Account Officer"  sortable="false"/>
                                <sjg:gridColumn name="remark" index="u.remark" title="Remark"  sortable="false" />
                                <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="false" />
                                <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="false" />                                        
                                <sjg:gridColumn name="createdtime" index="u.createdtime" title="Created Date And Time"  sortable="false" />   
                                <sjg:gridColumn name="lastupdatedtime" index="u.lastupdatedtime" title="Last Updated Date And Time"  sortable="false" />   
                            </sjg:grid> 
                        </div>

                        <div id="tablediv">
                            <s:url var="listurlap" action="listDualCustomerSearch"/>

                            <sjg:grid
                                id="gridtablePend"                                    
                                dataType="json"
                                href="%{listurlap}"
                                pager="true"
                                caption="Pending Customer Search"
                                gridModel="gridModelPend"
                                rowList="10,15,20"
                                rowNum="10"
                                autowidth="true"
                                rownumbers="true"
                                rowTotal="false"
                                viewrecords="true"  

                                >


                                <sjg:gridColumn name="id" index="id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                <sjg:gridColumn name="id" index="id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>
                                <sjg:gridColumn name="id" index="u.id" title="View" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/> 

                                <sjg:gridColumn name="userid" index="u.pKey" title="Unique ID"  sortable="true" frozen="true" width="50"/>      
                                <sjg:gridColumn name="cif" index="u.pKey" title="CID"  sortable="true" frozen="true" width="50"/>      
                                <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="false" key="true"/>
                                <sjg:gridColumn name="operationcode" index="u.operationcode" title="Operation Code"  sortable="false" hidden="true" key="true"/>
                                <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="false" key="true"/>
                                <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="false"/>                                                                    
                                <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="false"/>  
                                <sjg:gridColumn name="createtime" index="u.createdtime" title="Created Date And Time"  sortable="false" />                                                                  

                            </sjg:grid>  
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
        <!--</div>-->
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->



        <!-- end: BODY -->
    </body>
</html>
