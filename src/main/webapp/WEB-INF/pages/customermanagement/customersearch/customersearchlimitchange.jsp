<%-- 
    Document   : customersearchlimitchange
    Created on : Dec 21, 2017, 12:20:40 PM
    Author     : dilanka_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>view customer</title> 
        <script>


            function deleteformattertxnlimit(cellvalue, options, rowObject) {                
                return "<a href='#' title='Delete' onClick='javascript:removeTxnTypeLimitInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function removeTxnTypeLimitInit(keyval) {
                $('#messagelimit').empty();

                $("#deletedialoglimit").data('keyval', keyval).dialog('open');
                $("#deletedialoglimit").html('Are you sure you want to delete transaction type : ' + keyval + ' ?');
                return false;
            }

            function deleteLimit(keyval) {

                var userid = $('#userid').val();

                $.ajax({
                    url: '${pageContext.request.contextPath}/deleteLimitCustomerSearch.action',
                    data: {txntype: keyval,
                        userid: userid
                    },
                    dataType: "json",
                    type: "POST",

                    success: function (data) {
                        $("#deletesuccdialoglimit").dialog('open');
                        $("#deletesuccdialoglimit").html(data.message);
                        resetFieldData();                    
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function editformattertxnlimit(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:editTxnLimit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function editTxnLimit(keyval) {

                var seg = $('#CusSegmentTypelimit').val();
                var userid = $('#userid').val();

                $.ajax({
                    url: '${pageContext.request.contextPath}/findTxnLimitCustomerSearch.action',
                    data: {txntype: keyval,
                        CusSegmentTypelimit: seg,
                        userid: userid
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#messagelimit').empty();
                        var msg = data.message;
                        if (msg) {

                            $('#txntypelimit').val("");
                            $('#userLimit').val("");
                            $('#statuslimit').val("");
                            $('#messagelimit').text("");

                        } else {
                             $('#messagelimit').text("");
                            $('#txntypelimit').prop('disabled', true);
                            $('#txntypelimit').val(data.txntype);
                            $('#txntypelimithidden').val(data.txntype);
                            $('#txntype').val(data.txntype);
                            $('#userLimit').val(data.userLimit);
                            $('#statuslimit').val(data.status);

                            $('#globalVal').val(data.globalVal);
                            $('#globalVal').html(data.globalVal);
                            $('#useridLimit').val(data.userid);
                            $('#addlimitbtn').hide();
                            $('#updatelimitbtn').show();

                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function findMax(keyval) {

                var seg = $('#CusSegmentTypelimit').val();
                var userid = $('#userid').val();

                $.ajax({
                    url: '${pageContext.request.contextPath}/findTxnLimitGlobalCustomerSearch.action',
                    data: {txntype: keyval,
                        CusSegmentTypelimit: seg,
                        userid: userid
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#messagelimit').empty();
                        var msg = data.message;
                        if (msg) {

                            $('#txntypelimit').val("");
                            $('#userLimit').val("");
                            $('#statuslimit').val("");
                            $('#messagelimit').html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span>"
                                    +"<span>"+msg
                                    +"</span></p></div></div>");
//                            $('#messagelimit').html(msg);

                        } else {
                            $('#messagelimit').text("");
                            $('#globalVal').val(data.globalVal);
                            $('#globalVal').html(data.globalVal);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function resetFieldData() {
                $('#txntypelimit').val("");
                $('#userLimit').val("");
//                $('#messagelimit').text("");

                $('#txntypelimit').prop('disabled', false);
                $('#txntypelimit').val("");

                
                $('#statuslimit').val("");
                $('#globalVal').text("");
                jQuery("#gridtabletxnlimit").trigger("reloadGrid");

                $('#updatelimitbtn').hide();
                $('#addlimitbtn').show();

            }
            function resetALLData() {
                $('#txntypelimit').val("");
                $('#userLimit').val("");
                $('#messagelimit').text("");

                $('#txntypelimit').prop('disabled', false);
                $('#txntypelimit').val("");

                
                $('#statuslimit').val("");
                $('#globalVal').text("");
                jQuery("#gridtabletxnlimit").trigger("reloadGrid");

                $('#updatelimitbtn').hide();
                $('#addlimitbtn').show();

            }



        </script>
    </head>
    <body>

        <s:div id="messagelimit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:set var="vlimitadd" ><s:property value="vlimitadd" default="true"/></s:set>
        <s:set var="vlimitupdate" ><s:property value="vlimitupdate" default="true"/></s:set>
        <s:set var="vlimitdelete" ><s:property value="vlimitdelete" default="true"/></s:set>
        <s:form id="limitcs" method="post" action="CustomerSearch" theme="simple" cssClass="form" >
            <s:hidden name="csrfValue" id="csrfValue" value="%{#session.csrfValue}"/>
            <s:hidden id="userid" name="userid" value="%{userid}"></s:hidden>
            <s:hidden id="txntype" name="txntype" />
            <s:hidden id="CusSegmentTypelimit" name="CusSegmentTypelimit" />

            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="horizontal_line_popup"></div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Customer ID/CID</label>
                        <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="ciflimit" id="ciflimit" cssClass="form-control" />
                    </div>  
                </div> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Transaction Type</label>
                        <s:select cssClass="form-control" name="txntypelimit" id="txntypelimit" headerValue="-- Select Transaction Type --" list="%{transactiontypeList}"   headerKey="" listKey="transferId" listValue="description" onchange="findMax(value)"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>User Limit</label>

                        <s:textfield  cssClass="form-control" name="userLimit" id="userLimit" maxLength="13" 
                                      />    
                        <span style="color: red"><label><strong>Max Value : </strong></label></span>
                                    <s:label name="globalVal" id="globalVal" maxLength="16"/>   
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" headerValue="--Select Status--" headerKey="" cssClass="form-control"  id="statuslimit" list="%{statusList}"  name="statuslimit" listKey="statuscode" listValue="description"/>                  
                    </div>
                </div>
            </div>  
            <div class="row row_popup form-inline">                
                <div class="col-sm-12 text-right">
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            id="canceladdlimit"
                            onClick="resetALLData()"
                            cssClass="ui-button-reset"
                            />                          
                    </div>
                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">
                        <s:url action="AddLimitCustomerSearch" var="insertlimiturl"/>
                        <sj:submit
                            button="true"
                            value="Add Limit"
                            href="%{insertlimiturl}"
                            targets="messagelimit"
                            id="addlimitbtn"
                            cssClass="ui-button-submit" 
                            disabled="#vlimitadd"
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">
                        <s:url action="UpdateLimitCustomerSearch" var="updatelimiturl"/>
                        <sj:submit
                            button="true"
                            value="Update Limit"
                            href="%{updatelimiturl}"
                            targets="messagelimit"
                            id="updatelimitbtn"
                            cssClass="ui-button-submit" 
                            hidden="true"
                            />                        
                    </div>
                </div>
            </div>

            <div style="margin-top: 20px;">

                <s:url var="listurl" action="listLimitCustomerSearch?userid=%{userid}"/>

                <sjg:grid

                    id="gridtabletxnlimit"
                    caption="Transaction Type Limit Change"
                    dataType="json"
                    href="%{listurl}"
                    pager="true"
                    gridModel="gridModelLimit"
                    rowList="10,15,20"
                    targets="messagelimit"
                    rowNum="10"
                    autowidth="true"
                    rownumbers="true"
                    onCompleteTopics="completetopics"
                    rowTotal="false"
                    viewrecords="true"
                    shrinkToFit="true"
                    >
                    <sjg:gridColumn name="txntype" index="txntype" title="Transaction Type" sortable="false" hidden="true"/>
                    <sjg:gridColumn name="status" index="status" title="Status" sortable="false" hidden="true"/>
                    <sjg:gridColumn name="txntype" index="txntype" title="Edit" width="60" align="center" sortable="false" formatter="editformattertxnlimit" hidden="#vlimitupdate"/>
                    <sjg:gridColumn name="txntype" index="txntype" title="Delete" width="70" align="center" sortable="false" formatter="deleteformattertxnlimit" hidden="#vlimitdelete"/> 
                    <sjg:gridColumn name="txntypedes" index="txntypedes" title="Transaction Type" sortable="false"/>
                    <sjg:gridColumn name="userLimit" index="userLimit" title="Min Amount" sortable="false" />    
                    <sjg:gridColumn name="statusdes" index="statusdes" title="Status"  sortable="false" />
                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true" />
                    <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true" />                                        
                    <sjg:gridColumn name="createdTime" index="u.createdTime" title="Created Date And Time"  sortable="true" />   
                    <sjg:gridColumn name="lastUpdatedTime" index="u.lastUpdatedTime" title="Last Updated Date And Time"  sortable="true" /> 
                </sjg:grid> 

            </div>
        </s:form>

        <sj:dialog 
            id="deletedialoglimit" 
            buttons="{ 
            'OK':function() { deleteLimit($(this).data('keyval'));$( this ).dialog( 'close' ); },
            'Cancel':function() { $( this ).dialog( 'close' );} 
            }" 
            autoOpen="false" 
            modal="true" 
            title="Delete Limit"                            
            />
        <sj:dialog 
            id="deletesuccdialoglimit" 
            buttons="{
            'OK':function() { $( this ).dialog( 'close' );}
            }"  
            autoOpen="false" 
            modal="true" 
            title="Deleting Process." 
            />
        <!-- Start delete error dialog box -->
        <sj:dialog 
            id="deleteerrordialoglimit" 
            buttons="{
            'OK':function() { $( this ).dialog( 'close' );}                                    
            }" 
            autoOpen="false" 
            modal="true" 
            title="Delete error."
            />
    </body>
</html>
