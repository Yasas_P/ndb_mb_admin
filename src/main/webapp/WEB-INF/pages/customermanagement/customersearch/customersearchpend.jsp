<%-- 
    Document   : customersearchpend
    Created on : Aug 6, 2019, 10:50:25 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View Pend Customer Search</title> 
    </head>
    <script type="text/javascript">
        $(document).ready(function () {
            var taskCode = $('#taskCodePendView').val();
            if (taskCode == "CUSC") {
                $('#oldNewValCustomerStatus').show();
            } else if (taskCode == "UCUS") {
                $('#pendDeviceDetailId').show();
                $('#oldNewValCusDeviceStatus').show();
            } else if (taskCode == "UCPA") {
                $('#oldNewValCusPrimeAcc').show();
            } else if (taskCode == "CUUC") {
                $('#oldNewValCusDataUpdate').show();
            } else if (taskCode == "BLOCKA") {
                $('#oldNewValApplicationStatusUpdate').show();
            }
        });
    </script>
    <body>
        <s:div id="pmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="customersearchpend" method="post" action="CustomerSearch" theme="simple" cssClass="form" enctype="multipart/form-data"  >
            <s:hidden id="taskCodePendView" name="taskCode" value="%{pendOldNew.taskCode}" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>User Name</label>
                        <s:textfield value="%{pendOldNew.username}" cssClass="form-control" name="username" id="username_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>CID</label>
                        <s:textfield value="%{pendOldNew.cid}" cssClass="form-control" id="cid_p"  name="cif"  readonly="true"  />
                    </div>
                </div>               
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>NIC</label>
                        <s:textfield value="%{pendOldNew.nic}" cssClass="form-control" id="nic_p" name="nic"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Task</label>
                        <s:textfield value="%{pendOldNew.taskDescription}" cssClass="form-control" name="task" id="task_p"  readonly="true" />
                    </div>
                </div>               
            </div>
            <div id="pendDeviceDetailId"  hidden="true">        
                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Device ID</label>
                            <s:textfield value="%{pendOldNew.deviceId}" cssClass="form-control" name="deviceId" id="deviceId_p" readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Device Nick Name</label>
                            <s:textfield value="%{pendOldNew.deviceNickName}" cssClass="form-control" name="deviceNickName" id="deviceNickName_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Build Number</label>
                            <s:textfield value="%{pendOldNew.deviceBuildNumber}" cssClass="form-control" name="deviceBuildNumber" id="deviceBuildNumber_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Device Manufacture</label>
                            <s:textfield value="%{pendOldNew.deviceManufacture}" cssClass="form-control" name="deviceManufacture" id="deviceManufacture_p" readonly="true" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Field(s)</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Old Value</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>New Value</label>
                    </div>
                </div>
            </div>
            <div id="oldNewValCustomerStatus"  hidden="true">            
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Customer Status</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldCustomerStatus}" cssClass="form-control" name="oldCustomerStatus" id="oldCustomerStatus_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newCustomerStatus}" cssClass="form-control" name="newCustomerStatus" id="newCustomerStatus_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Waive Off Status</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldWaveOffStatus}" cssClass="form-control" name="oldWaveOffStatus" id="oldWaveOffStatus_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newWaveOffStatus}" cssClass="form-control" name="newWaveOffStatus" id="newWaveOffStatus_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Charge Amount</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldChargeAmount}" cssClass="form-control" name="oldChargeAmount" id="oldChargeAmount_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newChargeAmount}" cssClass="form-control" name="newChargeAmount" id="newChargeAmount_p"  readonly="true" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="oldNewValApplicationStatusUpdate"  hidden="true">            

                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Mobile Banking Status</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldMobStatus}" cssClass="form-control" name="oldMobStatus" id="oldMobStatusp"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newMobStatus}" cssClass="form-control" name="newMobStatus" id="newMobStatus_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Internet Banking Status</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldIbStatus}" cssClass="form-control" name="oldIbStatus" id="oldIbStatus_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newIbStatus}" cssClass="form-control" name="newIbStatus" id="newIbStatus_p"  readonly="true" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="oldNewValCusDeviceStatus"  hidden="true">  
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Device Status</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldDeviceStatus}" cssClass="form-control" name="oldDeviceStatus" id="oldDeviceStatus_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newDeviceStatus}" cssClass="form-control" name="newDeviceStatus" id="newDeviceStatus_p"  readonly="true" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="oldNewValCusPrimeAcc" hidden="true">
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Default Acc Type</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldDefaultAccType}" cssClass="form-control" name="oldDefaultAccType" id="oldDefaultAccType_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newDefaultAccType}" cssClass="form-control" name="newDefaultAccType" id="newDefaultAccType_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Default Acc Number</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldDefaultAccNo}" cssClass="form-control" name="oldDefaultAccNo" id="oldDefaultAccNo_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newDefaultAccNo}" cssClass="form-control" name="newDefaultAccNo" id="newDefaultAccNo_p"  readonly="true" />
                        </div>
                    </div>
                </div>
            </div>
            <div id="oldNewValCusDataUpdate" hidden="true">
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Customer Name</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldCustomerName}" cssClass="form-control" name="oldCustomerName" id="oldCustomerName_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newCustomerName}" cssClass="form-control" name="newCustomerName" id="newCustomerName_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <!--                <div class="row row_popup">
                                    <div class="col-sm-3" >
                                        <div class="form-group">
                                            <label>Status Code</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                <%--<s:textfield value="%{pendOldNew.oldStatus}" cssClass="form-control" name="oldStatus" id="oldStatus_p"  readonly="true" />--%>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <%--<s:textfield value="%{pendOldNew.newStatus}" cssClass="form-control" name="newStatus" id="newStatus_p"  readonly="true" />--%>
            </div>
        </div>
    </div>-->
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Segment Type</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldSegmentType}" cssClass="form-control" name="oldSegmentType" id="oldSegmentType_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newSegmentType}" cssClass="form-control" name="newSegmentType" id="newSegmentType_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Customer Category</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldCustomerCategory}" cssClass="form-control" name="oldCustomerCategory" id="oldCustomerCategory_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newCustomerCategory}" cssClass="form-control" name="newCustomerCategory" id="newCustomerCategory_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>NIC</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldNic}" cssClass="form-control" name="oldNic" id="oldNic_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldNic}" cssClass="form-control" name="oldNic" id="oldNic_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>DOB</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldDob}" cssClass="form-control" name="oldDob" id="oldDob_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newDob}" cssClass="form-control" name="newDob" id="newDob_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Gender</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldGender}" cssClass="form-control" name="oldGender" id="oldGender_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newGender}" cssClass="form-control" name="newGender" id="newGender_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Mobile Number</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldMobileNumber}" cssClass="form-control" name="oldMobileNumber" id="oldMobileNumber_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newMobileNumber}" cssClass="form-control" name="newMobileNumber" id="newMobileNumber_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Email</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldEmail}" cssClass="form-control" name="oldEmail" id="oldEmail_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newEmail}" cssClass="form-control" name="newEmail" id="newEmail_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Secondary Mobile</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldSecondaryMobile}" cssClass="form-control" name="oldSecondaryMobile" id="oldSecondaryMobile_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newSecondaryMobile}" cssClass="form-control" name="newSecondaryMobile" id="newSecondaryMobile_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Secondary Email</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldSecondaryEmail}" cssClass="form-control" name="oldSecondaryEmail" id="oldSecondaryEmail_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newSecondaryEmail}" cssClass="form-control" name="newSecondaryEmail" id="newSecondaryEmail_p"  readonly="true" />
                        </div>
                    </div>
                </div>
<!--                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Address</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldPermanentAdd}" cssClass="form-control" name="oldPermanentAdd" id="oldPermanentAdd_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newPermanentAdd}" cssClass="form-control" name="newPermanentAdd" id="newPermanentAdd_p"  readonly="true" />
                        </div>
                    </div>
                </div>-->
                <div class="row row_popup">
                    <div class="col-sm-3" >
                        <div class="form-group">
                            <label>Account Officer</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.oldAccountofficer}" cssClass="form-control" name="oldAccountofficer" id="oldAccountofficer_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <s:textfield value="%{pendOldNew.newAccountofficer}" cssClass="form-control" name="newAccountofficer" id="newAccountofficer_p"  readonly="true" />
                        </div>
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
