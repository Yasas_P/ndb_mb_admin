<%-- 
    Document   : customersearchupdate
    Created on : Mar 7, 2018, 3:24:43 PM
    Author     : dilanka_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>view customer</title> 
    </head>
    <script>
        function resetInputs() {

            $("#amessageupdate").text("");

            var userid = $("#useridupdate").val();

            $.ajax({
                url: '${pageContext.request.contextPath}/findUpdateCustomerSearch.action',
                data: {userid: userid},
                dataType: "json",
                type: "POST",
                success: function (data) {
                    if (data.message) {
                        $("#amessageupdate").text(data.message);
                        $("#contactMobileupdate").val("");
                        $("#emailupdate").val("");
                    } else {
                        $("#amessageupdate").text("");
                        $("#contactMobileupdate").val(data.contactMobile);
                        $("#emailupdate").val(data.email);

                    }
                },
                error: function (data) {
                    window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                }
            });

        }

        function editdeviceformatter(cellvalue, options, rowObject) {
            return "<a href='#' title='Edit' onClick='javascript:editDeviceInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
        }

        function editDeviceInit(keyval) {
            $.ajax({
                url: '${pageContext.request.contextPath}/findDeviceCustomerSearch.action',
                data: {deviceid: keyval
                },
                dataType: "json",
                type: "POST",
                success: function (data) {
                    $('#amessageupdate').empty();
                    var msg = data.message;
                    if (msg) {

                        $('#oldvalue').val("");
                        $('#nicknameview').text("");
                        $('#deviceManufactureview').text("");
                        $('#modelnoview').text("");
                        $('#deviceBuildNumberview').text("");
                        $('#statusSearchview').val("");

                        $('#updateButtonedit').button("disable");

                    } else {

                        $('#deviceid').val(data.deviceid);

                        $('#nicknameview').text(data.deviceNickName);
                        $('#modelnoview').text(data.deviceModel);
                        $('#deviceBuildNumberview').text(data.deviceBuildNumber);
                        $('#deviceManufactureview').text(data.deviceManufacture);

                        $('#statusSearchview').val(data.status);
                        $('#updateButtonedit').button("enable");


//                        $("#devicegridtable").jqGrid('setGridParam', {dattype: 'json'}).trigger('reloadGrid');
//                        jQuery("#devicegridtable").trigger("reloadGrid");
                    }
                },
                error: function (data) {
                    window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                }
            });
        }

        function resetFieldData() {

            $('#oldvalue').val("");
            $('#nicknameview').text("");
            $('#deviceManufactureview').text("");
            $('#modelnoview').text("");
            $('#deviceBuildNumberview').text("");
            $('#statusSearchview').val("");
            $('#statusSearchview').attr('disable', false);

            jQuery("#devicegridtable").trigger("reloadGrid");
        }


    </script>
    <body>
        <s:div id="amessageupdate">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>

        <s:set var="vupdate"><s:property value="vupdate" default="true"/></s:set>

        <s:form id="updatecustomer" method="post" action="*CustomerSearch" theme="simple" cssClass="form" >
            <s:hidden name="csrfValue" id="csrfValue" value="%{#session.csrfValue}"/>
            <%--<s:hidden name="userid" id="useridupdate"/>--%>
            <s:hidden id="useridupdate" name="userid" value="%{userid}"></s:hidden>
            <s:hidden id="deviceid" name="deviceid"></s:hidden>

                <div class="row row_popup">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="row row_popup"> 
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label >Customer ID/CID</label>
                                    <s:label disabled="false" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="cif" id="cifview" cssClass="form-control" />
                                </div>  
                            </div> 
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label >NIC/Passport No</label>
                                    <td><s:label disabled="false" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="nic" id="nicview" cssClass="form-control"/>
                                </div>
                            </div>
                        </div>
                        <div class="row row_popup">
                            <div class="horizontal_line_popup"></div>
                        </div>
                    </div>              
                </div>
                <div class="col-sm-12">
                    <div class="row row_popup"> 
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label >Device Nick Name</label>                              
                            </div>  
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="nickname" id="nicknameview" cssClass="form-control" />
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row row_popup"> 
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label >Model No</label>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <td><s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="modelno" id="modelnoview" cssClass="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row row_popup"> 
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label >Build Number</label>

                            </div>
                        </div> 
                        <div class="col-sm-7">
                            <div class="form-group">
                                <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="deviceBuildNumber" id="deviceBuildNumberview" cssClass="form-control" />
                            </div>
                        </div> 
                    </div>
                </div>            
                <div class="col-sm-12">
                    <div class="row row_popup"> 
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label >Manufacture</label>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="form-group">
                                <td><s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="deviceManufacture" id="deviceManufactureview" cssClass="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>    
                <div class="col-sm-12" >
                    <div class="row row_popup"> 
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>Device Block Status</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <s:select cssClass="form-control" name="devicestatus" id="statusSearchview" headerValue="-- Select Status --" list="%{statusList}"   headerKey="" listKey="statuscode" listValue="description" />
                            </div>
                        </div>
                        <div class="col-sm-4    ">
                            <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                                <s:url action="UpdateDeviceStatusCustomerSearch" var="updateturl"/>
                                <sj:submit
                                    button="true"
                                    value="Update"
                                    href="%{updateturl}"
                                    targets="amessageupdate"
                                    id="updateButtonedit"
                                    cssClass="ui-button-submit" 
                                    />     
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </s:form> 
    <div id="tablediv">

        <s:url var="listurl" action="listDeviceCustomerSearch?userid=%{userid}"/>

        <sjg:grid
            id="devicegridtable"
            caption="Device Info :"
            dataType="json"
            href="%{listurl}"
            pager="true"
            gridModel="gridModelDevice"
            rowList="10,15,20"
            rowNum="10"
            autowidth="true"
            rownumbers="true"
            onCompleteTopics="completetopics"
            rowTotal="false"
            viewrecords="true"
            onErrorTopics="anyerrors"
            sortable="false"
            shrinkToFit="false"
            >

            <sjg:gridColumn name="deviceid" index="b.deviceid" title="Edit" width="30" sortable="false" formatter="editdeviceformatter"/>

            <sjg:gridColumn name="deviceid" index="b.deviceid" title="Device ID"  sortable="true" frozen="true"/>
            <sjg:gridColumn name="deviceNickName" index="b.deviceNickName" title="Device Nickname"  sortable="true" frozen="true"/>

            <sjg:gridColumn name="deviceModel" index="b.deviceModel" title="Model"  sortable="true"/>
            <sjg:gridColumn name="deviceBuildNumber" index="b.deviceBuildNumber" title="BuildNumber"  sortable="true"/>
            <sjg:gridColumn name="deviceManufacture" index="b.deviceManufacture" title="Manufacture"  sortable="true"/>
            <sjg:gridColumn name="deviceOsVersion" index="b.deviceOsVersion" title="Os Version"  sortable="true"/>
            <sjg:gridColumn name="devicestatus" index="b.devicestatus" title="Status"  sortable="true" hidden="true"/>
            <sjg:gridColumn name="devicestatusDes" index="b.devicestatusDes" title="Block Status"  sortable="true"/>
            <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true" />
            <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true" />                                        
            <sjg:gridColumn name="createdtime" index="u.createdTime" title="Created Date And Time"  sortable="true" />   
            <sjg:gridColumn name="devicelastUpdatedTime" index="u.lastUpdatedTime" title="Last Updated Date And Time"  sortable="true" /> 
            <%--<sjg:gridColumn name="devicelastUpdatedTime" index="b.devicelastUpdatedTime" title="Last Updated Time"  sortable="true"/>--%>
        </sjg:grid> 
    </div> 
</body>
</html>

