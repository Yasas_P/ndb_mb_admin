<%-- 
    Document   : customersearchview
    Created on : Apr 24, 2019, 3:03:58 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>view customer</title> 
        <script type="text/javascript">
            $(document).ready(function () {

                var statusCategory = $('#statusCategoryview').val();
                if (statusCategory != 'DFLT') {
                    $('#statusView').prop("disabled", true);
                    $('#waveoffView').prop("disabled", true);
                    $('#chargeAmountView').prop("disabled", true);
                    $('#updateCustomerStateButtonedit').prop("disabled", true);
                }else{
                    waveOffStatusChange();
                }
            });

            function viewPage(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findCustomerStatusCustomerSearch.action',
                    data: {userid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageview').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#statusView').val("");
                            $('#waveoffView').val("");
                            $('#chargeAmountView').val("");
                        } else {
                            $('#statusView').val(data.status);
                            $('#waveoffView').val(data.waveoff);
                            $('#chargeAmountView').val(data.chargeAmount);
                            waveOffStatusChange();
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function resetAllViewData() {
                var userid = $('#useridview').val();
                viewPage(userid);
            }
            
            function waveOffStatusChange(){
                var waveoff = $('#waveoffView').val();
                if(waveoff=="YES"){
                    $('#chargeAmountView').val("0");
                    $('#chargeAmountView').prop("readonly", true);
                }else{
                    $('#chargeAmountView').prop("readonly", false);
                }
            }
        </script>
    </head>
    <body>
        <s:div id="amessageview">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>


        <s:set var="vpinreset"><s:property value="vpinreset" default="true"/></s:set>

        <s:form id="viewcustomer" method="post" action="*CustomerSearch" theme="simple" cssClass="form" >
            <s:hidden name="csrfValue" id="csrfValue" value="%{#session.csrfValue}"/>
            <s:hidden name="userid" id="useridview"/>
            <s:hidden name="statusCategory" id="statusCategoryview"/>
            <s:hidden name="daonDeviceId" id="daonDeviceId"/>
            <s:hidden name="customerName" id="customerName"/>
            <s:hidden name="cifhidden" id="cifhidden"/>
            <s:hidden name="usernamehidden" id="usernamehidden"/>
            <s:hidden name="emailhidden" id="emailhidden"/>
            <s:hidden name="mobilehidden" id="mobilehidden"/>
            
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>

            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Unique ID</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="userid" id="useridDetview" cssClass="form-control" />
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer ID/CID</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="cif" id="cifview" cssClass="form-control" />
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >User Name</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="username" id="usernameview" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >User Segment</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="cusSegmentType" id="cusSegmentTypeview" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >On Board Channel</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="onBoardChannel" id="onBoardChannelview" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >On Board Type</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="onBoardType" id="onBoardTypeview" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer Category</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="customerCategory" id="customerCategoryDetview" cssClass="form-control" />
                                </div>
                            </div>  
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Login Status</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="loginStatus" id="loginStatusview" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Primary Account Type</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="defType" id="defTypeview" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Primary Acc/Card Number</label>
                                    <s:label  disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="defAccNo" id="defAccNoview" cssClass="form-control" />
                                </div>
                            </div> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer Name</label>
                                    <td><s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="customerName" id="customerNameview" cssClass="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >NIC/Passport No</label>
                                    <td><s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="nic" id="nicview" cssClass="form-control"/>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >DOB</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="dob" id="dobview" cssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Gender</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="gender" id="genderview" cssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Mobile</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="mobileNumber" id="mobileNumberview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Mobile(Secondary)</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="secondaryMobile" id="secondaryMobileview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Email</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="email" id="emailview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Email(Secondary)</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="secondaryEmail" id="secondaryEmailview"  cssClass="form-control"/>
                                </div>  
                            </div>
<!--                            <div class="col-sm-4" >
                                <div class="form-group">
                                    <label >Address</label>
                                    <%--<s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"   name="permanentAdd" id="permanentAddview" cssClass="form-control" />--%>
                                </div>
                            </div> -->
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Block Status</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="loginAttemptStatus" id="loginAttemptStatus"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Invalid Login Attempts</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="loginAttempts" id="loginAttemptsview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Last Password Update Time</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="lastPassUpdateTime" id="lastPassUpdateTimeview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Fee Cycle Start Date</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="finalFeeDudDay" id="finalFeeDudDayview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Account Officer</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="accountofficer" id="accountofficerview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Remark</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="remark" id="remarkview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Maker</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="maker" id="makerview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Checker</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="checker" id="checkerview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Created Time</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="createdtime" id="Createdtimeview"  cssClass="form-control"/>
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Last Updated Time</label>
                                    <s:label style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="lastupdatedtime" id="lastupdatedtimeview"  cssClass="form-control"/>
                                </div>  
                            </div>
                        </div>

<!--                        <div class="row row_popup"> 

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label >Address Information</label>
                                </div>
                                <div class="col-sm-4" style="background: #E4E4E4">
                                    <div class="form-group">
                                        <label >Permanent</label>
                                        <s:textarea rows="5" cols="4" disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="permAddress" id="permanentAddview" cssClass="form-control" />
                                    </div>
                                </div>    
                                <div class="col-sm-4" style="background: #E4E4E4">
                                    <div class="form-group">
                                        <label>Correspondence</label>
                                        <td><s:textarea rows="5" cols="4" disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="correspondenceAdd" id="correspondenceAddview" cssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-sm-4" style="background: #E4E4E4">
                                    <div class="form-group">
                                        <label>Office</label>
                                        <s:textarea rows="5" cols="4" disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="officeAdd" id="officeAddview" cssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>-->

                        <div class="row row_popup"> 
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer Last Login</label>
                                    <s:label disabled="true" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;"  name="lastlogindate" id="lastlogindateview" cssClass="form-control" />
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Customer Status</label>
                                    <s:select cssClass="form-control" name="status" id="statusView" headerValue="-- Select Status --" list="%{cusStatusList}"   headerKey="" listKey="statuscode" listValue="description" />
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Waive Off Status</label>
                                    <s:select cssClass="form-control" name="waveoff" id="waveoffView" headerValue="-- Select Waive Off --" list="%{waveOffStatusList}"   headerKey="" listKey="statuscode" listValue="description" onchange="waveOffStatusChange()" />
                                </div>  
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label >Charge Amount</label>
                                   <s:textfield value="%{chargeAmount}" cssClass="form-control" name="chargeAmount" id="chargeAmountView" maxLength="8" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                                </div>  
                            </div>
                        </div>
                        <div class="row row_1">
                        </div>
                        <div class="row row_1 form-inline">
                            <div class="col-sm-8    ">

                            </div>
                            <div class="col-sm-4 text-right">
<!--                                <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                                    <s:url action="PinResetCustomerSearch" var="pinreseturl"/>
                                    <%--<sj:submit--%>
                                        button="true"
                                        value="Pin Reset"
                                        href="%{pinreseturl}"
                                        targets="amessageview"
                                        id="pinresetedit"
                                        cssClass="ui-button-submit" 
                                        cssStyle="text"
                                        disabled="#vpinreset"
                                        />     
                                </div>-->
                                <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                                    <sj:submit 
                                        button="true" 
                                        value="Reset" 
                                        name="reset" 
                                        onClick="resetAllViewData()" 
                                        cssClass="ui-button-reset"

                                        />   
                                </div>
                                <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                                    <s:url action="UpdateCustomerStatusCustomerSearch" var="updateturl"/>
                                    <sj:submit
                                        button="true"
                                        value="Update"
                                        href="%{updateturl}"
                                        targets="amessageview"
                                        id="updateCustomerStateButtonedit"
                                        cssClass="ui-button-submit" 
                                        cssStyle="text"
                                        />     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </s:form>
    </body>
</html>
