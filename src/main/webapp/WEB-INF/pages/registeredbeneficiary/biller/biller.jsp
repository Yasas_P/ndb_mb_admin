<%-- 
    Document   : biller
    Created on : Oct 1, 2019, 3:51:00 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Payee Explorer</title>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;
                //  var isGenerate = false;

                if (recors > 0 && isGenerate == false) {
                    $('#view2').button("enable");
                } else {
                    $('#view2').button("disable");
                }
                
            });
            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('billerform');
                form.action = 'reportGenerateBiller.action';
                form.submit();
                $('#view2').button("disable");
            }
            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function searchPage() {
                $('#message').empty();

                var fromDate = $('#fromDate').val();
                var toDate = $('#toDate').val();
                var cif = $('#cif').val();
                var beneficiaryType = $('#beneficiaryType').val();
                var customerCategory = $('#customerCategory').val();
                var billerCategory = $('#billerCategory').val();
                var biller = $('#biller').val();
                var bankCode = $('#bankCode').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        fromDate: fromDate,
                        toDate: toDate,
                        cif: cif,
                        beneficiaryType: beneficiaryType,
                        customerCategory: customerCategory,
                        billerCategory: billerCategory,
                        biller: biller,
                        bankCode: bankCode,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetAllData() {

                $('#fromDate').val("");
                $('#toDate').val("");
                $('#cif').val("");
                $('#beneficiaryType').val("");
                $('#customerCategory').val("");
                $('#billerCategory').val("");
                $('#biller').val("");
                $('#bankCode').val("");

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        fromDate: '',
                        toDate: '',
                        cif: '',
                        beneficiaryType: '',
                        customerCategory: '',
                        billerCategory: '',
                        biller: '',
                        bankCode: '',
                        search: false
                    }
                });

                setdate();

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

            }

            function setdate() {
                $("#fromDate").datepicker("setDate", new Date());
                $("#toDate").datepicker("setDate", new Date());
            }

        </script>
    </head>

    <body onload="setdate()">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form id="billerform" method="post" action="Biller" theme="simple" cssClass="form" >

                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>

                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >From Date</label>
                                                <sj:datepicker cssClass="form-control" id="fromDate" name="fromDate" readonly="true" maxDate="d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >To Date</label>
                                                <sj:datepicker cssClass="form-control" id="toDate" name="toDate" readonly="true" maxDate="+1d" changeYear="true"
                                                               buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>CID </label>
                                                <s:textfield cssClass="form-control" name="cif" id="cif" maxLength="30" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Customer Category </label>
                                                <s:textfield cssClass="form-control" name="customerCategory" id="customerCategory" maxLength="100" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Biller Category</label>
                                                <s:select  cssClass="form-control" name="billerCategory" id="billerCategory" list="%{categoryList}"   headerKey=""  headerValue="--Select Category--" listKey="billercatcode" listValue="description" value="%{categoryCode}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Biller </label>
                                                <s:textfield cssClass="form-control" name="biller" id="biller" maxLength="50"  />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Bank Code </label>
                                                <s:textfield cssClass="form-control" name="bankCode" id="bankCode" maxLength="10"  />
                                            </div>
                                        </div>
                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-10 col-xs-10">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                disabled="#vsearch"
                                                onclick="searchPage()"
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"

                                                />
                                        </div> 
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"

                                                />
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View CSV" 
                                                name="view2" 
                                                id="view2" 
                                                onClick="todocsv()" 
                                                disabled="#vgenerate"/> 
                                        </div>
                                    </div>  
                                </div>
                            </div>
                                        <!-- ----place for dialog box--- -->
                        </div>
                        <div id="tablediv">
                            <s:url var="listurl" action="listBiller"/>
                            <s:set var="pcaption">${CURRENTPAGE}</s:set>

                            <sjg:grid
                                id="gridtable"
                                caption="%{pcaption}"
                                dataType="json"
                                href="%{listurl}"
                                pager="true"
                                gridModel="gridModel"
                                rowList="10,15,20"
                                rowNum="10"
                                autowidth="true"
                                rownumbers="true"
                                onCompleteTopics="completetopics"
                                rowTotal="false"
                                viewrecords="true"
                                onErrorTopics="anyerrors"
                                shrinkToFit="false"
                                >
                                <sjg:gridColumn name="userId" index="u.userid" title="Unique ID"  sortable="false" width="50"/>                             
                                <sjg:gridColumn name="cif" index="u.cif" title="CID"  sortable="false" width="50"/>
                                <sjg:gridColumn name="userName" index="u.userName" title="User Name"  sortable="false" width="70"/>
                                <sjg:gridColumn name="customerName" index="u.customerName" title="Customer Name"  sortable="false" width="70"/>
                                <sjg:gridColumn name="customerCategory" index="u.customerCategory" title="Customer Category"  sortable="false" width="70"/>
                                <sjg:gridColumn name="mobileNo" index="u.mobileNo" title="Mobile No"  sortable="false" width="70"/>
                                <sjg:gridColumn name="registationDate" index="u.registationDate" title=" Date"  sortable="false" width="70"/>
                                <sjg:gridColumn name="beneficiaryType" index="u.beneficiaryType" title="Beneficiary Type"  sortable="false" width="70" />
                                <sjg:gridColumn name="registationBeneficiary" index="u.statusDes" title="Registration Beneficiary"  sortable="false" width="70"/>
                                <sjg:gridColumn name="name" index="u.name" title="Name"  sortable="false" width="70"/>
                                <%--<sjg:gridColumn name="accountType" index="u.accountType" title="Account Type"  sortable="false" width="70"/>--%>
                                <%--<sjg:gridColumn name="bankName" index="u.bankName" title="Bank Name"  sortable="false" width="70"/>--%>
                                <sjg:gridColumn name="bankCode" index="u.bankCode" title="Bank Code"  sortable="false" width="70"/>
                                <sjg:gridColumn name="branchName" index="u.branchName" title="Branch Name"  sortable="false"  width="70"/>
                                <sjg:gridColumn name="billerCategory" index="u.billerCategory" title="Biller Category"  sortable="false" width="70"/>   
                                <sjg:gridColumn name="biller" index="u.biller" title="Biller"  sortable="false"/>        
                            </sjg:grid> 
                        </div>
                    </div>
                </div>
                <!-- end: PAGE CONTENT-->
            </div>
        </div>
        <!-- end: PAGE -->
        <!--</div>-->
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->



        <!-- end: BODY -->
    </body>
</html>