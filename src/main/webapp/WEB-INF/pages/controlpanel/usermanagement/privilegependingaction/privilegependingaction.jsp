<%-- 
    Document   : privilegependingaction
    Created on : Aug 17, 2017, 1:12:26 PM
    Author     : dilanka_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%--<%@page import="com.epic.sbank.util.varlist.SessionVarlist"%>--%>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <title>Pending Actions</title>
        <script type="text/javascript">
            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function searchpend() {
                $('#message').empty();

                var taskcodeSearch = $('#taskcodesearch').val();
                var usernameSearch = $('#usernamesearch').val();
                var pkeySearch = $('#pkeySearch').val();
                var pkey1Search = $('#pkey1Search').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        taskcodeSearch: taskcodeSearch,
                        usernameSearch: usernameSearch,
                        pkeySearch: pkeySearch,
                        pkey1Search: pkey1Search,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

//                getApprovalCount();
            }
            function viewformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:viewPendingActionInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.createduser + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function viewPendingActionInit(keyval, createduser) {
                $("#updatedialog").data('id', keyval).data('createduser', createduser).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("viewDualPrivilegePendingActions.action?id=" + $led.data('id') + "&createduser=" + $led.data('createduser'));
            });


            function resetAllData() {

                $('#taskcodesearch').val("");
                $('#usernamesearch').val("");
                $('#pkeySearch').val("");
                $('#pkey1Search').val("");

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        taskcodeSearch: '',
                        usernameSearch: '',
                        pkeySearch: '',
                        pkey1Search: '',
                        search: false
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

            }

            function resetFieldData() {

                $("#viewcommentprivilege").val("");
                $('#viewcommentprivilege').attr('disabled', 'true');

                $('#resetBtn').attr('disabled', 'true');
                $('#searchbutview').attr('disabled', 'true');

                $('#approveUPBtn').attr('disabled', 'true');
                $('#rejectUPBtn').attr('disabled', 'true');
                $('#cancelBtn').attr('disabled', 'true');

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

//                getApprovalCount();
            }

            function getApprovalCount() {

                $.ajax({
                    url: '${pageContext.request.contextPath}/getApprovalCountPrivilegePendingActions.action',
                    data: {},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#divDivmsg').empty();
                        var msg = data.message;
                        if (msg) {

                            $('#appcount').text("");
                            $('#appcountprivilege').text("");

                        } else {

                            if (data.approvalCount == "0") {
                                $('#appcount').attr('style', 'display: none;');
                            } else {
                                $('#appcount').attr('style', 'display: ;');
                                $('#appcount').text(data.approvalCount);
                            }

                            if (data.appcountprivilege == "0") {
                                $('#appcountprivilege').attr('style', 'display: none;');
                            } else {
                                $('#appcountprivilege').attr('style', 'display: ;');
                                $('#appcountprivilege').text(data.appcountprivilege);
                            }

                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }
        </script>
    </head>

    <body style="">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set var="vsearch" ><s:property value="vsearch" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form id="pendingactionsearch" method="post" action="PendingAction" theme="simple" cssClass="form" >

                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>User Role</label>
                                                <s:select cssClass="form-control" name="pkeySearch" id="pkeySearch" headerValue="-- Select User Role --" list="%{userRoleList}"   headerKey="" listKey="userrolecode" listValue="description" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Created User</label>
                                                <s:textfield cssClass="form-control" name="username" id="usernamesearch" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"/>
                                            </div>
                                        </div>
                                    </div>
                                </s:form>
                                <div class="row row_1"></div>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                disabled="#vsearch"
                                                onclick="searchpend()"  
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div> 
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                />
                                        </div>
                                    </div>
                                </div>

                                <!-- Start update dialog box -->
                                <sj:dialog                                     
                                    id="updatedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Privilege Pending Actions"
                                    onOpenTopics="openviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="550"
                                    dialogClass= "dialogclass"
                                    />
                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="listPrivilegePendingActions"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    >
                                    <sjg:gridColumn name="pkey" index="u.pkey" title="View Request" width="50" align="center" formatter="viewformatter" sortable="false"  />                                   
                                    <sjg:gridColumn name="pkeydes" index="u.pkeydes" title="User Role"  sortable="false" />
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Created User"  sortable="false"/>
                                    <sjg:gridColumn name="statusdes" index="u.statusdes" title="Status"  sortable="false"/>
                                </sjg:grid> 
                            </div>
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->



        <!-- end: BODY -->
    </body>
</html>

