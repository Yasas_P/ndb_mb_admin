<%-- 
    Document   : privilegependingactionview
    Created on : Aug 17, 2017, 1:12:56 PM
    Author     : dilanka_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View System User</title>
        <script>
            $(document).ready(function () {

                if ('<s:property value="vapprove"/>' == 'true') {

                } else {
                    $('#approveUPBtn').attr('disabled', 'true');
                }

                if ('<s:property value="vreject"/>' == 'true') {

                } else {
                    $('#rejectUPBtn').attr('disabled', 'true');
                }
            });

            var height_a;
            var height_u;
            $("#min_n4").hide();
            // maximize popup dialog 
            function maximize() {
                $(window).scrollTop(0);
                height_a = $("#updatedialog").height(); // get sizes
                //height_u = $("#updatedialog").height();// get sizes
                $("#updatedialog").height($(window).height() - 60);
                //$("#updatedialog").height($(window).height() - 60);
                $(".ui-dialog").width($(window).width() - 40);
                $(".ui-dialog").height($(window).height() - 10);
                $("#gridtablepend").jqGrid('setGridWidth', $(".ui-dialog").width() - 44);
//                $("#resize").css('columns', '100px 2'); // content to 2 columns
                $(".ui-dialog").center();
                $("#max_n4").hide();
                $("#min_n4").show();
            }
            // reset to previous popup dialog
            function resetwindow() {
                $(window).scrollTop(0);
                $("#updatedialog").height(height_a); // set sizes
                //$("#updatedialog").height(height_u);// set sizes
                $(".ui-dialog").width("900");
                $(".ui-dialog").height("550");
                $("#gridtablepend").jqGrid('setGridWidth', $(".ui-dialog").width() - 44);
//                $("#resize").css('columns', '100px 1');
                $(".ui-dialog").center();
                $("#max_n4").show();
                $("#min_n4").hide();
            }
            //center popup dialog
            jQuery.fn.center = function () {
                this.css("position", "fixed");
                this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) +
                        $(window).scrollTop()) + "px");
                this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                        $(window).scrollLeft()) + "px");
                return this;
            };

            function searchpendview() {
                $('#message').empty();

                var sectionSearchView = $('#sectionSearchView').val();
                var pageSearchView = $('#pageSearchView').val();
                var taskSearchView = $('#taskSearchView').val();

                $("#gridtablepend").jqGrid('setGridParam', {
                    postData: {
                        sectionSearchView: sectionSearchView,
                        pageSearchView: pageSearchView,
                        taskSearchView: taskSearchView,
                        search: true
                    }
                });

                $("#gridtablepend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablepend").trigger("reloadGrid");

            }

            function resetAllDataView() {
                $("#viewcommentprivilege").val("");
                $('#amessageedit').text("");

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetAllDataSearchView() {

                $('#sectionSearchView').val("");
                $('#pageSearchView').val("");
                $('#taskSearchView').val("");

                $("#gridtablepend").jqGrid('setGridParam', {
                    postData: {
                        sectionSearchView: '',
                        pageSearchView: '',
                        taskSearchView: '',
                        search: false
                    }
                });

                $("#gridtablepend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablepend").trigger("reloadGrid");
            }

            function a() {
                alert("22 :" + $('#idview').val() + $('#createduserview').val());
                $.ajax({
                    url: '${pageContext.request.contextPath}/ApprovePrivilegePendingActions.action',
                    data: {id: $('#idview').val(), createduser: $('#createduserview').val()},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {

                            alert("123 : " + msg);
                            $('#amessageedit').val(msg);


                        } else {


                        }

                        $("#gridtable").jqGrid('setGridParam', {page: 1});
                        jQuery("#gridtable").trigger("reloadGrid");

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });


            }

        </script>

    </head>
    <body >
        <div class="col-sm-12">
            <div style="text-align: right">
                <sj:submit 
                    id="max_n4"
                    button="true" 
                    value="Maximize" 
                    onClick="maximize()"
                    onClickTopics="Maximize"
                    cssStyle="height: 16px;padding: 1px;width: 55px;font-size: 11px;background: orange;color: white;border-color: gray;"
                    />                          
                <sj:submit 
                    id="min_n4"
                    button="true" 
                    value="Minimize" 
                    onClick="resetwindow()"
                    onClickTopics="Minimize"
                    cssStyle="height: 16px;padding: 1px;width: 55px;font-size: 11px;background: orange;color: white;border-color: gray;"
                    />                     
            </div>
        </div>

        <s:div id="amessageedit">
            <s:actionerror theme="jquery" id="abc"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="pendingactionview" method="post" action="PrivilegePendingActions"  theme="simple" cssClass="form">   
            <div class="row row_popup"> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Section</label>
                        <s:textfield name="sectionSearchView" id="sectionSearchView" cssClass="form-control" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Page</label>
                        <s:textfield name="pageSearchView" id="pageSearchView"  cssClass="form-control" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Task</label>
                        <s:textfield name="taskSearchView" id="taskSearchView"  cssClass="form-control" />
                    </div>
                </div>
            </div>
        </s:form>    
        <div class="row row_popup form-inline" style="margin-bottom: 20px;">
            <div class="col-sm-4">
                <div class="form-group">
                    <sj:submit 
                        button="true"
                        value="Search" 
                        disabled="#vsearch"
                        onclick="searchpendview()"  
                        id="searchbutview"
                        cssClass="btn btn-sm active" 
                        cssStyle="background-color: #ada9a9" 
                        />
                </div> 
                <div class="form-group">
                    <sj:submit 
                        button="true" 
                        value="Reset" 
                        name="reset"
                        id ="resetBtn"
                        onClick="resetAllDataSearchView()" 
                        cssClass="btn btn-default btn-sm"
                        />
                </div>
            </div>
        </div>
        <div id="tablediv" style="margin-bottom: 10px;" >
            <s:url var="listurlpend" action="listDualPrivilegePendingActions?id=%{id}&createduser=%{createduser}"/>
            <sjg:grid
                id="gridtablepend"
                caption="Pending Tasks"
                dataType="json"
                href="%{listurlpend}"
                pager="true"
                gridModel="gridModelPending"
                rowList="10,15,20"
                rowNum="10"
                autowidth="true"
                rownumbers="true"
                onCompleteTopics="completetopics"
                rowTotal="false"
                viewrecords="true" 
                onErrorTopics="anyerrors"
                shrinkToFit="true"
                >
                <sjg:gridColumn name="id" index="id" title="ID" hidden="true" frozen="true"/>
                <sjg:gridColumn name="section" index="section" title="Section"  sortable="false"/>
                <sjg:gridColumn name="page" index="page" title="Page" sortable="false" />
                <sjg:gridColumn name="task" index="task" title="Task" sortable="false"/> 
                <sjg:gridColumn name="createdtime" index="createdtime" title="Created Time"  sortable="false"/> 
            </sjg:grid> 
        </div>
        <s:form id="privilegepend" method="post" action="PrivilegePendingActions"  theme="simple" cssClass="form"> 
            <div class="row row_popup">
                <div class="col-sm-12" >
                    <div class="form-group">
                        <span id="span_cmnt" style="color: red">*</span><label style="font-weight: bold">Comments</label>
                        <s:textarea  name="comment" id="viewcommentprivilege" cssClass="form-control" maxLength="1500"/>                 
                    </div> 
                </div>
            </div>
            <div class="row row_popup">


                <s:hidden id="idview" name="id"/>
                <s:hidden id="createduserview" name="createduser"/>

                <div class="col-sm-9 form-inline">
                    <div class="form-group" style="margin-right: 10px;"> 
                        <s:url action="ApprovePrivilegePendingActions" var="approveurl"/>
                        <sj:submit
                            button="true"
                            value="Approve"
                            id="approveUPBtn"
                            href="%{approveurl}"
                            name="approveUPBtn"
                            targets="amessageedit"
                            disabled="#vapprove"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9" 
                            />  
                    </div>
                    <div class="form-group" style="margin-left: 10px;margin-right: 10px;">
                        <s:url action="RejectPrivilegePendingActions" var="rejecturl"/>
                        <sj:submit
                            button="true"
                            value="Reject"
                            id="rejectUPBtn"
                            name="rejectUPBtn"
                            href="%{rejecturl}"
                            targets="amessageedit"
                            disabled="#vreject"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9" 
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            id ="cancelBtn"
                            disabled="false"
                            onClick="resetAllDataView()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group" style="margin-top: 10px;">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
            </div>
        </s:form> 
    </body>
</html>




