<%-- 
    Document   : sendmailinsert
    Created on : Oct 17, 2019, 3:47:58 PM
    Author     : sivaganesan_t
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Mobile Bank Locator</title> 
        <style>
            .ui-widget {
                border-color: white;
            }
        </style>
        <script type="text/javascript">
            $.subscribe('resetAddButton', function (event, data) {
                $('#amessage').empty();
                $('#serviceId').val("");
//                $('#subject').val("");
                $('#toUser').val("");
                $('#messageEmail').val("");
                toleftall();
                resetAllDataCSV();
                $('#viewSegment').hide();
                $('#viewUser').hide();
            });

            function clickAdd() {

                $('#currentSegmentBox option').prop('selected', true);
                $('#newSegmentBox option').prop('selected', true);
                $('#newCidBox option').prop('selected', true);
                $('#sendmailadd').attr('action', 'sendSendMail');
                $('#sendmailadd').submit();

            }
            //----------- Creadit Multiselect------------------------
            function toleft() {
                $("#currentSegmentBox option:selected").each(function () {

                    $("#newSegmentBox").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function toright() {
                $("#newSegmentBox option:selected").each(function () {

                    $("#currentSegmentBox").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function toleftall() {
                $("#currentSegmentBox option").each(function () {

                    $("#newSegmentBox").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function torightall() {
                $("#newSegmentBox option").each(function () {

                    $("#currentSegmentBox").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });

            }

            function setRecipient(value) {
                var fieldType = value.val();
                if (fieldType == '3') {
                    resetAllDataCSV();
                    $('#viewSegment').show();
                    $('#viewUser').hide();
                } else if (fieldType == '2') {
                    toleftall()
                    $('#viewUser').show();
                    $('#viewSegment').hide();
                } else {
                    resetAllDataCSV();
                    toleftall()
                    $('#viewSegment').hide();
                    $('#viewUser').hide();
                }
            }
            ;
            function resetAllDataCSV() {

                $('#conXL').val("");
                $('#amessage').text("");
                $('#newCidBox').empty();
            }

            function todo() {
                form = document.getElementById('sendmailadd');
                form.action = 'templateSendMail';
                form.submit();
            }

            $(document).ready(function () {
                $('#conXL').on('change', function () {
                    $('#newCidBox').empty();
                    $('#amessage').text("");
                    var upload = $('#conXL').val();
                    var oMyForm = new FormData();
                    oMyForm.append("file", conXL.files[0]);
                    if (upload == "") {
                        $("#amessage").html("<div class='ui-widget actionError'>"
                                + "<div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'>"
                                + "<p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>"
                                + "No file has been uploaded"
                                + "</span></p></div></div>");
                    } else {

                        $.ajax({
                            url: '${pageContext.request.contextPath}/uploadCidSendMail.action',
                            data: oMyForm,
                            dataType: 'json',
                            type: 'POST',
                            processData: false,
                            contentType: false,
                            success: function (data) {

                                //  document.getElementById("popupdivmsg").innerHTML = "<span style='color:red'>"+data.errorMessage+"</span>";
                                if (data.errormessage !== null) {
                                    $("#amessage").html("<div class='ui-widget actionError'>"
                                            + "<div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'>"
                                            + "<p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>"
                                            + data.errormessage
                                            + "</span></p></div></div>");

                                } else {
                                    if (data.userList != null) {
                                        if (data.userList == "") {
                                            $("#amessage").html("<div class='ui-widget actionError'>"
                                                    + "<div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'>"
                                                    + "<p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>"
                                                    + "Uploaded file is empty"
                                                    + "</span></p></div></div>");
                                        } else {
                                            $.each(data.userList, function (index, item) {
                                                $('#newCidBox').append(
                                                        $('<option></option>').val(item.key).html(
                                                        item.value));
                                            });
                                        }
                                    } else {
                                        $("#amessage").html("<div class='ui-widget actionError'>"
                                                + "<div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'>"
                                                + "<p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>"
                                                + "Uploaded file is empty"
                                                + "</span></p></div></div>");
                                    }
                                }
                                $('#conXL').val("");
                            },
                            error: function (data) {
                                //                                        alert("error");
                                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                            }
                        });
                    }
                });
            });
        </script>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="sendmailadd" method="post" action="sendSendMail" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subject</label>
                        <s:select value="%{serviceId}" cssClass="form-control" id="serviceId" list="%{serviceCategoryList}"  name="serviceId" headerKey=""  headerValue="--Select Subject--" listKey="id" listValue="service"/>
                    </div>
                </div>
                <!--<div class="col-sm-3">-->
                    <!--<div class="form-group">-->
                        <!--<span style="color: red">*</span><label>Subject</label>-->
                        <%--<s:textfield value="%{subject}" cssClass="form-control" name="subject" id="subject" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g, ''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g, ''))" />--%>
                    <!--</div>-->
                <!--</div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Recipient</label>
                        <s:select value="%{toUser}" cssClass="form-control" id="toUser" list="%{recipientList}"  name="toUser" headerKey=""  headerValue="--Select Recipient--" listKey="key" listValue="value"  onchange="setRecipient($(this))"/>
                    </div>
                </div>
            </div>
            <div  id="viewSegment" style="display:none">        
                <div class="row row_popup" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span style="color: red">*</span><label >Segment</label>
                        </div>
                    </div>
                </div>        
                <div class="row row_popup">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <s:select cssClass="form-control" multiple="true"
                                      name="newSegmentBox" id="newSegmentBox" list="segmentList"									 
                                      ondblclick="toright()" style="height:160px;"  listKey="key" listValue="value"/>
                        </div>                
                    </div>
                    <div class="col-sm-2 text-center">
                        <div class="form-group">
                            <div class="row" style="height: 20px;"></div>
                            <div class="row">
                                <sj:a
                                    id="right" 
                                    onClick="toright()" 
                                    button="true"
                                    cssClass="ui-button-move"
                                    style="font-size:10px;width:60px;margin:4px;"> > </sj:a>
                                </div>
                                <div class="row">
                                <sj:a
                                    id="rightall" 
                                    onClick="torightall()" 
                                    button="true"
                                    cssClass="ui-button-move"
                                    style="font-size: 10px;width:60px;margin:4px;"> >> </sj:a>
                                </div>
                                <div class="row">
                                <sj:a
                                    id="left" 
                                    onClick="toleft()" 
                                    button="true"
                                    cssClass="ui-button-move"
                                    style="font-size:10px;width:60px;margin:4px;"> < </sj:a>
                                </div>
                                <div class="row">
                                <sj:a
                                    id="leftall" 
                                    onClick="toleftall()" 
                                    button="true"
                                    cssClass="ui-button-move"
                                    style="font-size:10px;width:60px;margin:4px;"> << </sj:a>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-5">
                            <div class="form-group"> 
                            <s:select cssClass="form-control" multiple="true" 
                                      name="currentSegmentBox" id="currentSegmentBox" list="currentSegmentList"									 
                                      listKey="key" listValue="value" ondblclick="toleft()" style="height:160px;" />

                        </div>
                    </div>
                </div>
            </div>
            <div id="viewUser" style="display:none">                
                <div class="row row_popup" >
                    <div class="row form-inline">
                        <div class="col-sm-12">
                            <div class="horizontal_line_popup"></div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" name="hiddenId" id="hiddenId" value="" />
                                <span style="color: red">*</span><label >Upload CSV files</label>
                                <input type="file" id="conXL" name="conXL" class="ui-button-submit" />
                            </div>
                        </div>
                    </div>
                    <div class="row row_popup form-inline">
                        <div class="col-sm-12 text-right">
                            <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                                <sj:submit 
                                    button="true" 
                                    value="Template" 
                                    onClick="todo()"
                                    cssClass="ui-button-submit"
                                    />                          
                            </div>
                            <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                                <sj:submit 
                                    button="true" 
                                    value="Reset" 
                                    onClick="resetAllDataCSV()"
                                    cssClass="ui-button-reset"
                                    />                          
                            </div> 
                        </div>
                    </div>
                    <div class="row row_popup" >
                        <div class="col-sm-6">
                            <div class="form-group">
                                <span style="color: red">*</span><label >CID</label>
                            </div>
                        </div>
                    </div>        
                    <div class="row row_popup">
                        <div class="col-sm-5">
                            <div class="form-group">
                                <s:select cssClass="form-control" multiple="true"
                                          name="newCidBox" id="newCidBox" list="cidList"									 
                                          style="height:160px;"  listKey="key" listValue="value"/>
                            </div>                
                        </div>    
                    </div>
                </div>
            </div>               
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Message</label>
                        <textarea name="messageEmail" id="messageEmail"  class="form-control" rows="8"  maxlength="2000"></textarea>
                    </div>
                </div>
            </div>                    
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <%--<s:url action="sendSendMail" var="sendurl"/>href="%{sendurl}"--%>
                        <sj:submit
                            button="true"
                            value="Send"
                            onclick="clickAdd()"
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>

                </div>
            </div>
        </s:form>
    </body>
</html>

