<%-- 
    Document   : sendmaildetailview
    Created on : Oct 21, 2019, 11:45:14 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <link rel="stylesheet" href="resouces/css/emailcss.css">
        <title>Insert Promotion</title>       
        <script type="text/javascript">

            function backToMain() {
                window.location = "${pageContext.request.contextPath}/viewSendMail.action?";
            }

            function todo() {


                form = document.getElementById('auditform2');
                form.action = 'individualReportSendMail';
//                form.submit();
            }
            function setSelectView(id) {
//                $("#fileId").val(id);
            }
            function getObjectFormView(obj, id) {
                $('.fileid', $(obj).closest("form")).val(id);
                $(obj).closest("form").submit();
            }

            $(document).ready(function () {
                var fieldType = $('#toUser_view').val();
                if (fieldType == '3') {
                    $('#viewSegmentView').show();
                } else if (fieldType == '2') {
                    $('#viewUserView').show();
                }
            });
        </script>
    </head>            
    <body>
        <s:div id="divmsgview">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:set id="vview" var="vview"><s:property value="vview" default="false"/></s:set>    
        <s:set id="vgenerateview" var="vgenerateview"><s:property value="vgenerateview" default=""/></s:set>    
        <s:form id="auditform2" method="post" action="*EmailMgt" cssClass="form" theme="simple">

            <div class="row row_popup"> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Message ID</label>
                        <s:label style="margin-bottom: 0px;" name="id"  value="%{sendMsg.id}" cssClass="form-control"/>
                    </div>  
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Subject</label>
                        <s:label style="margin-bottom: 0px;" name="serviceId"  value="%{sendMsg.serviceDes}" cssClass="form-control"/>
                    </div>  
                </div>
                <!--                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label >Subject</label>
                <%--<s:label style="margin-bottom: 0px;" name="subject"  value="%{sendMsg.subject}" cssClass="form-control"/>--%>
            </div>
        </div>  -->
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Recipients</label>
                        <s:label style="margin-bottom: 0px;" name="status"  value="%{sendMsg.toUser}" cssClass="form-control"/>
                        <s:hidden name="toUser" id="toUser_view" value="%{sendMsg.toUserId}" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">  

                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Processing Status</label>
                        <s:label style="margin-bottom: 0px;" name="adminStatus"  value="%{sendMsg.processingStatus}" cssClass="form-control"/>
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Maker</label>
                        <s:label style="margin-bottom: 0px;" name="maker"  value="%{sendMsg.maker}" cssClass="form-control"/>
                    </div> 
                </div>
<!--                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Checker</label>
                        <%--<s:label style="margin-bottom: 0px;" name="checker"  value="%{sendMsg.checker}" cssClass="form-control"/>--%>
                    </div> 
                </div>-->
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Created Date And Time</label>
                        <s:label style="margin-bottom: 0px;" name="createdDate"  value="%{sendMsg.createdDate}" cssClass="form-control"/>
                    </div> 
                </div>
            </div>
            <div class="row row_popup">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Last Updated Date And Time</label>
                        <s:label style="margin-bottom: 0px;" name="lastUpdatedDate"  value="%{sendMsg.lastUpdatedDate}" cssClass="form-control"/>
                    </div> 
                </div>
            </div>
            <div id="viewSegmentView" style="display:none">       
                <div class="row row_popup" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span style="color: red">*</span><label >Segment</label>
                        </div>
                    </div>
                </div>        
                <div class="row row_popup">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <s:select cssClass="form-control" multiple="true"
                                      name="newSegmentBox" id="newSegmentBoxPend" list="segmentList"									 
                                      style="height:160px;"  listKey="key" listValue="value"/>
                        </div>                
                    </div>
                </div>         
            </div>  
            <div id="viewUserView" style="display:none">    
                <div class="row row_popup" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span style="color: red">*</span><label >CID</label>
                        </div>
                    </div>
                </div>        
                <div class="row row_popup">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <s:select cssClass="form-control" multiple="true"
                                      name="newCidBox" id="newCidBoxPend" list="cidList"									 
                                      style="height:160px;"  listKey="key" listValue="value"/>
                        </div>                
                    </div>    
                </div>
            </div>          
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Message</label>
                        <s:textarea readonly="true" style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="messageEmail"  value="%{sendMsg.message}" cssClass="form-control"/>
                    </div>
                </div>
            </div>

            <div class="row row_popup text-right">
                <div class="col-sm-9"></div>
                <div class="col-sm-3">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">
                        <sj:submit
                            button="true"
                            value="View PDF"
                            id="viewindi" 
                            name="viewindi" 
                            onclick="todo()"  
                            disabled="#vgenerateview"

                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"     

                            />                        
                    </div>
                </div>
            </div>        
        </s:form>

        <!-- end: BODY -->
    </body>
</html>
