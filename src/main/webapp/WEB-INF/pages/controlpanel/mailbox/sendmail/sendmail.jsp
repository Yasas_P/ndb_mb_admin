<%-- 
    Document   : sendmail
    Created on : Oct 17, 2019, 1:12:03 PM
    Author     : sivaganesan_t
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <style>
            .activeStatus{
                background: #4CAF50;
                color:white;
            }
            .deactiveStatus{
                background: #ff2727;
                color:white;
            }
        </style>
        <script>
            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });
            function viewdownloadeformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }
            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmEmailmgt(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectEmailmgt(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            function viewPendInit(keyval) {
                $("#viewpenddialog").data('id', keyval).dialog('open');
            }
            
            $.subscribe('openviewpendtasktopage', function (event, data) {
                var $led = $("#viewpenddialog");
                $led.html("Loading..");
                $led.load("viewPendSendMail.action?id=" + $led.data('id'));
            });
            function confirmEmailmgt(keyval, popvar) {
                $('#divmsg').empty();
                
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');
                
                return false;
            }

            function rejectEmailmgt(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');
                
                return false;
            }

            function confirmCC(keyval,remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmSendMail.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectCC(keyval,remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectSendMail.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function detailviewformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='View Customer' onClick='javascript:detailviewInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }

            function detailviewInit(keyval) {
                $("#viewdialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openviewdetailtasktopage', function (event, data) {
                var $led = $("#viewdialog");
                $led.html("Loading..");
                $led.load("detailviewSendMail.action?id=" + $led.data('id'));
            });
            
            function viewSearchParam(val){
                $('#cif_s').val("");
                $('#segment_s').val("");
                 if (val == '3') {
                    $('#viewSegmentSearch').show();
                    $('#viewCIDSearch').hide();
                } else if (val == '2') {
                    $('#viewCIDSearch').show();
                    $('#viewSegmentSearch').hide();
                }else{
                    $('#viewCIDSearch').hide();
                    $('#viewSegmentSearch').hide();
                }
            }
            
            function searchEmail() {
                var fdate_s = $('#fdate_s').val();
                var todate_s = $('#todate_s').val();
                var toUser_s = $('#toUser_s').val();
                var cif_s = $('#cif_s').val();
                var segment_s = $('#segment_s').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        fdate_s: fdate_s,
                        todate_s: todate_s,
                        toUser_s: toUser_s,
                        cif_s: cif_s,
                        segment_s: segment_s,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetAllData() {
                $('#fdate_s').val("");
                $('#todate_s').val("");
                $('#toUser_s').val("");
                $('#cif_s').val("");
                $('#segment_s').val("");

                $('#viewCIDSearch').hide();
                $('#viewSegmentSearch').hide();
                
                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        fdate_s: '',
                        fdate_s: '',
                        toUser_s: '',
                        cif_s: '',
                        segment_s: '',
                        search: false
                    }
                });
                
                setdate();

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

            }

            function resetFieldData() {
                if (typeof toleftall === "function") { 
                    $('#serviceId').val("");
                    $('#subject').val("");
                    $('#toUser').val("");
                    $('#messageEmail').val("");
                    toleftall();
//                    resetAllDataCSV();
                    $('#conXL').val("");
                    $('#newCidBox').empty();
                    $('#viewSegment').hide();
                    $('#viewUser').hide();
                }
                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");
                
                
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
            
            function setdate() {
                $("#fdate_s").datepicker("setDate", new Date());
                $("#todate_s").datepicker("setDate", new Date());
            }
            
            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;

                if (recors > 0 && isGenerate == false) {
                    $('#view2').button("enable");
                } else {
                    $('#view2').button("disable");
                }
            });
            
            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('sendmailsearch');
                form.action = 'reportGenerateSendMail.action';
                form.submit();

                $('#view2').button("disable");
            }

        </script>
    </head>

    <body onload="setdate()">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>
                            <s:set var="vsend"><s:property value="vsend" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>
                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                                <div id="formstyle">
                                <s:form id="sendmailsearch" method="post" action="SendMail" theme="simple" cssClass="form" >
                                    <s:hidden name="csrfValue" id="csrfValue" value="%{#session.csrfValue}"/>
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >From Date</label>
                                                <sj:datepicker cssClass="form-control" id="fdate_s" name="fdate_s" readonly="true" maxDate="d" changeYear="true"
                                                    buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >To Date</label>
                                                <sj:datepicker cssClass="form-control" id="todate_s" name="todate_s" readonly="true" maxDate="+1d" changeYear="true"
                                                    buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200"/>
                                            </div>
                                        </div>                                                                             
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Recipients</label>
                                                <s:select  name="toUser_s" id="toUser_s" cssClass="form-control"  list="%{recipientList}"  headerValue="-- Select Recipients--" headerKey=""  listKey="key" listValue="value" onchange="viewSearchParam($(this).val())"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3" id="viewSegmentSearch" style="display:none">
                                            <div class="form-group">
                                                <label >Segment</label>
                                                <s:select  name="segment_s" id="segment_s" cssClass="form-control"  list="%{segmentTypeList}"  headerValue="-- Select Segment--" headerKey=""  listKey="segmentcode" listValue="description"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3" id="viewCIDSearch" style="display:none">
                                            <div class="form-group">
                                                <label >CID</label>
                                                <s:textfield  cssClass="form-control" name="cif_s" id="cif_s" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">

                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                disabled="#vsearch"
                                                onclick="searchEmail()"
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>                                         
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                />
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View CSV" 
                                                name="view2" 
                                                id="view2" 
                                                onClick="todocsv()" 
                                                disabled="#vgenerate"/> 
                                        </div>
                                    </div>
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-3 text-right">
                                        <div class="form-group">
                                        <s:url var="sendurl" action="viewPopupSendMail"/>
                                            <sj:submit 
                                                openDialog="remotedialog"
                                                button="true"
                                                href="%{sendurl}"
                                                disabled="#vsend"
                                                value="Send Message"
                                                id="addButton_new"   
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>
                                    </div>
                                </div>
                                 
                                <sj:dialog                                     
                                    id="remotedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Send New Message"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />     
                                <!-- Start view dialog box -->
                                <sj:dialog                                     
                                    id="viewdialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Send Message"
                                    onOpenTopics="openviewdetailtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    cssStyle="overflow:auto"
                                    />
                                <!-- Start view pend dialog box -->
                                <sj:dialog                                     
                                    id="viewpenddialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Pending Message"
                                    onOpenTopics="openviewpendtasktopage"
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteAtm($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete ATM"                            
                                    />
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmCC($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectCC($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />

                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="listSendMail"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="false"
                                    onGridCompleteTopics="gridComplete"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    sortable="false"
                                    shrinkToFit="false"
                                    >
                                    <sjg:gridColumn name="id" index="u.id" title="View" width="60" sortable="false" formatter="detailviewformatter" frozen="false" cssClass="nopopup" />
                                    <sjg:gridColumn name="id" index="u.id" title="Message ID"  sortable="true"/>
                                    <sjg:gridColumn name="serviceDes" index="u.inboxServiceCategory.id" title="Subject"  sortable="true"/>
                                    <%--<sjg:gridColumn name="subject" index="u.subject" title="Subject"  sortable="true" />--%>
                                    <sjg:gridColumn name="toUser" index="u.toUser" title="Recipients"  sortable="true" />
                                    <sjg:gridColumn name="processingStatus" index="u.processingStatus" title="Processing Status"  sortable="false" />
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="false" />
                                    <%--<sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="false" />--%>  
                                    <sjg:gridColumn name="createdDate" index="u.createdDate" title="Created Date And Time"  sortable="true" hidden="false"/>
                                    <sjg:gridColumn name="lastUpdatedDate" index="u.lastUpdatedDate" title="Last Updated Date And Time"  sortable="true" hidden="false"/>
                                </sjg:grid> 
                            </div> 
                            <!-- start dual auth table -->
<!--                            <div id="tablediv">
                                <%--<s:url var="listurlap" action="approveListSendMail"/>--%>

                                <%--<sjg:grid--%>
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Send Messages"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    rowTotal="false"
                                    viewrecords="true"  

                                    >
                                    <%--<sjg:gridColumn name="id" index="id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>--%>                        
                                    <%--<sjg:gridColumn name="id" index="id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>--%>
                                    <%--<sjg:gridColumn name="id" index="u.id" title="View" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/>--%> 
                                    <%--<sjg:gridColumn name="id" index="u.id" title="Message ID"  sortable="true" hidden="true"/>--%>
                                    <%--<sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>--%>
                                    <%--<sjg:gridColumn name="serviceDes" index="u.inboxServiceCategory.id" title="Subject"  sortable="true"/>--%>
                                    <%--<sjg:gridColumn name="subject" index="u.subject" title="Subject"  sortable="true" />--%> 
                                    <%--<sjg:gridColumn name="toUser" index="u.toUser" title="Recipients"  sortable="true" />--%>                                                   
                                    <%--<sjg:gridColumn name="maker" index="u.maker" title="Inputter"  sortable="true"/>--%>  
                                    <%--<sjg:gridColumn name="createdDate" index="u.createdDate" title="Created Time"  sortable="true" hidden="false"/>--%>
                                <%--</sjg:grid>--%>  
                            </div>   -->
                        </div>
                        <!-- end: PAGE CONTENT-->
                    </div>
                </div>
                <!-- end: PAGE -->
            </div>
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!-- end: BODY -->
    </body>
</html>

