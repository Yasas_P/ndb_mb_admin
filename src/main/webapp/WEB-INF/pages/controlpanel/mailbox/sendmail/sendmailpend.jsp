<%-- 
    Document   : sendmailpend
    Created on : Oct 21, 2019, 10:20:06 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Pending Send Mail</title> 
        <script>
            $( document ).ready(function() {
                var fieldType = $('#toUser_pend').val();
                if (fieldType == '3') {
                    $('#viewSegmentPend').show();
                } else if (fieldType == '2') {
                    $('#viewUserPend').show();
                }
            });
        </script>
    </head>
    <body>
        <s:div id="pendmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="sendmailpend" method="post" action="SendMail" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subject </label>
                        <s:label style="margin-bottom: 0px;" name="serviceDes"  value="%{sendMsgTemp.serviceDes}" cssClass="form-control"/>
                    </div>
                </div>
                <!--<div class="col-sm-4">-->
                    <!--<div class="form-group">-->
                        <!--<span style="color: red">*</span><label>Subject</label>-->
                        <%--<s:label style="margin-bottom: 0px;" name="subject"  value="%{sendMsgTemp.subject}" cssClass="form-control"/>--%>
                    <!--</div>-->
                <!--</div>-->
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Recipients</label>
                        <s:label style="margin-bottom: 0px;" name="toUser"  value="%{sendMsgTemp.toUser}" cssClass="form-control"/>
                        <s:hidden name="toUser" id="toUser_pend" value="%{sendMsgTemp.toUserId}" />
                    </div>
                </div>
            </div>  
            <div id="viewSegmentPend" style="display:none">       
                <div class="row row_popup" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span style="color: red">*</span><label >Segment</label>
                        </div>
                    </div>
                </div>        
                <div class="row row_popup">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <s:select cssClass="form-control" multiple="true"
                                      name="newSegmentBox" id="newSegmentBoxPend" list="segmentList"									 
                                      style="height:160px;"  listKey="key" listValue="value"/>
                        </div>                
                    </div>
                </div>         
            </div>  
            <div id="viewUserPend" style="display:none">    
                <div class="row row_popup" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <span style="color: red">*</span><label >CID</label>
                        </div>
                    </div>
                </div>        
                <div class="row row_popup">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <s:select cssClass="form-control" multiple="true"
                                      name="newCidBox" id="newCidBoxPend" list="cidList"									 
                                      style="height:160px;"  listKey="key" listValue="value"/>
                        </div>                
                    </div>    
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Message</label>
                        <s:textarea value="%{sendMsgTemp.message}" cssClass="form-control" style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="message" id="message_pend" maxLength="2000" readonly="true" />
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
