<%-- 
    Document   : transactiontypeedit
    Created on : Aug 1, 2016, 10:34:05 AM
    Author     : samith_k
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Transaction Type</title>

        <script>
            function editMailSubject(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/FindMailSubject.action',
                    data: {subjectid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#divmsg').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#subjectidedit').val("");
                            $('#subjectidedit').attr('readOnly', true);
                            $("#subjectidedit").css("color", "black");
                            $('#serviceedit').val("");
                            $('#statusedit').val("");

                            $('#amessageedit').text("");
                            $('#updateButtonedit').button("disable");
                        } else {
                            $('#oldvalue').val(data.oldvalue);
                            $('#subjectidedit').val(data.subjectid);
                            $('#subjectidedit').attr('readOnly', true);
                            $("#subjectidedit").css("color", "#858585");
                            $('#serviceedit').val(data.service);
                            $('#statusedit').prop('disabled', false);
                            $('#statusedit').val(data.status);                         
                            $('#amessageedit').text("");
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var id = $('#subjectidedit').val();
                editMailSubject(id);
            }


        </script>      
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="idedit" method="post" action="MailSubject" theme="simple" cssClass="form" >
             <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
            
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>ID</label>
                        <s:textfield name="subjectid" id="subjectidedit" maxLength="4" readonly="true" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))" cssClass="form-control"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield  name="service" id="serviceedit" maxLength="30" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" cssClass="form-control"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select  id="statusedit" list="%{statusList}"  name="status" headerValue="--Select Status--" headerKey="" listKey="statuscode" listValue="description" cssClass="form-control"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">               
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 form-inline">
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <s:url action="UpdateMailSubject" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButtonedit"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />     
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
