<%-- 
    Document   : email subject
    Created on : Feb 3, 2016, 4:10:07 PM
    Author     : chathuri_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head>

        <%@include file="/stylelinks.jspf" %>

        <script type="text/javascript">

            function editformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:editMailSubjectInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function deleteformatter(cellvalue, options, rowObject) {
                return "<a href='#/' title='Delete' onClick='javascript:deleteMailSubjectInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            
            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmMailSubject(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectMailSubject(&#34;" + cellvalue +  "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            
            function confirmMailSubject(keyval, popvar) {
                $('#divmsg').empty();
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function rejectMailSubject(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function confirmTT(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmMailSubject.action',
                    data: {subjectid: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectTT(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectMailSubject.action',
                    data: {subjectid: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }

            function editMailSubjectInit(keyval) {
                $("#updatedialog").data('subjectid', keyval).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {

                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("detailMailSubject.action?subjectid=" + $led.data('subjectid'));
            });


            function deleteMailSubjectInit(keyval) {
                $('#divmsg').empty();

                $("#deletedialog").data('keyval', keyval).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete mail subject ' + keyval + ' ?');
                return false;
            }

            function deleteMailSubject(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/DeleteMailSubject.action',
                    data: {subjectid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#deletesuccdialog").dialog('open');
                        $("#deletesuccdialog").html(data.message);
                        jQuery("#gridtable").trigger("reloadGrid");
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function resetSearchData() {
                $('#idsearch').val("");
                $('#descriptionsearch').val("");
                $('#statussearch').val("");

                $("#gridtable").jqGrid('setGridParam', {postData: {
                        s_id: '',
                        s_service: '',
                        s_status: '',
                        search: false
                    }});

                $("#gridtable").jqGrid('setGridParam', {page: 1});

                jQuery("#gridtable").trigger("reloadGrid");
            }
            function searchMailSubject() {
                var id = $('#idsearch').val();
                var description = $('#descriptionsearch').val();
                var status = $('#statussearch').val();
               

                $("#gridtable").jqGrid('setGridParam', {postData: {
                        s_id: id,
                        s_service: description,
                        s_status: status,
                        search: true
                    }});

                $("#gridtable").jqGrid('setGridParam', {page: 1});

                jQuery("#gridtable").trigger("reloadGrid");
            }
            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function resetFieldData() {
                $('#idsearch').val("");
                $('#subjectid').val("");
                $('#service').val("");
                $('#status').val("");

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");
                
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
        </script>
        <title></title>
    </head>

    <body style="">
        <jsp:include page="/header.jsp"/>

        <div class="main-container">


            <jsp:include page="/leftmenu.jsp"/>

            <div class="main-content">

                <div class="container" style="min-height: 760px;">


                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->

                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>
                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set var="vupdatebutt"><s:property value="vupdatebutt" default="true"/></s:set>
                            <s:set var="vupdatelink"><s:property value="vupdatelink" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form id="mailsubjectsearch" method="post" action="MailSubject" theme="simple" cssClass="form" >


                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>ID </label>
                                                <s:textfield name="idsearch" id="idsearch" maxLength="4" cssClass="form-control" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Description </label>
                                                <s:textfield  name="descriptionsearch" id="descriptionsearch" maxLength="30" cssClass="form-control" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                                            </div>                                      
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Status</label>
                                                <s:select  id="statussearch" list="%{statusList}"  headerValue="--Select Status--" headerKey="" name="statussearch" listKey="statuscode" listValue="description" disabled="false"  cssClass="form-control"/>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </s:form>
                                    
                                    <div class="row row_1 form-inline">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <sj:submit 
                                                    button="true"
                                                    value="Search" 
                                                    href="#"
                                                    onClick="searchMailSubject()"  
                                                    disabled="#vsearch"
                                                    id="searchbut"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                            <div class="form-group">                               
                                                <sj:submit 
                                                    button="true" 
                                                    id="cancelsearch"
                                                    value="Reset" 
                                                    onClick="resetSearchData()"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;"
                                                    /> 
                                            </div>
                                        </div>
                                        <div class="col-sm-5"></div>
                                        <div class="col-sm-3  text-right">
                                            <div class="form-group">                                               
                                                <s:url var="addurl" action="ViewPopupMailSubject"/>                                                    
                                                <sj:submit 
                                                    openDialog="remotedialog"
                                                    button="true"
                                                    href="%{addurl}"
                                                    disabled="#vadd"
                                                    value="Add Mail Subject"
                                                    id="addButton" 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                        </div>
                                    </div> 
                            </div>
                            <sj:dialog                                     
                                id="updatedialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="Update Mail Subject"
                                onOpenTopics="openviewtasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                />
                            <sj:dialog                                     
                                id="remotedialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                title="Add Mail Subject"                            
                                loadingText="Loading .."                            
                                position="center"                            
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                />
                            <!-- Start delete confirm dialog box -->
                            <sj:dialog 
                                id="deletedialog" 
                                buttons="{ 
                                'OK':function() { deleteMailSubject($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Delete Mail Subject"                            
                                />
                            <!-- Start delete process dialog box -->
                            <sj:dialog 
                                id="deletesuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                title="Deleting Process." 
                                />
                            <!-- Start delete error dialog box -->
                            <sj:dialog 
                                id="deleteerrordialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}                                    
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Delete error."
                                /> 
                            <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmTT($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectTT($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />

                            <div id="tablediv">
                                <s:url var="listurl" action="ListMailSubject"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    >
                                    <sjg:gridColumn name="subjectid" index="TT.ID" title="Edit" width="25" align="center" sortable="false" formatter="editformatter" hidden="#vupdatelink"/>
                                    <sjg:gridColumn name="subjectid" index="TT.ID" title="Delete" width="40" align="center" sortable="false" formatter="deleteformatter" hidden="#vdelete"/>  
                                    <sjg:gridColumn name="subjectid" index="TT.ID" title="ID"  sortable="true"/>
                                    <sjg:gridColumn name="service" index="TT.SERVICE" title="Description"  sortable="true"/>
                                    <sjg:gridColumn name="status" index="ST.DESCRIPTION" title="Status"  sortable="true"/> 
                                    <sjg:gridColumn name="maker" index="TT.MAKER" title="Maker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="checker" index="TT.CHECKER" title="Checker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="createdtime" index="TT.CREATEDTIME" title="Created Date And Time"  sortable="true" />
                                    <sjg:gridColumn name="lastupdatedtime" index="TT.LASTUPDATEDTIME" title="Last Updated Date And Time"  sortable="true" />
                                </sjg:grid> 
                            </div>
                                
                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="approveListMailSubject"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Mail Subject"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"  

                                    >
                                    <sjg:gridColumn name="id" index="id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>                   

                                    <sjg:gridColumn name="subjectid" index="u.PKey" title="ID"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                    
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="true"/>  
                                    <sjg:gridColumn name="createtime" index="u.createtime" title="Created Date And Time"  sortable="true" />                                                                  

                                </sjg:grid>  
                            </div>      

                        </div>

                    </div>




                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->



        <!-- end: BODY -->
    </body>
</html>

