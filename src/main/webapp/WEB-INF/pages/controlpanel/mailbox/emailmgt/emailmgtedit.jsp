<%-- 
    Document   : emailmgtedit
    Created on : Nov 1, 2019, 9:03:43 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Edit Inbox Message</title>       
        <script type="text/javascript">

            jQuery("#gridtable").trigger("reloadGrid");

            function backToMain() {
                window.location = "${pageContext.request.contextPath}/viewEmailMgt.action?";
            }

            function editCustomerCat(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findEmailMgt.action',
                    data: {id: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#divmsg').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#mailStatusEdit').val("");

                            $('#amessageedit').text("");
                            $('#updateButtonedit').button("disable");
                        } else {
                            $('#mailStatusEdit').val(data.mailStatus);
                            $('#amessageedit').text("");
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var id = $('#idEdit').val();
                editCustomerCat(id);
            }

        </script>
        <style>
            .ui-widget .ui-widget{
                border: none;
            }
        </style>
    </head>            
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="emailmgtedit" method="post" action="*EmailMgt" cssClass="form" theme="simple">
            <s:hidden id="idEdit" name="id" value="%{dataBean.id}" ></s:hidden>
                <div class="row row_popup"> 
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label >Message ID</label>
                        <s:label style="margin-bottom: 0px;" name="id"  value="%{dataBean.id}" cssClass="form-control"/>
                    </div>  
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >User Name</label>
                        <s:label style="margin-bottom: 0px;" name="userName"  value="%{dataBean.userName}"  cssClass="form-control"/>
                    </div>
                </div> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >CID</label>
                        <s:label style="margin-bottom: 0px;" name="userCif"  value="%{dataBean.cif}" cssClass="form-control"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup"> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Subject</label>
                        <s:label style="margin-bottom: 0px;" name="serviceId"  value="%{dataBean.serviceDes}" cssClass="form-control"/>
                    </div>  
                </div>
                <!--<div class="col-sm-4">-->
                <!--<div class="form-group">-->
                <!--<label >Subject</label>-->
                <%--<s:label style="margin-bottom: 0px;" name="subject"  value="%{dataBean.subject}" cssClass="form-control"/>--%>
                <!--</div>-->
                <!--</div>-->    
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status</label>
                        <s:label style="margin-bottom: 0px;" name="status"  value="%{dataBean.status}" cssClass="form-control"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Admin Read Status</label>
                        <s:label style="margin-bottom: 0px;" name="adminStatus"  value="%{dataBean.adminStatus}" cssClass="form-control"/>
                    </div> 
                </div>
            </div>
            <div class="row row_popup">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Customer Read Status</label>
                        <s:label style="margin-bottom: 0px;" name="readStatus"  value="%{dataBean.readStatus}" cssClass="form-control"/>
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Channel Type</label>
                        <s:label style="margin-bottom: 0px;" name="channelType"  value="%{dataBean.channelType}" cssClass="form-control"/>
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Maker</label>
                        <s:label style="margin-bottom: 0px;" name="maker"  value="%{dataBean.maker}" cssClass="form-control"/>
                    </div> 
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Created Date And Time</label>
                        <s:label style="margin-bottom: 0px;" name="createdDate"  value="%{dataBean.createdDate}" cssClass="form-control"/>
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Last Updated Date And Time</label>
                        <s:label style="margin-bottom: 0px;" name="lastUpdatedDate"  value="%{dataBean.lastUpdatedDate}" cssClass="form-control"/>
                    </div> 
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Message</label>
                        <s:textarea readonly="true" style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="messageEmail"  value="%{dataBean.message}" cssClass="form-control"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">                                                                              
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Mail Status</label>
                        <s:select  name="mailStatus" id="mailStatusEdit" value="%{dataBean.mailStatus}" cssClass="form-control"  list="%{mailStatusList}"  headerValue="-- Select Mail Status--" headerKey=""  listKey="statuscode" listValue="description"/>
                    </div>
                </div>  
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">               
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 form-inline">
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <s:url action="updateEmailMgt" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButtonedit"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />     
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
        <!-- end: BODY -->
    </body>
</html>
