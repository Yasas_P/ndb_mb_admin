<%-- 
    Document   : emailmgtdetailview
    Created on : Oct 15, 2019, 3:49:55 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <link rel="stylesheet" href="resouces/css/emailcss.css">
        <title>View Inbox Message</title>       
        <script type="text/javascript">
            
            jQuery("#gridtable").trigger("reloadGrid");
            
            function backToMain() {
                window.location = "${pageContext.request.contextPath}/viewEmailMgt.action?";
            }

            function todo() {
                
                
                form = document.getElementById('auditform2');
                form.action = 'individualReportEmailMgt';
//                form.submit();
            }
            function setSelectView(id) {
//                $("#fileId").val(id);
            }
            function getObjectFormView(obj,id) {
                $('.fileid',$(obj).closest("form")).val(id);
                $(obj).closest("form").submit();
            }
        </script>
    </head>            
    <body>
        <s:div id="divmsg">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:set id="vview" var="vview"><s:property value="vview" default="false"/></s:set>    
        <s:set id="vgenerateview" var="vgenerateview"><s:property value="vgenerateview" default=""/></s:set>    
        <s:form id="auditform2" method="post" action="*EmailMgt" cssClass="form" theme="simple">

            <div class="row row_popup"> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Message ID</label>
                        <s:label style="margin-bottom: 0px;" name="id"  value="%{dataBean.id}" cssClass="form-control"/>
                    </div>  
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >User Name</label>
                        <s:label style="margin-bottom: 0px;" name="userName"  value="%{dataBean.userName}"  cssClass="form-control"/>
                    </div>
                </div> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >CID</label>
                        <s:label style="margin-bottom: 0px;" name="userCif"  value="%{dataBean.cif}" cssClass="form-control"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup"> 
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Subject</label>
                        <s:label style="margin-bottom: 0px;" name="serviceId"  value="%{dataBean.serviceDes}" cssClass="form-control"/>
                    </div>  
                </div>
                <!--<div class="col-sm-4">-->
                    <!--<div class="form-group">-->
                        <!--<label >Subject</label>-->
                        <%--<s:label style="margin-bottom: 0px;" name="subject"  value="%{dataBean.subject}" cssClass="form-control"/>--%>
                    <!--</div>-->
                <!--</div>-->    
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Status</label>
                        <s:label style="margin-bottom: 0px;" name="status"  value="%{dataBean.status}" cssClass="form-control"/>
                    </div>
                </div>
                    <div class="col-sm-4">
                    <div class="form-group">
                        <label >Admin Read Status</label>
                        <s:label style="margin-bottom: 0px;" name="adminStatus"  value="%{dataBean.adminStatus}" cssClass="form-control"/>
                    </div> 
                </div>
            </div>
            <div class="row row_popup">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Customer Read Status</label>
                        <s:label style="margin-bottom: 0px;" name="readStatus"  value="%{dataBean.readStatus}" cssClass="form-control"/>
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Mail Status</label>
                        <s:label style="margin-bottom: 0px;" name="mailStatus"  value="%{dataBean.mailStatusDes}" cssClass="form-control"/>
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Channel Type</label>
                        <s:label style="margin-bottom: 0px;" name="channelType"  value="%{dataBean.channelType}" cssClass="form-control"/>
                    </div> 
                </div>
            </div>
            <div class="row row_popup">
                    <div class="col-sm-4">
                    <div class="form-group">
                        <label >Maker</label>
                        <s:label style="margin-bottom: 0px;" name="maker"  value="%{dataBean.maker}" cssClass="form-control"/>
                    </div> 
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Created Date And Time</label>
                        <s:label style="margin-bottom: 0px;" name="createdDate"  value="%{dataBean.createdDate}" cssClass="form-control"/>
                    </div> 
                </div>
                    
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Last Updated Date And Time</label>
                        <s:label style="margin-bottom: 0px;" name="lastUpdatedDate"  value="%{dataBean.lastUpdatedDate}" cssClass="form-control"/>
                    </div> 
                </div>
            </div>                 
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Message</label>
                        <s:textarea readonly="true" style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="messageEmail"  value="%{dataBean.message}" cssClass="form-control"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup text-right">
                <div class="col-sm-9"></div>
                <div class="col-sm-3">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">
                        <sj:submit
                            button="true"
                            value="View PDF"
                            id="viewindi" 
                            name="viewindi" 
                            onclick="todo()"  
                            disabled="#vgenerateview"
                            
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"     
                            
                            />                        
                    </div>
                </div>
            </div>
        </s:form>
        
        <div class="row row_popup">    
            <s:if test="%{attachmentSize != 0}">
                <s:form  method="post" action="downloadEmailMgt" theme="simple">
                    <s:hidden name="fileId" cssClass="fileid"/>
                    <div class="row">
                        <div class="count"><span><s:property value="attachmentSize" /></span>
                                <s:if test="%{attachmentSize == 1}">
                                Attachment
                            </s:if>
                            <s:elseif test="%{attachmentSize == 0}">
                                Attachment
                            </s:elseif>
                            <s:else>
                                Attachments
                            </s:else>
                        </div>
                        <div class="attchements">
                            <s:iterator value="attachmentList">
                                <s:set name="format" value="fileFormat.toString()" />
                                <s:if test = '%{#format == "png" || #format == "jpg"}'>
                                    <button onclick="setSelectView('<s:property  value="id" />');getObjectFormView(this, '<s:property  value="id" />');" type="button" class="fileBut" ><span class="glyphicon glyphicon-picture" style="color: #9C27B0;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                                </s:if>
                                <s:else>
                                    <button onclick="setSelectView('<s:property  value="id" />');getObjectFormView(this, '<s:property  value="id" />');" type="button" class="fileBut" ><span class="glyphicon glyphicon-file" style="color: #FF5722;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                                </s:else>
                            </s:iterator>
                        </div>
                        <sj:submit cssClass="btn btn-sm btn-functions" button="true" value="Download" id="downPendButton" disabled="#vdownload" cssStyle="visibility:hidden;"/>
                    </div>
                    <div style="margin-bottom: 10px"></div>
                </s:form>
            </s:if>
        </div>  
        <!-- end: BODY -->
    </body>
</html>
