<%-- 
    Document   : emailmgt
    Created on : Oct 25, 2018, 2:22:32 PM
    Author     : prathibha_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <style>
            .activeStatus{
                background: #4CAF50;
                color:white;
            }
            .deactiveStatus{
                background: #ff2727;
                color:white;
            }
        </style>
        <script>
            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });
             function editformatter(cellvalue, options, rowObject) {
                if (rowObject.mailStatus == "NEW" || rowObject.mailStatus == "INPR") {
                    return "<a href='#' title='Edit' onClick='javascript:editEmailInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }
            function viewdownloadeformatter(cellvalue, options, rowObject) {//operation
                if (rowObject.operation == "REPL" ) {
                    return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else {
                    return "--";
                }
            }
            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmEmailmgt(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectEmailmgt(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            function viewPendInit(keyval) {
                $("#viewpenddialog").data('id', keyval).dialog('open');
            }
            
            $.subscribe('openviewpendtasktopage', function (event, data) {
                var $led = $("#viewpenddialog");
                $led.html("Loading..");
                $led.load("viewPendEmailMgt.action?id=" + $led.data('id'));
            });
            function confirmEmailmgt(keyval, popvar) {
                $('#divmsg').empty();
                
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');
                
                return false;
            }

            function rejectEmailmgt(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');
                
                return false;
            }

            function confirmCC(keyval,remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmEmailMgt.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectCC(keyval,remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectEmailMgt.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function detailviewformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='View Customer' onClick='javascript:detailviewInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }
            function viewnreplyformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:viewnreplyInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }
            function attachmentformatter(cellvalue, options, rowObject) {
                if (cellvalue == "YES") {
                    return "<span class='glyphicon glyphicon-paperclip'></span>";
                } else {
                    return "--";
                }
            }
            function viewnreplyInit(keyval) {
                $("#viewnreplydialog").data('id', keyval).dialog('open');
            }
            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#viewnreplydialog");
                $led.html("Loading..");
                $led.load("detailListEmailMgt.action?id=" + $led.data('id'));
//                $led.load("detailEmailMgt.action?id=" + $led.data('id'));
            });
            function editEmailInit(keyval) {
                $("#vieweditdialog").data('id', keyval).dialog('open');
            }
            $.subscribe('openedittasktopage', function (event, data) {
                var $led = $("#vieweditdialog");
                $led.html("Loading..");
                $led.load("detaileditEmailMgt.action?id=" + $led.data('id'));
//                $led.load("detailEmailMgt.action?id=" + $led.data('id'));
            });

            function detailviewInit(keyval) {
                $("#viewdialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openviewdetailtasktopage', function (event, data) {
                var $led = $("#viewdialog");
                $led.html("Loading..");
                $led.load("detailviewEmailMgt.action?id=" + $led.data('id'));
            });
            // table color effects
            $.subscribe('gridComplete', function (event, data) {
                $.each(jQuery(data).jqGrid('getRowData'), function (i, item) {
                    $("#" + (item.id), data).find("td").not(".nopopup").click(function () {
                        viewnreplyInit(item.id);
                    });
                    $("#" + (item.id), data).find("td").not(".nopopup").mouseenter(function () {
                        if (item.adminStatus == "Read") { //deleted
                            $("#" + (item.id), data).find("td").not(".nopopup").css({'background': "white", 'cursor': 'pointer'});
                        }
                        if (item.adminStatus == "Unread") {
                            $("#" + (item.id), data).find("td").not(".nopopup").css({'background': "#272727", 'cursor': 'pointer'});
                        }

                    });
                    $("#" + (item.id), data).find("td").not(".nopopup").mouseout(function () {
                        if (item.adminStatus == "Read") { //deleted
                            $("#" + (item.id), data).find("td").not(".nopopup").css({'background': "#e8e8e8", 'cursor': 'pointer', 'color': '#525252', 'font-weight': 'normal'});
                        }
                        if (item.adminStatus == "Unread") {
                            $("#" + (item.id), data).find("td").not(".nopopup").css({'background': "#525252", 'cursor': 'pointer', 'color': 'white', 'font-weight': 'bold'})
                        }
                    });
                    if (item.adminStatus == "Read") { //deleted
                        $("#" + (item.id), data).find("td").not(".nopopup").css({'background': "#e8e8e8", 'cursor': 'pointer', 'color': '#525252', 'font-weight': 'normal'});
                    }
                    if (item.adminStatus == "Unread") {
                        $("#" + (item.id), data).find("td").not(".nopopup").css({'background': "#525252", 'cursor': 'pointer', 'color': 'white', 'font-weight': 'bold'})
                    }
                });
            });

            function searchEmail() {
                var userName_s = $('#userName_s').val();
                var cif_s = $('#cif_s').val();
                var status_s = $('#status_s').val();
                var adminStatus_s = $('#adminStatus_s').val();
                var mobileStatus_s = $('#mobileStatus_s').val();
                var mailStatus_s = $('#mailStatus_s').val();
                var fdate_s = $('#fdate_s').val();
                var todate_s = $('#todate_s').val();
                var channelType_s = $('#channelType_s').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        userName_s: userName_s,
                        cif_s: cif_s,
                        status_s: status_s,
                        adminStatus_s: adminStatus_s,
                        mobileStatus_s: mobileStatus_s,
                        mailStatus_s: mailStatus_s,
                        fdate_s: fdate_s,
                        todate_s: todate_s,
                        channelType_s: channelType_s,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetAllData() {

                $('#userName_s').val("");
                $('#cif_s').val("");
                $('#status_s').val("");
                $('#adminStatus_s').val("");
                $('#mobileStatus_s').val("");
                $('#mailStatus_s').val("");
                $('#fdate_s').val("");
                $('#todate_s').val("");
                $('#channelType_s').val("");

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        userName_s: '',
                        cif_s: '',
                        status_s: '',
                        adminStatus_s: '',
                        mobileStatus_s: '',
                        mailStatus_s: '',
                        fdate_s: '',
                        todate_s: '',
                        channelType_s: '',
                        search: false
                    }
                });
                
                setdate();

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

            }

            function resetFieldData() {
                $('#userName_s').val("");
                $('#cif_s').val("");
                $('#status_s').val("");
                $('#adminStatus_s').val("");
                $('#messageEmailReply_f3').val("");
                $('#file-upload').val("");
                $('.uploadFileNameList').empty();

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");
                $('#viewnreplydialog').scrollTop($('#viewnreplydialog')[0].scrollHeight);
                
                
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
            
            function setdate() {
                $("#fdate_s").datepicker("setDate", new Date());
                $("#todate_s").datepicker("setDate", new Date());
            }
            
            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;

                if (recors > 0 && isGenerate == false) {
                    $('#view2').button("enable");
                } else {
                    $('#view2').button("disable");
                }
            });
            
            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('emailmanagementsearch');
                form.action = 'reportGenerateEmailMgt.action';
                form.submit();

                $('#view2').button("disable");
            }

        </script>
    </head>

    <body onload="setdate()">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vupdatestatus"><s:property value="vupdatestatus" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>
                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                                <div id="formstyle">
                                <s:form id="emailmanagementsearch" method="post" action="EmailMgt" theme="simple" cssClass="form" >
                                    <s:hidden name="csrfValue" id="csrfValue" value="%{#session.csrfValue}"/>
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >From Date</label>
                                                <sj:datepicker cssClass="form-control" id="fdate_s" name="fdate_s" readonly="true" maxDate="d" changeYear="true"
                                                    buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >To Date</label>
                                                <sj:datepicker cssClass="form-control" id="todate_s" name="todate_s" readonly="true" maxDate="+1d" changeYear="true"
                                                    buttonImageOnly="true" displayFormat="yy-mm-dd" yearRange="2000:2200"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>User Name</label>
                                                <s:textfield cssClass="form-control" name="userName_s"  id="userName_s" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>CID</label>
                                                <s:textfield  cssClass="form-control" name="cif_s" id="cif_s" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"/>
                                            </div>
                                        </div>     
                                    </div>
                                    <div class="row row_1">                                                                              
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Status</label>
                                                <s:select  name="status_s" id="status_s" cssClass="form-control"  list="%{statusList}"  headerValue="-- Select Status--" headerKey=""  listKey="statuscode" listValue="description"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Admin Read Status</label>
                                                <s:select  name="adminStatus_s" id="adminStatus_s" cssClass="form-control"  list="%{adminStatusList}"  headerValue="-- Select Status--" headerKey=""  listKey="statuscode" listValue="description"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Customer Read Status</label>
                                                <s:select  name="mobileStatus_s" id="mobileStatus_s" cssClass="form-control"  list="%{adminStatusList}"  headerValue="-- Select Status--" headerKey=""  listKey="statuscode" listValue="description"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Mail Status</label>
                                                <s:select  name="mailStatus_s" id="mailStatus_s" cssClass="form-control"  list="%{mailStatusList}"  headerValue="-- Select Mail Status--" headerKey=""  listKey="statuscode" listValue="description"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label >Channel Type</label>
                                                <s:select cssClass="form-control" name="channelType_s" id="channelType_s" headerValue="-- Select Channel Type --" list="%{channelTypeList}"   headerKey="" listKey="key" listValue="value" />
                                            </div>
                                        </div> 
                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                disabled="#vsearch"
                                                onclick="searchEmail()"
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>                                         
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                />
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View CSV" 
                                                name="view2" 
                                                id="view2" 
                                                onClick="todocsv()" 
                                                disabled="#vgenerate"/> 
                                        </div>
                                    </div>
                                </div>
                                <!-- Start view dialog box -->
                                <sj:dialog                                     
                                    id="viewdialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Message"
                                    onOpenTopics="openviewdetailtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    cssStyle="overflow:auto"
                                    />
                                <!-- Start edit dialog box -->
                                <sj:dialog                                     
                                    id="vieweditdialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Edit Message Status"
                                    onOpenTopics="openedittasktopage"
                                    loadingText="Loading .."
                                    width="1000"
                                    height="600"
                                    dialogClass= "dialogclass"
                                    />
                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="viewnreplydialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Reply Message"
                                    onOpenTopics="openviewtasktopage"
                                    loadingText="Loading .."
                                    width="1000"
                                    height="600"
                                    dialogClass= "dialogclass"
                                    />
                                <!-- Start view pend dialog box -->
                                <sj:dialog                                     
                                    id="viewpenddialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Pending Message"
                                    onOpenTopics="openviewpendtasktopage"
                                    loadingText="Loading .."
                                    width="1000"
                                    height="600"
                                    dialogClass= "dialogclass"
                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteAtm($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete ATM"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process." 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error."
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmCC($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectCC($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />

                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="listEmailMgt"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="false"
                                    onGridCompleteTopics="gridComplete"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    sortable="false"
                                    shrinkToFit="false"
                                    >
                                    <sjg:gridColumn name="id" index="u.id" title="Edit" width="30" sortable="false" formatter="viewnreplyformatter" frozen="false" hidden="true"/>
                                    <sjg:gridColumn name="id" index="u.id" title="Update Status" width="60" sortable="false" formatter="editformatter" frozen="false" hidden="#vupdatestatus" align="center" cssClass="nopopup"/>
                                    <sjg:gridColumn name="id" index="u.id" title="View" width="60" sortable="false" formatter="detailviewformatter" frozen="false" cssClass="nopopup" />
                                    <sjg:gridColumn name="id" index="u.id" title="Message ID"  sortable="true"/>
                                    <%--<sjg:gridColumn name="message" index="u.message" title="Message"  sortable="true"/>--%>
                                    <sjg:gridColumn name="userName" index="u.swtMobileUser.username" title="User Name"  sortable="true"/>
                                    <sjg:gridColumn name="refId" index="u.refId" title="refId"  sortable="fasle" hidden="true"/>
                                    <%--<sjg:gridColumn name="userId" index="u.swtMobileUser.id" title="User ID"  sortable="true"/>--%>
                                    <sjg:gridColumn name="cif" index="u.swtMobileUser.cif" title="CID"  sortable="true"/>
                                    <sjg:gridColumn name="serviceDes" index="u.inboxServiceCategory.id" title="Subject"  sortable="true"/>
                                    <%--<sjg:gridColumn name="attachmentHave" title="Attachment"  sortable="false" formatter="attachmentformatter" align="center" width="70"/>--%>
                                    <%--<sjg:gridColumn name="subject" index="u.subject" title="Subject"  sortable="true" />--%>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true" hidden="false"/>
                                    <sjg:gridColumn name="adminStatus" index="u.adminStatus.description" title="Admin Read Status"  sortable="true" hidden="false"/>
                                    <sjg:gridColumn name="readStatus" index="u.readStatus" title="Customer Read Status"  sortable="true" hidden="false"/>
                                    <sjg:gridColumn name="mailStatus" index="u.mailStatus" title="Mail Status"  sortable="true" hidden="true"/>
                                    <sjg:gridColumn name="mailStatusDes" index="u.mailStatus" title="Mail Status"  sortable="true" hidden="false"/>
                                    <sjg:gridColumn name="channelType" index="u.channelType" title="Channel Type"  sortable="true" hidden="false"/>
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="false" />
                                    <%--<sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="false" />--%>  
                                    <sjg:gridColumn name="createdDate" index="u.createdDate" title="Created Date And Time"  sortable="true" hidden="false"/>
                                    <sjg:gridColumn name="lastUpdatedDate" index="u.lastUpdatedDate" title="Last Updated Date And Time"  sortable="true" hidden="false"/>
                                </sjg:grid> 
                            </div> 
                            <!-- start dual auth table -->
<!--                            <div id="tablediv">
                                <%--<s:url var="listurlap" action="approveListEmailMgt"/>--%>

                                <%--<sjg:grid--%>
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Inbox Messages"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    rowTotal="false"
                                    viewrecords="true"  

                                    >
                                    <%--<sjg:gridColumn name="id" index="id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>--%>                        
                                    <%--<sjg:gridColumn name="id" index="id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>--%>
                                <%--<sjg:gridColumn name="id" index="u.id" title="View" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/>--%> 
                                    <%--<sjg:gridColumn name="id" index="u.id" title="Message ID"  sortable="true" hidden="true"/>--%>
                                    <%--<sjg:gridColumn name="messageId" index="u.MessageId" title="Message ID"  sortable="true"/>--%>
                                    <%--<sjg:gridColumn name="message" index="u.message" title="Message"  sortable="true"/>--%>
                                    <%--<sjg:gridColumn name="operation" index="u.operation" title="Operation" hidden="true" sortable="true" key="true"/>--%>not need
                                    <%--<sjg:gridColumn name="operationDes" index="u.operation" title="Operation"  sortable="true" key="true"/>--%>
                                    <%--<sjg:gridColumn name="userName" index="u.swtMobileUser.username" title="User Name"  sortable="true"/>--%>
                                    <%--<sjg:gridColumn name="refId" index="u.refId" title="refId"  sortable="fasle" hidden="true"/>--%>
                                    <%--<sjg:gridColumn name="userId" index="u.swtMobileUser.id" title="User ID"  sortable="true"/>--%>not need
                                    <%--<sjg:gridColumn name="cif" index="u.swtMobileUser.cif" title="CID"  sortable="true"/>--%>
                                    <%--<sjg:gridColumn name="serviceDes" index="u.inboxServiceCategory.id" title="Subject"  sortable="true"/>--%>
                                    <%--<sjg:gridColumn name="attachmentHave" title="Attachment"  sortable="false" formatter="attachmentformatter" align="center" width="70"/>--%>
                                    <%--<sjg:gridColumn name="subject" index="u.subject" title="Subject"  sortable="true" />--%>
                                    <%--<sjg:gridColumn name="mailStatus" index="u.mailStatus" title="Mail Status"  sortable="true" />--%>
                                    <%--<sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true" hidden="false"/>--%>
                                    <%--<sjg:gridColumn name="adminStatus" index="u.adminStatus.description" title="Admin Read Status"  sortable="true" hidden="false"/>--%>
                                    <%--<sjg:gridColumn name="readStatus" index="u.readStatus" title="Mobile Read Status"  sortable="true" hidden="false"/>--%>                                                    
                                    <%--<sjg:gridColumn name="createduser" index="u.maker" title="Inputter"  sortable="true"/>--%>  
                                    <%--<sjg:gridColumn name="createdDate" index="u.createdDate" title="Created Time"  sortable="true" hidden="false"/>--%>
                                <%--</sjg:grid>--%>  
                            </div>   -->
                        </div>
                        <!-- end: PAGE CONTENT-->
                    </div>
                </div>
                <!-- end: PAGE -->
            </div>
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->



        <!-- end: BODY -->
        <script>
            $(document).ajaxSuccess(function (event, xhr, settings) {
                if ("${pageContext.request.contextPath}/reply2EmailMgt.action" == settings.url) {
                    var response = $.trim(xhr.responseText.toString());
                    if (response == "E1") {
                        $("#messageError").html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>Error occurred while processing</span></p></div></div>");
                    } else if (response == "E2") {
//                        $("#messageError").html("<span style='color:red;margin:5px'></span>");
                        $("#messageError").html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>Attached file formats not acceptable</span></p></div></div>");
                    } else if (response == "E3") {
//                        $("#messageError").html("<span style='color:red;margin:5px'>Please enter reply message to send</span>");
                        $("#messageError").html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>Please enter reply message to send</span></p></div></div>");
                    } else if (response == "E4") {
//                        $("#messageError").html("<span style='color:red;margin:5px'>Pending available</span>");
                        $("#messageError").html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>Pending available</span></p></div></div>");
                    } else if (response == "E5") {
//                        $("#messageError").html("<span style='color:red;margin:5px'>Record does not exists</span>");
                        $("#messageError").html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>Record does not exists</span></p></div></div>");
//                    } else if (response == "S1") {
//                        $("#messageError").html("<span style='color:#fffa90;margin:5px'>Successfully made a request to reply email</span>");
                    } else if (response == "E6") {
//                        $("#messageError").html("<span style='color:red;margin:5px'>Record does not exists</span>");
                        $("#messageError").html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span><span>Attached image is too large. Attached file size should be less than 10MB.</span></p></div></div>");
                    } else {
//                        $("#messageError").html("<div class='ui-state-highlight ui-corner-all' style='padding: 0.3em 0.7em;'> <p><span class='ui-icon ui-icon-info' style='float: left; margin-right: 0.3em;'></span><span>Successfully made a request to reply email </span></p></div>");
                        $("#messageError").html("<div class='ui-state-highlight ui-corner-all' style='padding: 0.3em 0.7em;'> <p><span class='ui-icon ui-icon-info' style='float: left; margin-right: 0.3em;'></span><span>Email has replied successfully </span></p></div>");
//                        $("#messageError").empty();
                        $("#messageContent").append(xhr.responseText);
                    }
                }
            });
        </script>
    </body>
</html>
