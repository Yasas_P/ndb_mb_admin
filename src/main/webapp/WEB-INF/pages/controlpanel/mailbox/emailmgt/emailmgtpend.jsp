<%-- 
    Document   : emailmgtpend
    Created on : Oct 10, 2019, 3:38:48 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/emailcss.css">
        <title>Email View Pend</title><style>
        #newval_container {
            border: 1px solid;
            padding: 10px;
            color: green;
            padding-left: 40px;
        }

        #newval_title {
            float: left;
            padding: 0 5px;
            margin: -20px 0 0 30px;
            background: #fff;
        }
</style>
        <script>
            function setSelectPend(id) {
//                $("#fileId").val(id);
            }
            function getObjectFormPend(obj, id) {
                $('.fileid', $(obj).closest("form")).val(id);
                $(obj).closest("form").submit();
            }
        </script>
    </head>
    <body>
        <div class="container">
            <s:div id="amessagepend">
                <s:actionerror theme="jquery"/>
                <s:actionmessage theme="jquery"/>
            </s:div>

            <div class="row">
                <div id="subject_f1"><s:property value="subject" /></div>
                <div class="icon_in">inbox</div>
            </div>
            <div class="row">
                <div class="line2"></div>
            </div>
            <s:iterator value="inputBeanList">
                <div class="row">
                    <div class="line2"></div>
                </div>
                <s:form id="emailMgtPend" method="post" action="EmailMgt" theme="simple" cssClass="form" >
                    <s:hidden name="csrfValue" id="csrfValuePend" value="%{#session.csrfValue}"/>
                    <s:hidden name="id" id="idPend"/>

                    <div class="row">


                        <div id="userName_f1"><s:property value="userName" /> <span><s:property value="userCif" /></span></div>

                        <s:if test = "%{isReply == 1}">
                            <div class="icon_pin"><span class="glyphicon glyphicon-share-alt"></span></div>
                            </s:if>

                        <div id="createDate_f1"><s:property value="createdDate" /></div>

                        <s:if test = "%{hasAttachment == 1}">
                            <div class="icon_pin"><span class="glyphicon glyphicon-paperclip"></span></div>
                            </s:if>
                        <div class="recipientName_f1">to <span><s:property value="recipientName" /></span></div>


                    </div>
                    <div class="row">
                        <div class="msg">
                            <s:property value="messageEmail" />
                        </div>

                    </div>


                </s:form>
                <s:if test="%{attachmentSize != 0}">
                    <s:form id="" method="post" action="downloadEmailMgt" theme="simple">
                        <s:hidden name="fileId" cssClass="fileid"/>
                        <div class="row">
                            <div class="count"><span><s:property value="attachmentSize" /></span>
                                    <s:if test="%{attachmentSize == 1}">
                                    Attachment
                                </s:if>
                                <s:elseif test="%{attachmentSize == 0}">
                                    Attachment
                                </s:elseif>
                                <s:else>
                                    Attachments
                                </s:else>
                            </div>
                            <div class="attchements">
                                <s:iterator value="attachmentList">
                                    <s:set name="format" value="fileFormat.toString()" />
                                    <s:if test = '%{#format == "png" || #format == "jpg"}'>
                                        <button onclick="setSelectPend('<s:property  value="id" />');getObjectFormPend(this, '<s:property  value="id" />');
                                                " type="button" class="fileBut" ><span class="glyphicon glyphicon-picture" style="color: #9C27B0;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                                    </s:if>
                                    <s:else>
                                        <button onclick="setSelectPend('<s:property  value="id" />');getObjectFormPend(this, '<s:property  value="id" />');" type="button" class="fileBut" ><span class="glyphicon glyphicon-file" style="color: #FF5722;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                                    </s:else>
                                </s:iterator>
                            </div>
                            <sj:submit cssClass="btn btn-sm btn-functions" button="true" value="Download" id="downPendButton" disabled="#vdownload" cssStyle="visibility:hidden;"/>
                        </div>
                        <div style="margin-bottom: 10px"></div>
                    </s:form>
                </s:if>

                <div class="row">
                    <div class="line"></div>
                </div>
            </s:iterator>
            <div style="padding:10px"></div>            
            <!--New pending value-->   
            <div id="newval_container">
                <div id="newval_title">New Reply Message</div>
                <s:form method="post" action="EmailMgt" theme="simple" cssClass="form" >
                    <s:hidden name="csrfValue" id="csrfValuePend" value="%{#session.csrfValue}"/>
                    <s:hidden name="id" id="idPend"/>

                    <div class="row">


                        <div id="userName_f1"><s:property value="userName" /> <span><s:property value="userCif" /></span></div>

                        <s:if test = "%{isReply == 1}">
                            <div class="icon_pin"><span class="glyphicon glyphicon-share-alt"></span></div>
                            </s:if>

                        <div id="createDate_f1"><s:property value="createdDate" /></div>

                        <s:if test = "%{hasAttachment == 1}">
                            <div class="icon_pin"><span class="glyphicon glyphicon-paperclip"></span></div>
                            </s:if>
                        <div class="recipientName_f1">to <span><s:property value="recipientName" /></span></div>


                    </div>
                    <div class="row">
                        <div class="msg">
                            <s:property value="messageEmail" />
                        </div>

                    </div>


                </s:form>
                <s:if test="%{attachmentSize != 0}">
                    <s:form  method="post" action="downloadTempEmailMgt" theme="simple">
                        <s:hidden name="fileId" cssClass="fileid"/>
                        <div class="row">
                            <div class="count"><span><s:property value="attachmentSize" /></span>
                                    <s:if test="%{attachmentSize == 1}">
                                    Attachment
                                </s:if>
                                <s:elseif test="%{attachmentSize == 0}">
                                    Attachment
                                </s:elseif>
                                <s:else>
                                    Attachments
                                </s:else>
                            </div>
                            <div class="attchements">
                                <s:iterator value="attachmentTempList">
                                    <s:set name="format" value="fileFormat.toString()" />
                                    <s:if test = '%{#format == "png" || #format == "jpg"}'>
                                        <button onclick="setSelectPend('<s:property  value="id" />');getObjectFormPend(this, '<s:property  value="id" />');" type="button" class="fileBut" ><span class="glyphicon glyphicon-picture" style="color: #9C27B0;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                                    </s:if>
                                    <s:else>
                                        <button onclick="setSelectPend('<s:property  value="id" />');getObjectFormPend(this, '<s:property  value="id" />');" type="button" class="fileBut" ><span class="glyphicon glyphicon-file" style="color: #FF5722;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                                    </s:else>
                                </s:iterator>
                            </div>
                            <sj:submit cssClass="btn btn-sm btn-functions" button="true" value="Download" id="downPendButton" disabled="#vdownload" cssStyle="visibility:hidden;"/>
                        </div>
                        <div style="margin-bottom: 10px"></div>
                    </s:form>
                </s:if>  
            </div>        

        </div>
    </body>
</html>
