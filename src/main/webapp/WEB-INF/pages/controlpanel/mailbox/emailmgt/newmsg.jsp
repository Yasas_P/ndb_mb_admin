<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<s:if test="hasActionErrors()">
    <s:property value="message"/>
</s:if>
<s:else>
    <script type="text/javascript">resetFieldData();</script>
    <%--<s:property value="message"/>--%>
    <div class="row">
        <div id="userName_f1"><s:property value="userName" /> <span><s:property value="userCif" /></span></div>
        <s:if test = "%{isReply == 1}">
            <div class="icon_pin"><span class="glyphicon glyphicon-share-alt"></span></div>
            </s:if>
        <div id="createDate_f1"><s:property value="createdDate" /></div>
        <s:if test = "%{hasAttachment == 1}">
            <div class="icon_pin"><span class="glyphicon glyphicon-paperclip"></span></div>
            </s:if>
        <div class="recipientName_f1">to <span><s:property value="recipientName" /></span></div>
    </div>
    <div class="row">
        <div class="msg">
            <s:property value="messageEmail" />
        </div>
    </div>
   
    <s:if test="%{attachmentSize != 0}">
        <s:form id="" method="post" action="downloadEmailMgt" theme="simple">
            <s:hidden name="csrfValue" id="csrfValue" value="%{#session.csrfValue}"/>
            <s:hidden name="fileId" cssClass="fileid"/>
            <div class="row">
                <div class="count"><span><s:property value="attachmentSize" /></span>
                    <s:if test="%{attachmentSize == 1}">
                        Attachment
                    </s:if>
                    <s:elseif test="%{attachmentSize == 0}">
                        Attachment
                    </s:elseif>
                    <s:else>
                        Attachments
                    </s:else>
                </div>
                <div class="attchements">
                    <s:iterator value="attachmentList">
                        <s:set name="format" value="fileFormat.toString()" />
                        <s:if test = '%{#format == "png" || #format == "jpg"}'>
                            <button onclick="setSelect('<s:property  value="id" />');getObjectForm(this, '<s:property  value="id" />');" type="button" class="fileBut" ><span class="glyphicon glyphicon-picture" style="color: #9C27B0;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                        </s:if>
                        <s:else>
                            <button onclick="setSelect('<s:property  value="id" />');getObjectForm(this, '<s:property  value="id" />');" type="button" class="fileBut" ><span class="glyphicon glyphicon-file" style="color: #FF5722;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                        </s:else>
                    </s:iterator>
                </div>
                <sj:submit cssClass="btn btn-sm btn-functions" button="true" value="Download" id="downButton" disabled="#vdownload" cssStyle="visibility:hidden;"/>
            </div>
            <div style="margin-bottom: 10px"></div>
        </s:form>
    </s:if>
    <div class="row">
        <div class="line"></div>
    </div>
</s:else>
