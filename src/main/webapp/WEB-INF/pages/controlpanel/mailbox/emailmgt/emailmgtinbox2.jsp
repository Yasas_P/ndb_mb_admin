<%-- 
    Document   : emailmgtinbox
    Created on : Oct 25, 2018, 2:25:33 PM
    Author     : prathibha_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/emailcss.css">
        <title>Email View</title>
        <script>
            function resetReplyData() {
                $('#messageEmailReply_f3').val("");
                $('#file-upload').val("");
                $('.uploadFileNameList').empty();
                $("#messageError").empty();
            }
            function changeImage() {
                $("#attachmentFile").change(function (event) {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#attachImg").attr("src", tmppath);
                });
            }
            jQuery("#gridtable").trigger("reloadGrid");


            function setSelect(id) {
//                $("#fileId").val(id);
            }
            function getObjectForm(obj,id) {
                $('.fileid',$(obj).closest("form")).val(id);
                $(obj).closest("form").submit();
            }
            $(document).ready(function () {
                var mailStatus =$("#mailStatus2").val();
                var vreply = <s:property value="vreply"/>;
                if(vreply==true || mailStatus!='INPR'){
                     $("#repbtn").prop( "disabled", true );
                }else if(vreply==false && mailStatus=='INPR'){
                    $("#repbtn").click(function () {
                        $(this).parent().hide();
                        $(".row > .replybox").show();
                        $('#viewnreplydialog').scrollTop($('#viewnreplydialog')[0].scrollHeight);
                    });
                }
                
            });

        </script>
        <style>
            .glyphicon-share-alt{
                -moz-transform: scaleX(-1);
                -o-transform: scaleX(-1);
                -webkit-transform: scaleX(-1);
                transform: scaleX(-1);
                filter: FlipH;
                -ms-filter: "FlipH";
            }

        </style>

    </head>
    <body>
        <div class="container">
            <s:div id="amessageedit">
                <s:actionerror theme="jquery"/>
                <s:actionmessage theme="jquery"/>
            </s:div>
            <s:set var="vdownload"><s:property value="vdownload" default="true"/></s:set>
            <s:set var="vreply"><s:property value="vreply" default="true"/></s:set>
            
            <div class="row">
                    <div id="subject_f1"><s:property value="subject" /></div>
                <div class="icon_in">inbox</div>
            </div>
            <div class="row">
                <div class="line2"></div>
            </div>
            <s:iterator value="inputBeanList">
                <div class="row">
                    <div class="line2"></div>
                </div>
                <s:form id="emailMgtedit" method="post" action="EmailMgt" theme="simple" cssClass="form" >
                    <s:hidden name="csrfValue" id="csrfValue" value="%{#session.csrfValue}"/>
                    <s:hidden name="id" id="id"/>

                    <div class="row">


                        <div id="userName_f1"><s:property value="userName" /> <span><s:property value="userCif" /></span></div>

                        <s:if test = "%{isReply == 1}">
                            <div class="icon_pin"><span class="glyphicon glyphicon-share-alt"></span></div>
                            </s:if>

                        <div id="createDate_f1"><s:property value="createdDate" /></div>

                        <s:if test = "%{hasAttachment == 1}">
                            <div class="icon_pin"><span class="glyphicon glyphicon-paperclip"></span></div>
                            </s:if>
                        <div class="recipientName_f1">to <span><s:property value="recipientName" /></span></div>


                    </div>
                    <div class="row">
                        <div class="msg">
                            <s:property value="messageEmail" />
                        </div>

                    </div>


                </s:form>
                <s:if test="%{attachmentSize != 0}">
                    <s:form id="" method="post" action="downloadEmailMgt" theme="simple">
                        <s:hidden name="fileId" cssClass="fileid"/>
                        <div class="row">
                            <div class="count"><span><s:property value="attachmentSize" /></span>
                                    <s:if test="%{attachmentSize == 1}">
                                    Attachment
                                </s:if>
                                <s:elseif test="%{attachmentSize == 0}">
                                    Attachment
                                </s:elseif>
                                <s:else>
                                    Attachments
                                </s:else>
                            </div>
                            <div class="attchements">
                                <s:iterator value="attachmentList">
                                    <s:set name="format" value="fileFormat.toString()" />
                                    <s:if test = '%{#format == "png" || #format == "jpg"}'>
                                        <button onclick="setSelect('<s:property  value="id" />');getObjectForm(this,'<s:property  value="id" />'); " type="button" class="fileBut" ><span class="glyphicon glyphicon-picture" style="color: #9C27B0;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                                    </s:if>
                                    <s:else>
                                        <button onclick="setSelect('<s:property  value="id" />');getObjectForm(this,'<s:property  value="id" />'); " type="button" class="fileBut" ><span class="glyphicon glyphicon-file" style="color: #FF5722;"></span>&nbsp;&nbsp;<s:property  value="fileName" /></button>
                                    </s:else>
                                </s:iterator>
                            </div>
                            <sj:submit cssClass="btn btn-sm btn-functions" button="true" value="Download" id="downButton" disabled="#vdownload" cssStyle="visibility:hidden;"/>
                        </div>
                        <div style="margin-bottom: 10px"></div>
                    </s:form>
                </s:if>

                <div class="row">
                    <div class="line"></div>
                </div>
            </s:iterator>

            <div id="messageContent" style="margin: 12px 0px;"></div>
            <div id="messageError"></div>

            <sj:div id="tragetHide" cssStyle="display:none">
            </sj:div>


            <s:form id="emailMgtedit2" method="post" action="EmailMgt" theme="simple" cssClass="form" enctype="multipart/form-data" >
                <s:hidden name="id" id="id2"/>
                <s:hidden name="mailStatus" id="mailStatus2"/>
                <div class="row">
                    <div class="replybtn">
                        <button type="button" class="btn btn-default btn-sm" id="repbtn" >
                            <span class="glyphicon glyphicon-share-alt"></span> Reply
                        </button>
                    </div>
                    <div class="replybox">
                        <div class="replymsg">
                            <div class="form-group">
                                <label for="comment"><span class="glyphicon glyphicon-share-alt"></span> Reply Message</label>
                                <textarea name="messageEmailReply" id="messageEmailReply_f3"  class="form-control" rows="8" id="comment" maxlength="2000"></textarea>
                            </div>
                        </div>
                        <div class="fileuploadBtn">
                            <label id="filePin">
                                <span class="glyphicon glyphicon-paperclip"></span>
                                <s:file name="filesUpload" multiple="multiple" id="file-upload" accept="jpg"/>
                            </label>
                        </div>
                        <div class="button">
                            <s:url action="reply2EmailMgt" var="replyurl"/>
                            <sj:submit
                                button="true"
                                value="Send"
                                href="%{replyurl}"
                                targets="tragetHide"
                                id="updateButtonedit_f3"
                                cssClass="sendbtn"
                                cssStyle="position: absolute;margin-top: -50px;margin-left: 18px;"
                                />  
                        </div>
                        <div class="button">
                            <sj:submit
                                button="true"
                                value="Reset"
                                id="Reset_f3"
                                onClick="resetReplyData()" 
                                cssClass="sendbtn"
                                cssStyle="position: absolute;margin-top: -50px;margin-left: 135px;"
                                />  
                        </div>
                    </div>
                    <div class="uploadFileNameList"></div>
                </div>
            </s:form>
        </div>
        <script>
            $(document).ready(function () {
                var count = '<s:property value="unreadCount" />';
                $('.profile-button2 > .noti').text(count);
                
                $('#file-upload').change(function () {
                    var file = $('#file-upload')[0].files;
                    if (file.length > 0) {
                        $('.uploadFileNameList').empty();
//                        $('.uploadFileNameList').show();
                        for (var i = 0; i < file.length; i++) {
                            $('.uploadFileNameList').append('<div class="ufileName"><span class="glyphicon glyphicon-paperclip"></span>' + file[i].name + '</div>');
                        }
                    }
                    $('#viewnreplydialog').scrollTop($('#viewnreplydialog')[0].scrollHeight);
                });
            });
        </script>
    </body>
</html>
