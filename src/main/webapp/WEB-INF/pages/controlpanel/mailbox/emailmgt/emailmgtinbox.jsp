<%-- 
    Document   : emailmgtinbox
    Created on : Oct 25, 2018, 2:25:33 PM
    Author     : prathibha_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/emailcss.css">
        <title>Email View</title>
        <script>
            function changeImage() {
                $("#attachmentFile").change(function (event) {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#attachImg").attr("src", tmppath);
                });
            }
            jQuery("#gridtable").trigger("reloadGrid");


            function setSelect(ss) {
                $("#fileId").val(ss);
            }
            $(document).ready(function () {
                $("#repbtn").click(function () {
                    $(this).parent().hide();
                    $(".row > .replybox").show();
                });
            });
        </script>

    </head>
    <body>
        <div class="container">
            <s:div id="amessageedit">
                <s:actionerror theme="jquery"/>
                <s:actionmessage theme="jquery"/>
            </s:div>
            <s:set var="vdownload"><s:property value="vdownload" default="true"/></s:set>


            <s:form id="emailMgtedit" method="post" action="EmailMgt" theme="simple" cssClass="form" >
                <s:hidden name="csrfValue" id="csrfValue" value="%{#session.csrfValue}"/>
                <s:hidden name="id" id="id"/>
                <div class="row">
                    <div id="subject_f1"><s:property value="subject" /></div>
                    <div class="icon_in">inbox</div>
                </div>
                <div class="row">
                    <div class="line2"></div>
                </div>
                <div class="row">
                    <div id="userName_f1"><s:property value="userName" /> <span>< cif : <s:property value="userCif" /> ></span></div>
                    <div id="createDate_f1"><s:property value="createdDate" /></div>
                    <s:if test = "%{hasAttachment == 1}">
                        <div class="icon_pin"><span class="glyphicon glyphicon-paperclip"></span></div>
                        </s:if>
                    <div class="recipientName_f1">to <span><s:property value="recipientName" /></span></div>
                </div>
                <div class="row">
                    <div class="msg">
                        <s:property value="messageEmail" />
                    </div>

                </div>
                <div class="row">
                    <div class="line"></div>
                </div>
            </s:form>
            <s:form id="downloadEmailMgtID" method="post" action="downloadEmailMgt" theme="simple">
                <s:hidden name="fileId" id="fileId"/>
                <div class="row">
                    <div class="count"><span><s:property value="attachmentSize" /></span>
                            <s:if test="%{attachmentSize == 1}">
                            Attachment
                        </s:if>
                        <s:elseif test="%{attachmentSize == 0}">
                            Attachment
                        </s:elseif>
                        <s:else>
                            Attachments
                        </s:else>
                    </div>
                    <div class="attchements">
                        <s:iterator value="attachmentList">
                            <div onclick="setSelect('<s:property  value="id" />'); javascript:downloadEmailMgtID.submit();" class="file"><s:property  value="fileName" /></div>
                        </s:iterator>
                    </div>
                    <sj:submit cssClass="btn btn-sm btn-functions" button="true" value="Download" id="downButton" disabled="#vdownload" cssStyle="visibility:hidden;"/>
                </div>
            </s:form>
            <s:form id="emailMgtedit2" method="post" action="EmailMgt" theme="simple" cssClass="form" >
                <s:hidden name="id" id="id2"/>
                <div class="row">
                    <div class="replybtn">
                        <button type="button" class="btn btn-default btn-sm" id="repbtn">
                            <span class="glyphicon glyphicon-share-alt"></span> Reply
                        </button>
                    </div>
                    <div class="replybox">
                        <div class="replymsg">
                            <div class="form-group">
                                <label for="comment"><span class="glyphicon glyphicon-share-alt"></span> Reply Message</label>
                                <textarea name="messageEmailReply" id="messageEmailReply_f3"  class="form-control" rows="8" id="comment"></textarea>
                            </div>
                        </div>
                        <div class="button">
                            <s:url action="replyEmailMgt" var="replyurl"/>
                            <sj:submit
                                button="true"
                                value="Send"
                                href="%{replyurl}"
                                targets="amessageedit"
                                id="updateButtonedit_f3"
                                cssClass="sendbtn"
                                cssStyle="position: absolute;margin-top: -50px;margin-left: 18px;"
                                />  
                        </div>
                    </div>
                </div>
            </s:form>
        </div>
    </body>
</html>
