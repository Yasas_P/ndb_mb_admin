<%-- 
    Document   : passwordpolicypend
    Created on : Jun 27, 2019, 3:05:43 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View Pend Password Policy</title> 
    </head>
    <body>
        <s:div id="pmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="passwordpolicypend" method="post" action="PasswordPolicy" theme="simple" cssClass="form" enctype="multipart/form-data"  >
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Password Policy</label>
                        <s:textfield value="%{passwordpolicydes}" cssClass="form-control" name="passwordpolicydes" id="passwordpolicydes_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Minimum Length</label>
                        <s:textfield value="%{minimumlength}" cssClass="form-control" id="minimumlength_p"  name="minimumlength"  readonly="true"  />
                    </div>
                </div>               
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Maximum Length</label>
                        <s:textfield value="%{maximumlength}" cssClass="form-control" id="maximumlength_p" name="maximumlength"  readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Minimum Special Characters</label>
                        <s:textfield value="%{minimumspecialcharacters}" cssClass="form-control" name="minimumspecialcharacters" id="minimumspecialcharacters_p"  readonly="true" />
                    </div>
                </div>               
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Minimum Upper Case Characters</label>
                        <s:textfield value="%{minimumuppercasecharacters}" cssClass="form-control" name="minimumuppercasecharacters" id="minimumuppercasecharacters_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Minimum Lower Case Characters</label>
                        <s:textfield value="%{minimumlowercasecharacters}" cssClass="form-control" name="minimumlowercasecharacters" id="minimumlowercasecharacters_p"  readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Minimum Numerical Characters</label>
                        <s:textfield value="%{minimumnumericalcharacters}" cssClass="form-control" name="minimumnumericalcharacters" id="minimumnumericalcharacters_p"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Allowed Repeat Characters</label>
                        <s:textfield value="%{repeatcharactersallow}" cssClass="form-control" name="repeatcharactersallow" id="repeatcharactersallow_p" readonly="true" />
                    </div>
                </div>
                <s:set name="passwordpolicyid" value="passwordpolicyid"/>        
                <s:if test="%{#passwordpolicyid!=3}">     
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Password Expiry Period</label>
                            <s:textfield value="%{passwordexpiryperiod}" cssClass="form-control" name="passwordexpiryperiod" id="passwordexpiryperiod_p"  readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Password Expiry Notification Period</label>
                            <s:textfield value="%{minimumpasswordchangeperiod}" cssClass="form-control" name="minimumpasswordchangeperiod" id="minimumpasswordchangeperiod_p"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>No. of History Passwords</label>
                            <s:textfield value="%{noofhistorypassword}" cssClass="form-control" name="noofhistorypassword" id="noofhistorypassword_p" readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Idle Account Expiry Period</label>
                            <s:textfield value="%{idleaccountexpiryperiod}" cssClass="form-control" name="idleaccountexpiryperiod" id="idleaccountexpiryperiod_p" readonly="true" />
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>No. of Invalid Login Attempts</label>
                            <s:textfield value="%{noofinvalidloginattempt}" cssClass="form-control" name="noofinvalidloginattempt" id="noofinvalidloginattempt_p"  readonly="true" />
                        </div>
                    </div>
                </s:if>        
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
