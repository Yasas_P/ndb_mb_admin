<%-- 
    Document   : delistedcontract
    Created on : Jul 1, 2019, 9:23:05 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
//            $.subscribe('onclicksearch', function (event, data) {
            function search() {
                $('#message').empty();

                var cidSearch = $('#cidSearch').val();
//                var delistedCategorySearch = $('#delistedCategorySearch').val();
                var contractNumberSearch = $('#contractNumberSearch').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        cidSearch: cidSearch,
//                        delistedCategorySearch: delistedCategorySearch,
                        contractNumberSearch: contractNumberSearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
//            );

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });
            
            function deleteformatter(cellvalue, options, rowObject) {
                return "<a href='#/' title='Delete' onClick='javascript:deleteDelistedContractInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.delistedCategory + "&#34;,&#34;" + rowObject.contractNumber + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            
            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmDelistedContract(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectDelistedContract(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            function viewdownloadeformatter(cellvalue, options, rowObject) {
                if (rowObject.operationcode == "UPLD") {
                    return "<a href='pendCsvDownloadeDelistedContract.action?id=" + cellvalue + "' onclick='downloadmsgempty()' title='Downloade'><img class='ui-icon ui-icon-arrowthickstop-1-s' style='display:inline-table;border:none;'/></a>";
                } else {    
                    return "--";
                }
            }
            
            function deleteDelistedContractInit(cid,delistedCategory,contractNumber) {
                $('#divmsg').empty();

                $("#deletedialog").data('cid', cid).data('delistedCategory',delistedCategory).data('contractNumber',contractNumber).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete delisted contract (CID : ' + cid+ ', Delisted Category : ' +delistedCategory +', Contract Number : ' +contractNumber +') ?');
                return false;
            }

            function deleteDelistedContract(cid,delistedCategory,contractNumber) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/deleteDelistedContract.action',
                    data: {cid: cid, delistedCategory: delistedCategory, contractNumber: contractNumber},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#deletesuccdialog").dialog('open');
                        $("#deletesuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }
            
            function confirmDelistedContract(keyval, popvar) {
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');


                return false;
            }

            function rejectDelistedContract(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function confirmBC(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmDelistedContract.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectBC(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectDelistedContract.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }

            function resetAllData() {
                $('#cidSearch').val("");
//                $('#delistedCategorySearch').val("");
                $('#contractNumberSearch').val("");

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        cidSearch: '',
//                        delistedCategorySearch: '',
                        contractNumberSearch: '',
                        search: false
                    }
                });
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetFieldData() {
                $('#cid').val("");
                $('#addDelist').button("disable");
                $('#currentContractNumberBox').empty();
                $('#newContractNumberBox').empty();
                $('#accontList_query').empty();

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }


        </script>
        <title></title>
    </head>
    <body style="">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set id="vassign" var="vassign"><s:property value="vassign" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form cssClass="form" id="delistedcontractsearch" method="post" action="DelistedContract" theme="simple" >
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>CID </label>
                                                <s:textfield cssClass="form-control" name="cidSearch" id="cidSearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Delisted Category</label>
                                                <s:select  cssClass="form-control" name="delistedCategorySearch" id="delistedCategorySearch" list="%{delistedCategoryList}"   headerKey=""  headerValue="--Select Delisted Category--" listKey="code" listValue="description" value="%{delistedCategory}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Contract Number</label>
                                                <s:textfield cssClass="form-control" name="contractNumberSearch" id="contractNumberSearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                                            </div>
                                        </div>
                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                href="#"
                                                disabled="#vsearch"
                                                onClick="search()"  
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div> 
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                />
                                        </div>
                                    </div>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-6  text-right"> 
                                        <div class="form-group">
                                            <s:url var="uploadurl" action="viewPopupcsvDelistedContract"/>   
                                            <sj:submit                                                      
                                                openDialog="remotedialog"
                                                button="true"
                                                href="%{uploadurl}"
                                                disabled="#vupload"
                                                value="Upload Delisted Contract"
                                                id="uploadButton"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>
<!--                                        <div class="form-group">                                               
                                            <%--<s:url var="addurl" action="viewPopupDelistedContract"/>--%>                                                    
                                            <%--<sj:submit--%> 
                                                openDialog="remotedialog"
                                                button="true"
                                                href="%{addurl}"
                                                disabled="#vadd"
                                                value="Assign Delisted Contract"
                                                id="addButton"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>-->
                                    </div>
                                </div>

                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="remotedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Assign Delisted Contract"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />  
                                <!-- Start update dialog box -->
                                <sj:dialog                                     
                                    id="updatedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Update Delisted Contract"
                                    onOpenTopics="openviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteDelistedContract($(this).data('cid'),$(this).data('delistedCategory'),$(this).data('contractNumber'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete Delisted Contract"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process" 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error"
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmBC($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectBC($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />
                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="listDelistedContract"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    >
                                    <sjg:gridColumn name="cid" index="u.id.cid" title="Delete" width="40" align="center" formatter="deleteformatter" sortable="false" hidden="#vdelete"/>                                     
                                    <sjg:gridColumn name="cid" index="u.id.cid" title="CID"  sortable="true"/>
                                    <sjg:gridColumn name="delistedCategory" index="u.id.delistedCategory " title="Delisted Category"  sortable="true" hidden="true"/>
                                    <sjg:gridColumn name="delistedCategoryDes" index="u.delistedCategory.description " title="Delisted Category"  sortable="true" />
                                    <sjg:gridColumn name="contractNumber" index="u.id.contractNumber" title="Contract Number"  sortable="true"/>
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="createdtime" index="u.createtime" title="Created Date And Time"  sortable="true" />
                                    <sjg:gridColumn name="lastupdatedtime" index="u.lastupdatedtime" title="Last Updated Date And Time"  sortable="true" />
                                </sjg:grid> 
                            </div>

                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="approveListDelistedContract"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Delisted Contract"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"  
                                    >

                                    <sjg:gridColumn name="id" index="u.id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="u.id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>
                                    <sjg:gridColumn name="id" index="u.id" title="Download" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/> 

                                    <sjg:gridColumn name="cid" index="u.id" title="CID"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="delistedCategory" index="u.id" title="Delisted Category"  sortable="true" key="true" hidden="true"/>
                                    <sjg:gridColumn name="contractNumber" index="u.id" title="Contract Number"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                                     
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="false"/>  
                                    <sjg:gridColumn name="createtime" index="u.createdtime" title="Created Date And Time"  sortable="false" />
                                </sjg:grid>  
                            </div>  
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!-- end: BODY -->
    </body>
</html>
