<%-- 
    Document   : delistedcontractinsert
    Created on : Jul 1, 2019, 10:26:51 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Assign Delisted Contract</title> 
    </head>
    <script>
        function accInquiry() {

            var cid = $("#cid").val();

            $.ajax({
                url: '${pageContext.request.contextPath}/cusAccountInqDelistedContract.action',
                data: {cid: cid},
                dataType: "json",
                type: "POST",
                success: function (data) {
                    var msg = data.message;

                    if (msg) {
                        $('#amessageaccupdate').html("<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'> <p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span>"
                                + "<span>" + msg
                                + "</span></p></div></div>");
                        $('#cid').empty();
                        $('#addDelist').button("enable");
                        $('#currentContractNumberBox').empty();
                        $('#newContractNumberBox').empty();
                        $('#accontList_query').empty();
                    } else {
                        $('#amessageaccupdate').text("");
                        $('#defaultAccountnew_pa').empty();
                        $('#addDelist').button("enable");
                        $('#currentContractNumberBox').empty();
                        $('#newContractNumberBox').empty();
                        $.each(data.newContractNumberBox, function (index, item) {
                            $('#newContractNumberBox').append(
                                    $('<option></;option>').val(item).html(item)
                                    );
                        });
                        $.each(data.currentContractNumberBox, function (index, item) {
                            $('#currentContractNumberBox').append(
                                    $('<option></option>').val(item).html(item)
                                    );
                        });
                        
                        $('#accontList_query').empty();
                        var html = "";
                        var count = 0;
                        $.each(data.contractDatailList, function (index, item) {
                            html += "<div class='row row_popup'><div class='col-sm-2'><div class='form-group'>" +
                                    "<div >" + item.accNumber + "</div>" +
                                    "</div></div><div class='col-sm-2'> <div class='form-group'>" +
                                    "<div  >" + item.accType + "</div>" +
                                    "</div> </div><div class='col-sm-2'><div class='form-group'>" +
                                    "<div  >" + item.accStatus+ "</div>" +
                                    "</div> </div><div class='col-sm-2'><div class='form-group'>" +
                                    "<div  >" + item.currency + "</div>" +
                                    "</div> </div><div class='col-sm-2'><div class='form-group'>" +
                                    "<div  >" + item.accProduct + "</div>" +
                                    "</div> </div><div class='col-sm-2'><div class='form-group'>" +
                                    "<div  >" + item.availBalance + "</div>" +
                                    "</div></div></div >";
                            $('#u_primaryaccount').append(
                                    $('<option></option>').val(item.accountNumber).html(item.accountNumber)
                                    );
                            count++;
                        });
                        if (count > 0) {
//                            $('#accountSeparaterline').show();//prop("hidden","false");
                            $('#accontList_query').html(html);
                        }

                    }
                },
                error: function (data) {
                    window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                }
            });

        }
        $.subscribe('resetdelist', function (event, data) {
            $('#amessageaccupdate').text("");
            $('#cid').val("");
            $('#addDelist').button("disable");
            $('#currentContractNumberBox').empty();
            $('#newContractNumberBox').empty();
            $('#accontList_query').empty();
        });

        function clickAdd() {

            $('#currentContractNumberBox option').prop('selected', true);
            $('#newContractNumberBox option').prop('selected', true);
            $('#adddelistedcontract').submit();
        }
        //----------- delisted Contract Multiselect------------------------
        function toleft() {
            $("#currentContractNumberBox option:selected").each(function () {

                $("#newContractNumberBox").append($('<option>', {
                    value: $(this).val(),
                    text: $(this).text()
                }));
                $(this).remove();
            });
        }

        function toright() {
            $("#newContractNumberBox option:selected").each(function () {

                $("#currentContractNumberBox").append($('<option>', {
                    value: $(this).val(),
                    text: $(this).text()
                }));
                $(this).remove();
            });
        }

        function toleftall() {
            $("#currentContractNumberBox option").each(function () {

                $("#newContractNumberBox").append($('<option>', {
                    value: $(this).val(),
                    text: $(this).text()
                }));
                $(this).remove();
            });
        }

        function torightall() {
            $("#newContractNumberBox option").each(function () {

                $("#currentContractNumberBox").append($('<option>', {
                    value: $(this).val(),
                    text: $(this).text()
                }));
                $(this).remove();
            });

        }

    </script>
    <body>
        <s:div id="amessageaccupdate">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>

        <s:set var="vupdate"><s:property value="vupdate" default="true"/></s:set>

        <s:form id="assigndelistedcontract" method="post" action="assignDelistedContract" theme="simple" cssClass="form" >

            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="row row_popup"> 
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label >CID</label>
                                    <s:textfield cssClass="form-control" name="cid" id="cid" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                                </div>  
                            </div> 
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <sj:submit
                                        button="true"
                                        value="Check"
                                        onClick="accInquiry()" 
                                        cssClass="form-control btn_normal"
                                        cssStyle="border-radius: 12px;background-color:#969595;color:white;" 
                                        />
                                </div>
                            </div>
                        </div>
                        <div class="row row_popup">
                            <div class="horizontal_line_popup"></div>
                        </div>
                        <div class="row row_popup">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <span style="color: red">*</span><label >Contract Number</label>
                                </div>
                            </div>
                        </div>         
                        <div class="row row_popup">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <s:select cssClass="form-control" multiple="true"
                                              name="newContractNumberBox" id="newContractNumberBox" list="newNumberList"									 
                                              ondblclick="toright()" style="height:160px;"  listKey="key" listValue="value"/>
                                </div>                
                            </div>
                            <div class="col-sm-2 text-center">
                                <div class="form-group">
                                    <div class="row" style="height: 20px;"></div>
                                    <div class="row">
                                        <sj:a
                                            id="right" 
                                            onClick="toright()" 
                                            button="true"
                                            cssClass="ui-button-move"
                                            style="font-size:10px;width:60px;margin:4px;"> > </sj:a>
                                        </div>
                                        <div class="row">
                                        <sj:a
                                            id="rightall" 
                                            onClick="torightall()" 
                                            button="true"
                                            cssClass="ui-button-move"
                                            style="font-size: 10px;width:60px;margin:4px;"> >> </sj:a>
                                        </div>
                                        <div class="row">
                                        <sj:a
                                            id="left" 
                                            onClick="toleft()" 
                                            button="true"
                                            cssClass="ui-button-move"
                                            style="font-size:10px;width:60px;margin:4px;"> < </sj:a>
                                        </div>
                                        <div class="row">
                                        <sj:a
                                            id="leftall" 
                                            onClick="toleftall()" 
                                            button="true"
                                            cssClass="ui-button-move"
                                            style="font-size:10px;width:60px;margin:4px;"> << </sj:a>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-5">
                                    <div class="form-group"> 
                                    <s:select cssClass="form-control" multiple="true" 
                                              name="currentContractNumberBox" id="currentContractNumberBox" list="currentNumberList"									 
                                              ondblclick="toleft()" style="height:160px;" />

                                </div>
                            </div>
                        </div>
                    </div>              
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class='row row_popup'>
                <div class='col-sm-2' >
                    <div class='form-group'>
                        <label >Account Number</label>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class='form-group'>
                        <label >Account Type</label>
                    </div> 
                </div>
                <div class='col-sm-2'>
                    <div class='form-group'>
                        <label >Account Status</label>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class='form-group'>
                        <label >Currency</label>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class='form-group'>
                        <label >Product code</label>
                    </div>
                </div>
                <div class='col-sm-2'>
                    <div class='form-group'>
                        <label >Available Balance</label>
                    </div>
                </div>
            </div >
            <div id="accontList_query">

            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>    
                <div class="col-sm-6  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <%--<s:url action="addDelistedContract" var="adddelisturl"/>--%>
                        <sj:submit
                            button="true"
                            id ="addDelist"
                            value="Assign"
                            onclick="clickAdd()"
                            targets="amessageaccupdate"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            disabled="true"
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetdelist"
                            />                          
                    </div>

                </div>
            </div>
        </div>
    </s:form> 
</body>
</html>
