<%-- 
    Document   : terms
    Created on : May 16, 2019, 11:10:37 AM
    Author     : sivaganesan_t
--%>

<%@page import="com.epic.ndb.util.varlist.CommonVarList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <style>
            .mce-content-body ul{
                list-style-position: inside;
            }
        </style>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
            function onlooad() {
                $('#updateButton').button("disable");
                $('#deleteButton').button("disable");
                $('#status').attr('disabled', true);
            }

            function changeVersion() {
                var isGenerate = <s:property value="vupdatelink"/>;
                var isdelete = <s:property value="vdelete"/>;

//                alert(isGenerate + " " + isdelete + val);
                if (!isGenerate) {
                    $('#updateButton').button("enable");
                } else {
                    $('#updateButton').button("disable");
                }

                if (!isdelete) {
                    $('#deleteButton').button("enable");
                } else {
                    $('#deleteButton').button("disable");
                }

                loadVersions();
            }

            function loadVersions() {
                var versionno = $('#versionno').val();
                $.ajax({
                    url: '${pageContext.request.contextPath}/LoadVersionTerms',
                    data: {
                        versionno: versionno
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#divmsg').empty();
                        var msg = data.message;
                        if (msg) {
                        } else {
                            if (data.description) {
                                $('#descriptionupmethod').val(data.description);
                                var content = document.getElementById("description_hidden").value;
                                tinyMCE.get('descriptionupmethod').setContent(content, {format: 'html'});
                                var contents = data.description;
                                tinyMCE.get('descriptionupmethod').setContent(contents, {format: 'html'});
                            } else {
                                $('#descriptionupmethod').val("");
                                tinyMCE.get('descriptionupmethod').setContent('', {format: 'html'});
                            }

                            $('#status').val(data.status);
                            $('#oldvalue').val(data.oldvalue);

                            if (data.status == 'DEACT') {
                                $('#status').attr('disabled', false);
                            } else if (data.status == 'ACT') {
                                $('#status').attr('disabled', false);
//                                $('#status').attr('disabled', true);//please uncomment
                                $('#statusAct').val(data.statusAct);
//                                $('#deleteButton').button("disable");

                            } else {
                                $('#status').attr('disabled', false);
//                                $('#status').attr('disabled', true);//please uncomment
                            }
                            $('#statusAct').val(data.statusAct);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }
            
            function deleteTermInit() {
                $('#divmsg').empty();
                var versionno = $('#versionno').val();
                var status = $('#status').val();
                $("#deletedialog").data({'version': versionno, 'status': status}).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete terms and conditions : ' + versionno + ' ?');
                return false;
            }

            function deleteTerm(versionno, status) {

                $.ajax({
                    url: '${pageContext.request.contextPath}/DeleteTerms',
                    data: {versionno: versionno,
                        status: status
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        var msg = data.errorMessage;
                        if (msg == null) {
                            resetAllData();
                            document.getElementById("divmsg").innerHTML = "<div class='ui-widget actionMessage'><div class='ui-state-highlight ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'><p><span class='ui-icon ui-icon-info' style='float: left; margin-right: 0.3em;'></span><span>" + data.message + "</span></p></div></div></div>";
                            $('html, body').animate({scrollTop: "0px"}, 'fast');
                        } else {
                            resetAllData();
                            document.getElementById("divmsg").innerHTML = "<div class='ui-widget actionError'><div class='ui-state-error ui-corner-all' style='padding: 0.3em 0.7em; margin-top: 20px;'><p><span class='ui-icon ui-icon-info' style='float: left; margin-right: 0.3em;'></span><span>" + msg + "</span></p></div></div></div>";
                            $('html, body').animate({scrollTop: "0px"}, 'fast');
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }


                });
            }

            // Open dialog and add tinymce to it
            $(document).ready(function () {
                $('#addButton').click(function () {
                    $("#dialogAdd").dialog({
                        width: 950,
                        height: $(window).height(),
                        modal: true,
                        close: function () {
                            tinyMCE.get('descriptionaddmethod').setContent('', {format: 'html'});
                        }
                    });

                    tinymce.init({
                        selector: '#descriptionaddmethod',
                        height: 175,
                        toolbar_items_size: 'small',
                        menubar: false,
//                        forced_root_block: '',
                        theme: 'modern',
                        plugins: ["advlist  link image lists charmap print preview hr anchor pagebreak spellchecker",
                            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                            "table contextmenu directionality emoticons template textcolor fullpage textcolor colorpicker textpattern"
                        ],
                        formats: {},
                        image_advtab: true,
                        toolbar1: 'undo redo | fontselect fontsizeselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview | forecolor | codesample | fullscreen',
                        content_css: [
//                            '//www.tinymce.com/css/codepen.min.css',
                            'resouces/css/tiny.css'
//                           
                        ]
                    });
//                    
                });
            });

            function resetAllDataAdd() {
                $('#descriptionaddmethod').val("");
                tinyMCE.get('descriptionaddmethod').setContent('', {format: 'html'});
                $('#versionnoadd').val("");
                $('#statusadd').val("");
                $('#amessage').text("");
            }

            function resetAllData() {
                $("#versionno").val("");
                $("#descriptionupmethod").val("");
                $("#status").attr('disabled', true);
                $("#status").val("");
                $('#updateButton').button("disable");
                $('#deleteButton').button("disable");
                $("#divmsg").empty();
                tinyMCE.get('descriptionupmethod').setContent('', {format: 'html'});
            }

            function resetFieldData() {
                $("#versionno").val("");
                $("#descriptionupmethod").val("");
                $("#status").attr('disabled', true);
                $("#status").val("");
                tinyMCE.get('descriptionupmethod').setContent('', {format: 'html'});
                $('#updateButton').button("disable");
                $('#deleteButton').button("disable");
                $('#descriptionaddmethod').val("");
                $('#versionnoadd').val("");
                $('#statusadd').val("");
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
                resetVersionList();
            }
            function clearQuill() {
                tinyMCE.get('descriptionaddmethod').setContent('', {format: 'html'});
            }

            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmTerms(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectTerms(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            function viewformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }
            function confirmTerms(keyval) {
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');


                return false;
            }

            function rejectTerms(keyval) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function viewPendInit(keyval) {
                $("#dialogPend").dialog({
                    width: 950,
                    height: 480,
                    dialogClass: 'fixed-dialog',
                    modal: true,
                    close: function () {
                        tinyMCE.get('descriptionpendmethod').setContent('', {format: 'html'});
                    }
                });
                tinymce.init({
                    selector: '#descriptionpendmethod',
                    height: 250,
                    toolbar_items_size: 'small',
                    menubar: false,
                    readonly: 1,
//                                                        forced_root_block: '',
                    theme: 'modern',
                    plugins: ["advlist lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "table contextmenu directionality emoticons template textcolor fullpage textcolor colorpicker textpattern"
                    ],
                    formats: {},
                    toolbar1: 'undo redo | fontselect fontsizeselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview | forecolor | codesample | fullscreen',
                    content_css: [
//                                                            '//www.tinymce.com/css/codepen.min.css',
                        'resouces/css/tiny.css'

                    ]
                });
                $.ajax({
                    url: '${pageContext.request.contextPath}/LoadTempVersionTerms',
                    data: {
                        versionno: keyval
                    },
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#divmsg').empty();
                        var msg = data.message;
                        if (msg) {
                        } else {
                            if (data.description) {
                                var contents = data.description;
                                tinyMCE.get('descriptionpendmethod').setContent(contents, {format: 'html'});
                            } else {
                                $('#descriptionupmethod').val("");
                                tinyMCE.get('descriptionpendmethod').setContent('', {format: 'html'});
                            }

                            $('#versionnopend').val(data.versionno);
                            $('#statuspend').val(data.status);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function confirmTC(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/ConfirmTerms.action',
                    data: {versionno: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errorMessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errorMessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectTC(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/RejectTerms.action',
                    data: {versionno: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errorMessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errorMessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }

            function resetVersionList() {
                $.ajax({
                    url: '${pageContext.request.contextPath}/FindVersionListTerms',
                    data: {},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        var msg = data.message;
                        if (msg) {
                        } else {
                            $('#versionno').empty();
                            $('#versionno').append("<option value=''>--Select Version Number--</option>");
                            $.each(data.versionMap, function(index, item) {
                                $('#versionno').append(
                                        $('<option></option>').val(item.key).html(
                                        item.value));
                            });
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }
            
            function alpha(e) {
                var k;
                document.all ? k = e.keyCode : k = e.which;
                return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || (k >= 48 && k <= 57) || (k == 13) || (k == 95) ||(k == 46));
            }

        </script>
        <title></title>
    </head>
    <body onload="onlooad()">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set id="vupdatelink" var="vupdatelink"><s:property value="vupdatelink" default="true"/></s:set>
                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set id="vdelete" var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form cssClass="form" id="terms" method="post" action="Terms" theme="simple" >
                                    <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline" >
                                                <span style="color: red">*</span><label>Version Number</label>
                                                <s:select cssClass="form-control" id="versionno" name="versionno" 
                                                          list="versionMap"  
                                                          headerValue="--Select Version Number--"  headerKey=""  
                                                          listKey="key" listValue="value" disabled="false"
                                                          onchange="changeVersion()"/>  
                                            </div>

                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">    
                                                <span style="color: red">*</span><label>Status </label>
                                                <s:select  id="status" list="%{statusList}" 
                                                           headerValue="--Select Status--" headerKey="" name="status" 
                                                           listKey="statuscode" listValue="description" onchange="changeStatus()" 
                                                           cssClass="form-control"  /> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <span style="color: red">*</span><label>Description</label> 
                                                <div class="standalone-container">

                                                    <div  id="descriptionupmethod"  style=" height: 175px;"> </div>
                                                    <input type="hidden" name="description" id="description_hidden" />
                                                    <input type="hidden" name="descriptiontxt" id="descriptiontxt" />
                                                </div>

                                                <script type="text/javascript">
//                                                  
                                                    //update terms
                                                    function set2() {

//                                                        document.getElementById("description_hidden").value = tinyMCE.activeEditor.getContent({format: 'html'});
                                                        document.getElementById("description_hidden").value = tinyMCE.get('descriptionupmethod').getContent({format: 'html'});
                                                        document.getElementById("descriptiontxt").value = tinyMCE.get('descriptionupmethod').getContent({format: 'html'}).replace(/(<([^>]+)>)/ig, '');

//                                                        var str = document.getElementById("description_hidden").value;
//                                                        var str2 = document.getElementById("descriptiontxt").value;
//                                                        var res = str.replace("<head>", "<head><meta name='format-detection' content='telephone=no'>");
//                                                        var res2 = str2.replace("<head>", "<head><meta name='format-detection' content='telephone=no'>");

//                                                        document.getElementById("description_hidden").value = res;
//                                                        document.getElementById("descriptiontxt").value = res2;
//                                                        console.log(document.getElementById("description_hidden").value);

                                                    }

                                                    //add terms
                                                    function set() {
                                                        document.getElementById("descriptionadd_hidden").value = tinyMCE.get('descriptionaddmethod').getContent({format: 'html'});
                                                        document.getElementById("descriptionaddtxt").value = tinyMCE.get('descriptionaddmethod').getContent({format: 'html'}).replace(/(<([^>]+)>)/ig, '');

                                                        var str = document.getElementById("descriptionadd_hidden").value;
                                                        var str2 = document.getElementById("descriptionaddtxt").value;
                                                        var res = str.replace("<head>", "<head><meta name='format-detection' content='telephone=no'>");
                                                        var res2 = str2.replace("<head>", "<head><meta name='format-detection' content='telephone=no'>");

                                                        document.getElementById("descriptionadd_hidden").value = res;
                                                        document.getElementById("descriptionaddtxt").value = res2;
//                                                        console.log(document.getElementById("descriptionadd_hidden").value);
                                                    }

                                                    function hideNetbeansGlassPane()
                                                    {
                                                        if (document.getElementById('netbeans_glasspane'))
                                                        {
                                                            document.getElementById('netbeans_glasspane').remove();
                                                            console.log("ss");
                                                        }
                                                    }
                                                    setTimeout(hideNetbeansGlassPane, 500);
//                                                   
                                                    tinymce.init({
                                                        selector: '#descriptionupmethod',
                                                        height: 250,
                                                        toolbar_items_size: 'small',
                                                        menubar: false,
//                                                        forced_root_block: '',
                                                        theme: 'modern',
                                                        plugins: ["advlist lists charmap print preview hr anchor pagebreak spellchecker",
                                                            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                                                            "table contextmenu directionality emoticons template textcolor fullpage textcolor colorpicker textpattern"
                                                        ],
                                                        formats: {},
                                                        toolbar1: 'undo redo | fontselect fontsizeselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview | forecolor | codesample | fullscreen',
                                                        content_css: [
//                                                            '//www.tinymce.com/css/codepen.min.css',
                                                            'resouces/css/tiny.css'

                                                        ]
                                                    });


                                                </script>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row row_1">
                                        <div class="col-sm-4">
                                            <div class="form-group form-inline">
                                                <span class="mandatoryfield">Mandatory fields are marked with *</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row row_1"></div>
                                    <div class="row row_1 form-inline">
                                        <!--<div class="col-sm-2"></div>-->
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <sj:submit button="true" 
                                                           value="Reset" 
                                                           name="reset" 
                                                           onClick="resetAllData()"
                                                           cssClass="form-control btn_normal"
                                                           cssStyle="border-radius: 12px;" />

                                            </div>
                                            <div class="form-group">
                                                <s:url var="updateurl" action="UpdateTerms"/>

                                                <sj:submit button="true" 
                                                           onclick="set2()"
                                                           href="%{updateurl}" 
                                                           disabled="#vupdatelink"
                                                           value="Update" 
                                                           targets="divmsg"   
                                                           id="updateButton"
                                                           cssClass="form-control btn_normal"
                                                           cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                           />

                                            </div>
                                            <div class="form-group">
                                                <sj:submit button="true" 
                                                           value="Delete" 
                                                           disabled="#vdelete"
                                                           id="deleteButton"
                                                           onclick="deleteTermInit(this)"
                                                           cssClass="form-control btn_normal"
                                                           cssStyle="border-radius: 12px;background-color:#969595;color:white;" />

                                            </div>
                                        </div>
                                        <div class="col-sm-4 text-right">
                                            <div class="form-group">
                                                <%--<s:url var="addurl" action="ViewPopupTerms"/>--%>
                                                <!--<button value="Add New Terms"  disabled="#vadd" id="addButton" Class="form-control btn_normal" Style="border-radius: 12px;background-color:#969595;color:white;"></button>-->
                                                <sj:submit                                                      
                                                    button="true"
                                                    disabled="#vadd"
                                                    value="Add New Terms And Conditions"
                                                    id="addButton"
                                                    onclick="resetAllDataAdd()"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                        </div>        
                                    </div>


                                </s:form>

                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteTerm($(this).data('version'),$(this).data('status'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete Terms"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process." 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error."
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmTC($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectTC($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />
                            </div>
                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="ApproveListTerms"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Terms And Conditions"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"  
                                    >

                                    <sjg:gridColumn name="versionNo" index="u.versionNo" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="versionNo" index="u.versionNo" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>
                                    <sjg:gridColumn name="versionNo" index="u.versionNo" title="View" width="40" align="center" formatter="viewformatter" hidden="#vdual"/>

                                    <sjg:gridColumn name="versionNo" index="u.versionNo" title="Version Number"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <%--<sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>--%>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                                     
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="false"/>  
                                    <sjg:gridColumn name="createtime" index="u.createdtime" title="Created Date And Time"  sortable="false" />
                                </sjg:grid>  
                            </div>
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!--//////////////start: Add Div/////////////////////////////-->

        <div id="dialogAdd" title="Add Terms And Conditions" style="display: none; overflow: hidden; z-index: 10000;">
            <s:div id="amessage">
                <s:actionerror theme="jquery"/>
                <s:actionmessage theme="jquery"/>
            </s:div>

            <s:form cssClass="form" id="termsadd" method="post" action="#" theme="simple" >
                <div class="row row_popup">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label >Version Number </label>
                            <s:textfield name="versionnoadd" id="versionnoadd" cssClass="form-control"  maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 .]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 .]/g,''))" onkeypress="return alpha(event)"/> 
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">    
                            <span style="color: red">*</span><label>Status </label>
                            <s:select  id="statusadd" list="%{statusList}" 
                                       headerValue="--Select Status--" headerKey="" name="statusadd" 
                                       listKey="statuscode" listValue="description" disabled="false"  
                                       cssClass="form-control"  /> 
                        </div>
                    </div>     
                </div>
                <div class="row row_popup">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <span style="color: red">*</span><label>Description</label> 
                            <div class="standalone-container">
                                <div  id="descriptionaddmethod" style=" height: 175px;"> </div>
                                <input type="hidden" name="descriptionadd" id="descriptionadd_hidden" />
                                <input type="hidden" name="descriptionaddtxt" id="descriptionaddtxt" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row row_popup form-inline">
                    <div class="col-sm-9">
                        <div class="form-group">
                            <span class="mandatoryfield">Mandatory fields are marked with *</span>
                        </div>
                    </div>
                    <div class="col-sm-3 text-right">
                        <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                            <sj:submit 
                                button="true" 
                                value="Reset"
                                id="canclebtn"
                                onClick="resetAllDataAdd()"
                                cssClass="btn btn-default btn-sm"
                                cssStyle="border-radius: 12px;"
                                />                          
                        </div>
                        <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">
                            <s:url action="AddTerms" var="inserturl"/>
                            <sj:submit
                                button="true"
                                value="Add"
                                onclick="set()"
                                href="%{inserturl}"
                                targets="amessage"
                                id="addbtn"
                                cssClass="btn btn-sm active" 
                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                />   

                            <!--                            <input 
                                                            type="button" 
                                                            onclick="submitformnormal()" 
                                                            value="Add Normal" 
                                                            class="btn btn-sm active"
                                                            style="border-radius: 12px;background-color:#969595;color:white;"
                                                            />-->
                        </div>
                    </div>
                </div>
            </s:form>
        </div>
        <!--////////////////end: Add Div////////////////////////////-->

        <!--//////////////start: view pend Div/////////////////////////////-->

        <div id="dialogPend" title="View Pending Terms And Conditions" style="display: none; overflow: hidden">
            <s:div id="pmessage">
                <s:actionerror theme="jquery"/>
                <s:actionmessage theme="jquery"/>
            </s:div>

            <s:form cssClass="form" id="termspend" method="post" action="#" theme="simple" >
                <div class="row row_popup">

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label >Version Number </label>
                            <s:textfield name="versionno" id="versionnopend" cssClass="form-control"  maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 .]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 .]/g,''))" disabled="true"/> 
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">    
                            <label>Status </label>
                            <s:select  id="statuspend" list="%{statusList}" 
                                       headerValue="--Select Status--" headerKey="" name="status" 
                                       listKey="statuscode" listValue="description" disabled="true"  
                                       cssClass="form-control"   /> 
                        </div>
                    </div>     
                </div>
                <div class="row row_popup">
                    <div class="col-sm-12">

                        <div class="form-group">
                            <label>Description</label> 
                            <div class="standalone-container">
                                <div  id="descriptionpendmethod" style=" height: 175px;"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </s:form>
        </div>
        <!--////////////////end: view pend Div////////////////////////////-->
        <!-- end: BODY -->
    </body>
</html>
