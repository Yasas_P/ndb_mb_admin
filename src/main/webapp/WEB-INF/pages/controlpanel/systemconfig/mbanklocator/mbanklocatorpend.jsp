<%-- 
    Document   : mbanklocatorpend
    Created on : Apr 24, 2019, 10:10:51 AM
    Author     : sivaganesan_t
--%>

<%-- 
    Document   : promotionmgtpend
    Created on : Mar 29, 2019, 10:42:23 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View Pend Promotion</title> 
    </head>
    <body>
        <s:div id="pmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="mbanklocatorpend" method="post" action="MBankLocator" theme="simple" cssClass="form" enctype="multipart/form-data"  >
            <div class="row row_popup">
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Locator ID </label>
                        <%--<s:textfield value="%{locatorid}" cssClass="form-control" name="locatorid" id="locatorid" maxLength="8" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Locator Code </label>
                        <s:textfield value="%{locatorcode}" cssClass="form-control" name="locatorcode" id="locatorcode_p" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Channel Type</label>
                        <s:select cssClass="form-control" name="channelType" id="channelType_p" list="%{channelTypeList}"   headerKey=""  headerValue="--Select Channel Type--" listKey="channelType" listValue="channelType" value="%{channelType}" disabled="true" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status_p" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" disabled="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Country Code</label>
                        <s:textfield value="%{countryCode}" cssClass="form-control" name="countryCode" id="countryCode_p" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" readonly="true" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Channel Subtype</label>
                        <%--<s:textfield value="%{channelSubType}" cssClass="form-control" name="channelSubType" id="channelSubType" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                    </div>
                </div>               -->
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Country</label>
                        <s:textfield  value="%{country}" cssClass="form-control" name="country" id="country_p" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region</label>
                        <s:textfield cssClass="form-control" name="masterRegion" id="masterRegion_p" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" readonly="true" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Region</label>
                        <%--<s:textfield value="%{region}" cssClass="form-control" name="region" id="region" maxLength="30" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion</label>
                        <s:textfield  value="%{subRegion}" cssClass="form-control" name="subRegion" id="subRegion_p" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Name</label>
                        <s:textfield value="%{name}" cssClass="form-control" name="name" id="name_p" maxLength="100"  readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Address</label>
                        <s:textfield value="%{address}" cssClass="form-control" name="address" id="address_p" maxLength="510" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Postal Code</label>
                        <s:textfield value="%{postalCode}" cssClass="form-control" name="postalCode" id="postalCode_p" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Contacts</label>
                        <s:textfield value="%{contacts}" cssClass="form-control" name="contacts" id="contacts_p" maxLength="100" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Fax</label>
                        <s:textfield value="%{fax}" cssClass="form-control" name="fax" id="fax_p" maxLength="100" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours Mon to Fri</label>
                        <s:textfield value="%{openingHsMonFri}" cssClass="form-control" name="openingHsMonFri" id="openingHsMonFri_p" maxLength="250" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours at Sat</label>
                        <s:textfield value="%{openingHsSat}" cssClass="form-control" name="openingHsSat" id="openingHsSat_p" maxLength="250" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours at Sun</label>
                        <s:textfield value="%{openingHsSunHol}" cssClass="form-control" name="openingHsSunHol" id="openingHsSunHol_p" maxLength="250" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Holiday Banking</label>
                        <s:textfield value="%{holidayBanking}" cssClass="form-control" name="holidayBanking" id="holidayBanking_p" maxLength="250" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Latitude</label>
                        <s:textfield value="%{latitude}" cssClass="form-control" name="latitude" id="latitude_p" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Longitude</label>
                        <s:textfield value="%{longitude}" cssClass="form-control" name="longitude" id="longitude_p" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Location Tag</label>
                        <s:textfield value="%{locationTag}" cssClass="form-control" name="locationTag" id="locationTag_p" maxLength="100" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Language</label>
                        <s:textfield value="%{language}" cssClass="form-control" name="language" id="language_p" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z ]/g,''))" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>ATM On Location</label>
                        <s:textfield value="%{atmOnLocation}" cssClass="form-control" name="atmOnLocation" id="atmOnLocation_p" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>CRM On Location</label>
                        <s:textfield value="%{crmOnLocation}" cssClass="form-control" name="crmOnLocation" id="crmOnLocation_p" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Pawning On Location</label>
                        <s:textfield value="%{pawningOnLocation}" cssClass="form-control" name="pawningOnLocation" id="pawningOnLocation_p" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Safe Deposit Lockers</label>
                        <s:textfield value="%{safeDepositLockers}" cssClass="form-control" name="safeDepositLockers" id="safeDepositLockers_p" maxLength="30" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Leasing Desk 365 Days</label>
                        <s:textfield value="%{leasingDesk365}" cssClass="form-control" name="leasingDesk365" id="leasingDesk365_p" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>PRV Centre</label>
                        <s:textfield value="%{prvCentre}" cssClass="form-control" name="prvCentre" id="prvCentre_p" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Branchless Banking</label>
                        <s:textfield value="%{branchlessBanking}" cssClass="form-control" name="branchlessBanking" id="branchlessBanking_p" maxLength="30" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
        </s:form>
    </body>
</html>