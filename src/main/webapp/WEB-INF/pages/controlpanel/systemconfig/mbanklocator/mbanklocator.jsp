<%-- 
    Document   : mbanklocator
    Created on : Mar 14, 2019, 3:10:12 PM
    Author     : sivaganesan_t
--%>

<%@page import="com.epic.ndb.util.varlist.CommonVarList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
            $(function () {
                $('#longitudeSearch').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#longitudeSearch")[0].value = "";
                    }
                });
                $('#latitudeSearch').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#latitudeSearch")[0].value = "";
                    }
                });
            });
            function searchPage() {
                $('#message').empty();

                var locatorcodeSearch = $('#locatorcodeSearch').val();
                var channelTypeSearch = $('#channelTypeSearch').val();
                var statusSearch = $('#statusSearch').val();
//                var countryCodeSearch = $('#countryCodeSearch').val();
//                var countrySearch = $('#countrySearch').val();
                var masterRegionSearch = $('#masterRegionSearch').val();
                var subRegionSearch = $('#subRegionSearch').val();
                var nameSearch = $('#nameSearch').val();
                var addressSearch = $('#addressSearch').val();
                var contactsSearch = $('#contactsSearch').val();
                var openingHsMonFriSearch = $('#openingHsMonFriSearch').val();
                var longitudeSearch = $('#longitudeSearch').val();
                var latitudeSearch = $('#latitudeSearch').val();
                var languageSearch = $('#languageSearch').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        locatorcodeSearch: locatorcodeSearch,
                        channelTypeSearch: channelTypeSearch,
                        statusSearch: statusSearch,
//                        countryCodeSearch: countryCodeSearch,
//                        countrySearch: countrySearch,
                        masterRegionSearch: masterRegionSearch,
                        subRegionSearch: subRegionSearch,
                        nameSearch: nameSearch,
                        addressSearch: addressSearch,
                        contactsSearch: contactsSearch,
                        openingHsMonFriSearch: openingHsMonFriSearch,
                        longitudeSearch: longitudeSearch,
                        latitudeSearch: latitudeSearch,
                        languageSearch: languageSearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
            ;

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function viewdownloadeformatter(cellvalue, options, rowObject) {
                if (rowObject.operationcode == "UPLD") {
                    return "<a href='pendCsvDownloadeMBankLocator.action?id=" + cellvalue + "' onclick='downloadmsgempty()' title='Downloade'><img class='ui-icon ui-icon-arrowthickstop-1-s' style='display:inline-table;border:none;'/></a>";
                } else {
                    return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                }
            }

            function editformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:editLocatorsInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function viewformatter(cellvalue, options, rowObject) {
                return "<a title='Edit' onClick='javascript:viewDetailLocatorsInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function deleteformatter(cellvalue, options, rowObject) {
                return "<a href='#/' title='Delete' onClick='javascript:deleteLocatorsInit(&#34;" + cellvalue  + "&#34;,&#34;" + rowObject.locatorcode + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmLocators(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectLocators(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function viewPendInit(keyval) {
                $("#viewpenddialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openviewpendtasktopage', function (event, data) {
                var $led = $("#viewpenddialog");
                $led.html("Loading..");
                $led.load("viewPendMBankLocator.action?id=" + $led.data('id'));
            });

            function confirmLocators(keyval, popvar) {
                $('#divmsg').empty();

                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function rejectLocators(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function confirmMBL(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmMBankLocator.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectMBL(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectMBankLocator.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }


            function editLocatorsInit(keyval) {
                $("#updatedialog").data('locatorid', keyval).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("detailMBankLocator.action?locatorid=" + $led.data('locatorid'));
            });

            function viewDetailLocatorsInit(keyval) {
                $("#viewdetaildialog").data('locatorid', keyval).dialog('open');
            }

            $.subscribe('opendetailviewtasktopage', function (event, data) {
                var $led = $("#viewdetaildialog");
                $led.html("Loading..");
                $led.load("viewPopupDetailMBankLocator.action?locatorid=" + $led.data('locatorid'));
            });

            function deleteLocatorsInit(keyval,locatorcode) {
                $('#divmsg').empty();

                $("#deletedialog").data('keyval', keyval).data('locatorcode', locatorcode).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete mobile bank locator ' + locatorcode + ' ?');
                return false;
            }

            function deleteLocators(keyval,locatorcode) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/deleteMBankLocator.action',
                    data: {locatorid: keyval, locatorcode: locatorcode},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#deletesuccdialog").dialog('open');
                        $("#deletesuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function resetAllData() {
                $('#locatorcodeSearch').val("");
                $('#channelTypeSearch').val("");
                $('#statusSearch').val("");
//                $('#countryCodeSearch').val("");
//                $('#countrySearch').val("");
                $('#masterRegionSearch').val("");
                $('#subRegionSearch').val("");
                $('#nameSearch').val("");
                $('#addressSearch').val("");
                $('#contactsSearch').val("");
                $('#openingHsMonFriSearch').val("");
                $('#longitudeSearch').val("");
                $('#latitudeSearch').val("");
                $('#languageSearch').val("");


                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        locatorcodeSearch: '',
                        channelTypeSearch: '',
                        statusSearch: '',
//                        countryCodeSearch: '',
//                        countrySearch: '',
                        masterRegionSearch: '',
                        subRegionSearch: '',
                        nameSearch: '',
                        addressSearch: '',
                        contactsSearch: '',
                        openingHsMonFriSearch: '',
                        longitudeSearch: '',
                        latitudeSearch: '',
                        languageSearch: '',
                        search: false
                    }
                });
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetFieldData() {

//                $('#locatorid').val("");
                $('#locatorcode').val("");
                $('#channelType').val("");
                $('#status').val("");
//                $('#channelSubType').val("");
                $('#countryCode').val("");
                $('#country').val("");
                $('#masterRegion').val("");
//                $('#region').val("");
                $('#subRegion').val("");
                $('#name').val("");
                $('#address').val("");
                $('#postalCode').val("");
                $('#contacts').val("");
                $('#fax').val("");
                $('#openingHsMonFri').val("");
                $('#openingHsSat').val("");
                $('#openingHsSunHol').val("");
                $('#holidayBanking').val("");
                $('#longitude').val("");
                $('#latitude').val("");
                $('#locationTag').val("");
                $('#language').val("");
                $('#atmOnLocation').val("");
                $('#crmOnLocation').val("");
                $('#pawningOnLocation').val("");
                $('#safeDepositLockers').val("");
                $('#leasingDesk365').val("");
                $('#prvCentre').val("");
                $('#branchlessBanking').val("");

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }

            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('mbanklocatorsearch');
                form.action = 'reportGenerateMBankLocator.action';
                form.submit();

                //    $('#view').button("disable");
                //    $('#view1').button("disable");
                $('#view2').button("disable");
            }

            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;
                //  var isGenerate = false;

                if (recors > 0 && isGenerate == false) {
                    //    $('#view').button("enable");
                    //    $('#view1').button("enable");
                    $('#view2').button("enable");
                } else {
                    //    $('#view').button("disable");
                    //    $('#view1').button("disable");
                    $('#view2').button("disable");
                }
            });
        </script>
        <title></title>
    </head>
    <body style="">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set var="vupdatebutt"><s:property value="vupdatebutt" default="true"/></s:set>
                            <s:set var="vupdatelink"><s:property value="vupdatelink" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form cssClass="form" id="mbanklocatorsearch" method="post" action="MBankLocator" theme="simple" >
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                        <div class="row row_1">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Locator Code </label>
                                                <s:textfield cssClass="form-control" name="locatorcodeSearch" id="locatorcodeSearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Channel Type</label>
                                                <s:select cssClass="form-control" name="channelTypeSearch" id="channelTypeSearch" list="%{channelTypeList}"   headerKey=""  headerValue="--Select Channel Type--" listKey="channelType" listValue="channelType" value="%{channeltype}" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Status</label>
                                                <s:select  cssClass="form-control" name="statusSearch" id="statusSearch" list="%{statusList}"   headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" value="%{status}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Master Region</label>
                                                <s:textfield cssClass="form-control" name="masterRegionSearch" id="masterRegionSearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                                            </div>
                                        </div>
                                        <!--                                        <div class="col-sm-3">
                                                                                    <div class="form-group">
                                                                                        <label>Country Code</label>
                                        <%--<s:textfield cssClass="form-control" name="countryCodeSearch" id="countryCodeSearch" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                                    </div>
                                </div>-->
                                    </div>
                                    <div class="row row_1">
                                        <!--                                        <div class="col-sm-3">
                                                                                    <div class="form-group">
                                                                                        <label>Country</label>
                                        <%--<s:textfield cssClass="form-control" name="countrySearch" id="countrySearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                                    </div>
                                </div>-->
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Subregion</label>
                                                <s:textfield cssClass="form-control" name="subRegionSearch" id="subRegionSearch" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <s:textfield cssClass="form-control" name="nameSearch" id="nameSearch" maxLength="100"  />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Address</label>
                                                <s:textfield cssClass="form-control" name="addressSearch" id="addressSearch" maxLength="510"  />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Contacts</label>
                                                <s:textfield cssClass="form-control" name="contactsSearch" id="contactsSearch" maxLength="100" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Opening Hours From Mon to Fri</label>
                                                <s:textfield cssClass="form-control" name="openingHsMonFriSearch" id="openingHsMonFriSearch" maxLength="250"  />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Latitude</label>
                                                <s:textfield cssClass="form-control" name="latitudeSearch" id="latitudeSearch"  maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Longitude</label>
                                                <s:textfield cssClass="form-control" name="longitudeSearch" id="longitudeSearch"  maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))"  />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Language</label>
                                                <s:textfield cssClass="form-control" name="languageSearch" id="languageSearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z ]/g,''))" />
                                            </div>
                                        </div>
                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                href="#"
                                                disabled="#vsearch"
                                                onClick="searchPage()"  
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div> 
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                />
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View CSV" 
                                                name="view2" 
                                                id="view2" 
                                                onClick="todocsv()" 
                                                disabled="#vgenerate"/> 
                                        </div>
                                    </div>
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-6  text-right">
                                        <div class="form-group">
                                            <s:url var="uploadurl" action="viewPopupcsvMBankLocator"/>   
                                            <sj:submit                                                      
                                                openDialog="remotedialog"
                                                button="true"
                                                href="%{uploadurl}"
                                                disabled="#vupload"
                                                value="Upload Mobile Bank Locator"
                                                id="uploadButton"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>
                                        <div class="form-group">                                               
                                            <s:url var="addurl" action="viewPopupMBankLocator"/>                                                    
                                            <sj:submit 
                                                openDialog="remotedialog"
                                                button="true"
                                                href="%{addurl}"
                                                disabled="#vadd"
                                                value="Add New Mobile Bank Locator"
                                                id="addButton" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>
                                    </div>
                                </div>

                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="remotedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Add Mobile Bank Locator"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />  
                                <!-- Start update dialog box -->
                                <sj:dialog                                     
                                    id="updatedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Update Mobile Bank Locator"
                                    onOpenTopics="openviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start detail dialog box -->
                                <sj:dialog                                     
                                    id="viewdetaildialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Mobile Bank Locator"
                                    onOpenTopics="opendetailviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteLocators($(this).data('keyval'),$(this).data('locatorcode'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete Mobile Bank Locator"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process" 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error"
                                    />
                                <!-- Start Pend view dialog box -->
                                <sj:dialog                                     
                                    id="viewpenddialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Pending Mobile Bank Locator"
                                    onOpenTopics="openviewpendtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmMBL($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectMBL($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />
                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="ListMBankLocator"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    shrinkToFit="false"
                                    > 
                                    <sjg:gridColumn name="locatorid" index="u.locatorid" title="View" width="40" align="center" formatter="viewformatter" sortable="false" />
                                    <sjg:gridColumn name="locatorid" index="u.locatorid" title="Edit" width="25" align="center" formatter="editformatter" sortable="false" hidden="#vupdatelink"/>
                                    <sjg:gridColumn name="locatorid" index="u.locatorid" title="Delete" width="40" align="center" formatter="deleteformatter" sortable="false" hidden="#vdelete"/>                                    
                                    <sjg:gridColumn name="locatorid" index="u.locatorid" title="Locator ID"  sortable="true" hidden="true"/>
                                    <sjg:gridColumn name="locatorcode" index="u.locatorcode" title="Locator Code"  sortable="true"/>
                                    <sjg:gridColumn name="channelType" index="u.channelType" title="Channel Type"  sortable="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>
                                    <sjg:gridColumn name="countryCode" index="u.countryCode" title="Country Code"  sortable="true"/>
                                    <sjg:gridColumn name="country" index="u.country" title="Country"  sortable="true"/>
                                    <sjg:gridColumn name="masterRegion" index="u.masterRegion" title="Master Region"  sortable="true"/>
                                    <sjg:gridColumn name="subRegion" index="u.subRegion" title="Subregion"  sortable="true"/>
                                    <sjg:gridColumn name="name" index="u.name" title="Name"  sortable="true"/>
                                    <sjg:gridColumn name="address" index="u.address" title="Address"  sortable="true"/>
                                    <sjg:gridColumn name="postalCode" index="u.postalCode" title="Postal Code"  sortable="true"/>
                                    <sjg:gridColumn name="contacts" index="u.contacts" title="Contacts"  sortable="true"/>
                                    <sjg:gridColumn name="fax" index="u.fax" title="Fax"  sortable="true"/>
                                    <sjg:gridColumn name="openingHsMonFri" index="u.openingHsMonFri" title="Opening Hours From Mon To Fri"  sortable="true"/>
                                    <sjg:gridColumn name="openingHsSat" index="u.openingHsSat" title="Opening Hours At Sat "  sortable="true"/>
                                    <sjg:gridColumn name="openingHsSunHol" index="u.openingHsSunHol" title="Opening Hours at Sun"  sortable="true"/>
                                    <sjg:gridColumn name="holidayBanking" index="u.holidayBanking" title="Holiday Banking"  sortable="true"/>
                                    <sjg:gridColumn name="latitude" index="u.latitude" title="Latitude"  sortable="true"/>
                                    <sjg:gridColumn name="longitude" index="u.longitude" title="Longitude"  sortable="true"/>
                                    <sjg:gridColumn name="locationTag" index="u.locationTag" title="Location Tag"  sortable="true"/>
                                    <sjg:gridColumn name="language" index="u.language" title="Language"  sortable="true"/>
                                    <sjg:gridColumn name="atmOnLocation" index="u.atmOnLocation" title="ATM On Location"  sortable="true"/>
                                    <sjg:gridColumn name="crmOnLocation" index="u.crmOnLocation" title="CRM On Location"  sortable="true"/>
                                    <sjg:gridColumn name="pawningOnLocation" index="u.pawningOnLocation" title="Pawning On Location"  sortable="true"/>
                                    <sjg:gridColumn name="safeDepositLockers" index="u.safeDepositLockers" title="Safe Deposit Lockers"  sortable="true"/>
                                    <sjg:gridColumn name="leasingDesk365" index="u.leasingDesk365" title="Leasing Desk 365 Days"  sortable="true"/>
                                    <sjg:gridColumn name="prvCentre" index="u.prvCentre" title="PRV Centre"  sortable="true"/>
                                    <sjg:gridColumn name="branchlessBanking" index="u.branchlessBanking" title="Branchless Banking"  sortable="true"/>
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="createdtime" index="u.createdtime" title="Created Date And Time"  sortable="true" />
                                    <sjg:gridColumn name="lastupdatedtime" index="u.lastupdatedtime" title="Last Updated Date And Time"  sortable="true" />

                                </sjg:grid> 
                            </div>

                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="approveListMBankLocator"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Mobile Bank Locator"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"  

                                    >
                                    <sjg:gridColumn name="id" index="id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>
                                    <sjg:gridColumn name="id" index="u.id" title="View/Download" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/> 

                                    <sjg:gridColumn name="locatorid" index="u.locatorid" title="Locator Code"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                    
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="true"/>  
                                    <sjg:gridColumn name="createtime" index="u.createtime" title="Created Date And Time"  sortable="true" />                                                                  

                                </sjg:grid>  
                            </div>  
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!-- end: BODY -->
    </body>
</html>
