<%-- 
    Document   : mbanklocatoredit
    Created on : Mar 15, 2019, 8:47:43 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Branch</title> 
        <script type="text/javascript">
            function editLocators(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findMBankLocator.action',
                    data: {locatorid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#locatoridEdit').val("");
                            $('#locatorcodeEdit').val("");
                            $('#locatoridEdit').attr('readOnly', false);
                            $('#channelTypeEdit').val("");
                            $('#statusEdit').val("");
//                            $('#channelSubTypeEdit').val("");
                            $('#countryCodeEdit').val("");
                            $('#countryEdit').val("");
                            $('#masterRegionEdit').val("");
//                            $('#regionEdit').val("");
                            $('#subRegionEdit').val("");
                            $('#nameEdit').val("");
                            $('#addressEdit').val("");
                            $('#postalCodeEdit').val("");
                            $('#contactsEdit').val("");
                            $('#faxEdit').val("");
                            $('#openingHsMonFriEdit').val("");
                            $('#openingHsSatEdit').val("");
                            $('#openingHsSunHolEdit').val("");
                            $('#holidayBankingEdit').val("");
                            $('#longitudeEdit').val("");
                            $('#latitudeEdit').val("");
                            $('#locationTagEdit').val("");
                            $('#languageEdit').val("");
                            $('#atmOnLocationEdit').val("");
                            $('#crmOnLocationEdit').val("");
                            $('#pawningOnLocationEdit').val("");
                            $('#safeDepositLockersEdit').val("");
                            $('#leasingDesk365Edit').val("");
                            $('#prvCentreEdit').val("");
                            $('#branchlessBankingEdit').val("");
                            $('#divmsg').text("");
                        } else {
                            $('#locatorcodeEdit').val(data.locatorcode);
                            $('#channelTypeEdit').val(data.channelType);
                            $('#statusEdit').val(data.status);
//                            $('#channelSubTypeEdit').val(data.channelSubType);
                            $('#countryCodeEdit').val(data.countryCode);
                            $('#countryEdit').val(data.country);
                            $('#masterRegionEdit').val(data.masterRegion);
//                            $('#regionEdit').val(data.region);
                            $('#subRegionEdit').val(data.subRegion);
                            $('#nameEdit').val(data.name);
                            $('#addressEdit').val(data.address);
                            $('#postalCodeEdit').val(data.postalCode);
                            $('#contactsEdit').val(data.contacts);
                            $('#faxEdit').val(data.fax);
                            $('#openingHsMonFriEdit').val(data.openingHsMonFri);
                            $('#openingHsSatEdit').val(data.openingHsSat);
                            $('#openingHsSunHolEdit').val(data.openingHsSunHol);
                            $('#holidayBankingEdit').val(data.holidayBanking);
                            $('#longitudeEdit').val(data.longitude);
                            $('#latitudeEdit').val(data.latitude);
                            $('#locationTagEdit').val(data.locationTag);
                            $('#languageEdit').val(data.language);
                            $('#atmOnLocationEdit').val(data.atmOnLocation);
                            $('#crmOnLocationEdit').val(data.crmOnLocation);
                            $('#pawningOnLocationEdit').val(data.pawningOnLocation);
                            $('#safeDepositLockersEdit').val(data.safeDepositLockers);
                            $('#leasingDesk365Edit').val(data.leasingDesk365);
                            $('#prvCentreEdit').val(data.prvCentre);
                            $('#branchlessBankingEdit').val(data.branchlessBanking);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var locatorid = $('#locatoridEdit').val();
                editLocators(locatorid);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="mbanklocatoredit" method="post" action="MBankLocator" theme="simple" cssClass="form" >
            <s:hidden id="locatoridEdit" name="locatorid" value="%{locatorid}" />
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Locator Code </label>
                        <s:textfield value="%{locatorcode}" cssClass="form-control" name="locatorcode" id="locatorcodeEdit" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Channel Type</label>
                        <s:select value="%{channelType}" cssClass="form-control" name="channelType" id="channelTypeEdit" list="%{channelTypeList}"   headerKey=""  headerValue="--Select Channel Type--" listKey="channelType" listValue="channelType"  />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>               
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Channel Subtype</label>
                        <%--<s:textfield value="%{channelSubType}" cssClass="form-control" name="channelSubType" id="channelSubTypeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                    </div>
                </div>               -->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Country Code</label>
                        <s:textfield value="%{countryCode}" cssClass="form-control" name="countryCode" id="countryCodeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Country</label>
                        <s:textfield  value="%{country}" cssClass="form-control" name="country" id="countryEdit" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region</label>
                        <s:textfield cssClass="form-control" name="masterRegion" id="masterRegionEdit" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Region</label>
                        <%--<s:textfield value="%{region}" cssClass="form-control" name="region" id="regionEdit" maxLength="30" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion</label>
                        <s:textfield  value="%{subRegion}" cssClass="form-control" name="subRegion" id="subRegionEdit" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Name</label>
                        <s:textfield value="%{name}" cssClass="form-control" name="name" id="nameEdit" maxLength="100"  />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Address</label>
                        <s:textfield value="%{address}" cssClass="form-control" name="address" id="addressEdit" maxLength="510"  />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Postal Code</label>
                        <s:textfield value="%{postalCode}" cssClass="form-control" name="postalCode" id="postalCodeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Contacts</label>
                        <s:textfield value="%{contacts}" cssClass="form-control" name="contacts" id="contactsEdit" maxLength="100"  />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Fax</label>
                        <s:textfield value="%{fax}" cssClass="form-control" name="fax" id="faxEdit" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^0-9- ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9- ]/g,''))" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours Mon to Fri</label>
                        <s:textfield value="%{openingHsMonFri}" cssClass="form-control" name="openingHsMonFri" id="openingHsMonFriEdit" maxLength="250" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours at Sat</label>
                        <s:textfield value="%{openingHsSat}" cssClass="form-control" name="openingHsSat" id="openingHsSatEdit" maxLength="250"  />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours at Sun</label>
                        <s:textfield value="%{openingHsSunHol}" cssClass="form-control" name="openingHsSunHol" id="openingHsSunHolEdit" maxLength="250" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Holiday Banking</label>
                        <s:textfield value="%{holidayBanking}" cssClass="form-control" name="holidayBanking" id="holidayBankingEdit" maxLength="250" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Latitude</label>
                        <s:textfield value="%{latitude}" cssClass="form-control" name="latitude" id="latitudeEdit" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onblur="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Longitude</label>
                        <s:textfield value="%{longitude}" cssClass="form-control" name="longitude" id="longitudeEdit" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onblur="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Location Tag</label>
                        <s:textfield value="%{locationTag}" cssClass="form-control" name="locationTag" id="locationTagEdit" maxLength="100" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Language</label>
                        <s:textfield value="%{language}" cssClass="form-control" name="language" id="languageEdit" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z ]/g,''))" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>ATM On Location</label>
                        <s:textfield value="%{atmOnLocation}" cssClass="form-control" name="atmOnLocation" id="atmOnLocationEdit" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>CRM On Location</label>
                        <s:textfield value="%{crmOnLocation}" cssClass="form-control" name="crmOnLocation" id="crmOnLocationEdit" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Pawning On Location</label>
                        <s:textfield value="%{pawningOnLocation}" cssClass="form-control" name="pawningOnLocation" id="pawningOnLocationEdit" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Safe Deposit Lockers</label>
                        <s:textfield value="%{safeDepositLockers}" cssClass="form-control" name="safeDepositLockers" id="safeDepositLockersEdit" maxLength="30" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Leasing Desk 365 Days</label>
                        <s:textfield value="%{leasingDesk365}" cssClass="form-control" name="leasingDesk365" id="leasingDesk365Edit" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>PRV Centre</label>
                        <s:textfield value="%{prvCentre}" cssClass="form-control" name="prvCentre" id="prvCentreEdit" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Branchless Banking</label>
                        <s:textfield value="%{branchlessBanking}" cssClass="form-control" name="branchlessBanking" id="branchlessBankingEdit" maxLength="30" />
                    </div>
                </div>
            </div>        
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="updateMBankLocator" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
