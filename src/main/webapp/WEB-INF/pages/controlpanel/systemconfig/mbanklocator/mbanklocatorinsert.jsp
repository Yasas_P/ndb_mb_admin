<%-- 
    Document   : mbanklocatorinsert
    Created on : Mar 14, 2019, 5:07:31 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Mobile Bank Locator</title> 
        <script type="text/javascript">
            $(function () {
                $('#longitude').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#longitude")[0].value = "";
                    }
                });
                $('#latitude').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#latitude")[0].value = "";
                    }
                });
            });
            
            $.subscribe('resetAddButton', function(event, data) {
                $('#amessage').empty();
//                $('#locatorid').val("");
                $('#locatorcode').val("");
                $('#channelType').val("");
                $('#status').val("");
//                $('#channelSubType').val("");
                $('#countryCode').val("");
                $('#country').val("");
                $('#masterRegion').val("");
//                $('#region').val("");
                $('#subRegion').val("");
                $('#name').val("");
                $('#address').val("");
                $('#postalCode').val("");
                $('#contacts').val("");
                $('#fax').val("");
                $('#openingHsMonFri').val("");
                $('#openingHsSat').val("");
                $('#openingHsSunHol').val("");
                $('#holidayBanking').val("");
                $('#longitude').val("");
                $('#latitude').val("");
                $('#locationTag').val("");
                $('#language').val("");
                $('#atmOnLocation').val("");
                $('#crmOnLocation').val("");
                $('#pawningOnLocation').val("");
                $('#safeDepositLockers').val("");
                $('#leasingDesk365').val("");
                $('#prvCentre').val("");
                $('#branchlessBanking').val("");
            });
        </script>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="mbanklocatoradd" method="post" action="MBankLocator" theme="simple" cssClass="form" >
            <div class="row row_popup">
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Locator ID </label>
                        <%--<s:textfield value="%{locatorid}" cssClass="form-control" name="locatorid" id="locatorid" maxLength="8" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Locator Code </label>
                        <s:textfield value="%{locatorcode}" cssClass="form-control" name="locatorcode" id="locatorcode" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Channel Type</label>
                        <s:select cssClass="form-control" name="channelType" id="channelType" list="%{channelTypeList}"   headerKey=""  headerValue="--Select Channel Type--" listKey="channelType" listValue="channelType" value="%{channeltype}" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Country Code</label>
                        <s:textfield value="%{countryCode}" cssClass="form-control" name="countryCode" id="countryCode" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Channel Subtype</label>
                        <%--<s:textfield value="%{channelSubType}" cssClass="form-control" name="channelSubType" id="channelSubType" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                    </div>
                </div>               -->
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Country</label>
                        <s:textfield  value="%{country}" cssClass="form-control" name="country" id="country" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region</label>
                        <s:textfield cssClass="form-control" name="masterRegion" id="masterRegion" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Region</label>
                        <%--<s:textfield value="%{region}" cssClass="form-control" name="region" id="region" maxLength="30" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion</label>
                        <s:textfield  value="%{subRegion}" cssClass="form-control" name="subRegion" id="subRegion" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Name</label>
                        <s:textfield value="%{name}" cssClass="form-control" name="name" id="name" maxLength="100"  />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Address</label>
                        <s:textfield value="%{address}" cssClass="form-control" name="address" id="address" maxLength="510"  />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Postal Code</label>
                        <s:textfield value="%{postalCode}" cssClass="form-control" name="postalCode" id="postalCode" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Contacts</label>
                        <s:textfield value="%{contacts}" cssClass="form-control" name="contacts" id="contacts" maxLength="100" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Fax</label>
                        <s:textfield value="%{fax}" cssClass="form-control" name="fax" id="fax" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^0-9- ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9- ]/g,''))" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours Mon to Fri</label>
                        <s:textfield value="%{openingHsMonFri}" cssClass="form-control" name="openingHsMonFri" id="openingHsMonFri" maxLength="250" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours at Sat</label>
                        <s:textfield value="%{openingHsSat}" cssClass="form-control" name="openingHsSat" id="openingHsSat" maxLength="250"  />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours at Sun</label>
                        <s:textfield value="%{openingHsSunHol}" cssClass="form-control" name="openingHsSunHol" id="openingHsSunHol" maxLength="250" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Holiday Banking</label>
                        <s:textfield value="%{holidayBanking}" cssClass="form-control" name="holidayBanking" id="holidayBanking" maxLength="250" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Latitude</label>
                        <s:textfield value="%{latitude}" cssClass="form-control" name="latitude" id="latitude" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onblur="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Longitude</label>
                        <s:textfield value="%{longitude}" cssClass="form-control" name="longitude" id="longitude" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onblur="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Location Tag</label>
                        <s:textfield value="%{locationTag}" cssClass="form-control" name="locationTag" id="locationTag" maxLength="100" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Language</label>
                        <s:textfield value="%{language}" cssClass="form-control" name="language" id="language" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z ]/g,''))" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>ATM On Location</label>
                        <s:textfield value="%{atmOnLocation}" cssClass="form-control" name="atmOnLocation" id="atmOnLocation" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>CRM On Location</label>
                        <s:textfield value="%{crmOnLocation}" cssClass="form-control" name="crmOnLocation" id="crmOnLocation" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Pawning On Location</label>
                        <s:textfield value="%{pawningOnLocation}" cssClass="form-control" name="pawningOnLocation" id="pawningOnLocation" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Safe Deposit Lockers</label>
                        <s:textfield value="%{safeDepositLockers}" cssClass="form-control" name="safeDepositLockers" id="safeDepositLockers" maxLength="30" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Leasing Desk 365 Days</label>
                        <s:textfield value="%{leasingDesk365}" cssClass="form-control" name="leasingDesk365" id="leasingDesk365" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>PRV Centre</label>
                        <s:textfield value="%{prvCentre}" cssClass="form-control" name="prvCentre" id="prvCentre" maxLength="30" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Branchless Banking</label>
                        <s:textfield value="%{branchlessBanking}" cssClass="form-control" name="branchlessBanking" id="branchlessBanking" maxLength="30" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="addMBankLocator" var="inserturl"/>
                        <sj:submit
                            button="true"
                            value="Add"
                            href="%{inserturl}"
                            onClickTopics=""
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>
                   
                </div>
            </div>
        </s:form>
    </body>
</html>
