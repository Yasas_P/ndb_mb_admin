<%-- 
    Document   : mbanklocatordetail
    Created on : Aug 12, 2019, 2:54:47 PM
    Author     : sivaganesan_t
--%>

<%@page import="com.epic.ndb.util.varlist.CommonVarList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detail Mobile Bank Locator</title>
    </head>
    <body>
        <s:div id="amessagedetail">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="mbanklocatordetail" method="post" action="MBankLocator" theme="simple" cssClass="form" >
            <%--<s:hidden id="locatoridEdit" name="locatorid" value="%{locatorid}" />--%>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Locator Code </label>
                        <s:textfield value="%{locatorcode}" cssClass="form-control" name="locatorcode" id="locatorcodeDetail" maxLength="20" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Channel Type</label>
                        <s:select value="%{channelType}" cssClass="form-control" name="channelType" id="channelTypeDetail" list="%{channelTypeList}"   headerKey=""  headerValue="--Select Channel Type--" listKey="channelType" listValue="channelType" disabled="true" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusDetail" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" disabled="true" />
                    </div>
                </div>               
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Channel Subtype</label>
                        <%--<s:textfield value="%{channelSubType}" cssClass="form-control" name="channelSubType" id="channelSubTypeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                    </div>
                </div>               -->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Country Code</label>
                        <s:textfield value="%{countryCode}" cssClass="form-control" name="countryCode" id="countryCodeDetail" maxLength="10" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Country</label>
                        <s:textfield  value="%{country}" cssClass="form-control" name="country" id="countryDetail" maxLength="20" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region</label>
                        <s:textfield cssClass="form-control" name="masterRegion" id="masterRegionDetail" maxLength="20" readonly="true" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Region</label>
                        <%--<s:textfield value="%{region}" cssClass="form-control" name="region" id="regionEdit" maxLength="30" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion</label>
                        <s:textfield  value="%{subRegion}" cssClass="form-control" name="subRegion" id="subRegionDetail" maxLength="100" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Name</label>
                        <s:textfield value="%{name}" cssClass="form-control" name="name" id="nameDetail" maxLength="100" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Address</label>
                        <s:textfield value="%{address}" cssClass="form-control" name="address" id="addressDetail" maxLength="510" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Postal Code</label>
                        <s:textfield value="%{postalCode}" cssClass="form-control" name="postalCode" id="postalCodeDetail" maxLength="10" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Contacts</label>
                        <s:textfield value="%{contacts}" cssClass="form-control" name="contacts" id="contactsDetail" maxLength="100" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Fax</label>
                        <s:textfield value="%{fax}" cssClass="form-control" name="fax" id="faxDetail" maxLength="100" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours Mon to Fri</label>
                        <s:textfield value="%{openingHsMonFri}" cssClass="form-control" name="openingHsMonFri" id="openingHsMonFriDetail" maxLength="250" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours at Sat</label>
                        <s:textfield value="%{openingHsSat}" cssClass="form-control" name="openingHsSat" id="openingHsSatDetail" maxLength="250" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Opening Hours at Sun</label>
                        <s:textfield value="%{openingHsSunHol}" cssClass="form-control" name="openingHsSunHol" id="openingHsSunHolDetail" maxLength="250" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Holiday Banking</label>
                        <s:textfield value="%{holidayBanking}" cssClass="form-control" name="holidayBanking" id="holidayBankingDetail" maxLength="250" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Latitude</label>
                        <s:textfield value="%{latitude}" cssClass="form-control" name="latitude" id="latitudeDetail" maxLength="20" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Longitude</label>
                        <s:textfield value="%{longitude}" cssClass="form-control" name="longitude" id="longitudeDetail" maxLength="20" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Location Tag</label>
                        <s:textfield value="%{locationTag}" cssClass="form-control" name="locationTag" id="locationTagDetail" maxLength="100" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Language</label>
                        <s:textfield value="%{language}" cssClass="form-control" name="language" id="languageDetail" maxLength="20" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>ATM On Location</label>
                        <s:textfield value="%{atmOnLocation}" cssClass="form-control" name="atmOnLocation" id="atmOnLocationDetail" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>CRM On Location</label>
                        <s:textfield value="%{crmOnLocation}" cssClass="form-control" name="crmOnLocation" id="crmOnLocationDetail" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Pawning On Location</label>
                        <s:textfield value="%{pawningOnLocation}" cssClass="form-control" name="pawningOnLocation" id="pawningOnLocationDetail" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Safe Deposit Lockers</label>
                        <s:textfield value="%{safeDepositLockers}" cssClass="form-control" name="safeDepositLockers" id="safeDepositLockersDetail" maxLength="30" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Leasing Desk 365 Days</label>
                        <s:textfield value="%{leasingDesk365}" cssClass="form-control" name="leasingDesk365" id="leasingDesk365Detail" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>PRV Centre</label>
                        <s:textfield value="%{prvCentre}" cssClass="form-control" name="prvCentre" id="prvCentreDetail" maxLength="30" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Branchless Banking</label>
                        <s:textfield value="%{branchlessBanking}" cssClass="form-control" name="branchlessBanking" id="branchlessBankingDetail" maxLength="30" readonly="true" />
                    </div>
                </div>
            </div>        
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
