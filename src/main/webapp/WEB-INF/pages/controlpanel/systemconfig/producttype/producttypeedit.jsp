<%-- 
    Document   : producttypeedit
    Created on : Apr 10, 2019, 4:30:37 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Branch</title> 
        <script type="text/javascript">
            function editProductType(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findProductType.action',
                    data: {productType: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#productTypeEdit').val("");
                            $('#productTypeEdit').attr('readOnly', false);
                            $('#productNameEdit').val("");
                            $('#statusEdit').val("");
                            $('#productcategoryEdit').val("");
                            $('#divmsg').text("");
                        } else {
                            $('#productNameEdit').val(data.productName);
                            $('#statusEdit').val(data.status);
                            $('#productcategoryEdit').val(data.productcategory);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var productType = $('#productTypeEdit').val();
                editProductType(productType);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="producttypedit" method="post" action="ProductType" theme="simple" cssClass="form" >
            <%--<s:hidden id="bankcodeEdit" name="bankcode" value="%{bankcode}" />--%>
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Product Type </label>
                        <s:textfield value="%{productType}" cssClass="form-control" name="productType" id="productTypeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Product Name</label>
                        <s:textfield value="%{productName}" cssClass="form-control" name="productName" id="productNameEdit" maxLength="100" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>               
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Product Category</label>
                        <s:select value="%{productcategory}" cssClass="form-control" id="productcategoryEdit" list="%{productcategoryList}"  name="productcategory" headerKey=""  headerValue="--Select Product Category--" listKey="code" listValue="description" />
                    </div>
                </div>                
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="updateProductType" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
