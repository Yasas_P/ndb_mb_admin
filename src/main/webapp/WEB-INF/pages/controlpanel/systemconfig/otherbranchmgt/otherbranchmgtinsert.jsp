<%-- 
    Document   : otherbranchmgtinsert
    Created on : Apr 3, 2019, 8:02:23 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Mobile Bank Locator</title> 
        <script type="text/javascript">
            $.subscribe('resetAddButton', function(event, data) {
                $('#amessage').empty();
                $('#bank').val("");
                $('#branchcode').val("");
                $('#branchname').val("");
                $('#status').val("");
            });
        </script>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="otherbranchmgtadd" method="post" action="OtherBranchMgt" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bank </label>
                        <s:select cssClass="form-control" name="bank" id="bank" list="%{bankList}"   headerKey=""  headerValue="--Select Bank--" listKey="bankcode" listValue="bankname" value="%{bank}" disabled="false"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Branch Code </label>
                        <s:textfield value="%{bankcode}" cssClass="form-control" name="branchcode" id="branchcode" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Branch Name</label>
                        <s:textfield value="%{bankname}" cssClass="form-control" name="branchname" id="branchname" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="addOtherBranchMgt" var="inserturl"/>
                        <sj:submit
                            button="true"
                            value="Add"
                            href="%{inserturl}"
                            onClickTopics=""
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>
                   
                </div>
            </div>
        </s:form>
    </body>
</html>
