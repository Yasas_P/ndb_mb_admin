<%-- 
    Document   : otherbranchmgtedit
    Created on : Apr 3, 2019, 8:10:37 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Branch</title> 
        <script type="text/javascript">
            function editCardCenter(bank,branchcode) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findOtherBranchMgt.action',
                    data: {bank: bank,branchcode:branchcode},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#bankEdit').val("");
                            $('#bankEdit').attr('disabled', false);
                            $('#branchcodeEdit').val("");
                            $('#branchcodeEdit').attr('readOnly', false);
                            $('#branchnameEdit').val("");
                            $('#statusEdit').val("");
                            $('#divmsg').text("");
                        } else {
                            $('#branchnameEdit').val(data.branchname);
                            $('#statusEdit').val(data.status);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var bank = $('#bankhiddenEdit').val();
                var branchcode = $('#branchcodeEdit').val();
                editCardCenter(bank,branchcode);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="otherbranchmgtedit" method="post" action="OtherBranchMgt" theme="simple" cssClass="form" >
            <s:hidden id="bankhiddenEdit" name="bankhidden" value="%{bank}" />
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
            
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bank </label>
                        <s:select cssClass="form-control" name="bank" id="bankEdit" list="%{bankList}"   headerKey=""  headerValue="--Select Bank--" listKey="bankcode" listValue="bankname" value="%{bank}" disabled="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Branch Code </label>
                        <s:textfield value="%{branchcode}" cssClass="form-control" name="branchcode" id="branchcodeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Branch Name</label>
                        <s:textfield value="%{branchname}" cssClass="form-control" name="branchname" id="branchnameEdit" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>                
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="updateOtherBranchMgt" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
