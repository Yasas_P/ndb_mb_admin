<%-- 
    Document   : commonconfigedit
    Created on : Oct 1, 2018, 4:43:42 PM
    Author     : jayathissa_d
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Edit Common Configuration</title> 
        <script type="text/javascript">
            function editCommonConfig(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/FindCommonCon.action',
                    data: {paramCode: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#paramCodeEdit').val("");
                            $('#paramCodeEdit').attr('readOnly', false);
                            $("#paramCodeEdit").css("color", "black");
                            $('#descriptionEdit').val("");
                            $('#paramvalEdit').val("");
                            $('#divmsg').text("");
                        }
                        else {
                            $('#paramCodeEdit').val(data.paramCode);
                            $('#paramCodeEdit').attr('readOnly', true);
                            $("#paramCodeEdit").css("color", "#858585");
                            $('#descriptionEdit').val(data.description);
                            $('#paramvalEdit').val(data.paramVal);
                        }
                    },
                    error: function(data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var paramCode = $('#paramCodeEdit').val();
                editCommonConfig(paramCode);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="commonconfigedit" method="post" action="CommonCon" theme="simple" cssClass="form" >
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Parameter Code</label>
                        <s:textfield value="%{paramCode}" cssClass="form-control" name="paramCode" id="paramCodeEdit" maxLength="16" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield value="%{description}" cssClass="form-control" name="description" id="descriptionEdit" maxLength="120" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Parameter Value</label>
                       <s:textfield value="%{paramVal}" cssClass="form-control" name="paramVal" id="paramvalEdit" maxLength="512"/>
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    
                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="UpdateCommonCon" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
