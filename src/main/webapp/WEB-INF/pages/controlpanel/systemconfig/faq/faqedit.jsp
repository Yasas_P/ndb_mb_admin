<%-- 
    Document   : faqedit
    Created on : Aug 20, 2019, 8:42:38 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update FAQ</title> 
        <script type="text/javascript">
            function editFaq(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/FindFaq.action',
                    data: {id: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#idEdit').val("");
                            $('#idEdit').attr('readOnly', false);
                            $("#idEdit").css("color", "black");
                            $('#questionEdit').val("");
                            $('#answerEdit').val("");
                            $('#statusEdit').val("");
                            $('#divmsg').text("");
                        }
                        else {
                            $('#idEdit').val(data.id);
                            $('#idEdit').attr('readOnly', true);
                            $("#idEdit").css("color", "#858585");
                            $('#questionEdit').val(data.question);
                            $('#answerEdit').val(data.answer);
                            $('#statusEdit').val(data.status);
                        }
                    },
                    error: function(data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var code = $('#idEdit').val();
                editFaq(code);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="faqedit" method="post" action="Faq" theme="simple" cssClass="form" >
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>ID </label>
                        <s:textfield value="%{id}" cssClass="form-control" name="id" id="idEdit" maxLength="150" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Question</label>
                        <s:textfield value="%{question}" cssClass="form-control" name="question" id="questionEdit" maxLength="1000" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Answer</label>
                         <s:textarea value="%{answer}" style="margin-bottom: 0px; word-break: break-all;background-color: white;" cssClass="form-control" name="answer" id="answerEdit" maxLength="1000" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    
                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="UpdateFaq" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>

