<%-- 
    Document   : faqpend
    Created on : Aug 20, 2019, 3:28:30 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Pending FAQ</title> 
    </head>
    <body>
        <s:div id="pendmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="faqpend" method="post" action="Faq" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>ID </label>
                        <s:textfield value="%{id}" cssClass="form-control" name="id" id="id_pend" maxLength="8" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Task </label>
                        <s:textfield value="%{id}" cssClass="form-control" name="Task" id="task_pend" maxLength="8"  readonly="true" />
                    </div>
                </div>
            </div>
             <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Field(s)</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Old Value</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>New Value</label>
                    </div>
                </div>
            </div>        
            <div class="row row_popup">
                <div class="col-sm-2" >
                    <div class="form-group">
                        <label>Status</label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:select value="%{oldFaq.status}" cssClass="form-control" id="status_old_pend" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" disabled="true" />
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:select value="%{newFaq.status}" cssClass="form-control" id="status_new_pend" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" disabled="true" />
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-2" >
                    <div class="form-group">
                        <label>Question</label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:textarea value="%{oldFaq.question}" cssClass="form-control" style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="question" id="question_old_pend" maxLength="1000" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:textarea value="%{newFaq.question}" cssClass="form-control"  style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="question" id="question_new_pend" maxLength="1000" readonly="true"/>
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-2" >
                    <div class="form-group">
                        <label>Answer</label>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:textarea value="%{oldFaq.answer}" cssClass="form-control" style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="answer" id="answer_old_pend" maxLength="1000" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:textarea value="%{newFaq.answer}" cssClass="form-control" style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="answer" id="answer_new_pend" maxLength="1000" readonly="true" />
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
        </s:form>
    </body>
</html>

