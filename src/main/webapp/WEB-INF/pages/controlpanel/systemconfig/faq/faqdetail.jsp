<%-- 
    Document   : faqdetail
    Created on : Aug 20, 2019, 2:44:37 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View FAQ</title> 
    </head>
    <body>
        <s:div id="detailmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="faqdetail" method="post" action="Faq" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>ID </label>
                        <s:textfield value="%{id}" cssClass="form-control" name="id" id="id_detail" maxLength="8" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status_detail" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" disabled="true" />
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Question</label>
                        <s:textfield value="%{question}" cssClass="form-control" name="question" id="question_detail" maxLength="1000" readonly="true"/>
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-12">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Answer</label>
                        <s:textarea value="%{answer}" cssClass="form-control" style="margin-bottom: 0px; word-break: break-all;background-color: white;" name="answer" id="answer_detail" maxLength="1000" readonly="true" />
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
