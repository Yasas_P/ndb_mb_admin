<%-- 
    Document   : branchedit
    Created on : Oct 9, 2018, 1:40:48 PM
    Author     : jayathissa_d
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Branch</title> 
        <script type="text/javascript">
            function editBranch(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/FindBranch.action',
                    data: {branchCode: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#branchCodeEdit').val("");
                            $('#branchCodeEdit').attr('readOnly', false);
                            $("#branchCodeEdit").css("color", "black");
                            $('#branchNameEdit').val("");
                            $('#statusEdit').val("");
                            $('#managernameEdit').val("");
                            $('#mobileEdit').val("");
                            $('#divmsg').text("");
                        }
                        else {
                            $('#branchCodeEdit').val(data.branchCode);
                            $('#branchCodeEdit').attr('readOnly', true);
                            $("#branchCodeEdit").css("color", "#858585");
                            $('#branchNameEdit').val(data.branchName);
                            $('#managernameEdit').val(data.managername);
                            $('#mobileEdit').val(data.mobile);
                            $('#statusEdit').val(data.status);
                        }
                    },
                    error: function(data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var branchcode = $('#branchCodeEdit').val();
                editBranch(branchcode);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="branchedit" method="post" action="Branch" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Branch Code</label>
                        <s:textfield value="%{branchCode}" cssClass="form-control" name="branchCode" id="branchCodeEdit" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Branch Name</label>
                        <s:textfield value="%{branchName}" cssClass="form-control" name="branchName" id="branchNameEdit" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>                
            </div> 
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Manager Name</label>
                        <s:textfield value="%{managername}" cssClass="form-control" name="managername" id="managernameEdit" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Mobile Number</label>
                        <s:textfield value="%{mobile}" cssClass="form-control" name="mobile" id="mobileEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^0-9+]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9+]/g,''))"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    
                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="UpdateBranch" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
