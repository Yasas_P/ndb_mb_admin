<%-- 
    Document   : cardcenteredit
    Created on : Mar 18, 2019, 1:25:15 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Branch</title> 
        <script type="text/javascript">
            function editCardCenter(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findCardCenter.action',
                    data: {bankcode: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#bankcodeEdit').val("");
                            $('#bankcodeEdit').attr('readOnly', false);
                            $('#banknameEdit').val("");
                            $('#statusEdit').val("");
                            $('#fundTranModeEdit').val("");
                            $('#shortCodeEdit').val("");
                            $('#cardCodeCenterEdit').val("");
                            $('#divmsg').text("");
                        } else {
                            $('#banknameEdit').val(data.bankname);
                            $('#statusEdit').val(data.status);
                            $('#fundTranModeEdit').val(data.fundTranMode);
                            $('#shortCodeEdit').val(data.shortCode);
                            $('#cardCodeCenterEdit').val(data.cardCodeCenter);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var bankcode = $('#bankcodeEdit').val();
                editCardCenter(bankcode);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="cardcenteredit" method="post" action="CardCenter" theme="simple" cssClass="form" >
            <%--<s:hidden id="bankcodeEdit" name="bankcode" value="%{bankcode}" />--%>
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>

                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Bank Code </label>
                        <s:textfield value="%{bankcode}" cssClass="form-control" name="bankcode" id="bankcodeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bank Name</label>
                        <s:textfield value="%{bankname}" cssClass="form-control" name="bankname" id="banknameEdit" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Fund Tran Mode</label>
                        <s:select  cssClass="form-control" name="fundTranMode" id="fundTranModeEdit" list="%{fundTranModeMap}"   headerKey=""  headerValue="--Select Fund Tran Mode--" listKey="key" listValue="value" value="%{fundTranMode}" disabled="false"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Short Code</label>
                        <s:textfield value="%{shortCode}" cssClass="form-control" name="shortCode" id="shortCodeEdit" maxLength="4" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" />
                    </div>
                </div>               
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Card Code Center</label>
                        <s:textfield value="%{cardCodeCenter}" cssClass="form-control" name="cardCodeCenter" id="cardCodeCenterEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="updateCardCenter" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>

