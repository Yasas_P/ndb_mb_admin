<%-- 
    Document   : cardcenterinsert
    Created on : Mar 18, 2019, 1:19:11 PM
    Author     : sivaganesan_t
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Mobile Bank Locator</title> 
        <script type="text/javascript">
            $.subscribe('resetAddButton', function(event, data) {
                $('#amessage').empty();
                $('#bankcode').val("");
                $('#bankname').val("");
                $('#status').val("");
                $('#fundTranMode').val("");
                $('#shortCode').val("");
                $('#cardCodeCenter').val("");
            });
        </script>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="cardcenteradd" method="post" action="CardCenter" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bank Code </label>
                        <s:textfield value="%{bankcode}" cssClass="form-control" name="bankcode" id="bankcode" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bank Name</label>
                        <s:textfield value="%{bankname}" cssClass="form-control" name="bankname" id="bankname" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Fund Tran Mode</label>
                        <s:select  cssClass="form-control" name="fundTranMode" id="fundTranMode" list="%{fundTranModeMap}"   headerKey=""  headerValue="--Select Fund Tran Mode--" listKey="key" listValue="value" value="%{fundTranMode}" disabled="false"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">        
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Short Code</label>
                        <s:textfield value="%{shortCode}" cssClass="form-control" name="shortCode" id="shortCode" maxLength="4" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" />
                    </div>
                </div>               
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Card Code Center</label>
                        <s:textfield value="%{cardCodeCenter}" cssClass="form-control" name="cardCodeCenter" id="cardCodeCenter" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="addCardCenter" var="inserturl"/>
                        <sj:submit
                            button="true"
                            value="Add"
                            href="%{inserturl}"
                            onClickTopics=""
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>
                   
                </div>
            </div>
        </s:form>
    </body>
</html>

