<%-- 
    Document   : feeschargeinsert
    Created on : Apr 9, 2019, 5:23:37 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Mobile Bank Locator</title> 
        <script type="text/javascript">
            $.subscribe('resetAddButton', function(event, data) {
                $('#amessage').empty();
                $('#chargeCode').val("");
                $('#shortName').val("");
                $('#status').val("");
                $('#description').val("");
                $('#chargeAmount').val("");
                $('#transferType').val("");
            });
        </script>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="feeschargeadd" method="post" action="FeesCharge" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Charge Code </label>
                        <s:textfield cssClass="form-control" name="chargeCode" id="chargeCode" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Transfer Type </label>
                        <s:select cssClass="form-control" name="transferType" id="transferType" list="%{transferTypeList}"   headerKey=""  headerValue="--Select Transfer Type--" listKey="transferId" listValue="description" value="%{transferType}" disabled="false"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Short Name</label>
                        <s:textfield cssClass="form-control" name="shortName" id="shortName" maxLength="50" nkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>            
            </div>
            <div class="row row_popup">               
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield cssClass="form-control" name="description" id="description" maxLength="200" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>   
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Charge Amount</label>
                        <s:textfield cssClass="form-control" name="chargeAmount" id="chargeAmount"  maxLength="8" onkeyup="validateCurrency($(this));" onmouseout="validateCurrency($(this));" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="addFeesCharge" var="inserturl"/>
                        <sj:submit
                            button="true"
                            value="Add"
                            href="%{inserturl}"
                            onClickTopics=""
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
