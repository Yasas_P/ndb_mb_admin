<%-- 
    Document   : feeschargeedit
    Created on : Apr 10, 2019, 8:21:24 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Branch</title> 
        <script type="text/javascript">
            function editFeesCharge(keyval, transferType) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findFeesCharge.action',
                    data: {chargeCode: keyval, transferType: transferType},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#chargeCodeEdit').val("");
                            $('#chargeCodeEdit').attr('readOnly', false);
                            $('#shortNameEdit').val("");
                            $('#statusEdit').val("");
                            $('#descriptionEdit').val("");
                            $('#chargeAmountEdit').val("");
                            $('#transferTypeEdit').val("");
                            $('#transferTypeEdit').attr('disabled', false);
                            $('#divmsg').text("");
                        } else {
                            $('#shortNameEdit').val(data.shortName);
                            $('#statusEdit').val(data.status);
                            $('#descriptionEdit').val(data.description);
                            $('#chargeAmountEdit').val(data.chargeAmount);
//                            $('#transferTypeEdit').val(data.transferType);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var transferType = $('#transferTypeHiddenEdit').val();
                var chargeCode = $('#chargeCodeEdit').val();
                editFeesCharge(chargeCode, transferType);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="feeschargeedit" method="post" action="FeesCharge" theme="simple" cssClass="form" >
            <%--<s:hidden id="bankcodeEdit" name="bankcode" value="%{bankcode}" />--%>
            <s:hidden id="transferTypeHiddenEdit" name="transferTypeHidden" value="%{transferType}" />
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>

                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Charge Code </label>
                        <s:textfield cssClass="form-control" value="%{chargeCode}" name="chargeCode" id="chargeCodeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Transfer Type </label>
                        <s:select cssClass="form-control" name="transferType" id="transferTypeEdit" list="%{transferTypeList}"   headerKey=""  headerValue="--Select Transfer Type--" listKey="transferId" listValue="description" value="%{transferType}" disabled="true"/>
                    </div>
                </div>    
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Short Name</label>
                        <s:textfield cssClass="form-control" value="%{shortName}" name="shortName" id="shortNameEdit" maxLength="50" nkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control"  id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>                
            </div>
            <div class="row row_popup">            
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield cssClass="form-control" value="%{description}" name="description" id="descriptionEdit" maxLength="200" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>  
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Charge Amount</label>
                        <s:textfield cssClass="form-control" value="%{chargeAmount}" name="chargeAmount" id="chargeAmountEdit"  maxLength="8" onkeyup="validateCurrency($(this));" onmouseout="validateCurrency($(this));" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="updateFeesCharge" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
