<%-- 
    Document   : feeschargepend
    Created on : Aug 7, 2019, 11:52:21 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View Pend Fee Charge</title> 
    </head>
    <body>
        <s:div id="pmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="feeschargepend" method="post" action="FeesCharge" theme="simple" cssClass="form" enctype="multipart/form-data"  >
            <s:hidden id="taskCodePendView" name="taskCode" value="%{taskCode}" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Charge Code</label>
                        <s:textfield value="%{chargeCode}" cssClass="form-control" name="chargeCode" id="chargeCode_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Transfer Type</label>
                        <s:textfield value="%{transferType}" cssClass="form-control"  name="transferType" id="transferType_p" readonly="true"  />
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Task</label>
                        <s:textfield value="%{taskDescription}" cssClass="form-control" name="task" id="task_p"  readonly="true" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Field(s)</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Old Value</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>New Value</label>
                    </div>
                </div>
            </div>           
            <div class="row row_popup" style="height: 16px">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Short Name</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{shortNameOld}" cssClass="form-control"  name="shortNameOld" id="shortNameOld_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{shortName}" cssClass="form-control"  name="shortName" id="shortName_p" readonly="true"  />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Status</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield  value="%{statusOld}" cssClass="form-control"  name="statusOld" id="statusOld_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{status}" cssClass="form-control" name="status" id="status_p" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Description</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{descriptionOld}" cssClass="form-control" name="descriptionOld" id="descriptionOld_p" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{description}"  cssClass="form-control" name="description" id="description_p" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Charge Amount</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{chargeAmountOld}" cssClass="form-control"  name="chargeAmountOld" id="chargeAmountOld_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{chargeAmount}" cssClass="form-control"  name="chargeAmount" id="chargeAmount_p" readonly="true" />
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>