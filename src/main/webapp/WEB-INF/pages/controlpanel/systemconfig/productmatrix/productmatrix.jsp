<%-- 
    Document   : productmatrix
    Created on : Apr 11, 2019, 11:43:21 AM
    Author     : sivaganesan_t
--%>

<%@page import="com.epic.ndb.util.varlist.CommonVarList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
//            $.subscribe('onclicksearch', function (event, data) {
            function search(){
                $('#message').empty();

                var debitProductTypeSearch = $('#debitProductTypeSearch').val();
                var creditProductTypeSearch = $('#creditProductTypeSearch').val();
                var statusSearch = $('#statusSearch').val();
//                var debitCurrencySearch = $('#debitCurrencySearch').val();
                
                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        debitProductTypeSearch: debitProductTypeSearch,
                        creditProductTypeSearch: creditProductTypeSearch,
                        statusSearch: statusSearch,
//                        debitCurrencySearch: debitCurrencySearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
//            );

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function editformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:editProductMatrixInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.creditProductType + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function deleteformatter(cellvalue, options, rowObject) {
                return "<a href='#/' title='Delete' onClick='javascript:deleteProductMatrixInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.creditProductType + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            
            function viewformatter(cellvalue, options, rowObject) {
                return "<a title='Edit' onClick='javascript:viewDetailProductMatrixInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.creditProductType + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmProductMatrix(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectProductMatrix(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            function confirmProductMatrix(keyval, popvar) {
                $('#divmsg').empty();
                
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');
                
                return false;
            }

            function rejectProductMatrix(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');
                
                return false;
            }

            function confirmPM(keyval,remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmProductMatrix.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectPM(keyval,remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectProductMatrix.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }

            function viewdownloadeformatter(cellvalue, options, rowObject) {
                if (rowObject.operationcode == "UPLD") {
                    return "<a href='pendCsvDownloadeProductMatrix.action?id=" + cellvalue + "' onclick='downloadmsgempty()' title='Downloade'><img class='ui-icon ui-icon-arrowthickstop-1-s' style='display:inline-table;border:none;'/></a>";
                }else{
                    return "--";
                }
            }

            function editProductMatrixInit(debitProductType,creditProductType) {
                $("#updatedialog").data('debitProductType', debitProductType).data('creditProductType',creditProductType).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("detailProductMatrix.action?debitProductType=" + $led.data('debitProductType') + "&creditProductType=" + $led.data('creditProductType'));
            });
            
            function viewDetailProductMatrixInit(debitProductType,creditProductType) {
                $("#viewdetaildialog").data('debitProductType', debitProductType).data('creditProductType',creditProductType).dialog('open');
            }

            $.subscribe('opendetailviewtasktopage', function (event, data) {
                var $led = $("#viewdetaildialog");
                $led.html("Loading..");
                $led.load("viewPopupDetailProductMatrix.action?debitProductType=" + $led.data('debitProductType') + "&creditProductType=" + $led.data('creditProductType'));
            });

            function deleteProductMatrixInit(debitProductType,creditProductType) {
                $('#divmsg').empty();

                $("#deletedialog").data('debitProductType', debitProductType).data('creditProductType',creditProductType).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete product matrix (debit product type : ' + debitProductType+ ', credit product type: ' +creditProductType +') ?');
                return false;
            }

            function deleteProductMatrix(debitProductType,creditProductType) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/deleteProductMatrix.action',
                    data: {debitProductType: debitProductType,creditProductType:creditProductType},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#deletesuccdialog").dialog('open');
                        $("#deletesuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function resetAllData() {
                $('#debitProductTypeSearch').val("");
                $('#creditProductTypeSearch').val("");
                $('#statusSearch').val("");
//                $('#debitCurrencySearch').val("");


                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        debitProductTypeSearch: '',
                        creditProductTypeSearch: '',
                        statusSearch: '',
//                        debitCurrencySearch: '',
                        search: false
                    }
                });
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetFieldData() {

                $('#debitProductType').val("");
                $('#creditProductType').val("");
                $('#status').val("");
//                $('#debitCurrency').val("");
                if (typeof toleftall === "function") { 
                    toleftall();
                }
                if (typeof toleftallDebit === "function") { 
                    toleftallDebit();
                }

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }

            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('productmatrixsearch');
                form.action = 'reportGenerateProductMatrix.action';
                form.submit();

                //    $('#view').button("disable");
                //    $('#view1').button("disable");
                $('#view2').button("disable");
            }
            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;
                //  var isGenerate = false;

                if (recors > 0 && isGenerate == false) {
                    //    $('#view').button("enable");
                    //    $('#view1').button("enable");
                    $('#view2').button("enable");
                } else {
                    //    $('#view').button("disable");
                    //    $('#view1').button("disable");
                    $('#view2').button("disable");
                }
            });

        </script>
        <title></title>
    </head>
    <body style="">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set var="vupdatebutt"><s:property value="vupdatebutt" default="true"/></s:set>
                            <s:set var="vupdatelink"><s:property value="vupdatelink" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>
                            
                                <div id="formstyle">
                                <s:form cssClass="form" id="productmatrixsearch" method="post" action="ProductMatrix" theme="simple" >
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Debit Product Type</label>
                                                <s:select cssClass="form-control" name="debitProductTypeSearch" id="debitProductTypeSearch" list="%{productTypeList}"   headerKey=""  headerValue="--Select Debit Product Type--" listKey="productType" listValue="productName" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Credit Product Type</label>
                                                <s:select cssClass="form-control" name="creditProductTypeSearch" id="creditProductTypeSearch" list="%{productTypeList}"   headerKey=""  headerValue="--Select Credit Product Type--" listKey="productType" listValue="productName" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Status</label>
                                                <s:select  cssClass="form-control" name="statusSearch" id="statusSearch" list="%{statusList}"   headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" value="%{status}" disabled="false"/>
                                            </div>
                                        </div>
<!--                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Debit Currency</label>
                                                <%--<s:select  cssClass="form-control" name="debitCurrencySearch" id="debitCurrencySearch" list="%{productCurrencyList}"   headerKey=""  headerValue="--Select Debit Currency--" listKey="currencyCode" listValue="description" disabled="false"/>--%>
                                            </div>
                                        </div>-->
                                    </div>
                                </s:form>
                                    <div class="row row_1 form-inline">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <sj:submit 
                                                    button="true"
                                                    value="Search" 
                                                    href="#"
                                                    disabled="#vsearch"
                                                    onClick="search()"
                                                    id="searchbut"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div> 
                                            <div class="form-group">
                                                <sj:submit 
                                                    button="true" 
                                                    value="Reset" 
                                                    name="reset" 
                                                    onClick="resetAllData()" 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;"
                                                    />
                                            </div>
                                            <div class="form-group">
                                                <sj:submit 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    button="true" 
                                                    value="View CSV" 
                                                    name="view2" 
                                                    id="view2" 
                                                    onClick="todocsv()" 
                                                    disabled="#vgenerate"/> 
                                            </div>
                                        </div>
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-6  text-right">
                                            <div class="form-group">
                                                <s:url var="uploadurl" action="viewPopupcsvProductMatrix"/>   
                                                <sj:submit                                                      
                                                    openDialog="remotedialog"
                                                    button="true"
                                                    href="%{uploadurl}"
                                                    disabled="#vupload"
                                                    value="Upload Product Matrix"
                                                    id="uploadButton"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                            <div class="form-group">                                               
                                                <s:url var="addurl" action="viewPopupProductMatrix"/>                                                    
                                                <sj:submit 
                                                    openDialog="remotedialog"
                                                    button="true"
                                                    href="%{addurl}"
                                                    disabled="#vadd"
                                                    value="Add New Product Matrix"
                                                    id="addButton" 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                        </div>
                                    </div>

                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="remotedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Add Product Matrix"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />  
                                <!-- Start update dialog box -->
                                <sj:dialog                                     
                                    id="updatedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Update Product Matrix"
                                    onOpenTopics="openviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start detail dialog box -->
                                <sj:dialog                                     
                                    id="viewdetaildialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Product Matrix"
                                    onOpenTopics="opendetailviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteProductMatrix($(this).data('debitProductType'),$(this).data('creditProductType'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete Product Matrix"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process" 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error"
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmPM($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectPM($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />
                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="ListProductMatrix"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    shrinkToFit="false"
                                    > 
                                    <sjg:gridColumn name="debitProductType" index="u.id.debitProductType" title="View" width="25" align="center" formatter="viewformatter" sortable="false" />
                                    <sjg:gridColumn name="debitProductType" index="u.id.debitProductType" title="Edit" width="25" align="center" formatter="editformatter" sortable="false" hidden="#vupdatelink"/>
                                    <sjg:gridColumn name="debitProductType" index="u.id.debitProductType" title="Delete" width="40" align="center" formatter="deleteformatter" sortable="false" hidden="#vdelete"/>                                    
                                    <sjg:gridColumn name="creditProductType" index="u.id.creditProductType" title="Credit Product Type"  sortable="true" hidden="true"/>
                                    <sjg:gridColumn name="debitProductTypeDes" index="u.productTypeByDebitProductType.productName" title="Debit Product Type"  sortable="true"/>
                                    <sjg:gridColumn name="creditProductTypeDes" index="u.productTypeByCreditProductType.productName" title="Credit Product Name"  sortable="true"/>
                                    <sjg:gridColumn name="status" index="u.status.description" title="Status"  sortable="true"/>
                                    <%--<sjg:gridColumn name="debitCurrency" index="u.productCurrency.description" title="Debit Currency"  sortable="true"/>--%>
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="createdtime" index="u.createdtime" title="Created Date And Time"  sortable="true" />
                                    <sjg:gridColumn name="lastupdatedtime" index="u.lastupdatedtime" title="Last Updated Date And Time"  sortable="true" />

                                </sjg:grid> 
                            </div>

                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="approveListProductMatrix"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Product Matrix Mangement"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"  

                                    >
                                    <sjg:gridColumn name="id" index="id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>
                                    <sjg:gridColumn name="id" index="u.id" title="Download" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/> 
                                    
                                    <sjg:gridColumn name="debitProductType" index="u.fields" title="Debit Product Type"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="creditProductType" index="u.fields" title="Credit Product Type"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                    
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="true"/>  
                                    <sjg:gridColumn name="createtime" index="u.createtime" title="Created Date And Time"  sortable="true" />                                                                  

                                </sjg:grid> 
                                
                            </div>  
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!-- end: BODY -->
    </body>
</html>
