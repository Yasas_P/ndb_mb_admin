<%-- 
    Document   : productmatrixedit
    Created on : Apr 16, 2019, 1:10:06 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Product Matrix</title> 
        <script type="text/javascript">
            function editProductType(debitProductType,creditProductType) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findProductMatrix.action',
                    data: {debitProductType: debitProductType , creditProductType: creditProductType},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#debitProductTypeEdit').val("");
                            $('#creditProductTypeEdit').val("");
                            $('#debitProductTypeEdit').attr('diabled', false);
                            $('#creditProductTypeEdit').attr('diabled', false);
//                            $('#debitCurrencyEdit').val("");
                            $('#statusEdit').val("");
                            $('#divmsg').text("");
                            toleftallEdit();
                            toleftallDebitEdit() 
                        } else {
//                            $('#debitCurrencyEdit').val(data.debitCurrency);
                            $('#statusEdit').val(data.status);
                            
                            $('#newCreditCurrencyBoxEdit').empty();
                            $('#currentCreditCurrencyBoxEdit').empty();
                            $('#newDebitCurrencyBoxEdit').empty();
                            $('#currentDebitCurrencyBoxEdit').empty();

                            $.each(data.newCreditCurrencyList, function(index, item) {
                                $('#newCreditCurrencyBoxEdit').append(
                                        $('<option></option>').val(item.key).html(
                                        item.value));
                            });
                            $.each(data.currentCreditCurrencyList, function(index, item) {
                                $('#currentCreditCurrencyBoxEdit').append(
                                        $('<option></option>').val(item.key).html(
                                        item.value));
                            });
                            $.each(data.newDebitCurrencyList, function(index, item) {
                                $('#newDebitCurrencyBoxEdit').append(
                                        $('<option></option>').val(item.key).html(
                                        item.value));
                            });
                            $.each(data.currentDebitCurrencyList, function(index, item) {
                                $('#currentDebitCurrencyBoxEdit').append(
                                        $('<option></option>').val(item.key).html(
                                        item.value));
                            });
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var debitProductType = $('#debitProductTypeHiddenEdit').val();
                var creditProductType = $('#creditProductTypeHiddenEdit').val();
                editProductType(debitProductType,creditProductType);
            }
            
            function clickUpdate() {

                $('#currentCreditCurrencyBoxEdit option').prop('selected', true);
                $('#newCreditCurrencyBoxEdit option').prop('selected', true);
                $('#currentDebitCurrencyBoxEdit option').prop('selected', true);
                $('#newDebitCurrencyBoxEdit option').prop('selected', true);
                $('#productmatrixedit').submit();
            }
            
             //----------- Credit Multiselect------------------------
            function toleftEdit() {
                $("#currentCreditCurrencyBoxEdit option:selected").each(function () {

                    $("#newCreditCurrencyBoxEdit").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function torightEdit() {
                $("#newCreditCurrencyBoxEdit option:selected").each(function () {

                    $("#currentCreditCurrencyBoxEdit").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function toleftallEdit() {
                $("#currentCreditCurrencyBoxEdit option").each(function () {

                    $("#newCreditCurrencyBoxEdit").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function torightallEdit() {
                $("#newCreditCurrencyBoxEdit option").each(function () {

                    $("#currentCreditCurrencyBoxEdit").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });

            }
            
             //----------- Debit Multiselect------------------------
            function toleftDebitEdit() {
                $("#currentDebitCurrencyBoxEdit option:selected").each(function () {

                    $("#newDebitCurrencyBoxEdit").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function torightDebitEdit() {
                $("#newDebitCurrencyBoxEdit option:selected").each(function () {

                    $("#currentDebitCurrencyBoxEdit").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function toleftallDebitEdit() {
                $("#currentDebitCurrencyBoxEdit option").each(function () {

                    $("#newDebitCurrencyBoxEdit").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function torightallDebitEdit() {
                $("#newDebitCurrencyBoxEdit option").each(function () {

                    $("#currentDebitCurrencyBoxEdit").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });

            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="productmatrixedit" method="post" action="updateProductMatrix" theme="simple" cssClass="form" >
          <s:hidden id="debitProductTypeHiddenEdit" name="debitProductTypeHidden" value="%{debitProductType}" />
          <s:hidden id="creditProductTypeHiddenEdit" name="creditProductTypeHidden" value="%{creditProductType}" />
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Debit Product Type </label>
                        <s:select cssClass="form-control" name="debitProductType" id="debitProductTypeEdit" list="%{productTypeList}"   headerKey=""  headerValue="--Select Debit Product Type--" listKey="productType" listValue="productName"  disabled="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Credit Product Type</label>
                        <s:select cssClass="form-control" name="creditProductType" id="creditProductTypeEdit" list="%{productTypeList}"   headerKey=""  headerValue="--Select Credit Product Type--" listKey="productType" listValue="productName" disabled="true" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group ">
                        <span style="color: red">*</span><label >Debit Currency</label>
                        <%--<s:select  cssClass="form-control" name="debitCurrency" id="debitCurrencyEdit" list="%{productCurrencyList}"   headerKey=""  headerValue="--Select Debit Currency--" listKey="currencyCode" listValue="description" disabled="false"/>--%>
                    </div>
                </div>    -->
            </div>
            <div class="row row_popup">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Debit Currency</label>
                    </div>
                </div>
            </div>        
            <div class="row row_popup">
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:select cssClass="form-control" multiple="true"
                                  name="newDebitCurrencyBox" id="newDebitCurrencyBoxEdit" list="newDebitCurrencyList"									 
                                  ondblclick="torightDebitEdit()" style="height:160px;"  listKey="key" listValue="value"/>
                    </div>                
                </div>
                <div class="col-sm-2 text-center">
                    <div class="form-group">
                        <div class="row" style="height: 20px;"></div>
                        <div class="row">
                            <sj:a
                                id="rightDebitEdit" 
                                onClick="torightDebitEdit()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> > </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="rightallDebitEdit" 
                                onClick="torightallDebitEdit()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size: 10px;width:60px;margin:4px;"> >> </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="leftDebitEdit" 
                                onClick="toleftDebitEdit()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> < </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="leftallDebitEdit" 
                                onClick="toleftallDebitEdit()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> << </sj:a>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-5">
                        <div class="form-group"> 
                        <s:select cssClass="form-control" multiple="true" 
                                  name="currentDebitCurrencyBox" id="currentDebitCurrencyBoxEdit" list="currentDebitCurrencyList"									 
                                  ondblclick="toleftDebitEdit()" style="height:160px;" listKey="key" listValue="value" />

                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Credit Currency</label>
                    </div>
                </div>
            </div>        
            <div class="row row_popup">
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:select cssClass="form-control" multiple="true"
                                  name="newCreditCurrencyBox" id="newCreditCurrencyBoxEdit" list="newCreditCurrencyList"									 
                                  ondblclick="torightEdit()" style="height:160px;"  listKey="key" listValue="value"/>
                    </div>                
                </div>
                <div class="col-sm-2 text-center">
                    <div class="form-group">
                        <div class="row" style="height: 20px;"></div>
                        <div class="row">
                            <sj:a
                                id="rightEdit" 
                                onClick="torightEdit()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> > </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="rightallEdit" 
                                onClick="torightallEdit()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size: 10px;width:60px;margin:4px;"> >> </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="leftEdit" 
                                onClick="toleftEdit()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> < </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="leftallEdit" 
                                onClick="toleftallEdit()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> << </sj:a>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-5">
                        <div class="form-group"> 
                        <s:select cssClass="form-control" multiple="true" 
                                  name="currentCreditCurrencyBox" id="currentCreditCurrencyBoxEdit" list="currentCreditCurrencyList"									 
                                  ondblclick="toleftEdit()" style="height:160px;"  listKey="key" listValue="value" />

                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <%--<s:url action="updateProductType" var="updateturl"/>--%>
                        <sj:submit
                            button="true"
                            value="Update"
                            onclick="clickUpdate()"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>