<%-- 
    Document   : billcategory
    Created on : Apr 17, 2019, 10:12:10 AM
    Author     : prathibha_w
--%>

<%@page import="com.epic.ndb.util.varlist.CommonVarList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
//            $.subscribe('onclicksearch', function (event, data) {
            function search() {
                $('#message').empty();

                var billercatcodeSearch = $('#billercatcodeSearch').val();
                var nameSearch = $('#nameSearch').val();
                var descriptionSearch = $('#descriptionSearch').val();
                var statusSearch = $('#statusSearch').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        billercatcodeSearch: billercatcodeSearch,
                        nameSearch: nameSearch,
                        descriptionSearch: descriptionSearch,
                        statusSearch: statusSearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
//            );

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function editformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:editBillCategoryInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function deleteformatter(cellvalue, options, rowObject) {
                return "<a href='#/' title='Delete' onClick='javascript:deleteBillCategoryInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmBillCategory(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectBillCategory(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            function confirmBillCategory(keyval, popvar) {
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');


                return false;
            }

            function rejectBillCategory(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function confirmBC(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/ConfirmBillCategory.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectBC(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/RejectBillCategory.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }


            function editBillCategoryInit(keyval) {
                $("#updatedialog").data('billercatcode', keyval).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("DetailBillCategory.action?billercatcode=" + $led.data('billercatcode'));
            });

            function deleteBillCategoryInit(keyval) {
                $('#divmsg').empty();

                $("#deletedialog").data('keyval', keyval).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete bill category ' + keyval + ' ?');
                return false;
            }

            function deleteBillCategory(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/DeleteBillCategory.action',
                    data: {billercatcode: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#deletesuccdialog").dialog('open');
                        $("#deletesuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function resetAllData() {
                $('#billercatcodeSearch').val("");
                $('#descriptionSearch').val("");
                $('#nameSearch').val("");
                $('#statusSearch').val("");

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        billercatcodeSearch: '',
                        nameSearch: '',
                        descriptionSearch: '',
                        statusSearch: '',
                        search: false
                    }
                });
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetFieldData() {

                $('#billercatcode').val("");
                $('#description').val("");
                $('#name').val("");
                $('#status').val("");

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }


        </script>
        <title></title>
    </head>
    <body style="">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set var="vupdatebutt"><s:property value="vupdatebutt" default="true"/></s:set>
                            <s:set var="vupdatelink"><s:property value="vupdatelink" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form cssClass="form" id="billcatsearch" method="post" action="BillCategory" theme="simple" >
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Bill Category Code </label>
                                                <s:textfield cssClass="form-control" name="billercatcodeSearch" id="billercatcodeSearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Bill Category Name</label>
                                                <s:textfield cssClass="form-control" name="nameSearch" id="nameSearch" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Description</label>
                                                <s:textfield cssClass="form-control" name="descriptionSearch" id="descriptionSearch" maxLength="250" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Status</label>
                                                <s:select  cssClass="form-control" name="status" id="statusSearch" list="%{statusList}"   headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" value="%{status}" disabled="false"/>
                                            </div>
                                        </div>
                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                href="#"
                                                disabled="#vsearch"
                                                onClick="search()"  
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div> 
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                />
                                        </div>
                                    </div>
                                    <div class="col-sm-5"></div>
                                    <div class="col-sm-3  text-right">
                                        <div class="form-group">                                               
                                            <s:url var="addurl" action="ViewPopupBillCategory"/>                                                    
                                            <sj:submit 
                                                openDialog="remotedialog"
                                                button="true"
                                                href="%{addurl}"
                                                disabled="#vadd"
                                                value="Add Bill Category"
                                                id="addButton"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>
                                    </div>
                                </div>

                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="remotedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Add Bill Category"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                
                                    />  
                                <!-- Start update dialog box -->
                                <sj:dialog                                     
                                    id="updatedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Update Bill Category"
                                    onOpenTopics="openviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                   
                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteBillCategory($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete Bill Category"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process" 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error"
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmBC($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectBC($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />
                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="ListBillCategory"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    > 
                                    <sjg:gridColumn name="billercatcode" index="u.billercatcode" title="Edit" width="25" align="center" formatter="editformatter" sortable="false" hidden="#vupdatelink"/>
                                    <sjg:gridColumn name="billercatcode" index="u.billercatcode" title="Delete" width="40" align="center" formatter="deleteformatter" sortable="false" hidden="#vdelete"/>                                    
                                    <sjg:gridColumn name="billercatcode" index="u.billercatcode" title="Bill category Code"  sortable="true"/>
                                    <sjg:gridColumn name="name" index="u.name" title="Bill Category Name"  sortable="true"/>
                                    <sjg:gridColumn name="description" index="u.description" title="Description"  sortable="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="createdtime" index="u.createtime" title="Created Date And Time"  sortable="true" />
                                    <sjg:gridColumn name="lastupdatedtime" index="u.lastupdatedtime" title="Last Updated Date And Time"  sortable="true" />
                                </sjg:grid> 
                            </div>

                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="ApproveListBillCategory"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Bill Category"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"  
                                    >

                                    <sjg:gridColumn name="id" index="u.id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="u.id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>

                                    <sjg:gridColumn name="billercatcode" index="u.id" title="Bill Category Code"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                                     
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="false"/>  
                                    <sjg:gridColumn name="createtime" index="u.createdtime" title="Created Date And Time"  sortable="false" />
                                </sjg:grid>  
                            </div>  
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!-- end: BODY -->
    </body>
</html>

