<%-- 
    Document   : billserviceproviderdetail
    Created on : Aug 12, 2019, 1:24:44 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Detail Bill Category</title> 
        <script>
            $(document).ready(function () {
                $('#cusFieldsDetail').hide();
                if ($('#havingCustomFieldDetail').prop('checked')) {
                    $('#cusFieldsDetail').show();
                } else {
                    $('#cusFieldsDetail').hide();
                }
                
                var fieldType = $('#fieldIdTypeDetail').val();
                if (fieldType == '2' || fieldType == '3') {
                    $('#nontextFieldDetail').show();
                    $('#textFieldDetail').hide();
                } else if (fieldType == '1') {
                    $('#nontextFieldDetail').hide();
                    $('#textFieldDetail').show();
                }
                
                var billerType = $('#billerTypeCodeDetail').val();
                if(billerType=="MOB"){
                    $('#mobPrefixHideDetail').show();
                }else{
                    $('#mobPrefixHideDetail').hide();
                    $('#mobPrefixDetail').val("");
                }
            });

        </script>
    </head>
    <body>
        <s:div id="amessagedetail">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="billspdetail" method="post" action="BillSP" theme="simple" cssClass="form" enctype="multipart/form-data" >
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Provider ID</label>
                        <s:textfield value="%{providerId}" cssClass="form-control" name="providerId" id="providerIdDetail" maxLength="20" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Provider Name</label>
                        <s:textfield value="%{providerName}" cssClass="form-control" name="providerName" id="providerNameDetail" maxLength="50"  readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield value="%{description}" cssClass="form-control" name="description" id="descriptionDetail" maxLength="100"  readonly="true"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Category</label>
                        <s:select value="%{categoryCode}" cssClass="form-control" id="categoryCodeDetail" list="%{categoryList}"  name="categoryCode" headerKey=""  headerValue="--Select Bill Category--" listKey="billercatcode" listValue="description" disabled="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Display Name</label>
                        <s:textfield value="%{displayName}" cssClass="form-control" name="displayName" id="displayNameDetail" maxLength="100"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Collection Account</label>
                        <s:textfield value="%{collectionAccount}" cssClass="form-control" name="collectionAccount" id="collectionAccountDetail" maxLength="50" readonly="true"/>
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Code</label>
                        <s:textfield value="%{bankCode}" cssClass="form-control" name="bankCode" id="bankCodeDetail" maxLength="20"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Biller Type</label>
                        <s:select value="%{billerTypeCode}" cssClass="form-control" id="billerTypeCodeDetail" list="%{billerTypeList}"  name="billerTypeCode" headerKey=""  headerValue="--Select Biller Type--" listKey="billerTypeCode" listValue="billerTypeDescription" disabled="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusDetail" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" disabled="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Currency</label>
                        <s:select value="%{currency}" cssClass="form-control" id="currencyDetail" list="%{currencyList}"  name="currency" headerKey=""  headerValue="--Select Currency--" listKey="currencyCode" listValue="description" disabled="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Charge</label>
                        <s:select value="%{feeCharge}" cssClass="form-control" id="feeChargeDetail" list="%{feeChargeList}"  name="feeCharge" headerKey=""  headerValue="--Select Fee Charge--" listKey="key" listValue="key" disabled="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Product Type</label>
                        <s:select value="%{productType}" cssClass="form-control" id="productTypeDetail" list="%{productTypeList}"  name="productType" headerKey=""  headerValue="--Select Product Type--" listKey="key" listValue="value" disabled="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Biller Value Code</label>
                        <s:textfield value="%{billerValCode}" cssClass="form-control" name="billerValCode" id="billerValCodeDetail"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Validation Status</label>
                        <s:select value="%{validationStatus}" cssClass="form-control" id="validationStatusDetail" list="%{statusList}"  name="validationStatus" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" disabled="true" />
                    </div>
                </div>
            </div>        
            <div class="row row_popup">
                <div class="col-sm-4" id="mobPrefixHideDetail" style="display: none" >
                    <div class="form-group">
                        <span style="color: red">*</span><label>Mobile Prefix</label><span style="color: red"> Eg:-07X,0XXX</span>
                        <s:textfield value="%{mobPrefix}" cssClass="form-control" name="mobPrefix" id="mobPrefixDetail" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^0-9,]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9,]/g,''))" disabled="true" />
                    </div>
                </div>
                <div class="col-sm-4" style="margin-top: 28px;">
                    <div class="form-group">
                        <label style="float: left;margin: 5px 9px 3px 7px">Biller Custom Fields</label>
                        <s:checkbox  name="havingCustomField" id="havingCustomFieldDetail"  cssStyle="width: 20px;height: 20px;" disabled="true"/>
                    </div>
                </div>
            </div>

            <div id="cusFieldsDetail" >
                <div class="row row_popup">
                    <div class="horizontal_line_popup"></div>
                </div>
                <div class="row row_popup" >
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Field Name</label>
                            <s:textfield value="%{fieldName}" cssClass="form-control" name="fieldName" id="fieldNameDetail" maxLength="100"  readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Field Type</label>
                            <s:select value="%{fieldIdType}" cssClass="form-control" id="fieldIdTypeDetail" list="%{fieldTypeList}"  name="fieldIdType" headerKey=""  headerValue="--Select Field Type--" listKey="id" listValue="name" disabled="true" />
                        </div>
                    </div>
                    <div id="nontextFieldDetail"  style="display:none">    
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Field Value (JSON)</label>
                                <s:textfield value="%{fieldVal}" cssClass="form-control" name="fieldVal" id="fieldValDetail" maxLength="200" readonly="true" disabled="true" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="textFieldDetail"  style="display:none">             
                    <div class="row row_popup" >
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Length Min</label>
                                <s:textfield value="%{fieldLenMin}" cssClass="form-control" name="fieldLenMin" id="fieldLenMinDetail" maxLength="4"  readonly="true" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Length Max</label>
                                <s:textfield value="%{fieldLen}" cssClass="form-control" name="fieldLen" id="fieldLenDetail" maxLength="4"  readonly="true"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Validation</label>
                                <s:select value="%{fieldValidation}" cssClass="form-control" id="fieldValidationDetail" list="%{regexList}"  name="fieldValidation" headerKey=""  headerValue="--Select Field Validation--" listKey="paramvalue" listValue="description" disabled="true" />
                            </div>
                        </div>
                    </div>          
                    <div class="row row_popup" >    
                        <div class="col-sm-4" >
                            <div class="form-group">
                                <label>Place Holder</label>
                                <s:textfield value="%{placeHolder}" cssClass="form-control" name="placeHolder" id="placeHolderDetail" maxLength="50"  readonly="true" />
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-8">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Image (Maximum size (w*h) : 480x800 pixels, Valid extensions : gif,jpg,jpeg,png,img)</label>   
                        <div class="row">
                            <div class="col-sm-2">
                                <img onclick="mobileImgZoom(this.src)" class="image" src="<s:property value="imageUrl"/>" id="mobileImg_detail" name="mobileImgedit" alt="Mobile Image" >
                                <!--<span id="detailspanimg" class="ui-button-icon-primary ui-icon ui-icon-circle-close" style="position:absolute;top:10px;bottom:0;right:25%"></span>-->  
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>             
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
