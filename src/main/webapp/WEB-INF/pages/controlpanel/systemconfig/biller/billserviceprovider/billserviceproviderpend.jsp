<%-- 
    Document   : billserviceproviderpend
    Created on : Jun 13, 2019, 4:52:58 PM
    Author     : sivaganesan_t
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View Pend Bill Service Provider</title> 
    </head>
    <script type="text/javascript">
        $(document).ready(function () {
            var fieldType = $('#fieldIdTypePendView').val();
            if (fieldType == '2' || fieldType == '3') {
                $('#nontextFieldPendview').show();
                $('#textFieldPendview').hide();
            } else if (fieldType == '1') {
                $('#nontextFieldPendview').hide();
                $('#textFieldPendview').show();
            }
            
            var billerType = $('#billerTypeCode_p').val();
            if(billerType=="MOB"){
                $('#mobPrefixHide_p').show();
            }else{
                $('#mobPrefixHide_p').hide();
                $('#mobPrefix_p').val("");
            }
        });
    </script>
    <body>
        <s:div id="pmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="billsppend" method="post" action="BillSP" theme="simple" cssClass="form" enctype="multipart/form-data"  >
            <s:hidden id="fieldIdTypePendView" name="fieldIdType" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Bill Provider ID</label>
                        <s:textfield value="%{providerId}" cssClass="form-control" name="providerId" id="providerId_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Provider Name</label>
                        <s:textfield value="%{providerName}" cssClass="form-control" id="providerName_p"  name="providerName"  readonly="true"  />
                    </div>
                </div>               
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield value="%{description}" cssClass="form-control" id="description_p" name="description"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Category</label>
                        <s:textfield value="%{categoryCode}" cssClass="form-control" name="categoryCode" id="categoryCode_p"  readonly="true" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Display Name</label>
                        <s:textfield value="%{displayName}" cssClass="form-control" name="displayName" id="displayName_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Collection Account</label>
                        <s:textfield value="%{collectionAccount}" cssClass="form-control" name="collectionAccount" id="collectionAccount_p"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Bank Code</label>
                        <s:textfield value="%{bankCode}" cssClass="form-control" name="bankCode" id="bankCode_p"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Biller Type</label>
                        <s:textfield value="%{billerType}" cssClass="form-control" name="billerType" id="billerType_p" readonly="true" />
                        <s:hidden value="%{billerTypeCode}" cssClass="form-control" name="billerTypeCode" id="billerTypeCode_p"  />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:textfield value="%{status}" cssClass="form-control" name="status" id="status_p"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Currency</label>
                        <s:textfield value="%{currency}" cssClass="form-control" name="currency" id="currency_p"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Fee Charge</label>
                        <s:textfield value="%{feeCharge}" cssClass="form-control" name="feeCharge" id="feeCharge_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Product Type</label>
                        <s:textfield value="%{productType}" cssClass="form-control" name="productType" id="productType_p" readonly="true" />
                    </div>
                </div>
            </div>
                    <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Biller Value Code</label>
                        <s:textfield value="%{billerValCode}" cssClass="form-control" name="billerValCode" id="billerValCode_p"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Validation Status</label>
                        <s:textfield value="%{validationStatus}" cssClass="form-control" name="validationStatus" id="validationStatusss_p"  readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" id="mobPrefixHide_p" style="display: none" >
                    <div class="form-group">
                        <span style="color: red">*</span><label>Mobile Prefix</label>
                        <s:textfield value="%{mobPrefix}" cssClass="form-control" name="mobPrefix" id="mobPrefix_p" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^0-9,]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9,]/g,''))"  readonly="true"/>
                    </div>
                </div>
<!--                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Place Holder</label>
                        <%--<s:textfield value="%{placeHolder}" cssClass="form-control" name="placeHolder" id="placeHolder_p" maxLength="50" readonly="true" />--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Have Biller Custom Fields</label>
                        <s:textfield value="%{havingCustomField}" cssClass="form-control" name="havingCustomField" id="havingCustomField_p"  readonly="true" />
                    </div>
                </div>
            </div>
            <s:set name="havingCustomField" value="havingCustomField"/>        
            <s:if test="%{#havingCustomField==true}">   
                <div class="row row_popup">
                    <div class="horizontal_line_popup"></div>
                </div>        
                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Field Name</label>
                            <s:textfield value="%{fieldName}" cssClass="form-control" name="fieldName" id="fieldName_p" readonly="true" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Field Type</label>
                            <s:textfield value="%{fieldIdTypeDes}" cssClass="form-control" name="fieldIdTypeDes" id="fieldIdType_p"   readonly="true" />
                        </div>
                    </div>
                    <div id="nontextFieldPendview"  style="display:none">     
                        <div class="col-sm-3">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Value (JSON)</label>
                                <s:textfield value="%{fieldVal}" cssClass="form-control" name="fieldVal" id="fieldVal_p"  readonly="true" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="textFieldPendview"  style="display:none">             
                    <div class="row row_popup">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Length Min</label>
                                <s:textfield value="%{fieldLenMin}" cssClass="form-control" name="fieldLenMin" id="fieldLenMin_p"  readonly="true" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Length Max</label>
                                <s:textfield value="%{fieldLen}" cssClass="form-control" name="fieldLen" id="fieldLen_p"  readonly="true" />
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Validation</label>
                                <s:textfield value="%{fieldValidation}" cssClass="form-control" name="fieldValidation" id="fieldValidation_p"  readonly="true" />
                            </div>
                        </div>
                        <div class="col-sm-3" >
                            <div class="form-group">
                                <label>Place Holder</label>
                                <s:textfield value="%{placeHolder}" cssClass="form-control" name="placeHolder" id="placeHolder_p" maxLength="50" readonly="true" />
                            </div>
                        </div>
                    </div>
                </div>
            </s:if>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">        
                <div class="col-sm-6">
                    <div class="form-group">
                        <label >Mobile Image (Maximum size (w*h) : 480x800 pixels)</label>   
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="image" src="<s:property value="imageUrl"/>" id="mobileImg_p" alt="Mobile Image" > 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>

