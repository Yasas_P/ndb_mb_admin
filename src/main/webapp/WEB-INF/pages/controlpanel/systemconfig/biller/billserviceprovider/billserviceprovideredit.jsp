<%-- 
    Document   : billserviceprovideredit
    Created on : Apr 17, 2019, 3:08:25 PM
    Author     : prathibha_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Bill Category</title> 
        <script type="text/javascript">
            $(document).ready(function () {
                var fieldType = $('#fieldIdTypeEdit').val();
                if (fieldType == '2' || fieldType == '3') {
                    $('#nontextFieldEdit').show();
                    $('#textFieldEdit').hide();
                } else if (fieldType == '1') {
                    $('#nontextFieldEdit').hide();
                    $('#textFieldEdit').show();
                }
                
                var billerType = $('#billerTypeCodeEdit').val();
                if(billerType=="MOB"){
                    $('#mobPrefixHideEdit').show();
                }else{
                    $('#mobPrefixHideEdit').hide();
                    $('#mobPrefixEdit').val("");
                }
            });
            function editBillCategory(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/FindBillSP.action',
                    data: {providerId: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#providerIdEdit').val("");
                            $('#providerIdEdit').attr('readOnly', false);
                            $("#providerIdEdit").css("color", "black");
                            $('#providerNameEdit').val("");
                            $('#descriptionEdit').val("");
                            $('#categoryCodeEdit').val("");
                            $('#imageUrlEdit').val("");
                            $('#displayNameEdit').val("");
                            $('#collectionAccountEdit').val("");
                            $('#bankCodeEdit').val("");

                            $("#havingCustomFieldEdit").prop("checked", false);

                            $('#payTypeEdit').val("");
                            $('#billerTypeCodeEdit').val("");
                            $('#statusEdit').val("");
                            $('#currencyEdit').val("");
                            $('#feeChargeEdit').val("");
                            $('#productTypeEdit').val("");
                            $('#billerValCodeEdit').val("");
                            $('#validationStatusEdit').val("");
                            $('#mobPrefixHideEdit').hide();
                            $('#mobPrefixEdit').val("");

                            $('#fieldIdTypeEdit').val("");
                            $('#fieldValEdit').val("");
                            $('#fieldNameEdit').val("");
                            $('#fieldValidationEdit').val("");
                            $('#fieldLenEdit').val("");
                            $('#fieldLenMinEdit').val("");
                            $('#placeHolderEdit').val("");
                            $('#nontextFieldEdit').hide();
                            $('#textFieldEdit').hide();
                            $('#cusFieldsEdit').hide();
                            $('#divmsg').text("");
                        } else {
                            $('#providerIdEdit').val(data.providerId);
                            $('#providerIdEdit').attr('readOnly', true);
                            $("#providerIdEdit").css("color", "#858585");

                            $('#providerNameEdit').val(data.providerName);
                            $('#descriptionEdit').val(data.description);
                            $('#categoryCodeEdit').val(data.categoryCode);
//                            $('#imageUrlEdit').val(data.imageUrl);
                            $('#displayNameEdit').val(data.displayName);
                            $('#collectionAccountEdit').val(data.collectionAccount);
                            $('#bankCodeEdit').val(data.bankCode);

                            $("#havingCustomFieldEdit").prop("checked", data.havingCustomField);

//                            $('#payTypeEdit').val(data);
                            $('#billerTypeCodeEdit').val(data.billerTypeCode);
                            $('#statusEdit').val(data.status);
                            $('#currencyEdit').val(data.currency);
                            $('#feeChargeEdit').val(data.feeCharge);
                            $('#productTypeEdit').val(data.productType);
                            $('#billerValCodeEdit').val(data.billerValCode);
                            $('#validationStatusEdit').val(data.validationStatus);
                            if(data.billerTypeCode=="MOB"){
                                $('#mobPrefixHideEdit').show();
                                $('#mobPrefixEdit').val(data.mobPrefix);
                            }else{
                                $('#mobPrefixHideEdit').hide();
                                $('#mobPrefixEdit').val("");
                            }
//                            $('#placeHolderEdit').val(data.placeHolder);

                            if ($('#havingCustomFieldEdit').prop('checked')) {
                                $('#cusFieldsEdit').show();

                                $('#fieldIdTypeEdit').val(data.fieldIdType);
                                $('#fieldNameEdit').val(data.fieldName);
                                if (data.fieldIdType == '2' || data.fieldIdType == '3') {
                                    $('#nontextFieldEdit').show();
                                    $('#textFieldEdit').hide();
                                    $('#fieldValEdit').val(data.fieldVal);
                                } else if (data.fieldIdType == '1') {
                                    $('#nontextFieldEdit').hide();
                                    $('#textFieldEdit').show();
                                    $('#fieldValidationEdit').val(data.fieldValidation);
                                    $('#fieldLenEdit').val(data.fieldLen);
                                    $('#fieldLenMinEdit').val(data.fieldLenMin);
                                    $('#placeHolderEdit').val(data.placeHolder);
                                } else {
                                    $('#nontextFieldEdit').hide();
                                    $('#textFieldEdit').hide();
                                }
                            } else {
                                $('#cusFieldsEdit').hide();
                            }

                            $('#mobileImg_edit').attr("src", data.imageUrl);

                            $('#imageUrlEdit').val(data.imageUrl);

                            $("#up_e").hide();
                            $("#down_e").hide();
                            $("#msg_e").hide();
                            $("#hasImage").val('NOIMG');
                            deletefirebase_edit();

                            $('#divmsg').text(data);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var code = $('#providerIdEdit').val();
                editBillCategory(code);
            }
            function FieldValueEdit(val) {
                $('#divmsg').empty();
                var fieldVal = $("#fieldValEdit").val();
                $("#editfielddialog").data('keyval', val).dialog('open');

                $("#editfielddialog").
                        html('<label id="errormessageIdEdit" style="display:none; padding: 0px 22px;color: red;border: 1px solid;margin: 2px 21px;">ID already exist</label>\n\
                        <div class="row row_popup">\n\
                            <div class="col-sm-8" style="padding:0">\n\
                               <div class="form-group">\n\
                                   <label>Field Value (JSON)</label>\n\
                                   <textarea rows="4" cols="37" id="textJsonEdit" disabled="true"/>\n\
                               </div>\n\
                            </div>\n\
                            <div class="col-sm-4" style="padding-top:40px;">\n\
                                <button onclick="clearEdit()" class="btn btn-default btn-sm ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" >Clear Json</button>\n\
                            </div>\n\
                         </div>\n\
                        <div class="row row_popup">\n\
                            <div class="horizontal_line_popup"></div>\n\
                        </div>\n\
                        <div class="row row_popup">\n\
                            <div class="col-sm-4" style="padding:0">\n\
                                <div class="form-group">\n\
                                    <label>ID</label>\n\
                                    <input class="form-control" id="id1Edit" type="text" name="ss" maxlength="10" />\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-sm-4" style="padding:0">\n\
                                <div class="form-group">\n\
                                    <label>Name</label>\n\
                                    <input class="form-control" id="id2Edit" type="text" name="ss" maxlength="100"/>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-sm-4" style="padding-top:20px;">\n\
                                <div class="form-group">\n\
                                     <button onclick="editJason()" class="btn btn-sm active ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" style="background-color: #ada9a9" style="padding:40px;">Add Field</button>\n\
                                </div>\n\
                            </div>\n\
                        </div>');
                $("#textJsonEdit").val(fieldVal);
                return false;
            }

            function clearEdit() {
                $("#textJsonEdit").val("");
                $("#errormessageIdEdit").hide();
            }

            function editJason() {
                var fieldVal = $("#textJsonEdit").val();
                var val1 = $("#id1Edit").val();
                var val2 = $("#id2Edit").val();
                var idval = true;
                if (val1) {
                    if (fieldVal) {
                        var jsonObj = $.parseJSON(fieldVal);

                        var searchTerm = val1;
                        var results = jsonObj.filter(function (obj) {
                            if (obj.id.indexOf(searchTerm) > -1) {
                                idval = false;
                                globBoolEdit = false;
                            }
                        });
                    }
                    if (idval) {
                        globBoolEdit = true;
                        var string = '{ "id":"' + val1 + '","name":"' + val2 + '"}';

                        if (fieldVal) {
                            fieldVal = fieldVal.substring(1, fieldVal.length - 1);

                            fieldVal += "," + string;

                            fieldVal = "[" + fieldVal + "]";
                            if (fieldVal.length > 200) {
                                $("#errormessageIdEdit").show();
                                $("#errormessageIdEdit").html("Field value maximum length should not exceed 200");
                            } else {
                                $("#textJsonEdit").val(fieldVal);
                                $("#id1Edit").val("");
                                $("#id2Edit").val("");
                                $("#errormessageIdEdit").hide();
                            }

                        } else {
                            fieldVal = "[" + string + "]";
                            if (fieldVal.length > 200) {
                                $("#errormessageIdEdit").show();
                                $("#errormessageIdEdit").html("Field value maximum length should not exceed 200");
                            } else {
                                $("#textJsonEdit").val(fieldVal);
                                $("#id1Edit").val("");
                                $("#id2Edit").val("");
                                $("#errormessageIdEdit").hide();
                            }
                        }
                    } else {
                        $("#errormessageIdEdit").show();
                        $("#errormessageIdEdit").html("ID already exist");
                    }
                } else {
                    $("#errormessageIdEdit").show();
                    $("#errormessageIdEdit").html("ID can not be empty");
                }
            }
            // validation
            var globBoolEdit = true;
            function editFieldVal() {
                var fieldvalue = $("#textJsonEdit").val();
                $("#fieldValEdit").val(fieldvalue);
                $("#textJsonEdit").val("");
            }
            function changeMobileImageEdit() {
                $("#mobileImgFile").change(function (event) {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#mobileImg_edit").attr("src", tmppath);
                });
            }

            $('#editspanimg').click(function () {
                $('#mobileImgFile').val("");
                $('#imageUrlEdit').val("");
                $("#mobileImg_edit").attr("src", "");
                $("#hasImage").val('NOIMG');
            });

            //========================= firebase =================================

            var file_mob_1_width;
            var file_mob_1_height;

            $(document).ready(function () {
                var _URL = window.URL || window.webkitURL;
                $("#hasImage").val('NOIMG');

                $("#mobileImgFile").change(function (e) {
                    var file, img;
                    if ((file = this.files[0])) {
                        img = new Image();
                        img.onload = function () {
                            file_mob_1_width = this.width;
                            file_mob_1_height = this.height;
                        };
                        img.onerror = function () {
                            alert("not a valid file: " + file.type);
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                    $("#hasImage").val('IMG');
                });
            });

            function uploadtofirebase_edit() {

                var file_mob_if = $('#mobileImgFile').val();
                var imagecount = 0;
                var sendImageCount = 0;

                if (file_mob_if != "") {
                    imagecount++;
                }
                var status = true;
                var status_size = validateImage();
//                var status_size = "";


                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    const file_mob_name = file_mob_if != "" ? $('#mobileImgFile').get(0).files[0].name : null
                    var fileNameExt = file_mob_name.substr(file_mob_name.lastIndexOf('.') + 1);
                    const validImageTypes = ['gif', 'jpg', 'jpeg', 'png', 'img'];
                    if (validImageTypes.includes(fileNameExt)) {
                        if (status_size == "") {
                            $("#msg_e").hide();

                            var metadata = {
                                contentType: 'image/jpeg',
                            };

                            const ref_mob = firebase.storage().ref('promotions/');
                            var file_mob = file_mob_if != "" ? $('#mobileImgFile').get(0).files[0] : null;

                            var name_mob = "";

                            if (file_mob_if != "") {
                                name_mob = file_mob.name;
                            }


                            $("#up_e").fadeIn();
                            $("#down_e").fadeOut();

                            $('#amessageedit').empty();

                            //first image
                            if (file_mob_if != "") {
                                const task_mob = ref_mob.child(name_mob).put(file_mob, metadata).then(function () {
                                    ref_mob.child(name_mob).getDownloadURL().then(function (url) {
                                        console.log("mob  - " + url);
                                        $("#imageUrlEdit").val(url);
                                        sendImageCount++;
                                        if (imagecount == sendImageCount) {
                                            $("#up_e").hide();
                                            $("#down_e").fadeIn();
                                            $("#hasImage").val('NOIMG');
                                        }

                                        return url;
                                    });
                                });
                            }
                        } else {
                            $("#up_e").hide();
                            $("#down_e").hide();
                            $("#msg_e").show();
                            $("#msg_e").html(status_size);
                        }
                    } else {
                        $("#up_e").hide();
                        $("#down_e").hide();
                        $("#msg_e").show();
                        $("#msg_e").html("Please upload image with one of the following extensions:gif,jpg,jpeg,png,img")
                    }
                } else {
                    $("#up_e").hide();
                    $("#down_e").hide();
                    $("#msg_e").show();
                    $("#msg_e").html("Please select the image......")
                }
            }


            function deletefirebase_edit() {

                var file_mob_if = $('#mobileImgFile').val();

                var status = true;

                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    $("#msg_e").hide();
                    $("#up_e").hide();
                    $("#down_e").hide();

                    const ref_mob = firebase.storage().ref('promotions/');
                    const file_mob = file_mob_if != "" ? $('#mobileImgFile').get(0).files[0] : null;
                    $('#mobileImgFile').val("");

                    var name_mob = "";

                    if (file_mob_if != "") {
                        name_mob = file_mob.name;
                        const task_mob = ref_mob.child(name_mob).delete().then(function () {
                            console.log("deleted");
//                            $('#firebaseImgMob1Urledit').val(img1);
//                            $('#mobileImgFile').val("");
                        }).catch(function (error) {
                            switch (error.code) {
                                case 'storage/object_not_found':
                                    break;
                                case 'storage/unauthorized':
                                    break;
                                case 'storage/canceled':
                                    break;
                                case 'storage/unknown':
                                    break;
                            }
                        });
                    }
                } else {
                    $('#mobileImgFile').val("");
                }
            }

            function setFieldTypeVarEdit(value) {
                var fieldType = value.val();
                $('#fieldValEdit').val("");
                $('#fieldValidationEdit').val("");
                $('#fieldLenEdit').val("");
                $('#fieldLenMinEdit').val("");
                $('#placeHolderEdit').val("");
                if (fieldType == '1') {
                    $('#nontextFieldEdit').hide();
                    $('#textFieldEdit').show()();
                } else if (fieldType == '2' || fieldType == '3') {
                    $('#nontextFieldEdit').show();
                    $('#textFieldEdit').hide();
                } else {
                    $('#nontextFieldEdit').hide();
                    $('#textFieldEdit').hide();
                }
            }
            ;
            
            $('#billerTypeCodeEdit').change(function () {
                var billerType = $('#billerTypeCodeEdit').val();
                if(billerType=="MOB"){
                    $('#mobPrefixHideEdit').show();
                }else{
                    $('#mobPrefixHideEdit').hide();
                    $('#mobPrefixEdit').val("");
                }    
            });

        </script>
        <style>
            .verticalLine {
                border-left:solid #e6e5e5;    
                height:100px;
                width:5em;
            }
            .firebaseclass{
                background: url(resouces/images/firebaselogo.svg) no-repeat;
                background-position: 74px;
                background-size: 100px;
                cursor: pointer;
                display: inline-block;
                height: 35px;
                width: 185px;
                padding: 9px;
                font-size: 13px;
                font-family: sans-serif;
                font-weight: bold;
                margin: 0 0 0 22px;
                border: 1px solid #cecece;
                transition: 0.3s;
            }
            .firebaseclass:hover{
                border: 1px solid black;
            }
            .megFirebase{
                padding: 9px;
                font-size: 12px;
                font-weight: bold;
                color: #FF5722;
                margin-left: -10px;
            }
        </style>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="billspedit" method="post" action="BillSP" theme="simple" cssClass="form" enctype="multipart/form-data" >
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
            <s:hidden id="hasImage" name="hasImage" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Bill Provider ID</label>
                        <s:textfield value="%{providerId}" cssClass="form-control" name="providerId" id="providerIdEdit" maxLength="20" readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Provider Name</label>
                        <s:textfield value="%{providerName}" cssClass="form-control" name="providerName" id="providerNameEdit" maxLength="50" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield value="%{description}" cssClass="form-control" name="description" id="descriptionEdit" maxLength="100" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Category</label>
                        <s:select value="%{categoryCode}" cssClass="form-control" id="categoryCodeEdit" list="%{categoryList}"  name="categoryCode" headerKey=""  headerValue="--Select Bill Category--" listKey="billercatcode" listValue="description"/>
                    </div>
                </div>
                <!--                <div class="col-sm-4">
                                    <div class="form-group">
                                        <span style="color: red">*</span><label>Image URL</label>
                <%--<s:textfield value="%{imageUrl}" cssClass="form-control" name="imageUrl" id="imageUrlEdit" maxLength="200" />--%>
            </div>
        </div>-->
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Display Name</label>
                        <s:textfield value="%{displayName}" cssClass="form-control" name="displayName" id="displayNameEdit" maxLength="100" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Collection Account</label>
                        <s:textfield value="%{collectionAccount}" cssClass="form-control" name="collectionAccount" id="collectionAccountEdit" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Code</label>
                        <s:textfield value="%{bankCode}" cssClass="form-control" name="bankCode" id="bankCodeEdit" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Biller Type</label>
                        <s:select value="%{billerTypeCode}" cssClass="form-control" id="billerTypeCodeEdit" list="%{billerTypeList}"  name="billerTypeCode" headerKey=""  headerValue="--Select Biller Type--" listKey="billerTypeCode" listValue="billerTypeDescription"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Currency</label>
                        <s:select value="%{currency}" cssClass="form-control" id="currencyEdit" list="%{currencyList}"  name="currency" headerKey=""  headerValue="--Select Currency--" listKey="currencyCode" listValue="description"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Charge</label>
                        <s:select value="%{feeCharge}" cssClass="form-control" id="feeChargeEdit" list="%{feeChargeList}"  name="feeCharge" headerKey=""  headerValue="--Select Fee Charge--" listKey="key" listValue="key"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Product Type</label>
                        <s:select value="%{productType}" cssClass="form-control" id="productTypeEdit" list="%{productTypeList}"  name="productType" headerKey=""  headerValue="--Select Product Type--" listKey="key" listValue="value"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Biller Value Code</label>
                        <s:textfield value="%{billerValCode}" cssClass="form-control" name="billerValCode" id="billerValCodeEdit" maxLength="20" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Validation Status</label>
                        <s:select value="%{validationStatus}" cssClass="form-control" id="validationStatusEdit" list="%{statusList}"  name="validationStatus" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>

            </div>        
            <div class="row row_popup">
                <div class="col-sm-4" id="mobPrefixHideEdit" style="display: none" >
                    <div class="form-group">
                        <span style="color: red">*</span><label>Mobile Prefix</label><span style="color: red"> Eg:-07X,0XXX</span>
                        <s:textfield value="%{mobPrefix}" cssClass="form-control" name="mobPrefix" id="mobPrefixEdit" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^0-9,]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9,]/g,''))" />
                    </div>
                </div>
<!--                <div class="col-sm-4" >
                    <div class="form-group">
                        <label>Place Holder</label>
                        <%--<s:textfield value="%{placeHolder}" cssClass="form-control" name="placeHolder" id="placeHolderEdit" maxLength="50" />--%>
                    </div>
                </div>-->
                <div class="col-sm-4" style="margin-top: 28px;">
                    <div class="form-group">
                        <label style="float: left;margin: 5px 9px 3px 7px">Biller Custom Fields</label>
                        <s:checkbox  name="havingCustomField" id="havingCustomFieldEdit"  cssStyle="width: 20px;height: 20px;"/>
                    </div>
                </div>
            </div>

            <div id="cusFieldsEdit" >
                <div class="row row_popup">
                    <div class="horizontal_line_popup"></div>
                </div>
                <div class="row row_popup" >
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Field Name</label>
                            <s:textfield value="%{fieldName}" cssClass="form-control" name="fieldName" id="fieldNameEdit" maxLength="100" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Field Type</label>
                            <s:select value="%{fieldIdType}" cssClass="form-control" id="fieldIdTypeEdit" list="%{fieldTypeList}"  name="fieldIdType" headerKey=""  headerValue="--Select Field Type--" listKey="id" listValue="name" onchange="setFieldTypeVarEdit($(this))" />
                        </div>
                    </div>
                    <div id="nontextFieldEdit"  style="display:none">    
                        <div class="col-sm-4">
                            <div class="form-group">
                                <!--<span style="color: red">*</span>-->
                                <label>Field Value (JSON)</label>
                                <s:textfield value="%{fieldVal}" cssClass="form-control" name="fieldVal" id="fieldValEdit" maxLength="200" onclick="FieldValueEdit()" readonly="true"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="textFieldEdit"  style="display:none">             
                    <div class="row row_popup" >
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Length Min</label>
                                <s:textfield value="%{fieldLenMin}" cssClass="form-control" name="fieldLenMin" id="fieldLenMinEdit" maxLength="4" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Length Max</label>
                                <s:textfield value="%{fieldLen}" cssClass="form-control" name="fieldLen" id="fieldLenEdit" maxLength="4" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Validation</label>
                                <s:select value="%{fieldValidation}" cssClass="form-control" id="fieldValidationEdit" list="%{regexList}"  name="fieldValidation" headerKey=""  headerValue="--Select Field Validation--" listKey="paramvalue" listValue="description"/>
                                <%--<s:textfield value="%{fieldValidation}" cssClass="form-control" name="fieldValidation" id="fieldValidationEdit" maxLength="150" onkeyup="$(this).val($(this).val().replace(/[ ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[ ]/g,''))"/>--%>
                            </div>
                        </div>
                    </div>          
                    <div class="row row_popup" >    
                        <div class="col-sm-4" >
                            <div class="form-group">
                                <label>Place Holder</label>
                                <s:textfield value="%{placeHolder}" cssClass="form-control" name="placeHolder" id="placeHolderEdit" maxLength="50" />
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-8">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Image (Maximum size (w*h) : 480x800 pixels, Valid extensions : gif,jpg,jpeg,png,img)</label>   
                        <div class="row">
                            <div class="col-sm-2">
                                <img onclick="mobileImgZoom(this.src)" class="image" src="<s:property value="imageUrl"/>" id="mobileImg_edit" name="mobileImgedit" alt="Mobile Image" >
                                <span id="editspanimg" class="ui-button-icon-primary ui-icon ui-icon-circle-close" style="position:absolute;top:10px;bottom:0;right:25%"></span>  
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-8" style="margin-top: 40px;">
                                <s:file name="mobileImg" id="mobileImgFile" onclick="changeMobileImageEdit();"/>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>            
            <div class="row row_popup">  
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="firebaseclass" onclick="uploadtofirebase_edit();">Upload to</div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <div id="up_e" style="display: none;color: #008a00" class="megFirebase">Images uploading....</div>
                        <div id="down_e" style="display: none;color: blue" class="megFirebase">Images uploaded successfully</div>
                        <div id="msg_e" style="display: none" class="megFirebase"></div>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Img  - Firebase URL</label>
                        <s:textarea readonly="true" name="imageUrl" id="imageUrlEdit" cssClass="form-control" maxLength="300" rows="2" value="%{imageUrl}"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>             
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="UpdateBillSP" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>

                </div>
            </div>
        </s:form>
        <sj:dialog 
            id="editfielddialog" 
            buttons="{ 
            'OK':function() { editFieldVal();$( this ).dialog( 'close' ); },
            'Cancel':function() { $( this ).dialog( 'close' );} 
            }" 
            autoOpen="false" 
            modal="true" 
            title="Edit Fields"          
            width="550" 
            />
    </body>
    <script>
        $(document).ready(function () {
            $('#cusFieldsEdit').hide();
            if ($('#havingCustomFieldEdit').prop('checked')) {
                $('#cusFieldsEdit').show();
            } else {
                $('#cusFieldsEdit').hide();
            }

            $('#havingCustomFieldEdit').click(function () {
                if ($('#havingCustomFieldEdit').prop('checked')) {

                    $('#fieldIdTypeEdit').val("");
                    $('#fieldValEdit').val("");
                    $('#fieldNameEdit').val("");
                    $('#fieldValidationEdit').val("");
                    $('#fieldLenEdit').val("");
                    $('#fieldLenMinEdit').val("");
                    $('#placeHolderEdit').val("");
                    $('#nontextFieldEdit').hide();
                    $('#textFieldEdit').hide();

                }
                $("#cusFieldsEdit").toggle(this.checked);
            });
        });

    </script>
</html>