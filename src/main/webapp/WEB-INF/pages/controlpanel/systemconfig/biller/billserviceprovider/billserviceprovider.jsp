<%-- 
    Document   : billserviceprovider
    Created on : Apr 17, 2019, 3:08:04 PM
    Author     : prathibha_w
--%>

<%@page import="com.epic.ndb.util.varlist.CommonVarList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <script src="resouces/js/firebaseconf.js" ></script>
        <script type="text/javascript">
//            $.subscribe('onclicksearch', function (event, data) {
            function search() {
                $('#message').empty();

                var providerIdSearch = $('#providerIdSearch').val();
                var providerNameSearch = $('#providerNameSearch').val();
                var categoryCodeSearch = $('#categoryCodeSearch').val();
                var displayNameSearch = $('#displayNameSearch').val();
                var collectionAccountSearch = $('#collectionAccountSearch').val();
                var billerTypeCodeSearch = $('#billerTypeCodeSearch').val();
                var descriptionSearch = $('#descriptionSearch').val();
                var statusSearch = $('#statusSearch').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        providerIdSearch: providerIdSearch,
                        providerNameSearch: providerNameSearch,
                        categoryCodeSearch: categoryCodeSearch,
                        displayNameSearch: displayNameSearch,
                        collectionAccountSearch: collectionAccountSearch,
                        billerTypeCodeSearch: billerTypeCodeSearch,
                        descriptionSearch: descriptionSearch,
                        statusSearch: statusSearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
//            );

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function editformatter(cellvalue, options, rowObject) {
                return "<a title='Edit' onClick='javascript:editBillSPInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            
            function viewformatter(cellvalue, options, rowObject) {
                return "<a title='View' onClick='javascript:viewDetailBillSPInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function deleteformatter(cellvalue, options, rowObject) {
                return "<a  title='Delete' onClick='javascript:deleteBillSPInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function confirmformatter(cellvalue, options, rowObject) {
                return "<a  title='Approve' onClick='javascript:confirmBillSP(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.providerId + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a  title='Reject' onClick='javascript:rejectBillSP(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.providerId + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            function viewdownloadeformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }

            function viewPendInit(keyval) {
                $("#viewpenddialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openviewpendtasktopage', function (event, data) {
                var $led = $("#viewpenddialog");
                $led.html("Loading..");
                $led.load("viewPendBillSP.action?id=" + $led.data('id'));
            });
            function confirmBillSP(keyval, popvar) {
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');


                return false;
            }

            function rejectBillSP(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function confirmBSP(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/ConfirmBillSP.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectBSP(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/RejectBillSP.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }


            function editBillSPInit(keyval) {
                $("#updatedialog").data('providerId', keyval).dialog('open');
            }

            function viewDetailBillSPInit(keyval) {
                $("#viewdetaildialog").data('providerId', keyval).dialog('open');
            }

            $.subscribe('opendetailviewtasktopage', function (event, data) {
                var $led = $("#viewdetaildialog");
                $led.html("Loading..");
                $led.load("viewPopupDetailBillSP.action?providerId=" + $led.data('providerId'));
            });
            
            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("DetailBillSP.action?providerId=" + $led.data('providerId'));
            });

            function deleteBillSPInit(keyval) {
                $('#divmsg').empty();

                $("#deletedialog").data('keyval', keyval).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete bill service provider ' + keyval + ' ?');
                return false;
            }

            function deleteBillSP(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/DeleteBillSP.action',
                    data: {providerId: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#deletesuccdialog").dialog('open');
                        $("#deletesuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function resetAllData() {
                $('#providerIdSearch').val("");
                $('#providerNameSearch').val("");
                $('#categoryCodeSearch').val("");
                $('#displayNameSearch').val("");
                $('#collectionAccountSearch').val("");
                $('#billerTypeCodeSearch').val("");
                $('#descriptionSearch').val("");
                $('#statusSearch').val("");

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        providerIdSearch: '',
                        providerNameSearch: '',
                        categoryCodeSearch: '',
                        displayNameSearch: '',
                        collectionAccountSearch: '',
                        billerTypeCodeSearch: '',
                        descriptionSearch: '',
                        statusSearch: '',
                        search: true
                    }
                });
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetFieldData() {

                $('#providerId').val("");
                $('#providerName').val("");
                $('#description').val("");
                $('#categoryCode').val("");
                $('#imageUrl').val("");
                $('#displayName').val("");
                $('#collectionAccount').val("");
                $('#bankCode').val("");
                $('#isHavingCustomField').val("");
                $('#payType').val("");
                $('#billerTypeCode').val("");
                $('#status').val("");
                $('#currency').val("");
                $('#feeCharge').val("");
                $('#productType').val("");
                $('#billerValCode').val("");
                $('#validationStatus').val("");
//                $('#fieldId').val("");
                $('#fieldIdType').val("");
                $('#fieldVal').val("");
                $('#fieldName').val("");
                $('#fieldValidation').val("");
                $('#fieldLen').val("");
                $('#fieldLenMin').val("");
                $('#placeHolder').val("");

                var mobPrefixHide = document.getElementById("mobPrefixHide");
                if(mobPrefixHide){
                    $('#mobPrefixHide').hide();
                    $('#mobPrefix').val("");
                }
                var cusFields = document.getElementById("cusFields");
                var havingCustomField = document.getElementById("havingCustomField");
                if (cusFields && havingCustomField) {
                    $('#cusFields').hide();
                    $('#nontextField').hide();
                    $('#textField').hide();
                    $("#havingCustomField").prop("checked", false);
                }

                $("#mobileImg_add").attr("src", "");
                $("#hasImageAdd").val('NOIMG');
                $('#mobileImg').val("");

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }

            function validateImage() {
                var status_size = "";
                if (file_mob_1_width > 480) {
                    status_size = "Mobile image width should be less than or equal to 480 pixels";
                } else if (file_mob_1_height > 800) {
                    status_size = "Mobile image height should be less than or equal to 800 pixels";
                } else {
                    status_size = ""
                }
                return status_size;
            }

            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('billspsearch');
                form.action = 'reportGenerateBillSP.action';
                form.submit();

                $('#view2').button("disable");
            }
            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;
                //  var isGenerate = false;

                if (recors > 0 && isGenerate == false) {
                    $('#view2').button("enable");
                } else {
                    $('#view2').button("disable");
                }
            });

        </script>
        <title></title>
    </head>
    <body style="">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set var="vupdatebutt"><s:property value="vupdatebutt" default="true"/></s:set>
                            <s:set var="vupdatelink"><s:property value="vupdatelink" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vupload"><s:property value="vupload" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form cssClass="form" id="billspsearch" method="post" action="BillSP" theme="simple" >
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                        <div class="row row_1">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Provider Id</label>
                                                <s:textfield cssClass="form-control" name="providerIdSearch" id="providerIdSearch" maxLength="20" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Provider Name</label>
                                                <s:textfield cssClass="form-control" name="providerNameSearch" id="providerNameSearch" maxLength="50" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Description</label>
                                                <s:textfield cssClass="form-control" name="descriptionSearch" id="descriptionSearch" maxLength="100" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Bill Category</label>
                                                <s:select  cssClass="form-control" name="categoryCode" id="categoryCodeSearch" list="%{categoryList}"   headerKey=""  headerValue="--Select Category--" listKey="billercatcode" listValue="description" value="%{categoryCode}" disabled="false"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Display Name</label>
                                                <s:textfield cssClass="form-control" name="displayNameSearch" id="displayNameSearch" maxLength="100" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Collection Account</label>
                                                <s:textfield cssClass="form-control" name="collectionAccountSearch" id="collectionAccountSearch" maxLength="50" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Biller Type</label>
                                                <s:select  cssClass="form-control" name="billerTypeCode" id="billerTypeCodeSearch" list="%{billerTypeList}"   headerKey=""  headerValue="--Select Biller Type--" listKey="billerTypeCode" listValue="billerTypeDescription" value="%{billreTypeCode}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Status</label>
                                                <s:select  cssClass="form-control" name="status" id="statusSearch" list="%{statusList}"   headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" value="%{status}" disabled="false"/>
                                            </div>
                                        </div>
                                    </div>
                                </s:form>
                                <div class="row row_1 form-inline">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true"
                                                value="Search" 
                                                href="#"
                                                disabled="#vsearch" 
                                                onClick="search()"
                                                id="searchbut"
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div> 
                                        <div class="form-group">
                                            <sj:submit 
                                                button="true" 
                                                value="Reset" 
                                                name="reset" 
                                                onClick="resetAllData()" 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;"
                                                />
                                        </div>
                                        <div class="form-group">
                                            <sj:submit 
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                button="true" 
                                                value="View CSV" 
                                                name="view2" 
                                                id="view2" 
                                                onClick="todocsv()" 
                                                disabled="#vgenerate"/> 
                                        </div>
                                    </div>
                                    <div class="col-sm-5"></div>
                                    <div class="col-sm-3  text-right">
                                        <div class="form-group">                                               
                                            <s:url var="addurl" action="ViewPopupBillSP"/>                                                    
                                            <sj:submit 
                                                openDialog="remotedialog"
                                                button="true"
                                                href="%{addurl}"
                                                disabled="#vadd"
                                                value="Add Service Provider"
                                                id="addButton"  
                                                cssClass="form-control btn_normal"
                                                cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                />
                                        </div>
                                    </div>
                                </div>
                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="remotedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Add Service Provider"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                    />  
                                <!-- Start update dialog box -->
                                <sj:dialog                                     
                                    id="updatedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Update Service Provider"
                                    onOpenTopics="openviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"

                                    />
                                <!-- Start detail dialog box -->
                                <sj:dialog                                     
                                    id="viewdetaildialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Service Provider"
                                    onOpenTopics="opendetailviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"

                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteBillSP($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete Service Provider"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process" 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error"
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmBSP($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectBSP($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />
                                <!-- Start Pend view dialog box -->
                                <sj:dialog                                     
                                    id="viewpenddialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Pending Service Provider"
                                    onOpenTopics="openviewpendtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="ListBillSP"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    shrinkToFit="false"
                                    > 
                                    <sjg:gridColumn name="providerId" index="u.providerId" title="View" width="40" align="center" formatter="viewformatter" sortable="false" />
                                    <sjg:gridColumn name="providerId" index="u.providerId" title="Edit" width="25" align="center" formatter="editformatter" sortable="false" hidden="#vupdatelink"/>
                                    <sjg:gridColumn name="providerId" index="u.providerId" title="Delete" width="40" align="center" formatter="deleteformatter" sortable="false" hidden="#vdelete"/>                                    
                                    <sjg:gridColumn name="providerId" index="u.providerId" title="Provider ID"  sortable="true"/>
                                    <sjg:gridColumn name="providerName" index="u.providerName" title="Provider Name"  sortable="true"/>
                                    <sjg:gridColumn name="categoryCode" index="u.billCategory" title="Bill Category"  sortable="true"/>
                                    <sjg:gridColumn name="description" index="u.description" title="Description"  sortable="true"/>
                                    <sjg:gridColumn name="imageUrl" index="u.imageUrl" title="Image URL"  sortable="true"/>
                                    <sjg:gridColumn name="displayName" index="u.displayName" title="Display Name"  sortable="true"/>
                                    <sjg:gridColumn name="collectionAccount" index="u.collectionAccount" title="Collection Account"  sortable="true"/>
                                    <sjg:gridColumn name="bankCode" index="u.bankCode" title="Bank Code"  sortable="true"/>
                                    <sjg:gridColumn name="payType" index="u.payType" title="Pay Type"  sortable="true"/>
                                    <sjg:gridColumn name="billreTypeCode" index="u.billBillerType" title="Biller Type"  sortable="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>
                                    <sjg:gridColumn name="currency" index="u.currency" title="Currency"  sortable="true"/>
                                    <sjg:gridColumn name="feeCharge" index="u.chargeCode" title="Fee Charge"  sortable="true"/>
                                    <sjg:gridColumn name="productType" index="u.productType" title="Product Type"  sortable="true"/>
                                    <sjg:gridColumn name="billerValCode" index="u.billerValCode" title="Biller Value Code"  sortable="true"/>
                                    <sjg:gridColumn name="validationStatus" index="u.validationStatus" title="Validation Status"  sortable="true"/>
                                    <sjg:gridColumn name="mobPrefix" index="u.mobPrefix" title="Mobile Prefix"  sortable="true"/>
                                    <%--<sjg:gridColumn name="placeHolder" index="u.placeHolder" title="Place Holder"  sortable="true"/>--%>
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="createdtime" index="u.createdtime" title="Created Date And Time"  sortable="true" />
                                    <sjg:gridColumn name="lastupdatedtime" index="u.lastupdatedtime" title="Last Updated Date And Time"  sortable="true" />
                                </sjg:grid> 
                            </div>

                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="ApproveListBillSP"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Bill Service Providers"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"  
                                    >

                                    <sjg:gridColumn name="id" index="u.id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="u.id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>
                                    <sjg:gridColumn name="id" index="u.id" title="View" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/> 

                                    <sjg:gridColumn name="providerId" index="u.id" title="Provider ID"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                                     
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="false"/>  
                                    <sjg:gridColumn name="createtime" index="u.createdtime" title="Created Date And Time"  sortable="false" />
                                </sjg:grid>  
                            </div>
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!-- end: BODY -->
    </body>
</html>
