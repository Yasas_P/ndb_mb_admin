<%-- 
    Document   : billserviceproviderinset
    Created on : Apr 17, 2019, 3:08:15 PM
    Author     : prathibha_w
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Bill Category</title> 
        <script type="text/javascript">
            $(document).ready(function () {
                $('#havingCustomField').click(function () {
                    if ($('#havingCustomField').prop('checked')) {
                        $('#fieldIdType').val("");
                        $('#fieldVal').val("");
                        $('#fieldName').val("");
                        $('#fieldValidation').val("");
                        $('#fieldLen').val("");
                        $('#fieldLenMin').val("");
                        $('#placeHolder').val("");
                        $('#nontextField').hide();
                        $('#textField').hide();
                    }
                    $("#cusFields").toggle(this.checked);
                });


                $.subscribe('resetAddButton', function (event, data) {
                    $('#amessage').empty();

                    $('#providerId').val("");
                    $('#providerName').val("");
                    $('#description').val("");
                    $('#categoryCode').val("");
//                    $('#imageUrl').val("");
                    $('#displayName').val("");
                    $('#collectionAccount').val("");
                    $('#bankCode').val("");
                    $('#payType').val("");
                    $('#billerTypeCode').val("");
                    $('#status').val("");
                    $('#currency').val("");
                    $('#feeCharge').val("");
                    $('#productType').val("");
                    $('#billerValCode').val("");
                    $('#validationStatus').val("");

//                $('#fieldId').val("");
                    $('#fieldIdType').val("");
                    $('#fieldVal').val("");
                    $('#fieldName').val("");
                    $('#fieldValidation').val("");
                    $('#fieldLen').val("");
                    $('#fieldLenMin').val("");
                    $('#placeHolder').val("");
                    $('#nontextField').hide();
                    $('#textField').hide();

                    $('#mobPrefixHide').hide();
                    $('#mobPrefix').val("");

                    $('#cusFields').hide();
                    $("#havingCustomField").prop("checked", false);
                    //edit by siva
                    $("#mobileImg_add").attr("src", "");
                    $("#hasImageAdd").val('NOIMG');

                    $("#up").hide();
                    $("#down").hide();
                    $("#msg").hide();
                    deletefirebase();
                });
            });


//            function FieldValue(val) {
//                $('#divmsg').empty();
//                $("#addfielddialog").data('keyval', val).dialog('open');
//
//                $("#addfielddialog").html('<label id="errormessageId" style="display:none; padding: 0px 22px;color: red;border: 1px solid;margin: 2px 21px;">ID already exist</label><div class="row row_popup"><div class="col-sm-6" style="padding:0"><div class="form-group"><label>ID</label><input class="form-control" id="id1" type="text" name="ss" /></div></div><div class="col-sm-6" style="padding:0"><div class="form-group"><label>Name</label><input class="form-control" id="id2" type="text" name="ss" /></div></div></div>');
//                return false;
//            }
            function FieldValue(val) {
                $('#divmsg').empty();
                var fieldVal = $("#fieldVal").val();
                $("#addfielddialog").data('keyval', val).dialog('open');

                $("#addfielddialog").
                        html('<label id="errormessageId" style="display:none; padding: 0px 22px;color: red;border: 1px solid;margin: 2px 21px;">ID already exist</label>\n\
                        <div class="row row_popup">\n\
                            <div class="col-sm-8" style="padding:0">\n\
                               <div class="form-group">\n\
                                   <label>Field Value (JSON)</label>\n\
                                   <textarea rows="4" cols="37" id="textJsonAdd" disabled="true"/>\n\
                               </div>\n\
                            </div>\n\
                            <div class="col-sm-4" style="padding-top:40px;">\n\
                                <button onclick="clearAdd()" class="btn btn-default btn-sm ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" >Clear Json</button>\n\
                            </div>\n\
                         </div>\n\
                        <div class="row row_popup">\n\
                            <div class="horizontal_line_popup"></div>\n\
                        </div>\n\
                        <div class="row row_popup">\n\
                            <div class="col-sm-4" style="padding:0">\n\
                                <div class="form-group">\n\
                                    <label>ID</label>\n\
                                    <input class="form-control" id="id1" type="text" name="ss" maxlength="10"/>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-sm-4" style="padding:0">\n\
                                <div class="form-group">\n\
                                    <label>Name</label>\n\
                                    <input class="form-control" id="id2" type="text" name="ss" maxlength="100"/>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-sm-4" style="padding-top:20px;">\n\
                                <div class="form-group">\n\
                                     <button onclick="addJason()" class="btn btn-sm active ui-button ui-widget ui-state-default ui-corner-all ui-state-hover" style="background-color: #ada9a9" style="padding:40px;">Add Field</button>\n\
                                </div>\n\
                            </div>\n\
                        </div>');
                $("#textJsonAdd").val(fieldVal);
                return false;
            }

            function clearAdd() {
                $("#textJsonAdd").val("");
                $("#errormessageId").hide();
            }

            function addJason() {
                var fieldVal = $("#textJsonAdd").val();
                var val1 = $("#id1").val();
                var val2 = $("#id2").val();
                var idval = true;
                if (val1) {
                    if (fieldVal) {
                        var jsonObj = $.parseJSON(fieldVal);

                        var searchTerm = val1;
                        var results = jsonObj.filter(function (obj) {
                            if (obj.id.indexOf(searchTerm) > -1) {
                                idval = false;
                                globBool = false;
                            }
                        });
                    }
                    if (idval) {
                        globBool = true;
                        var string = '{ "id":"' + val1 + '","name":"' + val2 + '"}';

                        if (fieldVal) {
                            fieldVal = fieldVal.substring(1, fieldVal.length - 1);

                            fieldVal += "," + string;

                            fieldVal = "[" + fieldVal + "]";

                            if (fieldVal.length > 200) {
                                $("#errormessageId").show();
                                $("#errormessageId").html("Field value maximum length should not exceed 200");
                            } else {
                                $("#textJsonAdd").val(fieldVal);
                                $("#id1").val("");
                                $("#id2").val("");
                                $("#errormessageId").hide();
                            }

                        } else {
                            fieldVal = "[" + string + "]";
                            if (fieldVal.length > 200) {
                                $("#errormessageId").show();
                                $("#errormessageId").html("Field value maximum length should not exceed 200");
                            } else {
                                $("#textJsonAdd").val(fieldVal);
                                $("#id1").val("");
                                $("#id2").val("");
                                $("#errormessageId").hide();
                            }
                        }
                    } else {
                        $("#errormessageId").show();
                        $("#errormessageId").html("ID already exist");
                    }
                } else {
                    $("#errormessageId").show();
                    $("#errormessageId").html("ID can not be empty");
                }
            }
            // validation
            var globBool = true;
            function addFieldVal() {
                var fieldvalue = $("#textJsonAdd").val();
                $("#fieldVal").val(fieldvalue);
                $("#textJsonAdd").val("");
            }
//            function addFieldVal(val1, val2) {
//
//                var fieldVal = $("#fieldVal").val();
//                var idval = true;
//
//                if (fieldVal) {
//                    var jsonObj = $.parseJSON(fieldVal);
//
//                    var searchTerm = val1;
//                    var results = jsonObj.filter(function (obj) {
//                        if (obj.id.indexOf(searchTerm) > -1) {
//                            idval = false;
//                            globBool = false;
//                        }
//                    });
//                }
//
//                if (idval) {
//                    globBool = true;
//                    var string = '{ "id":"' + val1 + '","name":"' + val2 + '"}';
//
//                    if (fieldVal) {
//                        fieldVal = fieldVal.substring(1, fieldVal.length - 1);
//
//                        fieldVal += "," + string;
//
//                        fieldVal = "[" + fieldVal + "]";
//                        $("#fieldVal").val(fieldVal);
//
//                    } else {
//                        fieldVal = "[" + string + "]";
//                        $("#fieldVal").val(fieldVal);
//                    }
//
//                } else {
//                    $("#errormessageId").show();
//                }
//            }
            //edit by siva
            $('#spanimg').click(function () {
                $('#mobileImg').val("");
                $("#mobileImg_add").attr("src", "");
                $('#imageUrl').val("");
                $("#hasImageAdd").val('NOIMG');
            });
            function changeMobileImage() {
                $("#mobileImg").change(function (event) {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#mobileImg_add").attr("src", tmppath);
                });
            }

            //========================= firebase ===================================

            var file_mob_1_width;
            var file_mob_1_height;

            $(document).ready(function () {
                var _URL = window.URL || window.webkitURL;
                $("#hasImageAdd").val('NOIMG');
                $("#mobileImg").change(function (e) {
                    var file, img;
                    if ((file = this.files[0])) {
                        img = new Image();
                        img.onload = function () {
                            file_mob_1_width = this.width;
                            file_mob_1_height = this.height;
                        };
                        img.onerror = function () {
                            alert("not a valid file: " + file.type);
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                    $("#hasImageAdd").val('IMG');
                });
            });



            function uploadtofirebase() {

                var file_mob_if = $('#mobileImg').val();
                var status = true;
                var status_size = validateImage();
//                var status_size = "";
                var imagecount = 0;
                var sendImageCount = 0;

                if (file_mob_if != "") {
                    imagecount++;
                }
                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    const file_mob_name = file_mob_if != "" ? $('#mobileImg').get(0).files[0].name : null
                    var fileNameExt = file_mob_name.substr(file_mob_name.lastIndexOf('.') + 1);
                    const validImageTypes = ['gif', 'jpg', 'jpeg', 'png', 'img'];
                    if (validImageTypes.includes(fileNameExt)) {
                        if (status_size == "") {

                            $("#msg").hide();

                            var metadata = {
                                contentType: 'image/jpeg',
                            };

                            const ref_mob = firebase.storage().ref('billerserviceprovider/');
                            const file_mob = file_mob_if != "" ? $('#mobileImg').get(0).files[0] : null;
//                        const file_mob = $('#mobileImg').get(0).files[0];

                            const name_mob = file_mob_if != "" ? file_mob.name : null;

                            $("#up").fadeIn();
                            $("#down").fadeOut();
                            //first image
                            if (file_mob_if != "") {
                                const task_mob = ref_mob.child(name_mob).put(file_mob, metadata).then(function () {
                                    ref_mob.child(name_mob).getDownloadURL().then(function (url) {
                                        console.log("mob - " + url);
                                        $("#imageUrl").val(url);
                                        sendImageCount++;
                                        if (imagecount == sendImageCount) {
                                            $("#up").hide();
                                            $("#down").fadeIn();
                                            $("#hasImageAdd").val('NOIMG');
                                        }
                                        return url;
                                    });
                                });
                            }

                            console.log(imagecount);
                            console.log(sendImageCount);
                        } else {
                            $("#up").hide();
                            $("#down").hide();
                            $("#msg").show();
                            $("#msg").html(status_size);
                        }
                    } else {
                        $("#up").hide();
                        $("#down").hide();
                        $("#msg").show();
                        $("#msg").html("Please upload image with one of the following extensions:gif,jpg,jpeg,png,img")
                    }
                } else {
                    $("#up").hide();
                    $("#down").hide();
                    $("#msg").show();
                    $("#msg").html("Please select the image...")
                }
            }

            function deletefirebase() {

                var file_mob_if = $('#mobileImg').val();
                var status = true;

                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    $("#msg").hide();
                    $("#up").hide();
                    $("#down").hide();

                    const ref_mob = firebase.storage().ref('billerserviceprovider/');
                    const file_mob = file_mob_if != "" ? $('#mobileImg').get(0).files[0] : null;

                    const name_mob = file_mob_if != "" ? file_mob.name : null;

                    if (file_mob_if != "") {
                        const task_mob = ref_mob.child(name_mob).delete().then(function () {
                            console.log("deleted");
                            $('#imageUrl').val("");
                            $('#mobileImg').val("");
                        }).catch(function (error) {
                            switch (error.code) {
                                case 'storage/object_not_found':
                                    break;
                                case 'storage/unauthorized':
                                    break;
                                case 'storage/canceled':
                                    break;
                                case 'storage/unknown':
                                    break;
                            }
                        });
                    }

                } else {
                    $('#mobileImg').val("");

                    $('#imageUrl').val("");
                }
            }

            function setFieldTypeVar(value) {
                var fieldType = value.val();
                $('#fieldVal').val("");
                $('#fieldValidation').val("");
                $('#fieldLen').val("");
                $('#fieldLenMin').val("");
                $('#placeHolder').val("");
                if (fieldType == '1') {
                    $('#nontextField').hide();
                    $('#textField').show()();
                } else if (fieldType == '2' || fieldType == '3') {
                    $('#nontextField').show();
                    $('#textField').hide();
                } else {
                    $('#nontextField').hide();
                    $('#textField').hide();
                }
            }
            ;

            $('#billerTypeCode').change(function () {
                var billerType = $('#billerTypeCode').val();
                if (billerType == "MOB") {
                    $('#mobPrefixHide').show();
                } else {
                    $('#mobPrefixHide').hide();
                    $('#mobPrefix').val("");
                }
            });
        </script>
        <style>
            .verticalLine {
                border-left:solid #e6e5e5;    
                height:75px;
                width:5em;
            }
            .firebaseclass{
                background: url(resouces/images/firebaselogo.svg) no-repeat;
                background-position: 74px;
                background-size: 100px;
                cursor: pointer;
                display: inline-block;
                height: 35px;
                width: 185px;
                padding: 9px;
                font-size: 13px;
                font-family: sans-serif;
                font-weight: bold;
                margin: 0 0 0 22px;
                border: 1px solid #cecece;
                transition: 0.3s;
            }
            .firebaseclass:hover{
                border: 1px solid black;
            }
            .megFirebase{
                padding: 9px;
                font-size: 12px;
                font-weight: bold;
                color: #FF5722;
                margin-left: -10px;
            }
        </style>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="billspadd" method="post" action="BillSP" theme="simple" cssClass="form" enctype="multipart/form-data" >
            <s:hidden id="hasImageAdd" name="hasImage" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Bill Provider ID</label>
                        <s:textfield value="%{providerId}" cssClass="form-control" name="providerId" id="providerId" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Provider Name</label>
                        <s:textfield value="%{providerName}" cssClass="form-control" name="providerName" id="providerName" maxLength="50" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield value="%{description}" cssClass="form-control" name="description" id="description" maxLength="100" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Bill Category</label>
                        <s:select value="%{categoryCode}" cssClass="form-control" id="categoryCode" list="%{categoryList}"  name="categoryCode" headerKey=""  headerValue="--Select Bill Category--" listKey="billercatcode" listValue="description"  />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Display Name</label>
                        <s:textfield value="%{displayName}" cssClass="form-control" name="displayName" id="displayName" maxLength="100" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Collection Account</label>
                        <s:textfield value="%{collectionAccount}" cssClass="form-control" name="collectionAccount" id="collectionAccount" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Bank Code</label>
                        <s:textfield value="%{bankCode}" cssClass="form-control" name="bankCode" id="bankCode" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Biller Type</label>
                        <s:select value="%{billerTypeCode}" cssClass="form-control" id="billerTypeCode" list="%{billerTypeList}"  name="billerTypeCode" headerKey=""  headerValue="--Select Biller Type--" listKey="billerTypeCode" listValue="billerTypeDescription"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Currency</label>
                        <s:select value="%{currency}" cssClass="form-control" id="currency" list="%{currencyList}"  name="currency" headerKey=""  headerValue="--Select Currency--" listKey="currencyCode" listValue="description"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Fee Charge</label>
                        <s:select value="%{feeCharge}" cssClass="form-control" id="feeCharge" list="%{feeChargeList}"  name="feeCharge" headerKey=""  headerValue="--Select Fee Charge--" listKey="key" listValue="key"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Product Type</label>
                        <s:select value="%{productType}" cssClass="form-control" id="productType" list="%{productTypeList}"  name="productType" headerKey=""  headerValue="--Select Product Type--" listKey="key" listValue="value"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Biller Value Code</label>
                        <s:textfield value="%{billerValCode}" cssClass="form-control" name="billerValCode" id="billerValCode" maxLength="20" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Validation Status</label>
                        <s:select value="%{validationStatus}" cssClass="form-control" id="validationStatus" list="%{statusList}"  name="validationStatus" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>

            </div>
            <div class="row row_popup" >
                <div class="col-sm-4" id="mobPrefixHide" style="display: none" >
                    <div class="form-group">
                        <span style="color: red">*</span><label>Mobile Prefix</label><span style="color: red"> Eg:-07X,0XXX</span>
                        <s:textfield value="%{mobPrefix}" cssClass="form-control" name="mobPrefix" id="mobPrefix" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^0-9,]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9,]/g,''))" />
                    </div>
                </div>
                <!--                <div class="col-sm-4" id="placeHolderHide" >
                                    <div class="form-group">
                                        <label>Place Holder</label>
                <%--<s:textfield value="%{placeHolder}" cssClass="form-control" name="placeHolder" id="placeHolder" maxLength="50"  />--%>
            </div>
        </div>-->
                <div class="col-sm-4" style="margin-top: 28px;">
                    <div class="form-group">
                        <label style="float: left;margin: 5px 9px 3px 7px">Biller Custom Fields</label>
                        <s:checkbox  name="havingCustomField" id="havingCustomField"  cssStyle="width: 20px;height: 20px;"/>
                    </div>
                </div>
            </div>

            <div id="cusFields"  style="display:none">
                <div class="row row_popup">
                    <div class="horizontal_line_popup"></div>
                </div>
                <div class="row row_popup" >
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Field Name</label>
                            <s:textfield value="%{fieldName}" cssClass="form-control" name="fieldName" id="fieldName" maxLength="100" />
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Field Type</label>
                            <s:select value="%{fieldIdType}" cssClass="form-control" id="fieldIdType" list="%{fieldTypeList}"  name="fieldIdType" headerKey=""  headerValue="--Select Field Type--" listKey="id" listValue="name" onchange="setFieldTypeVar($(this))" />
                        </div>
                    </div>
                    <div id="nontextField"  style="display:none">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <!--                            <span style="color: red">*</span>-->
                                <span style="color: red">*</span><label>Field Value (JSON)</label>
                                <s:textfield value="%{fieldVal}" cssClass="form-control" name="fieldVal" id="fieldVal"  maxLength="200" readonly="true" onclick="FieldValue()" cssStyle="cursor:pointer" placeholder="Click to add"/>
                                <span style="color:red;display: none" id="jsonValMessage" >Invalid JSON string</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="textField"  style="display:none">            
                    <div class="row row_popup" >
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Length Min</label>
                                <s:textfield value="%{fieldLenMin}" cssClass="form-control" name="fieldLenMin" id="fieldLenMin" maxLength="4" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Length Max</label>
                                <s:textfield value="%{fieldLen}" cssClass="form-control" name="fieldLen" id="fieldLen" maxLength="4" onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^0-9]/g,''))"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <span style="color: red">*</span><label>Field Validation</label>
                                <s:select value="%{fieldValidation}" cssClass="form-control" id="fieldValidation" list="%{regexList}"  name="fieldValidation" headerKey=""  headerValue="--Select Field Validation--" listKey="paramvalue" listValue="description"/>
                                <%--<s:textfield value="%{fieldValidation}" cssClass="form-control" name="fieldValidation" id="fieldValidation" maxLength="150" onkeyup="$(this).val($(this).val().replace(/[ ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[ ]/g,''))"/>--%>
                            </div>
                        </div>
                    </div>          
                    <div class="row row_popup" >
                        <div class="col-sm-4"  >
                            <div class="form-group">
                                <label>Place Holder</label>
                                <s:textfield value="%{placeHolder}" cssClass="form-control" name="placeHolder" id="placeHolder" maxLength="50"  />
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">        
                <div class="col-sm-8">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Image (Maximum size (w*h) : 480x800 pixels, Valid extensions : gif,jpg,jpeg,png,img)</label>   
                        <div class="row">
                            <div class="col-sm-2">
                                <img class="image" src="data:image/jpeg;base64,<s:property value="mobileImgedit"/>" id="mobileImg_add" alt="Mobile Image" >
                                <span id="spanimg" class="ui-button-icon-primary ui-icon ui-icon-circle-close" style="position:absolute;top:10px;bottom:0;right:20%"></span>  
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-8" style="margin-top: 40px;">
                                <s:file name="mobileImg" id="mobileImg" onclick="changeMobileImage();"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">  
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="firebaseclass" onclick="uploadtofirebase();">Upload to</div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <div id="up" style="display: none;color: #008a00" class="megFirebase">Images uploading....</div>
                        <div id="down" style="display: none;color: blue" class="megFirebase">Images uploaded successfully</div>
                        <div id="msg" style="display: none" class="megFirebase"></div>
                    </div>
                </div>
            </div>   
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Img - Firebase URL</label>
                        <s:textarea readonly="true" name="imageUrl" id="imageUrl" cssClass="form-control" maxLength="300" rows="2"/>
                    </div>
                </div>
            </div>             
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>            
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="AddBillSP" var="inserturl"/>
                        <sj:submit
                            button="true"
                            value="Add"
                            href="%{inserturl}"
                            onClickTopics=""
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>

                </div>
            </div>
        </s:form>
        <sj:dialog 
            id="addfielddialog" 
            buttons="{ 
            'OK':function() { addFieldVal();$( this ).dialog( 'close' ); },
            'Cancel':function() { $( this ).dialog( 'close' );} 
            }" 
            autoOpen="false" 
            modal="true" 
            title="Add Fields"          
            width="550" 
            />
    </body>
</html>