<%-- 
    Document   : segmentedit
    Created on : Nov 8, 2018, 8:42:42 AM
    Author     : jayathissa_d
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Segment</title> 
        <script type="text/javascript">
            function editSegment(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/FindSegment.action',
                    data: {segmentcode: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#segmentcodeEdit').val("");
                            $('#segmentcodeEdit').attr('readOnly', false);
                            $("#segmentcodeEdit").css("color", "black");
                            $('#descriptionEdit').val("");
                            $('#statusEdit').val("");
                            $('#waveoffedit').val("");
                            $('#chargeAmountedit').val("");
                            $('#divmsg').text("");
                        }
                        else {
                            $('#segmentcodeEdit').val(data.segmentcode);
                            $('#segmentcodeEdit').attr('readOnly', true);
                            $("#segmentcodeEdit").css("color", "#858585");
                            $('#descriptionEdit').val(data.description);
                            $('#statusEdit').val(data.status);
                            $('#waveoffedit').val(data.waveoff);
                            $('#chargeAmountedit').val(data.chargeAmount);
                        }
                    },
                    error: function(data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var code = $('#segmentcodeEdit').val();
                editSegment(code);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="segmentedit" method="post" action="Segment" theme="simple" cssClass="form" >
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Segment Code</label>
                        <s:textfield value="%{segmentcode}" cssClass="form-control" name="segmentcode" id="segmentcodeEdit" maxLength="150"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Description</label>
                        <s:textfield value="%{description}" cssClass="form-control" name="description" id="descriptionEdit" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Waive Off Status</label>
                        <s:select value="%{waveoff}" cssClass="form-control" name="waveoff" id="waveoffedit" headerValue="-- Select Waive Off --" list="%{waveOffStatusList}"   headerKey="" listKey="statuscode" listValue="description" />
                    </div>  
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Charge Amount</label>
                        <s:textfield value="%{chargeAmount}" cssClass="form-control" name="chargeAmount" id="chargeAmountedit" maxLength="8" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div>
            </div>  
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">
                    
                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="UpdateSegment" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
