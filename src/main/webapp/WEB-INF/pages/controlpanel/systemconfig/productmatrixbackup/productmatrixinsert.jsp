<%-- 
    Document   : productmatrixinsert
    Created on : Apr 16, 2019, 8:47:50 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Mobile Bank Locator</title> 
        <script type="text/javascript">
            $.subscribe('resetAddButton', function(event, data) {
                $('#amessage').empty();
                $('#debitProductType').val("");
                $('#creditProductType').val("");
                $('#debitCurrency').val("");
                $('#status').val("");
                toleftall();
            });
            
            function clickAdd() {

                $('#currentCreditCurrencyBox option').prop('selected', true);
                $('#newCreditCurrencyBox option').prop('selected', true);
                $('#productmatrixadd').submit();
            }
            //----------- Segment Type Mgt------------------------
            function toleft() {
                $("#currentCreditCurrencyBox option:selected").each(function () {

                    $("#newCreditCurrencyBox").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function toright() {
                $("#newCreditCurrencyBox option:selected").each(function () {

                    $("#currentCreditCurrencyBox").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function toleftall() {
                $("#currentCreditCurrencyBox option").each(function () {

                    $("#newCreditCurrencyBox").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });
            }

            function torightall() {
                $("#newCreditCurrencyBox option").each(function () {

                    $("#currentCreditCurrencyBox").append($('<option>', {
                        value: $(this).val(),
                        text: $(this).text()
                    }));
                    $(this).remove();
                });

            }

        </script>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="productmatrixadd" method="post" action="addProductMatrix" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Debit Product Type </label>
                        <s:select cssClass="form-control" name="debitProductType" id="debitProductType" list="%{productTypeList}"   headerKey=""  headerValue="--Select Debit Product Type--" listKey="productType" listValue="productName" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Credit Product Type</label>
                        <s:select cssClass="form-control" name="creditProductType" id="creditProductType" list="%{productTypeList}"   headerKey=""  headerValue="--Select Credit Product Type--" listKey="productType" listValue="productName" />
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group ">
                        <span style="color: red">*</span><label >Debit Currency</label>
                        <s:select  cssClass="form-control" name="debitCurrency" id="debitCurrency" list="%{productCurrencyList}"   headerKey=""  headerValue="--Select Debit Currency--" listKey="currencyCode" listValue="description" disabled="false"/>
                    </div>
                </div>    
            </div>
            <div class="row row_popup">
                <div class="col-sm-6">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Credit Currency</label>
                    </div>
                </div>
            </div>        
            <div class="row row_popup">
                <div class="col-sm-5">
                    <div class="form-group">
                        <s:select cssClass="form-control" multiple="true"
                                  name="newCreditCurrencyBox" id="newCreditCurrencyBox" list="newCreditCurrencyList"									 
                                  ondblclick="toright()" style="height:160px;"  listKey="key" listValue="value"/>
                    </div>                
                </div>
                <div class="col-sm-2 text-center">
                    <div class="form-group">
                        <div class="row" style="height: 20px;"></div>
                        <div class="row">
                            <sj:a
                                id="right" 
                                onClick="toright()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> > </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="rightall" 
                                onClick="torightall()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size: 10px;width:60px;margin:4px;"> >> </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="left" 
                                onClick="toleft()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> < </sj:a>
                            </div>
                            <div class="row">
                            <sj:a
                                id="leftall" 
                                onClick="toleftall()" 
                                button="true"
                                cssClass="ui-button-move"
                                style="font-size:10px;width:60px;margin:4px;"> << </sj:a>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-5">
                        <div class="form-group"> 
                        <s:select cssClass="form-control" multiple="true" 
                                  name="currentCreditCurrencyBox" id="currentCreditCurrencyBox" list="currentCreditCurrencyList"									 
                                  ondblclick="toleft()" style="height:160px;" />

                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <%--<s:url action="addProductMatrix" var="inserturl"/>--%>
                        <sj:submit
                            button="true"
                            value="Add"
                            onclick="clickAdd()"
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>
                   
                </div>
            </div>
        </s:form>
    </body>
</html>
