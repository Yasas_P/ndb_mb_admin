<%--
  User: ruwan_e
  Date: 1/16/14
  Time: 3:16 PM
--%>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<!DOCTYPE html>

<html>
    <head>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
            
            function confirmformatter(cellvalue, options, rowObject) {
                return "<a  title='Approve' onClick='javascript:confirmPasswordPolicy(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.providerId + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a  title='Reject' onClick='javascript:rejectPasswordPolicy(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.providerId + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            
            function viewdownloadeformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
            }
            
            function viewPendInit(keyval) {
                $("#viewpenddialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openviewpendtasktopage', function (event, data) {
                var $led = $("#viewpenddialog");
                $led.html("Loading..");
                $led.load("viewPendPasswordPolicy.action?id=" + $led.data('id'));
            });
            function confirmPasswordPolicy(keyval, popvar) {
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');


                return false;
            }

            function rejectPasswordPolicy(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function confirmPP(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmPasswordPolicy.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectPP(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectPasswordPolicy.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            window.onload = function() {
//                $("#passwordpolicyid").css("color", "#858585");
                var passwordpolicyid = $('#passwordpolicyid').val();
                if(passwordpolicyid=='3'){
                     $('#nonNameValueHide').hide();
                }
            };

            function editPasswordPolicy(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findPasswordPolicy',
                    data: {passwordpolicyid: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                        $('#divmsg').empty();
                        var msg = data.message;
                        if (msg) {
                            alert(data.message);
                        }
                        else {
                            $('#oldvalue').val(data.oldvalue);
//                            $('#passwordpolicyid').attr('readOnly', true);
//                            $("#passwordpolicyid").css("color", "#858585");
                            $('#passwordpolicyid').val(data.passwordpolicyid);
                            $('#minimumlength').val(data.minimumlength);
                            $('#maximumlength').val(data.maximumlength);
                            $('#minimumspecialcharacters').val(data.minimumspecialcharacters);
                            $('#minimumuppercasecharacters').val(data.minimumuppercasecharacters);
                            $('#minimumnumericalcharacters').val(data.minimumnumericalcharacters);
                            $('#minimumlowercasecharacters').val(data.minimumlowercasecharacters);
                            $('#repeatcharactersallow').val(data.repeatcharactersallow);
//                            $('#initialpasswordexpirystatus').val(data.initialpasswordexpirystatus);
                            $('#passwordexpiryperiod').val(data.passwordexpiryperiod);
                            $('#noofhistorypassword').val(data.noofhistorypassword);
                            $('#minimumpasswordchangeperiod').val(data.minimumpasswordchangeperiod);
                            $('#idleaccountexpiryperiod').val(data.idleaccountexpiryperiod);
                            $('#noofinvalidloginattempt').val(data.noofinvalidloginattempt);
//                            $('#addButton').button("disable");
                            $('#updateButton').button("enable");
                        }
                    },
                    error: function(data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
//                        $("#deleteerrordialog").html("Error occurred while processing.").dialog('open');
                    }
                });
            }
            
            function changePasswordPolicy(){
                var passwordpolicyid = $('#passwordpolicyid').val();
//                $('#passwordpolicyid').attr('readOnly', true);
                if(passwordpolicyid=='3'){
                     $('#nonNameValueHide').hide();
                }else{
                    $('#nonNameValueHide').show();
                }
                editPasswordPolicy(passwordpolicyid);
            }

            function resetAllData() {

                var s = $("#resetButton").is(':disabled')
                if (s == true) {
//                    $('#passwordpolicyid').attr('readOnly', false);
//                    $('#passwordpolicyid').val("");
//                    $("#passwordpolicyid").css("color", "black");
                    $('#minimumlength').val("");
                    $('#maximumlength').val("");
                    $('#minimumspecialcharacters').val("");
                    $('#minimumuppercasecharacters').val("");
                    $('#minimumnumericalcharacters').val("");
                    $('#minimumlowercasecharacters').val("");
                    $('#repeatcharactersallow').val("");
//                    $('#initialpasswordexpirystatus').val("");
                    $('#passwordexpiryperiod').val("");
                    $('#noofhistorypassword').val("");
                    $('#minimumpasswordchangeperiod').val("");
                    $('#idleaccountexpiryperiod').val("");
                    $('#noofinvalidloginattempt').val("");
//                    $('#addButton').button("enable");
//                    $('#updateButton').button("disable");
                    $('#divmsg').text("");
                } else {
//                    print("ssssELSEEEEE");
                    var passwordpolicyid = $('#passwordpolicyid').val();
//                    $('#passwordpolicyid').attr('readOnly', true);
                    editPasswordPolicy(passwordpolicyid);
                }

            }

            function resetFieldData() {
                $.ajax({
                    url: '${pageContext.request.contextPath}/resetPasswordPolicy.action',
                    data: {},
                    dataType: "json",
                    type: "POST",
                    success: function(data) {
                        var msg = data.message;
                        if (msg) {
                            $('#passwordpolicyid').attr('readOnly', false);
                            $('#passwordpolicyid').val("");
                            $("#passwordpolicyid").css("color", "black");
                            $('#minimumlength').val("");
                            $('#maximumlength').val("");
                            $('#minimumspecialcharacters').val("");
                            $('#minimumuppercasecharacters').val("");
                            $('#minimumnumericalcharacters').val("");
                            $('#minimumlowercasecharacters').val("");
                            $('#repeatcharactersallow').val("");
//                            $('#initialpasswordexpirystatus').val("");
                            $('#passwordexpiryperiod').val("");
                            $('#noofhistorypassword').val("");
                            $('#minimumpasswordchangeperiod').val("");
                            $('#idleaccountexpiryperiod').val("");
                            $('#noofinvalidloginattempt').val("");
//                            $('#addButton').button("enable");
                            $('#updateButton').button("disable");
                            $('#nonNameValueHide').show();
                            
                            $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                            jQuery("#gridtablePend").trigger("reloadGrid");
                        }
                        else {
                            $('#oldvalue').val(data.oldvalue);
//                            $('#passwordpolicyid').attr('readOnly', true);
                            $('#passwordpolicyid').val(data.passwordpolicyid);
//                            $("#passwordpolicyid").css("color", "#858585");
                            $('#minimumlength').val(data.minimumlength);
                            $('#maximumlength').val(data.maximumlength);
                            $('#minimumspecialcharacters').val(data.minimumspecialcharacters);
                            $('#minimumuppercasecharacters').val(data.minimumuppercasecharacters);
                            $('#minimumnumericalcharacters').val(data.minimumnumericalcharacters);
                            $('#minimumlowercasecharacters').val(data.minimumlowercasecharacters);
                            $('#repeatcharactersallow').val(data.repeatcharactersallow);
//                            $('#initialpasswordexpirystatus').val(data.initialpasswordexpirystatus);
                            $('#passwordexpiryperiod').val(data.passwordexpiryperiod);
                            $('#noofhistorypassword').val(data.noofhistorypassword);
                            $('#minimumpasswordchangeperiod').val(data.minimumpasswordchangeperiod);
                            $('#idleaccountexpiryperiod').val(data.idleaccountexpiryperiod);
                            $('#noofinvalidloginattempt').val(data.noofinvalidloginattempt);
//                            $('#addButton').button("disable");
                            $('#updateButton').button("enable");
                            if(data.passwordpolicyid=='3'){
                                $('#nonNameValueHide').hide();
                            }else{
                               $('#nonNameValueHide').show();
                            }
                            $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                            jQuery("#gridtablePend").trigger("reloadGrid");
                        }
                    },
                    error: function(data) {
//                        $("#deleteerrordialog").html("Error occurred while processing.").dialog('open');
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });

            }

        </script>
        <title></title>
    </head>
    <body style="">
        <jsp:include page="/header.jsp"/>

        <div class="main-container">


            <jsp:include page="/leftmenu.jsp"/>

            <div class="main-content">

                <div class="container" style="min-height: 760px;">


                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->

                    <div class="row">


                        <div id="content1">


                            <s:div id="divmsg">

                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set id="vadd" var="vadd">
                                <s:property value="vadd" default="true"/>
                            </s:set>
                            <s:set var="vupdatebutt">
                                <s:property value="vupdatebutt" default="true"/>
                            </s:set>
                            <s:set var="vupdatelink">
                                <s:property value="vupdatelink" default="true"/>
                            </s:set>
                            <s:set var="vdelete">
                                <s:property value="vdelete" default="true"/>
                            </s:set>                            
                            <s:set var="policyid">
                                <s:property value="policyid" default="true"/>
                            </s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>

                            <div id="formstyle">

                                <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
                                <s:url var="addurl" action="addPasswordPolicy"/>
                                <s:url var="updateurl" action="updatePasswordPolicy"/>

                                <s:form id="customercategoryadd" method="post" action="PasswordPolicy" theme="simple">

                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <span style="color: red">*</span><label>Password Policy </label>
                                                <s:select  cssClass="form-control" name="passwordpolicyid" id="passwordpolicyid" list="%{passwordpolicyList}"  listKey="key" listValue="value" value="%{passwordpolicyid}" disabled="false" onchange="changePasswordPolicy()"/>
                                                <%--<s:textfield cssClass="form-control" name="passwordpolicyid" id="passwordpolicyid"--%>                                                        
<!--                                                             maxLength="5" 
                                                             onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))" 
                                                             readonly="#policyid"/>  -->                                     
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <span style="color: red">*</span><label>Minimum Length</label>
                                                <s:textfield cssClass="form-control" name="minimumlength" id="minimumlength"
                                                             maxLength="2"
                                                             onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                             />                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <span style="color: red">*</span><label>Maximum Length</label>
                                                <s:textfield cssClass="form-control" name="maximumlength" id="maximumlength"
                                                             maxLength="2"
                                                             onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                             />                                        </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <span style="color: red">*</span><label>Minimum Special Characters</label>
                                                <s:textfield cssClass="form-control" name="minimumspecialcharacters" id="minimumspecialcharacters"
                                                             maxLength="2"
                                                             onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                             />                                                
                                            </div>
                                        </div>       

                                    </div>

                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <span style="color: red">*</span><label>Minimum Upper Case Characters</label>
                                                <s:textfield cssClass="form-control" name="minimumuppercasecharacters" id="minimumuppercasecharacters"
                                                             maxLength="2"
                                                             onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                             />                                          </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <span style="color: red">*</span><label>Minimum Lower Case Characters</label>
                                                <s:textfield cssClass="form-control" name="minimumlowercasecharacters" id="minimumlowercasecharacters"
                                                             maxLength="2"
                                                             onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                             />                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <span style="color: red">*</span><label>Minimum Numerical Characters</label>
                                                <s:textfield cssClass="form-control" name="minimumnumericalcharacters" id="minimumnumericalcharacters"
                                                             maxLength="2"
                                                             onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                             />                                       </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <span style="color: red">*</span><label>Allowed Repeat Characters</label>
                                                <s:textfield cssClass="form-control" name="repeatcharactersallow" id="repeatcharactersallow"
                                                             maxLength="2"
                                                             onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                             />                                              
                                            </div>
                                        </div>       

                                    </div>

                                   <div id ="nonNameValueHide">         
                                        <div class="row row_1">
                                            <div class="col-sm-3">
                                                <div class="form-group form-inline">
                                                    <span style="color: red">*</span><label>Password Expiry Period</label>
                                                    <s:textfield cssClass="form-control" name="passwordexpiryperiod" id="passwordexpiryperiod"
                                                                 maxLength="5"
                                                                 onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                                 /> (days)                                         
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group form-inline">
                                                    <span style="color: red">*</span><label>Password Expiry Notification Period</label>
                                                    <s:textfield cssClass="form-control" name="minimumpasswordchangeperiod" id="minimumpasswordchangeperiod"
                                                                 maxLength="5"
                                                                 onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                                 />  (days)                                          </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group form-inline">
                                                    <span style="color: red">*</span><label>No. of History Passwords</label>
                                                    <s:textfield cssClass="form-control" name="noofhistorypassword" id="noofhistorypassword"
                                                                 maxLength="5"
                                                                 onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                                 />                                       </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group form-inline">
                                                    <span style="color: red">*</span><label>Idle Account Expiry Period</label>
                                                    <s:textfield cssClass="form-control" name="idleaccountexpiryperiod" id="idleaccountexpiryperiod"
                                                                 maxLength="5"
                                                                 onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                                 /> (days)                                            
                                                </div>
                                            </div>       

                                        </div>

                                        <div class="row row_1">
                                            <div class="col-sm-3">
                                                <div class="form-group form-inline">
                                                    <span style="color: red">*</span><label>No. of Invalid Login Attempts</label>
                                                    <s:textfield cssClass="form-control" name="noofinvalidloginattempt" id="noofinvalidloginattempt"
                                                                 maxLength="5"
                                                                 onkeyup="$(this).val($(this).val().replace(/[^0-9]/g,''))"
                                                                 />                                         </div>
                                            </div>



                                        </div>
                                    </div>    
                                    <div class="row row_1">
                                        <div class="col-sm-4">
                                            <div class="form-group form-inline">
                                                <span class="mandatoryfield">Mandatory fields are marked with *</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row row_1"></div>
                                    <div class="row row_1 form-inline">
                                        <!--<div class="col-sm-2"></div>-->
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <sj:submit button="true" 
                                                           href="%{updateurl}" 
                                                           value="Update" 
                                                           targets="divmsg"   
                                                           id="updateButton"
                                                           cssClass="form-control btn_normal"
                                                           cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                           />

                                            </div>
                                            <div class="form-group">
                                                <sj:submit button="true" 
                                                           value="Reset" 
                                                           name="reset" 
                                                           onClick="resetAllData()"
                                                           cssClass="form-control btn_normal"
                                                           cssStyle="border-radius: 12px;" />

                                            </div> 
                                        </div>

                                    </div>


                                </s:form>
                            </div>
                            <!-- Start delete confirm dialog box -->
                            <sj:dialog 
                                id="deletedialog" 
                                buttons="{ 
                                'OK':function() { deletePage($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Delete Page"                            
                                />
                            <!-- Start delete process dialog box -->
                            <sj:dialog 
                                id="deletesuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                title="Deleting Process." 
                                />
                            <!-- Start delete error dialog box -->
                            <sj:dialog 
                                id="deleteerrordialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}                                    
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                title="Delete error."
                                />
                             <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmPP($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                            <!-- Start approve process dialog box -->
                            <sj:dialog 
                                id="confirmsuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                width="350"
                                title="Requested Operation Approving Process" 
                                />

                            <!-- Start reject confirm dialog box -->
                            <sj:dialog 
                                id="rejectdialog" 
                                buttons="{ 
                                'OK':function() { rejectPP($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                'Cancel':function() { $( this ).dialog( 'close' );} 
                                }" 
                                autoOpen="false" 
                                modal="true" 
                                dialogClass= "fixed-dialog"
                                width="600"
                                height="300"
                                title="Reject Requested Operation"                            
                                />
                            <!-- Start reject process dialog box -->
                            <sj:dialog 
                                id="rejectsuccdialog" 
                                buttons="{
                                'OK':function() { $( this ).dialog( 'close' );}
                                }"  
                                autoOpen="false" 
                                modal="true" 
                                dialogClass= "fixed-dialog"
                                width="350"
                                title="Requested Operation Rejecting Process" 
                                />
                            <!-- Start Pend view dialog box -->
                            <sj:dialog                                     
                                id="viewpenddialog"                                 
                                autoOpen="false" 
                                modal="true" 
                                position="center"
                                title="View Pending Password Policy"
                                onOpenTopics="openviewpendtasktopage" 
                                loadingText="Loading .."
                                width="900"
                                height="450"
                                dialogClass= "fixed-dialog"
                                />

                        <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="approveListPasswordPolicy"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Password Policy"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"  
                                    >

                                    <sjg:gridColumn name="id" index="u.id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="u.id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>
                                    <sjg:gridColumn name="id" index="u.id" title="View" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/> 

                                    <sjg:gridColumn name="passwordPolicy" index="u.id" title="Password Policy"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                                     
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="false"/>  
                                    <sjg:gridColumn name="createtime" index="u.createdtime" title="Created Date And Time"  sortable="false" />
                                </sjg:grid>  
                            </div> 
                        </div>   
                    </div>
                </div>
            </div>
        </div>                            
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->



    </body>
</html>
