<%-- 
    Document   : transactionlimitinsert
    Created on : Apr 8, 2019, 1:41:13 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Mobile Bank Locator</title> 
        <script type="text/javascript">
            $.subscribe('resetAddButton', function(event, data) {
                $('#amessage').empty();
                $('#transferType').val("");
                $('#segmentType').val("");
                $('#status').val("");
                $('#defaultLimit').val("");
                $('#tranLimitMin').val("");
                $('#tranLimitMax').val("");
                $('#dailyLimitMin').val("");
                $('#dailyLimitMax').val("");
            });
        </script>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="transactionlimitadd" method="post" action="TransactionLimitMgt" theme="simple" cssClass="form" >
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Transfer Type </label>
                        <s:select cssClass="form-control" name="transferType" id="transferType" list="%{transferTypeList}"   headerKey=""  headerValue="--Select Transfer Type--" listKey="transferId" listValue="description" value="%{transferType}" disabled="false"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Segment Type </label>
                        <s:select cssClass="form-control" name="segmentType" id="segmentType" list="%{segmentTypeList}"   headerKey=""  headerValue="--Select Segment Type--" listKey="segmentcode" listValue="description" value="%{segmentType}" disabled="false"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Default Limit</label>
                        <s:textfield cssClass="form-control" name="defaultLimit" id="defaultLimit" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Tran Limit Min</label>
                        <s:textfield cssClass="form-control" name="tranLimitMin" id="tranLimitMin" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Tran Limit Max</label>
                        <s:textfield cssClass="form-control" name="tranLimitMax" id="tranLimitMax" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Daily Limit Min</label>
                        <s:textfield cssClass="form-control" name="dailyLimitMin" id="dailyLimitMin" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Daily Limit Max</label>
                        <s:textfield cssClass="form-control" name="dailyLimitMax" id="dailyLimitMax" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div> 
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="addTransactionLimitMgt" var="inserturl"/>
                        <sj:submit
                            button="true"
                            value="Add"
                            href="%{inserturl}"
                            onClickTopics=""
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>
                   
                </div>
            </div>
        </s:form>
    </body>
</html>
