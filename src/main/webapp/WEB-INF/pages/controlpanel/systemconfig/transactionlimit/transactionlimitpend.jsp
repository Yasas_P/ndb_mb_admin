<%-- 
    Document   : transactionlimitpend
    Created on : Aug 7, 2019, 2:26:28 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View Pend Customer Search</title> 
    </head>
    <body>
        <s:div id="pmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="transactionlimitpend" method="post" action="TransactionLimitMgt" theme="simple" cssClass="form" enctype="multipart/form-data"  >
            <s:hidden id="taskCodePendView" name="taskCode" value="%{taskCode}" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>Transfer Type</label>
                        <s:textfield value="%{transferType}" cssClass="form-control" name="transferType" id="transferType_p" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Segment Type</label>
                        <s:textfield value="%{segmentType}" cssClass="form-control"  name="segmentType" id="segmentType_p" readonly="true"  />
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Task</label>
                        <s:textfield value="%{taskDescription}" cssClass="form-control" name="task" id="task_p"  readonly="true" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Field(s)</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Old Value</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>New Value</label>
                    </div>
                </div>
            </div>           
            <div class="row row_popup" style="height: 16px">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Status</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{statusOld}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="statusOld" id="statusOld_p" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{status}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="status" id="status_p" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Default Limit</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label  value="%{defaultLimitOld}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="defaultLimitOld" id="defaultLimitOld_p" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{defaultLimit}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="defaultLimit" id="defaultLimit_p" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Tran Limit Min</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{tranLimitMinOld}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="tranLimitMinOld" id="tranLimitMinOld_p" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{tranLimitMin}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="tranLimitMin" id="tranLimitMin_p" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Tran Limit Max</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{tranLimitMaxOld}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="tranLimitMaxOld" id="tranLimitMaxOld_p" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{tranLimitMax}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="tranLimitMax" id="tranLimitMax_p" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Daily Limit Min</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{dailyLimitMinOld}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="dailyLimitMinOld" id="dailyLimitMinOld_p" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{dailyLimitMin}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="dailyLimitMin" id="dailyLimitMin_p" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3" >
                    <div class="form-group">
                        <label>Daily Limit Max</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{dailyLimitMaxOld}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="dailyLimitMaxOld" id="dailyLimitMaxOld_p" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:label value="%{dailyLimitMax}" cssClass="form-control" style="margin-bottom: 0px; padding-top: 7px; border-radius: 4px !important;" name="dailyLimitMax" id="dailyLimitMax_p" />
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
