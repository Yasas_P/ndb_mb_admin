<%-- 
    Document   : transactionlimit
    Created on : Apr 8, 2019, 11:20:53 AM
    Author     : sivaganesan_t
--%>

<%@page import="com.epic.ndb.util.varlist.CommonVarList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <script type="text/javascript">
//            $.subscribe('onclicksearch', function (event, data) {
            function search(){
                $('#message').empty();

                var transferTypeSearch = $('#transferTypeSearch').val();
                var segmentTypeSearch = $('#segmentTypeSearch').val();
                var statusSearch = $('#statusSearch').val();
                var defaultLimitSearch = $('#defaultLimitSearch').val();
                var dailyLimitMinSearch = $('#dailyLimitMinSearch').val();
                var dailyLimitMaxSearch = $('#dailyLimitMaxSearch').val();
                var tranLimitMinSearch = $('#tranLimitMinSearch').val();
                var tranLimitMaxSearch = $('#tranLimitMaxSearch').val();
                
                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        transferTypeSearch: transferTypeSearch,
                        segmentTypeSearch: segmentTypeSearch,
                        statusSearch: statusSearch,
                        defaultLimitSearch: defaultLimitSearch,
                        dailyLimitMinSearch: dailyLimitMinSearch,
                        dailyLimitMaxSearch: dailyLimitMaxSearch,
                        tranLimitMinSearch: tranLimitMinSearch,
                        tranLimitMaxSearch: tranLimitMaxSearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
//            );

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function editformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:editTransactionLimitInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.segmentType + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function deleteformatter(cellvalue, options, rowObject) {
                return "<a href='#/' title='Delete' onClick='javascript:deleteTransactionLimitInit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.segmentType + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmTransactionLimit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectTransactionLimit(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
             
            function viewPendInit(keyval) {
                $("#viewpenddialog").data('id', keyval).dialog('open');
            }
            
            $.subscribe('openviewpendtasktopage', function (event, data) {
                var $led = $("#viewpenddialog");
                $led.html("Loading..");
                $led.load("viewPendTransactionLimitMgt.action?id=" + $led.data('id'));
            });
            
            function viewdownloadeformatter(cellvalue, options, rowObject) {
//                if (rowObject.operationcode == "UPLD") {
//                    return "<a href='pendCsvDownloadeTransactionLimitMgt.action?id=" + cellvalue + "' onclick='downloadmsgempty()' title='Downloade'><img class='ui-icon ui-icon-arrowthickstop-1-s' style='display:inline-table;border:none;'/></a>";
//                }else 
                if (rowObject.operationcode == "ADD" || rowObject.operationcode == "UPDATE" ) {
                    return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                }else{
                    return "--";
                }
            }
            function confirmTransactionLimit(keyval, popvar) {
                $('#divmsg').empty();
                
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');
                
                return false;
            }

            function rejectTransactionLimit(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');
                
                return false;
            }

            function confirmTL(keyval,remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmTransactionLimitMgt.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectTL(keyval,remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectTransactionLimitMgt.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }


            function editTransactionLimitInit(transferType,segmentType) {
                $("#updatedialog").data('transferType', transferType).data('segmentType', segmentType).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("detailTransactionLimitMgt.action?transferType=" + $led.data('transferType') + "&segmentType=" + encodeURI($led.data('segmentType')));
            });

            function deleteTransactionLimitInit(transferType,segmentType) {
                $('#divmsg').empty();

                $("#deletedialog").data('transferType', transferType).data('segmentType', segmentType).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete transaction limit (transfer type : ' + transferType+ ', segment type ' +segmentType +') ?');
                return false;
            }

            function deleteTransactionLimit(transferType,segmentType) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/deleteTransactionLimitMgt.action',
                    data: {transferType: transferType,segmentType: segmentType},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#deletesuccdialog").dialog('open');
                        $("#deletesuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function resetAllData() {
                $('#transferTypeSearch').val("");
                $('#segmentTypeSearch').val("");
                $('#statusSearch').val("");
                $('#defaultLimitSearch').val("");
                $('#dailyLimitMinSearch').val("");
                $('#dailyLimitMaxSearch').val("");
                $('#tranLimitMinSearch').val("");
                $('#tranLimitMaxSearch').val("");


                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        transferTypeSearch: '',
                        segmentTypeSearch: '',
                        statusSearch: '',
                        defaultLimitSearch: '',
                        tranLimitMinSearch: '',
                        tranLimitMaxSearch: '',
                        dailyLimitMinSearch: '',
                        dailyLimitMaxSearch: '',
                        search: false
                    }
                });
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetFieldData() {

                $('#transferType').val("");
                $('#segmentType').val("");
                $('#status').val("");
                $('#defaultLimit').val("");
                $('#tranLimitMin').val("");
                $('#tranLimitMax').val("");
                $('#dailyLimitMin').val("");
                $('#dailyLimitMax').val("");

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
      
            function validateCurrency(value) {
                //var value= $("#field1").val();
                var regex = /^[0-9]\d*(((,\d{3}){1})?(\.\d{0,2})?)$/;
                var tempVal=value.val();
                if (regex.test(value.val()))
                {
//                    //Input is valid, check the number of decimal places
//                    var twoDecimalPlaces = /\.\d{2}$/g;
//                    var oneDecimalPlace = /\.\d{1}$/g;
//                    var noDecimalPlacesWithDecimal = /\.\d{0}$/g;
//                    if(value.match(twoDecimalPlaces ))
//                    {
//                        //all good, return as is
//                        //return tempVal;
//                    }
//                    if(value.match(noDecimalPlacesWithDecimal))
//                    {
//                        //add two decimal places
//                        //return tempVal+'00';
//                    }
//                    if(value.match(oneDecimalPlace ))
//                    {
//                        //ad one decimal place
//                        //return tempVal+'0';
//                    }
//                    //else there is no decimal places and no decimal
//                    //return tempVal+".00";
                }else{
                    tempVal ="";
                }
                value.val(tempVal);
            };
            
            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('transactionlimitsearch');
                form.action = 'reportGenerateTransactionLimitMgt.action';
                form.submit();

                //    $('#view').button("disable");
                //    $('#view1').button("disable");
                $('#view2').button("disable");
            }
            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;
                //  var isGenerate = false;

                if (recors > 0 && isGenerate == false) {
                    //    $('#view').button("enable");
                    //    $('#view1').button("enable");
                    $('#view2').button("enable");
                } else {
                    //    $('#view').button("disable");
                    //    $('#view1').button("disable");
                    $('#view2').button("disable");
                }
            });

        </script>
        <title></title>
    </head>
    <body style="">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>

                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set var="vupdatebutt"><s:property value="vupdatebutt" default="true"/></s:set>
                            <s:set var="vupdatelink"><s:property value="vupdatelink" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form cssClass="form" id="transactionlimitsearch" method="post" action="TransactionLimitMgt" theme="simple" >
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Transfer Type </label>
                                                <s:select cssClass="form-control" name="transferTypeSearch" id="transferTypeSearch" list="%{transferTypeList}"   headerKey=""  headerValue="--Select Transfer Type--" listKey="transferId" listValue="description" value="%{transferType}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Segment Type </label>
                                                <s:select cssClass="form-control" name="segmentTypeSearch" id="segmentTypeSearch" list="%{SegmentTypeList}"   headerKey=""  headerValue="--Select Segment Type--" listKey="segmentcode" listValue="description" value="%{segmentType}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Status</label>
                                                <s:select  cssClass="form-control" name="statusSearch" id="statusSearch" list="%{statusList}"   headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" value="%{status}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Default Limit</label>
                                                <s:textfield cssClass="form-control" name="defaultLimitSearch" id="defaultLimitSearch" maxLength="13" onkeyup="validateCurrency($(this));" onmouseout="validateCurrency($(this));" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Tran Limit Min</label>
                                                <s:textfield cssClass="form-control" name="tranLimitMinSearch" id="tranLimitMinSearch" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Tran Limit Max</label>
                                                <s:textfield cssClass="form-control" name="tranLimitMaxSearch" id="tranLimitMaxSearch" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Daily Limit Min</label>
                                                <s:textfield cssClass="form-control" name="dailyLimitMinSearch" id="dailyLimitMinSearch" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Daily Limit Max</label>
                                                <s:textfield cssClass="form-control" name="dailyLimitMaxSearch" id="dailyLimitMaxSearch" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                                            </div>
                                        </div>
                                    </div> 
                                </s:form>       
                                    <div class="row row_1 form-inline">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <sj:submit 
                                                    button="true"
                                                    value="Search" 
                                                    href="#"
                                                    disabled="#vsearch"
                                                    onClick="search()"
                                                    id="searchbut"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div> 
                                            <div class="form-group">
                                                <sj:submit 
                                                    button="true" 
                                                    value="Reset" 
                                                    name="reset" 
                                                    onClick="resetAllData()" 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;"
                                                    />
                                            </div>
                                            <div class="form-group">
                                                <sj:submit 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    button="true" 
                                                    value="View CSV" 
                                                    name="view2" 
                                                    id="view2" 
                                                    onClick="todocsv()" 
                                                    disabled="#vgenerate"/> 
                                            </div>
                                        </div>
                                        <div class="col-sm-5"></div>
                                        <div class="col-sm-3  text-right">
<!--                                             <div class="form-group">
                                                <%--<s:url var="uploadurl" action="viewPopupcsvTransactionLimitMgt"/>--%>   
                                                <%--<sj:submit--%>                                                      
                                                    openDialog="remotedialog"
                                                    button="true"
                                                    href="%{uploadurl}"
                                                    disabled="#vupload"
                                                    value="Upload Transaction Limit"
                                                    id="uploadButton"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>-->
                                            <div class="form-group">                                               
                                                <s:url var="addurl" action="viewPopupTransactionLimitMgt"/>                                                    
                                                <sj:submit 
                                                    openDialog="remotedialog"
                                                    button="true"
                                                    href="%{addurl}"
                                                    disabled="#vadd"
                                                    value="Add New Transaction Limit"
                                                    id="addButton" 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                        </div>
                                    </div>

                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="remotedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Add Transaction Limit"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />  
                                <!-- Start update dialog box -->
                                <sj:dialog                                     
                                    id="updatedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Update Transaction Limit"
                                    onOpenTopics="openviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deleteTransactionLimit($(this).data('transferType'),$(this).data('segmentType'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete  Transaction Limit"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process" 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error"
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmTL($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectTL($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />
                                <!-- Start Pend view dialog box -->
                                <sj:dialog                                     
                                    id="viewpenddialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Pending Transaction Limit"
                                    onOpenTopics="openviewpendtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="ListTransactionLimitMgt"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    shrinkToFit="false"
                                    > 
                                    <sjg:gridColumn name="transferType" index="u.id.transferType" title="Edit" width="25" align="center" formatter="editformatter" sortable="false" hidden="#vupdatelink"/>
                                    <sjg:gridColumn name="transferType" index="u.id.transferType" title="Delete" width="40" align="center" formatter="deleteformatter" sortable="false" hidden="#vdelete"/>                                    
                                    <sjg:gridColumn name="segmentType" index="u.id.segmentType" title="Segment Type Code"  sortable="true" hidden="true"/>
                                    <sjg:gridColumn name="transferTypeDes" index="u.transferType.description" title="Transfer Type"  sortable="true"/>
                                    <sjg:gridColumn name="segmentTypeDes" index="u.segmentType.description" title="Segment Type"  sortable="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>
                                    <sjg:gridColumn name="defaultLimit" index="u.defaultLimit" title="Default Limit"  sortable="true"/>
                                    <sjg:gridColumn name="tranLimitMin" index="u.tranLimitMin" title="Tran Limit Min"  sortable="true"/>
                                    <sjg:gridColumn name="tranLimitMax" index="u.tranLimitMax" title="Tran Limit Max"  sortable="true"/>
                                    <sjg:gridColumn name="dailyLimitMin" index="u.dailyLimitMin" title="Daily Limit Min"  sortable="true"/>
                                    <sjg:gridColumn name="dailyLimitMax" index="u.dailyLimitMax" title="Daily Limit Max"  sortable="true"/>
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="createdtime" index="u.createdtime" title="Created Date And Time"  sortable="true" />
                                    <sjg:gridColumn name="lastupdatedtime" index="u.lastupdatedtime" title="Last Updated Date And Time"  sortable="true" />

                                </sjg:grid> 
                            </div>

                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="approveListTransactionLimitMgt"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Transaction Limit Mangement"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    rowTotal="false"
                                    viewrecords="true"  

                                    >
                                    <sjg:gridColumn name="id" index="id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>
                                    <sjg:gridColumn name="id" index="u.id" title="View" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/> 

                                    <sjg:gridColumn name="transferType" index="u.id" title="Transfer Type"  sortable="false" key="true"/>
                                    <sjg:gridColumn name="segmentType" index="u.id" title="Segment Type"  sortable="false" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="false" key="true"/>
                                    <sjg:gridColumn name="operationcode" index="u.operation" title="Operation Code"  sortable="false" key="true" hidden="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="false" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="false"/>                                                                    
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="false"/>  
                                    <sjg:gridColumn name="createtime" index="u.createdtime" title="Created Date And Time"  sortable="false" />                                                                  

                                </sjg:grid>  
                            </div>  
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!-- end: BODY -->
    </body>
</html>