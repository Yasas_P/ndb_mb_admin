<%-- 
    Document   : transactionlimitedit
    Created on : Apr 8, 2019, 1:55:09 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Branch</title> 
        <script type="text/javascript">
            function editTransactionLimit(transferType,segmentType) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findTransactionLimitMgt.action',
                    data: {transferType: transferType,segmentType: segmentType},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#transferTypeEdit').val("");
                            $('#transferTypeEdit').attr('disabled', false);
                            $('#segmentTypeEdit').val("");
                            $('#segmentTypeEdit').attr('disabled', false);
                            $('#statusEdit').val("");
                            $('#defaultLimitEdit').val("");
                            $('#tranLimitMinEdit').val("");
                            $('#tranLimitMaxEdit').val("");
                            $('#dailyLimitMinEdit').val("");
                            $('#dailyLimitMaxEdit').val("");
                            $('#divmsg').text("");
                        } else {
                            $('#statusEdit').val(data.status);
                            $('#defaultLimitEdit').val(data.defaultLimit);
                            $('#tranLimitMinEdit').val(data.tranLimitMin);
                            $('#tranLimitMaxEdit').val(data.tranLimitMax);
                            $('#dailyLimitMinEdit').val(data.dailyLimitMin);
                            $('#dailyLimitMaxEdit').val(data.dailyLimitMax);
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var transferType = $('#transferTypeEdit').val();
                var segmentTyp = $('#segmentTypeEdit').val();
                editTransactionLimit(transferType,segmentTyp);
            }

        </script>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="transactionlimitedit" method="post" action="TransactionLimitMgt" theme="simple" cssClass="form" >
            <s:hidden id="segmentTypeHiddenEdit" name="segmentTypeHidden" value="%{segmentType}" />
            <s:hidden id="transferTypeHiddenEdit" name="transferTypeHidden" value="%{transferType}" />
            <s:hidden id="oldvalue" name="oldvalue" ></s:hidden>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Transfer Type </label>
                        <s:select cssClass="form-control" name="transferType" id="transferTypeEdit" list="%{transferTypeList}"   headerKey=""  headerValue="--Select Transfer Type--" listKey="transferId" listValue="description" value="%{transferType}" disabled="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Segment Type </label>
                        <s:select cssClass="form-control" name="segmentType" id="segmentTypeEdit" list="%{segmentTypeList}"   headerKey=""  headerValue="--Select Segment Type--" listKey="segmentcode" listValue="description" value="%{segmentType}" disabled="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Default Limit</label>
                        <s:textfield cssClass="form-control" name="defaultLimit" id="defaultLimitEdit" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Tran Limit Min</label>
                        <s:textfield cssClass="form-control" name="tranLimitMin" id="tranLimitMinEdit" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Tran Limit Max</label>
                        <s:textfield cssClass="form-control" name="tranLimitMax" id="tranLimitMaxEdit" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Daily Limit Min</label>
                        <s:textfield cssClass="form-control" name="dailyLimitMin" id="dailyLimitMinEdit" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Daily Limit Max</label>
                        <s:textfield cssClass="form-control" name="dailyLimitMax" id="dailyLimitMaxEdit" maxLength="13" onkeyup="validateCurrency($(this))" onmouseout="validateCurrency($(this))" />
                    </div>
                </div> 
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="updateTransactionLimitMgt" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
