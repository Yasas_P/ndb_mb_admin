<%-- 
    Document   : promotionmgtinsert
    Created on : Mar 12, 2019, 4:16:11 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Insert Promotion</title> 
        <script type="text/javascript">
            $(function () {
                $('#longitude').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#longitude")[0].value = "";
                    }
                });
                $('#latitude').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#latitude")[0].value = "";
                    }
                });
            });
            $.subscribe('resetAddButton', function (event, data) {
                $('#amessage').empty();
                $('#subregioncode').val("");
                $('#status').val("");
                $('#promotionsCategories').val("");
                $('#subregionname').val("");
                $('#subreginaddress').val("");
                $('#freetext').val("");
                $('#info').val("");
                $('#tradinghours').val("");
                $('#longitude').val("");
                $('#latitude').val("");
                $('#promoconditions').val("");
                $('#phoneno').val("");
                $('#merchantwebsite').val("");
                $('#image').val("");
                $('#regioncode').val("");
                $('#regionname').val("");
                $('#masterregoncode').val("");
                $('#masterregionname').val("");
                $('#cardtype').val("");
                $('#cardimage').val("");
                $('#startdate').val("");
                $('#enddate').val("");
//                $('#radius').val("");
                $('#ispushnotification').attr('checked', false);

                $("#mobileImg_add").attr("src", "");
                $("#hasImageAdd").val('NOIMG');

                $("#up").hide();
                $("#down").hide();
                $("#msg").hide();
                deletefirebase();
            });

            $('#spanimg').click(function () {
                $('#mobileImg').val("");
                $("#mobileImg_add").attr("src", "");
                $('#firebaseImgMobUrl').val("");
                $("#hasImageAdd").val('NOIMG');
            });

            function changeMobileImage() {
                $("#mobileImg").change(function (event) {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#mobileImg_add").attr("src", tmppath);
                });
            }
            //========================= firebase ===================================

            var file_mob_1_width;
            var file_mob_1_height;

            $(document).ready(function () {
                var _URL = window.URL || window.webkitURL;
                $("#hasImageAdd").val('NOIMG');
                $("#mobileImg").change(function (e) {
                    var file, img;
                    if ((file = this.files[0])) {
                        img = new Image();
                        img.onload = function () {
                            file_mob_1_width = this.width;
                            file_mob_1_height = this.height;
                        };
                        img.onerror = function () {
                            alert("not a valid file: " + file.type);
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                    $("#hasImageAdd").val('IMG');
                });
            });



            function uploadtofirebase() {

                var file_mob_if = $('#mobileImg').val();
                var status = true;
                var status_size = validateImage();
//                var status_size = "";
                var imagecount = 0;
                var sendImageCount = 0;

                if (file_mob_if != "") {
                    imagecount++;
                }
                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    const file_mob_name = file_mob_if != "" ? $('#mobileImg').get(0).files[0].name : null
                    var fileNameExt = file_mob_name.substr(file_mob_name.lastIndexOf('.') + 1);
                    const validImageTypes = ['gif', 'jpg', 'jpeg', 'png', 'img'];
                    if (validImageTypes.includes(fileNameExt)) {
                        if (status_size == "") {

                            $("#msg").hide();

                            var metadata = {
                                contentType: 'image/jpeg',
                            };

                            const ref_mob = firebase.storage().ref('promotions/');
                            const file_mob = file_mob_if != "" ? $('#mobileImg').get(0).files[0] : null;
//                        const file_mob = $('#mobileImg').get(0).files[0];

                            const name_mob = file_mob_if != "" ? file_mob.name : null;

                            $("#up").fadeIn();
                            $("#down").fadeOut();
                            //first image
                            if (file_mob_if != "") {
                                const task_mob = ref_mob.child(name_mob).put(file_mob, metadata).then(function () {
                                    ref_mob.child(name_mob).getDownloadURL().then(function (url) {
                                        console.log("mob - " + url);
                                        $("#firebaseImgMobUrl").val(url);
                                        sendImageCount++;
                                        if (imagecount == sendImageCount) {
                                            $("#up").hide();
                                            $("#down").fadeIn();
                                            $("#hasImageAdd").val('NOIMG');
                                        }
                                        return url;
                                    });
                                });
                            }

                            console.log(imagecount);
                            console.log(sendImageCount);
                        } else {
                            $("#up").hide();
                            $("#down").hide();
                            $("#msg").show();
                            $("#msg").html(status_size);
                        }
                    } else {
                        $("#up").hide();
                        $("#down").hide();
                        $("#msg").show();
                        $("#msg").html("Please upload image with one of the following extensions:gif,jpg,jpeg,png,img")
                    }
                } else {
                    $("#up").hide();
                    $("#down").hide();
                    $("#msg").show();
                    $("#msg").html("Please select the image...")
                }
            }

            function deletefirebase() {

                var file_mob_if = $('#mobileImg').val();
                var status = true;

                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    $("#msg").hide();
                    $("#up").hide();
                    $("#down").hide();

                    const ref_mob = firebase.storage().ref('promotions/');
                    const file_mob = file_mob_if != "" ? $('#mobileImg').get(0).files[0] : null;

                    const name_mob = file_mob_if != "" ? file_mob.name : null;

                    if (file_mob_if != "") {
                        const task_mob = ref_mob.child(name_mob).delete().then(function () {
                            console.log("deleted");
                            $('#firebaseImgMobUrl').val("");
                            $('#mobileImg').val("");
                        }).catch(function (error) {
                            switch (error.code) {
                                case 'storage/object_not_found':
                                    break;
                                case 'storage/unauthorized':
                                    break;
                                case 'storage/canceled':
                                    break;
                                case 'storage/unknown':
                                    break;
                            }
                        });
                    }

                } else {
                    $('#mobileImg').val("");

                    $('#firebaseImgMobUrl').val("");
                }
            }
        </script>
        <style>
            .verticalLine {
                border-left:solid #e6e5e5;    
                height:75px;
                width:5em;
            }
            .firebaseclass{
                background: url(resouces/images/firebaselogo.svg) no-repeat;
                background-position: 74px;
                background-size: 100px;
                cursor: pointer;
                display: inline-block;
                height: 35px;
                width: 185px;
                padding: 9px;
                font-size: 13px;
                font-family: sans-serif;
                font-weight: bold;
                margin: 0 0 0 22px;
                border: 1px solid #cecece;
                transition: 0.3s;
            }
            .firebaseclass:hover{
                border: 1px solid black;
            }
            .megFirebase{
                padding: 9px;
                font-size: 12px;
                font-weight: bold;
                color: #FF5722;
                margin-left: -10px;
            }
        </style>
    </head>
    <body>
        <s:div id="amessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="promotionmgtadd" method="post" action="PromotionMgt" theme="simple" cssClass="form" enctype="multipart/form-data" >
            <s:hidden id="hasImageAdd" name="hasImage" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <span style="color: red">*</span><label>Subregion Code</label>
                        <s:textfield value="%{subregioncode}" cssClass="form-control" name="subregioncode" id="subregioncode" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"/>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="status" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>               
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Promotions Category</label>
                        <s:select value="%{promotionsCategories}" cssClass="form-control" id="promotionsCategories" list="%{promotionsCategoriesList}"  name="promotionsCategories" headerKey=""  headerValue="--Select Promotion Categories--" listKey="catcode" listValue="catname"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Name</label>
                        <s:textfield value="%{subregionname}" cssClass="form-control" name="subregionname" id="subregionname" maxLength="255" />
                    </div>
                </div>               
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Address</label>
                        <s:textfield value="%{subreginaddress}" cssClass="form-control" name="subreginaddress" id="subreginaddress" maxLength="512" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Free Text</label>
                        <s:textfield value="%{freetext}" cssClass="form-control" name="freetext" id="freetext" maxLength="512" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Information</label>
                        <s:textfield value="%{info}" cssClass="form-control" name="info" id="info" maxLength="255" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Trading Hours</label>
                        <s:textfield value="%{tradinghours}" cssClass="form-control" name="tradinghours" id="tradinghours" maxLength="20" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Latitude</label>
                        <s:textfield value="%{latitude}" cssClass="form-control" name="latitude" id="latitude" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Longitude</label>
                        <s:textfield value="%{longitude}" cssClass="form-control" name="longitude" id="longitude" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Radius[In kilometer(km)]</label>
                        <%--<s:textfield value="%{radius}" name="radius" id="radius" cssClass="form-control" maxLength="20" onchange="validateFloatKeyPress(this);"/>--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Promotion Conditions</label>
                        <s:textfield value="%{promoconditions}" cssClass="form-control" name="promoconditions" id="promoconditions" maxLength="95"  />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Phone Number</label>
                        <s:textfield value="%{phoneno}" cssClass="form-control" name="phoneno" id="phoneno" maxLength="33" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red"></span><label>Merchant Web Site</label>
                            <s:textfield value="%{merchantwebsite}" cssClass="form-control" name="merchantwebsite" id="merchantwebsite" maxLength="255" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Region Code</label>
                        <s:textfield value="%{regioncode}" cssClass="form-control" name="regioncode" id="regioncode" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Region Name</label>
                        <s:textfield value="%{regionname}" cssClass="form-control" name="regionname" id="regionname" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region Code</label>
                        <s:textfield value="%{masterregoncode}" cssClass="form-control" name="masterregoncode" id="masterregoncode" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region Name</label>
                        <s:textfield value="%{masterregionname}" cssClass="form-control" name="masterregionname" id="masterregionname" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Card Type</label>
                        <s:textfield value="%{cardtype}" cssClass="form-control" name="cardtype" id="cardtype" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Card Image</label>
                        <s:textfield value="%{cardimage}" cssClass="form-control" name="cardimage" id="cardimage" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Start Date</label>
                        <sj:datepicker 
                            cssClass="form-control" 
                            id="startdate" 
                            name="startdate" 
                            readonly="true" 
                            buttonImageOnly="true" 
                            timepicker="true" 
                            displayFormat="dd-mm-yy" 
                            minDate="0"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >End Date</label>
                        <sj:datepicker 
                            cssClass="form-control" 
                            id="enddate" 
                            name="enddate" 
                            readonly="true" 
                            buttonImageOnly="true"
                            timepicker="true" 
                            displayFormat="dd-mm-yy" 
                            minDate="0"/>
                    </div>
                </div>
                <div class="row row_popup"> 
                    <div class="col-sm-4" style="margin-top: 28px;">
                        <div class="form-group">
                            <label style="float: left;margin: 5px 9px 3px 7px">Notifications Enable</label>
                            <s:checkbox name="ispushnotification" id="ispushnotification" label="Push Notifications Enable" fieldValue="1" cssStyle="width: 20px;height: 20px;" />
                        </div>
                    </div>
                </div>    
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">        
                <div class="col-sm-9">
                    <div class="form-group">
                        <label >Mobile Image (Maximum size (w*h) : 480x800 pixels, Valid extensions : gif,jpg,jpeg,png,img)</label>   
                        <div class="row">
                            <div class="col-sm-2">
                                <img class="image" src="data:image/jpeg;base64,<s:property value="mobileImgedit"/>" id="mobileImg_add" alt="Mobile Image">
                                <span id="spanimg" class="ui-button-icon-primary ui-icon ui-icon-circle-close" style="position:absolute;top:10px;bottom:0;right:20%"></span>  
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-8" style="margin-top: 40px;">
                                <s:file name="mobileImg" id="mobileImg" onclick="changeMobileImage();"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">  
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="firebaseclass" onclick="uploadtofirebase();">Upload to</div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <div id="up" style="display: none;color: #008a00" class="megFirebase">Images uploading....</div>
                        <div id="down" style="display: none;color: blue" class="megFirebase">Images uploaded successfully</div>
                        <div id="msg" style="display: none" class="megFirebase"></div>
                    </div>
                </div>
            </div>   
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Mobile Img - Firebase URL</label>
                        <s:textarea readonly="true" name="firebaseImgMobUrl" id="firebaseImgMobUrl" cssClass="form-control" maxLength="300" rows="2"/>
                    </div>
                </div>
            </div>             
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3  text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 0px;">
                        <s:url action="addPromotionMgt" var="inserturl"/>
                        <sj:submit
                            button="true"
                            value="Add"
                            href="%{inserturl}"
                            onClickTopics=""
                            targets="amessage"
                            id="addbtn"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"                          
                            />                        
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            name="reset" 
                            cssClass="btn btn-default btn-sm"
                            onClickTopics="resetAddButton"
                            />                          
                    </div>

                </div>
            </div>
        </s:form>
    </body>
</html>
