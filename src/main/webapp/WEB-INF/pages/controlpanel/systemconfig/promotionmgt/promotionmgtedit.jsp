<%-- 
    Document   : promotionmgtedit
    Created on : Mar 13, 2019, 8:02:23 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Update Promotion</title> 
        <script type="text/javascript">
             $(function () {
                $('#latitudeEdit').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#latitudeEdit")[0].value = "";
                    }
                });
                $('#longitudeEdit').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#longitudeEdit")[0].value = "";
                    }
                });
            });
            function editPromotion(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findPromotionMgt.action',
                    data: {subregioncode: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#amessageedit').empty();
                        var msg = data.message;
                        if (msg) {
//                            $('#promoidEdit').val("");
                            $('#promotionsCardsEdit').val("");
                            $('#statusEdit').val("");
                            $('#promotionsCategoriesEdit').val("");
                            $('#subregionnameEdit').val("");
                            $('#subreginaddressEdit').val("");
                            $('#freetextEdit').val("");
                            $('#infoEdit').val("");
                            $('#tradinghoursEdit').val("");
                            $('#latitudeEdit').val("");
                            $('#longitudeEdit').val("");
                            $('#promoconditionsEdit').val("");
                            $('#phonenoEdit').val("");
                            $('#merchantwebsiteEdit').val("");
                            $('#imageEdit').val("");
                            $('#regioncodeEdit').val("");
                            $('#regionnameEdit').val("");
                            $('#masterregoncodeEdit').val("");
                            $('#masterregionnameEdit').val("");
                            $('#cardtypeEdit').val("");
                            $('#cardimageEdit').val("");
                            $('#startdateEdit').val("");
                            $('#enddateEdit').val("");
//                            $('#radiusEdit').val("");
                            $('#ispushnotificationedit').attr('checked', false);
                            $('#divmsg').text("");
                        } else {
//                            $('#promotionsCardsEdit').val(data.promotionsCards);
                            $('#statusEdit').val(data.status);
                            $('#promotionsCategoriesEdit').val(data.promotionsCategories);
                            $('#subregionnameEdit').val(data.subregionname);
                            $('#subreginaddressEdit').val(data.subreginaddress);
                            $('#freetextEdit').val(data.freetext);
                            $('#infoEdit').val(data.info);
                            $('#tradinghoursEdit').val(data.tradinghours);
                            $('#longitudeEdit').val(data.longitude);
                            $('#latitudeEdit').val(data.latitude);
                            $('#promoconditionsEdit').val(data.promoconditions);
                            $('#phonenoEdit').val(data.phoneno);
                            $('#merchantwebsiteEdit').val(data.merchantwebsite);
//                            $('#imageEdit').val(data.image);
                            $('#regioncodeEdit').val(data.regioncode);
                            $('#regionnameEdit').val(data.regionname);
                            $('#masterregoncodeEdit').val(data.masterregoncode);
                            $('#masterregionnameEdit').val(data.masterregionname);
                            $('#cardtypeEdit').val(data.cardtype);
                            $('#cardimageEdit').val(data.cardimage);

                            $('#mobileImg_edit').attr("src", data.image);

                            $('#firebaseImgMobUrledit').val(data.image);
                            $('#startdateEdit').val(data.startdate);
                            $('#enddateEdit').val(data.enddate);
//                            $('#radiusEdit').val(data.radius);
                            if (data.ispushnotification) {
                                document.getElementById("ispushnotificationedit").checked = true;
                            } else {
                                document.getElementById("ispushnotificationedit").checked = false;
                            }
                            
                            $("#up_e").hide();
                            $("#down_e").hide();
                            $("#msg_e").hide();
                            $("#hasImage").val('NOIMG');
                            deletefirebase_edit();
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function cancelData() {
                var subregioncode = $('#subregioncodeEdit').val();
                editPromotion(subregioncode);
            }

            function changeMobileImageEdit() {
                $("#mobileImgFile").change(function (event) {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#mobileImg_edit").attr("src", tmppath);
                });
            }

            $('#editspanimg').click(function () {
                $('#mobileImgFile').val("");
                $('#firebaseImgMobUrledit').val("");
                $("#mobileImg_edit").attr("src", "");
                $("#hasImage").val('NOIMG');
            });

            //========================= firebase =================================

            var file_mob_1_width;
            var file_mob_1_height;

            $(document).ready(function () {
                var _URL = window.URL || window.webkitURL;
                $("#hasImage").val('NOIMG');

                $("#mobileImgFile").change(function (e) {
                    var file, img;
                    if ((file = this.files[0])) {
                        img = new Image();
                        img.onload = function () {
                            file_mob_1_width = this.width;
                            file_mob_1_height = this.height;
                        };
                        img.onerror = function () {
                            alert("not a valid file: " + file.type);
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                    $("#hasImage").val('IMG');
                });
            });

            function uploadtofirebase_edit() {

                var file_mob_if = $('#mobileImgFile').val();
                var imagecount = 0;
                var sendImageCount = 0;

                if (file_mob_if != "") {
                    imagecount++;
                }
                var status = true;
                var status_size = validateImage();
//                var status_size = "";


                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    const file_mob_name = file_mob_if != "" ? $('#mobileImgFile').get(0).files[0].name : null
                    var fileNameExt = file_mob_name.substr(file_mob_name.lastIndexOf('.') + 1);
                    const validImageTypes = ['gif', 'jpg', 'jpeg', 'png', 'img'];
                    if (validImageTypes.includes(fileNameExt)) {
                        if (status_size == "") {
                            $("#msg_e").hide();

                            var metadata = {
                                contentType: 'image/jpeg',
                            };

                            const ref_mob = firebase.storage().ref('promotions/');
                            var file_mob = file_mob_if != "" ? $('#mobileImgFile').get(0).files[0] : null;

                            var name_mob = "";

                            if (file_mob_if != "") {
                                name_mob = file_mob.name;
                            }


                            $("#up_e").fadeIn();
                            $("#down_e").fadeOut();

                            $('#amessageedit').empty();

                            //first image
                            if (file_mob_if != "") {
                                const task_mob = ref_mob.child(name_mob).put(file_mob, metadata).then(function () {
                                    ref_mob.child(name_mob).getDownloadURL().then(function (url) {
                                        console.log("mob  - " + url);
                                        $("#firebaseImgMobUrledit").val(url);
                                        sendImageCount++;
                                        if (imagecount == sendImageCount) {
                                            $("#up_e").hide();
                                            $("#down_e").fadeIn();
                                            $("#hasImage").val('NOIMG');
                                        }

                                        return url;
                                    });
                                });
                            }
                        } else {
                            $("#up_e").hide();
                            $("#down_e").hide();
                            $("#msg_e").show();
                            $("#msg_e").html(status_size);
                        }
                    } else {
                        $("#up_e").hide();
                        $("#down_e").hide();
                        $("#msg_e").show();
                        $("#msg_e").html("Please upload image with one of the following extensions:gif,jpg,jpeg,png,img")
                    }
                } else {
                    $("#up_e").hide();
                    $("#down_e").hide();
                    $("#msg_e").show();
                    $("#msg_e").html("Please select the image......")
                }
            }


            function deletefirebase_edit() {

                var file_mob_if = $('#mobileImgFile').val();

                var status = true;

                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    $("#msg_e").hide();
                    $("#up_e").hide();
                    $("#down_e").hide();

                    const ref_mob = firebase.storage().ref('promotions/');
                    const file_mob = file_mob_if != "" ? $('#mobileImgFile').get(0).files[0] : null;
                    $('#mobileImgFile').val("");

                    var name_mob = "";

                    if (file_mob_if != "") {
                        name_mob = file_mob.name;
                        const task_mob = ref_mob.child(name_mob).delete().then(function () {
                            console.log("deleted");
//                            $('#firebaseImgMob1Urledit').val(img1);
//                            $('#mobileImgFile').val("");
                        }).catch(function (error) {
                            switch (error.code) {
                                case 'storage/object_not_found':
                                    break;
                                case 'storage/unauthorized':
                                    break;
                                case 'storage/canceled':
                                    break;
                                case 'storage/unknown':
                                    break;
                            }
                        });
                    }
                } else {
                    $('#mobileImgFile').val("");
                }
            }

            $(document).ready(function () {

                if ($("#ispushnotificationhidden").val() == 1) {
                    $("#ispushnotificationedit").attr("checked", "checked");
                }
            });
        </script>
        <style>
            .verticalLine {
                border-left:solid #e6e5e5;    
                height:100px;
                width:5em;
            }
            .firebaseclass{
                background: url(resouces/images/firebaselogo.svg) no-repeat;
                background-position: 74px;
                background-size: 100px;
                cursor: pointer;
                display: inline-block;
                height: 35px;
                width: 185px;
                padding: 9px;
                font-size: 13px;
                font-family: sans-serif;
                font-weight: bold;
                margin: 0 0 0 22px;
                border: 1px solid #cecece;
                transition: 0.3s;
            }
            .firebaseclass:hover{
                border: 1px solid black;
            }
            .megFirebase{
                padding: 9px;
                font-size: 12px;
                font-weight: bold;
                color: #FF5722;
                margin-left: -10px;
            }
        </style>
    </head>
    <body>
        <s:div id="amessageedit">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="promotionmgtedit" method="post" action="PromotionMgt" theme="simple" cssClass="form" enctype="multipart/form-data" >
            <s:hidden id="imgEmptyTest" name="imgEmptyTest"></s:hidden>
            <s:hidden id="hasImage" name="hasImage" ></s:hidden>
            <s:hidden name="ispushnotificationhidden" id="ispushnotificationhidden"></s:hidden>
            <%--<s:hidden id="promoidEdit" name="promoid" value="%{promoid}" />--%>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Code </label>
                        <s:textfield value="%{subregioncode}" cssClass="form-control" name="subregioncode" id="subregioncodeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" readonly="true"/>
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Code</label>
                        <%--<s:textfield value="%{promotionsCards}" cssClass="form-control" name="promotionsCards" id="promotionsCardsEdit" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>--%>
                        <%--<s:select value="%{promotionsCards}" cssClass="form-control" id="promotionsCardsEdit" list="%{promotionsCardsList}"  name="promotionsCards" headerKey=""  headerValue="--Select Subregion Code--" listKey="subregioncode" listValue="subregioncode"/>--%>
                    </div>
                </div>-->

                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusEdit" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description"/>
                    </div>
                </div>               
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Promotions Category</label>
                        <s:select value="%{promotionsCategories}" cssClass="form-control" id="promotionsCategoriesEdit" list="%{promotionsCategoriesList}"  name="promotionsCategories" headerKey=""  headerValue="--Select Promotion Category--" listKey="catcode" listValue="catname"/>
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Name</label>
                        <s:textfield value="%{subregionname}" cssClass="form-control" name="subregionname" id="subregionnameEdit" maxLength="255" />
                    </div>
                </div>              
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Address</label>
                        <s:textfield value="%{subreginaddress}" cssClass="form-control" name="subreginaddress" id="subreginaddressEdit" maxLength="512" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Free Text</label>
                        <s:textfield value="%{freetext}" cssClass="form-control" name="freetext" id="freetextEdit" maxLength="512" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Information</label>
                        <s:textfield value="%{info}" cssClass="form-control" name="info" id="infoEdit" maxLength="255" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Trading Hours</label>
                        <s:textfield value="%{tradinghours}" cssClass="form-control" name="tradinghours" id="tradinghoursEdit" maxLength="20" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Latitude</label>
                        <s:textfield value="%{latitude}" cssClass="form-control" name="latitude" id="latitudeEdit" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Longitude</label>
                        <s:textfield value="%{longitude}" cssClass="form-control" name="longitude" id="longitudeEdit" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Radius[In kilometer(km)]</label>
                        <%--<s:textfield name="radius" id="radiusEdit" cssClass="form-control" maxLength="20" onchange="validateFloatKeyPress(this);"/>--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Promotion Conditions</label>
                        <s:textfield value="%{promoconditions}" cssClass="form-control" name="promoconditions" id="promoconditionsEdit" maxLength="95"  />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Phone Number</label>
                        <s:textfield value="%{phoneno}" cssClass="form-control" name="phoneno" id="phonenoEdit" maxLength="33" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red"></span><label>Merchant Web Site</label>
                        <s:textfield value="%{merchantwebsite}" cssClass="form-control" name="merchantwebsite" id="merchantwebsiteEdit" maxLength="255" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Region Code</label>
                        <s:textfield value="%{regioncode}" cssClass="form-control" name="regioncode" id="regioncodeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Region Name</label>
                        <s:textfield value="%{regionname}" cssClass="form-control" name="regionname" id="regionnameEdit" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region Code</label>
                        <s:textfield value="%{masterregoncode}" cssClass="form-control" name="masterregoncode" id="masterregoncodeEdit" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region Name</label>
                        <s:textfield value="%{masterregionname}" cssClass="form-control" name="masterregionname" id="masterregionnameEdit" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Card Type</label>
                        <s:textfield value="%{cardtype}" cssClass="form-control" name="cardtype" id="cardtypeEdit" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Card Image</label>
                        <s:textfield value="%{cardimage}" cssClass="form-control" name="cardimage" id="cardimageEdit" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" />
                    </div>
                </div>
                <!--                <div class="col-sm-3">
                                    <div class="form-group">
                                        <span style="color: red">*</span><label>Image</label>
                <%--<s:textfield value="%{image}" cssClass="form-control" name="image" id="imageEdit" maxLength="100" />--%>
            </div>
        </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Start Date</label>
                        <sj:datepicker 
                            cssClass="form-control" 
                            id="startdateEdit" 
                            name="startdate" 
                            readonly="true" 
                            buttonImageOnly="true" 
                            timepicker="true" 
                            displayFormat="dd-mm-yy" 
                            minDate="0"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >End Date</label>
                        <sj:datepicker 
                            cssClass="form-control" 
                            id="enddateEdit" 
                            name="enddate" 
                            readonly="true" 
                            buttonImageOnly="true"
                            timepicker="true" 
                            displayFormat="dd-mm-yy" 
                            minDate="0"/>
                    </div>
                </div>
                <div class="col-sm-4" style="margin-top: 28px;">
                    <div class="form-group">
                        <label style="float: left;margin: 5px 9px 3px 7px">Notifications Enable</label>
                        <s:checkbox name="ispushnotification" id="ispushnotificationedit" label="Push Notifications Enable" fieldValue="1" cssStyle="width: 20px;height: 20px;" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>        
            <div class="row row_popup">
                <div class="col-sm-9">
                    <div class="form-group">
                        <label >Mobile Image One (Maximum size (w*h) : 480x800 pixels, Valid extensions : gif,jpg,jpeg,png,img)</label>   
                        <div class="row">
                            <div class="col-sm-2">
                                <img onclick="mobileImgZoom(this.src)" class="image" src="<s:property value="image"/>" id="mobileImg_edit" name="mobileImgedit" alt="Mobile Image" >
                                <span id="editspanimg" class="ui-button-icon-primary ui-icon ui-icon-circle-close" style="position:absolute;top:10px;bottom:0;right:25%"></span>  
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-8" style="margin-top: 40px;">
                                <s:file name="mobileImg" id="mobileImgFile" onclick="changeMobileImageEdit();"/>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">  
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="firebaseclass" onclick="uploadtofirebase_edit();">Upload to</div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <div id="up_e" style="display: none;color: #008a00" class="megFirebase">Images uploading....</div>
                        <div id="down_e" style="display: none;color: blue" class="megFirebase">Images uploaded successfully</div>
                        <div id="msg_e" style="display: none" class="megFirebase"></div>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label >Mobile Img  - Firebase URL</label>
                        <s:textarea readonly="true" name="firebaseImgMobUrl" id="firebaseImgMobUrledit" cssClass="form-control" maxLength="300" rows="2" value="%{image}"/>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>        
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
                <div class="col-sm-3 text-right">

                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="updatePromotionMgt" var="updateturl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updateturl}"
                            targets="amessageedit"
                            id="updateButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
