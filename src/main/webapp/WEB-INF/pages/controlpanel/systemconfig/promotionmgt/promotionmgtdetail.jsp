<%-- 
    Document   : promotionmgtdetail
    Created on : Aug 12, 2019, 2:15:05 PM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Detail Promotion Management</title>
    </head>
    <body>
        <s:div id="amessagedetail">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="promotionmgtdetail" method="post" action="PromotionMgt" theme="simple" cssClass="form" enctype="multipart/form-data" >
            <%--<s:hidden id="promoidEdit" name="promoid" value="%{promoid}" />--%>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Code </label>
                        <s:textfield value="%{subregioncode}" cssClass="form-control" name="subregioncode" id="subregioncodeDetail" maxLength="10"  readonly="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Status</label>
                        <s:select value="%{status}" cssClass="form-control" id="statusDetail" list="%{statusList}"  name="status" headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" disabled="true" />
                    </div>
                </div>               
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Promotions Category</label>
                        <s:select value="%{promotionsCategories}" cssClass="form-control" id="promotionsCategoriesDetail" list="%{promotionsCategoriesList}"  name="promotionsCategories" headerKey=""  headerValue="--Select Promotion Category--" listKey="catcode" listValue="catname" disabled="true" />
                    </div>
                </div> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Name</label>
                        <s:textfield value="%{subregionname}" cssClass="form-control" name="subregionname" id="subregionnameDetail" maxLength="255" readonly="true" />
                    </div>
                </div>              
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Address</label>
                        <s:textfield value="%{subreginaddress}" cssClass="form-control" name="subreginaddress" id="subreginaddressDetail" maxLength="512" readonly="true"  />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Free Text</label>
                        <s:textfield value="%{freetext}" cssClass="form-control" name="freetext" id="freetextDetail" maxLength="512" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Information</label>
                        <s:textfield value="%{info}" cssClass="form-control" name="info" id="infoDetail" maxLength="255" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Trading Hours</label>
                        <s:textfield value="%{tradinghours}" cssClass="form-control" name="tradinghours" id="tradinghoursDetail" maxLength="20" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Latitude</label>
                        <s:textfield value="%{latitude}" cssClass="form-control" name="latitude" id="latitudeDetail" maxLength="20" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Longitude</label>
                        <s:textfield value="%{longitude}" cssClass="form-control" name="longitude" id="longitudeDetail" maxLength="20" readonly="true" />
                    </div>
                </div>
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Radius[In kilometer(km)]</label>
                        <%--<s:textfield name="radius" id="radiusEdit" cssClass="form-control" maxLength="20" onchange="validateFloatKeyPress(this);"/>--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Promotion Conditions</label>
                        <s:textfield value="%{promoconditions}" cssClass="form-control" name="promoconditions" id="promoconditionsDetail" maxLength="95" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Phone Number</label>
                        <s:textfield value="%{phoneno}" cssClass="form-control" name="phoneno" id="phonenoDetail" maxLength="33" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red"></span><label>Merchant Web Site</label>
                        <s:textfield value="%{merchantwebsite}" cssClass="form-control" name="merchantwebsite" id="merchantwebsiteDetail" maxLength="255" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Region Code</label>
                        <s:textfield value="%{regioncode}" cssClass="form-control" name="regioncode" id="regioncodeDetail" maxLength="10" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Region Name</label>
                        <s:textfield value="%{regionname}" cssClass="form-control" name="regionname" id="regionnameDetail" maxLength="50" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region Code</label>
                        <s:textfield value="%{masterregoncode}" cssClass="form-control" name="masterregoncode" id="masterregoncodeDetail" maxLength="10" readonly="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Master Region Name</label>
                        <s:textfield value="%{masterregionname}" cssClass="form-control" name="masterregionname" id="masterregionnameDetail" maxLength="50" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Card Type</label>
                        <s:textfield value="%{cardtype}" cssClass="form-control" name="cardtype" id="cardtypeDetail" maxLength="50" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Card Image</label>
                        <s:textfield value="%{cardimage}" cssClass="form-control" name="cardimage" id="cardimageDetail" maxLength="100" readonly="true" />
                    </div>
                </div>
                <!--                <div class="col-sm-3">
                                    <div class="form-group">
                                        <span style="color: red">*</span><label>Image</label>
                <%--<s:textfield value="%{image}" cssClass="form-control" name="image" id="imageEdit" maxLength="100" />--%>
            </div>
        </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Start Date</label>
                        <s:textfield 
                            cssClass="form-control" 
                            id="startdateDetail" 
                            name="startdate" 
                            disabled="true"/>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label >End Date</label>
                        <s:textfield 
                            cssClass="form-control" 
                            id="enddateDetail" 
                            name="enddate" 
                            disabled="true" />
                    </div>
                </div>
                <div class="col-sm-4" style="margin-top: 28px;">
                    <div class="form-group">
                        <label style="float: left;margin: 5px 9px 3px 7px">Notifications Enable</label>
                        <s:checkbox name="ispushnotification" id="ispushnotificationDetail" label="Push Notifications Enable" fieldValue="1" cssStyle="width: 20px;height: 20px;" disabled="true" />
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>        
            <div class="row row_popup">
                <div class="col-sm-9">
                    <div class="form-group">
                        <label >Mobile Image One (Maximum size (w*h) : 480x800 pixels, Valid extensions : gif,jpg,jpeg,png,img)</label>   
                        <div class="row">
                            <div class="col-sm-2">
                                <img onclick="mobileImgZoom(this.src)" class="image" src="<s:property value="image"/>" id="mobileImg_detail" name="mobileImgedit" alt="Mobile Image" >
                                <!--<span id="editspanimg" class="ui-button-icon-primary ui-icon ui-icon-circle-close" style="position:absolute;top:10px;bottom:0;right:25%"></span>-->  
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>        
            <div class="row row_popup form-inline">
                <div class="col-sm-9">
                    <div class="form-group">
                        <span class="mandatoryfield">Mandatory fields are marked with *</span>
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
