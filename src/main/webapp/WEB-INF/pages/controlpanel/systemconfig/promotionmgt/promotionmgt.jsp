<%-- 
    Document   : promotionmgt
    Created on : Mar 11, 2019, 4:24:13 PM
    Author     : sivaganesan_t
--%>

<%@page import="com.epic.ndb.util.varlist.CommonVarList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="/stylelinks.jspf" %>
        <script src="resouces/js/firebaseconf.js" ></script>
        <script type="text/javascript">
            $(function () {
                $('#longitudeSearch').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#longitudeSearch")[0].value = "";
                    }
                });
                $('#latitudeSearch').keyup(function () {
                    var str = $(this).val();
                    if (/(^\.+$)|(^\d+$)|(^\d+\.$)|(^\.\d+$)|(^\d+\.\d+$)/.test(str)) {
                    } else {
                        $("#latitudeSearch")[0].value = "";
                    }
                });
            });
           function searchPage() {
                $('#message').empty();

                var promoidSearch = $('#promoidSearch').val();
                var subregioncodeSearch = $('#subregioncodeSearch').val();
                var statusSearch = $('#statusSearch').val();
                var promotionsCategoriesSearch = $('#promotionsCategoriesSearch').val();
                var subregionnameSearch = $('#subregionnameSearch').val();
                var subreginaddressSearch = $('#subreginaddressSearch').val();
                var freetextSearch = $('#freetextSearch').val();
                var infoSearch = $('#infoSearch').val();
                var latitudeSearch = $('#latitudeSearch').val();
                var longitudeSearch = $('#longitudeSearch').val();
                var promoconditionsSearch = $('#promoconditionsSearch').val();
                var phonenoSearch = $('#phonenoSearch').val();
                var merchantwebsiteSearch = $('#merchantwebsiteSearch').val();

                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        promoidSearch: promoidSearch,
                        subregioncodeSearch: subregioncodeSearch,
                        statusSearch: statusSearch,
                        promotionsCategoriesSearch: promotionsCategoriesSearch,
                        subregionnameSearch: subregionnameSearch,
                        subreginaddressSearch: subreginaddressSearch,
                        freetextSearch: freetextSearch,
                        infoSearch: infoSearch,
                        latitudeSearch: latitudeSearch,
                        longitudeSearch: longitudeSearch,
                        promoconditionsSearch: promoconditionsSearch,
                        phonenoSearch: phonenoSearch,
                        merchantwebsiteSearch: merchantwebsiteSearch,
                        search: true
                    }
                });

                $("#gridtable").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtable").trigger("reloadGrid");
                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            };

            $.subscribe('anyerrors', function (event, data) {
                window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
            });

            function editformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Edit' onClick='javascript:editPromotionInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-pencil' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            
            function viewformatter(cellvalue, options, rowObject) {
                return "<a title='Edit' onClick='javascript:viewDetailPromotionInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            
            function viewdownloadeformatter(cellvalue, options, rowObject) {
                if (rowObject.operationcode == "UPLD") {
                    return "<a href='pendCsvDownloadePromotionMgt.action?id=" + cellvalue + "' onclick='downloadmsgempty()' title='Downloade'><img class='ui-icon ui-icon-arrowthickstop-1-s' style='display:inline-table;border:none;'/></a>";
                } else if(rowObject.operationcode == "UPDI") {
                    return "<a href='#' title='View' onClick='javascript:viewPendDefImg(&#34;" + rowObject.fields + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                }else if (rowObject.operationcode == "ADD" || rowObject.operationcode == "UPDATE" ) {
                    return "<a href='#' title='View' onClick='javascript:viewPendInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-newwin' style='display: block; margin-left: auto; margin-right: auto;'/></a>";
                } else{
                    return "--";
                }
            }

            function deleteformatter(cellvalue, options, rowObject) {
                return "<a href='#/' title='Delete' onClick='javascript:deletePromotionInit(&#34;" + cellvalue + "&#34;)'><img class='ui-icon ui-icon-trash' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function confirmformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Approve' onClick='javascript:confirmPromotion(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-check' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }

            function rejectformatter(cellvalue, options, rowObject) {
                return "<a href='#' title='Reject' onClick='javascript:rejectPromotion(&#34;" + cellvalue + "&#34;,&#34;" + rowObject.mobileno + "&#34;)'><img class='ui-icon ui-icon-close' style='display: block;margin-left: auto;margin-right: auto;'/></a>";
            }
            function viewPendDefImg(imgUrl){
                 $("#penddefimagedialog").dialog('open');
                 $("#penddefimagedialog").html('<img class="image" src="'+imgUrl+'" >');
            }
            function confirmPromotion(keyval, popvar) {
                $('#divmsg').empty();
                $("#confirmdialog").data('keyval', keyval).dialog('open');
                $("#confirmdialog").html('Are you sure you want to approve this operation ?<br />');
                $("#confirmdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgconfirm',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#confirmdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#confirmdialog").append('<textarea rows="3" cols="73"  name="commentConfirm" id="commentConfirm" maxlength="250"></textarea><br /><br />');
                $("#confirmdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function rejectPromotion(keyval, popvar) {
                $('#divmsg').empty();
                $("#rejectdialog").data('keyval', keyval).dialog('open');
                $("#rejectdialog").html('Are you sure you want to reject this operation ?<br />');
                $("#rejectdialog").append($('<input>', {
                    type: 'text',
                    id: 'errormsgreject',
                    disabled: 'true',
                    readonly: 'true',
                    style: 'border-color: #ffffff;color: red;'
                }));

                $("#rejectdialog").append('<br /><span style="color: red">*</span><label>Remark</label><br /> ');
                $("#rejectdialog").append('<textarea rows="3" cols="73"  name="commentReject" id="commentReject" maxlength="250"></textarea><br /><br />');
                $("#rejectdialog").append('<span style="color: red">Mandatory fields are marked with *</span> ');

                return false;
            }

            function confirmPM(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/confirmPromotionMgt.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.errormessage) {
                            $("#confirmdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgconfirm").val(data.errormessage);

                        } else {
                            $("#confirmsuccdialog").dialog('open');
                            $("#confirmsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }
            function rejectPM(keyval, remark) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/rejectPromotionMgt.action',
                    data: {id: keyval, remark: remark},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {

                        if (data.errormessage) {
                            $("#rejectdialog").data('keyval', keyval).dialog('open');
                            $("#errormsgreject").val(data.errormessage);

                        } else {
                            $("#rejectsuccdialog").dialog('open');
                            $("#rejectsuccdialog").html(data.message);
                            resetFieldData();
                        }

                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";

                    }
                });
            }


            function editPromotionInit(keyval) {
                $("#updatedialog").data('subregioncode', keyval).dialog('open');
            }

            $.subscribe('openviewtasktopage', function (event, data) {
                var $led = $("#updatedialog");
                $led.html("Loading..");
                $led.load("detailPromotionMgt.action?subregioncode=" +encodeURI($led.data('subregioncode')));
            });
            
            function viewDetailPromotionInit(keyval) {
                $("#viewdetaildialog").data('subregioncode', keyval).dialog('open');
            }

            $.subscribe('opendetailviewtasktopage', function (event, data) {
                var $led = $("#viewdetaildialog");
                $led.html("Loading..");
                $led.load("viewPopupDetailPromotionMgt.action?subregioncode=" +encodeURI($led.data('subregioncode')));
            });
            
            function viewPendInit(keyval) {
                $("#viewpenddialog").data('id', keyval).dialog('open');
            }

            $.subscribe('openviewpendtasktopage', function (event, data) {
                var $led = $("#viewpenddialog");
                $led.html("Loading..");
                $led.load("viewPendPromotionMgt.action?id=" + $led.data('id'));
            });

            function deletePromotionInit(keyval) {
                $('#divmsg').empty();

                $("#deletedialog").data('keyval', keyval).dialog('open');
                $("#deletedialog").html('Are you sure you want to delete promotion ' + keyval + ' ?');
                return false;
            }

            function deletePromotion(keyval) {
                $.ajax({
                    url: '${pageContext.request.contextPath}/deletePromotionMgt.action',
                    data: {subregioncode: keyval},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $("#deletesuccdialog").dialog('open');
                        $("#deletesuccdialog").html(data.message);
                        resetFieldData();
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function resetAllData() {
//                $('#promoidSearch').val("");
                $('#subregioncodeSearch').val("");
                $('#statusSearch').val("");
                $('#promotionsCategoriesSearch').val("");
                $('#subregionnameSearch').val("");
                $('#subreginaddressSearch').val("");
                $('#freetextSearch').val("");
                $('#infoSearch').val("");
                $('#longitudeSearch').val("");
                $('#latitudeSearch').val("");
                $('#promoconditionsSearch').val("");
                $('#phonenoSearch').val("");
                $('#merchantwebsiteSearch').val("");


                $("#gridtable").jqGrid('setGridParam', {
                    postData: {
                        promoidSearch: '',
                        subregioncodeSearch: '',
                        statusSearch: '',
                        promotionsCategoriesSearch: '',
                        subregionnameSearch: '',
                        subreginaddressSearch: '',
                        freetextSearch: '',
                        infoSearch: '',
                        longitudeSearch: '',
                        latitudeSearch: '',
                        promoconditionsSearch: '',
                        phonenoSearch: '',
                        merchantwebsiteSearch: '',
                        search: false
                    }
                });
                jQuery("#gridtable").trigger("reloadGrid");
            }

            function resetFieldData() {

//                $('#promoid').val("");
                $('#subregioncode').val("");
                $('#status').val("");
                $('#promotionsCategories').val("");
                $('#subregionname').val("");
                $('#subreginaddress').val("");
                $('#freetext').val("");
                $('#info').val("");
                $('#tradinghours').val("");
                $('#longitude').val("");
                $('#latitude').val("");
                $('#promoconditions').val("");
                $('#phoneno').val("");
                $('#merchantwebsite').val("");
                $('#image').val("");
                $('#regioncode').val("");
                $('#regionname').val("");
                $('#masterregoncode').val("");
                $('#masterregionname').val("");
                $('#cardtype').val("");
                $('#cardimage').val("");
                $('#startdate').val("");
                $('#enddate').val("");
//                $('#radius').val("");
                var ispushnotification = document.getElementById("ispushnotification");
                if(ispushnotification){
                  $('#ispushnotification').attr('checked', false);
                }
                $('#mobileImg').val("");
                $('#mobileImgFile').val("");
                $("#mobileImg_add").attr("src", "");
                $('#firebaseImgMobUrl').val("");
                $('#mobileImg_deffile').val("");

                $("#gridtable").jqGrid('setGridParam', {postData: {search: false}});
                jQuery("#gridtable").trigger("reloadGrid");

                $("#gridtablePend").jqGrid('setGridParam', {page: 1});
                jQuery("#gridtablePend").trigger("reloadGrid");
            }
            
            function todocsv() {
                $('#reporttype').val("csv");
                form = document.getElementById('promotionmgtsearch');
                form.action = 'reportGeneratePromotionMgt.action';
                form.submit();

                //    $('#view').button("disable");
                //    $('#view1').button("disable");
                $('#view2').button("disable");
            }
            
            $.subscribe('completetopics', function (event, data) {
                var recors = $("#gridtable").jqGrid('getGridParam', 'records');
                var isGenerate = <s:property value="vgenerate"/>;
                //  var isGenerate = false;

                if (recors > 0 && isGenerate == false) {
                    //    $('#view').button("enable");
                    //    $('#view1').button("enable");
                    $('#view2').button("enable");
                } else {
                    //    $('#view').button("disable");
                    //    $('#view1').button("disable");
                    $('#view2').button("disable");
                }
            });

            function validateImage() {
                var status_size = "";
                if (file_mob_1_width > 480) {
                    status_size = "Mobile image width should be less than or equal to 480 pixels";
                } else if (file_mob_1_height > 800) {
                    status_size = "Mobile image height should be less than or equal to 800 pixels";
                } else {
                    status_size = ""
                }
                return status_size;
            }
            
            
            function validateFloatKeyPress(el) {
                var v = parseFloat(el.value);
                el.value = (isNaN(v)) ? '' : v.toFixed(2);
            }
        </script>
        <title></title>
    </head>
    <body style="">
        <jsp:include page="/header.jsp"/>
        <div class="main-container">
            <jsp:include page="/leftmenu.jsp"/>
            <div class="main-content">
                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE NAVIGATION BAR -->
                    <jsp:include page="/navbar.jsp"/>
                    <!-- end: NAVIGATION BAR -->
                    <div class="row">
                        <div id="content1">
                            <s:div id="divmsg">
                                <s:actionerror theme="jquery"/>
                                <s:actionmessage theme="jquery"/>
                            </s:div>
                            <s:set var="vgenerate"><s:property value="vgenerate" default="true"/></s:set>
                            <s:set id="vadd" var="vadd"><s:property value="vadd" default="true"/></s:set>
                            <s:set var="vupdatebutt"><s:property value="vupdatebutt" default="true"/></s:set>
                            <s:set var="vupdatelink"><s:property value="vupdatelink" default="true"/></s:set>
                            <s:set var="vdelete"><s:property value="vdelete" default="true"/></s:set>
                            <s:set var="vconfirm"><s:property value="vconfirm" default="true"/></s:set>
                            <s:set var="vreject"><s:property value="vreject" default="true"/></s:set>
                            <s:set var="vsearch"><s:property value="vsearch" default="true"/></s:set>
                            <s:set var="vdual"><s:property value="vdual" default="true"/></s:set>
                            <s:set var="vupdatedefimg"><s:property value="vupdatedefimg" default="true"/></s:set>

                                <div id="formstyle">
                                <s:form cssClass="form" id="promotionmgtsearch" method="post" action="PromotionMgt" theme="simple" >
                                    
                                    <s:hidden name="reporttype" id="reporttype"></s:hidden>
                                    <div class="row row_1">
                                        <!--                                        <div class="col-sm-3">
                                                                                    <div class="form-group">
                                                                                        <label>promotion ID </label>
                                        <%--<s:textfield cssClass="form-control" name="promoidSearch" id="promoidSearch" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>--%>
                                    </div>
                                </div>-->
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Subregion Code</label>
                                                <s:textfield cssClass="form-control" name="subregioncodeSearch" id="subregioncodeSearch" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group form-inline">
                                                <label >Status</label>
                                                <s:select  cssClass="form-control" name="statusSearch" id="statusSearch" list="%{statusList}"   headerKey=""  headerValue="--Select Status--" listKey="statuscode" listValue="description" value="%{status}" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Promotions Category</label>
                                                <s:select cssClass="form-control" name="promotionsCategoriesSearch" id="promotionsCategoriesSearch" list="%{promotionsCategoriesList}" headerKey=""  headerValue="--Select Promotion Category--" listKey="catcode" listValue="catname" value="" disabled="false"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Subregion Name</label>
                                                <s:textfield cssClass="form-control" name="subregionnameSearch" id="subregionnameSearch" maxLength="255" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Subregion Address</label>
                                                <s:textfield cssClass="form-control" name="subreginaddressSearch" id="subreginaddressSearch" maxLength="512" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Free Text</label>
                                                <s:textfield cssClass="form-control" name="freetextSearch" id="freetextSearch" maxLength="100"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Information</label>
                                                <s:textfield cssClass="form-control" name="infoSearch" id="infoSearch" maxLength="255" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Latitude</label>
                                                <s:textfield cssClass="form-control" name="latitudeSearch" id="latitudeSearch" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row_1">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Longitude</label>
                                                <s:textfield cssClass="form-control" name="longitudeSearch" id="longitudeSearch" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Promotion Conditions</label>
                                                <s:textfield cssClass="form-control" name="promoconditionsSearch" id="promoconditionsSearch" maxLength="95" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Phone Number</label>
                                                <s:textfield cssClass="form-control" name="phonenoSearch" id="phonenoSearch" maxLength="30" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Merchant Web Site</label>
                                                <s:textfield cssClass="form-control" name="merchantwebsiteSearch" id="merchantwebsiteSearch" maxLength="255"/>
                                            </div>
                                        </div>
                                    </div>
                                </s:form>
                                    <div class="row row_1 form-inline">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <sj:submit 
                                                    button="true"
                                                    value="Search" 
                                                    href="#"
                                                    disabled="#vsearch"
                                                    onClick="searchPage()"  
                                                    id="searchbut"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div> 
                                            <div class="form-group">
                                                <sj:submit 
                                                    button="true" 
                                                    value="Reset" 
                                                    name="reset" 
                                                    onClick="resetAllData()" 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;"
                                                    />
                                            </div>
                                            <div class="form-group">
                                                <sj:submit 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    button="true" 
                                                    value="View CSV" 
                                                    name="view2" 
                                                    id="view2" 
                                                    onClick="todocsv()" 
                                                    disabled="#vgenerate"/> 
                                            </div>
                                        </div>
                                        <div class="col-sm-8  text-right">
                                            <div class="form-group">                                               
                                                <s:url var="defimagurl" action="viewDefImgPromotionMgt"/>                                                    
                                                <sj:submit 
                                                    openDialog="uploadeimgdialog"
                                                    button="true"
                                                    href="%{defimagurl}"
                                                    disabled="#vupdatedefimg"
                                                    value="Upload Default Image"
                                                    id="defImagButton" 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                            <div class="form-group">
                                                <s:url var="uploadurl" action="viewPopupcsvPromotionMgt"/>   
                                                <sj:submit                                                      
                                                    openDialog="remotedialog"
                                                    button="true"
                                                    href="%{uploadurl}"
                                                    disabled="#vupload"
                                                    value="Upload Promotion"
                                                    id="uploadButton"
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                            <div class="form-group">                                               
                                                <s:url var="addurl" action="viewPopupPromotionMgt"/>                                                    
                                                <sj:submit 
                                                    openDialog="remotedialog"
                                                    button="true"
                                                    href="%{addurl}"
                                                    disabled="#vadd"
                                                    value="Add New Promotion"
                                                    id="addButton" 
                                                    cssClass="form-control btn_normal"
                                                    cssStyle="border-radius: 12px;background-color:#969595;color:white;"
                                                    />
                                            </div>
                                        </div>
                                    </div>

                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="remotedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Add Promotion"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />  
                                <!-- Start add dialog box -->
                                <sj:dialog                                     
                                    id="uploadeimgdialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Upload Default Image"                            
                                    loadingText="Loading .."                            
                                    position="center"                            
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />  
                                <!-- Start update dialog box -->
                                <sj:dialog                                     
                                    id="updatedialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Update Promotion"
                                    onOpenTopics="openviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start detail dialog box -->
                                <sj:dialog                                     
                                    id="viewdetaildialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="Promotion"
                                    onOpenTopics="opendetailviewtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start Pend view dialog box -->
                                <sj:dialog                                     
                                    id="viewpenddialog"                                 
                                    autoOpen="false" 
                                    modal="true" 
                                    position="center"
                                    title="View Pending Promotion"
                                    onOpenTopics="openviewpendtasktopage" 
                                    loadingText="Loading .."
                                    width="900"
                                    height="450"
                                    dialogClass= "fixed-dialog"
                                    />
                                <!-- Start pend Default image dialog box -->
                                <sj:dialog 
                                    id="penddefimagedialog" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="300"
                                    height="300"
                                    title="View Pending Default Image"                            
                                    />
                                <!-- Start delete confirm dialog box -->
                                <sj:dialog 
                                    id="deletedialog" 
                                    buttons="{ 
                                    'OK':function() { deletePromotion($(this).data('keyval'));$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete Promotion"                            
                                    />
                                <!-- Start delete process dialog box -->
                                <sj:dialog 
                                    id="deletesuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Deleting Process" 
                                    />
                                <!-- Start delete error dialog box -->
                                <sj:dialog 
                                    id="deleteerrordialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}                                    
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    title="Delete error"
                                    />
                                <!-- Start approve confirm dialog box -->
                                <sj:dialog 
                                    id="confirmdialog" 
                                    buttons="{ 
                                    'OK':function() { confirmPM($(this).data('keyval'),$('#commentConfirm').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Approve Requested Operation"                            
                                    />
                                <!-- Start approve process dialog box -->
                                <sj:dialog 
                                    id="confirmsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    width="350"
                                    title="Requested Operation Approving Process" 
                                    />

                                <!-- Start reject confirm dialog box -->
                                <sj:dialog 
                                    id="rejectdialog" 
                                    buttons="{ 
                                    'OK':function() { rejectPM($(this).data('keyval'),$('#commentReject').val());$( this ).dialog( 'close' ); },
                                    'Cancel':function() { $( this ).dialog( 'close' );} 
                                    }" 
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="600"
                                    height="300"
                                    title="Reject Requested Operation"                            
                                    />
                                <!-- Start reject process dialog box -->
                                <sj:dialog 
                                    id="rejectsuccdialog" 
                                    buttons="{
                                    'OK':function() { $( this ).dialog( 'close' );}
                                    }"  
                                    autoOpen="false" 
                                    modal="true" 
                                    dialogClass= "fixed-dialog"
                                    width="350"
                                    title="Requested Operation Rejecting Process" 
                                    />
                            </div>
                            <div id="tablediv">
                                <s:url var="listurl" action="ListPromotionMgt"/>
                                <s:set var="pcaption">${CURRENTPAGE}</s:set>

                                <sjg:grid
                                    id="gridtable"
                                    caption="%{pcaption}"
                                    dataType="json"
                                    href="%{listurl}"
                                    pager="true"
                                    gridModel="gridModel"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    onCompleteTopics="completetopics"
                                    rowTotal="false"
                                    viewrecords="true"
                                    onErrorTopics="anyerrors"
                                    shrinkToFit="false"
                                    > 
                                    <sjg:gridColumn name="subregioncode" index="u.subregioncode" title="View" width="40" align="center" formatter="viewformatter" sortable="false" />
                                    <sjg:gridColumn name="subregioncode" index="u.subregioncode" title="Edit" width="25" align="center" formatter="editformatter" sortable="false" hidden="#vupdatelink"/>
                                    <sjg:gridColumn name="subregioncode" index="u.subregioncode" title="Delete" width="40" align="center" formatter="deleteformatter" sortable="false" hidden="#vdelete"/>                                                    
                                    <sjg:gridColumn name="subregioncode" index="u.subregioncode" title="Subregion Code"  sortable="true"/>
                                    <%--<sjg:gridColumn name="promotionsCards" index="u.promotionsCards.subregioncode" title="Subregion Code"  sortable="true"/>--%>
                                    <sjg:gridColumn name="status" index="u.status.description" title="Status"  sortable="true"/>
                                    <sjg:gridColumn name="promotionsCategories" index="u.promotionsCategories.catname" title="Promotions Categories"  sortable="true"/>
                                    <sjg:gridColumn name="subregionname" index="u.subregionname" title="Subregion Name"  sortable="true"/>
                                    <sjg:gridColumn name="subreginaddress" index="u.subreginaddress" title="Subregin Address"  sortable="true"/>
                                    <sjg:gridColumn name="freetext" index="u.freetext" title="Free Text"  sortable="true"/>
                                    <sjg:gridColumn name="info" index="u.info" title="Information"  sortable="true"/>
                                    <sjg:gridColumn name="tradinghours" index="u.tradinghours" title="Trading Hours"  sortable="true"/>
                                    <sjg:gridColumn name="latitude" index="u.latitude" title="Latitude"  sortable="true"/>
                                    <sjg:gridColumn name="longitude" index="u.longitude" title="Longitude"  sortable="true"/>
                                    <sjg:gridColumn name="promoconditions" index="u.promoconditions" title="Promotion Conditions"  sortable="true"/>
                                    <sjg:gridColumn name="phoneno" index="u.phoneno" title="Phone Number"  sortable="true"/>
                                    <sjg:gridColumn name="merchantwebsite" index="u.merchantwebsite" title="Merchant Website"  sortable="true"/>
                                    <sjg:gridColumn name="image" index="u.image" title="Image"  sortable="true"/>
                                    <sjg:gridColumn name="regioncode" index="u.regioncode" title="Region Code"  sortable="true"/>
                                    <sjg:gridColumn name="regionname" index="u.regionname" title="Region Name"  sortable="true"/>
                                    <sjg:gridColumn name="masterregoncode" index="u.masterregoncode" title="Master Regon Code"  sortable="true"/>
                                    <sjg:gridColumn name="masterregionname" index="u.masterregionname" title="Master Regon Name"  sortable="true"/>
                                    <sjg:gridColumn name="cardtype" index="u.cardtype" title="Card Type"  sortable="true"/>
                                    <sjg:gridColumn name="cardimage" index="u.cardimage" title="Card Image"  sortable="true"/>
                                    <sjg:gridColumn name="startdate" index="u.startdate" title="Start Date"  sortable="true"/>
                                    <sjg:gridColumn name="enddate" index="u.enddate" title="End Date"  sortable="true"/>
                                    <sjg:gridColumn name="maker" index="u.maker" title="Maker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="checker" index="u.checker" title="Checker"  sortable="true"/>                                   
                                    <sjg:gridColumn name="createtime" index="u.createdtime" title="Created Date And Time"  sortable="true" />
                                    <sjg:gridColumn name="lastupdatedtime" index="u.lastupdatedtime" title="Last Updated Date And Time"  sortable="true" />

                                </sjg:grid> 
                            </div>

                            <!-- start dual auth table -->
                            <div id="tablediv">
                                <s:url var="listurlap" action="approveListPromotionMgt"/>

                                <sjg:grid
                                    id="gridtablePend"                                    
                                    dataType="json"
                                    href="%{listurlap}"
                                    pager="true"
                                    caption="Pending Promotion Management"
                                    gridModel="gridModelPend"
                                    rowList="10,15,20"
                                    rowNum="10"
                                    autowidth="true"
                                    rownumbers="true"
                                    rowTotal="false"
                                    viewrecords="true"  

                                    >
                                    <sjg:gridColumn name="id" index="id" title="Approve" width="40" align="center"  formatter="confirmformatter" hidden="#vconfirm"/>                        
                                    <sjg:gridColumn name="id" index="id" title="Reject" width="40" align="center" formatter="rejectformatter" hidden="#vreject"/>                   
                                    <sjg:gridColumn name="id" index="u.id" title="View/Download" width="40" align="center" formatter="viewdownloadeformatter" sortable="false" hidden="#vdual"/> 

                                    <sjg:gridColumn name="subregioncode" index="u.subregioncode" title="Subregion Code"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="operation" index="u.operation" title="Operation"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="fields" index="u.fields" title="Added/Updated Data"  sortable="true" key="true"/>
                                    <sjg:gridColumn name="status" index="u.status" title="Status"  sortable="true"/>                                                                    
                                    <sjg:gridColumn name="createduser" index="u.createduser" title="Inputter"  sortable="true"/>  
                                    <sjg:gridColumn name="createtime" index="u.createtime" title="Created Date And Time"  sortable="true" />                                                                  

                                </sjg:grid>  
                            </div>  
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->
        <!-- start: FOOTER -->
        <jsp:include page="/footer.jsp"/>
        <!-- end: FOOTER -->
        <!-- end: BODY -->
    </body>
</html>
