<%-- 
    Document   : promotionmgtdefimg
    Created on : May 15, 2019, 11:48:09 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>Upload Default Image</title>

        <script>
            function cancelDefImgData() {
                $.ajax({
                    url: '${pageContext.request.contextPath}/findDefImgPromotionMgt.action',
                    data: {},
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        $('#defimgmessage').empty();
                        var msg = data.message;
                        if (msg) {
                            $('#mobileImg_deffile').val("");
                            $("#mobileImg_def").attr("src", "");
                            $('#firebaseImgMobUrl_def').val("");
                            $('#defimgmessage').text("");
                        } else {
                            $("#mobileImg_def").attr("src", data.image);
                            $('#firebaseImgMobUrl_def').val(data.image);

                            $("#up_def").hide();
                            $("#down_def").hide();
                            $("#msg_def").hide();
                            $("#hasImage_def").val('NOIMG');
                            deletefirebase_def();
                        }
                    },
                    error: function (data) {
                        window.location = "${pageContext.request.contextPath}/LogoutUserLogin.action?";
                    }
                });
            }

            function changeMobileImageDef() {
                $("#mobileImg_deffile").change(function (event) {
                    var tmppath = URL.createObjectURL(event.target.files[0]);
                    $("#mobileImg_def").attr("src", tmppath);
                });
            }

            $('#spanimg_def').click(function () {
                $('#mobileImg_deffile').val("");
                $('#firebaseImgMobUrl_def').val("");
                $("#mobileImg_def").attr("src", "");
                $("#hasImage_def").val('NOIMG');
            });

            //========================= firebase =================================

            var file_mob_1_width;
            var file_mob_1_height;

            $(document).ready(function () {
                var _URL = window.URL || window.webkitURL;
                $("#hasImage_def").val('NOIMG');

                $("#mobileImg_deffile").change(function (e) {
                    var file, img;
                    if ((file = this.files[0])) {
                        img = new Image();
                        img.onload = function () {
                            file_mob_1_width = this.width;
                            file_mob_1_height = this.height;
                        };
                        img.onerror = function () {
                            alert("not a valid file: " + file.type);
                        };
                        img.src = _URL.createObjectURL(file);
                    }
                    $("#hasImage_def").val('IMG');
                });
            });

            function uploadtofirebase_def() {

                var file_mob_if = $('#mobileImg_deffile').val();
                var imagecount = 0;
                var sendImageCount = 0;

                if (file_mob_if != "") {
                    imagecount++;
                }
                var status = true;
                var status_size = validateImage();
//                var status_size = "";


                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    const file_mob_name = file_mob_if != "" ? $('#mobileImg_deffile').get(0).files[0].name : null
                    var fileNameExt = file_mob_name.substr(file_mob_name.lastIndexOf('.') + 1);
                    const validImageTypes = ['gif', 'jpg', 'jpeg', 'png', 'img'];
                    if (validImageTypes.includes(fileNameExt)) {
                        if (status_size == "") {
                            $("#msg_def").hide();

                            var metadata = {
                                contentType: 'image/jpeg',
                            };

                            const ref_mob = firebase.storage().ref('promotions/');
                            var file_mob = file_mob_if != "" ? $('#mobileImg_deffile').get(0).files[0] : null;

                            var name_mob = "";

                            if (file_mob_if != "") {
                                name_mob = file_mob.name;
                            }


                            $("#up_def").fadeIn();
                            $("#down_def").fadeOut();

                            $('#defimgmessage').empty();

                            //first image
                            if (file_mob_if != "") {
                                const task_mob = ref_mob.child(name_mob).put(file_mob, metadata).then(function () {
                                    ref_mob.child(name_mob).getDownloadURL().then(function (url) {
                                        console.log("mob  - " + url);
                                        $("#firebaseImgMobUrl_def").val(url);
                                        sendImageCount++;
                                        if (imagecount == sendImageCount) {
                                            $("#up_def").hide();
                                            $("#down_def").fadeIn();
                                            $("#hasImage_def").val('NOIMG');
                                        }

                                        return url;
                                    });
                                });
                            }
                        } else {
                            $("#up_def").hide();
                            $("#down_def").hide();
                            $("#msg_def").show();
                            $("#msg_def").html(status_size);
                        }
                    } else {
                        $("#up_def").hide();
                        $("#down_def").hide();
                        $("#msg_def").show();
                        $("#msg_def").html("Please upload image with one of the following extensions:gif,jpg,jpeg,png,img")
                    }
                } else {
                    $("#up_def").hide();
                    $("#down_def").hide();
                    $("#msg_def").show();
                    $("#msg_def").html("Please select the image......")
                }
            }


            function deletefirebase_def() {

                var file_mob_if = $('#mobileImg_deffile').val();

                var status = true;

                if (file_mob_if == "") {
                    status = false;
                }

                if (status) {
                    $("#msg_def").hide();
                    $("#up_def").hide();
                    $("#down_def").hide();

                    const ref_mob = firebase.storage().ref('promotions/');
                    const file_mob = file_mob_if != "" ? $('#mobileImg_deffile').get(0).files[0] : null;
                    $('#mobileImg_deffile').val("");

                    var name_mob = "";
                    if (file_mob_if != "") {
                        name_mob = file_mob.name;
                        const task_mob = ref_mob.child(name_mob).delete().then(function () {
                            console.log("deleted");
//                            $('#firebaseImgMob1Urledit').val(img1);
//                            $('#mobileImgFile').val("");
                        }).catch(function (error) {
                            switch (error.code) {
                                case 'storage/object_not_found':
                                    break;
                                case 'storage/unauthorized':
                                    break;
                                case 'storage/canceled':
                                    break;
                                case 'storage/unknown':
                                    break;
                            }
                        });
                    }
                } else {
                    $('#mobileImg_deffile').val("");
                }
            }
        </script>
        <style>
            .verticalLine {
                border-left:solid #e6e5e5;    
                height:75px;
                width:5em;
            }
            .firebaseclass{
                background: url(resouces/images/firebaselogo.svg) no-repeat;
                background-position: 74px;
                background-size: 100px;
                cursor: pointer;
                display: inline-block;
                height: 35px;
                width: 185px;
                padding: 9px;
                font-size: 13px;
                font-family: sans-serif;
                font-weight: bold;
                margin: 0 0 0 22px;
                border: 1px solid #cecece;
                transition: 0.3s;
            }
            .firebaseclass:hover{
                border: 1px solid black;
            }
            .megFirebase{
                padding: 9px;
                font-size: 12px;
                font-weight: bold;
                color: #FF5722;
                margin-left: -10px;
            }
        </style>
    </head>
    <body>
        <s:div id="defimgmessage">
            <s:actionerror theme="jquery" />
            <s:actionmessage theme="jquery"/>
        </s:div>

        <s:form id="promotionmgtdefimg" method="post"   theme="simple" cssClass="form" enctype="multipart/form-data" >
            <s:hidden id="hasImage_def" name="hasImage" ></s:hidden>
                <div class="row row_popup">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <span class="mandatoryfield">Mandatory fields are marked with *</span>
                        </div>
                    </div>
                </div>
                <div class="row row_popup">
                    <div class="horizontal_line_popup"></div>
                </div>
                <div class="row row_popup">        
                    <div class="col-sm-8">
                        <div class="form-group">
                            <span style="color: red">*</span><label >Default Mobile Image (Maximum size (w*h) : 480x800 pixels, Valid extensions : gif,jpg,jpeg,png,img)</label>   
                            <div class="row">
                                <div class="col-sm-2">
                                    <img class="image" src="<s:property value="image"/>" id="mobileImg_def" alt="Mobile Image" >
                                <span id="spanimg_def" class="ui-button-icon-primary ui-icon ui-icon-circle-close" style="position:absolute;top:10px;bottom:0;right:20%"></span>  
                            </div>
                            <div class="col-sm-1"></div>
                            <div class="col-sm-8" style="margin-top: 40px;">
                                <s:file name="mobileImg" id="mobileImg_deffile" onclick="changeMobileImageDef();"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">  
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="firebaseclass" onclick="uploadtofirebase_def();">Upload to</div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <div id="up_def" style="display: none;color: #008a00" class="megFirebase">Images uploading....</div>
                        <div id="down_def" style="display: none;color: blue" class="megFirebase">Images uploaded successfully</div>
                        <div id="msg_def" style="display: none" class="megFirebase"></div>
                    </div>
                </div>
            </div>   
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">
                <div class="col-sm-4">
                    <div class="form-group">
                        <span style="color: red">*</span><label >Default Mobile Img - Firebase URL</label>
                        <s:textarea readonly="true" name="firebaseImgMobUrl" id="firebaseImgMobUrl_def" cssClass="form-control" maxLength="300" rows="2" value="%{image}"/>
                    </div>
                </div>
            </div>             
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>

            <div class="row row_popup form-inline">
                <div class="col-sm-12 text-right">
                    <div class="form-group" style=" margin-left: 0px;margin-right: 10px;">                                               
                        <s:url action="updateDefImgPromotionMgt" var="updatedefimgurl"/>
                        <sj:submit
                            button="true"
                            value="Update"
                            href="%{updatedefimgurl}"
                            targets="defimgmessage"
                            id="updateDefImgButton"
                            cssClass="btn btn-sm active" 
                            cssStyle="background-color: #ada9a9"
                            />  
                    </div>
                    <div class="form-group" style=" margin-left: 10px;margin-right: 0px;">
                        <sj:submit 
                            button="true" 
                            value="Reset" 
                            onClick="cancelDefImgData()"
                            cssClass="btn btn-default btn-sm"
                            />                          
                    </div>

                </div>
            </div>        


        </div>

    </s:form>
</body>
</html>
