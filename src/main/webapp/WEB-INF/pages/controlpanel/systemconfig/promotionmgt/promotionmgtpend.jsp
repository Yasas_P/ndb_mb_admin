<%-- 
    Document   : promotionmgtpend
    Created on : Mar 29, 2019, 10:42:23 AM
    Author     : sivaganesan_t
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib  uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="resouces/css/common/common_popup.css">
        <title>View Pend Promotion</title> 
    </head>
    <body>
        <s:div id="pmessage">
            <s:actionerror theme="jquery"/>
            <s:actionmessage theme="jquery"/>
        </s:div>
        <s:form id="promotionmgtpend" method="post" action="PromotionMgt" theme="simple" cssClass="form" enctype="multipart/form-data"  >
            <s:hidden id="taskCodePendView" name="taskCode" value="%{taskCode}" ></s:hidden>
            <div class="row row_popup">
                    <!--                <div class="col-sm-3">
                                        <div class="form-group">
                                            <span style="color: red">*</span><label>promotion ID </label>
                <%--<s:textfield value="%{promoid}" cssClass="form-control" name="promoid" id="promoid" maxLength="20" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"/>--%>
            </div>
        </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Subregion Code</label>
                        <s:textfield value="%{subregioncode}" cssClass="form-control" name="subregioncode" id="subregioncode_p" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''))"  readonly="true" />
                        <%--<s:select value="%{promotionsCards}" cssClass="form-control" id="promotionsCards" list="%{promotionsCardsList}"  name="promotionsCards" headerKey=""  headerValue="--Select Subregion Code--" listKey="subregioncode" listValue="subregioncode"/>--%>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Task</label>
                        <s:textfield value="%{taskDescription}" cssClass="form-control" name="task" id="task_p"  readonly="true" />
                    </div>
                </div>
            </div> <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Field(s)</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Old Value</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>New Value</label>
                    </div>
                </div>
            </div>      
            <div class="row row_popup">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Status</label>
                    </div>
                </div>               
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.status}" cssClass="form-control" id="statusold_p"  name="statusold"  readonly="true"  />
                    </div>
                </div>             
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{status}" cssClass="form-control" id="status_p"  name="status"  readonly="true"  />
                    </div>
                </div>   
            </div>      
            <div class="row row_popup">              
                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Promotions Category</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.promotionsCategories}" cssClass="form-control" id="promotionsCategoriesold_p" name="promotionsCategoriesold"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionsCategories}" cssClass="form-control" id="promotionsCategories_p" name="promotionsCategories"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Subregion Name</label>
                    </div>
                </div>               
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.subregionname}" cssClass="form-control" name="subregionnameold" id="subregionnameold_p" maxLength="255"  readonly="true" />
                    </div>
                </div>               
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{subregionname}" cssClass="form-control" name="subregionname" id="subregionname_p" maxLength="255"  readonly="true" />
                    </div>
                </div> 
            </div>      
            <div class="row row_popup">               
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Subregion Address</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.subreginaddress}" cssClass="form-control" name="subreginaddressold" id="subreginaddressold_p" maxLength="512"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{subreginaddress}" cssClass="form-control" name="subreginaddress" id="subreginaddress_p" maxLength="512"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Free Text</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.freetext}" cssClass="form-control" name="freetextold" id="freetextold_p" maxLength="100"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{freetext}" cssClass="form-control" name="freetext" id="freetext_p" maxLength="100"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Information</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.info}" cssClass="form-control" name="infoold" id="infoold_p" maxLength="255"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{info}" cssClass="form-control" name="info" id="info_p" maxLength="255"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Trading Hours</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.tradinghours}" cssClass="form-control" name="tradinghoursold" id="tradinghoursold_p" maxLength="20"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{tradinghours}" cssClass="form-control" name="tradinghours" id="tradinghours_p" maxLength="20"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Latitude</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.latitude}" cssClass="form-control" name="latitudeold" id="latitudeold_p" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{latitude}" cssClass="form-control" name="latitude" id="latitude_p" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Longitude</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.longitude}" cssClass="form-control" name="longitudeold" id="longitudeold_p" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{longitude}" cssClass="form-control" name="longitude" id="longitude_p" maxLength="20" onmouseout="$(this).val($(this).val().replace(/[^0-9.]/g,''))" onkeyup="$(this).val($(this).val().replace(/[^0-9.]/g,''))"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
<!--                <div class="col-sm-3">
                    <div class="form-group">
                        <span style="color: red">*</span><label>Radius[In kilometer(km)]</label>
                        <%--<s:textfield value="%{radius}" cssClass="form-control" name="radius" id="radius_p" maxLength="20"   readonly="true" />--%>
                    </div>
                </div>-->
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Promotion Conditions</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.promoconditions}" cssClass="form-control" name="promoconditionsold" id="promoconditionsold_p" maxLength="95"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promoconditions}" cssClass="form-control" name="promoconditions" id="promoconditions_p" maxLength="95"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Phone Number</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.phoneno}" cssClass="form-control" name="phonenoold" id="phonenoold_p" maxLength="33"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{phoneno}" cssClass="form-control" name="phoneno" id="phoneno_p" maxLength="33"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Merchant Web Site</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.merchantwebsite}" cssClass="form-control" name="merchantwebsiteold" id="merchantwebsiteold_p" maxLength="255"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{merchantwebsite}" cssClass="form-control" name="merchantwebsite" id="merchantwebsite_p" maxLength="255"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Master Region Name</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.masterregionname}" cssClass="form-control" name="masterregionnameold" id="masterregionnameold_p" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{masterregionname}" cssClass="form-control" name="masterregionname" id="masterregionname_p" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                       <label>Region Code</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.regioncode}" cssClass="form-control" name="regioncodeold" id="regioncodeold_p" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{regioncode}" cssClass="form-control" name="regioncode" id="regioncode_p" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                       <label>Region Name</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.regionname}" cssClass="form-control" name="regionnameold" id="regionnameold_p" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{regionname}" cssClass="form-control" name="regionname" id="regionname_p" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Master Region Code</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.masterregoncode}" cssClass="form-control" name="masterregoncodeold" id="masterregoncodeold_p" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{masterregoncode}" cssClass="form-control" name="masterregoncode" id="masterregoncode_p" maxLength="10" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Card Type</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.cardtype}" cssClass="form-control" name="cardtypeold" id="cardtypeold_p" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{cardtype}" cssClass="form-control" name="cardtype" id="cardtype_p" maxLength="50" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Card Image</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.cardimage}" cssClass="form-control" name="cardimageold" id="cardimageold_p" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{cardimage}" cssClass="form-control" name="cardimage" id="cardimage_p" maxLength="100" onkeyup="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" onmouseout="$(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''))" readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Start Date</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.startdate}" cssClass="form-control" name="startdateold" id="startdateold_p" maxLength="50" readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{startdate}" cssClass="form-control" name="startdate" id="startdate_p" maxLength="50" readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>End Date</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.enddate}" cssClass="form-control" name="enddateold" id="enddateold_p" maxLength="50"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{enddate}" cssClass="form-control" name="enddate" id="enddate_p" maxLength="50"  readonly="true" />
                    </div>
                </div>
            </div>      
            <div class="row row_popup"> 
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Notifications Enable</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{promotionOldVal.ispushnotification}" cssClass="form-control" name="ispushnotificationold" id="ispushnotificationold_p" maxLength="50"  readonly="true" />
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <s:textfield value="%{ispushnotification}" cssClass="form-control" name="ispushnotification" id="ispushnotification_p" maxLength="50"  readonly="true" />
                    </div>
                </div>
                <!--                <div class="col-sm-3">
                                    <div class="form-group">
                                        <span style="color: red">*</span><label>Image</label>
                <%--<s:textfield value="%{image}" cssClass="form-control" name="image" id="image" maxLength="100" />--%>
            </div>
        </div>-->
            </div>
            <div class="row row_popup">
                <div class="horizontal_line_popup"></div>
            </div>
            <div class="row row_popup">        
                <div class="col-sm-3">
                    <div class="form-group">
                        <label >Mobile Image </label>   
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <img class="image" src="<s:property value="promotionOldVal.image"/>" id="mobileImgold_p" alt="Mobile Image" > 
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <img class="image" src="<s:property value="image"/>" id="mobileImg_p" alt="Mobile Image" > 
                    </div>
                </div>
            </div>
        </s:form>
    </body>
</html>
