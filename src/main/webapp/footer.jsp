<style>
    #footecss{
        background-color: #221F20; 
        border-bottom-left-radius: 5px; 
        border-bottom-right-radius: 5px; 
        padding: 2px;
        padding-bottom: 3px;
        padding-left: 15px;
        padding-right: 15px;
        font-family: sans-serif; 
        color: #b3b3b3 ;
        font-weight: bold; 
        text-shadow: 0 2px 1px black;
        text-decoration: none;
    }
    #footerf{
        bottom: 0;
        left: 0;
        position: fixed;
        right: 0;
        z-index: 10000;
        margin-bottom: 19px;
        background: #221F20;
        height: 5px;
    }
</style>
<div class="footer-fixed">
        <div id="footerf"></div>
        <div class="footer">
            <div class="footer-inner">
                <span  ><a id="footecss" href="http://www.epiclanka.net" >Copyright � 2019 Epic Lanka (pvt) Ltd. All rights reserved.</a></span>
            </div>
            <div class="footer-items">
                <span class="go-top">
                    <em class="clip-chevron-up"></em>
                </span>
            </div>
        </div>
    </div>