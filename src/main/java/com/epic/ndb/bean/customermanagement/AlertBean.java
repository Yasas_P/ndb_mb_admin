/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.customermanagement;

/**
 *
 * @author Yasas_P
 */
public class AlertBean {

    private String from_account_number = "N/A";
    private String to_account_number = "N/A";
    private String currency_code = "LKR";
    private String amount = "N/A";
    private String date_time = "N/A";
    private String from_product_name = "N/A";
    private String to_product_name = "N/A";
    private String remarks = "";
    private String buying_rate = "N/A";
    private String title = "N/A";
    private String name = "Customer";
    private String nick_name = "N/A";
    private String bank_name = "N/A";
    private String card_number = "N/A";
    private String biller_name = "N/A";
    private String ref_number = "N/A";
    private String other_bank_name = "N/A";
    private String payment_type = "N/A";
    private String date = "N/A";
    private String time = "N/A";
    private String prevLimit = "N/A";
    private String newLimit = "N/A";
    private String check_number = "N/A";
    private String reason = "N/A";
    private String service_charge = "N/A";
    private String voucher_name = "N/A";
    private String merchant_name = "N/A";
    private String statement_type = "N/A";
    private String device_name = "N/A";
    private String PromotionMessgae = "N/A";
    private String PromotionId = "N/A";

    private String pushid;
    private String mobile_number;
    private String emial;
    private String email_subjec = "N/A";
    private String push_title = "NTB Mobile banking";
    private String pin = "N/A";
    private String message_body = "N/A";
    private int mode;

    public String getFrom_account_number() {
        return from_account_number;
    }

    public void setFrom_account_number(String from_account_number) {
        this.from_account_number = from_account_number;
    }

    public String getTo_account_number() {
        return to_account_number;
    }

    public void setTo_account_number(String to_account_number) {
        this.to_account_number = to_account_number;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate_time() {
        return date_time;
    }

    public void setDate_time(String date_time) {
        this.date_time = date_time;
    }

    public String getFrom_product_name() {
        return from_product_name;
    }

    public void setFrom_product_name(String from_product_name) {
        this.from_product_name = from_product_name;
    }

    public String getTo_product_name() {
        return to_product_name;
    }

    public void setTo_product_name(String to_product_name) {
        this.to_product_name = to_product_name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getBuying_rate() {
        return buying_rate;
    }

    public void setBuying_rate(String buying_rate) {
        this.buying_rate = buying_rate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }

    public String getBiller_name() {
        return biller_name;
    }

    public void setBiller_name(String biller_name) {
        this.biller_name = biller_name;
    }

    public String getRef_number() {
        return ref_number;
    }

    public void setRef_number(String ref_number) {
        this.ref_number = ref_number;
    }

    public String getOther_bank_name() {
        return other_bank_name;
    }

    public void setOther_bank_name(String other_bank_name) {
        this.other_bank_name = other_bank_name;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrevLimit() {
        return prevLimit;
    }

    public void setPrevLimit(String prevLimit) {
        this.prevLimit = prevLimit;
    }

    public String getNewLimit() {
        return newLimit;
    }

    public void setNewLimit(String newLimit) {
        this.newLimit = newLimit;
    }

    public String getCheck_number() {
        return check_number;
    }

    public void setCheck_number(String check_number) {
        this.check_number = check_number;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getService_charge() {
        return service_charge;
    }

    public void setService_charge(String service_charge) {
        this.service_charge = service_charge;
    }

    public String getVoucher_name() {
        return voucher_name;
    }

    public void setVoucher_name(String voucher_name) {
        this.voucher_name = voucher_name;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    public void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getStatement_type() {
        return statement_type;
    }

    public void setStatement_type(String statement_type) {
        this.statement_type = statement_type;
    }

    public String getDevice_name() {
        return device_name;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public String getPromotionMessgae() {
        return PromotionMessgae;
    }

    public void setPromotionMessgae(String PromotionMessgae) {
        this.PromotionMessgae = PromotionMessgae;
    }

    public String getPromotionId() {
        return PromotionId;
    }

    public void setPromotionId(String PromotionId) {
        this.PromotionId = PromotionId;
    }

    public String getPushid() {
        return pushid;
    }

    public void setPushid(String pushid) {
        this.pushid = pushid;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getEmial() {
        return emial;
    }

    public void setEmial(String emial) {
        this.emial = emial;
    }

    public String getEmail_subjec() {
        return email_subjec;
    }

    public void setEmail_subjec(String email_subjec) {
        this.email_subjec = email_subjec;
    }

    public String getPush_title() {
        return push_title;
    }

    public void setPush_title(String push_title) {
        this.push_title = push_title;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getMessage_body() {
        return message_body;
    }

    public void setMessage_body(String message_body) {
        this.message_body = message_body;
    }

    
    
}
