package com.epic.ndb.bean.customermanagement;

/**
 *
 * @author sivaganesan_t
 */
public class CustomerUpdateBean {
    
    //---------------common/identification variable------------
    private String userid;
    private String username;
    private String cid;
    private String nic;
    private String taskCode;
    private String taskDescription;
    
    //---------------For customer status update task--------------------
    private String oldCustomerStatus;
    private String oldWaveOffStatus;
    private String oldChargeAmount;
    
    private String newCustomerStatus;
    private String newWaveOffStatus;
    private String newChargeAmount;
    
    private String oldMobStatus;
    private String oldIbStatus;
    private String newMobStatus;
    private String newIbStatus;
    
    //---------------For device status update task-------------------
    private String deviceId;
    private String deviceNickName;
    private String deviceBuildNumber;
    private String deviceManufacture;
    
    private String oldDeviceStatus;
    
    private String newDeviceStatus;
    
    //---------------For primary account update task-------------------
    private String oldDefaultAccType;
    private String oldDefaultAccNo;
    
    private String newDefaultAccType;
    private String newDefaultAccNo;

    //---------------For Customer data Updata------------------
    private String oldEmail;
    private String oldMobileNumber;
    private String oldSecondaryEmail;
    private String oldSecondaryMobile;
    private String oldPermanentAdd;
    private String oldSegmentType;
    private String oldCustomerName;
    private String oldStatus;
    private String oldCustomerCategory;
    private String oldNic;
    private String oldDob;
    private String oldAccountofficer;
    private String oldGender;
    
    private String newEmail;
    private String newMobileNumber;
    private String newSecondaryEmail;
    private String newSecondaryMobile;
    private String newPermanentAdd;
    private String newSegmentType;
    private String newCustomerName;
    private String newStatus;
    private String newCustomerCategory;
    private String newNic;
    private String newDob;
    private String newAccountofficer;
    private String newGender;
    
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getOldCustomerStatus() {
        return oldCustomerStatus;
    }

    public void setOldCustomerStatus(String oldCustomerStatus) {
        this.oldCustomerStatus = oldCustomerStatus;
    }

    public String getOldWaveOffStatus() {
        return oldWaveOffStatus;
    }

    public void setOldWaveOffStatus(String oldWaveOffStatus) {
        this.oldWaveOffStatus = oldWaveOffStatus;
    }

    public String getOldChargeAmount() {
        return oldChargeAmount;
    }

    public void setOldChargeAmount(String oldChargeAmount) {
        this.oldChargeAmount = oldChargeAmount;
    }

    public String getNewCustomerStatus() {
        return newCustomerStatus;
    }

    public void setNewCustomerStatus(String newCustomerStatus) {
        this.newCustomerStatus = newCustomerStatus;
    }

    public String getNewWaveOffStatus() {
        return newWaveOffStatus;
    }

    public void setNewWaveOffStatus(String newWaveOffStatus) {
        this.newWaveOffStatus = newWaveOffStatus;
    }

    public String getNewChargeAmount() {
        return newChargeAmount;
    }

    public void setNewChargeAmount(String newChargeAmount) {
        this.newChargeAmount = newChargeAmount;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceNickName() {
        return deviceNickName;
    }

    public void setDeviceNickName(String deviceNickName) {
        this.deviceNickName = deviceNickName;
    }

    public String getDeviceBuildNumber() {
        return deviceBuildNumber;
    }

    public void setDeviceBuildNumber(String deviceBuildNumber) {
        this.deviceBuildNumber = deviceBuildNumber;
    }

    public String getDeviceManufacture() {
        return deviceManufacture;
    }

    public void setDeviceManufacture(String deviceManufacture) {
        this.deviceManufacture = deviceManufacture;
    }

    public String getOldDeviceStatus() {
        return oldDeviceStatus;
    }

    public void setOldDeviceStatus(String oldDeviceStatus) {
        this.oldDeviceStatus = oldDeviceStatus;
    }

    public String getNewDeviceStatus() {
        return newDeviceStatus;
    }

    public void setNewDeviceStatus(String newDeviceStatus) {
        this.newDeviceStatus = newDeviceStatus;
    }

    public String getOldDefaultAccType() {
        return oldDefaultAccType;
    }

    public void setOldDefaultAccType(String oldDefaultAccType) {
        this.oldDefaultAccType = oldDefaultAccType;
    }

    public String getOldDefaultAccNo() {
        return oldDefaultAccNo;
    }

    public void setOldDefaultAccNo(String oldDefaultAccNo) {
        this.oldDefaultAccNo = oldDefaultAccNo;
    }

    public String getNewDefaultAccType() {
        return newDefaultAccType;
    }

    public void setNewDefaultAccType(String newDefaultAccType) {
        this.newDefaultAccType = newDefaultAccType;
    }

    public String getNewDefaultAccNo() {
        return newDefaultAccNo;
    }

    public void setNewDefaultAccNo(String newDefaultAccNo) {
        this.newDefaultAccNo = newDefaultAccNo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOldEmail() {
        return oldEmail;
    }

    public void setOldEmail(String oldEmail) {
        this.oldEmail = oldEmail;
    }

    public String getOldMobileNumber() {
        return oldMobileNumber;
    }

    public void setOldMobileNumber(String oldMobileNumber) {
        this.oldMobileNumber = oldMobileNumber;
    }

    public String getOldSecondaryEmail() {
        return oldSecondaryEmail;
    }

    public void setOldSecondaryEmail(String oldSecondaryEmail) {
        this.oldSecondaryEmail = oldSecondaryEmail;
    }

    public String getOldSecondaryMobile() {
        return oldSecondaryMobile;
    }

    public void setOldSecondaryMobile(String oldSecondaryMobile) {
        this.oldSecondaryMobile = oldSecondaryMobile;
    }

    public String getOldPermanentAdd() {
        return oldPermanentAdd;
    }

    public void setOldPermanentAdd(String oldPermanentAdd) {
        this.oldPermanentAdd = oldPermanentAdd;
    }

    public String getOldSegmentType() {
        return oldSegmentType;
    }

    public void setOldSegmentType(String oldSegmentType) {
        this.oldSegmentType = oldSegmentType;
    }

    public String getOldCustomerName() {
        return oldCustomerName;
    }

    public void setOldCustomerName(String oldCustomerName) {
        this.oldCustomerName = oldCustomerName;
    }

    public String getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }

    public String getOldCustomerCategory() {
        return oldCustomerCategory;
    }

    public void setOldCustomerCategory(String oldCustomerCategory) {
        this.oldCustomerCategory = oldCustomerCategory;
    }

    public String getOldNic() {
        return oldNic;
    }

    public void setOldNic(String oldNic) {
        this.oldNic = oldNic;
    }

    public String getOldDob() {
        return oldDob;
    }

    public void setOldDob(String oldDob) {
        this.oldDob = oldDob;
    }

    public String getOldAccountofficer() {
        return oldAccountofficer;
    }

    public void setOldAccountofficer(String oldAccountofficer) {
        this.oldAccountofficer = oldAccountofficer;
    }

    public String getOldGender() {
        return oldGender;
    }

    public void setOldGender(String oldGender) {
        this.oldGender = oldGender;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getNewMobileNumber() {
        return newMobileNumber;
    }

    public void setNewMobileNumber(String newMobileNumber) {
        this.newMobileNumber = newMobileNumber;
    }

    public String getNewSecondaryEmail() {
        return newSecondaryEmail;
    }

    public void setNewSecondaryEmail(String newSecondaryEmail) {
        this.newSecondaryEmail = newSecondaryEmail;
    }

    public String getNewSecondaryMobile() {
        return newSecondaryMobile;
    }

    public void setNewSecondaryMobile(String newSecondaryMobile) {
        this.newSecondaryMobile = newSecondaryMobile;
    }

    public String getNewPermanentAdd() {
        return newPermanentAdd;
    }

    public void setNewPermanentAdd(String newPermanentAdd) {
        this.newPermanentAdd = newPermanentAdd;
    }

    public String getNewSegmentType() {
        return newSegmentType;
    }

    public void setNewSegmentType(String newSegmentType) {
        this.newSegmentType = newSegmentType;
    }

    public String getNewCustomerName() {
        return newCustomerName;
    }

    public void setNewCustomerName(String newCustomerName) {
        this.newCustomerName = newCustomerName;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    public String getNewCustomerCategory() {
        return newCustomerCategory;
    }

    public void setNewCustomerCategory(String newCustomerCategory) {
        this.newCustomerCategory = newCustomerCategory;
    }

    public String getNewNic() {
        return newNic;
    }

    public void setNewNic(String newNic) {
        this.newNic = newNic;
    }

    public String getNewDob() {
        return newDob;
    }

    public void setNewDob(String newDob) {
        this.newDob = newDob;
    }

    public String getNewAccountofficer() {
        return newAccountofficer;
    }

    public void setNewAccountofficer(String newAccountofficer) {
        this.newAccountofficer = newAccountofficer;
    }

    public String getNewGender() {
        return newGender;
    }

    public void setNewGender(String newGender) {
        this.newGender = newGender;
    }

    public String getOldMobStatus() {
        return oldMobStatus;
    }

    public void setOldMobStatus(String oldMobStatus) {
        this.oldMobStatus = oldMobStatus;
    }

    public String getOldIbStatus() {
        return oldIbStatus;
    }

    public void setOldIbStatus(String oldIbStatus) {
        this.oldIbStatus = oldIbStatus;
    }

    public String getNewMobStatus() {
        return newMobStatus;
    }

    public void setNewMobStatus(String newMobStatus) {
        this.newMobStatus = newMobStatus;
    }

    public String getNewIbStatus() {
        return newIbStatus;
    }

    public void setNewIbStatus(String newIbStatus) {
        this.newIbStatus = newIbStatus;
    }

}
