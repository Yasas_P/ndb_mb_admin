/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.customermanagement;

/**
 *
 * @author sivaganesan_t
 */
public class CustomerSearchBean {
    private String userid;
    private String status;
    private String promotionMtSegmentType;
    private String nic;
    private String dob;
    private String cif;
    private String username;
    private String email;
    private String mobileNumber;
    private String loginAttempts;
    private String loginAttemptStatus;
    private String lastupdatedtime;
    private String createdtime;
    private String otp;
    private String otpExpTime;
    private String mobilePin;
    private String userTraceNumber;
    private String secondaryEmail;
    private String secondaryMobile;
    private String latitude;
    private String longitude;
    private String statusDes;
    private String customerName;
    private String lastLoggedinDatetime;
    private String onBoardChannel;
    private String onBoardType;
    private String customerCategory;
    private String defType;
    private String defAccNo;
    private String loginStatus;
    private String lastPassUpdateTime;
    private String finalFeeDudDay;
    private String accountofficer;
    
    private String gender;
    private String idtype;
    private String permanentaddress;
    private String correspondenceaddress;
    private String officeaddress;
    private String usersegment;
    private String waveoff;
    private String chargeAmount;
    
    private String remark;
    private String maker;
    private String checker;
    
    private String deviceid;
    private String devicecreatedTime;
    private String deviceBuildNumber;
    private String deviceManufacture;
    private String deviceModel;
    private String deviceNickName;
    private String deviceOsVersion;
    private String deviceimie;
    private String devicelastUpdatedTime;
    private String devicepushId;
    private String devicepushSha;
    private String devicestatus;
    private String devicestatusDes;
    private String deviceuserid;
    
    private long fullCount;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromotionMtSegmentType() {
        return promotionMtSegmentType;
    }

    public void setPromotionMtSegmentType(String promotionMtSegmentType) {
        this.promotionMtSegmentType = promotionMtSegmentType;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(String loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtpExpTime() {
        return otpExpTime;
    }

    public void setOtpExpTime(String otpExpTime) {
        this.otpExpTime = otpExpTime;
    }

    public String getMobilePin() {
        return mobilePin;
    }

    public void setMobilePin(String mobilePin) {
        this.mobilePin = mobilePin;
    }

    public String getUserTraceNumber() {
        return userTraceNumber;
    }

    public void setUserTraceNumber(String userTraceNumber) {
        this.userTraceNumber = userTraceNumber;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public String getSecondaryMobile() {
        return secondaryMobile;
    }

    public void setSecondaryMobile(String secondaryMobile) {
        this.secondaryMobile = secondaryMobile;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStatusDes() {
        return statusDes;
    }

    public void setStatusDes(String statusDes) {
        this.statusDes = statusDes;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype;
    }

    public String getPermanentaddress() {
        return permanentaddress;
    }

    public void setPermanentaddress(String permanentaddress) {
        this.permanentaddress = permanentaddress;
    }

    public String getCorrespondenceaddress() {
        return correspondenceaddress;
    }

    public void setCorrespondenceaddress(String correspondenceaddress) {
        this.correspondenceaddress = correspondenceaddress;
    }

    public String getOfficeaddress() {
        return officeaddress;
    }

    public void setOfficeaddress(String officeaddress) {
        this.officeaddress = officeaddress;
    }

    public String getUsersegment() {
        return usersegment;
    }

    public void setUsersegment(String usersegment) {
        this.usersegment = usersegment;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getDevicecreatedTime() {
        return devicecreatedTime;
    }

    public void setDevicecreatedTime(String devicecreatedTime) {
        this.devicecreatedTime = devicecreatedTime;
    }

    public String getDeviceBuildNumber() {
        return deviceBuildNumber;
    }

    public void setDeviceBuildNumber(String deviceBuildNumber) {
        this.deviceBuildNumber = deviceBuildNumber;
    }

    public String getDeviceManufacture() {
        return deviceManufacture;
    }

    public void setDeviceManufacture(String deviceManufacture) {
        this.deviceManufacture = deviceManufacture;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceNickName() {
        return deviceNickName;
    }

    public void setDeviceNickName(String deviceNickName) {
        this.deviceNickName = deviceNickName;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getDeviceimie() {
        return deviceimie;
    }

    public void setDeviceimie(String deviceimie) {
        this.deviceimie = deviceimie;
    }

    public String getDevicelastUpdatedTime() {
        return devicelastUpdatedTime;
    }

    public void setDevicelastUpdatedTime(String devicelastUpdatedTime) {
        this.devicelastUpdatedTime = devicelastUpdatedTime;
    }

    public String getDevicepushId() {
        return devicepushId;
    }

    public void setDevicepushId(String devicepushId) {
        this.devicepushId = devicepushId;
    }

    public String getDevicepushSha() {
        return devicepushSha;
    }

    public void setDevicepushSha(String devicepushSha) {
        this.devicepushSha = devicepushSha;
    }

    public String getDevicestatus() {
        return devicestatus;
    }

    public void setDevicestatus(String devicestatus) {
        this.devicestatus = devicestatus;
    }

    public String getDevicestatusDes() {
        return devicestatusDes;
    }

    public void setDevicestatusDes(String devicestatusDes) {
        this.devicestatusDes = devicestatusDes;
    }

    public String getDeviceuserid() {
        return deviceuserid;
    }

    public void setDeviceuserid(String deviceuserid) {
        this.deviceuserid = deviceuserid;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLastLoggedinDatetime() {
        return lastLoggedinDatetime;
    }

    public void setLastLoggedinDatetime(String lastLoggedinDatetime) {
        this.lastLoggedinDatetime = lastLoggedinDatetime;
    }

    public String getOnBoardChannel() {
        return onBoardChannel;
    }

    public void setOnBoardChannel(String onBoardChannel) {
        this.onBoardChannel = onBoardChannel;
    }

    public String getOnBoardType() {
        return onBoardType;
    }

    public void setOnBoardType(String onBoardType) {
        this.onBoardType = onBoardType;
    }

    public String getCustomerCategory() {
        return customerCategory;
    }

    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    public String getDefType() {
        return defType;
    }

    public void setDefType(String defType) {
        this.defType = defType;
    }

    public String getDefAccNo() {
        return defAccNo;
    }

    public void setDefAccNo(String defAccNo) {
        this.defAccNo = defAccNo;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getLastPassUpdateTime() {
        return lastPassUpdateTime;
    }

    public void setLastPassUpdateTime(String lastPassUpdateTime) {
        this.lastPassUpdateTime = lastPassUpdateTime;
    }

    public String getFinalFeeDudDay() {
        return finalFeeDudDay;
    }

    public void setFinalFeeDudDay(String finalFeeDudDay) {
        this.finalFeeDudDay = finalFeeDudDay;
    }

    public String getWaveoff() {
        return waveoff;
    }

    public void setWaveoff(String waveoff) {
        this.waveoff = waveoff;
    }

    public String getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getAccountofficer() {
        return accountofficer;
    }

    public void setAccountofficer(String accountofficer) {
        this.accountofficer = accountofficer;
    }

    public String getLoginAttemptStatus() {
        return loginAttemptStatus;
    }

    public void setLoginAttemptStatus(String loginAttemptStatus) {
        this.loginAttemptStatus = loginAttemptStatus;
    }
    
}
