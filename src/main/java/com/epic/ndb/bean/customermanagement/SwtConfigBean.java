/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.customermanagement;

/**
 *
 * @author sivaganesan_t
 */
public class SwtConfigBean {
    private String id;
    private String ntbRestBaseUrl;
    private String restAutorization;
    private String restContentType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNtbRestBaseUrl() {
        return ntbRestBaseUrl;
    }

    public void setNtbRestBaseUrl(String ntbRestBaseUrl) {
        this.ntbRestBaseUrl = ntbRestBaseUrl;
    }

    public String getRestAutorization() {
        return restAutorization;
    }

    public void setRestAutorization(String restAutorization) {
        this.restAutorization = restAutorization;
    }

    public String getRestContentType() {
        return restContentType;
    }

    public void setRestContentType(String restContentType) {
        this.restContentType = restContentType;
    }
    
}
