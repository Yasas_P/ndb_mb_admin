/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.customermanagement;

import com.epic.ndb.bean.analytics.ChannelTypeBean;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.TransferType;
import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class CustomerSearchInputBean {

    private String userid;
    private String status;
    private String statusCategory;
    private String CusSegmentType;
    private String customerCategory;
    private String nic;
    private String dob;
    private String cif;
    private String ciflimit;
    private String cifhidden;
    private String username;
    private String email;
    private String mobileNumber;
    private String loginAttempts;
    private String loginAttemptStatus;
    private String lastupdatedtime;
    private String createdtime;
    private Blob profileImage;
    private String profImageTimestamp;
    private String otp;
    private String otpExpTime;
    private String mobilePin;
    private String userTraceNumber;
    private String secondaryEmail;
    private String secondaryMobile;
    private Double latitude;
    private Double longitude;
    private String gender;
    private String globalVal;
    private String daonDeviceId;
    private String lastlogindate;
    private String id;
    private String permanentAdd;
    private String correspondenceAdd;
    private String officeAdd;
    private String customerName;
    private String accountofficer;
    private String defaultAccountOld;
    private String defaultAccountnew;
    //------------new added field-----------
    private String onBoardChannel;
    private String onBoardType;
    private String defType;
    private String defTypeHidden;
    private String defAccNo;
    private String nameoncard;
    private String loginStatus;
    private String lastPassUpdateTime;
    private String finalFeeDudDay;
    private String waveoff;
    private String chargeAmount;
    private String maker;
    private String checker;
    private String cardToken;
    private String accountproduct;
    private CustomerBean newCustomerData;
    private CustomerBean newCustomerDataSession;
    private CustomerBean oldCustomerData;

    private List<AccountBean> accountList = new ArrayList<AccountBean>();
    private List<ChannelTypeBean> onBoardChannelList = new ArrayList<ChannelTypeBean>();
    private List<KeyValueBean> onBoardTypeList = new ArrayList<KeyValueBean>();
    private List<KeyValueBean> defaultAccountTypeList = new ArrayList<KeyValueBean>();

    private String txntype;
    private String userLimit;
    private String useridLimit;
    private String txntypelimit;
    private String statuslimit;
    private String CusSegmentTypelimit;

    private String message;
    private String errormessage;

    private ByteArrayInputStream excelStream;
    private ByteArrayInputStream zipStream;
    private String reporttype;

    private List<Status> statusList;
    private List<Status> cusStatusList;
    private List<Status> waveOffStatusList;
    private List<SegmentType> segmentTypeList;
    private List<TransferType> transactiontypeList;

    private String currentUser;
    private String remark;

    /*------------------------list data table  ------------------------------*/
    private List<CustomerSearchBean> gridModel;
    private List<CustomerSearchPendBean> gridModelPend;
    private List<CustomerSearchBean> gridModelDevice;
    private List<CustomerSearchLimitBean> gridModelLimit;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;

    /*------------------------list data table  ------------------------------*/
 /*------------------------for search ------------------------------*/
    private String cifSearch;
    private String usernameSearch;
    private String nicSearch;
    private String statusSearch;
    private String statusDesSearch;
    private String firstnameSearch;
    private String lastnameSearch;
    private String mobilenoSearch;
    private String emailSearch;
    private String segmentTypeSearch;
    private String fromDate;
    private String toDate;
    private String customerCategorySearch;
    private String onBoardChannelSearch;
    private String onBoardTypeSearch;
    private String waveoffSearch;
    private String defTypeSearch;
    private String defAccNoSearch;

    private String SearchAudit;
    private String newValueAudit;
    private boolean search;
    /*------------------------for search ------------------------------*/

 /*-------for access control-----------*/
    private boolean vdelete;//
    private boolean vsearch;//
    private boolean vviewcustomer;//
    private boolean vconfirm;//
    private boolean vreject;//
    private boolean vstatuschange;//
    private boolean vregister;
    private boolean vlimitchange;
    private boolean vreconfirm;
    private boolean vresetattempts;
    private boolean vinvalidsignup;
    private boolean vupdate;//
    private boolean vgenerate;
    private boolean vconfeegenerate;
    private boolean vgenerateview;
    private boolean vpinreset;
    private boolean vreg;
    private boolean vviewreg;
    private boolean vlimitadd;//
    private boolean vlimitupdate;//
    private boolean vlimitdelete;//
    private boolean vprimaccupdate;//
    private boolean vattemptreset;//
    private boolean vdual;//
    private boolean vpasswordreset;//
    private boolean vblock;
    private boolean vupdatecustomer;//update by t24 data
    /*-------for access control-----------*/

    private String statusByMbStatus;
    private String statusByIbStatus;

    // Device details
    private String deviceid;
    private String devicecreatedTime;
    private String deviceBuildNumber;
    private String deviceManufacture;
    private String deviceModel;
    private String deviceNickName;
    private String deviceOsVersion;
    private String deviceimie;
    private String devicelastUpdatedTime;
    private String devicepushId;
    private String devicepushSha;
    private String devicestatus;
    private String deviceuserid;
    private String fullname;

    // Pending Old New Value
    private CustomerUpdateBean pendOldNew;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCategory() {
        return statusCategory;
    }

    public void setStatusCategory(String statusCategory) {
        this.statusCategory = statusCategory;
    }

    public String getCusSegmentType() {
        return CusSegmentType;
    }

    public void setCusSegmentType(String CusSegmentType) {
        this.CusSegmentType = CusSegmentType;
    }

    public String getCusSegmentTypelimit() {
        return CusSegmentTypelimit;
    }

    public void setCusSegmentTypelimit(String CusSegmentTypelimit) {
        this.CusSegmentTypelimit = CusSegmentTypelimit;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getCiflimit() {
        return ciflimit;
    }

    public void setCiflimit(String ciflimit) {
        this.ciflimit = ciflimit;
    }

    public String getCifhidden() {
        return cifhidden;
    }

    public void setCifhidden(String cifhidden) {
        this.cifhidden = cifhidden;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(String loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public Blob getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(Blob profileImage) {
        this.profileImage = profileImage;
    }

    public String getProfImageTimestamp() {
        return profImageTimestamp;
    }

    public void setProfImageTimestamp(String profImageTimestamp) {
        this.profImageTimestamp = profImageTimestamp;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtpExpTime() {
        return otpExpTime;
    }

    public void setOtpExpTime(String otpExpTime) {
        this.otpExpTime = otpExpTime;
    }

    public String getMobilePin() {
        return mobilePin;
    }

    public void setMobilePin(String mobilePin) {
        this.mobilePin = mobilePin;
    }

    public String getUserTraceNumber() {
        return userTraceNumber;
    }

    public void setUserTraceNumber(String userTraceNumber) {
        this.userTraceNumber = userTraceNumber;
    }

    public String getSecondaryEmail() {
        return secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    public String getSecondaryMobile() {
        return secondaryMobile;
    }

    public void setSecondaryMobile(String secondaryMobile) {
        this.secondaryMobile = secondaryMobile;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGlobalVal() {
        return globalVal;
    }

    public void setGlobalVal(String globalVal) {
        this.globalVal = globalVal;
    }

    public String getDaonDeviceId() {
        return daonDeviceId;
    }

    public void setDaonDeviceId(String daonDeviceId) {
        this.daonDeviceId = daonDeviceId;
    }

    public List<CustomerSearchBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<CustomerSearchBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getCifSearch() {
        return cifSearch;
    }

    public void setCifSearch(String cifSearch) {
        this.cifSearch = cifSearch;
    }

    public String getUsernameSearch() {
        return usernameSearch;
    }

    public void setUsernameSearch(String usernameSearch) {
        this.usernameSearch = usernameSearch;
    }

    public String getNicSearch() {
        return nicSearch;
    }

    public void setNicSearch(String nicSearch) {
        this.nicSearch = nicSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getFirstnameSearch() {
        return firstnameSearch;
    }

    public void setFirstnameSearch(String firstnameSearch) {
        this.firstnameSearch = firstnameSearch;
    }

    public String getLastnameSearch() {
        return lastnameSearch;
    }

    public void setLastnameSearch(String lastnameSearch) {
        this.lastnameSearch = lastnameSearch;
    }

    public String getMobilenoSearch() {
        return mobilenoSearch;
    }

    public void setMobilenoSearch(String mobilenoSearch) {
        this.mobilenoSearch = mobilenoSearch;
    }

    public String getEmailSearch() {
        return emailSearch;
    }

    public void setEmailSearch(String emailSearch) {
        this.emailSearch = emailSearch;
    }

    public String getSearchAudit() {
        return SearchAudit;
    }

    public void setSearchAudit(String SearchAudit) {
        this.SearchAudit = SearchAudit;
    }

    public String getNewValueAudit() {
        return newValueAudit;
    }

    public void setNewValueAudit(String newValueAudit) {
        this.newValueAudit = newValueAudit;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVviewcustomer() {
        return vviewcustomer;
    }

    public void setVviewcustomer(boolean vviewcustomer) {
        this.vviewcustomer = vviewcustomer;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public boolean isVstatuschange() {
        return vstatuschange;
    }

    public void setVstatuschange(boolean vstatuschange) {
        this.vstatuschange = vstatuschange;
    }

    public boolean isVregister() {
        return vregister;
    }

    public void setVregister(boolean vregister) {
        this.vregister = vregister;
    }

    public boolean isVlimitchange() {
        return vlimitchange;
    }

    public void setVlimitchange(boolean vlimitchange) {
        this.vlimitchange = vlimitchange;
    }

    public boolean isVreconfirm() {
        return vreconfirm;
    }

    public void setVreconfirm(boolean vreconfirm) {
        this.vreconfirm = vreconfirm;
    }

    public boolean isVresetattempts() {
        return vresetattempts;
    }

    public void setVresetattempts(boolean vresetattempts) {
        this.vresetattempts = vresetattempts;
    }

    public boolean isVinvalidsignup() {
        return vinvalidsignup;
    }

    public void setVinvalidsignup(boolean vinvalidsignup) {
        this.vinvalidsignup = vinvalidsignup;
    }

    public boolean isVupdate() {
        return vupdate;
    }

    public void setVupdate(boolean vupdate) {
        this.vupdate = vupdate;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public boolean isVpinreset() {
        return vpinreset;
    }

    public void setVpinreset(boolean vpinreset) {
        this.vpinreset = vpinreset;
    }

    public boolean isVreg() {
        return vreg;
    }

    public void setVreg(boolean vreg) {
        this.vreg = vreg;
    }

    public boolean isVviewreg() {
        return vviewreg;
    }

    public void setVviewreg(boolean vviewreg) {
        this.vviewreg = vviewreg;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public List<Status> getCusStatusList() {
        return cusStatusList;
    }

    public void setCusStatusList(List<Status> cusStatusList) {
        this.cusStatusList = cusStatusList;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<CustomerSearchPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<CustomerSearchPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPermanentAdd() {
        return permanentAdd;
    }

    public void setPermanentAdd(String permanentAdd) {
        this.permanentAdd = permanentAdd;
    }

    public String getCorrespondenceAdd() {
        return correspondenceAdd;
    }

    public void setCorrespondenceAdd(String correspondenceAdd) {
        this.correspondenceAdd = correspondenceAdd;
    }

    public String getOfficeAdd() {
        return officeAdd;
    }

    public void setOfficeAdd(String officeAdd) {
        this.officeAdd = officeAdd;
    }

    public String getLastlogindate() {
        return lastlogindate;
    }

    public void setLastlogindate(String lastlogindate) {
        this.lastlogindate = lastlogindate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<CustomerSearchBean> getGridModelDevice() {
        return gridModelDevice;
    }

    public void setGridModelDevice(List<CustomerSearchBean> gridModelDevice) {
        this.gridModelDevice = gridModelDevice;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getDevicecreatedTime() {
        return devicecreatedTime;
    }

    public void setDevicecreatedTime(String devicecreatedTime) {
        this.devicecreatedTime = devicecreatedTime;
    }

    public String getDeviceBuildNumber() {
        return deviceBuildNumber;
    }

    public void setDeviceBuildNumber(String deviceBuildNumber) {
        this.deviceBuildNumber = deviceBuildNumber;
    }

    public String getDeviceManufacture() {
        return deviceManufacture;
    }

    public void setDeviceManufacture(String deviceManufacture) {
        this.deviceManufacture = deviceManufacture;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDeviceNickName() {
        return deviceNickName;
    }

    public void setDeviceNickName(String deviceNickName) {
        this.deviceNickName = deviceNickName;
    }

    public String getDeviceOsVersion() {
        return deviceOsVersion;
    }

    public void setDeviceOsVersion(String deviceOsVersion) {
        this.deviceOsVersion = deviceOsVersion;
    }

    public String getDeviceimie() {
        return deviceimie;
    }

    public void setDeviceimie(String deviceimie) {
        this.deviceimie = deviceimie;
    }

    public String getDevicelastUpdatedTime() {
        return devicelastUpdatedTime;
    }

    public void setDevicelastUpdatedTime(String devicelastUpdatedTime) {
        this.devicelastUpdatedTime = devicelastUpdatedTime;
    }

    public String getDevicepushId() {
        return devicepushId;
    }

    public void setDevicepushId(String devicepushId) {
        this.devicepushId = devicepushId;
    }

    public String getDevicepushSha() {
        return devicepushSha;
    }

    public void setDevicepushSha(String devicepushSha) {
        this.devicepushSha = devicepushSha;
    }

    public String getDevicestatus() {
        return devicestatus;
    }

    public void setDevicestatus(String devicestatus) {
        this.devicestatus = devicestatus;
    }

    public String getDeviceuserid() {
        return deviceuserid;
    }

    public void setDeviceuserid(String deviceuserid) {
        this.deviceuserid = deviceuserid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getStatusDesSearch() {
        return statusDesSearch;
    }

    public void setStatusDesSearch(String statusDesSearch) {
        this.statusDesSearch = statusDesSearch;
    }

    public ByteArrayInputStream getExcelStream() {
        return excelStream;
    }

    public void setExcelStream(ByteArrayInputStream excelStream) {
        this.excelStream = excelStream;
    }

    public ByteArrayInputStream getZipStream() {
        return zipStream;
    }

    public void setZipStream(ByteArrayInputStream zipStream) {
        this.zipStream = zipStream;
    }

    public List<TransferType> getTransactiontypeList() {
        return transactiontypeList;
    }

    public void setTransactiontypeList(List<TransferType> transactiontypeList) {
        this.transactiontypeList = transactiontypeList;
    }

    public String getTxntype() {
        return txntype;
    }

    public void setTxntype(String txntype) {
        this.txntype = txntype;
    }

    public String getUserLimit() {
        return userLimit;
    }

    public void setUserLimit(String userLimit) {
        this.userLimit = userLimit;
    }

    public String getUseridLimit() {
        return useridLimit;
    }

    public void setUseridLimit(String useridLimit) {
        this.useridLimit = useridLimit;
    }

    public String getTxntypelimit() {
        return txntypelimit;
    }

    public void setTxntypelimit(String txntypelimit) {
        this.txntypelimit = txntypelimit;
    }

    public String getStatuslimit() {
        return statuslimit;
    }

    public void setStatuslimit(String statuslimit) {
        this.statuslimit = statuslimit;
    }

    public List<CustomerSearchLimitBean> getGridModelLimit() {
        return gridModelLimit;
    }

    public void setGridModelLimit(List<CustomerSearchLimitBean> gridModelLimit) {
        this.gridModelLimit = gridModelLimit;
    }

    public boolean isVlimitadd() {
        return vlimitadd;
    }

    public void setVlimitadd(boolean vlimitadd) {
        this.vlimitadd = vlimitadd;
    }

    public boolean isVlimitupdate() {
        return vlimitupdate;
    }

    public void setVlimitupdate(boolean vlimitupdate) {
        this.vlimitupdate = vlimitupdate;
    }

    public boolean isVlimitdelete() {
        return vlimitdelete;
    }

    public void setVlimitdelete(boolean vlimitdelete) {
        this.vlimitdelete = vlimitdelete;
    }

    public List<SegmentType> getSegmentTypeList() {
        return segmentTypeList;
    }

    public void setSegmentTypeList(List<SegmentType> segmentTypeList) {
        this.segmentTypeList = segmentTypeList;
    }

    public String getSegmentTypeSearch() {
        return segmentTypeSearch;
    }

    public void setSegmentTypeSearch(String segmentTypeSearch) {
        this.segmentTypeSearch = segmentTypeSearch;
    }

    public String getDefaultAccountnew() {
        return defaultAccountnew;
    }

    public void setDefaultAccountnew(String defaultAccountnew) {
        this.defaultAccountnew = defaultAccountnew;
    }

    public String getDefaultAccountOld() {
        return defaultAccountOld;
    }

    public void setDefaultAccountOld(String defaultAccountOld) {
        this.defaultAccountOld = defaultAccountOld;
    }

    public List<AccountBean> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<AccountBean> accountList) {
        this.accountList = accountList;
    }

    public boolean isVprimaccupdate() {
        return vprimaccupdate;
    }

    public void setVprimaccupdate(boolean vprimaccupdate) {
        this.vprimaccupdate = vprimaccupdate;
    }

    public List<ChannelTypeBean> getOnBoardChannelList() {
        return onBoardChannelList;
    }

    public void setOnBoardChannelList(List<ChannelTypeBean> onBoardChannelList) {
        this.onBoardChannelList = onBoardChannelList;
    }

    public List<KeyValueBean> getOnBoardTypeList() {
        return onBoardTypeList;
    }

    public void setOnBoardTypeList(List<KeyValueBean> onBoardTypeList) {
        this.onBoardTypeList = onBoardTypeList;
    }

    public String getCustomerCategory() {
        return customerCategory;
    }

    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    public String getCustomerCategorySearch() {
        return customerCategorySearch;
    }

    public void setCustomerCategorySearch(String customerCategorySearch) {
        this.customerCategorySearch = customerCategorySearch;
    }

    public String getOnBoardChannelSearch() {
        return onBoardChannelSearch;
    }

    public void setOnBoardChannelSearch(String onBoardChannelSearch) {
        this.onBoardChannelSearch = onBoardChannelSearch;
    }

    public String getOnBoardTypeSearch() {
        return onBoardTypeSearch;
    }

    public void setOnBoardTypeSearch(String onBoardTypeSearch) {
        this.onBoardTypeSearch = onBoardTypeSearch;
    }

    public String getOnBoardChannel() {
        return onBoardChannel;
    }

    public void setOnBoardChannel(String onBoardChannel) {
        this.onBoardChannel = onBoardChannel;
    }

    public String getOnBoardType() {
        return onBoardType;
    }

    public void setOnBoardType(String onBoardType) {
        this.onBoardType = onBoardType;
    }

    public String getDefType() {
        return defType;
    }

    public void setDefType(String defType) {
        this.defType = defType;
    }

    public String getDefAccNo() {
        return defAccNo;
    }

    public void setDefAccNo(String defAccNo) {
        this.defAccNo = defAccNo;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getLastPassUpdateTime() {
        return lastPassUpdateTime;
    }

    public void setLastPassUpdateTime(String lastPassUpdateTime) {
        this.lastPassUpdateTime = lastPassUpdateTime;
    }

    public String getFinalFeeDudDay() {
        return finalFeeDudDay;
    }

    public void setFinalFeeDudDay(String finalFeeDudDay) {
        this.finalFeeDudDay = finalFeeDudDay;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public List<Status> getWaveOffStatusList() {
        return waveOffStatusList;
    }

    public void setWaveOffStatusList(List<Status> waveOffStatusList) {
        this.waveOffStatusList = waveOffStatusList;
    }

    public String getWaveoff() {
        return waveoff;
    }

    public void setWaveoff(String waveoff) {
        this.waveoff = waveoff;
    }

    public String getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getWaveoffSearch() {
        return waveoffSearch;
    }

    public void setWaveoffSearch(String waveoffSearch) {
        this.waveoffSearch = waveoffSearch;
    }

    public String getAccountofficer() {
        return accountofficer;
    }

    public void setAccountofficer(String accountofficer) {
        this.accountofficer = accountofficer;
    }

    public boolean isVattemptreset() {
        return vattemptreset;
    }

    public void setVattemptreset(boolean vattemptreset) {
        this.vattemptreset = vattemptreset;
    }

    public String getLoginAttemptStatus() {
        return loginAttemptStatus;
    }

    public void setLoginAttemptStatus(String loginAttemptStatus) {
        this.loginAttemptStatus = loginAttemptStatus;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public CustomerUpdateBean getPendOldNew() {
        return pendOldNew;
    }

    public void setPendOldNew(CustomerUpdateBean pendOldNew) {
        this.pendOldNew = pendOldNew;
    }

    public boolean isVpasswordreset() {
        return vpasswordreset;
    }

    public void setVpasswordreset(boolean vpasswordreset) {
        this.vpasswordreset = vpasswordreset;
    }

    public List<KeyValueBean> getDefaultAccountTypeList() {
        return defaultAccountTypeList;
    }

    public void setDefaultAccountTypeList(List<KeyValueBean> defaultAccountTypeList) {
        this.defaultAccountTypeList = defaultAccountTypeList;
    }

    public String getDefTypeHidden() {
        return defTypeHidden;
    }

    public void setDefTypeHidden(String defTypeHidden) {
        this.defTypeHidden = defTypeHidden;
    }

    public String getNameoncard() {
        return nameoncard;
    }

    public void setNameoncard(String nameoncard) {
        this.nameoncard = nameoncard;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getAccountproduct() {
        return accountproduct;
    }

    public void setAccountproduct(String accountproduct) {
        this.accountproduct = accountproduct;
    }

    public boolean isVconfeegenerate() {
        return vconfeegenerate;
    }

    public void setVconfeegenerate(boolean vconfeegenerate) {
        this.vconfeegenerate = vconfeegenerate;
    }

    public String getDefTypeSearch() {
        return defTypeSearch;
    }

    public void setDefTypeSearch(String defTypeSearch) {
        this.defTypeSearch = defTypeSearch;
    }

    public boolean isVblock() {
        return vblock;
    }

    public void setVblock(boolean vblock) {
        this.vblock = vblock;
    }

    public boolean isVupdatecustomer() {
        return vupdatecustomer;
    }

    public void setVupdatecustomer(boolean vupdatecustomer) {
        this.vupdatecustomer = vupdatecustomer;
    }

    public CustomerBean getNewCustomerData() {
        return newCustomerData;
    }

    public void setNewCustomerData(CustomerBean newCustomerData) {
        this.newCustomerData = newCustomerData;
    }

    public CustomerBean getOldCustomerData() {
        return oldCustomerData;
    }

    public void setOldCustomerData(CustomerBean oldCustomerData) {
        this.oldCustomerData = oldCustomerData;
    }

    public CustomerBean getNewCustomerDataSession() {
        return newCustomerDataSession;
    }

    public void setNewCustomerDataSession(CustomerBean newCustomerDataSession) {
        this.newCustomerDataSession = newCustomerDataSession;
    }

    public String getStatusByMbStatus() {
        return statusByMbStatus;
    }

    public void setStatusByMbStatus(String statusByMbStatus) {
        this.statusByMbStatus = statusByMbStatus;
    }

    public String getStatusByIbStatus() {
        return statusByIbStatus;
    }

    public void setStatusByIbStatus(String statusByIbStatus) {
        this.statusByIbStatus = statusByIbStatus;
    }
    public String getDefAccNoSearch() {
        return defAccNoSearch;
    }

    public void setDefAccNoSearch(String defAccNoSearch) {
        this.defAccNoSearch = defAccNoSearch;
    }
}
