/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.customermanagement;

/**
 *
 * @author sivaganesan_t
 */
public class AccountBean {
    private String accountNumber;
    private String accountMaskedNumber;
    private String nameoncard;
    private String accountproduct;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountMaskedNumber() {
        return accountMaskedNumber;
    }

    public void setAccountMaskedNumber(String accountMaskedNumber) {
        this.accountMaskedNumber = accountMaskedNumber;
    }

    public String getNameoncard() {
        return nameoncard;
    }

    public void setNameoncard(String nameoncard) {
        this.nameoncard = nameoncard;
    }

    public String getAccountproduct() {
        return accountproduct;
    }

    public void setAccountproduct(String accountproduct) {
        this.accountproduct = accountproduct;
    }
    
}
