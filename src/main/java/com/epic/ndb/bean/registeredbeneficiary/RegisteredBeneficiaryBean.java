/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.registeredbeneficiary;

/**
 *
 * @author sivaganesan_t
 */
public class RegisteredBeneficiaryBean {
    
    private String userId;
    private String cif;
    private String userName;
    private String customerName;
    private String customerCategory;
    private String mobileNo;
    private String registationDate;
    private String beneficiaryType;
    private String registationBeneficiary;
    private String status;
    private String name;
    private String accountType;
    private String bankCode;
    private String bankName;
    private String branchName;
    private String billerCategory;
    private String biller;

    private long fullCount;

    /**
     *
     * @return
     */
    public String getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     */
    public String getCif() {
        return cif;
    }

    /**
     *
     * @param cif
     */
    public void setCif(String cif) {
        this.cif = cif;
    }

    /**
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @param userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     *
     * @return
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     *
     * @param customerName
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     *
     * @return
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     *
     * @param mobileNo
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     *
     * @return
     */
    public String getRegistationDate() {
        return registationDate;
    }

    /**
     *
     * @param registationDate
     */
    public void setRegistationDate(String registationDate) {
        this.registationDate = registationDate;
    }

    /**
     *
     * @return
     */
    public String getRegistationBeneficiary() {
        return registationBeneficiary;
    }

    /**
     *
     * @param registationBeneficiary
     */
    public void setRegistationBeneficiary(String registationBeneficiary) {
        this.registationBeneficiary = registationBeneficiary;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     *
     * @param accountType
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    /**
     *
     * @return
     */
    public String getBankName() {
        return bankName;
    }

    /**
     *
     * @param bankName
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     *
     * @return
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     *
     * @param branchName
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     *
     * @return
     */
    public String getBillerCategory() {
        return billerCategory;
    }

    /**
     *
     * @param billerCategory
     */
    public void setBillerCategory(String billerCategory) {
        this.billerCategory = billerCategory;
    }

    /**
     *
     * @return
     */
    public String getBiller() {
        return biller;
    }

    /**
     *
     * @param biller
     */
    public void setBiller(String biller) {
        this.biller = biller;
    }

    /**
     *
     * @return
     */
    public long getFullCount() {
        return fullCount;
    }

    /**
     *
     * @param fullCount
     */
    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    /**
     *
     * @return
     */
    public String getCustomerCategory() {
        return customerCategory;
    }

    /**
     *
     * @param customerCategory
     */
    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    /**
     *
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public String getBeneficiaryType() {
        return beneficiaryType;
    }

    /**
     *
     * @param beneficiaryType
     */
    public void setBeneficiaryType(String beneficiaryType) {
        this.beneficiaryType = beneficiaryType;
    }

    /**
     *
     * @return
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     *
     * @param bankCode
     */
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
    
}
