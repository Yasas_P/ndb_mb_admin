/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

/**
 *
 * @author sivaganesan_t
 */
public class ProductMatrixBean {
    
    private String debitProductType;
    private String creditProductType;
    private String debitProductTypeDes;
    private String creditProductTypeDes;
    private String status;
    private String debitCurrency;
    private String maker;
    private String checker;
    private String createdtime;
    private String lastupdatedtime;
    private long fullCount;
    

    public String getDebitProductType() {
        return debitProductType;
    }

    public void setDebitProductType(String debitProductType) {
        this.debitProductType = debitProductType;
    }

    public String getCreditProductType() {
        return creditProductType;
    }

    public void setCreditProductType(String creditProductType) {
        this.creditProductType = creditProductType;
    }

    public String getDebitProductTypeDes() {
        return debitProductTypeDes;
    }

    public void setDebitProductTypeDes(String debitProductTypeDes) {
        this.debitProductTypeDes = debitProductTypeDes;
    }

    public String getCreditProductTypeDes() {
        return creditProductTypeDes;
    }

    public void setCreditProductTypeDes(String creditProductTypeDes) {
        this.creditProductTypeDes = creditProductTypeDes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDebitCurrency() {
        return debitCurrency;
    }

    public void setDebitCurrency(String debitCurrency) {
        this.debitCurrency = debitCurrency;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }
    
    
}
