/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.usermanagement;

/**
 *
 * @author dilanka_w
 */
public class PendingActionBean {

    private String id;
    private String page;
    private String task;
    private String userrole;
    private String fields;
    private String pkey;
    private String pkeydes;
    private String pkey1;
    private String fieldid;
    private String createduser;
    private String confirmeduser;
    private String createdtime;
    private String status;
    private String statusdes;
    private String lastupdatedtime;
    private long fullCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getUserrole() {
        return userrole;
    }

    public void setUserrole(String userrole) {
        this.userrole = userrole;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public String getConfirmeduser() {
        return confirmeduser;
    }

    public void setConfirmeduser(String confirmeduser) {
        this.confirmeduser = confirmeduser;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    public String getPkey() {
        return pkey;
    }

    public void setPkey(String pkey) {
        this.pkey = pkey;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public String getPkey1() {
        return pkey1;
    }

    public void setPkey1(String pkey1) {
        this.pkey1 = pkey1;
    }

    public String getFieldid() {
        return fieldid;
    }

    public void setFieldid(String fieldid) {
        this.fieldid = fieldid;
    }

    public String getPkeydes() {
        return pkeydes;
    }

    public void setPkeydes(String pkeydes) {
        this.pkeydes = pkeydes;
    }

    public String getStatusdes() {
        return statusdes;
    }

    public void setStatusdes(String statusdes) {
        this.statusdes = statusdes;
    }

}
