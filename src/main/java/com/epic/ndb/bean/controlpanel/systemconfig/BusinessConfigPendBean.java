
package com.epic.ndb.bean.controlpanel.systemconfig;

/**
 *
 * @author sivaganesan_t
 */
public class BusinessConfigPendBean {
    
    private String id;
    private String paramcode;
    private String paramval;
    private String operation;
    private String createduser;
    private String status;
    private String fields;
    private long fullCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParamcode() {
        return paramcode;
    }

    public void setParamcode(String paramcode) {
        this.paramcode = paramcode;
    }

    public String getParamval() {
        return paramval;
    }

    public void setParamval(String paramval) {
        this.paramval = paramval;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }
    
}
