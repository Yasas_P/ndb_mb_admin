/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.ChannelType;
import com.epic.ndb.util.mapping.Status;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class MBankLocatorInputBean {
    
    private String locatorid;
    private String locatorcode;
    private String channelType;
    private String channelSubType;
    private String countryCode;
    private String country;
    private String masterRegion;
    private String region;
    private String subRegion;
    private String name;
    private String address;
    private String postalCode;
    private String contacts;
    private String openingHsMonFri;
    private String openingHsSat;
    private String openingHsSunHol;
    private String geoCode;
    private String locationTag;
    private String language;
    private String status;
    private String maker;
    private String checker;
    private String latitude;
    private String longitude;
    private String fax;
    private String atmOnLocation;
    private String crmOnLocation;
    private String pawningOnLocation;
    private String safeDepositLockers;
    private String leasingDesk365;
    private String prvCentre;
    private String branchlessBanking;
    private String holidayBanking;
    
    private String message;
    private String errormessage;
    private String reporttype;
    
    private String remark;
    private String defaultStatus;
    
    private List<Status> statusList;
    private List<ChannelType> channelTypeList;
    
    private String newvalue;
    private String oldvalue;
    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vdual;
    private boolean vgenerate;
    private boolean vgenerateview;
    private transient boolean vupload;
    /*-------for access control-----------*/
 /*------------------------list data table  ------------------------------*/
    private List<MBankLocatorBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    /*------------------------list data table  ------------------------------*/
    
    private String locatoridSearch;
    private String locatorcodeSearch;
    private String channelTypeSearch;
    private String channelSubTypeSearch;
    private String countryCodeSearch;
    private String countrySearch;
    private String masterRegionSearch;
    private String regionSearch;
    private String subRegionSearch;
    private String nameSearch;
    private String addressSearch;
    private String postalCodeSearch;
    private String contactsSearch;
    private String openingHsMonFriSearch;
    private String openingHsSatSearch;
    private String openingHsSunHolSearch;
    private String geoCodeSearch;
    private String locationTagSearch;
    private String languageSearch;
    private String statusSearch;
    private String latitudeSearch;
    private String longitudeSearch;
    private boolean search;
    
    private String currentUser;
    private List<MBankLocatorPendBean> gridModelPend;
    
    
    private String hiddenId;
    
    private String conXLFileName;
    private File conXL;
    
    private InputStream fileInputStream = null;
    private long fileLength ;
    
    private String id;

    public String getLocatorid() {
        return locatorid;
    }

    public void setLocatorid(String locatorid) {
        this.locatorid = locatorid;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getChannelSubType() {
        return channelSubType;
    }

    public void setChannelSubType(String channelSubType) {
        this.channelSubType = channelSubType;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMasterRegion() {
        return masterRegion;
    }

    public void setMasterRegion(String masterRegion) {
        this.masterRegion = masterRegion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSubRegion() {
        return subRegion;
    }

    public void setSubRegion(String subRegion) {
        this.subRegion = subRegion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getOpeningHsMonFri() {
        return openingHsMonFri;
    }

    public void setOpeningHsMonFri(String openingHsMonFri) {
        this.openingHsMonFri = openingHsMonFri;
    }

    public String getOpeningHsSat() {
        return openingHsSat;
    }

    public void setOpeningHsSat(String openingHsSat) {
        this.openingHsSat = openingHsSat;
    }

    public String getOpeningHsSunHol() {
        return openingHsSunHol;
    }

    public void setOpeningHsSunHol(String openingHsSunHol) {
        this.openingHsSunHol = openingHsSunHol;
    }

    public String getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(String geoCode) {
        this.geoCode = geoCode;
    }

    public String getLocationTag() {
        return locationTag;
    }

    public void setLocationTag(String locationTag) {
        this.locationTag = locationTag;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public List<MBankLocatorBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<MBankLocatorBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getLocatoridSearch() {
        return locatoridSearch;
    }

    public void setLocatoridSearch(String locatoridSearch) {
        this.locatoridSearch = locatoridSearch;
    }

    public String getChannelTypeSearch() {
        return channelTypeSearch;
    }

    public void setChannelTypeSearch(String channelTypeSearch) {
        this.channelTypeSearch = channelTypeSearch;
    }

    public String getChannelSubTypeSearch() {
        return channelSubTypeSearch;
    }

    public void setChannelSubTypeSearch(String channelSubTypeSearch) {
        this.channelSubTypeSearch = channelSubTypeSearch;
    }

    public String getCountryCodeSearch() {
        return countryCodeSearch;
    }

    public void setCountryCodeSearch(String countryCodeSearch) {
        this.countryCodeSearch = countryCodeSearch;
    }

    public String getCountrySearch() {
        return countrySearch;
    }

    public void setCountrySearch(String countrySearch) {
        this.countrySearch = countrySearch;
    }

    public String getMasterRegionSearch() {
        return masterRegionSearch;
    }

    public void setMasterRegionSearch(String masterRegionSearch) {
        this.masterRegionSearch = masterRegionSearch;
    }

    public String getRegionSearch() {
        return regionSearch;
    }

    public void setRegionSearch(String regionSearch) {
        this.regionSearch = regionSearch;
    }

    public String getSubRegionSearch() {
        return subRegionSearch;
    }

    public void setSubRegionSearch(String subRegionSearch) {
        this.subRegionSearch = subRegionSearch;
    }

    public String getNameSearch() {
        return nameSearch;
    }

    public void setNameSearch(String nameSearch) {
        this.nameSearch = nameSearch;
    }

    public String getAddressSearch() {
        return addressSearch;
    }

    public void setAddressSearch(String addressSearch) {
        this.addressSearch = addressSearch;
    }

    public String getPostalCodeSearch() {
        return postalCodeSearch;
    }

    public void setPostalCodeSearch(String postalCodeSearch) {
        this.postalCodeSearch = postalCodeSearch;
    }

    public String getContactsSearch() {
        return contactsSearch;
    }

    public void setContactsSearch(String contactsSearch) {
        this.contactsSearch = contactsSearch;
    }

    public String getOpeningHsMonFriSearch() {
        return openingHsMonFriSearch;
    }

    public void setOpeningHsMonFriSearch(String openingHsMonFriSearch) {
        this.openingHsMonFriSearch = openingHsMonFriSearch;
    }

    public String getOpeningHsSatSearch() {
        return openingHsSatSearch;
    }

    public void setOpeningHsSatSearch(String openingHsSatSearch) {
        this.openingHsSatSearch = openingHsSatSearch;
    }

    public String getOpeningHsSunHolSearch() {
        return openingHsSunHolSearch;
    }

    public void setOpeningHsSunHolSearch(String openingHsSunHolSearch) {
        this.openingHsSunHolSearch = openingHsSunHolSearch;
    }

    public String getGeoCodeSearch() {
        return geoCodeSearch;
    }

    public void setGeoCodeSearch(String geoCodeSearch) {
        this.geoCodeSearch = geoCodeSearch;
    }

    public String getLocationTagSearch() {
        return locationTagSearch;
    }

    public void setLocationTagSearch(String locationTagSearch) {
        this.locationTagSearch = locationTagSearch;
    }

    public String getLanguageSearch() {
        return languageSearch;
    }

    public void setLanguageSearch(String languageSearch) {
        this.languageSearch = languageSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<MBankLocatorPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<MBankLocatorPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitudeSearch() {
        return latitudeSearch;
    }

    public void setLatitudeSearch(String latitudeSearch) {
        this.latitudeSearch = latitudeSearch;
    }

    public String getLongitudeSearch() {
        return longitudeSearch;
    }

    public void setLongitudeSearch(String longitudeSearch) {
        this.longitudeSearch = longitudeSearch;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAtmOnLocation() {
        return atmOnLocation;
    }

    public void setAtmOnLocation(String atmOnLocation) {
        this.atmOnLocation = atmOnLocation;
    }

    public String getCrmOnLocation() {
        return crmOnLocation;
    }

    public void setCrmOnLocation(String crmOnLocation) {
        this.crmOnLocation = crmOnLocation;
    }

    public String getPawningOnLocation() {
        return pawningOnLocation;
    }

    public void setPawningOnLocation(String pawningOnLocation) {
        this.pawningOnLocation = pawningOnLocation;
    }

    public String getSafeDepositLockers() {
        return safeDepositLockers;
    }

    public void setSafeDepositLockers(String safeDepositLockers) {
        this.safeDepositLockers = safeDepositLockers;
    }

    public String getLeasingDesk365() {
        return leasingDesk365;
    }

    public void setLeasingDesk365(String leasingDesk365) {
        this.leasingDesk365 = leasingDesk365;
    }

    public String getPrvCentre() {
        return prvCentre;
    }

    public void setPrvCentre(String prvCentre) {
        this.prvCentre = prvCentre;
    }

    public String getBranchlessBanking() {
        return branchlessBanking;
    }

    public void setBranchlessBanking(String branchlessBanking) {
        this.branchlessBanking = branchlessBanking;
    }

    public String getHolidayBanking() {
        return holidayBanking;
    }

    public void setHolidayBanking(String holidayBanking) {
        this.holidayBanking = holidayBanking;
    }

    public List<ChannelType> getChannelTypeList() {
        return channelTypeList;
    }

    public void setChannelTypeList(List<ChannelType> channelTypeList) {
        this.channelTypeList = channelTypeList;
    }

    public String getConXLFileName() {
        return conXLFileName;
    }

    public void setConXLFileName(String conXLFileName) {
        this.conXLFileName = conXLFileName;
    }

    public File getConXL() {
        return conXL;
    }

    public void setConXL(File conXL) {
        this.conXL = conXL;
    }

    public String getHiddenId() {
        return hiddenId;
    }

    public void setHiddenId(String hiddenId) {
        this.hiddenId = hiddenId;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public String getLocatorcode() {
        return locatorcode;
    }

    public void setLocatorcode(String locatorcode) {
        this.locatorcode = locatorcode;
    }

    public String getLocatorcodeSearch() {
        return locatorcodeSearch;
    }

    public void setLocatorcodeSearch(String locatorcodeSearch) {
        this.locatorcodeSearch = locatorcodeSearch;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public boolean isVupload() {
        return vupload;
    }

    public void setVupload(boolean vupload) {
        this.vupload = vupload;
    }
    
}
