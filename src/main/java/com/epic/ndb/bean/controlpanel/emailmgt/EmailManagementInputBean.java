/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.emailmgt;

import com.epic.ndb.bean.analytics.ChannelTypeBean;
import com.epic.ndb.util.mapping.InboxAttachment;
import com.epic.ndb.util.mapping.InboxAttachmentTemp;
import com.epic.ndb.util.mapping.Status;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author prathibha_w
 */
public class EmailManagementInputBean {

    private String id;
    private String messageEmail;
    private String messageEmailReply;
    private String refId;
    private String serviceId;
    private String subject;
    private String status;
    private String readStatus;
    private String createdDate;
    private String lastUpdatedDate;
    private String adminStatus;
    private String mailStatus;
    private String userId;
    private String userName;
    private String replierUsername;
    private String recipientName;
    private String message;
    private String errormessage;
    private String newvalue;
    private String oldvalue;
    private String userCif;
    private String unreadCount;

    private List<Status> statusList;
    private List<Status> adminStatusList;
    private List<Status> mailStatusList;
    private List<InboxAttachment> attachmentList;
    private List<InboxAttachmentTemp> attachmentTempList;
    private List<EmailManagementInputBean> inputBeanList;
    private List<ChannelTypeBean> channelTypeList ;
    private String defaultStatus;
    private EmailManagementBean dataBean;
    private String reporttype;

    private int hasAttachment;
    private int attachmentSize;
    private int isReply;
    private int countiterate;
    

    //================= file upload ===============
    private List<File> filesUpload = new ArrayList<File>();
    private List<String> filesUploadContentType = new ArrayList<String>();
    private List<String> filesUploadFileName = new ArrayList<String>();

    //=====================email attachmet
    private String fileName_db;
    private String fileFormat_db;
    private String emailId;
    private String attachmentId;
    private String fileId;

    private File attachmentFile;
    private String attachmentFileName;

    private byte[] attach;
    private String attachImg;

    private String cif_s;
    private String userName_s;
    private String status_s;
    private String adminStatus_s;
    private String mobileStatus_s;
    private String mailStatus_s;
    private String fdate_s;
    private String todate_s;
    private String channelType_s;

    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vupdatestatus;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vdownload;
    private boolean vreply;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vdual;
    private boolean vgenerate;
    private boolean vgenerateview;

    /*-------for access control-----------*/
 /*------------------------list data table  ------------------------------*/
    private List<EmailManagementBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    private boolean search;

    /*------------------------list data table  ------------------------------*/
    
    /*------------------------pending list data table  ------------------------------*/
    
    private List<EmailManagementPendBean> gridModelPend;
    private String remark;
    /*------------------------pending list data table  ------------------------------*/
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessageEmail() {
        return messageEmail;
    }

    public void setMessageEmail(String messageEmail) {
        this.messageEmail = messageEmail;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getAdminStatus() {
        return adminStatus;
    }

    public void setAdminStatus(String adminStatus) {
        this.adminStatus = adminStatus;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getReplierUsername() {
        return replierUsername;
    }

    public void setReplierUsername(String replierUsername) {
        this.replierUsername = replierUsername;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public String getUserCif() {
        return userCif;
    }

    public void setUserCif(String userCif) {
        this.userCif = userCif;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public List<Status> getAdminStatusList() {
        return adminStatusList;
    }

    public void setAdminStatusList(List<Status> adminStatusList) {
        this.adminStatusList = adminStatusList;
    }

    public List<InboxAttachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<InboxAttachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public List<EmailManagementInputBean> getInputBeanList() {
        return inputBeanList;
    }

    public void setInputBeanList(List<EmailManagementInputBean> inputBeanList) {
        this.inputBeanList = inputBeanList;
    }

    public int getIsReply() {
        return isReply;
    }

    public void setIsReply(int isReply) {
        this.isReply = isReply;
    }

    public int getCountiterate() {
        return countiterate;
    }

    public void setCountiterate(int countiterate) {
        this.countiterate = countiterate;
    }

    public List<File> getFilesUpload() {
        return filesUpload;
    }

    public void setFilesUpload(List<File> filesUpload) {
        this.filesUpload = filesUpload;
    }

    public List<String> getFilesUploadContentType() {
        return filesUploadContentType;
    }

    public void setFilesUploadContentType(List<String> filesUploadContentType) {
        this.filesUploadContentType = filesUploadContentType;
    }

    public List<String> getFilesUploadFileName() {
        return filesUploadFileName;
    }

    public void setFilesUploadFileName(List<String> filesUploadFileName) {
        this.filesUploadFileName = filesUploadFileName;
    }

    
    public String getDefaultStatus() {
        return defaultStatus;
    }

    public int getHasAttachment() {
        return hasAttachment;
    }

    public void setHasAttachment(int hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public int getAttachmentSize() {
        return attachmentSize;
    }

    public void setAttachmentSize(int attachmentSize) {
        this.attachmentSize = attachmentSize;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public String getFileName_db() {
        return fileName_db;
    }

    public void setFileName_db(String fileName_db) {
        this.fileName_db = fileName_db;
    }

    public String getFileFormat_db() {
        return fileFormat_db;
    }

    public void setFileFormat_db(String fileFormat_db) {
        this.fileFormat_db = fileFormat_db;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public File getAttachmentFile() {
        return attachmentFile;
    }

    public void setAttachmentFile(File attachmentFile) {
        this.attachmentFile = attachmentFile;
    }

    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }

    public byte[] getAttach() {
        return attach;
    }

    public void setAttach(byte[] attach) {
        this.setAttach(attach);
    }

    public String getAttachImg() {
        return attachImg;
    }

    public void setAttachImg(String attachImg) {
        this.attachImg = attachImg;
    }

    public String getCif_s() {
        return cif_s;
    }

    public void setCif_s(String cif_s) {
        this.cif_s = cif_s;
    }

    public String getUserName_s() {
        return userName_s;
    }

    public void setUserName_s(String userName_s) {
        this.userName_s = userName_s;
    }

    public String getStatus_s() {
        return status_s;
    }

    public void setStatus_s(String status_s) {
        this.status_s = status_s;
    }

    public String getAdminStatus_s() {
        return adminStatus_s;
    }

    public void setAdminStatus_s(String adminStatus_s) {
        this.adminStatus_s = adminStatus_s;
    }

    public String getFdate_s() {
        return fdate_s;
    }

    public void setFdate_s(String fdate_s) {
        this.fdate_s = fdate_s;
    }

    public String getTodate_s() {
        return todate_s;
    }

    public void setTodate_s(String todate_s) {
        this.todate_s = todate_s;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVdownload() {
        return vdownload;
    }

    public void setVdownload(boolean vdownload) {
        this.vdownload = vdownload;
    }

    public List<EmailManagementBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<EmailManagementBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getMessageEmailReply() {
        return messageEmailReply;
    }

    public void setMessageEmailReply(String messageEmailReply) {
        this.messageEmailReply = messageEmailReply;
    }

    public String getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(String unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getMobileStatus_s() {
        return mobileStatus_s;
    }

    public void setMobileStatus_s(String mobileStatus_s) {
        this.mobileStatus_s = mobileStatus_s;
    }

    public boolean isVreply() {
        return vreply;
    }

    public void setVreply(boolean vreply) {
        this.vreply = vreply;
    }

    public List<EmailManagementPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<EmailManagementPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public List<InboxAttachmentTemp> getAttachmentTempList() {
        return attachmentTempList;
    }

    public void setAttachmentTempList(List<InboxAttachmentTemp> attachmentTempList) {
        this.attachmentTempList = attachmentTempList;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public EmailManagementBean getDataBean() {
        return dataBean;
    }

    public void setDataBean(EmailManagementBean dataBean) {
        this.dataBean = dataBean;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public List<Status> getMailStatusList() {
        return mailStatusList;
    }

    public void setMailStatusList(List<Status> mailStatusList) {
        this.mailStatusList = mailStatusList;
    }

    public String getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(String mailStatus) {
        this.mailStatus = mailStatus;
    }

    public String getMailStatus_s() {
        return mailStatus_s;
    }

    public void setMailStatus_s(String mailStatus_s) {
        this.mailStatus_s = mailStatus_s;
    }

    public boolean isVupdatestatus() {
        return vupdatestatus;
    }

    public void setVupdatestatus(boolean vupdatestatus) {
        this.vupdatestatus = vupdatestatus;
    }

    public List<ChannelTypeBean> getChannelTypeList() {
        return channelTypeList;
    }

    public void setChannelTypeList(List<ChannelTypeBean> channelTypeList) {
        this.channelTypeList = channelTypeList;
    }

    public String getChannelType_s() {
        return channelType_s;
    }

    public void setChannelType_s(String channelType_s) {
        this.channelType_s = channelType_s;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }
}
