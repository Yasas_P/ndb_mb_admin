/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

/**
 *
 * @author sivaganesan_t
 */
public class TransactionLimitBean {
    private String segmentType;
    private String transferType;
    private String status;
    private String segmentTypeDes;
    private String transferTypeDes;
    private String defaultLimit;
    private String dailyLimitMin;
    private String dailyLimitMax;
    private String tranLimitMin;
    private String tranLimitMax;
    private String maker;
    private String checker;
    private String createdtime;
    private String lastupdatedtime;
    private long fullCount;

    public String getSegmentType() {
        return segmentType;
    }

    public void setSegmentType(String segmentType) {
        this.segmentType = segmentType;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSegmentTypeDes() {
        return segmentTypeDes;
    }

    public void setSegmentTypeDes(String segmentTypeDes) {
        this.segmentTypeDes = segmentTypeDes;
    }

    public String getTransferTypeDes() {
        return transferTypeDes;
    }

    public void setTransferTypeDes(String transferTypeDes) {
        this.transferTypeDes = transferTypeDes;
    }

    public String getDefaultLimit() {
        return defaultLimit;
    }

    public void setDefaultLimit(String defaultLimit) {
        this.defaultLimit = defaultLimit;
    }

    public String getDailyLimitMin() {
        return dailyLimitMin;
    }

    public void setDailyLimitMin(String dailyLimitMin) {
        this.dailyLimitMin = dailyLimitMin;
    }

    public String getDailyLimitMax() {
        return dailyLimitMax;
    }

    public void setDailyLimitMax(String dailyLimitMax) {
        this.dailyLimitMax = dailyLimitMax;
    }

    public String getTranLimitMin() {
        return tranLimitMin;
    }

    public void setTranLimitMin(String tranLimitMin) {
        this.tranLimitMin = tranLimitMin;
    }

    public String getTranLimitMax() {
        return tranLimitMax;
    }

    public void setTranLimitMax(String tranLimitMax) {
        this.tranLimitMax = tranLimitMax;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }
    
}
