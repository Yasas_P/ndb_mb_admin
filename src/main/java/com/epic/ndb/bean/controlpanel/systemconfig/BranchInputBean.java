
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.Status;
import java.util.List;

/**
 *
 * @author jayathissa_d
 */
public class BranchInputBean {
    
    private String branchCode;
    private String branchName;
    private String status;
    private String managername;
    private String mobile;
    private String message;
    private String defaultStatus;
    private List<Status> statusList;
    private String newvalue;
    private String oldvalue;
    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    /*-------for access control-----------*/
    /*------------------------list data table  ------------------------------*/
    private List<BranchBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    /*------------------------list data table  ------------------------------*/

    private String branchCodeSearch;
    private String branchNameSearch;
    private String statusSearch;
    private String managernameSearch;
    private String mobileSearch;
    private String SearchAudit;
    private boolean search;
    
    private String currentUser;
    private List<BranchPendBean> gridModelPend;
          
    private String id;

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public List<BranchBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<BranchBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getBranchCodeSearch() {
        return branchCodeSearch;
    }

    public void setBranchCodeSearch(String branchCodeSearch) {
        this.branchCodeSearch = branchCodeSearch;
    }

    public String getBranchNameSearch() {
        return branchNameSearch;
    }

    public void setBranchNameSearch(String branchNameSearch) {
        this.branchNameSearch = branchNameSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getSearchAudit() {
        return SearchAudit;
    }

    public void setSearchAudit(String SearchAudit) {
        this.SearchAudit = SearchAudit;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<BranchPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<BranchPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getManagername() {
        return managername;
    }

    public void setManagername(String managername) {
        this.managername = managername;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getManagernameSearch() {
        return managernameSearch;
    }

    public void setManagernameSearch(String managernameSearch) {
        this.managernameSearch = managernameSearch;
    }

    public String getMobileSearch() {
        return mobileSearch;
    }

    public void setMobileSearch(String mobileSearch) {
        this.mobileSearch = mobileSearch;
    }
    
    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }
	
	
    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }
}
