/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

/**
 *
 * @author prathibha_w
 */
public class BillServiceProviderBean {

    private String providerId;
    private String providerName;
    private String description;
    private String categoryCode;
    private String imageUrl;
    private String displayName;
    private String collectionAccount;
    private String bankCode;
    private String isHavingCustomField;
    private boolean havingCustomField;
    private String payType;
    private String billreTypeCode;
    private String status;
    private String currency;
    private String feeCharge;
    private String productType;
    private String placeHolder;
    private String mobPrefix;
    private String billerValCode;
    private String validationStatus;
    
    private String fieldId;
    private String fieldIdType;
    private String fieldVal;
    private String fieldName;
    private String fieldValidation;
    private String fieldLen;
    
    private String lastupdatedtime;
    private String createdtime;
    private String maker;
    private String checker;
    private long fullCount;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getCollectionAccount() {
        return collectionAccount;
    }

    public void setCollectionAccount(String collectionAccount) {
        this.collectionAccount = collectionAccount;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getIsHavingCustomField() {
        return isHavingCustomField;
    }

    public void setIsHavingCustomField(String isHavingCustomField) {
        this.isHavingCustomField = isHavingCustomField;
    }

    public boolean isHavingCustomField() {
        return havingCustomField;
    }

    public void setHavingCustomField(boolean havingCustomField) {
        this.havingCustomField = havingCustomField;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getBillreTypeCode() {
        return billreTypeCode;
    }

    public void setBillreTypeCode(String billreTypeCode) {
        this.billreTypeCode = billreTypeCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldIdType() {
        return fieldIdType;
    }

    public void setFieldIdType(String fieldIdType) {
        this.fieldIdType = fieldIdType;
    }

    public String getFieldVal() {
        return fieldVal;
    }

    public void setFieldVal(String fieldVal) {
        this.fieldVal = fieldVal;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValidation() {
        return fieldValidation;
    }

    public void setFieldValidation(String fieldValidation) {
        this.fieldValidation = fieldValidation;
    }

    public String getFieldLen() {
        return fieldLen;
    }

    public void setFieldLen(String fieldLen) {
        this.fieldLen = fieldLen;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFeeCharge() {
        return feeCharge;
    }

    public void setFeeCharge(String feeCharge) {
        this.feeCharge = feeCharge;
    }
    
    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public String getMobPrefix() {
        return mobPrefix;
    }

    public void setMobPrefix(String mobPrefix) {
        this.mobPrefix = mobPrefix;
    }

    public String getBillerValCode() {
        return billerValCode;
    }

    public void setBillerValCode(String billerValCode) {
        this.billerValCode = billerValCode;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }
}
