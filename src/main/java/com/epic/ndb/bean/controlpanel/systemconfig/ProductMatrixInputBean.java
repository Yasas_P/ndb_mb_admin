/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.ProductCurrency;
import com.epic.ndb.util.mapping.ProductType;
import com.epic.ndb.util.mapping.Status;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class ProductMatrixInputBean {
    
    private String debitProductType;
    private String debitProductTypeHidden;
    private String creditProductType;
    private String creditProductTypeHidden;
    private String status;
    private String debitCurrency;
    private String creditCurrencies;
    
    private String message;
    private String errormessage;
    private String remark;
    private String defaultStatus;
    private List<Status> statusList;
    private List<ProductCurrency> productCurrencyList;
    private List<ProductType> productTypeList;
    private List<CommonKeyVal> newCreditCurrencyList = new ArrayList<CommonKeyVal>();
    private List<CommonKeyVal> currentCreditCurrencyList= new ArrayList<CommonKeyVal>();
    private List<CommonKeyVal> newDebitCurrencyList = new ArrayList<CommonKeyVal>();
    private List<CommonKeyVal> currentDebitCurrencyList= new ArrayList<CommonKeyVal>();

    private transient List<String> newCreditCurrencyBox;
    private transient List<String> currentCreditCurrencyBox;
    private transient List<String> newDebitCurrencyBox;
    private transient List<String> currentDebitCurrencyBox;
    private String newvalue;
    private String oldvalue;
    private String reporttype;
    
    
     /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vupload;
    private boolean vdual;
    private boolean vgenerate;
    /*-------for access control-----------*/
    /*------------------------list data table  ------------------------------*/
    private List<ProductMatrixBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    /*------------------------list data table  ------------------------------*/
    
    private String debitProductTypeSearch;
    private String creditProductTypeSearch;
    private String statusSearch;
    private String debitCurrencySearch;
    private boolean search;
    
    private String currentUser;
    private List<ProductMatrixPendBean> gridModelPend;
    private String id;
    private String hiddenId;
    
    private String conXLFileName;
    private File conXL;
    
    private InputStream fileInputStream = null;
    private long fileLength ;

    public String getDebitProductType() {
        return debitProductType;
    }

    public void setDebitProductType(String debitProductType) {
        this.debitProductType = debitProductType;
    }

    public String getCreditProductType() {
        return creditProductType;
    }

    public void setCreditProductType(String creditProductType) {
        this.creditProductType = creditProductType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public List<ProductCurrency> getProductCurrencyList() {
        return productCurrencyList;
    }

    public void setProductCurrencyList(List<ProductCurrency> productCurrencyList) {
        this.productCurrencyList = productCurrencyList;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public boolean isVupload() {
        return vupload;
    }

    public void setVupload(boolean vupload) {
        this.vupload = vupload;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public List<ProductMatrixBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<ProductMatrixBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getDebitProductTypeSearch() {
        return debitProductTypeSearch;
    }

    public void setDebitProductTypeSearch(String debitProductTypeSearch) {
        this.debitProductTypeSearch = debitProductTypeSearch;
    }

    public String getCreditProductTypeSearch() {
        return creditProductTypeSearch;
    }

    public void setCreditProductTypeSearch(String creditProductTypeSearch) {
        this.creditProductTypeSearch = creditProductTypeSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<ProductMatrixPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<ProductMatrixPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDebitCurrency() {
        return debitCurrency;
    }

    public void setDebitCurrency(String debitCurrency) {
        this.debitCurrency = debitCurrency;
    }

    public String getDebitCurrencySearch() {
        return debitCurrencySearch;
    }

    public void setDebitCurrencySearch(String debitCurrencySearch) {
        this.debitCurrencySearch = debitCurrencySearch;
    }

    public List<ProductType> getProductTypeList() {
        return productTypeList;
    }

    public void setProductTypeList(List<ProductType> productTypeList) {
        this.productTypeList = productTypeList;
    }

    public List<CommonKeyVal> getNewCreditCurrencyList() {
        return newCreditCurrencyList;
    }

    public void setNewCreditCurrencyList(List<CommonKeyVal> newCreditCurrencyList) {
        this.newCreditCurrencyList = newCreditCurrencyList;
    }

    public List<CommonKeyVal> getCurrentCreditCurrencyList() {
        return currentCreditCurrencyList;
    }

    public void setCurrentCreditCurrencyList(List<CommonKeyVal> currentCreditCurrencyList) {
        this.currentCreditCurrencyList = currentCreditCurrencyList;
    }

    public List<String> getNewCreditCurrencyBox() {
        return newCreditCurrencyBox;
    }

    public void setNewCreditCurrencyBox(List<String> newCreditCurrencyBox) {
        this.newCreditCurrencyBox = newCreditCurrencyBox;
    }

    public List<String> getCurrentCreditCurrencyBox() {
        return currentCreditCurrencyBox;
    }

    public void setCurrentCreditCurrencyBox(List<String> currentCreditCurrencyBox) {
        this.currentCreditCurrencyBox = currentCreditCurrencyBox;
    }

    public String getDebitProductTypeHidden() {
        return debitProductTypeHidden;
    }

    public void setDebitProductTypeHidden(String debitProductTypeHidden) {
        this.debitProductTypeHidden = debitProductTypeHidden;
    }

    public String getCreditProductTypeHidden() {
        return creditProductTypeHidden;
    }

    public void setCreditProductTypeHidden(String creditProductTypeHidden) {
        this.creditProductTypeHidden = creditProductTypeHidden;
    }
    
    public String getConXLFileName() {
        return conXLFileName;
    }

    public void setConXLFileName(String conXLFileName) {
        this.conXLFileName = conXLFileName;
    }

    public File getConXL() {
        return conXL;
    }

    public void setConXL(File conXL) {
        this.conXL = conXL;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public String getHiddenId() {
        return hiddenId;
    }

    public void setHiddenId(String hiddenId) {
        this.hiddenId = hiddenId;
    }

    public String getCreditCurrencies() {
        return creditCurrencies;
    }

    public void setCreditCurrencies(String creditCurrencies) {
        this.creditCurrencies = creditCurrencies;
    }

    public List<CommonKeyVal> getNewDebitCurrencyList() {
        return newDebitCurrencyList;
    }

    public void setNewDebitCurrencyList(List<CommonKeyVal> newDebitCurrencyList) {
        this.newDebitCurrencyList = newDebitCurrencyList;
    }

    public List<CommonKeyVal> getCurrentDebitCurrencyList() {
        return currentDebitCurrencyList;
    }

    public void setCurrentDebitCurrencyList(List<CommonKeyVal> currentDebitCurrencyList) {
        this.currentDebitCurrencyList = currentDebitCurrencyList;
    }

    public List<String> getNewDebitCurrencyBox() {
        return newDebitCurrencyBox;
    }

    public void setNewDebitCurrencyBox(List<String> newDebitCurrencyBox) {
        this.newDebitCurrencyBox = newDebitCurrencyBox;
    }

    public List<String> getCurrentDebitCurrencyBox() {
        return currentDebitCurrencyBox;
    }

    public void setCurrentDebitCurrencyBox(List<String> currentDebitCurrencyBox) {
        this.currentDebitCurrencyBox = currentDebitCurrencyBox;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }
}
