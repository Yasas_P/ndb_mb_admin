/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.OtherBank;
import com.epic.ndb.util.mapping.Status;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class OtherBranchMgtInputBean {
    private String bank;
    private String bankhidden;
    private String branchcode;
    private String status;
    private String branchname;
    
    private String message;
    private String errormessage;
    private String reporttype;
    
    private String remark;
    private String defaultStatus;
    private List<Status> statusList;
    private List<OtherBank> bankList;
    private String newvalue;
    private String oldvalue;
    
    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vupload;
    private boolean vdual;
    private boolean vgenerate;
    private boolean vgenerateview;
    /*-------for access control-----------*/
    /*------------------------list data table  ------------------------------*/
    private List<OtherBranchMgtBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    /*------------------------list data table  ------------------------------*/

    private String bankSearch;
    private String branchcodeSearch;
    private String statusSearch;
    private String branchnameSearch;
    private String SearchAudit;
    private boolean search;
    
    private String currentUser;
    private List<OtherBranchMgtPendBean> gridModelPend;
    
    private String conXLFileName;
    private File conXL;
    
    private InputStream fileInputStream = null;
    private long fileLength ;
    
    private String hiddenId;
          
    private String id;

    public String getBank() {
        return bank;
    }

    public void setBankcode(String bank) {
        this.setBank(bank);
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public List<OtherBranchMgtBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<OtherBranchMgtBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getBankSearch() {
        return bankSearch;
    }

    public void setBankSearch(String bankSearch) {
        this.bankSearch = bankSearch;
    }

    public String getBranchcodeSearch() {
        return branchcodeSearch;
    }

    public void setBranchcodeSearch(String branchcodeSearch) {
        this.branchcodeSearch = branchcodeSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getBranchnameSearch() {
        return branchnameSearch;
    }

    public void setBranchnameSearch(String branchnameSearch) {
        this.branchnameSearch = branchnameSearch;
    }

    public String getSearchAudit() {
        return SearchAudit;
    }

    public void setSearchAudit(String SearchAudit) {
        this.SearchAudit = SearchAudit;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<OtherBranchMgtPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<OtherBranchMgtPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public List<OtherBank> getBankList() {
        return bankList;
    }

    public void setBankList(List<OtherBank> bankList) {
        this.bankList = bankList;
    }

    public String getBankhidden() {
        return bankhidden;
    }

    public void setBankhidden(String bankhidden) {
        this.bankhidden = bankhidden;
    }

    public boolean isVupload() {
        return vupload;
    }

    public void setVupload(boolean vupload) {
        this.vupload = vupload;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public String getConXLFileName() {
        return conXLFileName;
    }

    public void setConXLFileName(String conXLFileName) {
        this.conXLFileName = conXLFileName;
    }

    public File getConXL() {
        return conXL;
    }

    public void setConXL(File conXL) {
        this.conXL = conXL;
    }

    public String getHiddenId() {
        return hiddenId;
    }

    public void setHiddenId(String hiddenId) {
        this.hiddenId = hiddenId;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }
    
}
