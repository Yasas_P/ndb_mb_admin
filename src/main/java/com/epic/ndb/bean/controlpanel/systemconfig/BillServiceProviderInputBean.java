/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.BillBillerType;
import com.epic.ndb.util.mapping.BillFieldType;
import com.epic.ndb.util.mapping.BillerCategory;
import com.epic.ndb.util.mapping.ProductCurrency;
import com.epic.ndb.util.mapping.RegularExpression;
import com.epic.ndb.util.mapping.Status;
import java.io.File;
import java.util.List;

/**
 *
 * @author prathibha_w
 */
public class BillServiceProviderInputBean {
    
    private String providerId;
    private String providerName;
    private String description;
    private String categoryCode;
    private List<BillerCategory> categoryList;
    private String imageUrl;
    private String displayName;
    private String collectionAccount;
    private String bankCode;
    private String isHavingCustomField;
    private boolean havingCustomField;
    private String payType;
    private String billerTypeCode;
    private String billerType;
    private String currency;
    private String feeCharge;
    private String productType;
    private String placeHolder;
    private String mobPrefix;
    private List<BillBillerType> billerTypeList;
    
    private File mobileImg;
    private String mobileImgContentType;
    private String mobileImgFileName;
    
    private String hasImage;
    
    private String fieldId;
    private String fieldIdType;
    private String fieldIdTypeDes;
    private List<BillFieldType> fieldTypeList;
    private List<CommonKeyVal> feeChargeList;
    private List<CommonKeyVal> productTypeList;
    private List<ProductCurrency> currencyList;
    private List<RegularExpression> regexList;
    private String fieldVal;
    private String fieldName;
    private String fieldValidation;
    private String fieldLen;
    private String fieldLenMin;
    
    private String message;
    private String errormessage;
    private String remark;
    private String status;
    private String defaultStatus;
    private List<Status> statusList;
    private String newvalue;
    private String oldvalue;
    private String reporttype;
    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vupload;
    private boolean vdual;
    private boolean vgenerate;
    /*-------for access control-----------*/
    /*------------------------list data table  ------------------------------*/
    private List<BillServiceProviderBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    private boolean search;
    
    /**----------- search ------------------ **/
    private String providerIdSearch;
    private String providerNameSearch;
    private String descriptionSearch;
    private String categoryCodeSearch;
    private String displayNameSearch;
    private String collectionAccountSearch;
    private String billerTypeCodeSearch;
    private String statusSearch;
    private String billerValCode;
    private String validationStatus;
    
    private String currentUser;
    private List<BillServiceProviderPendBean> gridModelPend;
    /*------------------------list data table  ------------------------------*/

    private String id;

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public List<BillerCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<BillerCategory> categoryList) {
        this.categoryList = categoryList;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getCollectionAccount() {
        return collectionAccount;
    }

    public void setCollectionAccount(String collectionAccount) {
        this.collectionAccount = collectionAccount;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getIsHavingCustomField() {
        return isHavingCustomField;
    }

    public void setIsHavingCustomField(String isHavingCustomField) {
        this.isHavingCustomField = isHavingCustomField;
    }

    public boolean isHavingCustomField() {
        return havingCustomField;
    }

    public void setHavingCustomField(boolean havingCustomField) {
        this.havingCustomField = havingCustomField;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getBillerTypeCode() {
        return billerTypeCode;
    }

    public void setBillerTypeCode(String billerTypeCode) {
        this.billerTypeCode = billerTypeCode;
    }

    public List<BillBillerType> getBillerTypeList() {
        return billerTypeList;
    }

    public void setBillerTypeList(List<BillBillerType> billerTypeList) {
        this.billerTypeList = billerTypeList;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldIdType() {
        return fieldIdType;
    }

    public void setFieldIdType(String fieldIdType) {
        this.fieldIdType = fieldIdType;
    }

    public List<BillFieldType> getFieldTypeList() {
        return fieldTypeList;
    }

    public void setFieldTypeList(List<BillFieldType> fieldTypeList) {
        this.fieldTypeList = fieldTypeList;
    }

    public String getFieldVal() {
        return fieldVal;
    }

    public void setFieldVal(String fieldVal) {
        this.fieldVal = fieldVal;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldValidation() {
        return fieldValidation;
    }

    public void setFieldValidation(String fieldValidation) {
        this.fieldValidation = fieldValidation;
    }

    public String getFieldLen() {
        return fieldLen;
    }

    public void setFieldLen(String fieldLen) {
        this.fieldLen = fieldLen;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public boolean isVupload() {
        return vupload;
    }

    public void setVupload(boolean vupload) {
        this.vupload = vupload;
    }

    public List<BillServiceProviderBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<BillServiceProviderBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getProviderIdSearch() {
        return providerIdSearch;
    }

    public void setProviderIdSearch(String providerIdSearch) {
        this.providerIdSearch = providerIdSearch;
    }

    public String getProviderNameSearch() {
        return providerNameSearch;
    }

    public void setProviderNameSearch(String providerNameSearch) {
        this.providerNameSearch = providerNameSearch;
    }

    public String getDescriptionSearch() {
        return descriptionSearch;
    }

    public void setDescriptionSearch(String descriptionSearch) {
        this.descriptionSearch = descriptionSearch;
    }

    public String getCategoryCodeSearch() {
        return categoryCodeSearch;
    }

    public void setCategoryCodeSearch(String categoryCodeSearch) {
        this.categoryCodeSearch = categoryCodeSearch;
    }

    public String getDisplayNameSearch() {
        return displayNameSearch;
    }

    public void setDisplayNameSearch(String displayNameSearch) {
        this.displayNameSearch = displayNameSearch;
    }

    public String getCollectionAccountSearch() {
        return collectionAccountSearch;
    }

    public void setCollectionAccountSearch(String collectionAccountSearch) {
        this.collectionAccountSearch = collectionAccountSearch;
    }

    public String getBillerTypeCodeSearch() {
        return billerTypeCodeSearch;
    }

    public void setBillerTypeCodeSearch(String billerTypeCodeSearch) {
        this.billerTypeCodeSearch = billerTypeCodeSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<BillServiceProviderPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<BillServiceProviderPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public File getMobileImg() {
        return mobileImg;
    }

    public void setMobileImg(File mobileImg) {
        this.mobileImg = mobileImg;
    }

    public String getMobileImgContentType() {
        return mobileImgContentType;
    }

    public void setMobileImgContentType(String mobileImgContentType) {
        this.mobileImgContentType = mobileImgContentType;
    }

    public String getMobileImgFileName() {
        return mobileImgFileName;
    }

    public void setMobileImgFileName(String mobileImgFileName) {
        this.mobileImgFileName = mobileImgFileName;
    }

    public String getHasImage() {
        return hasImage;
    }

    public void setHasImage(String hasImage) {
        this.hasImage = hasImage;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public List<CommonKeyVal> getFeeChargeList() {
        return feeChargeList;
    }

    public void setFeeChargeList(List<CommonKeyVal> feeChargeList) {
        this.feeChargeList = feeChargeList;
    }

    public List<ProductCurrency> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<ProductCurrency> currencyList) {
        this.currencyList = currencyList;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getFeeCharge() {
        return feeCharge;
    }

    public void setFeeCharge(String feeCharge) {
        this.feeCharge = feeCharge;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public List<CommonKeyVal> getProductTypeList() {
        return productTypeList;
    }

    public void setProductTypeList(List<CommonKeyVal> productTypeList) {
        this.productTypeList = productTypeList;
    }

    public List<RegularExpression> getRegexList() {
        return regexList;
    }

    public void setRegexList(List<RegularExpression> regexList) {
        this.regexList = regexList;
    }

    public String getFieldLenMin() {
        return fieldLenMin;
    }

    public void setFieldLenMin(String fieldLenMin) {
        this.fieldLenMin = fieldLenMin;
    }

    public String getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        this.placeHolder = placeHolder;
    }

    public String getFieldIdTypeDes() {
        return fieldIdTypeDes;
    }

    public void setFieldIdTypeDes(String fieldIdTypeDes) {
        this.fieldIdTypeDes = fieldIdTypeDes;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public String getMobPrefix() {
        return mobPrefix;
    }

    public void setMobPrefix(String mobPrefix) {
        this.mobPrefix = mobPrefix;
    }

    public String getBillerType() {
        return billerType;
    }

    public void setBillerType(String billerType) {
        this.billerType = billerType;
    }

    public String getBillerValCode() {
        return billerValCode;
    }

    public void setBillerValCode(String billerValCode) {
        this.billerValCode = billerValCode;
    }

    public String getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(String validationStatus) {
        this.validationStatus = validationStatus;
    }
}
