/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

/**
 *
 * @author sivaganesan_t
 */
public class MBankLocatorBean {

    private String locatorid;
    private String locatorcode;
    private String channelType;
    private String channelSubType;
    private String countryCode;
    private String country;
    private String masterRegion;
    private String region;
    private String subRegion;
    private String name;
    private String address;
    private String postalCode;
    private String contacts;
    private String openingHsMonFri;
    private String openingHsSat;
    private String openingHsSunHol;
    private String geoCode;
    private String locationTag;
    private String language;
    private String status;
    private String maker;
    private String checker;
    private String lastupdatedtime;
    private String createdtime;
    private String latitude;
    private String longitude;
    private String fax;
    private String atmOnLocation;
    private String crmOnLocation;
    private String pawningOnLocation;
    private String safeDepositLockers;
    private String leasingDesk365;
    private String prvCentre;
    private String branchlessBanking;
    private String holidayBanking;
    private long fullCount;

    public String getLocatorid() {
        return locatorid;
    }

    public void setLocatorid(String locatorid) {
        this.locatorid = locatorid;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getChannelSubType() {
        return channelSubType;
    }

    public void setChannelSubType(String channelSubType) {
        this.channelSubType = channelSubType;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMasterRegion() {
        return masterRegion;
    }

    public void setMasterRegion(String masterRegion) {
        this.masterRegion = masterRegion;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getSubRegion() {
        return subRegion;
    }

    public void setSubRegion(String subRegion) {
        this.subRegion = subRegion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getOpeningHsMonFri() {
        return openingHsMonFri;
    }

    public void setOpeningHsMonFri(String openingHsMonFri) {
        this.openingHsMonFri = openingHsMonFri;
    }

    public String getOpeningHsSat() {
        return openingHsSat;
    }

    public void setOpeningHsSat(String openingHsSat) {
        this.openingHsSat = openingHsSat;
    }

    public String getOpeningHsSunHol() {
        return openingHsSunHol;
    }

    public void setOpeningHsSunHol(String openingHsSunHol) {
        this.openingHsSunHol = openingHsSunHol;
    }

    public String getGeoCode() {
        return geoCode;
    }

    public void setGeoCode(String geoCode) {
        this.geoCode = geoCode;
    }

    public String getLocationTag() {
        return locationTag;
    }

    public void setLocationTag(String locationTag) {
        this.locationTag = locationTag;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAtmOnLocation() {
        return atmOnLocation;
    }

    public void setAtmOnLocation(String atmOnLocation) {
        this.atmOnLocation = atmOnLocation;
    }

    public String getCrmOnLocation() {
        return crmOnLocation;
    }

    public void setCrmOnLocation(String crmOnLocation) {
        this.crmOnLocation = crmOnLocation;
    }

    public String getPawningOnLocation() {
        return pawningOnLocation;
    }

    public void setPawningOnLocation(String pawningOnLocation) {
        this.pawningOnLocation = pawningOnLocation;
    }

    public String getSafeDepositLockers() {
        return safeDepositLockers;
    }

    public void setSafeDepositLockers(String safeDepositLockers) {
        this.safeDepositLockers = safeDepositLockers;
    }

    public String getLeasingDesk365() {
        return leasingDesk365;
    }

    public void setLeasingDesk365(String leasingDesk365) {
        this.leasingDesk365 = leasingDesk365;
    }

    public String getPrvCentre() {
        return prvCentre;
    }

    public void setPrvCentre(String prvCentre) {
        this.prvCentre = prvCentre;
    }

    public String getBranchlessBanking() {
        return branchlessBanking;
    }

    public void setBranchlessBanking(String branchlessBanking) {
        this.branchlessBanking = branchlessBanking;
    }

    public String getHolidayBanking() {
        return holidayBanking;
    }

    public void setHolidayBanking(String holidayBanking) {
        this.holidayBanking = holidayBanking;
    }

    public String getLocatorcode() {
        return locatorcode;
    }

    public void setLocatorcode(String locatorcode) {
        this.locatorcode = locatorcode;
    }
    
}
