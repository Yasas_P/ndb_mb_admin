/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.emailmgt;

import com.epic.ndb.bean.customermanagement.KeyValueBean;
import com.epic.ndb.util.mapping.InboxServiceCategory;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class SendMailInputBean {
    
    private String id;
    private String serviceId;
    private String subject;
    private String toUser;
    private String messageEmail;
    private String message;
    private String errormessage;
    private String newvalue;
    private String oldvalue;
    
    private List<Status> statusList;
    private List<Status> processingStatusList;
    private List<InboxServiceCategory> serviceCategoryList;
    private List<SegmentType> segmentTypeList;
    private List<KeyValueBean> recipientList;
    private List<KeyValueBean> segmentList = new ArrayList<KeyValueBean>();
    private List<KeyValueBean> currentSegmentList = new ArrayList<KeyValueBean>();
    private List<KeyValueBean> cidList = new ArrayList<KeyValueBean>();
    private List<KeyValueBean> userList;
    private transient List<String> newCidBox;
    private transient List<String> newSegmentBox;
    private transient List<String> currentSegmentBox;
    private String defaultStatus;
    
    
    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vsend;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vdual;
    private boolean vgenerateview;
    private boolean vgenerate;
    /*-------for access control-----------*/
    
    /*------------------------list data table  ------------------------------*/
    private List<SendMailBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    private boolean search;

    /*------------------------list data table  ------------------------------*/
        
    /*------------------------pending list data table  ------------------------------*/
    
    private List<SendMailPendBean> gridModelPend;
    private String remark;
    /*------------------------pending list data table  ------------------------------*/

    private SendMailPendBean sendMsgTemp;
    private SendMailBean sendMsg;
    
    /*----------------------search-------------------------*/
    
    private String fdate_s;
    private String todate_s;
    private String toUser_s;
    private String cif_s;
    private String segment_s;
    
    /*----------------------Report-------------------------*/
    private String reporttype;
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessageEmail() {
        return messageEmail;
    }

    public void setMessageEmail(String messageEmail) {
        this.messageEmail = messageEmail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public List<Status> getProcessingStatusList() {
        return processingStatusList;
    }

    public void setProcessingStatusList(List<Status> processingStatusList) {
        this.processingStatusList = processingStatusList;
    }

    public List<InboxServiceCategory> getServiceCategoryList() {
        return serviceCategoryList;
    }

    public void setServiceCategoryList(List<InboxServiceCategory> serviceCategoryList) {
        this.serviceCategoryList = serviceCategoryList;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public List<SendMailBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<SendMailBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public List<SendMailPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<SendMailPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFdate_s() {
        return fdate_s;
    }

    public void setFdate_s(String fdate_s) {
        this.fdate_s = fdate_s;
    }

    public String getTodate_s() {
        return todate_s;
    }

    public void setTodate_s(String todate_s) {
        this.todate_s = todate_s;
    }

    public boolean isVsend() {
        return vsend;
    }

    public void setVsend(boolean vsend) {
        this.vsend = vsend;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public List<KeyValueBean> getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(List<KeyValueBean> recipientList) {
        this.recipientList = recipientList;
    }

    public List<KeyValueBean> getSegmentList() {
        return segmentList;
    }

    public void setSegmentList(List<KeyValueBean> segmentList) {
        this.segmentList = segmentList;
    }

    public List<KeyValueBean> getUserList() {
        return userList;
    }

    public void setUserList(List<KeyValueBean> userList) {
        this.userList = userList;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public List<String> getNewSegmentBox() {
        return newSegmentBox;
    }

    public void setNewSegmentBox(List<String> newSegmentBox) {
        this.newSegmentBox = newSegmentBox;
    }

    public List<String> getCurrentSegmentBox() {
        return currentSegmentBox;
    }

    public void setCurrentSegmentBox(List<String> currentSegmentBox) {
        this.currentSegmentBox = currentSegmentBox;
    }

    public List<KeyValueBean> getCurrentSegmentList() {
        return currentSegmentList;
    }

    public void setCurrentSegmentList(List<KeyValueBean> currentSegmentList) {
        this.currentSegmentList = currentSegmentList;
    }

    public List<KeyValueBean> getCidList() {
        return cidList;
    }

    public void setCidList(List<KeyValueBean> cidList) {
        this.cidList = cidList;
    }

    public List<String> getNewCidBox() {
        return newCidBox;
    }

    public void setNewCidBox(List<String> newCidBox) {
        this.newCidBox = newCidBox;
    }

    public SendMailPendBean getSendMsgTemp() {
        return sendMsgTemp;
    }

    public void setSendMsgTemp(SendMailPendBean sendMsgTemp) {
        this.sendMsgTemp = sendMsgTemp;
    }

    public SendMailBean getSendMsg() {
        return sendMsg;
    }

    public void setSendMsg(SendMailBean sendMsg) {
        this.sendMsg = sendMsg;
    }

    public String getToUser_s() {
        return toUser_s;
    }

    public void setToUser_s(String toUser_s) {
        this.toUser_s = toUser_s;
    }

    public String getCif_s() {
        return cif_s;
    }

    public void setCif_s(String cif_s) {
        this.cif_s = cif_s;
    }

    public String getSegment_s() {
        return segment_s;
    }

    public void setSegment_s(String segment_s) {
        this.segment_s = segment_s;
    }
    
    public List<SegmentType> getSegmentTypeList() {
        return segmentTypeList;
    }

    public void setSegmentTypeList(List<SegmentType> segmentTypeList) {
        this.segmentTypeList = segmentTypeList;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }
}
