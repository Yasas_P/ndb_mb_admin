/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.TransferType;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class TransactionLimitInputBean {
    private String segmentType;
    private String segmentTypeHidden;
    private String transferType;
    private String transferTypeHidden;
    private String status;
    private String defaultLimit;
    private String dailyLimitMin;
    private String dailyLimitMax;
    private String tranLimitMin;
    private String tranLimitMax;
    private String message;
    private String errormessage;
    private String remark;
    private String defaultStatus;
    private List<Status> statusList;
    private List<TransferType> transferTypeList;
    private List<SegmentType> segmentTypeList;
    private String newvalue;
    private String oldvalue;
    private String reporttype;
    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vupload;
    private boolean vdual;
    private boolean vgenerate;
    /*-------for access control-----------*/
    /*------------------------list data table  ------------------------------*/
    private List<TransactionLimitBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    /*------------------------list data table  ------------------------------*/
    
    private String segmentTypeSearch;
    private String transferTypeSearch;
    private String statusSearch;
    private String defaultLimitSearch;
    private String dailyLimitMinSearch;
    private String dailyLimitMaxSearch;
    private String tranLimitMinSearch;
    private String tranLimitMaxSearch;
    private boolean search;
    
    private String currentUser;
    private List<TransactionLimitPendBean> gridModelPend;
          
    //---------------------old Value--------------
    
    private String statusOld;
    private String defaultLimitOld;
    private String dailyLimitMinOld;
    private String dailyLimitMaxOld;
    private String tranLimitMinOld;
    private String tranLimitMaxOld;
    /*------------------------Task Code----------------------------------------*/
    private String taskCode;
    private String taskDescription;
    
    private String id;

    public String getSegmentType() {
        return segmentType;
    }

    public void setSegmentType(String segmentType) {
        this.segmentType = segmentType;
    }

    public String getSegmentTypeHidden() {
        return segmentTypeHidden;
    }

    public void setSegmentTypeHidden(String segmentTypeHidden) {
        this.segmentTypeHidden = segmentTypeHidden;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getTransferTypeHidden() {
        return transferTypeHidden;
    }

    public void setTransferTypeHidden(String transferTypeHidden) {
        this.transferTypeHidden = transferTypeHidden;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDefaultLimit() {
        return defaultLimit;
    }

    public void setDefaultLimit(String defaultLimit) {
        this.defaultLimit = defaultLimit;
    }

    public String getDailyLimitMin() {
        return dailyLimitMin;
    }

    public void setDailyLimitMin(String dailyLimitMin) {
        this.dailyLimitMin = dailyLimitMin;
    }

    public String getDailyLimitMax() {
        return dailyLimitMax;
    }

    public void setDailyLimitMax(String dailyLimitMax) {
        this.dailyLimitMax = dailyLimitMax;
    }

    public String getTranLimitMin() {
        return tranLimitMin;
    }

    public void setTranLimitMin(String tranLimitMin) {
        this.tranLimitMin = tranLimitMin;
    }

    public String getTranLimitMax() {
        return tranLimitMax;
    }

    public void setTranLimitMax(String tranLimitMax) {
        this.tranLimitMax = tranLimitMax;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public List<TransactionLimitBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<TransactionLimitBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getSegmentTypeSearch() {
        return segmentTypeSearch;
    }

    public void setSegmentTypeSearch(String segmentTypeSearch) {
        this.segmentTypeSearch = segmentTypeSearch;
    }

    public String getTransferTypeSearch() {
        return transferTypeSearch;
    }

    public void setTransferTypeSearch(String transferTypeSearch) {
        this.transferTypeSearch = transferTypeSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getDefaultLimitSearch() {
        return defaultLimitSearch;
    }

    public void setDefaultLimitSearch(String defaultLimitSearch) {
        this.defaultLimitSearch = defaultLimitSearch;
    }

    public String getDailyLimitMinSearch() {
        return dailyLimitMinSearch;
    }

    public void setDailyLimitMinSearch(String dailyLimitMinSearch) {
        this.dailyLimitMinSearch = dailyLimitMinSearch;
    }

    public String getDailyLimitMaxSearch() {
        return dailyLimitMaxSearch;
    }

    public void setDailyLimitMaxSearch(String dailyLimitMaxSearch) {
        this.dailyLimitMaxSearch = dailyLimitMaxSearch;
    }

    public String getTranLimitMinSearch() {
        return tranLimitMinSearch;
    }

    public void setTranLimitMinSearch(String tranLimitMinSearch) {
        this.tranLimitMinSearch = tranLimitMinSearch;
    }

    public String getTranLimitMaxSearch() {
        return tranLimitMaxSearch;
    }

    public void setTranLimitMaxSearch(String tranLimitMaxSearch) {
        this.tranLimitMaxSearch = tranLimitMaxSearch;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<TransactionLimitPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<TransactionLimitPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isVupload() {
        return vupload;
    }

    public void setVupload(boolean vupload) {
        this.vupload = vupload;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public List<TransferType> getTransferTypeList() {
        return transferTypeList;
    }

    public void setTransferTypeList(List<TransferType> transferTypeList) {
        this.transferTypeList = transferTypeList;
    }

    public List<SegmentType> getSegmentTypeList() {
        return segmentTypeList;
    }

    public void setSegmentTypeList(List<SegmentType> segmentTypeList) {
        this.segmentTypeList = segmentTypeList;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public String getStatusOld() {
        return statusOld;
    }

    public void setStatusOld(String statusOld) {
        this.statusOld = statusOld;
    }

    public String getDefaultLimitOld() {
        return defaultLimitOld;
    }

    public void setDefaultLimitOld(String defaultLimitOld) {
        this.defaultLimitOld = defaultLimitOld;
    }

    public String getDailyLimitMinOld() {
        return dailyLimitMinOld;
    }

    public void setDailyLimitMinOld(String dailyLimitMinOld) {
        this.dailyLimitMinOld = dailyLimitMinOld;
    }

    public String getDailyLimitMaxOld() {
        return dailyLimitMaxOld;
    }

    public void setDailyLimitMaxOld(String dailyLimitMaxOld) {
        this.dailyLimitMaxOld = dailyLimitMaxOld;
    }

    public String getTranLimitMinOld() {
        return tranLimitMinOld;
    }

    public void setTranLimitMinOld(String tranLimitMinOld) {
        this.tranLimitMinOld = tranLimitMinOld;
    }

    public String getTranLimitMaxOld() {
        return tranLimitMaxOld;
    }

    public void setTranLimitMaxOld(String tranLimitMaxOld) {
        this.tranLimitMaxOld = tranLimitMaxOld;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }
    
}
