/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.DelistedCategory;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class DelistedContractInputBean {
    
    private String cid;
    private String delistedCategory;
    private String delistedCategoryDes;
    private String contractNumber;
    private transient List<String> newContractNumberBox;
    private transient List<String> currentContractNumberBox;
    
    private String message;
    private String errormessage;
    private String remark;
    private List<DelistedCategory> delistedCategoryList;
    private List<ContractDetailBean> contractDatailList;
    private List<CommonKeyVal> newNumberList = new ArrayList<CommonKeyVal>();
    private List<CommonKeyVal> currentNumberList= new ArrayList<CommonKeyVal>();
    private String newvalue;
    private String oldvalue;
    
    /*-------for access control-----------*/
    private boolean vassign;
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vupload;
    private boolean vdual;
    /*-------for access control-----------*/
    /*------------------------list data table  ------------------------------*/
    private List<DelistedContractBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    /*------------------------list data table  ------------------------------*/
    /*-------for search-----------*/
    private String cidSearch;
    private String delistedCategorySearch;
    private String contractNumberSearch;
    private String SearchAudit;
    private boolean search;
     /*-------for search-----------*/
     /*-------for pend-----------*/
    private String currentUser;
    private List<DelistedContractPendBean> gridModelPend;
          
    private String id;
    private String hiddenId;
    /*-------for pend-----------*/
    
    /*----------file uploade--------------*/
    
    private String conXLFileName;
    private File conXL;
    
    private InputStream fileInputStream = null;
    private long fileLength ;
     /*----------end file uploade--------------*/

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getDelistedCategory() {
        return delistedCategory;
    }

    public void setDelistedCategory(String delistedCategory) {
        this.delistedCategory = delistedCategory;
    }

    public String getDelistedCategoryDes() {
        return delistedCategoryDes;
    }

    public void setDelistedCategoryDes(String delistedCategoryDes) {
        this.delistedCategoryDes = delistedCategoryDes;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<DelistedCategory> getDelistedCategoryList() {
        return delistedCategoryList;
    }

    public void setDelistedCategoryList(List<DelistedCategory> delistedCategoryList) {
        this.delistedCategoryList = delistedCategoryList;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public List<DelistedContractBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<DelistedContractBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getCidSearch() {
        return cidSearch;
    }

    public void setCidSearch(String cidSearch) {
        this.cidSearch = cidSearch;
    }

    public String getDelistedCategorySearch() {
        return delistedCategorySearch;
    }

    public void setDelistedCategorySearch(String delistedCategorySearch) {
        this.delistedCategorySearch = delistedCategorySearch;
    }

    public String getContractNumberSearch() {
        return contractNumberSearch;
    }

    public void setContractNumberSearch(String contractNumberSearch) {
        this.contractNumberSearch = contractNumberSearch;
    }

    public String getSearchAudit() {
        return SearchAudit;
    }

    public void setSearchAudit(String SearchAudit) {
        this.SearchAudit = SearchAudit;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<DelistedContractPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<DelistedContractPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<CommonKeyVal> getNewNumberList() {
        return newNumberList;
    }

    public void setNewNumberList(List<CommonKeyVal> newNumberList) {
        this.newNumberList = newNumberList;
    }

    public List<CommonKeyVal> getCurrentNumberList() {
        return currentNumberList;
    }

    public void setCurrentNumberList(List<CommonKeyVal> currentNumberList) {
        this.currentNumberList = currentNumberList;
    }

    public List<String> getNewContractNumberBox() {
        return newContractNumberBox;
    }

    public void setNewContractNumberBox(List<String> newContractNumberBox) {
        this.newContractNumberBox = newContractNumberBox;
    }

    public List<String> getCurrentContractNumberBox() {
        return currentContractNumberBox;
    }

    public void setCurrentContractNumberBox(List<String> currentContractNumberBox) {
        this.currentContractNumberBox = currentContractNumberBox;
    }

    public List<ContractDetailBean> getContractDatailList() {
        return contractDatailList;
    }

    public void setContractDatailList(List<ContractDetailBean> contractDatailList) {
        this.contractDatailList = contractDatailList;
    }

    public boolean isVassign() {
        return vassign;
    }

    public void setVassign(boolean vassign) {
        this.vassign = vassign;
    }

    public boolean isVupload() {
        return vupload;
    }

    public void setVupload(boolean vupload) {
        this.vupload = vupload;
    }

    public String getConXLFileName() {
        return conXLFileName;
    }

    public void setConXLFileName(String conXLFileName) {
        this.conXLFileName = conXLFileName;
    }

    public File getConXL() {
        return conXL;
    }

    public void setConXL(File conXL) {
        this.conXL = conXL;
    }

    public String getHiddenId() {
        return hiddenId;
    }

    public void setHiddenId(String hiddenId) {
        this.hiddenId = hiddenId;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }
    
}
