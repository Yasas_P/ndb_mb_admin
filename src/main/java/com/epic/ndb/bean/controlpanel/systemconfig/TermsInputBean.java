/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.Status;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class TermsInputBean {

    private String status;
    private String statusadd;
    private String statusAct;
    
    private String description;
    private String descriptiontxt;
    private String descriptionadd;
    private String descriptionaddtxt;
    
    private String versionno;
    private String versionnoadd;
    
    private String category;
    private String categoryadd;
    
    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupload;
    private boolean vdownload;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vsearch;
    private boolean vdelete;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vdual;
    
    /*------------------------list data table  ------------------------------*/
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    
    private boolean search;
    private String remark;
    
    private String defaultStatus;
    private List<Status> StatusList;
    private List<CategoryBean> categoryList;
    private List<TermsVersionBean> versionMap = new ArrayList<TermsVersionBean>();
    
    private String oldvalue;
    private String newvalue;
    private String message;
    private String errorMessage;
    
    
    private String currentUser;
    private List<TermsConditionPendBean> gridModelPend;

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public List<Status> getStatusList() {
        return StatusList;
    }

    public void setStatusList(List<Status> StatusList) {
        this.StatusList = StatusList;
    }

    public List<CategoryBean> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryBean> categoryList) {
        this.categoryList = categoryList;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupload() {
        return vupload;
    }

    public void setVupload(boolean vupload) {
        this.vupload = vupload;
    }

    public boolean isVdownload() {
        return vdownload;
    }

    public void setVdownload(boolean vdownload) {
        this.vdownload = vdownload;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }
    
     public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public List<TermsVersionBean> getVersionMap() {
        return versionMap;
    }

    public void setVersionMap(List<TermsVersionBean> versionMap) {
        this.versionMap = versionMap;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusAct() {
        return statusAct;
    }

    public void setStatusAct(String statusAct) {
        this.statusAct = statusAct;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVersionno() {
        return versionno;
    }

    public void setVersionno(String versionno) {
        this.versionno = versionno;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescriptiontxt() {
        return descriptiontxt;
    }

    public void setDescriptiontxt(String descriptiontxt) {
        this.descriptiontxt = descriptiontxt;
    }

    public String getDescriptionadd() {
        return descriptionadd;
    }

    public void setDescriptionadd(String descriptionadd) {
        this.descriptionadd = descriptionadd;
    }

    public String getDescriptionaddtxt() {
        return descriptionaddtxt;
    }

    public void setDescriptionaddtxt(String descriptionaddtxt) {
        this.descriptionaddtxt = descriptionaddtxt;
    }

    public String getVersionnoadd() {
        return versionnoadd;
    }

    public void setVersionnoadd(String versionnoadd) {
        this.versionnoadd = versionnoadd;
    }

    public String getCategoryadd() {
        return categoryadd;
    }

    public void setCategoryadd(String categoryadd) {
        this.categoryadd = categoryadd;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatusadd() {
        return statusadd;
    }

    public void setStatusadd(String statusadd) {
        this.statusadd = statusadd;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<TermsConditionPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<TermsConditionPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }
}
