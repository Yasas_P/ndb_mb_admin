/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

import com.epic.ndb.util.mapping.PromotionsCards;
import com.epic.ndb.util.mapping.PromotionsCategories;
import com.epic.ndb.util.mapping.Status;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class PromotionMgtInputBean {

    public boolean isVupdatedefimg() {
        return vupdatedefimg;
    }

    public void setVupdatedefimg(boolean vupdatedefimg) {
        this.vupdatedefimg = vupdatedefimg;
    }

    private String promoid;
    private String subregioncode;
    private String status;
    private String promotionsCategories;
    private String promotionsCards;
    private String subregionname;
    private String subreginaddress;
    private String freetext;
    private String info;
    private String tradinghours;
    private String subregionlocation;
    private String promoconditions;
    private String phoneno;
    private String merchantwebsite;
    private String image;
    private String regioncode;
    private String regionname;
    private String latitude;
    private String longitude;
    private String masterregoncode;
    private String masterregionname;
    private String cardtype;
    private String cardimage;
    private String maker;
    private String checker;
    private String ispushnotification;
    private String ispushnotificationhidden;
    private String radius;
    
    private InputStream fileInputStream = null;
    private long fileLength ;
    
    private File mobileImg;
    private String mobileImgContentType;
    private String mobileImgFileName;
    
    private String firebaseImgMobUrl;
    
    private String hasImage;
    
    private String message;
    private String errormessage;
    private String reporttype;
    
    private String remark;
    private String defaultStatus;
    private String hiddenId;
    
    private String conXLFileName;
    private File conXL;
    
    private List<Status> statusList;
    private List<PromotionsCategories> promotionsCategoriesList;
    private List<PromotionsCards> promotionsCardsList;
    
    private String newvalue;
    private String oldvalue;
    
    private PromotionMgtBean promotionOldVal;
    /*-------for access control-----------*/
    private boolean vadd;
    private boolean vupdatebutt;
    private boolean vupdatelink;
    private boolean vupdatedefimg;
    private boolean vdelete;
    private boolean vsearch;
    private boolean vconfirm;
    private boolean vreject;
    private boolean vupload;
    private boolean vdual;
    private boolean vgenerate;
    private boolean vgenerateview;
    /*-------for access control-----------*/
 /*------------------------list data table  ------------------------------*/
    private List<PromotionMgtBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    /*------------------------list data table  ------------------------------*/

    private String promoidSearch;
    private String statusSearch;
    private String promotionsCategoriesSearch;
    private String promotionsCardsSearch;
    private String subregioncodeSearch;
    private String subregionnameSearch;
    private String subreginaddressSearch;
    private String freetextSearch;
    private String infoSearch;
    private String subregionlocationSearch;
    private String promoconditionsSearch;
    private String phonenoSearch;
    private String merchantwebsiteSearch;
    private String latitudeSearch;
    private String longitudeSearch;
    private boolean search;
    
    /*-----------------------Firebase data -----------------------------*/
    private String apiKey;
    private String authDomain;
    private String databaseUrl;
    private String projectId;
    private String storageBucket;
    private String messagingSenderId;
    private String appId;
    private String authEmail;
    private String authPassword;

    private String currentUser;
    private List<PromotionMgtPendBean> gridModelPend;
    
    private String startdate;
    private String enddate;
    
    private String id;
    
    
    /*------------------------Task Code----------------------------------------*/
    private String taskCode;
    private String taskDescription;
    

    public String getPromoid() {
        return promoid;
    }

    public void setPromoid(String promoid) {
        this.promoid = promoid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromotionsCategories() {
        return promotionsCategories;
    }

    public void setPromotionsCategories(String promotionsCategories) {
        this.promotionsCategories = promotionsCategories;
    }

    public String getPromotionsCards() {
        return promotionsCards;
    }

    public void setPromotionsCards(String promotionsCards) {
        this.promotionsCards = promotionsCards;
    }

    public String getSubregionname() {
        return subregionname;
    }

    public void setSubregionname(String subregionname) {
        this.subregionname = subregionname;
    }

    public String getSubreginaddress() {
        return subreginaddress;
    }

    public void setSubreginaddress(String subreginaddress) {
        this.subreginaddress = subreginaddress;
    }

    public String getFreetext() {
        return freetext;
    }

    public void setFreetext(String freetext) {
        this.freetext = freetext;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTradinghours() {
        return tradinghours;
    }

    public void setTradinghours(String tradinghours) {
        this.tradinghours = tradinghours;
    }

    public String getSubregionlocation() {
        return subregionlocation;
    }

    public void setSubregionlocation(String subregionlocation) {
        this.subregionlocation = subregionlocation;
    }

    public String getPromoconditions() {
        return promoconditions;
    }

    public void setPromoconditions(String promoconditions) {
        this.promoconditions = promoconditions;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getMerchantwebsite() {
        return merchantwebsite;
    }

    public void setMerchantwebsite(String merchantwebsite) {
        this.merchantwebsite = merchantwebsite;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRegioncode() {
        return regioncode;
    }

    public void setRegioncode(String regioncode) {
        this.regioncode = regioncode;
    }

    public String getRegionname() {
        return regionname;
    }

    public void setRegionname(String regionname) {
        this.regionname = regionname;
    }

    public String getMasterregoncode() {
        return masterregoncode;
    }

    public void setMasterregoncode(String masterregoncode) {
        this.masterregoncode = masterregoncode;
    }

    public String getMasterregionname() {
        return masterregionname;
    }

    public void setMasterregionname(String masterregionname) {
        this.masterregionname = masterregionname;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDefaultStatus() {
        return defaultStatus;
    }

    public void setDefaultStatus(String defaultStatus) {
        this.defaultStatus = defaultStatus;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public List<PromotionsCategories> getPromotionsCategoriesList() {
        return promotionsCategoriesList;
    }

    public void setPromotionsCategoriesList(List<PromotionsCategories> promotionsCategoriesList) {
        this.promotionsCategoriesList = promotionsCategoriesList;
    }

    public List<PromotionsCards> getPromotionsCardsList() {
        return promotionsCardsList;
    }

    public void setPromotionsCardsList(List<PromotionsCards> promotionsCardsList) {
        this.promotionsCardsList = promotionsCardsList;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOldvalue() {
        return oldvalue;
    }

    public void setOldvalue(String oldvalue) {
        this.oldvalue = oldvalue;
    }

    public boolean isVadd() {
        return vadd;
    }

    public void setVadd(boolean vadd) {
        this.vadd = vadd;
    }

    public boolean isVupdatebutt() {
        return vupdatebutt;
    }

    public void setVupdatebutt(boolean vupdatebutt) {
        this.vupdatebutt = vupdatebutt;
    }

    public boolean isVupdatelink() {
        return vupdatelink;
    }

    public void setVupdatelink(boolean vupdatelink) {
        this.vupdatelink = vupdatelink;
    }

    public boolean isVdelete() {
        return vdelete;
    }

    public void setVdelete(boolean vdelete) {
        this.vdelete = vdelete;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVconfirm() {
        return vconfirm;
    }

    public void setVconfirm(boolean vconfirm) {
        this.vconfirm = vconfirm;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public List<PromotionMgtBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<PromotionMgtBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getPromoidSearch() {
        return promoidSearch;
    }

    public void setPromoidSearch(String promoidSearch) {
        this.promoidSearch = promoidSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getPromotionsCategoriesSearch() {
        return promotionsCategoriesSearch;
    }

    public void setPromotionsCategoriesSearch(String promotionsCategoriesSearch) {
        this.promotionsCategoriesSearch = promotionsCategoriesSearch;
    }

    public String getPromotionsCardsSearch() {
        return promotionsCardsSearch;
    }

    public void setPromotionsCardsSearch(String promotionsCardsSearch) {
        this.promotionsCardsSearch = promotionsCardsSearch;
    }

    public String getSubregionnameSearch() {
        return subregionnameSearch;
    }

    public void setSubregionnameSearch(String subregionnameSearch) {
        this.subregionnameSearch = subregionnameSearch;
    }

    public String getSubreginaddressSearch() {
        return subreginaddressSearch;
    }

    public void setSubreginaddressSearch(String subreginaddressSearch) {
        this.subreginaddressSearch = subreginaddressSearch;
    }

    public String getFreetextSearch() {
        return freetextSearch;
    }

    public void setFreetextSearch(String freetextSearch) {
        this.freetextSearch = freetextSearch;
    }

    public String getInfoSearch() {
        return infoSearch;
    }

    public void setInfoSearch(String infoSearch) {
        this.infoSearch = infoSearch;
    }

    public String getSubregionlocationSearch() {
        return subregionlocationSearch;
    }

    public void setSubregionlocationSearch(String subregionlocationSearch) {
        this.subregionlocationSearch = subregionlocationSearch;
    }

    public String getPromoconditionsSearch() {
        return promoconditionsSearch;
    }

    public void setPromoconditionsSearch(String promoconditionsSearch) {
        this.promoconditionsSearch = promoconditionsSearch;
    }

    public String getPhonenoSearch() {
        return phonenoSearch;
    }

    public void setPhonenoSearch(String phonenoSearch) {
        this.phonenoSearch = phonenoSearch;
    }

    public String getMerchantwebsiteSearch() {
        return merchantwebsiteSearch;
    }

    public void setMerchantwebsiteSearch(String merchantwebsiteSearch) {
        this.merchantwebsiteSearch = merchantwebsiteSearch;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public List<PromotionMgtPendBean> getGridModelPend() {
        return gridModelPend;
    }

    public void setGridModelPend(List<PromotionMgtPendBean> gridModelPend) {
        this.gridModelPend = gridModelPend;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public File getMobileImg() {
        return mobileImg;
    }

    public void setMobileImg(File mobileImg) {
        this.mobileImg = mobileImg;
    }

    public String getMobileImgContentType() {
        return mobileImgContentType;
    }

    public void setMobileImgContentType(String mobileImgContentType) {
        this.mobileImgContentType = mobileImgContentType;
    }

    public String getMobileImgFileName() {
        return mobileImgFileName;
    }

    public void setMobileImgFileName(String mobileImgFileName) {
        this.mobileImgFileName = mobileImgFileName;
    }

    public String getFirebaseImgMobUrl() {
        return firebaseImgMobUrl;
    }

    public void setFirebaseImgMobUrl(String firebaseImgMobUrl) {
        this.firebaseImgMobUrl = firebaseImgMobUrl;
    }

    public String getHasImage() {
        return hasImage;
    }

    public void setHasImage(String hasImage) {
        this.hasImage = hasImage;
    }

    public String getSubregioncode() {
        return subregioncode;
    }

    public void setSubregioncode(String subregioncode) {
        this.subregioncode = subregioncode;
    }

    public String getSubregioncodeSearch() {
        return subregioncodeSearch;
    }

    public void setSubregioncodeSearch(String subregioncodeSearch) {
        this.subregioncodeSearch = subregioncodeSearch;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitudeSearch() {
        return latitudeSearch;
    }

    public void setLatitudeSearch(String latitudeSearch) {
        this.latitudeSearch = latitudeSearch;
    }

    public String getLongitudeSearch() {
        return longitudeSearch;
    }

    public void setLongitudeSearch(String longitudeSearch) {
        this.longitudeSearch = longitudeSearch;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public String getCardimage() {
        return cardimage;
    }

    public void setCardimage(String cardimage) {
        this.cardimage = cardimage;
    }

    public boolean isVupload() {
        return vupload;
    }

    public void setVupload(boolean vupload) {
        this.vupload = vupload;
    }

    public String getHiddenId() {
        return hiddenId;
    }

    public void setHiddenId(String hiddenId) {
        this.hiddenId = hiddenId;
    }

    public String getConXLFileName() {
        return conXLFileName;
    }

    public void setConXLFileName(String conXLFileName) {
        this.conXLFileName = conXLFileName;
    }

    public File getConXL() {
        return conXL;
    }

    public void setConXL(File conXL) {
        this.conXL = conXL;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public long getFileLength() {
        return fileLength;
    }

    public void setFileLength(long fileLength) {
        this.fileLength = fileLength;
    }

    public boolean isVdual() {
        return vdual;
    }

    public void setVdual(boolean vdual) {
        this.vdual = vdual;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getAuthDomain() {
        return authDomain;
    }

    public void setAuthDomain(String authDomain) {
        this.authDomain = authDomain;
    }

    public String getDatabaseUrl() {
        return databaseUrl;
    }

    public void setDatabaseUrl(String databaseUrl) {
        this.databaseUrl = databaseUrl;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getStorageBucket() {
        return storageBucket;
    }

    public void setStorageBucket(String storageBucket) {
        this.storageBucket = storageBucket;
    }

    public String getMessagingSenderId() {
        return messagingSenderId;
    }

    public void setMessagingSenderId(String messagingSenderId) {
        this.messagingSenderId = messagingSenderId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAuthEmail() {
        return authEmail;
    }

    public void setAuthEmail(String authEmail) {
        this.authEmail = authEmail;
    }

    public String getAuthPassword() {
        return authPassword;
    }

    public void setAuthPassword(String authPassword) {
        this.authPassword = authPassword;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getIspushnotification() {
        return ispushnotification;
    }

    public void setIspushnotification(String ispushnotification) {
        this.ispushnotification = ispushnotification;
    }

    public String getIspushnotificationhidden() {
        return ispushnotificationhidden;
    }

    public void setIspushnotificationhidden(String ispushnotificationhidden) {
        this.ispushnotificationhidden = ispushnotificationhidden;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public PromotionMgtBean getPromotionOldVal() {
        return promotionOldVal;
    }

    public void setPromotionOldVal(PromotionMgtBean promotionOldVal) {
        this.promotionOldVal = promotionOldVal;
    }

    public String getTaskCode() {
        return taskCode;
    }

    public void setTaskCode(String taskCode) {
        this.taskCode = taskCode;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }
}
