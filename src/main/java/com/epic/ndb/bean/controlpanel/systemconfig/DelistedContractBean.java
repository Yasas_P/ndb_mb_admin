/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

/**
 *
 * @author sivaganesan_t
 */
public class DelistedContractBean {
    
    private String cid;
    private String delistedCategory;
    private String delistedCategoryDes;
    private String contractNumber;
    private String lastupdatedtime;
    private String createdtime;
    private String maker;
    private String checker;
    private long fullCount;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getDelistedCategory() {
        return delistedCategory;
    }

    public void setDelistedCategory(String delistedCategory) {
        this.delistedCategory = delistedCategory;
    }

    public String getDelistedCategoryDes() {
        return delistedCategoryDes;
    }

    public void setDelistedCategoryDes(String delistedCategoryDes) {
        this.delistedCategoryDes = delistedCategoryDes;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public String getCreatedtime() {
        return createdtime;
    }

    public void setCreatedtime(String createdtime) {
        this.createdtime = createdtime;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }
    
}
