/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.systemconfig;

/**
 *
 * @author sivaganesan_t
 */
public class PromotionMgtBean {
    
    private String promoid;
    private String subregioncode;
    private String status;
    private String promotionsCategories;
    private String promotionsCards;
    private String subregionname;
    private String subreginaddress;
    private String freetext;
    private String info;
    private String tradinghours;
    private String subregionlocation;
    private String promoconditions;
    private String phoneno;
    private String merchantwebsite;
    private String image;
    private String regioncode;
    private String regionname;
    private String masterregoncode;
    private String masterregionname;
    private String startdate;
    private String enddate;
    private String ispushnotification;
    private String maker;
    private String checker;
    private String lastupdatedtime;
    private String createtime;
    private String latitude;
    private String longitude;
    private String cardtype;
    private String cardimage;
    private long fullCount;

    public String getPromoid() {
        return promoid;
    }

    public void setPromoid(String promoid) {
        this.promoid = promoid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPromotionsCategories() {
        return promotionsCategories;
    }

    public void setPromotionsCategories(String promotionsCategories) {
        this.promotionsCategories = promotionsCategories;
    }

    public String getPromotionsCards() {
        return promotionsCards;
    }

    public void setPromotionsCards(String promotionsCards) {
        this.promotionsCards = promotionsCards;
    }

    public String getSubregionname() {
        return subregionname;
    }

    public void setSubregionname(String subregionname) {
        this.subregionname = subregionname;
    }

    public String getSubreginaddress() {
        return subreginaddress;
    }

    public void setSubreginaddress(String subreginaddress) {
        this.subreginaddress = subreginaddress;
    }

    public String getFreetext() {
        return freetext;
    }

    public void setFreetext(String freetext) {
        this.freetext = freetext;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTradinghours() {
        return tradinghours;
    }

    public void setTradinghours(String tradinghours) {
        this.tradinghours = tradinghours;
    }

    public String getSubregionlocation() {
        return subregionlocation;
    }

    public void setSubregionlocation(String subregionlocation) {
        this.subregionlocation = subregionlocation;
    }

    public String getPromoconditions() {
        return promoconditions;
    }

    public void setPromoconditions(String promoconditions) {
        this.promoconditions = promoconditions;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getMerchantwebsite() {
        return merchantwebsite;
    }

    public void setMerchantwebsite(String merchantwebsite) {
        this.merchantwebsite = merchantwebsite;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRegioncode() {
        return regioncode;
    }

    public void setRegioncode(String regioncode) {
        this.regioncode = regioncode;
    }

    public String getRegionname() {
        return regionname;
    }

    public void setRegionname(String regionname) {
        this.regionname = regionname;
    }

    public String getMasterregoncode() {
        return masterregoncode;
    }

    public void setMasterregoncode(String masterregoncode) {
        this.masterregoncode = masterregoncode;
    }

    public String getMasterregionname() {
        return masterregionname;
    }

    public void setMasterregionname(String masterregionname) {
        this.masterregionname = masterregionname;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    public String getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(String lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    public String getSubregioncode() {
        return subregioncode;
    }

    public void setSubregioncode(String subregioncode) {
        this.subregioncode = subregioncode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCardtype() {
        return cardtype;
    }

    public void setCardtype(String cardtype) {
        this.cardtype = cardtype;
    }

    public String getCardimage() {
        return cardimage;
    }

    public void setCardimage(String cardimage) {
        this.cardimage = cardimage;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getIspushnotification() {
        return ispushnotification;
    }

    public void setIspushnotification(String ispushnotification) {
        this.ispushnotification = ispushnotification;
    }
    
}
