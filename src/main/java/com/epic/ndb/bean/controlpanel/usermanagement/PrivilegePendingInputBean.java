/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.controlpanel.usermanagement;

import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.PendingPagetask;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.Userrole;
import java.util.List;

/**
 *
 * @author dilanka_w
 */
public class PrivilegePendingInputBean {

    private String id;
    private String comment;
    private String viewuserrole;
    private String viewsection;
    private String viewpage;
    private String viewtask;

    private String dualAuthPage;
    private String dualAuthTask;

    private String approvalCount;
    private String appcountprivilege;
    private String createduser;

    /*---------system user rserrole,username------*/
    private String sysuserrole;
    private String sysusername;

    /*---------for search view----------*/
    private String sectionSearchView;
    private String pageSearchView;
    private String taskSearchView;

    /*---------for search----------*/
    private String taskcodeSearch;
    private String pkeySearch;
    private String pkey1Search;
    private String usernameSearch;
    private boolean search;
    private String message;

    private List<Task> taskList;
    private List<Page> pageList;
    private List<Userrole> userRoleList;
    private List<PendingPagetask> pendingPagetaskList;

    /*-------for access control-----------*/
    private boolean vapprove;
    private boolean vreject;
    private boolean vsearch;
    /*-------for access control-----------*/

 /*------------------------list data table  ------------------------------*/
    private List<PendingActionBean> gridModel;
    private List<PrivilegePendingDataBean> gridModelPending;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDualAuthPage() {
        return dualAuthPage;
    }

    public void setDualAuthPage(String dualAuthPage) {
        this.dualAuthPage = dualAuthPage;
    }

    public String getDualAuthTask() {
        return dualAuthTask;
    }

    public void setDualAuthTask(String dualAuthTask) {
        this.dualAuthTask = dualAuthTask;
    }

    public String getApprovalCount() {
        return approvalCount;
    }

    public void setApprovalCount(String approvalCount) {
        this.approvalCount = approvalCount;
    }

    public String getAppcountprivilege() {
        return appcountprivilege;
    }

    public void setAppcountprivilege(String appcountprivilege) {
        this.appcountprivilege = appcountprivilege;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public boolean isVapprove() {
        return vapprove;
    }

    public void setVapprove(boolean vapprove) {
        this.vapprove = vapprove;
    }

    public boolean isVreject() {
        return vreject;
    }

    public void setVreject(boolean vreject) {
        this.vreject = vreject;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public List<PendingActionBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<PendingActionBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getTaskcodeSearch() {
        return taskcodeSearch;
    }

    public void setTaskcodeSearch(String taskcodeSearch) {
        this.taskcodeSearch = taskcodeSearch;
    }

    public String getPkeySearch() {
        return pkeySearch;
    }

    public void setPkeySearch(String pkeySearch) {
        this.pkeySearch = pkeySearch;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public String getUsernameSearch() {
        return usernameSearch;
    }

    public void setUsernameSearch(String usernameSearch) {
        this.usernameSearch = usernameSearch;
    }

    public String getSysuserrole() {
        return sysuserrole;
    }

    public void setSysuserrole(String sysuserrole) {
        this.sysuserrole = sysuserrole;
    }

    public String getSysusername() {
        return sysusername;
    }

    public void setSysusername(String sysusername) {
        this.sysusername = sysusername;
    }

    public List<Page> getPageList() {
        return pageList;
    }

    public void setPageList(List<Page> pageList) {
        this.pageList = pageList;
    }

    public List<Userrole> getUserRoleList() {
        return userRoleList;
    }

    public void setUserRoleList(List<Userrole> userRoleList) {
        this.userRoleList = userRoleList;
    }

    public String getPkey1Search() {
        return pkey1Search;
    }

    public void setPkey1Search(String pkey1Search) {
        this.pkey1Search = pkey1Search;
    }

    public String getViewuserrole() {
        return viewuserrole;
    }

    public void setViewuserrole(String viewuserrole) {
        this.viewuserrole = viewuserrole;
    }

    public String getViewsection() {
        return viewsection;
    }

    public void setViewsection(String viewsection) {
        this.viewsection = viewsection;
    }

    public String getViewpage() {
        return viewpage;
    }

    public void setViewpage(String viewpage) {
        this.viewpage = viewpage;
    }

    public String getViewtask() {
        return viewtask;
    }

    public void setViewtask(String viewtask) {
        this.viewtask = viewtask;
    }

    public List<PendingPagetask> getPendingPagetaskList() {
        return pendingPagetaskList;
    }

    public void setPendingPagetaskList(List<PendingPagetask> pendingPagetaskList) {
        this.pendingPagetaskList = pendingPagetaskList;
    }

    public List<PrivilegePendingDataBean> getGridModelPending() {
        return gridModelPending;
    }

    public void setGridModelPending(List<PrivilegePendingDataBean> gridModelPending) {
        this.gridModelPending = gridModelPending;
    }

    public String getSectionSearchView() {
        return sectionSearchView;
    }

    public void setSectionSearchView(String sectionSearchView) {
        this.sectionSearchView = sectionSearchView;
    }

    public String getPageSearchView() {
        return pageSearchView;
    }

    public void setPageSearchView(String pageSearchView) {
        this.pageSearchView = pageSearchView;
    }

    public String getTaskSearchView() {
        return taskSearchView;
    }

    public void setTaskSearchView(String taskSearchView) {
        this.taskSearchView = taskSearchView;
    }

}
