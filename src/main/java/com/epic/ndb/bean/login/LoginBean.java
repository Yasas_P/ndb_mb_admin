/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.login;

/**
 *
 * @author chanuka
 */
public class LoginBean {

    private String message;
    private String errormessage;
    private String username;
    private String password;
    private int attempts;
    private String status;
    private String choosesection;
    private String hchoosesection;

    /**
     *
     * @return
     * 
     */
    public String getErrormessage() {
        return errormessage;
    }

    /**
     *
     * @param errormessage used to display error which are raised by operaions
     */
    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }

    /**
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     */
    public int getAttempts() {
        return attempts;
    }

    /**
     *
     * @param attempts
     */
    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    /**
     *
     * @return
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     */
    public String getChoosesection() {
        return choosesection;
    }

    /**
     *
     * @param choosesection
     */
    public void setChoosesection(String choosesection) {
        this.choosesection = choosesection;
    }

    /**
     *
     * @return
     */
    public String getHchoosesection() {
        return hchoosesection;
    }

    /**
     *
     * @param hchoosesection
     */
    public void setHchoosesection(String hchoosesection) {
        this.hchoosesection = hchoosesection;
    }

    /**
     *
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
