package com.epic.ndb.bean.analytics;

import com.epic.ndb.util.mapping.SchedulePayType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.SwtScheduleFrequency;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class ScheduledPaymentInputBean {
    
    private String fromDate;
    private String toDate;
    private String cif;
    private String status;
    private String payType;
    
   /*---------------------Drop Down List-------------------------------------*/
    private List<Status> statusList;
    private List<SchedulePayType> payTypeList;
    private List<SwtScheduleFrequency> frequencyList;
    
    /*------------------------list data table  ------------------------------*/
    private List<ScheduledPaymentBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    private long fullCount;

    private String reporttype;

    /*-------for access control-----------*/
    private boolean vgenerate;
    private boolean vgenerateview;
    private boolean vsearch;
    private boolean vviewlink;
    private boolean search;

    /*-------for access control-----------*/

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public List<SchedulePayType> getPayTypeList() {
        return payTypeList;
    }

    public void setPayTypeList(List<SchedulePayType> payTypeList) {
        this.payTypeList = payTypeList;
    }

    public List<SwtScheduleFrequency> getFrequencyList() {
        return frequencyList;
    }

    public void setFrequencyList(List<SwtScheduleFrequency> frequencyList) {
        this.frequencyList = frequencyList;
    }

    public List<ScheduledPaymentBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<ScheduledPaymentBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVviewlink() {
        return vviewlink;
    }

    public void setVviewlink(boolean vviewlink) {
        this.vviewlink = vviewlink;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

}
