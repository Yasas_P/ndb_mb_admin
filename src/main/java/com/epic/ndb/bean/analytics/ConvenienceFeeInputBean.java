/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.analytics;

import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class ConvenienceFeeInputBean {

    private List<CommonKeyVal> onBoardChannelList =new ArrayList<CommonKeyVal>();
    private List<CommonKeyVal> onBoardTypeList =new ArrayList<CommonKeyVal>();
    private List<CommonKeyVal> defaultAccountTypeList =new ArrayList<CommonKeyVal>();
    private List<CommonKeyVal> feeStatusList;
    private List<Status> statusList;
    private List<SegmentType> segmentTypeList;
    
    /*-----------------------report field start-----------------------------------------*/
    private String reporttype;
    /*-----------------------report field end--------------------------------------------*/
    
    /*------------------------list data table  ------------------------------*/
    private List<ConvenienceFeeBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;

    /*------------------------list data table  ------------------------------*/
    
     /*------------------------for search ------------------------------*/
    private String cifSearch;
    private String usernameSearch;
    private String statusSearch;
    private String feeStatusSearch;
    private String fromDate;
    private String toDate;
    private String customerCategorySearch;
    private String segmentTypeSearch;
    private String onBoardTypeSearch;
    private String defTypeSearch;
    private String defAccNoSearch;
    private String onBoardChannelSearch;
    private String mobilenoSearch;

    private boolean search;
    /*------------------------for search ------------------------------*/
    
    
    /*-------for access control-----------*/
    private boolean vgenerate;
    private boolean vgenerateview;
    private boolean vsearch;
    private boolean vviewlink;

    /*-------for access control-----------*/

    public List<CommonKeyVal> getOnBoardChannelList() {
        return onBoardChannelList;
    }

    public void setOnBoardChannelList(List<CommonKeyVal> onBoardChannelList) {
        this.onBoardChannelList = onBoardChannelList;
    }

    public List<CommonKeyVal> getOnBoardTypeList() {
        return onBoardTypeList;
    }

    public void setOnBoardTypeList(List<CommonKeyVal> onBoardTypeList) {
        this.onBoardTypeList = onBoardTypeList;
    }

    public List<CommonKeyVal> getDefaultAccountTypeList() {
        return defaultAccountTypeList;
    }

    public void setDefaultAccountTypeList(List<CommonKeyVal> defaultAccountTypeList) {
        this.defaultAccountTypeList = defaultAccountTypeList;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public List<CommonKeyVal> getFeeStatusList() {
        return feeStatusList;
    }

    public void setFeeStatusList(List<CommonKeyVal> feeStatusList) {
        this.feeStatusList = feeStatusList;
    }

    public List<ConvenienceFeeBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<ConvenienceFeeBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public String getCifSearch() {
        return cifSearch;
    }

    public void setCifSearch(String cifSearch) {
        this.cifSearch = cifSearch;
    }

    public String getUsernameSearch() {
        return usernameSearch;
    }

    public void setUsernameSearch(String usernameSearch) {
        this.usernameSearch = usernameSearch;
    }

    public String getStatusSearch() {
        return statusSearch;
    }

    public void setStatusSearch(String statusSearch) {
        this.statusSearch = statusSearch;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCustomerCategorySearch() {
        return customerCategorySearch;
    }

    public void setCustomerCategorySearch(String customerCategorySearch) {
        this.customerCategorySearch = customerCategorySearch;
    }

    public String getSegmentTypeSearch() {
        return segmentTypeSearch;
    }

    public void setSegmentTypeSearch(String segmentTypeSearch) {
        this.segmentTypeSearch = segmentTypeSearch;
    }

    public String getOnBoardTypeSearch() {
        return onBoardTypeSearch;
    }

    public void setOnBoardTypeSearch(String onBoardTypeSearch) {
        this.onBoardTypeSearch = onBoardTypeSearch;
    }

    public String getDefTypeSearch() {
        return defTypeSearch;
    }

    public void setDefTypeSearch(String defTypeSearch) {
        this.defTypeSearch = defTypeSearch;
    }

    public String getDefAccNoSearch() {
        return defAccNoSearch;
    }

    public void setDefAccNoSearch(String defAccNoSearch) {
        this.defAccNoSearch = defAccNoSearch;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVviewlink() {
        return vviewlink;
    }

    public void setVviewlink(boolean vviewlink) {
        this.vviewlink = vviewlink;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public String getOnBoardChannelSearch() {
        return onBoardChannelSearch;
    }

    public void setOnBoardChannelSearch(String onBoardChannelSearch) {
        this.onBoardChannelSearch = onBoardChannelSearch;
    }

    public String getMobilenoSearch() {
        return mobilenoSearch;
    }

    public void setMobilenoSearch(String mobilenoSearch) {
        this.mobilenoSearch = mobilenoSearch;
    }

    public List<SegmentType> getSegmentTypeList() {
        return segmentTypeList;
    }

    public void setSegmentTypeList(List<SegmentType> segmentTypeList) {
        this.segmentTypeList = segmentTypeList;
    }
    
    public String getFeeStatusSearch() {
        return feeStatusSearch;
    }

    public void setFeeStatusSearch(String feeStatusSearch) {
        this.feeStatusSearch = feeStatusSearch;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }
    
}
