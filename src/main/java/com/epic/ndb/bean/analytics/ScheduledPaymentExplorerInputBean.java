/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.analytics;

import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.util.mapping.BillServiceProvider;
import com.epic.ndb.util.mapping.BillerCategory;
import com.epic.ndb.util.mapping.ProductCurrency;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.SwtResponseCodes;
import com.epic.ndb.util.mapping.SwtTxnType;
import java.io.ByteArrayInputStream;
import java.util.List;

/**
 *
 * @author sivaganesan_t
 */
public class ScheduledPaymentExplorerInputBean {
    
    private String fromDate;
    private String toDate;
    private String nic;
    private String cif;
    private String txnType;
    private String txnId;
    private String fromAccNo;
    private String toAccNo;
    private String amount;
    private String custCif;
    private String responseCode;
    private String responseDescription;
    private String txnTypeDescription;
    private String customerCategory;
    private String channelType;
    private String currencyCode;
    private String tranRefNo;
    private String iblRefNo;
    private String billCategoryName;
    private String billProviderName;
    private String status;
    private String staffStatus;

    private List<SwtTxnType> txnTypeList;
    private List<SwtResponseCodes> responseList;
    private List<ChannelTypeBean> channelTypeList;
    private List<Status> statusList;
    private List<CommonKeyVal> txnStatusList;
    private List<CommonKeyVal> staffOrNotList;
    private List<ProductCurrency> currencyList;
    private List<BillerCategory> billerCategoryList;
    private List<BillServiceProvider> billServiceProviderList;

    /*------------------------list data table  ------------------------------*/
    private List<ScheduledPaymentExplorerBean> gridModel;
    private Integer rows = 0;
    private Integer page = 0;
    private Integer total = 0;
    private Long records = 0L;
    private String sord;
    private String sidx;
    private String searchField;
    private String searchString;
    private String searchOper;
    private boolean loadonce = false;
    private long fullCount;

    private ByteArrayInputStream excelStream;
    private ByteArrayInputStream zipStream;
    private String reporttype;

    /*-------for access control-----------*/
    private boolean vgenerate;
    private boolean vgenerateview;
    private boolean vsearch;
    private boolean vviewlink;
    private boolean search;

    /*-------for access control-----------*/

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getFromAccNo() {
        return fromAccNo;
    }

    public void setFromAccNo(String fromAccNo) {
        this.fromAccNo = fromAccNo;
    }

    public String getToAccNo() {
        return toAccNo;
    }

    public void setToAccNo(String toAccNo) {
        this.toAccNo = toAccNo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCustCif() {
        return custCif;
    }

    public void setCustCif(String custCif) {
        this.custCif = custCif;
    }

    public List<SwtTxnType> getTxnTypeList() {
        return txnTypeList;
    }

    public void setTxnTypeList(List<SwtTxnType> txnTypeList) {
        this.txnTypeList = txnTypeList;
    }

    public List<ScheduledPaymentExplorerBean> getGridModel() {
        return gridModel;
    }

    public void setGridModel(List<ScheduledPaymentExplorerBean> gridModel) {
        this.gridModel = gridModel;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Long getRecords() {
        return records;
    }

    public void setRecords(Long records) {
        this.records = records;
    }

    public String getSord() {
        return sord;
    }

    public void setSord(String sord) {
        this.sord = sord;
    }

    public String getSidx() {
        return sidx;
    }

    public void setSidx(String sidx) {
        this.sidx = sidx;
    }

    public String getSearchField() {
        return searchField;
    }

    public void setSearchField(String searchField) {
        this.searchField = searchField;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getSearchOper() {
        return searchOper;
    }

    public void setSearchOper(String searchOper) {
        this.searchOper = searchOper;
    }

    public boolean isLoadonce() {
        return loadonce;
    }

    public void setLoadonce(boolean loadonce) {
        this.loadonce = loadonce;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }

    public boolean isVgenerate() {
        return vgenerate;
    }

    public void setVgenerate(boolean vgenerate) {
        this.vgenerate = vgenerate;
    }

    public boolean isVgenerateview() {
        return vgenerateview;
    }

    public void setVgenerateview(boolean vgenerateview) {
        this.vgenerateview = vgenerateview;
    }

    public boolean isVsearch() {
        return vsearch;
    }

    public void setVsearch(boolean vsearch) {
        this.vsearch = vsearch;
    }

    public boolean isVviewlink() {
        return vviewlink;
    }

    public void setVviewlink(boolean vviewlink) {
        this.vviewlink = vviewlink;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public List<SwtResponseCodes> getResponseList() {
        return responseList;
    }

    public void setResponseList(List<SwtResponseCodes> responseList) {
        this.responseList = responseList;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public String getTxnTypeDescription() {
        return txnTypeDescription;
    }

    public void setTxnTypeDescription(String txnTypeDescription) {
        this.txnTypeDescription = txnTypeDescription;
    }

    public ByteArrayInputStream getExcelStream() {
        return excelStream;
    }

    public void setExcelStream(ByteArrayInputStream excelStream) {
        this.excelStream = excelStream;
    }

    public ByteArrayInputStream getZipStream() {
        return zipStream;
    }

    public void setZipStream(ByteArrayInputStream zipStream) {
        this.zipStream = zipStream;
    }

    public String getCustomerCategory() {
        return customerCategory;
    }

    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    public List<ChannelTypeBean> getChannelTypeList() {
        return channelTypeList;
    }

    public void setChannelTypeList(List<ChannelTypeBean> channelTypeList) {
        this.channelTypeList = channelTypeList;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getTranRefNo() {
        return tranRefNo;
    }

    public void setTranRefNo(String tranRefNo) {
        this.tranRefNo = tranRefNo;
    }

    public String getBillCategoryName() {
        return billCategoryName;
    }

    public void setBillCategoryName(String billCategoryName) {
        this.billCategoryName = billCategoryName;
    }

    public String getBillProviderName() {
        return billProviderName;
    }

    public void setBillProviderName(String billProviderName) {
        this.billProviderName = billProviderName;
    }

    public List<Status> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<Status> statusList) {
        this.statusList = statusList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CommonKeyVal> getTxnStatusList() {
        return txnStatusList;
    }

    public void setTxnStatusList(List<CommonKeyVal> txnStatusList) {
        this.txnStatusList = txnStatusList;
    }

    public List<CommonKeyVal> getStaffOrNotList() {
        return staffOrNotList;
    }

    public void setStaffOrNotList(List<CommonKeyVal> staffOrNotList) {
        this.staffOrNotList = staffOrNotList;
    }

    public String getStaffStatus() {
        return staffStatus;
    }

    public void setStaffStatus(String staffStatus) {
        this.staffStatus = staffStatus;
    }

    public String getIblRefNo() {
        return iblRefNo;
    }

    public void setIblRefNo(String iblRefNo) {
        this.iblRefNo = iblRefNo;
    }

    public List<ProductCurrency> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<ProductCurrency> currencyList) {
        this.currencyList = currencyList;
    }

    public List<BillerCategory> getBillerCategoryList() {
        return billerCategoryList;
    }

    public void setBillerCategoryList(List<BillerCategory> billerCategoryList) {
        this.billerCategoryList = billerCategoryList;
    }

    public List<BillServiceProvider> getBillServiceProviderList() {
        return billServiceProviderList;
    }

    public void setBillServiceProviderList(List<BillServiceProvider> billServiceProviderList) {
        this.billServiceProviderList = billServiceProviderList;
    }
}
