/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.analytics;

/**
 *
 * @author sivaganesan_t
 */
public class ConvenienceFeeBean {
    private String userid;
    private String cif;
    private String username;
    private String mobileNumber;
    private String promotionMtSegmentType;
    private String customerName;
    private String onBoardChannel;
    private String onBoardType;
    private String status;
    private String statusDes;
    private String loginStatus;
    private String customerCategory;
    private String defType;
    private String defAccNo;
    private String waveoff;//not needed
    private String chargeAmount;
    private String feeDeductStatus;
    private String errorMessage;
    private String date;
    
    private long fullCount;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getPromotionMtSegmentType() {
        return promotionMtSegmentType;
    }

    public void setPromotionMtSegmentType(String promotionMtSegmentType) {
        this.promotionMtSegmentType = promotionMtSegmentType;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOnBoardChannel() {
        return onBoardChannel;
    }

    public void setOnBoardChannel(String onBoardChannel) {
        this.onBoardChannel = onBoardChannel;
    }

    public String getOnBoardType() {
        return onBoardType;
    }

    public void setOnBoardType(String onBoardType) {
        this.onBoardType = onBoardType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getStatusDes() {
        return statusDes;
    }

    public void setStatusDes(String statusDes) {
        this.statusDes = statusDes;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }

    public String getCustomerCategory() {
        return customerCategory;
    }

    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    public String getDefType() {
        return defType;
    }

    public void setDefType(String defType) {
        this.defType = defType;
    }

    public String getDefAccNo() {
        return defAccNo;
    }

    public void setDefAccNo(String defAccNo) {
        this.defAccNo = defAccNo;
    }

    public String getWaveoff() {
        return waveoff;
    }

    public void setWaveoff(String waveoff) {
        this.waveoff = waveoff;
    }

    public String getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(String chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public String getFeeDeductStatus() {
        return feeDeductStatus;
    }

    public void setFeeDeductStatus(String feeDeductStatus) {
        this.feeDeductStatus = feeDeductStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }
    
}
