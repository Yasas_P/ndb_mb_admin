/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.bean.analytics;

import com.epic.ndb.util.mapping.SwtTransaction;

/**
 *
 * @author sivaganesan_t
 */
public class ScheduledPaymentExplorerBean {
    private String nic;
    private String cif;
    private String txnType;
    private String txnId;
    private String fromAccNo;
    private String toAccNo;
    
     private String status;
     private String leasingModel;
     private String leasingTypes;
     private String ntbRequest;
     private String postedMethod;
     private String responseCodes;
     private String mobileNumber;
     private String amount;
     private String lastupdated;
     private String localTime;
     private String serviceFee;
     private String payeeName;
     private String billCategoryName;
     private String billProviderName;
     private String billerName;
     private String merchantTypeName;
     private String merchantName;
     private String redeemPoints;
     private String redeemVoucherId;
     private String requestMobTime;
     private String rsponseApiTime;
     private String leaseAmount;
     private String leaseAvdPayment;
     private String leaseSellPrice;
     private String custName;
     private String custEmail;
     private String custCif;
     private String address1;
     private String chequeNumber;
     private String fromDate;
     private String toDate;
     private String cardNumber;
     private String conciergeCategoryName;//currently not added concierge
     private String conciergeProductName;
     private String conciergeItemName;
     private String conciergeQuantity;
     private String conciergeReqDeleveryDate;
     private String conciergeRecpName;
     private String conciergeRecpAddress;
     private String conciergeRecpContNum;
     private String statementFrom;
     private String statementTo;
     private String currencyCode;
     private String bankName;
     private String branchName;
     private String conciergeInstructions;
     private String conciergeReqContNum;
     private String remarks;
     private String accountNo;
     private String collectFromType;
     private String collectionBranchId;
     private String preferredEmail;
     private String requestDate;
     private String type;//
     private String inquirytype;//
     private String reason;//
     private String userId;//
     private String channelType;
     private String deviceId;
     private String customerCategory;
     private String appId;
     private String billRefNo;
     private String txnMode;
     private String tranRefNo;
     private String errDesc;
     private String userName;
     private String deviceManufacture;
     private String deviceBuildNumber;
     private String payTypeDes;
     private String payType;
     
     private String t24TranReference;
     private String t24TranStatus;
     private String IBLReference;
     private String IBLTranStatus;
     private String isOneTime;
     private String staffStatus;
     private String scheduleId;
     private String isPayToMobile;
    
    private long fullCount;
    
    public ScheduledPaymentExplorerBean(SwtTransaction txn) {
        txnId = txn.getTransactionId();
        txnType = txn.getTransactionType();
        fromAccNo = txn.getFromAccountNumber();
        toAccNo = txn.getToAccountNumber();
        amount = String.valueOf(txn.getAmount());
        custCif = txn.getCustCif();             
    }
    
    public ScheduledPaymentExplorerBean() {                
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getFromAccNo() {
        return fromAccNo;
    }

    public void setFromAccNo(String fromAccNo) {
        this.fromAccNo = fromAccNo;
    }

    public String getToAccNo() {
        return toAccNo;
    }

    public void setToAccNo(String toAccNo) {
        this.toAccNo = toAccNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLeasingModel() {
        return leasingModel;
    }

    public void setLeasingModel(String leasingModel) {
        this.leasingModel = leasingModel;
    }

    public String getLeasingTypes() {
        return leasingTypes;
    }

    public void setLeasingTypes(String leasingTypes) {
        this.leasingTypes = leasingTypes;
    }

    public String getPostedMethod() {
        return postedMethod;
    }

    public void setPostedMethod(String postedMethod) {
        this.postedMethod = postedMethod;
    }

    public String getResponseCodes() {
        return responseCodes;
    }

    public void setResponseCodes(String responseCodes) {
        this.responseCodes = responseCodes;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLastupdated() {
        return lastupdated;
    }

    public void setLastupdated(String lastupdated) {
        this.lastupdated = lastupdated;
    }

    public String getLocalTime() {
        return localTime;
    }

    public void setLocalTime(String localTime) {
        this.localTime = localTime;
    }

    public String getServiceFee() {
        return serviceFee;
    }

    public void setServiceFee(String serviceFee) {
        this.serviceFee = serviceFee;
    }

    public String getPayeeName() {
        return payeeName;
    }

    public void setPayeeName(String payeeName) {
        this.payeeName = payeeName;
    }

    public String getBillCategoryName() {
        return billCategoryName;
    }

    public void setBillCategoryName(String billCategoryName) {
        this.billCategoryName = billCategoryName;
    }

    public String getBillProviderName() {
        return billProviderName;
    }

    public void setBillProviderName(String billProviderName) {
        this.billProviderName = billProviderName;
    }

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    public String getMerchantTypeName() {
        return merchantTypeName;
    }

    public void setMerchantTypeName(String merchantTypeName) {
        this.merchantTypeName = merchantTypeName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getRedeemPoints() {
        return redeemPoints;
    }

    public void setRedeemPoints(String redeemPoints) {
        this.redeemPoints = redeemPoints;
    }

    public String getRedeemVoucherId() {
        return redeemVoucherId;
    }

    public void setRedeemVoucherId(String redeemVoucherId) {
        this.redeemVoucherId = redeemVoucherId;
    }

    public String getRequestMobTime() {
        return requestMobTime;
    }

    public void setRequestMobTime(String requestMobTime) {
        this.requestMobTime = requestMobTime;
    }

    public String getRsponseApiTime() {
        return rsponseApiTime;
    }

    public void setRsponseApiTime(String rsponseApiTime) {
        this.rsponseApiTime = rsponseApiTime;
    }

    public String getLeaseAmount() {
        return leaseAmount;
    }

    public void setLeaseAmount(String leaseAmount) {
        this.leaseAmount = leaseAmount;
    }

    public String getLeaseAvdPayment() {
        return leaseAvdPayment;
    }

    public void setLeaseAvdPayment(String leaseAvdPayment) {
        this.leaseAvdPayment = leaseAvdPayment;
    }

    public String getLeaseSellPrice() {
        return leaseSellPrice;
    }

    public void setLeaseSellPrice(String leaseSellPrice) {
        this.leaseSellPrice = leaseSellPrice;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustCif() {
        return custCif;
    }

    public void setCustCif(String custCif) {
        this.custCif = custCif;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(String chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getConciergeCategoryName() {
        return conciergeCategoryName;
    }

    public void setConciergeCategoryName(String conciergeCategoryName) {
        this.conciergeCategoryName = conciergeCategoryName;
    }

    public String getConciergeProductName() {
        return conciergeProductName;
    }

    public void setConciergeProductName(String conciergeProductName) {
        this.conciergeProductName = conciergeProductName;
    }

    public String getConciergeItemName() {
        return conciergeItemName;
    }

    public void setConciergeItemName(String conciergeItemName) {
        this.conciergeItemName = conciergeItemName;
    }

    public String getConciergeQuantity() {
        return conciergeQuantity;
    }

    public void setConciergeQuantity(String conciergeQuantity) {
        this.conciergeQuantity = conciergeQuantity;
    }

    public String getConciergeReqDeleveryDate() {
        return conciergeReqDeleveryDate;
    }

    public void setConciergeReqDeleveryDate(String conciergeReqDeleveryDate) {
        this.conciergeReqDeleveryDate = conciergeReqDeleveryDate;
    }

    public String getConciergeRecpName() {
        return conciergeRecpName;
    }

    public void setConciergeRecpName(String conciergeRecpName) {
        this.conciergeRecpName = conciergeRecpName;
    }

    public String getConciergeRecpAddress() {
        return conciergeRecpAddress;
    }

    public void setConciergeRecpAddress(String conciergeRecpAddress) {
        this.conciergeRecpAddress = conciergeRecpAddress;
    }

    public String getConciergeRecpContNum() {
        return conciergeRecpContNum;
    }

    public void setConciergeRecpContNum(String conciergeRecpContNum) {
        this.conciergeRecpContNum = conciergeRecpContNum;
    }

    public String getStatementFrom() {
        return statementFrom;
    }

    public void setStatementFrom(String statementFrom) {
        this.statementFrom = statementFrom;
    }

    public String getStatementTo() {
        return statementTo;
    }

    public void setStatementTo(String statementTo) {
        this.statementTo = statementTo;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getConciergeInstructions() {
        return conciergeInstructions;
    }

    public void setConciergeInstructions(String conciergeInstructions) {
        this.conciergeInstructions = conciergeInstructions;
    }

    public String getConciergeReqContNum() {
        return conciergeReqContNum;
    }

    public void setConciergeReqContNum(String conciergeReqContNum) {
        this.conciergeReqContNum = conciergeReqContNum;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getCollectFromType() {
        return collectFromType;
    }

    public void setCollectFromType(String collectFromType) {
        this.collectFromType = collectFromType;
    }

    public String getCollectionBranchId() {
        return collectionBranchId;
    }

    public void setCollectionBranchId(String collectionBranchId) {
        this.collectionBranchId = collectionBranchId;
    }

    public String getPreferredEmail() {
        return preferredEmail;
    }

    public void setPreferredEmail(String preferredEmail) {
        this.preferredEmail = preferredEmail;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInquirytype() {
        return inquirytype;
    }

    public void setInquirytype(String inquirytype) {
        this.inquirytype = inquirytype;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getFullCount() {
        return fullCount;
    }

    public void setFullCount(long fullCount) {
        this.fullCount = fullCount;
    }
    
    public String getNtbRequest() {
        return ntbRequest;
    }

    public void setNtbRequest(String ntbRequest) {
        this.ntbRequest = ntbRequest;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getCustomerCategory() {
        return customerCategory;
    }

    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
    
    public String getBillRefNo() {
        return billRefNo;
    }

    public void setBillRefNo(String billRefNo) {
        this.billRefNo = billRefNo;
    }

    public String getTxnMode() {
        return txnMode;
    }

    public void setTxnMode(String txnMode) {
        this.txnMode = txnMode;
    }
    
    public String getTranRefNo() {
        return tranRefNo;
    }

    public void setTranRefNo(String tranRefNo) {
        this.tranRefNo = tranRefNo;
    }

    public String getErrDesc() {
        return errDesc;
    }

    public void setErrDesc(String errDesc) {
        this.errDesc = errDesc;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeviceManufacture() {
        return deviceManufacture;
    }

    public void setDeviceManufacture(String deviceManufacture) {
        this.deviceManufacture = deviceManufacture;
    }

    public String getDeviceBuildNumber() {
        return deviceBuildNumber;
    }

    public void setDeviceBuildNumber(String deviceBuildNumber) {
        this.deviceBuildNumber = deviceBuildNumber;
    }

    public String getT24TranReference() {
        return t24TranReference;
    }

    public void setT24TranReference(String t24TranReference) {
        this.t24TranReference = t24TranReference;
    }

    public String getT24TranStatus() {
        return t24TranStatus;
    }

    public void setT24TranStatus(String t24TranStatus) {
        this.t24TranStatus = t24TranStatus;
    }

    public String getIBLReference() {
        return IBLReference;
    }

    public void setIBLReference(String IBLReference) {
        this.IBLReference = IBLReference;
    }

    public String getPayTypeDes() {
        return payTypeDes;
    }

    public void setPayTypeDes(String payTypeDes) {
        this.payTypeDes = payTypeDes;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getIBLTranStatus() {
        return IBLTranStatus;
    }

    public void setIBLTranStatus(String IBLTranStatus) {
        this.IBLTranStatus = IBLTranStatus;
    }

    public String getIsOneTime() {
        return isOneTime;
    }

    public void setIsOneTime(String isOneTime) {
        this.isOneTime = isOneTime;
    }

    public String getStaffStatus() {
        return staffStatus;
    }

    public void setStaffStatus(String staffStatus) {
        this.staffStatus = staffStatus;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getIsPayToMobile() {
        return isPayToMobile;
    }

    public void setIsPayToMobile(String isPayToMobile) {
        this.isPayToMobile = isPayToMobile;
    }
}
