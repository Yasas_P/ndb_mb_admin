/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.listener;

import com.epic.ndb.util.common.ConfigProperties;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.OSInit;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author chanuka
 */
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        
        try{
            OSInit os = new OSInit();
            os.initialize(sce);
            System.out.println("OS type initialized");
        }catch(Exception e){    
            e.printStackTrace();
        }
        
        try{
            ConfigProperties confProperties = new ConfigProperties();
            confProperties.initialize();
            System.out.println("Config Properties initialized");
        }catch(Exception e){    
            e.printStackTrace();
        }
        
        try {
            HibernateInit hi = new HibernateInit();
            hi.initialize();
            System.out.println("New Session Factory Initialized.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        try {
            HibernateInit.sessionFactory.close();
            System.out.println("Session Factory Destroyed.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
