package com.epic.ndb.util.mapping;
// Generated Dec 5, 2018 2:38:56 PM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SwtResponseCodes generated by hbm2java
 */
@Entity
@Table(name="SWT_RESPONSE_CODES"
)
public class SwtResponseCodes  implements java.io.Serializable {


     private String code;
     private String categroy;
     private String description;
     private Set<SwtTransaction> swtTransactions = new HashSet<SwtTransaction>(0);

    public SwtResponseCodes() {
    }

	
    public SwtResponseCodes(String code, String categroy, String description) {
        this.code = code;
        this.categroy = categroy;
        this.description = description;
    }
    public SwtResponseCodes(String code, String categroy, String description, Set<SwtTransaction> swtTransactions) {
       this.code = code;
       this.categroy = categroy;
       this.description = description;
       this.swtTransactions = swtTransactions;
    }
   
     @Id 

    
    @Column(name="CODE", unique=true, nullable=false, length=3)
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    
    @Column(name="CATEGROY", nullable=false, length=4)
    public String getCategroy() {
        return this.categroy;
    }
    
    public void setCategroy(String categroy) {
        this.categroy = categroy;
    }

    
    @Column(name="DESCRIPTION", nullable=false, length=200)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="swtResponseCodes")
    public Set<SwtTransaction> getSwtTransactions() {
        return this.swtTransactions;
    }
    
    public void setSwtTransactions(Set<SwtTransaction> swtTransactions) {
        this.swtTransactions = swtTransactions;
    }




}


