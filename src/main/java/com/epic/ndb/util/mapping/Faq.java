package com.epic.ndb.util.mapping;
// Generated Jul 4, 2019 9:29:00 AM by Hibernate Tools 4.3.1

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Faq generated by hbm2java
 */
@Entity
@Table(name="FAQ"
)
public class Faq  implements java.io.Serializable {


     private BigDecimal id;
     private Status status;
     private String question;
     private String answer;
     private String maker;
     private String checker;
     private Date lastupdatedtime;
     private Date createdtime;

    public Faq() {
    }

	
    public Faq(BigDecimal id) {
        this.id = id;
    }
    public Faq(BigDecimal id, Status status, String question, String answer, String maker, String checker, Date lastupdatedtime, Date createdtime) {
       this.id = id;
       this.status = status;
       this.question = question;
       this.answer = answer;
       this.maker = maker;
       this.checker = checker;
       this.lastupdatedtime = lastupdatedtime;
       this.createdtime = createdtime;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getId() {
        return this.id;
    }
    
    public void setId(BigDecimal id) {
        this.id = id;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="QUESTION", length=1000)
    public String getQuestion() {
        return this.question;
    }
    
    public void setQuestion(String question) {
        this.question = question;
    }

    
    @Column(name="ANSWER", length=1000)
    public String getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    
    @Column(name="MAKER", length=64)
    public String getMaker() {
        return this.maker;
    }
    
    public void setMaker(String maker) {
        this.maker = maker;
    }

    
    @Column(name="CHECKER", length=64)
    public String getChecker() {
        return this.checker;
    }
    
    public void setChecker(String checker) {
        this.checker = checker;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDTIME", length=7)
    public Date getLastupdatedtime() {
        return this.lastupdatedtime;
    }
    
    public void setLastupdatedtime(Date lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDTIME", length=7)
    public Date getCreatedtime() {
        return this.createdtime;
    }
    
    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }




}


