package com.epic.ndb.util.mapping;
// Generated Nov 2, 2018 12:35:29 PM by Hibernate Tools 4.3.1

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Notification generated by hbm2java
 */
@Entity
@Table(name = "NOTIFICATION"
)
public class Notification implements java.io.Serializable {

    private BigDecimal id;
    private SwtMtNotificationType swtMtNotificationType;
//     private SwtTransaction swtTransaction;
    private Promotions mobPromotions;
    private Status status;
//    private String typeId;
    private String message;
    private String notificationDateTime;
    private Date createdTime;
    private Date lastUpdatedTime;
    private BigDecimal pushNotificationSent;

    public Notification() {
    }

    public Notification(BigDecimal id, String message) {
        this.id = id;
        this.message = message;
    }

    public Notification(BigDecimal id, SwtMtNotificationType swtMtNotificationType, Promotions mobPromotions, Status status, String message, String notificationDateTime, Date createdTime, Date lastUpdatedTime, BigDecimal pushNotificationSent) {
        this.id = id;
        this.swtMtNotificationType = swtMtNotificationType;
//       this.swtTransaction = swtTransaction;
        this.mobPromotions = mobPromotions;
        this.status = status;
//        this.typeId = typeId;
        this.message = message;
        this.notificationDateTime = notificationDateTime;
        this.createdTime = createdTime;
        this.lastUpdatedTime = lastUpdatedTime;
        this.pushNotificationSent=pushNotificationSent;
    }

    @Id

    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "NOTIFICATION_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")

    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NOTIFICATION_TYPE")
    public SwtMtNotificationType getSwtMtNotificationType() {
        return this.swtMtNotificationType;
    }

    public void setSwtMtNotificationType(SwtMtNotificationType swtMtNotificationType) {
        this.swtMtNotificationType = swtMtNotificationType;
    }

//@ManyToOne(fetch=FetchType.LAZY)
//    @JoinColumn(name="TRANSACTION_ID")
//    public SwtTransaction getSwtTransaction() {
//        return this.swtTransaction;
//    }
//    
//    public void setSwtTransaction(SwtTransaction swtTransaction) {
//        this.swtTransaction = swtTransaction;
//    }
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PROMOTION_ID")
    public Promotions getMobPromotions() {
        return this.mobPromotions;
    }

    public void setMobPromotions(Promotions mobPromotions) {
        this.mobPromotions = mobPromotions;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS")
    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

//    @Column(name = "TYPE_ID", length = 32)
//    public String getTypeId() {
//        return this.typeId;
//    }
//
//    public void setTypeId(String typeId) {
//        this.typeId = typeId;
//    }

    @Column(name = "MESSAGE", nullable = false, length = 512)
    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Column(name = "NOTIFICATION_DATE_TIME", length = 20)
    public String getNotificationDateTime() {
        return this.notificationDateTime;
    }

    public void setNotificationDateTime(String notificationDateTime) {
        this.notificationDateTime = notificationDateTime;
    }

    @Column(name = "CREATED_TIME")
    public Date getCreatedTime() {
        return this.createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    @Column(name = "LAST_UPDATED_TIME")
    public Date getLastUpdatedTime() {
        return this.lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }
    
    @Column(name = "PUSH_NOTIFICATION_SENT")
    public BigDecimal getPushNotificationSent() {
        return pushNotificationSent;
    }

    public void setPushNotificationSent(BigDecimal pushNotificationSent) {
        this.pushNotificationSent = pushNotificationSent;
    }
}
