package com.epic.ndb.util.mapping;
// Generated Dec 5, 2018 2:38:56 PM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SwtMtPostedMethod generated by hbm2java
 */
@Entity
@Table(name="SWT_MT_POSTED_METHOD"
)
public class SwtMtPostedMethod  implements java.io.Serializable {


     private String code;
     private Status status;
     private String description;
     private Set<SwtTransaction> swtTransactions = new HashSet<SwtTransaction>(0);

    public SwtMtPostedMethod() {
    }

	
    public SwtMtPostedMethod(String code) {
        this.code = code;
    }
    public SwtMtPostedMethod(String code, Status status, String description, Set<SwtTransaction> swtTransactions) {
       this.code = code;
       this.status = status;
       this.description = description;
       this.swtTransactions = swtTransactions;
    }
   
     @Id 

    
    @Column(name="CODE", unique=true, nullable=false, length=2)
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="STATUS")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    
    @Column(name="DESCRIPTION", length=50)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="swtMtPostedMethod")
    public Set<SwtTransaction> getSwtTransactions() {
        return this.swtTransactions;
    }
    
    public void setSwtTransactions(Set<SwtTransaction> swtTransactions) {
        this.swtTransactions = swtTransactions;
    }




}


