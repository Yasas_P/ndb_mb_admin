package com.epic.ndb.util.mapping;
// Generated Apr 10, 2019 11:40:48 AM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ProductCreditCurrencyId generated by hbm2java
 */
@Embeddable
public class ProductCreditCurrencyId  implements java.io.Serializable {


     private String debitProductType;
     private String creditProductType;
     private String creditCurrencyCode;

    public ProductCreditCurrencyId() {
    }

    public ProductCreditCurrencyId(String debitProductType, String creditProductType, String creditCurrencyCode) {
       this.debitProductType = debitProductType;
       this.creditProductType = creditProductType;
       this.creditCurrencyCode = creditCurrencyCode;
    }
   


    @Column(name="DEBIT_PRODUCT_TYPE", nullable=false, length=10)
    public String getDebitProductType() {
        return this.debitProductType;
    }
    
    public void setDebitProductType(String debitProductType) {
        this.debitProductType = debitProductType;
    }


    @Column(name="CREDIT_PRODUCT_TYPE", nullable=false, length=10)
    public String getCreditProductType() {
        return this.creditProductType;
    }
    
    public void setCreditProductType(String creditProductType) {
        this.creditProductType = creditProductType;
    }


    @Column(name="CREDIT_CURRENCY_CODE", nullable=false, length=10)
    public String getCreditCurrencyCode() {
        return this.creditCurrencyCode;
    }
    
    public void setCreditCurrencyCode(String creditCurrencyCode) {
        this.creditCurrencyCode = creditCurrencyCode;
    }


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof ProductCreditCurrencyId) ) return false;
		 ProductCreditCurrencyId castOther = ( ProductCreditCurrencyId ) other; 
         
		 return ( (this.getDebitProductType()==castOther.getDebitProductType()) || ( this.getDebitProductType()!=null && castOther.getDebitProductType()!=null && this.getDebitProductType().equals(castOther.getDebitProductType()) ) )
 && ( (this.getCreditProductType()==castOther.getCreditProductType()) || ( this.getCreditProductType()!=null && castOther.getCreditProductType()!=null && this.getCreditProductType().equals(castOther.getCreditProductType()) ) )
 && ( (this.getCreditCurrencyCode()==castOther.getCreditCurrencyCode()) || ( this.getCreditCurrencyCode()!=null && castOther.getCreditCurrencyCode()!=null && this.getCreditCurrencyCode().equals(castOther.getCreditCurrencyCode()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getDebitProductType() == null ? 0 : this.getDebitProductType().hashCode() );
         result = 37 * result + ( getCreditProductType() == null ? 0 : this.getCreditProductType().hashCode() );
         result = 37 * result + ( getCreditCurrencyCode() == null ? 0 : this.getCreditCurrencyCode().hashCode() );
         return result;
   }   


}
