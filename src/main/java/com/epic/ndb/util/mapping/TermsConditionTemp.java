/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.mapping;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author sivaganesan_t
 */
@Entity
@Table(name = "TERMS_CONDITION_TEMP")
public class TermsConditionTemp {

    private String versionNo;
    private Status status;
    private String terms;
    private Task task;
    private String category;
    private String checker;
    private String maker;
    private Date createtime;
    private Date lastupdatedtime;
    private String inputterbranch;

    public TermsConditionTemp() {

    }

    public TermsConditionTemp(String versionNo) {
        this.versionNo = versionNo;
    }

    public TermsConditionTemp(String versionNo, Status status, String terms, Task task, String category, String checker, String maker, Date createtime, Date lastupdatedtime, String inputterbranch) {
        this.versionNo = versionNo;
        this.status = status;
        this.terms = terms;
        this.task = task;
        this.category = category;
        this.checker = checker;
        this.maker = maker;
        this.createtime = createtime;
        this.lastupdatedtime = lastupdatedtime;
        this.inputterbranch = inputterbranch;
    }

    @Id
    @Column(name = "VERSION_NO", unique = true, nullable = false, length = 20)
    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Column(name = "TERMS")
    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TASK")
    public Task getTask() {
        return this.task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @Column(name = "CATEGORY", length = 20)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column(name = "CHECKER", length = 64)
    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    @Column(name = "MAKER", length = 64)
    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDTIME", length = 7)
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LASTUPDATEDTIME", length = 7)
    public Date getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(Date lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    @Column(name = "INPUTTERBRANCH", length = 20)
    public String getInputterbranch() {
        return inputterbranch;
    }

    public void setInputterbranch(String inputterbranch) {
        this.inputterbranch = inputterbranch;
    }
}
