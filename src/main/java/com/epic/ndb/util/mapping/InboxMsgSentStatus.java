package com.epic.ndb.util.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sivaganesan_t
 */
@Entity
@Table(name="INBOX_MSG_SENT_STATUS")
public class InboxMsgSentStatus  implements java.io.Serializable  {
     
    private long id;
    private String status;

    public InboxMsgSentStatus() {
    }

    public InboxMsgSentStatus(long id, String status) {
        this.id = id;
        this.status = status;
    }
    
    @Id 
    @Column(name="ID", unique=true, nullable=false, precision=15, scale=0)
    public long getId() {
        return this.id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    @Column(name="STATUS", nullable=false, length=20)
    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
}
