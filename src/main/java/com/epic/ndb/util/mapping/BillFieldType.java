package com.epic.ndb.util.mapping;
// Generated Apr 17, 2019 3:36:46 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * BillFieldType generated by hbm2java
 */
@Entity
@Table(name="BILL_FIELD_TYPE")
public class BillFieldType  implements java.io.Serializable {


     private int id;
     private String name;
     private Set<BillCustomField> billCustomFields = new HashSet<BillCustomField>(0);

    public BillFieldType() {
    }

	
    public BillFieldType(int id) {
        this.id = id;
    }
    public BillFieldType(int id, String name, Set<BillCustomField> billCustomFields) {
       this.id = id;
       this.name = name;
       this.billCustomFields = billCustomFields;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false, precision=5, scale=0)
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    
    @Column(name="NAME", length=50)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="billFieldType")
    public Set<BillCustomField> getBillCustomFields() {
        return this.billCustomFields;
    }
    
    public void setBillCustomFields(Set<BillCustomField> billCustomFields) {
        this.billCustomFields = billCustomFields;
    }




}


