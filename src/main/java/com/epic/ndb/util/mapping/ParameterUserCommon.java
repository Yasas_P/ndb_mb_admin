package com.epic.ndb.util.mapping;
// Generated Mar 4, 2019 3:22:47 PM by Hibernate Tools 4.3.1


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * ParameterUserCommon generated by hbm2java
 */
@Entity
@Table(name="PARAMETER_USER_COMMON"
)
public class ParameterUserCommon  implements java.io.Serializable {


     private String paramcode;
     private String description;
     private String paramvalue;
     private String paramType;
     private String lastupdateduser;
     private Date lastupdatedtime;
     private Date createdtime;

    public ParameterUserCommon() {
    }

	
    public ParameterUserCommon(String paramcode, String paramvalue) {
        this.paramcode = paramcode;
        this.paramvalue = paramvalue;
    }
    public ParameterUserCommon(String paramcode, String description, String paramvalue, String paramType, String lastupdateduser, Date lastupdatedtime, Date createdtime) {
       this.paramcode = paramcode;
       this.description = description;
       this.paramvalue = paramvalue;
       this.paramType = paramType;
       this.lastupdateduser = lastupdateduser;
       this.lastupdatedtime = lastupdatedtime;
       this.createdtime = createdtime;
    }
   
     @Id 

    
    @Column(name="PARAMCODE", unique=true, nullable=false, length=16)
    public String getParamcode() {
        return this.paramcode;
    }
    
    public void setParamcode(String paramcode) {
        this.paramcode = paramcode;
    }

    
    @Column(name="DESCRIPTION", length=128)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="PARAMVALUE", nullable=false, length=1000)
    public String getParamvalue() {
        return this.paramvalue;
    }
    
    public void setParamvalue(String paramvalue) {
        this.paramvalue = paramvalue;
    }

    
    @Column(name="LASTUPDATEDUSER", length=64)
    public String getLastupdateduser() {
        return this.lastupdateduser;
    }
    
    public void setLastupdateduser(String lastupdateduser) {
        this.lastupdateduser = lastupdateduser;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="LASTUPDATEDTIME", length=7)
    public Date getLastupdatedtime() {
        return this.lastupdatedtime;
    }
    
    public void setLastupdatedtime(Date lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="CREATEDTIME", length=7)
    public Date getCreatedtime() {
        return this.createdtime;
    }
    
    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

    @Column(name="PARAMETER_TYPE", length=20)
    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }


}


