
package com.epic.ndb.util.mapping;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author sivaganesan_t
 */
@Embeddable
public class FeesChargesId implements java.io.Serializable {
    
     private String chargeCode;
     private String transferType;

    public FeesChargesId() {
    }

    public FeesChargesId(String chargeCode, String transferType) {
       this.chargeCode = chargeCode;
       this.transferType = transferType;
    }
   
    @Column(name="CHARGE_CODE", nullable=false, length=10)
    public String getChargeCode() {
        return chargeCode;
    }

    public void setChargeCode(String chargeCode) {
        this.chargeCode = chargeCode;
    }
    
    @Column(name="TRANSFER_TYPE", nullable=false, length=10)
    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof FeesChargesId) ) return false;
		 FeesChargesId castOther = ( FeesChargesId ) other; 
         
		 return ( (this.getChargeCode()==castOther.getChargeCode()) || ( this.getChargeCode()!=null && castOther.getChargeCode()!=null && this.getChargeCode().equals(castOther.getChargeCode()) ) )
 && ( (this.getTransferType()==castOther.getTransferType()) || ( this.getTransferType()!=null && castOther.getTransferType()!=null && this.getTransferType().equals(castOther.getTransferType()) ) );
   }
   
   public int hashCode() {
         int result = 17;
         
         result = 37 * result + ( getChargeCode() == null ? 0 : this.getChargeCode().hashCode() );
         result = 37 * result + ( getTransferType() == null ? 0 : this.getTransferType().hashCode() );
         return result;
   }   

}
