package com.epic.ndb.util.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author sivaganesan_t
 */
@Entity
@Table(name="PRODUCTCATEGORY")
public class Productcategory {
    private String code;
    private String description;

    public Productcategory() {
    }

    public Productcategory(String code, String description) {
        this.code = code;
        this.description = description;
    }
    
    @Id 

    @Column(name="CODE", unique=true, nullable=false, length=10)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name="DESCRIPTION", nullable=false, length=50)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
