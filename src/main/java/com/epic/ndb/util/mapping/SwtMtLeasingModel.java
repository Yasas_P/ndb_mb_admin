package com.epic.ndb.util.mapping;
// Generated Dec 5, 2018 2:38:56 PM by Hibernate Tools 4.3.1

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SwtMtLeasingModel generated by hbm2java
 */
@Entity
@Table(name="SWT_MT_LEASING_MODEL"
)
public class SwtMtLeasingModel  implements java.io.Serializable {


     private BigDecimal id;
     private String model;
     private Set<SwtTransaction> swtTransactions = new HashSet<SwtTransaction>(0);

    public SwtMtLeasingModel() {
    }

	
    public SwtMtLeasingModel(BigDecimal id, String model) {
        this.id = id;
        this.model = model;
    }
    public SwtMtLeasingModel(BigDecimal id, String model, Set<SwtTransaction> swtTransactions) {
       this.id = id;
       this.model = model;
       this.swtTransactions = swtTransactions;
    }
   
     @Id 

    
    @Column(name="ID", unique=true, nullable=false, precision=22, scale=0)
    public BigDecimal getId() {
        return this.id;
    }
    
    public void setId(BigDecimal id) {
        this.id = id;
    }

    
    @Column(name="MODEL", nullable=false, length=100)
    public String getModel() {
        return this.model;
    }
    
    public void setModel(String model) {
        this.model = model;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="swtMtLeasingModel")
    public Set<SwtTransaction> getSwtTransactions() {
        return this.swtTransactions;
    }
    
    public void setSwtTransactions(Set<SwtTransaction> swtTransactions) {
        this.swtTransactions = swtTransactions;
    }




}


