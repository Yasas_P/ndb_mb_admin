/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.mapping;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author sivaganesan_t
 */
@Entity
@Table(name="REGULAR_EXPRESSION"
)
public class RegularExpression {

    private String paramcode;
    private String description;
    private String paramvalue;
    private String lastupdateduser;
    private Date lastupdatedtime;
    private Date createdtime;

    public RegularExpression() {
    }

    public RegularExpression(String paramcode, String paramvalue) {
        this.paramcode = paramcode;
        this.paramvalue = paramvalue;
    }

    public RegularExpression(String paramcode, String description, String paramvalue, String lastupdateduser, Date lastupdatedtime, Date createdtime) {
        this.paramcode = paramcode;
        this.description = description;
        this.paramvalue = paramvalue;
        this.lastupdateduser = lastupdateduser;
        this.lastupdatedtime = lastupdatedtime;
        this.createdtime = createdtime;
    }

    @Id

    @Column(name = "PARAMCODE", unique = true, nullable = false, length = 20)
    public String getParamcode() {
        return this.paramcode;
    }

    public void setParamcode(String paramcode) {
        this.paramcode = paramcode;
    }

    @Column(name = "DESCRIPTION", length = 50)
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "PARAMVALUE", nullable = false, length = 50)
    public String getParamvalue() {
        return this.paramvalue;
    }

    public void setParamvalue(String paramvalue) {
        this.paramvalue = paramvalue;
    }

    @Column(name = "LASTUPDATEDUSER", length = 64)
    public String getLastupdateduser() {
        return this.lastupdateduser;
    }

    public void setLastupdateduser(String lastupdateduser) {
        this.lastupdateduser = lastupdateduser;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LASTUPDATEDTIME", length = 7)
    public Date getLastupdatedtime() {
        return this.lastupdatedtime;
    }

    public void setLastupdatedtime(Date lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATEDTIME", length = 7)
    public Date getCreatedtime() {
        return this.createdtime;
    }

    public void setCreatedtime(Date createdtime) {
        this.createdtime = createdtime;
    }

}
