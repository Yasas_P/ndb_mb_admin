package com.epic.ndb.util.mapping;
// Generated Oct 26, 2018 9:14:52 AM by Hibernate Tools 4.3.1

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * InboxAttachment generated by hbm2java
 */
@Entity
@Table(name = "INBOX_ATTACHMENT")
public class InboxAttachment implements java.io.Serializable {

    private BigDecimal id;
    private InboxMessage inboxMessage;
    private String fileName;
    private String fileFormat;
    private String attachmentFile;
    private Date createdDate;

    public InboxAttachment() {
    }

    public InboxAttachment(BigDecimal id, InboxMessage inboxMessage) {
        this.id = id;
        this.inboxMessage = inboxMessage;
    }


    public InboxAttachment(BigDecimal id, InboxMessage inboxMessage, String fileName, String fileFormat, String attachmentFile, Date createdDate) {
        this.id = id;
        this.inboxMessage = inboxMessage;
        this.fileName = fileName;
        this.fileFormat = fileFormat;
        this.attachmentFile = attachmentFile;
        this.createdDate = createdDate;
    }

    @Id
    @SequenceGenerator(name = "SequenceIdGenerator", sequenceName = "INBOX_ATTACHMENT_SEQ")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SequenceIdGenerator")

    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "EMAIL_ID", nullable = false)
    public InboxMessage getInboxMessage() {
        return this.inboxMessage;
    }

    public void setInboxMessage(InboxMessage inboxMessage) {
        this.inboxMessage = inboxMessage;
    }

    @Column(name = "FILE_NAME", length = 50)
    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Column(name = "FILE_FORMAT", length = 20)
    public String getFileFormat() {
        return this.fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }
     
    @Lob
    @Column(name = "ATTACHMENT_FILE")
    public String getAttachmentFile() {
        return this.attachmentFile;
    }

    public void setAttachmentFile(String attachmentFile) {
        this.attachmentFile = attachmentFile;
    }

    @Column(name = "CREATED_DATE")
    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
