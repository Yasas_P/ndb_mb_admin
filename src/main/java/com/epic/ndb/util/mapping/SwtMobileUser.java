package com.epic.ndb.util.mapping;
// Generated Mar 25, 2019 11:23:54 AM by Hibernate Tools 4.3.1

import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * SwtMobileUser generated by hbm2java
 */
@Entity
@Table(name = "SWT_MOBILE_USER")
public class SwtMobileUser implements java.io.Serializable {

    private String id;
    private SegmentType segmentType;
    private Status status;
    private SwtMtLanguage swtMtLanguage;//
    private SwtMtOtpMode swtMtOtpMode;//
    private String nic;
    private String dob;
    private String cif;
    private String username;
    private String email;
    private String mobileNumber;
    private BigDecimal loginAttempts;
    private Timestamp lastupdatedtime;
    private Timestamp createdtime;
    private String profImageTimestamp;
    private String otp;
    private Timestamp otpExpTime;
    private String mobilePin;
    private BigDecimal userTraceNumber;
    private String secondaryEmail;
    private String secondaryMobile;
    private Double latitude;
    private Double longitude;
    private String idType;
    private String permanentAddress;
    private String correspondenceAddress;
    private String officeAddress;
    private String gender;
    private String daonDeviceId;
    private Timestamp lastLoggedinDatetime;//
    private BigDecimal firstTimeLoginExpected;//
    private Timestamp lastPasswordUpdatedDate;//
    private Clob profileImage;//
    private String customerName;
    private String defaultAccNo;//
    private String userKey;//
    private BigDecimal defaultAccType;//
    private String defaultAccProduct;//DEFAULT_ACC_PRODUCT//
    private String userToken;//USER_TOKEN//
    private String nonDebitable;//NON_DEBITABLE//
    private String nickname;//
    private String remark;//
    private String maker;//
    private String checker;//
    private BigDecimal onBoardType;
    private String onBoardChannel;
    private String customerCategory;
    private Timestamp finalFeeDedYear;
    private Status waveoff;
    private BigDecimal chargeAmount;
    private String actofficer;
    private String defaultCardNo;
    private String passwordResetFlag;
    private Status statusByMbStatus;
    private Status statusByIbStatus;

    public SwtMobileUser() {
    }

    public SwtMobileUser(String id, BigDecimal loginAttempts) {
        this.id = id;
        this.loginAttempts = loginAttempts;
    }

    public SwtMobileUser(String id, SegmentType segmentType, Status status, SwtMtLanguage swtMtLanguage, SwtMtOtpMode swtMtOtpMode, String nic, String dob, String cif, String username, String email, String mobileNumber, BigDecimal loginAttempts, Timestamp lastupdatedtime, Timestamp createdtime, String profImageTimestamp, String otp, Timestamp otpExpTime, String mobilePin, BigDecimal userTraceNumber, String secondaryEmail, String secondaryMobile, Double latitude, Double longitude, String idType, String permanentAddress, String correspondenceAddress, String officeAddress, String gender, String daonDeviceId, Timestamp lastLoggedinDatetime, BigDecimal firstTimeLoginExpected, Timestamp lastPasswordUpdatedDate, Clob profileImage, String customerName, String defaultAccNo, String userKey, String defaultAccProduct, BigDecimal defaultAccType, String userToken, String nonDebitable, String nickname, String remark, String maker, String checker, BigDecimal onBoardType, String onBoardChannel, String customerCategory, Timestamp finalFeeDedYear, Status waveoff, BigDecimal chargeAmount, String actofficer, String defaultCardNo, String passwordResetFlag, Status statusByMbStatus, Status statusByIbStatus) {
        this.id = id;
        this.segmentType = segmentType;
        this.status = status;
        this.swtMtLanguage = swtMtLanguage;
        this.swtMtOtpMode = swtMtOtpMode;
        this.nic = nic;
        this.dob = dob;
        this.cif = cif;
        this.username = username;
        this.email = email;
        this.mobileNumber = mobileNumber;
        this.loginAttempts = loginAttempts;
        this.lastupdatedtime = lastupdatedtime;
        this.createdtime = createdtime;
        this.profImageTimestamp = profImageTimestamp;
        this.otp = otp;
        this.otpExpTime = otpExpTime;
        this.mobilePin = mobilePin;
        this.userTraceNumber = userTraceNumber;
        this.secondaryEmail = secondaryEmail;
        this.secondaryMobile = secondaryMobile;
        this.latitude = latitude;
        this.longitude = longitude;
        this.idType = idType;
        this.permanentAddress = permanentAddress;
        this.correspondenceAddress = correspondenceAddress;
        this.officeAddress = officeAddress;
        this.gender = gender;
        this.daonDeviceId = daonDeviceId;
        this.lastLoggedinDatetime = lastLoggedinDatetime;
        this.firstTimeLoginExpected = firstTimeLoginExpected;
        this.lastPasswordUpdatedDate = lastPasswordUpdatedDate;
        this.profileImage = profileImage;
        this.customerName = customerName;
        this.defaultAccNo = defaultAccNo;
        this.defaultAccProduct = defaultAccProduct;
        this.userToken = userToken;
        this.nonDebitable = nonDebitable;
        this.userKey = userKey;
        this.defaultAccType = defaultAccType;
        this.nickname = nickname;
        this.remark = remark;
        this.maker = maker;
        this.checker = checker;
        this.onBoardType = onBoardType;
        this.onBoardChannel = onBoardChannel;
        this.customerCategory = customerCategory;
        this.finalFeeDedYear = finalFeeDedYear;
        this.waveoff = waveoff;
        this.chargeAmount = chargeAmount;
        this.actofficer = actofficer;
        this.defaultCardNo = defaultCardNo;
        this.passwordResetFlag = passwordResetFlag;
        this.statusByMbStatus = statusByMbStatus;
        this.statusByIbStatus = statusByIbStatus;
    }

    @Id

    @Column(name = "ID", unique = true, nullable = false, length = 20)
    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_SEGMENT")
    public SegmentType getSegmentType() {
        return this.segmentType;
    }

    public void setSegmentType(SegmentType segmentType) {
        this.segmentType = segmentType;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS")
    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LANGUAGE_CODE")
    public SwtMtLanguage getSwtMtLanguage() {
        return this.swtMtLanguage;
    }

    public void setSwtMtLanguage(SwtMtLanguage swtMtLanguage) {
        this.swtMtLanguage = swtMtLanguage;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "OTP_MODE")
    public SwtMtOtpMode getSwtMtOtpMode() {
        return this.swtMtOtpMode;
    }

    public void setSwtMtOtpMode(SwtMtOtpMode swtMtOtpMode) {
        this.swtMtOtpMode = swtMtOtpMode;
    }

    @Column(name = "NIC", length = 20)
    public String getNic() {
        return this.nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    @Column(name = "DOB", length = 40)
    public String getDob() {
        return this.dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @Column(name = "CIF", length = 256)
    public String getCif() {
        return this.cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    @Column(name = "USERNAME", length = 50)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "EMAIL", length = 256)
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "MOBILE_NUMBER", length = 20)
    public String getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Column(name = "LOGIN_ATTEMPTS", nullable = false, precision = 22, scale = 0)
    public BigDecimal getLoginAttempts() {
        return this.loginAttempts;
    }

    public void setLoginAttempts(BigDecimal loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    @Column(name = "LASTUPDATEDTIME")
    public Timestamp getLastupdatedtime() {
        return this.lastupdatedtime;
    }

    public void setLastupdatedtime(Timestamp lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }

    @Column(name = "CREATEDTIME")
    public Timestamp getCreatedtime() {
        return this.createdtime;
    }

    public void setCreatedtime(Timestamp createdtime) {
        this.createdtime = createdtime;
    }

    @Column(name = "PROF_IMAGE_TIMESTAMP", length = 100)
    public String getProfImageTimestamp() {
        return this.profImageTimestamp;
    }

    public void setProfImageTimestamp(String profImageTimestamp) {
        this.profImageTimestamp = profImageTimestamp;
    }

    @Column(name = "OTP", length = 50)
    public String getOtp() {
        return this.otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @Column(name = "OTP_EXP_TIME")
    public Timestamp getOtpExpTime() {
        return this.otpExpTime;
    }

    public void setOtpExpTime(Timestamp otpExpTime) {
        this.otpExpTime = otpExpTime;
    }

    @Column(name = "MOBILE_PIN", length = 250)
    public String getMobilePin() {
        return this.mobilePin;
    }

    public void setMobilePin(String mobilePin) {
        this.mobilePin = mobilePin;
    }

    @Column(name = "USER_TRACE_NUMBER", precision = 22, scale = 0)
    public BigDecimal getUserTraceNumber() {
        return this.userTraceNumber;
    }

    public void setUserTraceNumber(BigDecimal userTraceNumber) {
        this.userTraceNumber = userTraceNumber;
    }

    @Column(name = "SECONDARY_EMAIL", length = 256)
    public String getSecondaryEmail() {
        return this.secondaryEmail;
    }

    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    @Column(name = "SECONDARY_MOBILE", length = 20)
    public String getSecondaryMobile() {
        return this.secondaryMobile;
    }

    public void setSecondaryMobile(String secondaryMobile) {
        this.secondaryMobile = secondaryMobile;
    }

    @Column(name = "LATITUDE", precision = 20, scale = 0)
    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Column(name = "LONGITUDE", precision = 20, scale = 0)
    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Column(name = "ID_TYPE", length = 20)
    public String getIdType() {
        return this.idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    @Column(name = "PERMANENT_ADDRESS", length = 250)
    public String getPermanentAddress() {
        return this.permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    @Column(name = "CORRESPONDENCE_ADDRESS", length = 250)
    public String getCorrespondenceAddress() {
        return this.correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    @Column(name = "OFFICE_ADDRESS", length = 250)
    public String getOfficeAddress() {
        return this.officeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        this.officeAddress = officeAddress;
    }

    @Column(name = "GENDER", length = 10)
    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Column(name = "DAON_DEVICE_ID", length = 20)
    public String getDaonDeviceId() {
        return this.daonDeviceId;
    }

    public void setDaonDeviceId(String daonDeviceId) {
        this.daonDeviceId = daonDeviceId;
    }

    @Column(name = "LAST_LOGGEDIN_DATETIME")
    public Timestamp getLastLoggedinDatetime() {
        return this.lastLoggedinDatetime;
    }

    public void setLastLoggedinDatetime(Timestamp lastLoggedinDatetime) {
        this.lastLoggedinDatetime = lastLoggedinDatetime;
    }

    @Column(name = "FIRST_TIME_LOGIN_EXPECTED", precision = 20, scale = 0)
    public BigDecimal getFirstTimeLoginExpected() {
        return this.firstTimeLoginExpected;
    }

    public void setFirstTimeLoginExpected(BigDecimal firstTimeLoginExpected) {
        this.firstTimeLoginExpected = firstTimeLoginExpected;
    }

    @Column(name = "LAST_PASSWORD_UPDATED_DATE")
    public Timestamp getLastPasswordUpdatedDate() {
        return this.lastPasswordUpdatedDate;
    }

    public void setLastPasswordUpdatedDate(Timestamp lastPasswordUpdatedDate) {
        this.lastPasswordUpdatedDate = lastPasswordUpdatedDate;
    }

    @Column(name = "PROFILE_IMAGE")
    public Clob getProfileImage() {
        return this.profileImage;
    }

    public void setProfileImage(Clob profileImage) {
        this.profileImage = profileImage;
    }

    @Column(name = "CUSTOMER_NAME", length = 200)
    public String getCustomerName() {
        return this.customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(name = "DEFAULT_ACC_NO", length = 20)
    public String getDefaultAccNo() {
        return this.defaultAccNo;
    }

    public void setDefaultAccNo(String defaultAccNo) {
        this.defaultAccNo = defaultAccNo;
    }

    @Column(name = "USER_KEY", length = 40)
    public String getUserKey() {
        return this.userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @Column(name = "DEFAULT_ACC_TYPE", precision = 22, scale = 0)
    public BigDecimal getDefaultAccType() {
        return this.defaultAccType;
    }

    public void setDefaultAccType(BigDecimal defaultAccType) {
        this.defaultAccType = defaultAccType;
    }

    @Column(name = "DEFAULT_ACC_PRODUCT", length = 100)
    public String getDefaultAccProduct() {
        return this.defaultAccProduct;
    }

    public void setDefaultAccProduct(String defaultAccProduct) {
        this.defaultAccProduct = defaultAccProduct;
    }

    @Column(name = "USER_TOKEN", length = 50)
    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    @Column(name = "NON_DEBITABLE", length = 20)
    public String getNonDebitable() {
        return nonDebitable;
    }

    public void setNonDebitable(String nonDebitable) {
        this.nonDebitable = nonDebitable;
    }

    @Column(name = "NICKNAME", length = 200)
    public String getNickname() {
        return this.nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Column(name = "REMARK", length = 250)
    public String getRemark() {
        return this.remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Column(name = "MAKER", length = 64)
    public String getMaker() {
        return this.maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    @Column(name = "CHECKER", length = 64)
    public String getChecker() {
        return this.checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    @Column(name = "ON_BOARD_TYPE")
    public BigDecimal getOnBoardType() {
        return onBoardType;
    }

    public void setOnBoardType(BigDecimal onBoardType) {
        this.onBoardType = onBoardType;
    }

    @Column(name = "ON_BOARD_CHANNEL", length = 2)
    public String getOnBoardChannel() {
        return onBoardChannel;
    }

    public void setOnBoardChannel(String onBoardChannel) {
        this.onBoardChannel = onBoardChannel;
    }

    @Column(name = "CUSTOMER_CATEGORY", length = 100)
    public String getCustomerCategory() {
        return customerCategory;
    }

    public void setCustomerCategory(String customerCategory) {
        this.customerCategory = customerCategory;
    }

    @Column(name = "FINAL_FEE_DED_DAY", length = 100)
    public Timestamp getFinalFeeDedYear() {
        return finalFeeDedYear;
    }

    public void setFinalFeeDedYear(Timestamp finalFeeDedYear) {
        this.finalFeeDedYear = finalFeeDedYear;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "WAIVEOFF")
    public Status getWaveoff() {
        return waveoff;
    }

    public void setWaveoff(Status waveoff) {
        this.waveoff = waveoff;
    }

    @Column(name = "CHARGE_AMOUNT", nullable = false, precision = 22, scale = 2)
    public BigDecimal getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(BigDecimal chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    @Column(name = "ACTOFFICER", length = 100)
    public String getActofficer() {
        return actofficer;
    }

    public void setActofficer(String actofficer) {
        this.actofficer = actofficer;
    }

    @Column(name = "DEFAULT_CARD_NO", length = 20)
    public String getDefaultCardNo() {
        return defaultCardNo;
    }

    public void setDefaultCardNo(String defaultCardNo) {
        this.defaultCardNo = defaultCardNo;
    }

    @Column(name = "PW_RESET_FLAG", length = 2)
    public String getPasswordResetFlag() {
        return passwordResetFlag;
    }

    public void setPasswordResetFlag(String passwordResetFlag) {
        this.passwordResetFlag = passwordResetFlag;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MB_STATUS", nullable = false)
    public Status getStatusByMbStatus() {
        return this.statusByMbStatus;
    }

    public void setStatusByMbStatus(Status statusByMbStatus) {
        this.statusByMbStatus = statusByMbStatus;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IB_STATUS", nullable = false)
    public Status getStatusByIbStatus() {
        return this.statusByIbStatus;
    }

    public void setStatusByIbStatus(Status statusByIbStatus) {
        this.statusByIbStatus = statusByIbStatus;
    }
}
