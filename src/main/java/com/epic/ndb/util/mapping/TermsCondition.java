package com.epic.ndb.util.mapping;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author sivaganesan_t
 */
@Entity
@Table(name = "TERMS_CONDITION")
public class TermsCondition  implements java.io.Serializable {
    
    private String versionNo;
    private Status status;
    private String terms;
    private String category;
    private String checker;
    private String maker;
    private Date createtime;
    private Date lastupdatedtime;

    public TermsCondition() {
        
    }

    public TermsCondition(String versionNo) {
        this.versionNo = versionNo;
    }

    public TermsCondition(String versionNo, Status status, String terms, String category, String checker, String maker, Date createtime, Date lastupdatedtime) {
        this.versionNo = versionNo;
        this.status = status;
        this.terms = terms;
        this.category = category;
        this.checker = checker;
        this.maker = maker;
        this.createtime = createtime;
        this.lastupdatedtime = lastupdatedtime;
    }

    @Id
    @Column(name = "VERSION_NO", unique = true, nullable = false, length = 20)
    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STATUS")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Column(name = "TERMS")
    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    @Column(name = "CATEGORY", length = 20)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Column(name="CHECKER", length=64)
    public String getChecker() {
        return checker;
    }

    public void setChecker(String checker) {
        this.checker = checker;
    }

    @Column(name="MAKER", length=64)
    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATEDTIME", length=7)
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="LASTUPDATEDTIME", length=7)
    public Date getLastupdatedtime() {
        return lastupdatedtime;
    }

    public void setLastupdatedtime(Date lastupdatedtime) {
        this.lastupdatedtime = lastupdatedtime;
    }
    
    
    
}
