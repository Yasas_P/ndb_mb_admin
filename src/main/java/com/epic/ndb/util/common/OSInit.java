/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.common;

import com.epic.ndb.util.varlist.CommonVarList;
import javax.servlet.ServletContextEvent;
import org.apache.commons.lang3.SystemUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author TU-Nuwan
 */
public class OSInit {

    public void initialize(ServletContextEvent servletContextEvent) {
        if (SystemUtils.IS_OS_LINUX) {
            Logger.getLogger(OSInit.class.getName()).log(Level.INFO, "Linux system detected");
            CommonVarList.PROPERTY_FILE_FOLDER_PATH = servletContextEvent.getServletContext().getInitParameter("property_file_path_linux");
        } else if (SystemUtils.IS_OS_WINDOWS) {
            Logger.getLogger(OSInit.class.getName()).log(Level.INFO, "Windows system detected");
            CommonVarList.PROPERTY_FILE_FOLDER_PATH = servletContextEvent.getServletContext().getInitParameter("property_file_path_windows");
        } else {
            Logger.getLogger(OSInit.class.getName()).log(Level.INFO, "File system cannot detected");
            CommonVarList.PROPERTY_FILE_FOLDER_PATH = servletContextEvent.getServletContext().getInitParameter("property_file_path_windows");
        }
    }

}
