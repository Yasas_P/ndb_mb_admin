/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.common;

import com.epic.ndb.util.varlist.CommonVarList;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author TU-Nuwan
 */
public class ConfigProperties {

    public void initialize() throws Exception {
        InputStream propertyfile=null;
        try {
            propertyfile = new FileInputStream(CommonVarList.PROPERTY_FILE_FOLDER_PATH);
            
            Properties propfile = new Properties();
            propfile.load(propertyfile);
            
            CommonVarList.INITIAL_CONTEXT_FACTORY = propfile.getProperty("initial_context_factory");
            CommonVarList.SECURITY_AUTHENTICATION = propfile.getProperty("security_authentication");
            CommonVarList.PROVIDER_URL = propfile.getProperty("provicer_url");
            CommonVarList.SECURITY_PRINCIPAL = propfile.getProperty("security_principle");
            CommonVarList.SECURITY_CREDENTIALS = propfile.getProperty("security_credential");
            

        } catch (Exception e) {
            throw e;
        } finally {
            if(propertyfile!=null){
               propertyfile.close();
            }
        }
    }
}
