/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.varlist;

/**
 * @author chanuka
 */
public class PageVarList {

    public static final String LOGIN_PAGE = "LGPG";
    public static final String TASK_MGT_PAGE = "UMTM";
    public static final String PAS_POLICY = "PWPM";
    public static final String USER_ROLE_PRIVILEGE_MGT = "UMUP";
    public static final String USER_ROLE_MGT_PAGE = "UMUR";
    public static final String SECTION_MGT_PAGE = "UMSM";
    public static final String PAGE_MGT_PAGE = "UMPM";
    public static final String SYSTEM_AUDIT_PAGE = "SCSA";
    public static final String SYSTEM_USER = "UMSU";
    public static final String COMMON_CONFIGURATION_PAGE = "SCCC";
    public static final String TXN_EXPLORER = "RETE";
    public static final String TXN_TYPE_MGT_PAGE = "TXTM";
    public static final String IB_MOB_USER_PARAM_PAGE = "MSUP";
    public static final String SMS_OUTBOX_PAGE = "RESO";
    public static final String SMS_SERVER_CONFIGURATION = "SCMT";
    public static final String SMS_CSV_MGT_PAGE = "SMSC";
    public static final String SMS_PERSONAL_ALERT_PAGE = "SMPA";
    public static final String SMS_CONTROL_PANEL_PAGE = "SMCP";

    public static final String SMS_TXT_MGT_PAGE = "SMST";
    public static final String SMS_SPECIAL_ALERT_PAGE = "SMSA";
    public static final String SMS_EXCLUDED_MOB_PAGE = "SMEM";
    public static final String SMS_APPLICATION_PAGE = "SMAP";
    public static final String SMS_APPLICATION_SERVICE_PAGE = "SAPS";
    public static final String SMS_EXCLUDED_PAGE = "SMEX";
    public static final String SMS_ALERT_TRANSACTION_PAGE = "SMAT";

    public static final String TXNTYPE_MGT_PAGE = "TXTM";
    public static final String SMPP_VANDER_CONFIGURATION_PAGE = "SMPC";
    public static final String COMMON_CONFIG_MGT_PAGE = "COCN";
    public static final String TXN_CODE_PAGE = "TXCM";

    public static final String CUSTOMER_SEARCH = "CUSS";
    public static final String CUSTOMER_SUB_MGT = "CURE";
    public static final String CUSTOMER_CATEGORY_PAGE = "CUCT";

    public static final String SMS_SCHEDULE_MGT_PAGE = "SSMT";
    public static final String BRANCH_MGT_PAGE = "BRMT";
    public static final String SMS_TEMPLATE_MGT_PAGE = "SOTM";
    public static final String TXN_CATEGORY_MGT_PAGE = "TRCM";
    public static final String TXN_TYPE_CODE_COMBINATION_PAGE = "TXTC";

    public static final String JOIN_CODE_MGT_PAGE = "JOCM";
    public static final String REMINDER_ALERT_MGT_PAGE = "REAT";
    public static final String TRANSACATION_ALERT_MGT_PAGE = "TRAT";
    public static final String SMS_OUTBOX_BACKUP_PAGE = "RESB";
    public static final String SEGMEMNT_MGT_PAGE = "SEMT";

    // Biller
    public static final String BILLER_CATEGORY_MGT_PAGE = "SCBC";
    public static final String BILLER_SERVICE_PROVIDER_PAGE = "BLSP";

    public static final String PRIVILEGE_PENDING_ACTION_PAGE = "DPPA";

    public static final String PROMOTION_MGT_PAGE = "SCPM";
    public static final String PROMOTION_CAT_PAGE = "PROC";
    public static final String M_BANK_LOCATOR_PAGE = "SCMB";
    public static final String CARD_CENTER_PAGE = "SCCC";
    public static final String OTHER_BRANCH_MGT_PAGE = "SCOB";
    public static final String TRANSACTION_LIMIT_MGT_PAGE = "SCTL";
    public static final String FEES_CHARGE_MGT_PAGE = "SCFC";
    public static final String PRODUCT_TYPE_MGT_PAGE = "SCPT";
    public static final String PRODUCT_MATRIX_MGT_PAGE = "SCMA";

    public static final String TRAN_EXPLORER = "TREP";
    public static final String TERMS_PAGE = "TERM";

    public static final String CUSTOMER_STATUS = "CUST";
    public static final String FEE_CONFIG_MGT_PAGE = "SCFE";
    public static final String DELISTED_CONTRACT_PAGE = "SCDL";

    public static final String BUSINESS_CONFIG_MGT_PAGE = "COBC";
    public static final String FAQ_MGT_PAGE = "FAQS";
    public static final String CARD_REQUEST_MGT_PAGE = "CRMT";
    public static final String SCHEDULED_PAYMENT = "SCPA";
    public static final String SCHEDULED_PAYMENT_HISTORY = "SCPH";
    public static final String CONVENIENCE_FEE = "COFE";
    public static final String PAYEE_EXPLORER = "REPA";
    public static final String PAY_TO_MOBILE = "REPE";
    public static final String BILLER = "REBI";
    
    public static final String EMAIL_MGT_PAGE = "EMMT";
    public static final String SENDEMAIL_MGT_PAGE = "EMSM";
    public static final String SCHEDULED_PAYMENT_EXPLORER_PAGE = "SPEP";
    
    public static final String MAIL_SUB_MGT_PAGE = "EMSJ";
}
