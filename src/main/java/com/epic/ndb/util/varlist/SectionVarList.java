/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.varlist;

/**
 *
 * @author chanuka
 */
public class SectionVarList {

    public static final String DEFAULT_SECTION = "DFSC";
    public static final String USERMANAGEMENT = "UMSC";
    public static final String SYSTEMCONFIGMANAGEMENT = "SCSC";
    public static final String SYSTEM_CONFIG = "SCSC";
    public static final String SMS_MANAGEMENT = "SMSC";
    public static final String CUSTOMER_ACCOUNT_MANAGEMENT = "CUMG";
    public static final String REPORT_EXPLORER = "RESC";
    public static final String CUSTOMER_MANAGEMENT = "CUSM";
    public static final String LOG_MANAGEMENT = "LGVI";
    public static final String OTP_CONFIGURATION = "OCSC";
    public static final String SYSTEM_AUDIT = "SYAU";
    public static final String ACQUIRER_CONFIG_MGT = "ACCM";
    public static final String ANALYTICS = "ANLT";
    public static final String REGISTERED_BENEFICIARY = "REBE";
    public static final String EMAIL_MANAGEMENT = "EMMT";
}
