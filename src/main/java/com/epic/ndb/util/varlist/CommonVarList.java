/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.varlist;

import java.util.HashMap;

/**
 *
 * @author chanuka
 */
public class CommonVarList {

    public static final int LOGIN_ATTEMPTS = 3;
    public static final String LEVEL_01 = "1";
    public static final String STATUS_ACTIVE = "ACT";
    public static final String STATUS_CUS_WAL_ACTIVE = "CWA";
    public static final String STATUS_CUS_WAL_DEACTIVE = "CWD";
    public static final String STATUS_CUS_WAL_CUS_DELETED = "CWDL";
    public static final String STATUS_YES = "YES";
    public static final String STATUS_NO = "NO";
    public static final String STATUS_UNREAD = "6";
    //public static final String STATUS_INITIAL = "INIT";
    public static final String STATUS_INITIATED = "ACRE";
    public static final String STATUS_INITIAL_DES = "Initial";
    public static final String STATUS_CONFIRMED = "CONF";
    public static final String STATUS_SENT = "SENT";
    public static final String STATUS_SENT_TO_QUEUE = "QUED";
    public static final String STATUS_PENDING = "PEND";
    public static final String STATUS_CUSTOMER_PEND = "PENC";
    public static final String STATUS_CATEGORY_GENERAL = "DFLT";
    public static final String STATUS_CATEGORY_LOGIN_HISTORY = "LOGN";
    public static final String STATUS_CATEGORY_CUS_WALLET = "CWS";
    public static final String STATUS_CATEGORY_OTP_REQUEST_INITIATE = "SER";
    public static final String STATUS_CATEGORY_SYSUSER = "SYUS";
    public static final String STATUS_CATEGORY_AUTH = "AUTH";
    public static final String STATUS_CATEGORY_CUSTOMER = "CUST";
    public static final String STATUS_CATEGORY_SWTCH = "SWTC";
    public static final String STATUS_DEACTIVE = "DEACT";
    public static final String STATUS_TRANSACTION = "TRAN";
    public static final String STATUS_DELETE = "DEL";
    public static final String SMS_BANKING_APPTYPE = "SMSB";
    public static final String TRANSACTION_TYPE = "RDCD";
    public static final String TRANSACTION_STATUS = "TXNS";
    public static final String STATUS_CAT_CONFIRMED = "CONF";
    public static final String SEGMENT_ALERT_MANAGEMENT = "Segment Alert Management";
    public static final String PERSONAL_ALERT_MANAGEMENT = "Personal Alert Management";
    public static final String SPECIAL_ALERT_MANAGEMENT = "Special Alert Management";
    public static final String STAFF_ALERT_MANAGEMENT = "Staff Alert Management";
    public static final String CORPORATE = "Corporate";
    public static final String NON_CORPORATE = "NonCorporate";
    public static final String SPECIAL_ALERT = "Special";
    public static final String STAFF_ALERT = "Staff";
    public static final String PERSONAL_ALERT = "Personal";
    public static final String SEGMENT_ALERT = "Segment";
    public static final String REPORT_SBANK_ADD_HEADER = "Sri Lanka";
    public static final String REPORT_SBANK_ADD = "40, Navam Mawatha, Colombo 02";
    public static final String REPORT_SBANK_TEL = "+94 11 244 8448";
    public static final String REPORT_SBANK_MAIL = "contact@ndbbank.com";
    public static final String JNDI_REPORT_CONNECTION = "RDB_JNDI";
//    public static HashMap<String, String> SMSMESSAGESMAP = new HashMap<String, String>();
    public static final String IMAGE_URL = "IMGURL";
    public static final String STATUS_REJECT = "CREJ";
    public static final String STATUS_APPROVED = "CAPR";
    public static final String SYS_MERCHANT = "MERCH";
    public static final String STATUS_CATEGORY_SMS_TXN = "SMTX";
    public static final String STATUS_CATEGORY_SMS_ALERT = "ALERT";
    public static final String REPORT_ADD_HEADER = "SRI LANKA";
    public static final String REPORT_ADDRESS = "40, Navam Mawatha, Colombo 02";
    public static final String REPORT_TEL = "+94 11 244 8448";
    public static final String REPORT_MAIL = "contact@ndbbank.com";
    public static final String SMS_ALERT_STATUS_CATEGORY = "SAS";
    public static final String STATUS_SMS_CONFIRMED = "ACON";
    //public static final String STATUS_SMS_SENT = "ASEN";
    public static final String STATUS_SMS_ALERT_QUE = "AQUE";
    public static final String SMS_CONTROL_PANEL_REFRESH_CODE = "51";
    public static final String SMS_CONTROL_PANEL_RESTART_CODE = "50";
    public static final String APPLICATION_CODE = "SSS";
    public static final String OUTBOX_TXN_TYPE_CODE_OTPT = "OTPT";
    public static final String OUTBOX_TXN_TYPE_CODE_SINGLE_ALERT = "SALERT";
    public static final String OUTBOX_TXN_TYPE_CODE_SPEBULK = "SPEBULK";
    public static final String OUTBOX_TXN_TYPE_CODE_PERBULK = "PERBULK";
    public static final String OUTBOX_TXN_TYPE_CODE_OTPT_DES = "OTP";
    public static final String OUTBOX_TXN_TYPE_CODE_SPEBULK_DES = "Special alert bulk";
    public static final String OUTBOX_TXN_TYPE_CODE_PERBULK_DES = "Personal alert bulk";
    public static final String OUTBOX_MSG_TYPE_CODE_SPEALERT = "SPEALERT";
    public static final String OUTBOX_MSG_TYPE_CODE_PERALERT = "PERALERT";
    public static final String OUTBOX_MSG_TYPE_CODE_SPEALERT_DES = "Special alert";
    public static final String OUTBOX_MSG_TYPE_CODE_PERALERT_DES = "Personal alert";
    public static final String OUTBOX_RQST_TYPE_CODE_WSPA = "WSPA";
    public static final String OUTBOX_RQST_TYPE_CODE_WPEA = "WPEA";
    public static final String OUTBOX_RQST_TYPE_CODE_WSPA_DES = "Web special alert";
    public static final String OUTBOX_RQST_TYPE_CODE_WPEA_DES = "Web personal alert";
    public static final String WEB_MODULE_APP_CODE = "WEB";
    public static final String SERVICECODE_SPECIAL_BULK = "WEBS";
    public static final String SERVICECODE_PERSONAL_BULK = "WEBP";

    public static final String STATUS_CUS_ACTIVE = "CACT";
    public static final String STATUS_CUS_DEACTIVE = "CDCT";
    public static final String STATUS_CUS_INITIAL = "CINI";
    
    public static final String STATUS_PENDING_REJECTED = "PREJ";
    public static final String STATUS_PENDING_CONFIRMED = "PAPR";

    public static final String STATUS_CATEGORY_EMAIL = "MAIL";
    public static final String STATUS_CATEGORY_INBOX = "INBO";
    
    public static final String STATUS_INBOX_NEW = "NEW";
    public static final String STATUS_INBOX_NOREPLY = "NORP";
    public static final String STATUS_INBOX_INPROGRESS = "INPR";
    public static final String STATUS_INBOX_COMPLETE = "COMP";

    public static final String IP = "127.0.0.1";
    public static final int PORT = 9988;
    public static final int TIMEOUT = 100000;
    
    
//    public static final String IP = "192.168.1.171";
//    public static final int PORT = 9001;
//    public static final int TIMEOUT = 100000;
    
    public static final String SWITCH_IP = "SWITCH_IP";
    public static final String SWITCH_PORT_CUSINQ = "SWITCH_PORT_CUSINQ";
    public static final String SWITCH_TIMEOUT_CUSINQ = "SWITCH_TIMEOUT_CUSINQ";
    
    public static final String CUSTOMER_ID_TYPE = "CID";
    public static final String BKP_DATE_RANGE_MAX_VALUE_PARAM_CODE = "SMS_BACKUP_DAYS";
    
    public static final String STATUS_SMS_ALERT_HOLD = "HOLD";
    public static final String STATUS_SMS_ALERT_QUE_PROCESSS = "APRO";

    public static final String PRO_IMG_URL = "PRO_IMG_URL";
    public static final String TXN_TYPE_CAT_TRANSFER = "TRANSFER";
    public static final String PRO_IMG_URL_CODE = "PRO_IMG_URL";
    public static final String COMMON_CONFIG_TERM_COND_VER_CODE = "TER_CON";
    public static final String COMMON_CONFIG_ACCOUNT_LIST_URN = "ACCOUNT_LIST_URN";
    public static final String COMMON_CONFIG_REST_BASE_URL = "REST_BASE_URL";
    public static final long SWT_CONFIG_ID = 1;
    
    public static final String BILL_SERVICE_PROVIDER_TRANSFER_TYPE = "BILLSERPRO";
    public static final String MOB_USER_NAME_POLICY_ID = "3";
    public static final String MOB_USER_PAS_POLICY_ID = "2";
    public static final String ADMIN_USER_PAS_POLICY_ID = "1";
    
    public static final String DELISTED_CATEGORY_ACCOUNT_CODE="ACCOUNT";
    public static final String CREDIT_TXN_TYPE="CREDIT";
    public static final String DEBIT_TXN_TYPE="DEBIT";
    
//     public static final String NDB_BANK_CODE="NDB_BANKCODE";
     public static final String NDB_BANK_CODE_CEFT="NDB_BANKCODECEFT";
     public static final String NDB_BANK_CODE_SLIP="NDB_BANKCODESLIP";
     
    public static final String CUSTOMER_LOGIN_STATUS_YES = "Yes";
    public static final String CUSTOMER_LOGIN_STATUS_NO = "No";
    
    public static final String COMMON_PARAMETER_TYPE_IT = "IT"; 
    public static final String COMMON_PARAMETER_TYPE_BUSINESS = "BUSINESS"; 
     //--- AD config -----
    public static String PROPERTY_FILE_FOLDER_PATH = "PROPERTY_FILE_FOLDER_PATH";
    public static String INITIAL_CONTEXT_FACTORY = "INITIAL_CONTEXT_FACTORY";
    public static String SECURITY_AUTHENTICATION = "SECURITY_AUTHENTICATION";
    public static String PROVIDER_URL = "PROVIDER_URL";
    public static String SECURITY_PRINCIPAL = "SECURITY_PRINCIPAL";
    public static String SECURITY_CREDENTIALS = "SECURITY_CREDENTIALS";
    
    
    public static final String CUSTOMER_PEND_STATUS_DES = "Pending";
    public static final String CUSTOMER_PEND_STATUS = "PENC";
    
    public static final String CUSTOMER_DEF_TYPE_ACC = "1";
    public static final String CUSTOMER_DEF_TYPE_CARD = "2";
    
    
    public static final String SEND_MESSAGE_TO_USER_SEGMENT = "3";
    public static final String SEND_MESSAGE_TO_USER_INDIVIDUAL = "2";
    public static final String SEND_MESSAGE_TO_USER_ALL = "1";
}
