/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.varlist;

/**
 * @author chanuka
 */
public class TaskVarList {

    public static final String ADD_TASK = "ADD";
    public static final String SEARCH_TASK = "SRCH";
    public static final String UPDATE_TASK = "UPDATE";
    public static final String DELETE_TASK = "DEL";
    public static final String VIEW_TASK = "VIEW";
    public static final String LOGIN_TASK = "LOGN";
    public static final String LOGOUT_TASK = "LGOT";
    public static final String ASSIGN_TASK = "ASGN";
    public static final String GENERATE_TASK = "GEN";
    public static final String CONFIRM_TASK = "CONF";
    public static final String REJECT_TASK = "REJT";
    public static final String SEND_TASK = "SEND";
    public static final String HOLD_TASK = "HOLD";
    public static final String PROCESS_TASK = "PRO";
    public static final String RESTART_TASK = "REST";
    public static final String PIN_RESET_TASK = "PRES";
    public static final String DOWNLOAD_TASK = "DWLD";
    public static final String UPLOAD_TASK = "UPLD";
    public static final String PAS_RESET_TASK = "PSRS";
    public static final String GENERATE_USER_PASS = "GNUP";
    public static final String PEND_TASK = "PEND";
    public static final String MIN_AMOUNT_UPDATE_TASK = "MAUP";
    public static final String BLOCK_CUS_ACCOUNT_TASK = "BLOCK";
    public static final String CUS_BLOCK_APPLICATIONS = "BLOCKA";
    public static final String CUS_ACCOUNTS_UPDATE_TASK = "CAUP";
    public static final String CUS_ACCOUNTS_MANUAL_UPDATE_TASK = "CAUM";
    public static final String VIEW_CUSTOMER_TASK = "CVIEW";
    public static final String VIEW_PENDING_TASK = "PVIEW";
    public static final String CUSTOMER_UPDATE_TASK= "UCUS";
    public static final String CUSTOMER_PAS_RESET_TASK= "CUPR";
    public static final String CUSTOMER_DATA_UPDATE_TASK= "CUUC";
    
    public static final String CUSTOMER_STATUS_CHANGE_TASK = "CUSC";
    public static final String CUSTOMER_ATTEMPTS_RESET = "ATMR";
    public static final String CUSTOMER_LIMIT_CHANGE = "CULC";
    public static final String ADD_CUSTOMER_LIMIT = "CUAL";
    public static final String UPDATE_CUSTOMER_LIMIT = "CUEL";
    public static final String DELETE_CUSTOMER_LIMIT = "CUDL";
    public static final String UPDATE_DEFAULT_IMG = "UPDI";
    public static final String CUS_PRIMARY_ACCOUNT_UPDATE_TASK = "UCPA";
    public static final String CON_FEE_REPORT_GEN_TASK = "COFG";
    public static final String REPLY_TASK = "REPL";
    public static final String STATUS_UPDATE_TASK = "UPST";

}
