/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.varlist;

/**
 *
 * @author chanuka
 */
public class SessionVarlist {

    //session object fo the application
    public static final String SESSION_OBJ = "SessionObject";
    //Users bean  
    public static final String SYSTEMUSER = "SYSTEMUSER";
    //Users bean  
    public static final String SECTIONPAGELIST = "SECTIONPAGELIST";
    //----for context parameter--------------
    public static final String USERMAP = "USERMAP";//HashMap<String,String>
    //HashMap<String,List<Task>>
    public static final String TASKMAP = "TASKMAP";
    //reset login message
    public static final String CURRENTPAGE = "CURRENTPAGE";
    public static final String CURRENTSECTION = "CURRENTSECTION";
    public static final String TXNSEARCHBEAN = "TXNSEARCHBEAN";
    public static final String TXNDETAILMERCHANTWISEBEAN = "TXNDETAILMERCHANTWISEBEAN";
    public static final String DSWISETXNBEAN = "DSWISETXNBEAN";
    public static final String IPGWISETXNBEAN = "IPGWISETXNBEAN";
    public static final String DSERRORBEAN = "DSERRORBEAN";
    public static final String NOOFTXNBEAN = "NOOFTXNBEAN";
    public static final String ACCOUNTBEAN = "ACCOUNTBEAN";
    public static final String CARDBEAN = "CARDBEAN";
    public static final String MOBILEBEAN = "MOBILEBEAN";
    public static final String CUSTOMERID = "CUSTOMERID";
    public static final String GENERALINFO = "GENERALINFO";
    public static final String ACCBANKTYPE = "ACCBANKTYPE";
    public static final String CARDBANKTYPE = "CARDBANKTYPE";

    // search SMS outbox 
    public static final String SMS_OUTBOX_SEARCHBEAN = "SMSOUTBOXSEARCHBEAN";
    public static final String SMS_OUTBOX_BACKUP_SEARCHBEAN = "SMSOUTBOXBACKUPSEARCHBEAN";
    
    // search Reminder Alert
    public static final String REMINDER_ALERT_SEARCHBEAN = "REMINDERALERTSEARCHBEAN";
    
    // search Transaction Alert
    public static final String TRANSACTION_ALERT_SEARCHBEAN = "TRANSACTIONALERTSEARCHBEAN";

    // search SMS inbox 
    public static final String SMS_INBOX_SEARCHBEAN = "SMSINBOXSEARCHBEAN";

    // search Audit 
    public static final String AUDIT_SEARCHBEAN = "AUDITSEARCHBEAN";

    // search LoginHistory 
    public static final String LOGIN_HISTORY_SEARCHBEAN = "LOGINHISTORYSEARCHBEAN";

    // individual search audit
    public static final String IND_AUDIT_SEARCHBEAN = "INDAUDITSEARCHBEAN";

    //minimum period for change password
    public static final String MIN_PAS_CHANGE_PERIOD = "MINPASSWORDCHANGEPERIOD";
    public static final String ONLY_SHOW_ONTIME = "SHOWONETIME";

    // password expiry period    
    public static final String PAS_EXPIRY_PERIOD = "PASSWORDEXPIRYPERIOD";

    public static final String CUSTOMER_SUBSCRIPTION_MGT = "CUSTOMERSSUBSCRIPTIONMGT";

    public static final String CUSTOMER_SEARCHBEAN = "CUSTOMERSEARCHBEAN";

    public static final String TRANSACTION_REPORT = "TRANSACTIONREPORT";

    //for keep risk transaction profile details
    public static final String TXN_CURRENCY = "TXNCURRENCY";
    public static final String TERMINAL_ORI = "TERMINALORI";

//        Txn report
    public static final String TRANS_EXPOR_SEARCHBEAN = "TRANSEXPORSEARCHBEAN";
    public static final String TRANS_EXPOR_INDIVIDUAL_BEAN = "TRANSEXPORINDIVIDUALBEAN";

    // individual search customer wallet
    public static final String IND_CUSTOMER_SEARCHBEAN = "INDCUSTOMERSEARCHBEAN";

    // search Audit 
    public static final String CUSTOMER_WALLET_SEARCHBEAN = "CUSTOMERWALLETSEARCHBEAN";

    /////
    public static final String MOBILEUPDATESPATH = "MOBILEUPDATESPATH";

    public static final String ASSIGN_LIST = "ASSIGNLIST";

    public static final String PROMOTION_REPORT = "PROMOTIONREPORT";

    public static final String ACQUIRER_TXN_RISK_LIST = "ACQUIRERTXNRISKLIST";
    public static final String ACQUIRER_COMMISION_LIST = "ACQUIRERCOMMISIONLIST";
    public static final String ACQUIRER_COMMISION_VALIDATE_MSG = "ACQUIRERCOMMISIONVALIDATEMSG";
    //for login
    public static final String LAST_LOGGED_DATE = "LOGGEDDATE";
    public static final String CURRENT_DATE_TIME = "CURRENTDATE";

    public static final String ACQ_VALIDATE_MSG = "ACQVALIDATEMSG";
    public static final String CUS_WALLET_MGT_SEARCHBEAN = "CUSWALLETMGTSEARCHBEAN";
    //

    public static final String RISK_PRO_VALIDATE_MSG = "RISKPROVALIDATEMSG";
    public static final String RISK_PROFILE_LIST = "RISKPROFILELIST";

    // search customer search
    public static final String CUSTOMERSEARCH_SEARCHBEAN = "CUSTOMERSEARCHSEARCHBEAN";

    public static final String ACQUIRER_PROMOTION_LIST = "ACQUIRERPROMOTIONLIST";
    public static final String ACQUIRER_PROMOTION_VALIDATE_MSG = "ACQUIRERPROMOTIONVALIDATEMSG";

    //for wallet mgt change limits
    public static final String WALLET_MGT_TXN_LIMITS_VALIDATE_MSG = "WALLETMGTTXNLIMITSVALIDATEMSG";
    public static final String WALLET_MGT_TXN_LIMITS_LIST = "WALLETMGTTXNLIMITSLIST";

    public static final String FILE_UPLOAD_BEAN = "FILEUPLOADBEAN";
    //List<Page>
    public static final String PAGELIST = "PAGELIST";
    //List<Section>
    public static final String SECTIONLIST = "SECTIONLIST";

     public static final String SMS_EXCLUDED_DETAIL_BEAN="SMSEXCLUDEDDETAILBEAN";
     
    public static final String CUSTOMER_SUBSCRIPTION_DETAIL ="CUSTOMERSUBSCRIPTIONDETAIL";
    public static final String CUSTOMER_SEARCH_ACCOUNT_DETAIL ="CUSTOMERSEARCHACCOUNTDETAIL";
    public static final String CUSTOMER_SEARCH_INPUT_BEAN="CUSTOMERSEARCHINPUTBEAN";
    public static final String CUSTOMER_SEARCH_ACC_CARD_LIST="CUSTOMERSEARCHACCCARDLIST";
    public static final String CUSTOMER_SEARCH_CUSTOMER_DATA="CUSTOMERSEARCHCUSTOMERDATA";
    
    public static final String DUALAUTH_APPROVE_PRIVILEGES = "DUALAUTHAPPROVEPRIVILEGES";
    public static final String DUALAUTH_REJECT_PRIVILEGES = "DUALAUTHREJECTPRIVILEGES";
    
    public static final String ACCOUNT_CUSTOMER_ID = "ACCOUNTCUSTOMERID";
    public static final String CUSTOMER_ACCOUNT_MOBILE_LIST = "CUSTOMERACCOUNTMOBILELIST";
    public static final String CUSTOMER_ACCOUNT_MOBILE_VALIDATE_MSG = "CUSTOMERACCOUNTMOBILEVALIDATEMSG";
    
    // promotion
    public static final String PROMOTION_SEARCH_BEAN = "PROMOTION_SEARCH_BEAN";
    // mbanklocator
    public static final String M_BANK_LOCATOR_SEARCH_BEAN = "M_BANK_LOCATOR_SEARCH_BEAN";
    // otherbranch
    public static final String OTHER_BRANCH_SEARCH_BEAN = "OTHER_BRANCH_SEARCH_BEAN";
    // cardcenter
    public static final String CARD_CENTER_SEARCH_BEAN = "CARD_CENTER_SEARCH_BEAN";
    // producttype
    public static final String PRODUCT_TYPE_SEARCH_BEAN = "PRODUCT_TYPE_SEARCH_BEAN";
    // transactionlimit
    public static final String TRANSACTION_LIMIT_SEARCH_BEAN = "TRANSACTION_LIMIT_SEARCH_BEAN";
    // productmatrix
    public static final String PRODUCT_MATRIX_SEARCH_BEAN = "PRODUCT_MATRIX_SEARCH_BEAN";
    // productmatrix
    public static final String BILL_SERVICE_PROVIDER_SEARCH_BEAN = "BILL_SERVICE_PROVIDER_SEARCH_BEAN";
    
     // search customer search
    public static final String TRANSACTION_SEARCHBEAN = "TRANSACTIONSEARCHBEAN";
    
     // search customer search
    public static final String SCHEDULEDPAYMENT_EXPLOERE_SEARCHBEAN = "SCHEDULEDPAYMENTEXPLOERESEARCHBEAN";
    
    // Delisted Contract CID
    public static final String DELISTED_CONTRACT_CID = "DELISTEDCONTRACTCID";
    
     // search customer search
    public static final String SCHEDULEDPAYMENT_SEARCHBEAN = "SCHEDULEDPAYMENTSEARCHBEAN";
    public static final String SCHEDULEDPAYMENTHISTORY_SEARCHBEAN = "SCHEDULEDPAYMENTHISTORYSEARCHBEAN";
    
     // search Convenience fee search
    public static final String CONVENIENCE_FEE_SEARCHBEAN = "CONVENIENCEFEESEARCHBEAN";
    
     // search Payee Explorer search
    public static final String PAYEE_EXPLORER_SEARCHBEAN = "PAYEEEXPLORERSEARCHBEAN";
    
     // search Payee Explorer search
    public static final String PAY_TO_MOBILE_SEARCHBEAN = "PAYTOMOBILESEARCHBEAN";
    
     // search Biller search
    public static final String BILLER_SEARCHBEAN = "BILLERSEARCHBEAN";
    
    // individual search InboxMessage
    public static final String IND_INBOX_SEARCHBEAN = "INDINBOXSEARCHBEAN";
    
    // individual search send message
    public static final String SEND_MESSAGE_USERLIST = "SENDMESSAGEUSERLIST";
    public static final String SEND_MESSAGE_IND_BEAN = "SENDMESSAGEINDBEAN";
    
    // email management search
    public static final String EMAILMANAGEMENT_SEARCHBEAN = "EMAILMANAGEMENTSEARCHBEAN";
    
    // send mail management
    public static final String SENDMAILMANAGEMENT_SEARCHBEAN = "SENDMAILMANAGEMENTSEARCHBEAN";

}
