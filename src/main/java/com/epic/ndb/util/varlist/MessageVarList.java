/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.util.varlist;

/**
 * @author chanuka
 */
public class MessageVarList {

    //common messages...................
    public static final String COMMON_ERROR_PROCESS = "Error occurred while processing";
    public static final String COMMON_ERROR_LOADING = "Error occurred while loading";
    public static final String COMMON_NOT_EXISTS = "Record does not exists";
    public static final String COMMON_PENDING_NOT_EXISTS = "Pending record does not exists";
    public static final String COMMON_AVAILABLE_MERCHANT = "Merchant ID not exists";
    public static final String COMMON_NOT_AVAILABLE_MERCHANT = "Merchant ID already exists";
    public static final String COMMON_AVAILABLE_TERMINAL = "Terminal ID not exists";
    public static final String COMMON_NOT_AVAILABLE_TERMINAL = "Terminal ID already exists";
    public static final String COMMON_NOT_DELETE = "Record cannot be deleted";
    public static final String COMMON_ALREADY_EXISTS = "Record already exists";
    public static final String COMMON_ALREADY_IN_USE = "Record already in use";
    public static final String COMMON_SUCC_INSERT = "created successfully";
    public static final String COMMON_SUCC_UPDATE = "updated successfully";
    public static final String COMMON_SUCC_UPLOAD = "uploaded successfully";
    public static final String COMMON_PENDING_AVAILABLE = "Pending available";
    public static final String COMMON_MSG_AUTHORIZATION_PENDING = " : Authorization pending";
    public static final String COMMON_APPROVED_SUCCESS = "Requested operation approved successfully";
    public static final String COMMON_REJECTED_SUCCESS = "Requested operation rejected successfully";
    public static final String COMMON_REMARK_COMMENT = "Comment cannot be empty";
    public static final String COMMON_EMPTY_REMARK = "Remark cannot be empty";
    public static final String COMMON_INACTIVE_RECORD_DETAIL_UPDATE = "Inactive record detail cannot be update";

    public static final String COMMON_SUCC_REJECT = "rejected successfully";
    public static final String COMMON_SUCC_APPROVED = "approved successfully";
    public static final String COMMON_SUCC_HOLD = "status change to hold successfully";
    public static final String COMMON_ERROR_REJECT = "Error occurred while rejecting";
    public static final String COMMON_ERROR_APPROVED = "Error occurred while approving";
    public static final String COMMON_ERROR_PINRESET = "Error occurred while reseting pin";
    public static final String COMMON_ERROR_ATTEMPTRESET = "Error occurred while reseting attempt count";

    public static final String COMMON_ERROR_UPDATE = "Error occurred while updating";
    public static final String COMMON_SUCC_DELETE = "deleted successfully";

    public static final String COMMON_SUCC_CONFIRM = "confirmed successfully";
    public static final String COMMON_SUCC_SEND = "sent successfully";
    public static final String COMMON_ERROR_CONFIRM = "Error occurred while confirming";
    public static final String COMMON_ERROR_SEND = "Error occurred while sending";
    public static final String COMMON_ERROR_HOLD = "Error occurred while holding";
    public static final String ALREADY_CONFIRM = "Already confirmed";
    public static final String COMMON_ERROR_DELETE = "Error occurred while deleting";
    public static final String COMMON_SUCC_ASSIGN = "assigned successfully";
    public static final String COMMON_SUCC_ACTIVATE = "Activated successfully";
    public static final String COMMON_ERROR_ACTIVATE = "Error occurred while activating";
    public static final String COMMON_WARN_CHANGE_PAS = "Your password will expire ";

    public static final String COMMON_SUCCESS_CREAT_ADD_TASK_PENDING = "Successfully made a request to add ";
    public static final String COMMON_SUCCESS_CREAT_ASSIGN_TASK_PENDING = "Successfully made a request to assign ";
    public static final String COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING = "Successfully made a request to update ";
    public static final String COMMON_SUCCESS_CREAT_BLOCK_TASK_PENDING = "Successfully made a request to block ";
    public static final String COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING = "Successfully made a request to delete ";
    public static final String COMMON_SUCCESS_CREAT_SEND_TASK_PENDING = "Successfully made a request to send ";
    
    public static final String FILE_UPLOAD_SUCCESS = "File has been successfully uploaded";
    public static final String FILE_UPLOAD_ERROR = "An error occurred while uploading the file";

    //--------------------Login---------------//
    public static final String LOGIN_EMPTY_USERNAME = "Username or password cannot be empty";
    public static final String LOGIN_EMPTY_USERNAME_PAS = "Username and password cannot be empty";
    public static final String LOGIN_EMPTY_PAS = "Username or password cannot be empty";
    public static final String LOGIN_INVALID = "Your login attempt was not successful. Please try again.";
    public static final String LOGIN_ERROR_LOAD = "Cannot login. Please contact administrator";
    public static final String LOGIN_ERROR_INVALID = "Invalid username or password";
    public static final String LOGIN_DEACTIVE = "User has been deactivated. Please contact administrator";
    public static final String LOGIN_DEACTIVE_ROLE = "User Role has been deactivated. Please contact administrator";
    public static final String PASRESET_EMPTY_PAS = "Current password cannot be empty";
    public static final String PASRESET_EMPTY_NEW_PAS = "New password cannot be empty";
    public static final String PASRESET_EMPTY_COM_PAS = "Retype new password cannot be empty";
    public static final String PASRESET_MATCH_PAS = "Passwords mismatched.";
    public static final String PASRESET_INVALID_CURR_PAS = "Current password invalid";
    // --------------------Task Management---------------//
    public static final String TASK_MGT_EMPTY_TASK_CODE = "Task code cannot be empty";
    public static final String TASK_MGT_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String TASK_MGT_EMPTY_SORTKEY = "Sort key cannot be empty";
    public static final String TASK_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String TASK_MGT_ERROR_SORTKEY_INVALID = "Sort key invalid";
    public static final String TASK_MGT_ERROR_DESC_INVALID = "Description invalid";
    public static final String TASK_MGT_ERROR_TASKCODE_INVALID = "Task code invalid";
    public static final String TASK_MGT_SORTKEY_ALREADY_EXSISTS = "Sort key already exists";
    //--------------------- Password policy management-------------//
    public static final String PASSPOLICY_MINLEN_INVALID = "Minimum length should be equal or greater than ";
    public static final String PASSPOLICY_MAXLEN_INVALID = "Maximum length should not exceed ";
    public static final String PASSPOLICY_MINLEN_EMPTY = "Minimum length cannot be empty";
    public static final String PASSPOLICY_MAXLEN_EMPTY = "Maximum length cannot be empty";
    public static final String PASSPOLICY_MIN_MAX_LENGHT_DIFF = "Maximum length should be greater than the minimum length";
    public static final String PASSPOLICY_SPECCHARS_EMPTY = "Minimum special characters cannot be empty";
    public static final String PASSPOLICY_SPECCHARS_SHOULD_BE_LESS = "Minimum special characters should be less than ";
    public static final String PASSPOLICY_MINSPECCHARS_EMPTY = "Minimum special characters cannot be empty";
    public static final String PASSPOLICY_MINUPPER_EMPTY = "Minimum upper case characters cannot be empty";
    public static final String PASSPOLICY_MINNUM_EMPTY = "Minimum numerical characters cannot be empty";
    public static final String PASSPOLICY_MINLOWER_EMPTY = "Minimum lower case characters cannot be empty";
    public static final String PASSPOLICY_SUCCESS_ADD = "Password policy successfully added";
    public static final String PASSPOLICY_SUCCESS_DELETE = "Password policy successfully deleted";
    public static final String PASSPOLICY_SUCCESS_UPDATE = "Password policy successfully updated";
    public static final String PASSPOLICY_STATUS_EMPTY = "Select status";
    public static final String PASSPOLICY_POLICYID_EMPTY = "Password policy ID cannot be empty";
    public static final String PASSPOLICY_REPEATE_CHARACTERS_EMPTY = "Allowed repeat characters cannot be empty";
    public static final String PASSPOLICY_REPEATE_CHARACTERS_ZERO = "Allowed repeat characters should be greater than 0";
    public static final String PASSPOLICY_INIT_PAS_EXPIRY_STATUS_EMPTY = "Initial password expiry status cannot be empty";
    public static final String PASSPOLICY_PAS_EXPIRY_PERIOD_EMPTY = "Password expiry period cannot be empty";
    public static final String PASSPOLICY_NO_OF_HISTORY_PAS_EMPTY = "No. of history passwords cannot be empty";
    public static final String PASSPOLICY_MIN_PAS_CHANGE_PERIOD_EMPTY = "Password expiry notification period cannot be empty";
    public static final String PASSPOLICY_IDLE_ACCOUNT_EXPIRY_PERIOD_EMPTY = "Idle account expiry period cannot be empty";
    public static final String PASSPOLICY_IDLE_ACCOUNT_EXPIRY_PERIOD_INVALID = "Idle account expiry period should be 10 days or above";
    public static final String PASSPOLICY_PAS_EXPIRY_PERIOD_INVALID = "Password expiry period should be 10 days or above";
    public static final String PASSPOLICY_NO_OF_INVALID_LOGIN_ATTEMPTS_EMPTY = "No. of invalid login attempts cannot be empty";
    public static final String PASSPOLICY_NO_OF_HISTORY_PAS_INVALID = "No. of history passwords should be 1 or above";
    // --------------------User Role Management---------------//
    public static final String USER_ROLE_MGT_EMPTY_USER_ROLE_CODE = "User role code cannot be empty";
    public static final String USER_ROLE_MGT_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String USER_ROLE_MGT_EMPTY_USER_ROLE_LEVEL = "User role level cannot be empty";
    public static final String USER_ROLE_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String USER_ROLE_MGT_ERROR_USER_ROLE_LEVEL_INVALID = "User role level invalid";
    public static final String USER_ROLE_MGT_ERROR_DESC_INVALID = "Description invalid";
    public static final String USER_ROLE_MGT_ERROR_USER_ROLE_CODE_INVALID = "User role code invalid";
    // --------------------Section Management---------------//
    public static final String SECTION_CODE_EMPTY = "Section code cannot be empty";
    public static final String SECTION_DESC_EMPTY = "Description cannot be empty";
    public static final String SECTION_SORY_KEY_EMPTY = "Sort key cannot be empty";
    public static final String SECTION_STATUS_EMPTY = "Status can not be empty";
    public static final String SECTION_CODE_ALREADY_EXISTS = "Section code already exists";
    public static final String SECTION_SORT_KEY_ALREADY_EXISTS = "Sort key already exists";
    public static final String SECTION_SORT_KEY_INVALID = "Sort key invalid";
   // --------------------Transaction Type Management---------------//
    public static final String TXN_TYPE_MGT_EMPTY_TT_CODE = "Transfer ID cannot be empty";
    public static final String TXN_TYPE_MGT_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String TXN_TYPE_MGT_EMPTY_SORTKEY = "Sort key cannot be empty";
    public static final String TXN_TYPE_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String TXN_TYPE_MGT_ERROR_SORTKEY_INVALID = "Sort key invalid";
    public static final String TXN_TYPE_MGT_ERROR_DESC_INVALID = "Description invalid";
    public static final String TXN_TYPE_MGT_ERROR_TT_CODE_INVALID = "Transfer ID invalid";

    // --------------------Customer Categories Management---------------//
    public static final String CUS_CAT_MGT_EMPTY_CODE = "Code cannot be empty";
    public static final String CUS_CAT_MGT_EMPTY_DESCRIPTION = "Description cannot be empty";
   public static final String CUS_CAT_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String CUS_CAT_MGT_EMPTY_WAVE_OFF_STATUS = "Waive off status cannot be empty";
    public static final String CUS_CAT_MGT_EMPTY_CHARGE_AMOUNT = "Charge amount cannot be empty";
   public static final String CUS_CAT_MGT_ERROR_DESC_INVALID = "Description invalid";
    public static final String CUS_CAT_MGT_ERROR_CODE_INVALID = "Transfer ID invalid";
    public static final String CUS_CAT_MGT_INVALID_CHARGE_AMOUNT = "Invalid charge amount";

// --------------------Page Management---------------//
    public static final String PAGE_MGT_EMPTY_PAGE_CODE = "Page code cannot be empty";
    public static final String PAGE_MGT_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String PAGE_MGT_EMPTY_SORTKEY = "Sort key cannot be empty";
    public static final String PAGE_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String PAGE_MGT_EMPTY_URL = "URL cannot be empty";
    public static final String PAGE_MGT_ERROR_SORTKEY_INVALID = "Sort key invalid";
    public static final String PAGE_MGT_ERROR_DESC_INVALID = "Description invalid";
    public static final String PAGE_MGT_ERROR_URL_INVALID = "URL invalid";
    public static final String PAGE_MGT_ERROR_PAGE_CODE_INVALID = "Page code invalid";
    public static final String PAGE_MGT_ERROR_SORT_KEY_ALREADY_EXSITS = "Sort key already exists";
    //------------------- System User Management----------//
    public static final String SYSUSER_MGT_EMPTY_USERNAME = "Username cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_PAS = "Password cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_EPF = "EPF cannot be empty";
    public static final String SYSUSER_MGT_PAS_MISSMATCH = "Confirm password mismatch with the password";
    public static final String SYSUSER_MGT_EMPTY_USERROLE = "User role cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_DUALAUTHUSER = "Dual auth user cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_BRANCH = "Branch cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_SERVICEID = "Service ID cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_EXPIRYDATE = "Password expiry date cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_DATEOFBIRTH = "Date of birth cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_FULLNAME = "Full name cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_ADDRESS1 = "Address cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_ADDRESS2 = "Address2 cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_CITY = "City cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_COANTACTNO = "Contact number cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_FAX = "Fax cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_EMAIL = "Email cannot be empty";
    public static final String SYSUSER_MGT_EMPTY_NIC = "NIC cannot be empty";
    public static final String SYSUSER_MGT_INVALID_EMAIL = "Invalid email";
    public static final String SYSUSER_MGT_INVALID_NIC = "Invalid NIC";
    public static final String SYSUSER_MGT_INVALID_NIC_NO_SUCH_USER = "Such a user cannot be exist";
    public static final String SYSUSER_MGT_INVALID_NIC_DOB = "Date of birth does not match with NIC";
    public static final String SYSUSER_MGT_INVALID_CONTACT_NO = "Invalid contact number";
    public static final String SYSUSER_MGT_INVALID_CONTACT = "Invalid contact number.Contact number should contain 10 digits.(Ex.07xxxxxxxx)";
    public static final String SYSUSER_PAS_TOO_SHORT = "Password is shorter than the expected minimum length ";
    public static final String SYSUSER_PAS_TOO_LARGE = "Password is longer than the expected maximum length ";
    public static final String SYSUSER_PAS_LESS_LOWER_CASE_CHARACTERS = "Lower case characters are less than required ";
    public static final String SYSUSER_PAS_LESS_UPPER_CASE_CHARACTERS = "Upper case characters are less than required ";
    public static final String SYSUSER_PAS_MORE_CHAR_REPEAT = "Password contains characters repeating more than allowed ";
    public static final String SYSUSER_PAS_LESS_NUMERICAL_CHARACTERS = "Numerical characters are less than required ";
    public static final String SYSUSER_PAS_LESS_SPACIAL_CHARACTERS = "Special characters are less than required ";
    public static final String SYSUSER_PAS_MISSMATCH = "Passwords mismatched ";
    public static final String RESET_PAS_SUCCESS = "Password reset successful ";
    public static final String SYSUSER_DEL_INVALID = " is already Logged-In, cannot be deleted! ";
    public static final String RESET_PASS_NEW_EXIST = "New password already exists in history ";
    public static final String RESET_PASS_CURRENT_EXIST = "Current password already exists in history ";
    public static final String RESET_PASS_SAME_NEW_CURRENT = "New password and current password cannot be the same ";

    // --------------------User Role Privilege Management---------------//
    public static final String USER_ROLE_PRI_EMPTY_USER_ROLE = "User role cannot be empty";
    public static final String USER_ROLE_PRI_EMPTY_CATAGARY = "Please select one of the categories to proceed";
    public static final String USER_ROLE_PRI_INVALID_CATAGARY = "Categories should be sections or pages or operations";
    public static final String USER_ROLE_PRI_EMPTY_SECTION = "Section cannot be empty";
    public static final String USER_ROLE_PRI_EMPTY_PAGE = "Page cannot be empty";
    public static final String USER_ROLE_PRI_SEC_DEPEND = "Cannot delete the section because pages are already assigned to it";
    public static final String USER_ROLE_PRI_PAGE_DEPEND = "Cannot delete the page because tasks are already assigned to it";

   //-----------------------------------common configuration-----------------------------
    public static final String COMMON_CONFIG_EMPTY_ID = "ID cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_MAX_ATTEMPT = "Max attempt cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_LOGOUT_TIME = "Logout time cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_CURRENT_SERIES = "Current series cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_ENVIRONMENT = "Environment cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_POS_TIME_OUT = "POS time out cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_API_KEY = "API key cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_COUNTRY_CODE = "Country code cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_PIN_ACT_PERIOD_CODE = "Pin activation period cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_ENCRYPTION_KEY = "Encryption key cannot be empty";

    public static final String COMMON_CONFIG_INVALID_ID = "Id is invalid";
    public static final String COMMON_CONFIG_INVALID_MAX_ATTEMPT = "Max attempt is invalid";
    public static final String COMMON_CONFIG_INVALID_LOGOUT_TIME = "Logout time is invalid";
    public static final String COMMON_CONFIG_INVALID_CURRENT_SERIES = "Current series is invalid";
    public static final String COMMON_CONFIG_INVALID_PIN_ACT_PERIOD_CODE = "Pin activation period is invalid";
    public static final String COMMON_CONFIG_INVALID_POS_TIME_OUT = "POS time out is invalid";
    public static final String COMMON_CONFIG_INVALID_COUNTRY_CODE = "Country code is invalid";
    public static final String COMMON_CONFIG_INVALID_ENVIRONMENT = "Environment is invalid";

    // --------------------Term and Conditions---------------//
    public static final String TERMS_EMPTY_TERM_TYPE = "Terms & conditions type cannot be empty";
    public static final String TERMS_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String TERMS_EMPTY_DESCRIPTION_CHANGE_INACTIVE_STATE = "Inactive terms & conditions description can not be change";
    public static final String TERMS_EMPTY_STATUS = "Status cannot be empty";
    public static final String TERMS_EMPTY_VERSION = "Version number cannot be empty";
    public static final String TERMS_VERSION_EXISTS = "Version number already exists";
    public static final String TERMS_EMPTY_CATEGORY = "Category cannot be empty";

//    // ----------------------- Common Configuration --------------------------------------------------
    public static final String COMMON_CONFIG_EMPTY_PARAM_CODE = "Parameter code cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String COMMON_CONFIG_EMPTY_PARAM_VAL = "Parameter value cannot be empty";
    public static final String COMMON_CONFIG_INVALID_PARAM_CODE = "Invalid parameter code";
    public static final String COMMON_CONFIG_INVALID_DESCRIPTION = "Invalid Description";

//    // ----------------------- Common Configuration --------------------------------------------------
    public static final String BUSINESS_CONFIG_EMPTY_PARAM_CODE = "Parameter code cannot be empty";
    public static final String BUSINESS_CONFIG_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String BUSINESS_CONFIG_EMPTY_PARAM_VAL = "Parameter value cannot be empty";
    public static final String BUSINESS_CONFIG_INVALID_PARAM_CODE = "Invalid parameter code";
    public static final String BUSINESS_CONFIG_INVALID_DESCRIPTION = "Invalid Description";
    // ----------------------- Fee Configuration --------------------------------------------------
    public static final String FEE_CONFIG_EMPTY_PARAM_CODE = "Parameter code cannot be empty";
    public static final String FEE_CONFIG_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String FEE_CONFIG_EMPTY_PARAM_VAL = "Parameter value cannot be empty";
    public static final String FEE_CONFIG_INVALID_PARAM_CODE = "Invalid parameter code";
    public static final String FEE_CONFIG_INVALID_DESCRIPTION = "Invalid Description";

    // ----------------------- Branch Management --------------------------------------------------
    public static final String BRANCH_MGT_EMPTY_BRANCH_CODE = "Branch code cannot be empty";
    public static final String BRANCH_MGT_EMPTY_BRANCH_NAME = "Branch name cannot be empty";
    public static final String BRANCH_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String BRANCH_MGT_INVALID_BRANCH_CODE = "Invalid Branch code";
    public static final String BRANCH_MGT_INVALID_BRANCH_NAME = "Invalid Branch name";
    public static final String BRANCH_MGT_EMPTY_MANAGER_NAME = "Manager name cannot be empty";
    public static final String BRANCH_MGT_EMPTY_MOBILE = "Mobile number cannot be empty";
    public static final String BRANCH_MGT_INVALID_MANAGER_NAME = "Invalid manager name";
    public static final String BRANCH_MGT_INVALID_MOBILE = "Invalid mobile number";

    // ------------------------ Customer Search ---------------//
    public static final String CUSTOMER_SEARCH_EMPTY_CUSTOMER_ID = "Customer ID cannot be empty";
    public static final String CUSTOMER_SEARCH_EMPTY_WAVE_OFF_STATUS = "Waive off status cannot be empty";
    public static final String CUSTOMER_SEARCH_EMPTY_STATUS = "Status cannot be empty";
    public static final String CUSTOMER_SEARCH_EMPTY_MOB_STATUS = "Mobile Banking status cannot be empty";
    public static final String CUSTOMER_SEARCH_EMPTY_IB_STATUS  =  "Internet Banking status cannot be empty";
    public static final String CUSTOMER_SEARCH_EMPTY_PRIMARY_ACCOUNT = "Primary account cannot be empty";
    public static final String CUSTOMER_SEARCH_EMPTY_PRIMARY_TYTPE = "Primary type cannot be empty";
    public static final String CUSTOMER_SEARCH_EMPTY_CIF = "CID cannot be empty";
    public static final String CUSTOMER_SEARCH_EMPTY_CHARGE_AMOUNT = "Charge amount cannot be empty";
    public static final String CUSTOMER_SEARCH_INVALID_CHARGE_AMOUNT = "Invalid charge amount";
   
    public static final String CUSTOMER_LIMIT_CIF_EMPTY = "Customer ID/CID cannot be empty";
    public static final String CUSTOMER_LIMIT_TRANSACTIONTYPE_EMPTY = "Transaction type cannot be empty";
    public static final String CUSTOMER_LIMIT_USERLIMIT_EMPTY = "User limit cannot be empty";
    public static final String CUSTOMER_LIMIT_STATUS_EMPTY = "Status cannot be empty";
    public static final String CUSTOMER_LIMIT_USERLIMIT_INVALID = "User limit should be less than or equal max value";
    public static final String CUSTOMER_LIMIT_USERLIMIT_INVALID_VAL = "Invalid user limit";
    public static final String CUSTOMER_SEARCH_RECORD_NOCHANGE = "Live record not change";
   
    public static final String CUSTOMER_SEARCH_DEVICE_STATUS_EMPTY = "Device status cannot be empty";
    
    // ------------------------Segment Management ---------------//
    public static final String SEGMENT_MGT_EMPTY_CODE = "Segment code cannot be empty";
    public static final String SEGMENT_MGT_EMPTY_DESCRIPTIION = "Description cannot be empty";
    public static final String SEGMENT_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String SEGMENT_MGT_EMPTY_WAVE_OFF_STATUS = "Waive off status cannot be empty";
    public static final String SEGMENT_MGT_EMPTY_CHARGE_AMOUNT = "Charge amount cannot be empty";
    public static final String SEGMENT_MGT_INVALID_CODE = "Invalid segment code";
    public static final String SEGMENT_MGT_INVALID_DESCRIPTIION = "Invalid description";
    public static final String SEGMENT_MGT_INVALID_CHARGE_AMOUNT = "Invalid charge amount";
    public static final String LARGE_DATE_RANGE = "Date range should not exceed ";

    // ----------------------- Promotion Management --------------------------------------------------
    public static final String PROMOTION_MGT_EMPTY_PROMOID = "Promotion ID be empty";
    public static final String PROMOTION_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_PROMOTIONCARD = "Subregion code cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_SUBREGIONCODE = "Subregion code cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_PROMOTIONCATEGORY = "Promotions category cannot be empty";
    public static final String PROMOTION_MGT_INVALID_PROMOTIONCATEGORY = "Invalid promotions category";
    public static final String PROMOTION_MGT_EMPTY_SUBREGIONNAME = "Subregion name cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_SUBREGIONADDRESS = "Subregion address cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_FREETEXT = "Free text cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_INFO = "Information cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_LONGITUDE = "Longitude cannot be empty";
    public static final String PROMOTION_MGT_INVALID_LONGITUDE = "Invalid longitude ";
    public static final String PROMOTION_MGT_EMPTY_LATITUDE = "Latitude cannot be empty";
    public static final String PROMOTION_MGT_INVALID_LATITUDE = "Invalid latitude ";
    public static final String PROMOTION_MGT_EMPTY_RADIOUS = "Radious cannot be empty";
    public static final String PROMOTION_MGT_INVALID_RADIOUS = "Invalid radious";
    public static final String PROMOTION_MGT_EMPTY_PROMOCONDITION = "Promotion conditions cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_PHONENO = "Phone number cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_MERCHANTWEBSITE = "Merchant website cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_IMAGE = "Image cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_REGIONCODE = "Region code cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_REGIONNAME = "Region name cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_MASTERREGIONCODE = "Master region code cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_MASTERREGIONNAME = "Master region name cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_CARDTYPE = "Card type cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_CARDIMAGE = "Card image cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_STARTDATE = "Start Date cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_ENDDATE = "End Date cannot be empty";
    public static final String PROMOTION_MGT_EMPTY_PUSH_NOTIFICATION_ENABLE = "Push Notification Enable cannot be empty";
    public static final String PROMOTION_MOBILE_IMAGE_URL_EMPTY = "Mobile image url cannot be empty";
    public static final String PROMOTION_MOBILE_DEFAULT_IMAGE_EMPTY = "Default mobile image cannot be empty";
    public static final String PROMOTION_MOBILE_IMAGE_FIREBASE = "Upload the selected image to firebase";
    public static final String PROMOTION_MGT_INVALID_SUBREGIONCODE = "Invalid subregion code";
    public static final String PROMOTION_MGT_INVALID_SUBREGIONCODE_LENGTH = "Subregion code maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_SUBREGIONNAME_LENGTH = "Subregion name maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_SUBREGIONADDRESS_LENGTH = "Subregion address maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_FREETEXT_LENGTH = "Free text maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_INFO_LENGTH = "Information maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_TRADINGHOURS_LENGTH = "Trading hours maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_LONGITUDE_LENGTH = "Longitude maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_LATITUDE_LENGTH = "Latitude maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_RADIOUS_LENGTH = "Radious maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_PROMOCONDITION_LENGTH = "Promotion conditions maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_PHONENO_LENGTH = "Phone number maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_MERCHANTWEBSITE_LENGTH = "Merchant website maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_IMAGE_LENGTH = "Image maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_REGIONCODE_LENGTH = "Region code maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_REGIONNAME_LENGTH = "Region name maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_MASTERREGIONCODE_LENGTH = "Master region code maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_MASTERREGIONNAME_LENGTH = "Master region name maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_CARDTYPE_LENGTH = "Card type maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_CARDIMAGE_LENGTH = "Card image maximum length should not exceed ";
    public static final String PROMOTION_MGT_INVALID_STARTDATE = "Invalid start date format";
    public static final String PROMOTION_MGT_INVALID_ENDDATE = "Invalid end date format";
    public static final String PROMOTION_MGT_INVALID_DATERANGE = "Invalid date range";
    public static final String PROMOTION_MGT_INVALID_PUSH_NOTIFICATION_ENABLE = "Invalid push notification enable";

    // ----------------------- Mobile Bank locater --------------------------------------------------
    public static final String M_BANK_LOCATOR_EMPTY_LOCATORID = "Locator ID be empty";
    public static final String M_BANK_LOCATOR_EMPTY_LOCATORCODE = "Locator code be empty";
    public static final String M_BANK_LOCATOR_EMPTY_STATUS = "Status cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_CHANNELTYPE = "Channel type cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_CHANNELSUBTYPE = "Channel subtype cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_COUNTRYCODE = "Country code cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_COUNTRY = "Country cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_MASTERREGION = "Master region cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_RERION = "Region cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_SUBREGION = "Subregion cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_NAME = "Name cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_ADDRESS = "Address cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_POSTALCODE = "Postal code cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_CONTACTS = "Contacts cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_OPENING_HS_MON_FRI = "Opening hours from  mon to fri cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_OPENING_HS_SAT = "Opening hours at sat cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_OPENING_HS_SUN = "Opening hours at sun cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_GEOCODE = "Geocode cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_LOCATIONTAG = "Location tag cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_LANGUAGE = "Language cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_LONGITUDE = "Longitude cannot be empty";
    public static final String M_BANK_LOCATOR_EMPTY_LATITUDE = "Latitude cannot be empty";
    public static final String M_BANK_LOCATOR_INVALID_LATITUDE = "Invalid latitude";
    public static final String M_BANK_LOCATOR_INVALID_LONGITUDE = "Invalid longitude";
    public static final String M_BANK_LOCATOR_INVALID_LOCATORCODE = "Invalid locator code";
    public static final String M_BANK_LOCATOR_INVALID_COUNTRYCODE = "Invalid country code";
    public static final String M_BANK_LOCATOR_INVALID_COUNTRY = "Invalid country";
    public static final String M_BANK_LOCATOR_INVALID_MASTERREGION = "Invalid master region";
    public static final String M_BANK_LOCATOR_INVALID_SUBREGION = "Invalid subregion";
    public static final String M_BANK_LOCATOR_INVALID_NAME = "Invalid name";
    public static final String M_BANK_LOCATOR_INVALID_LANGUAGE = "Invalid language";
    public static final String M_BANK_LOCATOR_INVALID_POSTALCODE = "Invalid postal code";
    public static final String M_BANK_LOCATOR_INVALID_CONTACTS = "Invalid contacts";
    public static final String M_BANK_LOCATOR_INVALID_FAX = "Invalid Fax";

    public static final String M_BANK_LOCATOR_INVALID_CHANNELTYPE = "Invalid channel type";
    public static final String M_BANK_LOCATOR_INVALID_LOCATORCODE_LENGTH = "Locator code maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_CHANNELTYPE_LENGTH = "Channel type maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_COUNTRYCODE_LENGTH = "Country code maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_COUNTRY_LENGTH = "Country maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_MASTERREGION_LENGTH = "Master region maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_SUBREGION_LENGTH = "Subregion maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_NAME_LENGTH = "Name maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_ADDRESS_LENGTH = "Address maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_POSTALCODE_LENGTH = "Postal code maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_CONTACTS_LENGTH = "Contacts maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_FAX_LENGTH = "Fax maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_OPENING_HS_MON_FRI_LENGTH = "Opening hours Mon to Fri maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_OPENING_HS_SAT_LENGTH = "Opening hours at Sat maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_OPENING_HS_SUN_LENGTH = "Opening hours at Sun maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_HOLIDAY_BANKING_LENGTH = "Holiday banking maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_LONGITUDE_LENGTH = "Longitude maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_LATITUDE_LENGTH = "Latitude maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_LOCATIONTAG_LENGTH = "Location Tag maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_LANGUAGE_LENGTH = "Language maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_ATM_ON_LOCATION_LENGTH = "ATM on location maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_CRM_ON_LOCATION_LENGTH = "CRM on location maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_PAWNING_ON_LOCATION_LENGTH = "Pawning on location maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_SAFE_DEPOSIT_LOCKER_LENGTH = "Safe deposit lockers maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_LEASING_DESK_345_LENGTH = "Leasing desk 365 days maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_PRV_CENTRE_LENGTH = "PRV centre maximum length should not exceed ";
    public static final String M_BANK_LOCATOR_INVALID_BRANCHLESS_BANKING_LENGTH = "Branchless banking maximum length should not exceed ";

    // ----------------------- Other Bank --------------------------------------------------
    public static final String CARD_CENTER_EMPTY_BANK_CODE = "Bank code cannot be empty";
    public static final String CARD_CENTER_EMPTY_STATUS = "Status cannot be empty";
    public static final String CARD_CENTER_EMPTY_FUND_TRAN_MODE = "Fund tran mode cannot be empty";
    public static final String CARD_CENTER_EMPTY_SHORT_CODE = "Short code cannot be empty";
    public static final String CARD_CENTER_EMPTY_BANK_NAME = "Bank name cannot be empty";
    public static final String CARD_CENTER_EMPTY_CARD_CODE_CENTER = "Card code center cannot be empty";
    public static final String CARD_CENTER_INVALID_CARD_CODE_CENTER = "Invalid card code center";
    public static final String CARD_CENTER_INVALID_BANK_CODE = "Invalid bank code";
    public static final String CARD_CENTER_INVALID_BANK_NAME = "Invalid bank name";
    public static final String CARD_CENTER_INVALID_SHORT_CODE = "Invalid short code";
    public static final String CARD_CENTER_BANK_CODE_USE = "Record in use";
    // ----------------------- Other Branch --------------------------------------------------
    public static final String OTHER_BRANCH_EMPTY_BANK = "Bank cannot be empty";
    public static final String OTHER_BRANCH_EMPTY_BRANCH_CODE = "Branch code cannot be empty";
    public static final String OTHER_BRANCH_EMPTY_STATUS = "Status cannot be empty";
    public static final String OTHER_BRANCH_EMPTY_BRANCH_NAME = "Branch name cannot be empty";
    public static final String OTHER_BRANCH_INVALID_BANK = "Invalid bank";
    public static final String OTHER_BRANCH_INVALID_BRANCH_CODE = "Invalid branch code";
    public static final String OTHER_BRANCH_INVALID_BRANCH_NAME = "Invalid branch name";

    public static final String OTHER_BRANCH_INVALID_BANKCODE_LENGTH = "Bank code maximum length should not exceed ";
    public static final String OTHER_BRANCH_INVALID_BRANCHCODE_LENGTH = "Branch code maximum length should not exceed ";
    public static final String OTHER_BRANCH_INVALID_BRANCHNAME_LENGTH = "Branch name maximum length should not exceed ";

    // ----------------------- Transaction Limit --------------------------------------------------
    public static final String TRANSACTION_LIMIT_EMPTY_TRANSFER_TYPE = "Transfer type cannot be empty";
    public static final String TRANSACTION_LIMIT_EMPTY_SEGMENT_TYPE = "Segment type cannot be empty";
    public static final String TRANSACTION_LIMIT_EMPTY_STATUS = "Status cannot be empty";
    public static final String TRANSACTION_LIMIT_EMPTY_DEFAULT_LIMIT = "Default limit cannot be empty";
    public static final String TRANSACTION_LIMIT_EMPTY_TRAN_LIMIT_MIN = "Tran limit min cannot be empty";
    public static final String TRANSACTION_LIMIT_EMPTY_TRAN_LIMIT_MAX = "Tran limit max cannot be empty";
    public static final String TRANSACTION_LIMIT_EMPTY_DAILY_LIMIT_MIN = "Daily limit min cannot be empty";
    public static final String TRANSACTION_LIMIT_EMPTY_DAILY_LIMIT_MAX = "Daily limit max cannot be empty";
    public static final String TRANSACTION_LIMIT_INVALID_DEFAULT_LIMIT = "Invalid default limit";
    public static final String TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN = "Invalid tran limit min";
    public static final String TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MAX = "Invalid tran limit max";
    public static final String TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MIN = "Invalid daily limit min";
    public static final String TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MAX = "Invalid daily limit max";
    public static final String TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN_LESS_MAX = "Tran limit min should be less than or equal tran limit max";
    public static final String TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MIN_LESS_MAX = "Daily limit min should be less than or equal daily limit max";
    public static final String TRANSACTION_LIMIT_INVALID_DEF_LIMIT_LESS_DAILY_LIMIT_MAX = "Default limit should be less than or equal daily limit max";
    public static final String TRANSACTION_LIMIT_INVALID_DEF_LIMIT_GERTER_DAILY_LIMIT_MIN = "Default limit should be greater than or equal daily limit min";
    public static final String TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN_GERTER_DAILY_LIMIT_MIN = "Tran limit min should be greater than or equal daily limit min";
    public static final String TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MAX_LESS_DAILY_LIMIT_MAX = "Tran limit max should be less than or equal daily limit max";

    // ----------------------- Fees Charge --------------------------------------------------
    public static final String FEES_CHARGE_EMPTY_CHARGE_CODE = "Charge code cannot be empty";
    public static final String FEES_CHARGE_EMPTY_SHORT_NAME = "Short name cannot be empty";
    public static final String FEES_CHARGE_EMPTY_STATUS = "Status cannot be empty";
    public static final String FEES_CHARGE_EMPTY_DESCRIPTION = "Description cannot be empty";
    public static final String FEES_CHARGE_EMPTY_CHARGE_AMOUNT = "Charge amount cannot be empty";
    public static final String FEES_CHARGE_EMPTY_TRANSFER_TYPE = "Transfer type cannot be empty";
    public static final String FEES_CHARGE_INVALID_CHARGE_CODE = "Invalid charge code";
    public static final String FEES_CHARGE_INVALID_SHORT_NAME = "Invalid short name";
    public static final String FEES_CHARGE_INVALID_DESCRIPTION = "Invalid description";
    public static final String FEES_CHARGE_INVALID_CHARGE_AMOUNT = "Invalid charge amount";

    // ------------------------Biller Category Management ---------------//
    public static final String BILLER_CATEGORY_MGT_EMPTY_CODE = "Bill category code cannot be empty";
    public static final String BILLER_CATEGORY_MGT_EMPTY_NAME = "Bill category name cannot be empty";
    public static final String BILLER_CATEGORY_MGT_EMPTY_DESCRIPTIION = "Description cannot be empty";
    public static final String BILLER_CATEGORY_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String BILLER_CATEGORY_MGT_INVALID_CODE = "Invalid bill category code";
    public static final String BILLER_CATEGORY_MGT_INVALID_DESCRIPTIION = "Invalid description";
    public static final String BILLER_CATEGORY_MGT_INALID_DEACTIVE = "Bill category cannot be Inactive, Record already in use";

    // ------------------------Biller Service Provider Management ---------------//
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_ID = "Provider ID cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_NAME = "Provider Name cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_DESCRIPTIION = "Description cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_CATEGORY_CODE = "Category code cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_IMAGE = "Image cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_IMAGE_URL = "Image url cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_DISPLAY_NAME = "Display name cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_COLLECTION_ACCOUNT = "Collection account cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_BANK_CODE = "Bank code cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_PAY_TYPE = "Pay type cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_BILLER_TYPE_CODE = "Biller type code cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_STATUS = "Status cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_CURRENCY = "Currency cannot be empty";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_PRODUCT_TYPE = "Product type cannot be empty";
    public static final String BILLER_SERVICE_IMAGE_FIREBASE_UPLOAD = "Upload the selected image to firebase";
    public static final String BILLER_SERVICE_IMAGE_COLLECTION_ACCOUNT = "Collection account cannot be empty for NDB";
    public static final String BILLER_SERVICE_PROVIDER_EMPTY_MOB_PREFIX = "Mobile prefix cannot be empty";

    public static final String BILLER_SP_EMPTY_FIELD_TYPE = "Field type cannot be empty";
    public static final String BILLER_SP_EMPTY_FIELD_NAME = "Field name cannot be empty";
    public static final String BILLER_SP_EMPTY_FIELD_VAL = "Field value cannot be empty";
    public static final String BILLER_SP_EMPTY_FIELD_VALIDATION = "Field validation cannot be empty";
    public static final String BILLER_SP_EMPTY_FIELD_LEN = "Maximum field length cannot be empty";
    public static final String BILLER_SP_EMPTY_FIELD_LEN_MIN = "Minimum field length cannot be empty";
    public static final String BILLER_SP_INVALID_FIELD_LEN_MIN_LESS_MAX = "Field length min should be less than or equal Field length max";

    // ----------------------- Product Type --------------------------------------------------
    public static final String PRODUCT_TYPE_EMPTY_PRODUCT_TYPE = "Product type cannot be empty";
    public static final String PRODUCT_TYPE_EMPTY_STATUS = "Status cannot be empty";
    public static final String PRODUCT_TYPE_EMPTY_PRODUCT_CATEGORY = "Product category cannot be empty";
    public static final String PRODUCT_TYPE_EMPTY_PRODUCT_NAME = "Product name cannot be empty";
    public static final String PRODUCT_TYPE_INVALID_PRODUCT_TYPE = "Invalid product type";
    public static final String PRODUCT_TYPE_INVALID_PRODUCT_NAME = "Invalid product name";

    // ----------------------- Product Matrix --------------------------------------------------
    public static final String PRODUCT_MATRIX_EMPTY_DEBIT_PRODUCT_TYPE = "Debit product type cannot be empty";
    public static final String PRODUCT_MATRIX_INVALID_DEBIT_PRODUCT_TYPE = "Invalid debit product type";
    public static final String PRODUCT_MATRIX_EMPTY_STATUS = "Status cannot be empty";
    public static final String PRODUCT_MATRIX_EMPTY_CREDIT_PRODUCT_TYPE = "Credit product type cannot be empty";
    public static final String PRODUCT_MATRIX_INVALID_CREDIT_PRODUCT_TYPE = "Invalid credit product type";
    public static final String PRODUCT_MATRIX_EMPTY_DEBIT_CURRENCY = "Debit currency cannot be empty";
    public static final String PRODUCT_MATRIX_INVALID_DEBIT_CURRENCY = "Invalid debit currency";
    public static final String PRODUCT_MATRIX_EMPTY_CREDIT_CURRENCY = "Credit currency cannot be empty";
    public static final String PRODUCT_MATRIX_INVALID_CREDIT_CURRENCY = "Invalid credit currency";

    // ------------------------ Delisted Contract ----------------------------------------------
    public static final String DELISTED_CONTRACT_EMPTY_CID = "CID cannot be empty";
    public static final String DELISTED_CONTRACT_EMPTY_DELISTED_CATEGORY = "Delisted category cannot be empty";
    public static final String DELISTED_CONTRACT_EMPTY_CONTRACT_NUMBER = "Contract number cannot be empty";
    public static final String DELISTED_CONTRACT_INVALID_CID_LENGTH = "CID maximum length should not exceed ";
    public static final String DELISTED_CONTRACT_INVALID_CONTRACT_NUMBER_LENGTH = "Contract number maximum length should not exceed ";
    public static final String DELISTED_CONTRACT_INVALID_DELISTED_CATEGORY = "Invalid delisted category";

    // ------------------------FAQ Management --------------------------------------------------
    public static final String FAQ_MGT_EMPTY_ID = "ID cannot be empty";
    public static final String FAQ_MGT_EMPTY_QUESTION = "Question cannot be empty";
    public static final String FAQ_MGT_EMPTY_ANSWER = "Answer cannot be empty";
    public static final String FAQ_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String FAQ_MGT_INVALID_ID = "Invalid ID";

    
    // ------------------------Promotion Category Management --------------------------------------------------

    public static final String PROMOTION_CAT_EMPTY_CODE = "Code cannot be empty";
    public static final String PROMOTION_CAT_EMPTY_NAME = "Name cannot be empty";
    public static final String PROMOTION_CAT_EMPTY_STATUS = "Status cannot be empty";
    public static final String PROMOTION_CAT_INVALID_CODE = "Invalid Code";
    public static final String PROMOTION_CAT_INVALID_NAME = "Invalid Name";

    //---------------------------Send Message----------------//
    public static final String SEND_MESSAGE_NO_UPLOADED_FILE = "No file has been uploaded";
    public static final String SEND_MESSAGE_EMPTY_SERVICE = "Service cannot be empty";
    public static final String SEND_MESSAGE_EMPTY_SUBJECT = "Subject cannot be empty";
    public static final String SEND_MESSAGE_EMPTY_RECIPIENT = "Recipient cannot be empty";
    public static final String SEND_MESSAGE_EMPTY_SEGMENT = "Segment cannot be empty";
    public static final String SEND_MESSAGE_EMPTY_CID = "CID cannot be empty";
    public static final String SEND_MESSAGE_EMPTY_MESSAGE = "MessaMAIL_SUBJECT_MGT_EMPTY_CODEge cannot be empty";
    public static final String SEND_MESSAGE_NO_CUSTOMER_SEGMENT = "Customers not exist for selected segment(s)";

    //---------------------------Inbox Message----------------//
    
    public static final String INBOX_MESSAGE_EMPTY_MAIL_STATUS = "Mail status cannot be empty";
    
       // --------------------Mail Subject Management---------------//
    public static final String MAIL_SUBJECT_MGT_EMPTY_CODE = "ID cannot be empty";
    public static final String MAIL_SUBJECT_MGT_EMPTY_DESCRIPTION = "Description cannot be empty";  
    public static final String MAIL_SUBJECT_MGT_EMPTY_STATUS = "Status cannot be empty";
    public static final String MAIL_SUBJECT_MGT_ERROR_DESC_INVALID = "Description invalid";
    public static final String MAIL_SUBJECT_MGT_ERROR_TT_CODE_INVALID = "ID invalid";
}
