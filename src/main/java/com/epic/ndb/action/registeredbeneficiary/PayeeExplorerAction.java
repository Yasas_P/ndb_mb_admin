/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.registeredbeneficiary;

import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.bean.registeredbeneficiary.RegisteredBeneficiaryBean;
import com.epic.ndb.bean.registeredbeneficiary.RegisteredBeneficiaryInputBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.dao.registeredbeneficiary.PayeeExplorerDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class PayeeExplorerAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {
    RegisteredBeneficiaryInputBean inputBean = new RegisteredBeneficiaryInputBean();
    
    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;
    
    @Override
    public Object getModel() {
        return inputBean;
    }
    
    public String execute() {
        System.out.println("called PayeeExplorerAction : execute");
        return SUCCESS;
    }
    
    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.PAYEE_EXPLORER;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Search".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }
    
    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.PAYEE_EXPLORER, request);

        inputBean.setVgenerate(true);
        inputBean.setVsearch(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                }
            }
        }
    }
    
    public String view() {

        System.out.println("called PayeeExplorerAction :view");
        String result = "view";

        try {
            this.applyUserPrivileges();
            
            inputBean.setBeneficiaryTypeList(this.getBeneficiaryTypeList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception ex) {
            addActionError("Payee Explorer " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PayeeExplorerAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String list() {
        System.out.println("called PayeeExplorerAction: List");
        try {
            if (inputBean.isSearch()) {
                int rows = inputBean.getRows();
                int page = inputBean.getPage();
                int to = (rows * page);
                int from = to - rows;
                long records = 0;
                String orderBy = "";

                if (!inputBean.getSidx().isEmpty()) {
                    orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
                } else {
                    orderBy = " order by P.CRT_DATE DESC ";
                }

                PayeeExplorerDAO dao = new PayeeExplorerDAO();
                List<RegisteredBeneficiaryBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

                /**
                 * for search audit
                 */
                if (inputBean.isSearch() && from == 0) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String searchParameters = "["
                            + checkEmptyorNullString("From Date", inputBean.getFromDate())
                            + checkEmptyorNullString("To Date", inputBean.getToDate())
                            + checkEmptyorNullString("CID", inputBean.getCif())
                            + checkEmptyorNullString("Customer Category", inputBean.getCustomerCategory())
                            + checkEmptyorNullString("Beneficiary Type", inputBean.getBeneficiaryType())
                            + "]";

                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PAYEE_EXPLORER, SectionVarList.REGISTERED_BENEFICIARY, "Payee Explorer search using " + searchParameters + " parameters ", null);
                    SystemAuditDAO sysdao = new SystemAuditDAO();
                    sysdao.saveAudit(audit);
                }

                if (!dataList.isEmpty()) {
                    records = dataList.get(0).getFullCount();
                    inputBean.setRecords(records);
                    inputBean.setGridModel(dataList);
                    int total = (int) Math.ceil((double) records / (double) rows);
                    inputBean.setTotal(total);

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.PAYEE_EXPLORER_SEARCHBEAN, inputBean);
                } else {
                    inputBean.setRecords(0L);
                    inputBean.setTotal(0);
                }
            }

        } catch (Exception e) {
            Logger.getLogger(PayeeExplorerAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " payee explorer ");
        }
        return "list";
    }
    
    public String reportGenerate() {

        System.out.println("called PayeeExplorerAction : reportGenerate");
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                PayeeExplorerDAO dao = new PayeeExplorerDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    RegisteredBeneficiaryInputBean searchBean = (RegisteredBeneficiaryInputBean) session.getAttribute(SessionVarlist.PAYEE_EXPLORER_SEARCHBEAN);
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new RegisteredBeneficiaryInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Payee_Report.csv");
                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.PAYEE_EXPLORER, SectionVarList.REGISTERED_BENEFICIARY, "Payee Explorer csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger
                            .getLogger(PayeeExplorerAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PayeeExplorerAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Payee Explorer");

            return "message";
        }
        return retMsg;
    }
    
    private List<CommonKeyVal> getBeneficiaryTypeList() {

        List<CommonKeyVal> beneficiaryType = new ArrayList<CommonKeyVal>();

        CommonKeyVal beneficiaryType1 = new CommonKeyVal();
        beneficiaryType1.setKey("NDBBANKACC");
        beneficiaryType1.setValue("Other NDB Account");
        beneficiaryType.add(beneficiaryType1);

        CommonKeyVal beneficiaryType2 = new CommonKeyVal();
        beneficiaryType2.setKey("OTHERBANKACC");
        beneficiaryType2.setValue("Other Bank Account");
        beneficiaryType.add(beneficiaryType2);

        return beneficiaryType;

    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

}
