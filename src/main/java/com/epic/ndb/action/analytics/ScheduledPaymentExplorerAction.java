/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.analytics;

import com.epic.ndb.bean.analytics.ChannelTypeBean;
import com.epic.ndb.bean.analytics.ScheduledPaymentExplorerBean;
import com.epic.ndb.bean.analytics.ScheduledPaymentExplorerInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.dao.analytics.ScheduledPaymentExplorerDAO;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.PartialList;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class ScheduledPaymentExplorerAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {
    
    ScheduledPaymentExplorerInputBean inputBean = new ScheduledPaymentExplorerInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    Map parameterMap = new HashMap();
    InputStream fileInputStream = null;

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public Map getParameterMap() {
        return parameterMap;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called ScheduledPaymentExplorerAction : execute");
        return SUCCESS;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.SCHEDULED_PAYMENT_EXPLORER_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Search".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.SCHEDULED_PAYMENT_EXPLORER_PAGE, request);

        inputBean.setVgenerate(true);
        inputBean.setVsearch(true);
        //inputBean.setVgenerateview(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.VIEW_TASK)) {
                }
            }
        }
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setTxnTypeList(dao.getFilteredTxnTypeListByCode());
            inputBean.setChannelTypeList(this.getChannelTypeList());
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_SWTCH));
            inputBean.setResponseList(dao.getResponseCodeList());
            inputBean.setTxnStatusList(this.getTxnStatusList());
            inputBean.setStaffOrNotList(this.getStaffNonStaffList());
            inputBean.setCurrencyList(dao.getProductCurrencyList());
            inputBean.setBillerCategoryList(dao.getBillerCategoryList());
            inputBean.setBillServiceProviderList(dao.getBilServiceProviderList());

            System.out.println("called ScheduledPaymentExplorerAction :view");

        } catch (Exception ex) {
            addActionError("Scheduled payment explorer " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(ScheduledPaymentExplorerAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called ScheduledPaymentExplorerAction : list");
        try {
            if (inputBean.isSearch()) {

                int rows = inputBean.getRows();
                int page = inputBean.getPage();
                int to = (rows * page);
                int from = to - rows;
                long records = 0;
                String sortIndex = "";
                String sortOrder = "";

                List<ScheduledPaymentExplorerBean> dataList = null;

                if (!inputBean.getSidx().isEmpty()) {
                    sortIndex = inputBean.getSidx();
                    sortOrder = inputBean.getSord();
                }
                HttpServletRequest request = ServletActionContext.getRequest();
                ScheduledPaymentExplorerDAO dao = new ScheduledPaymentExplorerDAO();

                PartialList<ScheduledPaymentExplorerBean> searchList = dao.getSearchList(inputBean, rows, from, sortIndex, sortOrder);

                String searchParameters = "["
                        + checkEmptyorNullString("From Date", inputBean.getFromDate())
                        + checkEmptyorNullString("To Date", inputBean.getToDate())
                        + checkEmptyorNullString("Customer NIC", inputBean.getNic())
                        + checkEmptyorNullString("Customer CID", inputBean.getCif())
                        + checkEmptyorNullString("Transaction Type", inputBean.getTxnType())
                        + checkEmptyorNullString("Status ", inputBean.getStatus())
                        + checkEmptyorNullString("T24 Ref No ", inputBean.getTranRefNo())
                        + checkEmptyorNullString("IBL Ref No ", inputBean.getIblRefNo())
                        + "]";

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SCHEDULED_PAYMENT_EXPLORER_PAGE, SectionVarList.ANALYTICS, "Scheduled payment explorer search using " + searchParameters + " parameters ", null, null, null);
                CommonDAO.saveAudit(audit);

                dataList = searchList.getList();
                records = searchList.getFullCount();

                if (!dataList.isEmpty()) {
                    inputBean.setRecords(records);
                    inputBean.setGridModel(dataList);
                    int total = (int) Math.ceil((double) records / (double) rows);
                    inputBean.setTotal(total);

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.SCHEDULEDPAYMENT_EXPLOERE_SEARCHBEAN, inputBean);

                } else {
                    inputBean.setRecords(0L);
                    inputBean.setTotal(0);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ScheduledPaymentExplorerAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Scheduled payment explorer " + MessageVarList.COMMON_ERROR_PROCESS);
            return "message";
        }
        return "list";
    }

    public String reportGenerate() {

        System.out.println("called ScheduledPaymentExplorerAction : reportGenerate");

        String retMsg = "view";

        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                ScheduledPaymentExplorerDAO dao = new ScheduledPaymentExplorerDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;

                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    ScheduledPaymentExplorerInputBean searchBean = (ScheduledPaymentExplorerInputBean) session.getAttribute(SessionVarlist.SCHEDULEDPAYMENT_EXPLOERE_SEARCHBEAN);

                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new ScheduledPaymentExplorerInputBean());
                    }

                    try {

                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Scheduled_Payment_Explorer_Report.csv");
                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.SCHEDULED_PAYMENT_EXPLORER_PAGE, SectionVarList.ANALYTICS, "Scheduled payment explorer csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger
                            .getLogger(ScheduledPaymentExplorerAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ScheduledPaymentExplorerAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Scheduled payment explorer");

            return "message";
        } finally {
        }
        return retMsg;
    }

    private List<ChannelTypeBean> getChannelTypeList() {

        List<ChannelTypeBean> channelTypeList = new ArrayList<ChannelTypeBean>();

        ChannelTypeBean channeltype1 = new ChannelTypeBean();
        channeltype1.setKey("2");
        channeltype1.setValue("Internet Banking");
        channelTypeList.add(channeltype1);

        ChannelTypeBean channeltype2 = new ChannelTypeBean();
        channeltype2.setKey("1");
        channeltype2.setValue("Mobile Banking");
        channelTypeList.add(channeltype2);

        return channelTypeList;

    }

    private List<CommonKeyVal> getTxnStatusList() {

        List<CommonKeyVal> statusList = new ArrayList<CommonKeyVal>();

        CommonKeyVal status1 = new CommonKeyVal();
        status1.setKey("SUCC");
        status1.setValue("Success");
        statusList.add(status1);

        CommonKeyVal status2 = new CommonKeyVal();
        status2.setKey("FAIL");
        status2.setValue("Failure");
        statusList.add(status2);

        return statusList;

    }
    
    private List<CommonKeyVal> getStaffNonStaffList() {

        List<CommonKeyVal> statusList = new ArrayList<CommonKeyVal>();

        CommonKeyVal status1 = new CommonKeyVal();
        status1.setKey("STAFF");
        status1.setValue("Staff");
        statusList.add(status1);

        CommonKeyVal status2 = new CommonKeyVal();
        status2.setKey("NONSTAFF");
        status2.setValue("Non Staff");
        statusList.add(status2);

        return statusList;

    }
}
