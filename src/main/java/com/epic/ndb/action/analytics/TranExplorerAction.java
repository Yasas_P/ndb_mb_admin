package com.epic.ndb.action.analytics;

import com.epic.ndb.bean.analytics.ChannelTypeBean;
import com.epic.ndb.bean.analytics.TranExplorerBean;
import com.epic.ndb.bean.analytics.TranExplorerInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.dao.analytics.TranExplorerDAO;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.PartialList;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;

/**
 *
 * @author jayathissa_d
 */
public class TranExplorerAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    TranExplorerInputBean inputBean = new TranExplorerInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    Map parameterMap = new HashMap();
    InputStream fileInputStream = null;

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public Map getParameterMap() {
        return parameterMap;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called TranExplorerAction : execute");
        return SUCCESS;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.TRAN_EXPLORER;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Search".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.TRAN_EXPLORER, request);

        inputBean.setVgenerate(true);
        inputBean.setVsearch(true);
        //inputBean.setVgenerateview(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.VIEW_TASK)) {
                }
            }
        }
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setTxnTypeList(dao.getFilteredTxnTypeListByCode());
            inputBean.setChannelTypeList(this.getChannelTypeList());
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_SWTCH));
            inputBean.setResponseList(dao.getResponseCodeList());
            inputBean.setTxnStatusList(this.getTxnStatusList());
            inputBean.setStaffOrNotList(this.getStaffNonStaffList());

            System.out.println("called TranExplorerAction :view");

        } catch (Exception ex) {
            addActionError("Transaction explorer " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(TranExplorerAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called TranExplorerAction : list");
        try {
            if (inputBean.isSearch()) {

                int rows = inputBean.getRows();
                int page = inputBean.getPage();
                int to = (rows * page);
                int from = to - rows;
                long records = 0;
                String sortIndex = "";
                String sortOrder = "";

                List<TranExplorerBean> dataList = null;

//                String orderBy = "";
//                if (!inputBean.getSidx().isEmpty()) {
//                    orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
//                }
                if (!inputBean.getSidx().isEmpty()) {
                    sortIndex = inputBean.getSidx();
                    sortOrder = inputBean.getSord();
                }
                HttpServletRequest request = ServletActionContext.getRequest();
                TranExplorerDAO dao = new TranExplorerDAO();

                PartialList<TranExplorerBean> searchList = dao.getSearchList(inputBean, rows, from, sortIndex, sortOrder);
                //List<TranExplorerBean> searchList = dao.getSearchList(inputBean, rows, from,orderBy);

                String searchParameters = "["
                        + checkEmptyorNullString("From Date", inputBean.getFromDate())
                        + checkEmptyorNullString("To Date", inputBean.getToDate())
                        + checkEmptyorNullString("Customer NIC", inputBean.getNic())
                        + checkEmptyorNullString("Customer CID", inputBean.getCif())
                        + checkEmptyorNullString("Transaction Type", inputBean.getTxnType())
                        + checkEmptyorNullString("Status ", inputBean.getStatus())
                        + checkEmptyorNullString("T24 Ref No ", inputBean.getTranRefNo())
                        + checkEmptyorNullString("IBL Ref No ", inputBean.getIblRefNo())
                        + checkEmptyorNullString("From Account No ", inputBean.getFromAccNo())
                        + checkEmptyorNullString("To Account No ", inputBean.getToAccNo())
                        //                        + checkEmptyorNullString("Response ", inputBean.getResponseCode())
                        + "]";

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.TRAN_EXPLORER, SectionVarList.ANALYTICS, "Transaction explorer search using " + searchParameters + " parameters ", null, null, null);
                CommonDAO.saveAudit(audit);

                dataList = searchList.getList();
                records = searchList.getFullCount();

                if (!dataList.isEmpty()) {
                    inputBean.setRecords(records);
                    inputBean.setGridModel(dataList);
                    int total = (int) Math.ceil((double) records / (double) rows);
                    inputBean.setTotal(total);

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.TRANSACTION_SEARCHBEAN, inputBean);

                } else {
                    inputBean.setRecords(0L);
                    inputBean.setTotal(0);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TranExplorerAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Transaction explorer " + MessageVarList.COMMON_ERROR_PROCESS);
            return "message";
        }
        return "list";
    }

    public String reportGenerate() {

        System.out.println("called TransExplorerAction : reportGenerate");

        String retMsg = "view";
        InputStream inputStream = null;
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy 'at' HH:mm a");
        JRSwapFileVirtualizer virtualizer = null;
        JasperPrint jasperPrint = null;
        byte[] outputFile;
        Session hSession = null;

        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("exel")) {
                System.err.println("EXEL printing");
                TranExplorerDAO dao = new TranExplorerDAO();
                retMsg = "excelreport";
                ByteArrayOutputStream outputStream = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    TranExplorerInputBean searchBean = (TranExplorerInputBean) session.getAttribute(SessionVarlist.TRANSACTION_SEARCHBEAN);
//                    Audittrace audittrace = Common.makeAudittrace(request, TaskVarList.REPORT_TASK, PageVarList.EXCEPTIONS_RPT_PAGE, this.getSearchParam() + " excel report viewed", null);
//                    Object object = new Object();

                    Object object = dao.generateExcelReport(searchBean);
                    if (object instanceof SXSSFWorkbook) {
                        SXSSFWorkbook workbook = (SXSSFWorkbook) object;
                        outputStream = new ByteArrayOutputStream();
                        workbook.write(outputStream);
                        inputBean.setExcelStream(new ByteArrayInputStream(outputStream.toByteArray()));
                        setContentLength(outputStream.toByteArray().length);
                        System.out.println("------" + outputStream.toByteArray());
                    } else if (object instanceof ByteArrayOutputStream) {
                        outputStream = (ByteArrayOutputStream) object;
                        inputBean.setZipStream(new ByteArrayInputStream(outputStream.toByteArray()));
                        retMsg = "zip";
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.TRAN_EXPLORER, SectionVarList.ANALYTICS, "Transaction explorer excel report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
//                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail excel report");
                    Logger.getLogger(TranExplorerAction.class.getName()).log(Level.SEVERE, null, e);
//                    this.loadPageData();
                    retMsg = "view";
                    throw e;
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.flush();
                            outputStream.close();
                        }

                    } catch (IOException ex) {
                        //do nothing
                    }
                }
            } else if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                TranExplorerDAO dao = new TranExplorerDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;

                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    TranExplorerInputBean searchBean = (TranExplorerInputBean) session.getAttribute(SessionVarlist.TRANSACTION_SEARCHBEAN);

                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new TranExplorerInputBean());
                    }

                    try {

                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Transaction_Explorer_Report.csv");
                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.TRAN_EXPLORER, SectionVarList.ANALYTICS, "Transaction explorer csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger
                            .getLogger(TranExplorerAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            } else if (inputBean.getReporttype().trim().equalsIgnoreCase("summeay")) {
                try {
                    TranExplorerDAO txndao = new TranExplorerDAO();
                    CommonDAO dao = new CommonDAO();
                    cal.setTime(CommonDAO.getSystemDateLogin());
                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    TranExplorerInputBean searchBean = (TranExplorerInputBean) session.getAttribute(SessionVarlist.TRANSACTION_SEARCHBEAN);

                    //get path
                    ServletContext context = ServletActionContext.getServletContext();
                    String imgPath = context.getRealPath("/resouces/images/ndb_bank_logo.png");

                    if (searchBean.getNic() != null && !searchBean.getNic().isEmpty()) {
                        parameterMap.put("nic", searchBean.getNic().trim());
                    } else {
                        parameterMap.put("nic", "--");
                    }
                    if (searchBean.getCif() != null && !searchBean.getCif().isEmpty()) {
                        parameterMap.put("cif", searchBean.getCif());
                    } else {
                        parameterMap.put("cif", "--");
                    }
                    if (searchBean.getTxnType() != null && !searchBean.getTxnType().isEmpty()) {
                        String txnTypeDes = dao.getSwtTxnDesByCode(searchBean.getTxnType());
                        parameterMap.put("txnTypeDes", txnTypeDes);
                        parameterMap.put("txnType", searchBean.getTxnType());
                    } else {
                        parameterMap.put("txnTypeDes", "--");
                        parameterMap.put("txnType", "--");
                    }
//                    if (searchBean.getResponseCode() != null && !searchBean.getResponseCode().isEmpty()) {
//                        String responseCodeDes=dao.getResponseDesByCode(searchBean.getResponseCode());
//                        parameterMap.put("responseCode", searchBean.getResponseCode());
//                        parameterMap.put("responseCodeDes", responseCodeDes);
//                    } else {
//                        parameterMap.put("responseCode", "--");
//                        parameterMap.put("responseCodeDes", "--");
//                    }
                    if (searchBean.getTranRefNo() != null && !searchBean.getTranRefNo().isEmpty()) {
                        parameterMap.put("txnRefNo", searchBean.getTranRefNo());
                    } else {
                        parameterMap.put("txnRefNo", "--");
                    }
                    if (searchBean.getIblRefNo()!= null && !searchBean.getIblRefNo().isEmpty()) {
                        parameterMap.put("iblRefNo", searchBean.getIblRefNo());
                    } else {
                        parameterMap.put("iblRefNo", "--");
                    }
                    String sql_staffStatus = "1=1";
                    String staffStatus = "--";
                    if (searchBean.getStaffStatus()!= null && !searchBean.getStaffStatus().isEmpty()) {
                        if (searchBean.getStaffStatus().equals("STAFF")) {
                            staffStatus= "Staff";
                            sql_staffStatus = " M.CUSTOMER_CATEGORY IN ('703')";
                        } else if (searchBean.getStaffStatus().equals("NONSTAFF")) {
                            staffStatus= "Non Staff";
                            sql_staffStatus = " M.CUSTOMER_CATEGORY NOT IN ('703')";
                        }
                        parameterMap.put("staffStatus", staffStatus);
                        parameterMap.put("sql_staffStatus", sql_staffStatus);
                    } else {
                        parameterMap.put("staffStatus", "--");
                        parameterMap.put("sql_staffStatus", sql_staffStatus);
                    }
                    if (searchBean.getChannelType() != null && !searchBean.getChannelType().isEmpty()) {
                        parameterMap.put("channelType", searchBean.getChannelType());
                        parameterMap.put("channelTypeDes", txndao.getChannelTypeDesByCode(searchBean.getChannelType()));
                    } else {
                        parameterMap.put("channelType", "--");
                        parameterMap.put("channelTypeDes", "--");
                    }
//                    if (searchBean.getStatus()!= null && !searchBean.getStatus().isEmpty()) {
//                        parameterMap.put("status", searchBean.getStatus());
//                        parameterMap.put("statusDes", dao.getStatusByprefix(searchBean.getStatus()));
//                    } else {
//                        parameterMap.put("status", "--");
//                        parameterMap.put("statusDes", "--");
//                    }

                    String sql_response_code = "1=1";
                    if (searchBean.getStatus() != null && !searchBean.getStatus().isEmpty()) {
                        if (searchBean.getStatus().equals("SUCC")) {
                            sql_response_code = " TR.RESPONCE_CODE ='000'";
                        } else if (searchBean.getStatus().equals("FAIL")) {
                            sql_response_code = " (TR.RESPONCE_CODE !='000' OR TR.RESPONCE_CODE IS null )";
                        }
                        parameterMap.put("responseCode", searchBean.getStatus());
                        parameterMap.put("sql_responseCode", sql_response_code);
                        parameterMap.put("statusDes", txndao.getStatusDescription(searchBean.getStatus()));
                    } else {
                        parameterMap.put("sql_responseCode", sql_response_code);
                        parameterMap.put("responseCode", "--");
                        parameterMap.put("statusDes", "--");
                    }
                    if (searchBean.getCurrencyCode() != null && !searchBean.getCurrencyCode().isEmpty()) {
                        parameterMap.put("currencyCode", searchBean.getCurrencyCode());
                    } else {
                        parameterMap.put("currencyCode", "--");
                    }
                    if (searchBean.getCustomerCategory() != null && !searchBean.getCustomerCategory().isEmpty()) {
                        parameterMap.put("customerCategory", searchBean.getCustomerCategory());
                    } else {
                        parameterMap.put("customerCategory", "--");
                    }
                    if (searchBean.getBillCategoryName() != null && !searchBean.getBillCategoryName().isEmpty()) {
                        parameterMap.put("billCategoryName", searchBean.getBillCategoryName());
                    } else {
                        parameterMap.put("billCategoryName", "--");
                    }
                    if (searchBean.getBillProviderName() != null && !searchBean.getBillProviderName().isEmpty()) {
                        parameterMap.put("billProviderName", searchBean.getBillProviderName());
                    } else {
                        parameterMap.put("billProviderName", "--");
                    }
                    if (searchBean.getToDate() != null && !searchBean.getToDate().isEmpty()) {
                        parameterMap.put("tdate", searchBean.getToDate().trim());
                    } else {
                        parameterMap.put("tdate", "--");
                    }
                    if (searchBean.getFromDate() != null && !searchBean.getFromDate().isEmpty()) {
                        parameterMap.put("fdate", searchBean.getFromDate().trim());
                    } else {
                        parameterMap.put("fdate", "--");
                    }
                    if (searchBean.getFromAccNo()!= null && !searchBean.getFromAccNo().isEmpty()) {
                        parameterMap.put("fromAccNo", searchBean.getFromAccNo().trim());
                    } else {
                        parameterMap.put("fromAccNo", "--");
                    }
                    if (searchBean.getToAccNo()!= null && !searchBean.getToAccNo().isEmpty()) {
                        parameterMap.put("toAccNo", searchBean.getToAccNo().trim());
                    } else {
                        parameterMap.put("toAccNo", "--");
                    }
                    parameterMap.put("bankaddressheader", CommonVarList.REPORT_ADD_HEADER);
                    parameterMap.put("printeddate", sdf.format(cal.getTime()));
                    parameterMap.put("bankaddress", CommonVarList.REPORT_ADDRESS);
                    parameterMap.put("banktel", CommonVarList.REPORT_TEL);
                    parameterMap.put("bankmail", CommonVarList.REPORT_MAIL);
                    parameterMap.put("imageurl", imgPath);

                    // Virtualizer 
                    String directory = ServletActionContext.getServletContext().getInitParameter("tmpreportpath");
                    File file = new File(directory);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    JRSwapFile swapFile = new JRSwapFile(directory, 4096, 200);
                    virtualizer = new JRSwapFileVirtualizer(300, swapFile, true);
                    parameterMap.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

                    String reportLocation = context.getRealPath("WEB-INF/pages/analytics/report/txn_summary_report.jasper");

                    hSession = HibernateInit.sessionFactory.openSession();
                    SessionImplementor sim = (SessionImplementor) hSession;

                    jasperPrint = JasperFillManager.fillReport(reportLocation, parameterMap, sim.connection());

                    if (virtualizer != null) {
                        virtualizer.setReadOnly(true);
                    }

                    outputFile = JasperExportManager.exportReportToPdf(jasperPrint);
                    fileInputStream = new ByteArrayInputStream(outputFile);
                    setContentLength(outputFile.length);

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.TRAN_EXPLORER, SectionVarList.ANALYTICS, "Transaction summary PDF report generated", null);
                    CommonDAO.saveAudit(audit);

                    retMsg = "download";
                } catch (Exception e) {
                    Logger.getLogger(TranExplorerAction.class.getName()).log(Level.SEVERE, null, e);
                    return "message";
                }
            } else if (inputBean.getReporttype().trim().equalsIgnoreCase("summeayExcel")) {
                ByteArrayOutputStream outputStream = null;
                try {
                    TranExplorerDAO txndao = new TranExplorerDAO();
                    
                    cal.setTime(CommonDAO.getSystemDateLogin());
                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    TranExplorerInputBean searchBean = (TranExplorerInputBean) session.getAttribute(SessionVarlist.TRANSACTION_SEARCHBEAN);
                    
                    Object object = txndao.generateSummeryExcelReport(searchBean);
                    
                    SXSSFWorkbook workbook = (SXSSFWorkbook) object;
                    outputStream = new ByteArrayOutputStream();
                    workbook.write(outputStream);
                    
                    inputBean.setExcelStream(new ByteArrayInputStream(outputStream.toByteArray()));
                    setContentLength(outputStream.toByteArray().length);
                    
                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.TRAN_EXPLORER, SectionVarList.ANALYTICS, "Transaction summary PDF report generated", null);
                    CommonDAO.saveAudit(audit);

                    retMsg = "downloadexcel";
                } catch (Exception e) {
                    Logger.getLogger(TranExplorerAction.class.getName()).log(Level.SEVERE, null, e);
                    return "message";
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TranExplorerAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Transaction explorer");

            return "message";
        } finally {
            if (virtualizer != null) {
                virtualizer.cleanup();
            }
//            try{
//                 connection.close();
////                 fileInputStream.close();
//            }catch(Exception ex){
//                
//        }
            if (hSession != null) {
                hSession.close();
            }
        }
        return retMsg;
    }

    private List<ChannelTypeBean> getChannelTypeList() {

        List<ChannelTypeBean> channelTypeList = new ArrayList<ChannelTypeBean>();

        ChannelTypeBean channeltype1 = new ChannelTypeBean();
        channeltype1.setKey("2");
        channeltype1.setValue("Internet Banking");
        channelTypeList.add(channeltype1);

        ChannelTypeBean channeltype2 = new ChannelTypeBean();
        channeltype2.setKey("1");
        channeltype2.setValue("Mobile Banking");
        channelTypeList.add(channeltype2);

        return channelTypeList;

    }

    private List<CommonKeyVal> getTxnStatusList() {

        List<CommonKeyVal> statusList = new ArrayList<CommonKeyVal>();

        CommonKeyVal status1 = new CommonKeyVal();
        status1.setKey("SUCC");
        status1.setValue("Success");
        statusList.add(status1);

        CommonKeyVal status2 = new CommonKeyVal();
        status2.setKey("FAIL");
        status2.setValue("Failure");
        statusList.add(status2);

        return statusList;

    }
    
    private List<CommonKeyVal> getStaffNonStaffList() {

        List<CommonKeyVal> statusList = new ArrayList<CommonKeyVal>();

        CommonKeyVal status1 = new CommonKeyVal();
        status1.setKey("STAFF");
        status1.setValue("Staff");
        statusList.add(status1);

        CommonKeyVal status2 = new CommonKeyVal();
        status2.setKey("NONSTAFF");
        status2.setValue("Non Staff");
        statusList.add(status2);

        return statusList;

    }

}
