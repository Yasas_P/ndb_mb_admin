/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.analytics;

import com.epic.ndb.bean.analytics.ConvenienceFeeBean;
import com.epic.ndb.bean.analytics.ConvenienceFeeInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.dao.analytics.ConvenienceFeeDAO;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class ConvenienceFeeAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {
    ConvenienceFeeInputBean inputBean = new ConvenienceFeeInputBean(); 
    
    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;
    
    @Override
    public Object getModel() {
        return inputBean;
    }
    
    public String execute() {
        System.out.println("called ConvenienceFeeAction : execute");
        return SUCCESS;
    }
    
    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.CONVENIENCE_FEE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Search".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }
    
    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.CONVENIENCE_FEE, request);

        inputBean.setVgenerate(true);
        inputBean.setVsearch(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                }
            }
        }
    }
    
    public String view() {

        System.out.println("called ConvenienceFeeAction :view");
        String result = "view";

        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusCusList(CommonVarList.STATUS_CATEGORY_CUSTOMER, CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setSegmentTypeList(dao.getSegmentTypeList());
            inputBean.setOnBoardChannelList(this.getChannelTypeList());
            inputBean.setOnBoardTypeList(this.getOnBoardTypeList());
            inputBean.setDefaultAccountTypeList(this.getDefaultAccountTypeList());
            inputBean.setFeeStatusList(this.getFeeStatusList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception ex) {
            addActionError("Convenience Fee " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(ConvenienceFeeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    
    public String list() {
        System.out.println("called ConvenienceFeeAction: List");
        try {
            if (inputBean.isSearch()) {
                int rows = inputBean.getRows();
                int page = inputBean.getPage();
                int to = (rows * page);
                int from = to - rows;
                long records = 0;
                String orderBy = "";

                if (!inputBean.getSidx().isEmpty()) {
                    orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
                } else {
                    orderBy = " order by F.TIMESTAMP DESC ";
                }

                ConvenienceFeeDAO dao = new ConvenienceFeeDAO();
                List<ConvenienceFeeBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

                /**
                 * for search audit
                 */
                if (inputBean.isSearch() && from == 0) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String searchParameters = "["
                            + checkEmptyorNullString("From Date", inputBean.getFromDate())
                            + checkEmptyorNullString("To Date", inputBean.getToDate())
                            + checkEmptyorNullString("CID", inputBean.getCifSearch())
                            + checkEmptyorNullString("User Name", inputBean.getUsernameSearch())
                            + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                            + checkEmptyorNullString("Mobile", inputBean.getMobilenoSearch())
                            + checkEmptyorNullString("Segment Type", inputBean.getSegmentTypeSearch())
                            + checkEmptyorNullString("Primary Type", inputBean.getDefTypeSearch())
                            + checkEmptyorNullString("Primary Acc/Card Number", inputBean.getDefAccNoSearch())
                            + checkEmptyorNullString("OnBoard Channel Type", inputBean.getOnBoardChannelSearch())
                            + checkEmptyorNullString("OnBoard Type", inputBean.getOnBoardTypeSearch())
                            + checkEmptyorNullString("Customer Category", inputBean.getCustomerCategorySearch())
                            + checkEmptyorNullString("Fee Deduct Status", inputBean.getFeeStatusSearch())
                            + "]";

                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.CONVENIENCE_FEE, SectionVarList.ANALYTICS, "Convenience Fee search using " + searchParameters + " parameters ", null);
                    SystemAuditDAO sysdao = new SystemAuditDAO();
                    sysdao.saveAudit(audit);
                }

                if (!dataList.isEmpty()) {
                    records = dataList.get(0).getFullCount();
                    inputBean.setRecords(records);
                    inputBean.setGridModel(dataList);
                    int total = (int) Math.ceil((double) records / (double) rows);
                    inputBean.setTotal(total);

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.CONVENIENCE_FEE_SEARCHBEAN, inputBean);
                } else {
                    inputBean.setRecords(0L);
                    inputBean.setTotal(0);
                }
            }

        } catch (Exception e) {
            Logger.getLogger(ConvenienceFeeAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " convenience fee");
        }
        return "list";
    }
    
    public String reportGenerate() {

        System.out.println("called ConvenienceFeeAction : reportGenerate");
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                ConvenienceFeeDAO dao = new ConvenienceFeeDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    ConvenienceFeeInputBean searchBean = (ConvenienceFeeInputBean) session.getAttribute(SessionVarlist.CONVENIENCE_FEE_SEARCHBEAN);
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new ConvenienceFeeInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Convenience_Fee_Report.csv");
                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.CONVENIENCE_FEE, SectionVarList.ANALYTICS, "Convenience Fee csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger
                            .getLogger(ConvenienceFeeAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ConvenienceFeeAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " convenience fee");

            return "message";
        }
        return retMsg;
    }
     
    private List<CommonKeyVal> getChannelTypeList() {

        List<CommonKeyVal> channelTypeList = new ArrayList<CommonKeyVal>();

        CommonKeyVal channeltype1 = new CommonKeyVal();
        channeltype1.setKey("2");
        channeltype1.setValue("Internet Banking");
        channelTypeList.add(channeltype1);

        CommonKeyVal channeltype2 = new CommonKeyVal();
        channeltype2.setKey("1");
        channeltype2.setValue("Mobile Banking");
        channelTypeList.add(channeltype2);

        return channelTypeList;

    }

    private List<CommonKeyVal> getOnBoardTypeList() {

        List<CommonKeyVal> channelTypeList = new ArrayList<CommonKeyVal>();

        CommonKeyVal onBoardType1 = new CommonKeyVal();
        onBoardType1.setKey("1");
        onBoardType1.setValue("Account");
        channelTypeList.add(onBoardType1);

        CommonKeyVal onBoardType2 = new CommonKeyVal();
        onBoardType2.setKey("2");
        onBoardType2.setValue("Card");
        channelTypeList.add(onBoardType2);

        return channelTypeList;

    }

    private List<CommonKeyVal> getDefaultAccountTypeList() {

        List<CommonKeyVal> channelTypeList = new ArrayList<CommonKeyVal>();

        CommonKeyVal onBoardType1 = new CommonKeyVal();
        onBoardType1.setKey("1");
        onBoardType1.setValue("Account");
        channelTypeList.add(onBoardType1);

        CommonKeyVal onBoardType2 = new CommonKeyVal();
        onBoardType2.setKey("2");
        onBoardType2.setValue("Card");
        channelTypeList.add(onBoardType2);

        return channelTypeList;

    }
    
    private List<CommonKeyVal> getFeeStatusList() {

        List<CommonKeyVal> statusList = new ArrayList<CommonKeyVal>();

        CommonKeyVal status1 = new CommonKeyVal();
        status1.setKey("SUCC");
        status1.setValue("Success");
        statusList.add(status1);

        CommonKeyVal status2 = new CommonKeyVal();
        status2.setKey("FAIL");
        status2.setValue("Failure");
        statusList.add(status2);

        return statusList;

    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }
    
}
