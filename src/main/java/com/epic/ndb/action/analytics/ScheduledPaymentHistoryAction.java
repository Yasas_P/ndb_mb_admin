package com.epic.ndb.action.analytics;

import com.epic.ndb.bean.analytics.ScheduledPaymentHistoryBean;
import com.epic.ndb.bean.analytics.ScheduledPaymentHistoryInputBean;
import com.epic.ndb.dao.analytics.ScheduledPaymentHistoryDAO;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;

/**
 *
 * @author sivaganesan_t
 */
public class ScheduledPaymentHistoryAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    ScheduledPaymentHistoryInputBean inputBean = new ScheduledPaymentHistoryInputBean();
    Map parameterMap = new HashMap();
    private InputStream fileInputStream = null;
    private String fileName;
    private long contentLength;

    public Map getParameterMap() {
        return parameterMap;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.SCHEDULED_PAYMENT_HISTORY;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Search".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.SCHEDULED_PAYMENT_HISTORY, request);

        inputBean.setVgenerate(true);
        inputBean.setVsearch(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.VIEW_TASK)) {
                }
            }
        }
    }

    public String execute() {
        System.out.println("called ScheduledPaymentHistoryAction : execute");
        return SUCCESS;
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();
            System.out.println("called ScheduledPaymentHistoryAction :view");

        } catch (Exception ex) {
            addActionError("Scheduled payment history " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(ScheduledPaymentHistoryAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called ScheduledPaymentHistoryAction : list");
        try {
            if (inputBean.isSearch()) {

                int rows = inputBean.getRows();
                int page = inputBean.getPage();
                int to = (rows * page);
                int from = to - rows;
                long records = 0;

                String orderBy = "";
                if (!inputBean.getSidx().isEmpty()) {
                    orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
                }

                HttpServletRequest request = ServletActionContext.getRequest();
                ScheduledPaymentHistoryDAO dao = new ScheduledPaymentHistoryDAO();

                List<ScheduledPaymentHistoryBean> searchList = dao.getSearchList(inputBean, rows, from, orderBy);

                String searchParameters = "["
                        + Common.checkEmptyorNullString("From Date", inputBean.getFromDate())
                        + Common.checkEmptyorNullString("To Date", inputBean.getToDate())
                        + Common.checkEmptyorNullString("Customer CID", inputBean.getCif())
                        + "]";

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SCHEDULED_PAYMENT_HISTORY, SectionVarList.ANALYTICS, "Scheduled payment history search using " + searchParameters + " parameters ", null, null, null);
                CommonDAO.saveAudit(audit);

                if (!searchList.isEmpty()) {
                    records = searchList.get(0).getFullCount();
                    inputBean.setRecords(records);
                    inputBean.setGridModel(searchList);
                    int total = (int) Math.ceil((double) records / (double) rows);
                    inputBean.setTotal(total);

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.SCHEDULEDPAYMENTHISTORY_SEARCHBEAN, inputBean);

                } else {
                    inputBean.setRecords(0L);
                    inputBean.setTotal(0);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ScheduledPaymentHistoryAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Scheduled payment history " + MessageVarList.COMMON_ERROR_PROCESS);
            return "message";
        }
        return "list";
    }

    public String reportGenerate() {

        System.out.println("called ScheduledPaymentHistoryAction : reportGeneration");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy 'at' HH:mm a");
        JRSwapFileVirtualizer virtualizer = null;
        JasperPrint jasperPrint = null;
        byte[] outputFile;
        Session hSession = null;
        InputStream inputStream = null;
        String retMsg = "view";

        try {
            if (inputBean.getReporttype().equals("pdf")) {
                cal.setTime(CommonDAO.getSystemDateLogin());

                HttpSession session = ServletActionContext.getRequest().getSession(false);
                ScheduledPaymentHistoryInputBean searchBean = (ScheduledPaymentHistoryInputBean) session.getAttribute(SessionVarlist.SCHEDULEDPAYMENTHISTORY_SEARCHBEAN);

                //get path
                ServletContext context = ServletActionContext.getServletContext();
                String imgPath = context.getRealPath("/resouces/images/ndb_bank_logo.png");

                CommonDAO dao = new CommonDAO();

                if (searchBean.getCif() != null && !searchBean.getCif().isEmpty()) {
                    parameterMap.put("cif", searchBean.getCif().trim());
                } else {
                    parameterMap.put("cif", "--");
                }               
                if (searchBean.getToDate() != null && !searchBean.getToDate().isEmpty()) {
                    parameterMap.put("tdate", searchBean.getToDate().trim());
                } else {
                    parameterMap.put("tdate", "--");
                }
                if (searchBean.getFromDate() != null && !searchBean.getFromDate().isEmpty()) {
                    parameterMap.put("fdate", searchBean.getFromDate().trim());
                } else {
                    parameterMap.put("fdate", "--");
                }

                parameterMap.put("bankaddressheader", CommonVarList.REPORT_ADD_HEADER);
                parameterMap.put("printeddate", sdf.format(cal.getTime()));
                parameterMap.put("bankaddress", CommonVarList.REPORT_ADDRESS);
                parameterMap.put("banktel", CommonVarList.REPORT_TEL);
                parameterMap.put("bankmail", CommonVarList.REPORT_MAIL);
                parameterMap.put("imageurl", imgPath);

                // Virtualizer 
                String directory = ServletActionContext.getServletContext().getInitParameter("tmpreportpath");
                File file = new File(directory);
                if (!file.exists()) {
                    file.mkdirs();
                }
                JRSwapFile swapFile = new JRSwapFile(directory, 4096, 200);
                virtualizer = new JRSwapFileVirtualizer(300, swapFile, true);
                parameterMap.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);

                String reportLocation = context.getRealPath("WEB-INF/pages/analytics/report/scheduled_payment _history.jasper");

                hSession = HibernateInit.sessionFactory.openSession();
                SessionImplementor sim = (SessionImplementor) hSession;

                jasperPrint = JasperFillManager.fillReport(reportLocation, parameterMap, sim.connection());

                if (virtualizer != null) {
                    virtualizer.setReadOnly(true);
                }

                outputFile = JasperExportManager.exportReportToPdf(jasperPrint);
                setFileInputStream(new ByteArrayInputStream(outputFile));
                setContentLength(outputFile.length);

                HttpServletRequest request = ServletActionContext.getRequest();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.SCHEDULED_PAYMENT_HISTORY, SectionVarList.ANALYTICS, "Shedule payment history PDF report generated", null);
                CommonDAO.saveAudit(audit);

                retMsg = "download";
            }else if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {
                
                ScheduledPaymentHistoryDAO dao = new ScheduledPaymentHistoryDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                ScheduledPaymentHistoryInputBean searchBean = (ScheduledPaymentHistoryInputBean) session.getAttribute(SessionVarlist.SCHEDULEDPAYMENTHISTORY_SEARCHBEAN);
                
                if (searchBean != null) {
                    sb = dao.makeCSVReport(searchBean);
                } else {
                    sb = dao.makeCSVReport(new ScheduledPaymentHistoryInputBean());
                }
                
                try {
                    inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                    setFileInputStream(inputStream);
                    setFileName("Scheduled_Payment_History_Report.csv");
                    setContentLength(sb.length());
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
                
                HttpServletRequest request = ServletActionContext.getRequest();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.SCHEDULED_PAYMENT_HISTORY, SectionVarList.ANALYTICS, "Shedule payment history csv report generated", null);
                CommonDAO.saveAudit(audit);
            }
        } catch (Exception e) {
            this.loadPageData();
            Logger.getLogger(ScheduledPaymentHistoryAction.class.getName()).log(Level.SEVERE, null, e);

            return "message";
        } finally {
            if (virtualizer != null) {
                virtualizer.cleanup();
            }
            if (hSession != null) {
                hSession.close();
            }
        }
        return retMsg;
    }

    private void loadPageData() {
        try {
            CommonDAO dao = new CommonDAO();
        } catch (Exception e) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Scheduled Payment History");
            Logger.getLogger(ScheduledPaymentHistoryAction.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }
}
