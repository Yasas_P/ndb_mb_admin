/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.SegmentBean;
import com.epic.ndb.bean.controlpanel.systemconfig.SegmentInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.SegmentPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SegmentDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author jayathissa_d
 */
public class SegmentAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    SegmentInputBean inputBean = new SegmentInputBean();

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called SegmentAction : execute");
        return SUCCESS;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.SEGMEMNT_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("Delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        }//newly changed
        else if ("activate".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("ViewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.SEGMEMNT_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called SegmentAction :view");

        } catch (Exception ex) {
            addActionError("Segment " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called SegmentAction: List");
        try {
            //if (inputBean.isSearch()) {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            SegmentDAO dao = new SegmentDAO();
            List<SegmentBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("Segment code", inputBean.getSegmentcodeSearch())
                        + checkEmptyorNullString("Description", inputBean.getDescriptionSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SEGMEMNT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Segment management search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" Segment " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String ApproveList() {
        System.out.println("called SegmentAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            SegmentDAO dao = new SegmentDAO();
            List<SegmentPendBean> dataList = dao.getPendingSegmentList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SEGMEMNT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Segment" + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String ViewPopup() {
        String result = "viewpopup";
        System.out.println("called SegmentAction : ViewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setWaveOffStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_AUTH));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called SegmentAction :ViewPopup");

        } catch (Exception ex) {
            addActionError("Segment " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String Add() {
        System.out.println("called SegmentAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SegmentDAO dao = new SegmentDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getSegmentcode() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getStatus() + "|"
                        + inputBean.getWaveoff() + "|"
                        + inputBean.getChargeAmount();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.SEGMEMNT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to add Segment (Segment code: " + inputBean.getSegmentcode() + ") ", null, null, newv);

                message = dao.insertSegment(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " segment ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Segment " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getSegmentcode() == null || inputBean.getSegmentcode().trim().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_CODE;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_DESCRIPTIION;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_STATUS;
        } else if (inputBean.getWaveoff() == null || inputBean.getWaveoff().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_WAVE_OFF_STATUS;
        } else if (inputBean.getChargeAmount() == null || inputBean.getChargeAmount().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_CHARGE_AMOUNT;
//        } else if (!Validation.isSpecailCharacter(inputBean.getSegmentcode())) {
//            message = MessageVarList.SEGMENT_MGT_INVALID_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getDescription())) {
            message = MessageVarList.SEGMENT_MGT_INVALID_DESCRIPTIION;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getChargeAmount())) {
            message = MessageVarList.SEGMENT_MGT_INVALID_CHARGE_AMOUNT;
        }

        return message;
    }

    public String Delete() {
        System.out.println("called SegmentAction : Delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SegmentDAO dao = new SegmentDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.SEGMEMNT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete segment (Segment code: " + inputBean.getSegmentcode() + ") ", null);
            message = dao.deleteSegment(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " segment ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
//            inputBean.setMessage(MessageVarList.COMMON_ERROR_DELETE);
        }
        return retType;
    }

    public String Detail() {
        System.out.println("called SegmentAction: Detail");
        SegmentType segmenttype = null;
        try {
            if (inputBean.getSegmentcode() != null && !inputBean.getSegmentcode().isEmpty()) {

                SegmentDAO dao = new SegmentDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setWaveOffStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_AUTH));
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                segmenttype = dao.findSegmentById(inputBean.getSegmentcode());

                inputBean.setSegmentcode(segmenttype.getSegmentcode());
                inputBean.setDescription(segmenttype.getDescription());
                inputBean.setStatus(segmenttype.getStatus());
                inputBean.setWaveoff(segmenttype.getWaveoff().getStatuscode());
                inputBean.setChargeAmount(segmenttype.getChargeAmount().toString());

                inputBean.setOldvalue(segmenttype.getSegmentcode() + "|"
                        + segmenttype.getDescription() + "|"
                        + segmenttype.getStatus() + "|"
                        + segmenttype.getWaveoff().getStatuscode() + "|"
                        + segmenttype.getChargeAmount().toString());

            } else {
                inputBean.setMessage("Empty segment code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Segment" + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String Update() {

        System.out.println("called SegmentAction : update");
        String retType = "message";

        try {
            if (inputBean.getSegmentcode() != null && !inputBean.getSegmentcode().isEmpty()) {
                SegmentDAO dao = new SegmentDAO();
                inputBean.setSegmentcode(inputBean.getSegmentcode());

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getSegmentcode() + "|"
                            + inputBean.getDescription() + "|"
                            + inputBean.getStatus() + "|"
                            + inputBean.getWaveoff() + "|"
                            + inputBean.getChargeAmount();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) {
                        String newValWithActState = inputBean.getSegmentcode() + "|"
                            + inputBean.getDescription() + "|"
                            + CommonVarList.STATUS_ACTIVE + "|"
                            + inputBean.getWaveoff() + "|"
                            + inputBean.getChargeAmount();
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.SEGMEMNT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update segment (Segment code: " + inputBean.getSegmentcode() + ") ", null, oldVal, newv);
                            message = dao.updateSegment(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " segment ");
                            } else {
                                addActionError(message);
                            }

                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Segment " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getSegmentcode() == null || inputBean.getSegmentcode().trim().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_CODE;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_DESCRIPTIION;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_STATUS;
        } else if (inputBean.getWaveoff() == null || inputBean.getWaveoff().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_WAVE_OFF_STATUS;
        } else if (inputBean.getChargeAmount() == null || inputBean.getChargeAmount().isEmpty()) {
            message = MessageVarList.SEGMENT_MGT_EMPTY_CHARGE_AMOUNT;
//        } else if (!Validation.isSpecailCharacter(inputBean.getSegmentcode())) {
//            message = MessageVarList.SEGMENT_MGT_INVALID_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getDescription())) {
            message = MessageVarList.SEGMENT_MGT_INVALID_DESCRIPTIION;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getChargeAmount())) {
            message = MessageVarList.SEGMENT_MGT_INVALID_CHARGE_AMOUNT;
        }
        return message;
    }

    public String Confirm() {
        System.out.println("called SegmentAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                SegmentDAO dao = new SegmentDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.SEGMEMNT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmSegment(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Segment " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called SegmentAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                SegmentDAO dao = new SegmentDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.SEGMEMNT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectSegment(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Segment " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    public String Find() {
        System.out.println("called SegmentAction: Find");
        SegmentType txntype = null;
        try {
            if (inputBean.getSegmentcode() != null && !inputBean.getSegmentcode().isEmpty()) {

                SegmentDAO dao = new SegmentDAO();

                txntype = dao.findSegmentById(inputBean.getSegmentcode());

                inputBean.setSegmentcode(txntype.getSegmentcode());
                inputBean.setDescription(txntype.getDescription());
                inputBean.setStatus(txntype.getStatus());
                inputBean.setWaveoff(txntype.getWaveoff().getStatuscode());
                inputBean.setChargeAmount(txntype.getChargeAmount().toString());

            } else {
                inputBean.setMessage("Empty segment code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Segment " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

}
