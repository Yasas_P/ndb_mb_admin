/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.bean.controlpanel.systemconfig.PasswordPolicyInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PasswordPolicyPendBean;
import com.epic.ndb.dao.controlpanel.systemconfig.PasswordPolicyDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.mapping.Passwordpolicy;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Yasas
 */
public class PasswordPolicyAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    PasswordPolicyInputBean inputBean = new PasswordPolicyInputBean();

    public PasswordPolicyAction() {
    }

    @Override
    public String execute() throws Exception {
        System.out.println("called PasswordPolicyAction : execute");
        return SUCCESS;
    }

    public Object getModel() {
        return inputBean;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.PAS_POLICY;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("reset".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() throws Exception {
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.PAS_POLICY, request);

            inputBean.setVadd(true);
            inputBean.setVupdatelink(true);
            inputBean.setVupdatebutt(true);
            inputBean.setVconfirm(true);
            inputBean.setVreject(true);
            inputBean.setVdual(true);
            PasswordPolicyDAO dao = new PasswordPolicyDAO();
            if (dao.isExistPasswordPolicy()) {
                if (tasklist != null && tasklist.size() > 0) {
                    for (Task task : tasklist) {
                        if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                            inputBean.setVadd(true);
//                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.LOGIN_TASK)) {
                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                            inputBean.setVupdatelink(false);
                            inputBean.setVupdatebutt(false);
                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_TASK)) {
                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                            inputBean.setVconfirm(false);
                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                            inputBean.setVreject(false);
                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                            inputBean.setVdual(false);
                        }
                    }

                    Passwordpolicy passwordpolicy = dao.getPasswordPolicyDetails();
                    inputBean.setPolicyid(true);

                    inputBean.setPasswordpolicyid(String.valueOf(passwordpolicy.getPasswordpolicyid()));
                    inputBean.setMinimumlength(String.valueOf(passwordpolicy.getMinimumlength()));
                    inputBean.setMaximumlength(String.valueOf(passwordpolicy.getMaximumlength()));
                    inputBean.setMinimumspecialcharacters(String.valueOf(passwordpolicy.getMinimumspecialcharacters()));
                    inputBean.setMinimumuppercasecharacters(String.valueOf(passwordpolicy.getMinimumuppercasecharacters()));
                    inputBean.setMinimumlowercasecharacters(String.valueOf(passwordpolicy.getMinimumlowercasecharacters()));
                    inputBean.setMinimumnumericalcharacters(String.valueOf(passwordpolicy.getMinimumnumericalcharacters()));
                    inputBean.setRepeatcharactersallow(String.valueOf(passwordpolicy.getRepeatcharactersallow()));

                    if (!inputBean.getPasswordpolicyid().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                        inputBean.setInitialpasswordexpirystatus(String.valueOf(passwordpolicy.getInitialpasswordexpirystatus()));
                        inputBean.setPasswordexpiryperiod(String.valueOf(passwordpolicy.getPasswordexpiryperiod()));
                        inputBean.setNoofhistorypassword(String.valueOf(passwordpolicy.getNoofhistorypassword()));
                        inputBean.setMinimumpasswordchangeperiod(String.valueOf(passwordpolicy.getMinimumpasswordchangeperiod()));
                        inputBean.setIdleaccountexpiryperiod(String.valueOf(passwordpolicy.getIdleaccountexpiryperiod()));
                        inputBean.setNoofinvalidloginattempt(String.valueOf(passwordpolicy.getNoofinvalidloginattempt()));
                    }

                    String oldVal = inputBean.getPasswordpolicyid() + "|"
                            + inputBean.getMinimumlength() + "|"
                            + inputBean.getMaximumlength() + "|"
                            + inputBean.getMinimumspecialcharacters() + "|"
                            + inputBean.getMinimumuppercasecharacters() + "|"
                            + inputBean.getMinimumlowercasecharacters() + "|"
                            + inputBean.getMinimumnumericalcharacters() + "|"
                            + inputBean.getRepeatcharactersallow() + "|"
                            + inputBean.getPasswordexpiryperiod();

                    if (!inputBean.getPasswordpolicyid().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                        oldVal += "|" + inputBean.getNoofhistorypassword() + "|"
                                + inputBean.getMinimumpasswordchangeperiod() + "|"
                                + inputBean.getIdleaccountexpiryperiod() + "|"
                                + inputBean.getNoofinvalidloginattempt();
                    }
                    inputBean.setOldvalue(oldVal);

                }
            } else {
                if (tasklist != null && tasklist.size() > 0) {
                    for (Task task : tasklist) {
                        if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                            inputBean.setVadd(false);
//                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.LOGIN_TASK)) {
                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                            inputBean.setVupdatelink(true);
                            inputBean.setVupdatebutt(true);
                        } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_TASK)) {
                        }
                    }
                    inputBean.setPolicyid(false);

                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            PasswordPolicyDAO dao = new PasswordPolicyDAO();
            inputBean.setPasswordpolicyList(dao.getPasswordPolicyList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

            System.out.println("called PasswordPolicyAction : view");

        } catch (Exception ex) {
            addActionError("Password policy action " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String approveList() {
        System.out.println("called PasswordPolicyAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            PasswordPolicyDAO dao = new PasswordPolicyDAO();
            List<PasswordPolicyPendBean> dataList = dao.getPendingPasswordPolicyList(inputBean, rows, from, orderBy);

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " password policy ");
        }
        return "list";
    }

    public String find() {
        System.out.println("called PasswordPolicyAction: Find");
        Passwordpolicy passwordpolicy = new Passwordpolicy();
        try {
            if (inputBean.getPasswordpolicyid() != null && !inputBean.getPasswordpolicyid().isEmpty()) {

                PasswordPolicyDAO dao = new PasswordPolicyDAO();

                passwordpolicy = dao.findPasswordPolicyById(inputBean.getPasswordpolicyid());

                inputBean.setPasswordpolicyid(String.valueOf(passwordpolicy.getPasswordpolicyid()));
                inputBean.setMinimumlength(String.valueOf(passwordpolicy.getMinimumlength()));
                inputBean.setMaximumlength(String.valueOf(passwordpolicy.getMaximumlength()));
                inputBean.setMinimumspecialcharacters(String.valueOf(passwordpolicy.getMinimumspecialcharacters()));
                inputBean.setMinimumuppercasecharacters(String.valueOf(passwordpolicy.getMinimumuppercasecharacters()));
                inputBean.setMinimumnumericalcharacters(String.valueOf(passwordpolicy.getMinimumnumericalcharacters()));
                inputBean.setMinimumlowercasecharacters(String.valueOf(passwordpolicy.getMinimumlowercasecharacters()));
                inputBean.setRepeatcharactersallow(String.valueOf(passwordpolicy.getRepeatcharactersallow()));
                inputBean.setInitialpasswordexpirystatus(String.valueOf(passwordpolicy.getInitialpasswordexpirystatus()));
                inputBean.setPasswordexpiryperiod(String.valueOf(passwordpolicy.getPasswordexpiryperiod()));
                inputBean.setNoofhistorypassword(String.valueOf(passwordpolicy.getNoofhistorypassword()));
                inputBean.setMinimumpasswordchangeperiod(String.valueOf(passwordpolicy.getMinimumpasswordchangeperiod()));
                inputBean.setIdleaccountexpiryperiod(String.valueOf(passwordpolicy.getIdleaccountexpiryperiod()));
                inputBean.setNoofinvalidloginattempt(String.valueOf(passwordpolicy.getNoofinvalidloginattempt()));

            } else {
                inputBean.setMessage("Empty password policy id.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Password policy " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String add() {
        System.out.println("called PasswordPolicyAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            PasswordPolicyDAO dao = new PasswordPolicyDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newV = inputBean.getPasswordpolicyid() + "|"
                        + inputBean.getMinimumlength() + "|"
                        + inputBean.getMaximumlength() + "|"
                        + inputBean.getMinimumspecialcharacters() + "|"
                        + inputBean.getMinimumuppercasecharacters() + "|"
                        + inputBean.getMinimumlowercasecharacters() + "|"
                        + inputBean.getMinimumnumericalcharacters() + "|"
                        + inputBean.getRepeatcharactersallow() + "|"
                        + inputBean.getPasswordexpiryperiod() + "|"
                        + inputBean.getMinimumpasswordchangeperiod() + "|"
                        + inputBean.getNoofhistorypassword() + "|"
                        + inputBean.getIdleaccountexpiryperiod() + "|"
                        + inputBean.getNoofinvalidloginattempt();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.PAS_POLICY, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Password policy details added", null, null, newV);

                message = dao.insertPasswordPolicy(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage("Password policy  " + MessageVarList.COMMON_SUCC_INSERT);
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Password policy " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String update() {

        System.out.println("called PasswordPolicyAction : update");
        String retType = "message";

        try {
            if (inputBean.getPasswordpolicyid() != null && !inputBean.getPasswordpolicyid().isEmpty()) {

                String message = this.validateInputs();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();
                    PasswordPolicyDAO dao = new PasswordPolicyDAO();

                    Passwordpolicy passwordpolicy = dao.findPasswordPolicyById(inputBean.getPasswordpolicyid());

                    String oldVal = String.valueOf(passwordpolicy.getPasswordpolicyid()) + "|"
                            + String.valueOf(passwordpolicy.getMinimumlength()) + "|"
                            + String.valueOf(passwordpolicy.getMaximumlength()) + "|"
                            + String.valueOf(passwordpolicy.getMinimumspecialcharacters()) + "|"
                            + String.valueOf(passwordpolicy.getMinimumuppercasecharacters()) + "|"
                            + String.valueOf(passwordpolicy.getMinimumlowercasecharacters()) + "|"
                            + String.valueOf(passwordpolicy.getMinimumnumericalcharacters()) + "|"
                            + String.valueOf(passwordpolicy.getRepeatcharactersallow());

                    if (!String.valueOf(passwordpolicy.getPasswordpolicyid()).equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                        oldVal += "|" + String.valueOf(passwordpolicy.getPasswordexpiryperiod()) + "|"
                                + String.valueOf(passwordpolicy.getMinimumpasswordchangeperiod()) + "|"
                                + String.valueOf(passwordpolicy.getNoofhistorypassword()) + "|"
                                + String.valueOf(passwordpolicy.getIdleaccountexpiryperiod() + "|"
                                        + String.valueOf(passwordpolicy.getNoofinvalidloginattempt()));
                    }
                    inputBean.setOldvalue(oldVal);

                    String newV = inputBean.getPasswordpolicyid() + "|"
                            + inputBean.getMinimumlength() + "|"
                            + inputBean.getMaximumlength() + "|"
                            + inputBean.getMinimumspecialcharacters() + "|"
                            + inputBean.getMinimumuppercasecharacters() + "|"
                            + inputBean.getMinimumlowercasecharacters() + "|"
                            + inputBean.getMinimumnumericalcharacters() + "|"
                            + inputBean.getRepeatcharactersallow();
                    if (!inputBean.getPasswordpolicyid().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                        newV += "|" + inputBean.getPasswordexpiryperiod() + "|"
                                + inputBean.getMinimumpasswordchangeperiod() + "|"
                                + inputBean.getNoofhistorypassword() + "|"
                                + inputBean.getIdleaccountexpiryperiod() + "|"
                                + inputBean.getNoofinvalidloginattempt();
                    }

                    System.out.println("OLD value :" + oldVal);
                    System.out.println("NEW value :" + newV);

                    if (!newV.equals(oldVal)) {

                        Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK,
                                PageVarList.PAS_POLICY, SectionVarList.SYSTEMCONFIGMANAGEMENT,
                                "Requested to update password policy ID ( ID : " + inputBean.getPasswordpolicyid() + " )", null, inputBean.getOldvalue(), newV);
                        message = dao.updatePasswordPolicy(inputBean, audit);

                        if (message.isEmpty()) {
                            addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " password policy ");
                        } else {
                            addActionError(message);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Password policy " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    private String validateInputs() {
        String message = "";

        if (inputBean.getPasswordpolicyid() == null || inputBean.getPasswordpolicyid().isEmpty()) {
            message = MessageVarList.PASSPOLICY_POLICYID_EMPTY;
        } else if (inputBean.getMinimumlength() == null || inputBean.getMinimumlength().isEmpty()) {
            message = MessageVarList.PASSPOLICY_MINLEN_EMPTY;
        } else if (inputBean.getMaximumlength() == null || inputBean.getMaximumlength().isEmpty()) {
            message = MessageVarList.PASSPOLICY_MAXLEN_EMPTY;
        } else if (Integer.parseInt(inputBean.getMinimumlength()) >= Integer.parseInt(inputBean.getMaximumlength())) {
            message = MessageVarList.PASSPOLICY_MIN_MAX_LENGHT_DIFF;
        } else if (inputBean.getMinimumspecialcharacters() == null || inputBean.getMinimumspecialcharacters().isEmpty()) {
            message = MessageVarList.PASSPOLICY_SPECCHARS_EMPTY;
        } else if (Integer.parseInt(inputBean.getMinimumspecialcharacters()) > 6) {
            message = MessageVarList.PASSPOLICY_SPECCHARS_SHOULD_BE_LESS + "7";
        } else if (inputBean.getMinimumuppercasecharacters() == null || inputBean.getMinimumuppercasecharacters().isEmpty()) {
            message = MessageVarList.PASSPOLICY_MINUPPER_EMPTY;
        } else if (inputBean.getMinimumlowercasecharacters() == null || inputBean.getMinimumlowercasecharacters().isEmpty()) {
            message = MessageVarList.PASSPOLICY_MINLOWER_EMPTY;
        } else if (inputBean.getMinimumnumericalcharacters() == null || inputBean.getMinimumnumericalcharacters().isEmpty()) {
            message = MessageVarList.PASSPOLICY_MINNUM_EMPTY;

        } else if (inputBean.getRepeatcharactersallow() == null || inputBean.getRepeatcharactersallow().isEmpty()) {
            message = MessageVarList.PASSPOLICY_REPEATE_CHARACTERS_EMPTY;
//        } else if (Integer.parseInt(inputBean.getRepeatcharactersallow())==0) {
//            message = MessageVarList.PASSPOLICY_REPEATE_CHARACTERS_ZERO;
            //       }  else if (inputBean.getInitialpasswordexpirystatus() == null || inputBean.getInitialpasswordexpirystatus().isEmpty()) {
            //            message = MessageVarList.PASSPOLICY_INIT_PASSWORD_EXPIRY_STATUS_EMPTY;
            //
        } else if (!inputBean.getPasswordpolicyid().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {//expect mobile user name policy
            if (inputBean.getPasswordexpiryperiod() == null || inputBean.getPasswordexpiryperiod().isEmpty()) {
                message = MessageVarList.PASSPOLICY_PAS_EXPIRY_PERIOD_EMPTY;
            } else if (inputBean.getMinimumpasswordchangeperiod() == null || inputBean.getMinimumpasswordchangeperiod().isEmpty()) {
                message = MessageVarList.PASSPOLICY_MIN_PAS_CHANGE_PERIOD_EMPTY;
            } else if (inputBean.getNoofhistorypassword() == null || inputBean.getNoofhistorypassword().isEmpty()) {
                message = MessageVarList.PASSPOLICY_NO_OF_HISTORY_PAS_EMPTY;
            } else if (inputBean.getIdleaccountexpiryperiod() == null || inputBean.getIdleaccountexpiryperiod().isEmpty()) {
                message = MessageVarList.PASSPOLICY_IDLE_ACCOUNT_EXPIRY_PERIOD_EMPTY;
            } else if (inputBean.getNoofinvalidloginattempt() == null || inputBean.getNoofinvalidloginattempt().isEmpty()) {
                message = MessageVarList.PASSPOLICY_NO_OF_INVALID_LOGIN_ATTEMPTS_EMPTY;
            }
        }
        if (message.isEmpty() && inputBean.getMinimumlowercasecharacters() != null && !inputBean.getMinimumlowercasecharacters().isEmpty()
                && inputBean.getMinimumnumericalcharacters() != null && !inputBean.getMinimumnumericalcharacters().isEmpty()
                && inputBean.getMinimumspecialcharacters() != null && !inputBean.getMinimumspecialcharacters().isEmpty()
                && inputBean.getMinimumuppercasecharacters() != null && !inputBean.getMinimumuppercasecharacters().isEmpty()) {

            Integer minLower = Integer.parseInt(inputBean.getMinimumlowercasecharacters());
            Integer minNumerical = Integer.parseInt(inputBean.getMinimumnumericalcharacters());
            Integer minSpecial = Integer.parseInt(inputBean.getMinimumspecialcharacters());
            Integer minUpper = Integer.parseInt(inputBean.getMinimumuppercasecharacters());
//        Integer minVal=Integer.parseInt(inputBean.get());

            Integer minLength = minLower + minNumerical + minSpecial + minUpper;
            Integer maxLength = minLength + 3;

            System.err.println("minLength " + minLength);
            System.err.println("maxLength " + maxLength);
//            try {
//
//                if (Integer.parseInt(inputBean.getMinimumlength()) < minLength) { //4
//                    if (message.equals("")) {
//                        message = MessageVarList.PASSPOLICY_MINLEN_INVALID + (minLength);
//                    }
//                }
//            } catch (Exception e) {
//                if (message.equals("")) {
//                    message = MessageVarList.PASSPOLICY_MINLEN_INVALID + (minLength);
//                }
//
//            }
//            try {
//                if (Integer.parseInt(inputBean.getMaximumlength()) > maxLength) { // 10 //12 now    11>12
//                    if (message.equals("")) {
//                        message = MessageVarList.PASSPOLICY_MAXLEN_INVALID + (maxLength);
//                    }
//                }
//            } catch (Exception e) {
//                if (message.equals("")) {
//                    message = MessageVarList.PASSPOLICY_MAXLEN_INVALID + (maxLength);
//                }
//            }
            if (!inputBean.getPasswordpolicyid().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {//expect mobile user name policy
                //check noofhistorypassword is 1(one) or above
                try {
                    if (Integer.parseInt(inputBean.getNoofhistorypassword()) <= 0) {
                        if (message.equals("")) {
                            message = MessageVarList.PASSPOLICY_NO_OF_HISTORY_PAS_INVALID;
                        }
                    }
                } catch (Exception e) {
                    if (message.equals("")) {
                        message = MessageVarList.PASSPOLICY_NO_OF_HISTORY_PAS_INVALID;
                    }

                }
                //check Idle Account Expiry Period is 10 or above
//                try {
//                    if (Integer.parseInt(inputBean.getIdleaccountexpiryperiod()) < 10) {
//                        if (message.equals("")) {
//                            message = MessageVarList.PASSPOLICY_IDLE_ACCOUNT_EXPIRY_PERIOD_INVALID;
//                        }
//                    }
//                } catch (Exception e) {
//                    if (message.equals("")) {
//                        message = MessageVarList.PASSPOLICY_IDLE_ACCOUNT_EXPIRY_PERIOD_INVALID;
//                    }
//
//                }

                //check Password Expiry Period is 10 or above
                try {
                    if (Integer.parseInt(inputBean.getPasswordexpiryperiod()) < 10) {
                        if (message.equals("")) {
                            message = MessageVarList.PASSPOLICY_PAS_EXPIRY_PERIOD_INVALID;
                        }
                    }
                } catch (Exception e) {
                    if (message.equals("")) {
                        message = MessageVarList.PASSPOLICY_PAS_EXPIRY_PERIOD_INVALID;
                    }

                }
            }
        }
        return message;
    }

//    public String list() {
//        System.out.println("called PasswordPolicyAction: List");
//        try {
//
//            int rows = inputBean.getRows();
//            int page = inputBean.getPage();
//            int to = (rows * page);
//            int from = to - rows;
//            long records = 0;
//            String orderBy = "";
//            if (!inputBean.getSidx().isEmpty()) {
//                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
//            }
//            PasswordPolicyDAO dao = new PasswordPolicyDAO();
//            List<PasswordPolicyBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);
//            if (!dataList.isEmpty()) {
//                records = dataList.get(0).getFullCount();
//                inputBean.setRecords(records);
//                inputBean.setGridModel(dataList);
//                int total = (int) Math.ceil((double) records / (double) rows);
//                inputBean.setTotal(total);
//            } else {
//                inputBean.setRecords(0L);
//                inputBean.setTotal(0);
//            }
//
//        } catch (Exception e) {
//            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, e);
//            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Password policy");
//        }
//        return "list";
//    }
    public String reset() {
        System.out.println("called PasswordPolicyAction: Reset");

        Passwordpolicy passwordpolicy;
        PasswordPolicyDAO dao = new PasswordPolicyDAO();
        try {
            if (dao.isExistPasswordPolicy()) {

                passwordpolicy = dao.getPasswordPolicyDetails();
                inputBean.setPpbean(passwordpolicy);

//                System.err.println(passwordpolicy.getPasswordexpiryperiod() + "<<<<<<<<<<<<<<");
//                System.err.println(inputBean.getPasswordexpiryperiod() + "<<<<<<<<<<<<<<");
//                System.err.println(inputBean.getPpbean().getPasswordexpiryperiod() + "<<<<<<<<<<<<<<");
                inputBean.setPasswordpolicyid(String.valueOf(passwordpolicy.getPasswordpolicyid()));
                inputBean.setMinimumlength(String.valueOf(passwordpolicy.getMinimumlength()));
                inputBean.setMaximumlength(String.valueOf(passwordpolicy.getMaximumlength()));
                inputBean.setMinimumspecialcharacters(String.valueOf(passwordpolicy.getMinimumspecialcharacters()));
                inputBean.setMinimumuppercasecharacters(String.valueOf(passwordpolicy.getMinimumuppercasecharacters()));
                inputBean.setMinimumnumericalcharacters(String.valueOf(passwordpolicy.getMinimumnumericalcharacters()));
                inputBean.setMinimumlowercasecharacters(String.valueOf(passwordpolicy.getMinimumlowercasecharacters()));
                inputBean.setRepeatcharactersallow(String.valueOf(passwordpolicy.getRepeatcharactersallow()));
                if (!inputBean.getPasswordpolicyid().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                    inputBean.setInitialpasswordexpirystatus(String.valueOf(passwordpolicy.getInitialpasswordexpirystatus()));
                    inputBean.setPasswordexpiryperiod(String.valueOf(passwordpolicy.getPasswordexpiryperiod()));
                    inputBean.setNoofhistorypassword(String.valueOf(passwordpolicy.getNoofhistorypassword()));
                    inputBean.setMinimumpasswordchangeperiod(String.valueOf(passwordpolicy.getMinimumpasswordchangeperiod()));
                    inputBean.setIdleaccountexpiryperiod(String.valueOf(passwordpolicy.getIdleaccountexpiryperiod()));
                    inputBean.setNoofinvalidloginattempt(String.valueOf(passwordpolicy.getNoofinvalidloginattempt()));
                }

                String oldVal = inputBean.getPpbean().getPasswordpolicyid() + "|"
                        + inputBean.getPpbean().getMinimumlength() + "|"
                        + inputBean.getPpbean().getMaximumlength() + "|"
                        + inputBean.getPpbean().getMinimumspecialcharacters() + "|"
                        + inputBean.getPpbean().getMinimumuppercasecharacters() + "|"
                        + inputBean.getPpbean().getMinimumlowercasecharacters() + "|"
                        + inputBean.getPpbean().getMinimumnumericalcharacters() + "|"
                        + inputBean.getPpbean().getRepeatcharactersallow();
                if (!inputBean.getPasswordpolicyid().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                    oldVal += "|" + inputBean.getPpbean().getPasswordexpiryperiod() + "|"
                            + inputBean.getPpbean().getMinimumpasswordchangeperiod() + "|"
                            + inputBean.getPpbean().getNoofhistorypassword() + "|"
                            + inputBean.getPpbean().getIdleaccountexpiryperiod() + "|"
                            + inputBean.getPpbean().getNoofinvalidloginattempt();
                }
                inputBean.setOldvalue(oldVal);

            } else {
                inputBean.setMessage("Empty password policy id.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Password policy " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "reset";

    }

    public String confirm() {
        System.out.println("called PasswordPolicyAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                PasswordPolicyDAO dao = new PasswordPolicyDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.PAS_POLICY, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmPasswordPolicy(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " password policy");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called PasswordPolicyAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                PasswordPolicyDAO dao = new PasswordPolicyDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.PAS_POLICY, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectPasswordPolicy(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " password policy ");
        }
        return retType;
    }

    public String viewPend() {
        System.out.println("called PasswordPolicyAction : viewPend");
        String message = null;
        String retType = "viewPend";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {

                PasswordPolicyDAO dao = new PasswordPolicyDAO();
                message = dao.viwPendPasswordPolicy(inputBean);
                if (!message.isEmpty()) {
                    addActionError(message);
                }
            } else {
                addActionError("Empty pending task ID.");
            }
        } catch (Exception e) {
            Logger.getLogger(PasswordPolicyAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " password policy  ");
        }
        return retType;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }
}
