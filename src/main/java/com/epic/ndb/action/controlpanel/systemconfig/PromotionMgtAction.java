/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.PromotionMgtBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PromotionMgtInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PromotionMgtPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.PromotionMgtDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Promotions;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class PromotionMgtAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    PromotionMgtInputBean inputBean = new PromotionMgtInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    private String serverPath;

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.PROMOTION_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("findDefImg".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("updateDefImg".equals(method)) {
            task = TaskVarList.UPDATE_DEFAULT_IMG;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewDefImg".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupcsv".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("pendCsvDownloade".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("template".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("upload".equals(method)) {
            task = TaskVarList.UPLOAD_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.PROMOTION_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVupdatedefimg(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVupload(true);
        inputBean.setVdual(true);
        inputBean.setVgenerate(true);
        inputBean.setVgenerateview(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPLOAD_TASK)) {
                    inputBean.setVupload(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_DEFAULT_IMG)) {
                    inputBean.setVupdatedefimg(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                    inputBean.setVgenerateview(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);

    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setPromotionsCategoriesList(dao.getPromotionCategoriesList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called PromotionMgtAction :view");

        } catch (Exception ex) {
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called PromotionMgtAction: List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            PromotionMgtDAO dao = new PromotionMgtDAO();
            List<PromotionMgtBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "Promotion management search using ["
                        + checkEmptyorNullString("Subregion Code", inputBean.getSubregioncode())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + checkEmptyorNullString("Promotions Categories", inputBean.getPromotionsCategoriesSearch())
                        + checkEmptyorNullString("Subregion Address", inputBean.getSubreginaddressSearch())
                        + checkEmptyorNullString("Free Text", inputBean.getFreetextSearch())
                        + checkEmptyorNullString("Info", inputBean.getInfoSearch())
                        + checkEmptyorNullString("Longitude", inputBean.getLongitudeSearch())
                        + checkEmptyorNullString("Latitude", inputBean.getLatitudeSearch())
                        + checkEmptyorNullString("Promotion Conditions", inputBean.getPromoconditionsSearch())
                        + checkEmptyorNullString("Phone No", inputBean.getPhonenoSearch())
                        + checkEmptyorNullString("Merchant Website", inputBean.getMerchantwebsiteSearch())
                        + "] parameters ";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, searchParameters, null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);

                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.PROMOTION_SEARCH_BEAN, inputBean);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called PromotionMgtAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            PromotionMgtDAO dao = new PromotionMgtDAO();
            List<PromotionMgtPendBean> dataList = dao.getPendingPromotionList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called PromotionMgtAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setPromotionsCategoriesList(dao.getPromotionCategoriesList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String viewDefImg() {
        String result = "viewdefimg";
        System.out.println("called PromotionMgtAction : viewDefImg");
        try {
            this.applyUserPrivileges();

            PromotionMgtDAO dao = new PromotionMgtDAO();
            String defImgUrl = dao.findDefaultImageURL();
            inputBean.setImage(defImgUrl);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String viewPopupcsv() {
        String result = "viewpopupcsv";
        System.out.println("called PromotionMgtAction : viewPopupcsv");
        try {
            this.applyUserPrivileges();
            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception e) {
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    public String upload() {
        System.out.println("called PromotionMgtAction : upload");
        String result = "messagecsv";
        ServletContext context = ServletActionContext.getServletContext();

        if (Common.getOS_Type().equals("WINDOWS")) {
            this.setServerPath(context.getRealPath("/resouces/csv_temp/promotionmgt"));
        } else if (Common.getOS_Type().equals("LINUX")) {
            this.setServerPath("/app_conf/Epic/csv_temp/promotionmgt");
        }
        try {
            if (inputBean.getHiddenId() != null) {

                HttpServletRequest request = ServletActionContext.getRequest();
                PromotionMgtDAO dao = new PromotionMgtDAO();

                String message = "";

                DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                Date date = new Date();

                message = this.getFile(inputBean.getConXLFileName()); // get file

                inputBean.setConXLFileName(dateFormat.format(date) + inputBean.getConXLFileName());

                File directory = new File(getServerPath());

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                if (-1 != inputBean.getConXLFileName().lastIndexOf("\\")) {
                    inputBean.setConXLFileName(inputBean.getConXLFileName().substring(inputBean.getConXLFileName().lastIndexOf("\\") + 1));
                }
                File filetoCreate = new File(getServerPath(), inputBean.getConXLFileName());
//                    if (isfileexists==true) {            //set to database
                if (filetoCreate.exists()) {                //set to local 
                    addActionError("File already exists.");
                    System.err.println("File already exists.");
                } else {

                }

                if (message.isEmpty()) {

                    if (inputBean.getConXL() == null) {
                    } else {
                        message = this.validateUpload();
                        if (message.isEmpty()) {
                            FileUtils.copyFile(inputBean.getConXL(), filetoCreate);
                            //                        System.out.println(filetoCreate.getPath());
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPLOAD_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to upload  promotion(s) ", null, null, null);
                            message = dao.uploadPromotion(inputBean, audit);
                        }
                    }

                }

                if (message.isEmpty()) {
                    addActionMessage("File uploaded successfully");
                    System.err.println("File uploaded successfully");

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            addActionError("Promotion Management  " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);

        }
        return result;
    }

    public String pendCsvDownloade() {
        System.out.println("called PromotionMgtAction : pendCsvDownloade");
        String message = null;
        String retType = "pendcsvdownloade";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                PromotionMgtDAO dao = new PromotionMgtDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.VIEW_PENDING_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Pending promtion csv file downloaded by pending task id " + inputBean.getId(), null);
                message = dao.pendPromotionCsvDownloade(inputBean, audit);
                if (!message.isEmpty()) {
                    addActionError(message);
                    retType = "message";
                } else {
                    setInputStream(inputBean.getFileInputStream());
                    setContentLength(inputBean.getFileLength());
                }
            } else {
                addActionError("Empty pending task ID.");
                retType = "message";
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            retType = "message";
        }
        return retType;
    }

    public String viewPend() {
        System.out.println("called PromotionMgtAction : viewPend");
        String message = null;
        String retType = "viewPend";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                PromotionMgtDAO dao = new PromotionMgtDAO();
                message = dao.viwPendPromotion(inputBean);
                if (!message.isEmpty()) {
                    addActionError(message);
                }
            } else {
                addActionError("Empty pending task ID.");
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return retType;
    }

    public String add() {

        System.out.println("called PromotionMgtAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            PromotionMgtDAO dao = new PromotionMgtDAO();

            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getSubregioncode() + "|" + inputBean.getStatus() + "|" + inputBean.getPromotionsCategories() + "|" + inputBean.getSubregionname()
                        + "|" + inputBean.getSubreginaddress() + "|" + inputBean.getFreetext() + "|" + inputBean.getInfo() + "|" + inputBean.getTradinghours()
                        + "|" + inputBean.getLongitude() + "|" + inputBean.getLatitude() + "|" + inputBean.getPromoconditions() + "|" + inputBean.getPhoneno() + "|" + inputBean.getMerchantwebsite() + "|" + inputBean.getFirebaseImgMobUrl()
                        + "|" + inputBean.getRegioncode() + "|" + inputBean.getRegionname() + "|" + inputBean.getMasterregoncode() + "|" + inputBean.getMasterregionname() + "|" + inputBean.getCardtype() + "|" + inputBean.getCardimage()
                        + "|" + inputBean.getStartdate() + "|" + inputBean.getEnddate()
                        + "|" + inputBean.getIspushnotification();
//                        + "|"+ inputBean.getRadius();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to add  promotion ( subregion code : " + inputBean.getSubregioncode() + " )", null, null, newv);

                message = dao.insertPromotion(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + "promotion ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called PromotionMgtAction: Detail");
        Promotions promotion = null;
        try {
            PromotionMgtDAO dao = new PromotionMgtDAO();
            CommonDAO commonDAO = new CommonDAO();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setPromotionsCategoriesList(commonDAO.getPromotionCategoriesList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            if (inputBean.getSubregioncode() != null && !inputBean.getSubregioncode().isEmpty()) {

                promotion = dao.findPromotionById(inputBean.getSubregioncode());
                inputBean.setStatus(promotion.getStatus().getStatuscode());
                inputBean.setPromotionsCategories(promotion.getPromotionsCategories().getCatcode());
                inputBean.setSubregionname(promotion.getSubregionname());
                inputBean.setSubreginaddress(promotion.getSubreginaddress());
                inputBean.setFreetext(promotion.getFreetext());
                inputBean.setInfo(promotion.getInfo());
                inputBean.setTradinghours(promotion.getTradinghours());
                inputBean.setLongitude(promotion.getLongitude().toString());
                inputBean.setLatitude(promotion.getLatitude().toString());
                inputBean.setPromoconditions(promotion.getPromoconditions());
                inputBean.setPhoneno(promotion.getPhoneno());
                inputBean.setMerchantwebsite(promotion.getMerchantwebsite());
                inputBean.setImage(promotion.getImage());
                inputBean.setRegioncode(promotion.getRegioncode());
                inputBean.setRegionname(promotion.getRegionname());
                inputBean.setMasterregoncode(promotion.getMasterregoncode());
                inputBean.setMasterregionname(promotion.getMasterregionname());
                inputBean.setCardtype(promotion.getCardtype());
                inputBean.setCardimage(promotion.getCardimage());
//                try{
//                    inputBean.setRadius(String.format("%.2f", promotion.getRadius()));
//                }catch(Exception e){
//                    inputBean.setRadius("");
//                }
                try {
                    inputBean.setStartdate(sdf.format(promotion.getStartdate()).substring(0, 16));
                } catch (Exception e) {
                    inputBean.setStartdate("");
                }
                try {
                    inputBean.setEnddate(sdf.format(promotion.getEnddate()).substring(0, 16));
                } catch (Exception e) {
                    inputBean.setEnddate("");
                }
                if (promotion.getIspushnotification() != null && promotion.getIspushnotification().equals(Boolean.TRUE)) {
                    inputBean.setIspushnotification("1");
                    inputBean.setIspushnotificationhidden("1");
                }
            } else {
                inputBean.setMessage("Empty subregion code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String viewPopupDetail() {
        System.out.println("called PromotionMgtAction: viewPopupDetail");
        Promotions promotion = null;
        try {
            PromotionMgtDAO dao = new PromotionMgtDAO();
            CommonDAO commonDAO = new CommonDAO();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setPromotionsCategoriesList(commonDAO.getPromotionCategoriesList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            if (inputBean.getSubregioncode() != null && !inputBean.getSubregioncode().isEmpty()) {

                promotion = dao.findPromotionById(inputBean.getSubregioncode());
                inputBean.setStatus(promotion.getStatus().getStatuscode());
                inputBean.setPromotionsCategories(promotion.getPromotionsCategories().getCatcode());
                inputBean.setSubregionname(promotion.getSubregionname());
                inputBean.setSubreginaddress(promotion.getSubreginaddress());
                inputBean.setFreetext(promotion.getFreetext());
                inputBean.setInfo(promotion.getInfo());
                inputBean.setTradinghours(promotion.getTradinghours());
                inputBean.setLongitude(promotion.getLongitude().toString());
                inputBean.setLatitude(promotion.getLatitude().toString());
                inputBean.setPromoconditions(promotion.getPromoconditions());
                inputBean.setPhoneno(promotion.getPhoneno());
                inputBean.setMerchantwebsite(promotion.getMerchantwebsite());
                inputBean.setImage(promotion.getImage());
                inputBean.setRegioncode(promotion.getRegioncode());
                inputBean.setRegionname(promotion.getRegionname());
                inputBean.setMasterregoncode(promotion.getMasterregoncode());
                inputBean.setMasterregionname(promotion.getMasterregionname());
                inputBean.setCardtype(promotion.getCardtype());
                inputBean.setCardimage(promotion.getCardimage());
//                try{
//                    inputBean.setRadius(String.format("%.2f", promotion.getRadius()));
//                }catch(Exception e){
//                    inputBean.setRadius("");
//                }
                try {
                    inputBean.setStartdate(sdf.format(promotion.getStartdate()).substring(0, 16));
                } catch (Exception e) {
                    inputBean.setStartdate("");
                }
                try {
                    inputBean.setEnddate(sdf.format(promotion.getEnddate()).substring(0, 16));
                } catch (Exception e) {
                    inputBean.setEnddate("");
                }
                if (promotion.getIspushnotification() != null && promotion.getIspushnotification().equals(Boolean.TRUE)) {
                    inputBean.setIspushnotification("1");
                    inputBean.setIspushnotificationhidden("1");
                }
            } else {
                inputBean.setMessage("Empty subregion code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "viewpopupdetail";
    }

    public String find() {
        System.out.println("called PromotionMgtAction: Find");
        Promotions promotion = null;
        try {
            if (inputBean.getSubregioncode() != null && !inputBean.getSubregioncode().isEmpty()) {

                PromotionMgtDAO dao = new PromotionMgtDAO();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

                promotion = dao.findPromotionById(inputBean.getSubregioncode());

                inputBean.setStatus(promotion.getStatus().getStatuscode());
                inputBean.setPromotionsCategories(promotion.getPromotionsCategories().getCatcode());
                inputBean.setSubregionname(promotion.getSubregionname());
                inputBean.setSubreginaddress(promotion.getSubreginaddress());
                inputBean.setFreetext(promotion.getFreetext());
                inputBean.setInfo(promotion.getInfo());
                inputBean.setTradinghours(promotion.getTradinghours());
                inputBean.setLongitude(promotion.getLongitude().toString());
                inputBean.setLatitude(promotion.getLatitude().toString());
                inputBean.setPromoconditions(promotion.getPromoconditions());
                inputBean.setPhoneno(promotion.getPhoneno());
                inputBean.setMerchantwebsite(promotion.getMerchantwebsite());
                inputBean.setImage(promotion.getImage());
                inputBean.setRegioncode(promotion.getRegioncode());
                inputBean.setRegionname(promotion.getRegionname());
                inputBean.setMasterregoncode(promotion.getMasterregoncode());
                inputBean.setMasterregionname(promotion.getMasterregionname());
                inputBean.setCardtype(promotion.getCardtype());
                inputBean.setCardimage(promotion.getCardimage());
//                try{
//                    inputBean.setRadius(String.format("%.2f",promotion.getRadius()));
//                }catch(Exception e){
//                    inputBean.setRadius("");
//                }
                try {
                    inputBean.setStartdate(sdf.format(promotion.getStartdate()).substring(0, 16));
                } catch (Exception e) {
                    inputBean.setStartdate("");
                }
                try {
                    inputBean.setEnddate(sdf.format(promotion.getEnddate()).substring(0, 16));
                } catch (Exception e) {
                    inputBean.setEnddate("");
                }
                if (promotion.getIspushnotification() != null && promotion.getIspushnotification().equals(Boolean.TRUE)) {
                    inputBean.setIspushnotification("1");
                    inputBean.setIspushnotificationhidden("1");
                }
            } else {
                inputBean.setMessage("Empty promotion Id.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String findDefImg() {
        System.out.println("called PromotionMgtAction: findDefImg");
        try {
            PromotionMgtDAO dao = new PromotionMgtDAO();
            String defImgUrl = dao.findDefaultImageURL();
            inputBean.setImage(defImgUrl);

        } catch (Exception ex) {
            inputBean.setMessage("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String update() {

        System.out.println("called PromotionMgtAction : update");
        String retType = "message";

        try {
            if (inputBean.getSubregioncode() != null && !inputBean.getSubregioncode().isEmpty()) {
                PromotionMgtDAO dao = new PromotionMgtDAO();

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

                    //Old Value
                    Promotions promotion = dao.findPromotionById(inputBean.getSubregioncode());

                    String startDate = "";
                    String endDate = "";
                    String pushNotificatinEnable = "";
                    String tradinghours = "";
                    String merchantwebsite = "";

                    try {
                        startDate = (sdf.format(promotion.getStartdate()).substring(0, 16));
                    } catch (Exception e) {
                        startDate = "";
                    }
                    try {
                        endDate = (sdf.format(promotion.getEnddate()).substring(0, 16));
                    } catch (Exception e) {
                        endDate = "";
                    }

                    if (promotion.getIspushnotification() != null && promotion.getIspushnotification().equals(Boolean.TRUE)) {
                        pushNotificatinEnable = "1";
                    } else {
                        pushNotificatinEnable = "false";
                    }
                    if (promotion.getTradinghours() != null && !promotion.getTradinghours().isEmpty()) {
                        tradinghours = promotion.getTradinghours();
                    }
                    if (promotion.getMerchantwebsite() != null && !promotion.getMerchantwebsite().isEmpty()) {
                        merchantwebsite = promotion.getMerchantwebsite();
                    }
                    if (promotion.getMerchantwebsite() != null && !promotion.getMerchantwebsite().isEmpty()) {
                        merchantwebsite = promotion.getMerchantwebsite();
                    }

                    String oldVal = promotion.getSubregioncode() + "|" + promotion.getStatus().getStatuscode() + "|" + promotion.getPromotionsCategories().getCatcode() + "|" + promotion.getSubregionname()
                            + "|" + promotion.getSubreginaddress() + "|" + promotion.getFreetext() + "|" + promotion.getInfo() + "|" + tradinghours
                            + "|" + promotion.getLongitude().toString() + "|" + promotion.getLatitude().toString() + "|" + promotion.getPromoconditions() + "|" + promotion.getPhoneno() + "|" + merchantwebsite + "|" + promotion.getImage()
                            + "|" + promotion.getRegioncode() + "|" + promotion.getRegionname() + "|" + promotion.getMasterregoncode() + "|" + promotion.getMasterregionname() + "|" + promotion.getCardtype() + "|" + promotion.getCardimage()
                            + "|" + startDate + "|" + endDate
                            + "|" + pushNotificatinEnable;

                    //New Value
                    String newv = inputBean.getSubregioncode() + "|" + inputBean.getStatus() + "|" + inputBean.getPromotionsCategories() + "|" + inputBean.getSubregionname()
                            + "|" + inputBean.getSubreginaddress() + "|" + inputBean.getFreetext() + "|" + inputBean.getInfo() + "|" + inputBean.getTradinghours()
                            + "|" + inputBean.getLongitude() + "|" + inputBean.getLatitude() + "|" + inputBean.getPromoconditions() + "|" + inputBean.getPhoneno() + "|" + inputBean.getMerchantwebsite() + "|" + inputBean.getFirebaseImgMobUrl()
                            + "|" + inputBean.getRegioncode() + "|" + inputBean.getRegionname() + "|" + inputBean.getMasterregoncode() + "|" + inputBean.getMasterregionname() + "|" + inputBean.getCardtype() + "|" + inputBean.getCardimage()
                            + "|" + inputBean.getStartdate() + "|" + inputBean.getEnddate()
                            + "|" + inputBean.getIspushnotification();

                    System.out.println("OLD value :" + oldVal);
                    System.out.println("NEW value :" + newv);

                    if (!newv.equals(oldVal)) {
                        String newValWithActState =  inputBean.getSubregioncode() + "|" + CommonVarList.STATUS_ACTIVE + "|" + inputBean.getPromotionsCategories() + "|" + inputBean.getSubregionname()
                            + "|" + inputBean.getSubreginaddress() + "|" + inputBean.getFreetext() + "|" + inputBean.getInfo() + "|" + inputBean.getTradinghours()
                            + "|" + inputBean.getLongitude() + "|" + inputBean.getLatitude() + "|" + inputBean.getPromoconditions() + "|" + inputBean.getPhoneno() + "|" + inputBean.getMerchantwebsite() + "|" + inputBean.getFirebaseImgMobUrl()
                            + "|" + inputBean.getRegioncode() + "|" + inputBean.getRegionname() + "|" + inputBean.getMasterregoncode() + "|" + inputBean.getMasterregionname() + "|" + inputBean.getCardtype() + "|" + inputBean.getCardimage()
                            + "|" + inputBean.getStartdate() + "|" + inputBean.getEnddate()
                            + "|" + inputBean.getIspushnotification();
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update  promotion ( subregion code : " + inputBean.getSubregioncode() + " )", null, null, newv);
                            message = dao.updatePromotion(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " promotion ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Promotion Management  " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    public String updateDefImg() {

        System.out.println("called PromotionMgtAction : updateDefImg");
        String retType = "message";

        try {
            PromotionMgtDAO dao = new PromotionMgtDAO();

            String message = this.validateDefImgUpdate();

            if (message.isEmpty()) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String newv = inputBean.getFirebaseImgMobUrl();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_DEFAULT_IMG, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update promotion default image ", null, null, newv);
                message = dao.updateDefImg(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " promotion default image ");
                } else {
                    addActionError(message);
                }

            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Promotion Management  " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    public String delete() {

        System.out.println("called PromotionMgtAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            PromotionMgtDAO dao = new PromotionMgtDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete  promotion ( subregion code : " + inputBean.getSubregioncode() + " )", null);
            message = dao.deletePromotion(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + "promotion ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called PromotionMgtAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                PromotionMgtDAO dao = new PromotionMgtDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmPromotion(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage("Promotion Management " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String reject() {
        System.out.println("called PromotionMgtAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                PromotionMgtDAO dao = new PromotionMgtDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectPromotion(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage("Promotion Management " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    public String template() {

        System.out.println("called PromotionMgtAction: template");
        String retType = "csv";
        FileWriter fileWriter = null;
        BufferedWriter bw = null;
        PrintWriter out = null;
        try {
            ServletContext context = ServletActionContext.getServletContext();
            String destPath = context.getRealPath("/resouces/csv_temp/promotionmgt");
            File fileToDownload = new File(destPath, "promotionmgt.csv");

            try {
                fileWriter = new FileWriter(fileToDownload);
                bw = new BufferedWriter(fileWriter);
                out = new PrintWriter(bw);

                out.println("Subregion Code,Subregion Name,Subregion Category Code,Free Text,promotion Info,Trading hours,Latitude,Longitude,Promotion Conditions,Phone Number,Merchant Website,Region code,Region Name,Master Region Code,Master Region Name,Card Type,Card Image,Start Date(dd/MM/yyyy HH:mm),End Date(dd/MM/yyyy HH:mm),Push Notification Enabled(1/0),Subregion Address");
                out.println("PR1,Test,SHP,10% Discount,10% Discount on total bill,12,6.873262,79.887582,Till 29th Feb 2016,0766995540,www.akira.lk,CMB,Colombo,CE,Central,Credit and Debit Cards,Credit,30/08/2019 08:15,25/09/2019 19:45,1,Test Address");

//                 
            } catch (Exception e) {
                retType = "message";
                addActionError("Promotion Management  " + MessageVarList.COMMON_ERROR_PROCESS);
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (fileWriter != null) {
                        fileWriter.close();
                    }
                } catch (Exception e) {
                }
            }

            setInputStream(new FileInputStream(fileToDownload));
            setFileName(fileToDownload.getName());
            setContentLength(fileToDownload.length());

        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            retType = "message";
        }

        return retType;
    }

    public String reportGenerate() {

        System.out.println("called PromotionMgtAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;

        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                PromotionMgtDAO dao = new PromotionMgtDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;

//                HttpServletResponse response = ServletActionContext.getResponse();
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    PromotionMgtInputBean searchBean = (PromotionMgtInputBean) session.getAttribute(SessionVarlist.PROMOTION_SEARCH_BEAN);
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new PromotionMgtInputBean());
                    }

//                    response.setContentType("text/csv");
//                    response.setHeader("Content-Disposition", "attachment; filename=\"Promotion_Mgt_Report.csv\"");
                    try {
//                        OutputStream outputStream = response.getOutputStream();
//                        String outputResult = sb.toString();
//                        outputStream.write(outputResult.getBytes());
//                        outputStream.flush();
//                        outputStream.close();
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Promotion_Mgt_Report.csv");
                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.PROMOTION_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Promotion csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger
                            .getLogger(PromotionMgtAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Promotion Management");

            return "message";
        }
        return retMsg;
    }

    public String getFireBaseParam() {
        System.out.println("called PromotionMgtAction: getFireBaseParam");
        String message = "";

        try {
            PromotionMgtDAO dao = new PromotionMgtDAO();

            message = dao.setFirebaseParam(inputBean);

            if (!message.isEmpty()) {
                inputBean.setMessage(message);
            }
        } catch (Exception ex) {
            inputBean.setMessage("Promotion " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "find";

    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getSubregioncode() == null || inputBean.getSubregioncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONCODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getSubregioncode())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_SUBREGIONCODE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_STATUS;
        } else if (inputBean.getPromotionsCategories() != null && inputBean.getPromotionsCategories().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PROMOTIONCATEGORY;
        } else if (inputBean.getSubregionname() == null || inputBean.getSubregionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONNAME;
        } else if (inputBean.getSubreginaddress() == null || inputBean.getSubreginaddress().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONADDRESS;
        } else if (inputBean.getFreetext() == null || inputBean.getFreetext().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_FREETEXT;
        } else if (inputBean.getInfo() == null || inputBean.getInfo().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_INFO;
        } else if (inputBean.getLatitude() == null || inputBean.getLatitude().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_LATITUDE;
        } else if (!Validation.isValidFloatValue(inputBean.getLatitude())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_LATITUDE;
        } else if (inputBean.getLongitude() == null || inputBean.getLongitude().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_LONGITUDE;
        } else if (!Validation.isValidFloatValue(inputBean.getLongitude())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_LONGITUDE;
//        } else if (inputBean.getRadius() == null || inputBean.getRadius().trim().isEmpty()) {
//                    message = MessageVarList.PROMOTION_MGT_EMPTY_RADIOUS;
//        } else if (!Validation.isValidFloatValue(inputBean.getRadius().trim())) {
//                    message = MessageVarList.PROMOTION_MGT_EMPTY_RADIOUS;
        } else if (inputBean.getPromoconditions() == null || inputBean.getPromoconditions().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PROMOCONDITION;
        } else if (inputBean.getPhoneno() == null || inputBean.getPhoneno().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PHONENO;
//        } else if (inputBean.getMerchantwebsite() == null || inputBean.getMerchantwebsite().trim().isEmpty()) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_MERCHANTWEBSITE;
        } else if (inputBean.getRegioncode() == null || inputBean.getRegioncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_REGIONCODE;
        } else if (inputBean.getRegionname() == null || inputBean.getRegionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_REGIONNAME;
        } else if (inputBean.getMasterregoncode() == null || inputBean.getMasterregoncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_MASTERREGIONCODE;
        } else if (inputBean.getMasterregionname() == null || inputBean.getMasterregionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_MASTERREGIONNAME;
        } else if (inputBean.getCardtype() == null || inputBean.getCardtype().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_CARDTYPE;
        } else if (inputBean.getCardimage() == null || inputBean.getCardimage().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_CARDIMAGE;
        } else if (inputBean.getStartdate() == null || inputBean.getStartdate().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_STARTDATE;
        } else if (inputBean.getEnddate() == null || inputBean.getEnddate().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_ENDDATE;
        } else if ((inputBean.getMobileImgFileName() != null && !inputBean.getMobileImgFileName().trim().isEmpty())
                && (inputBean.getFirebaseImgMobUrl() == null || inputBean.getFirebaseImgMobUrl().trim().isEmpty())) {
            message = MessageVarList.PROMOTION_MOBILE_IMAGE_URL_EMPTY;
        } else if (inputBean.getHasImage().equals("IMG")) {
            message = MessageVarList.PROMOTION_MOBILE_IMAGE_FIREBASE;
        } else if ((inputBean.getStartdate() != null || !inputBean.getStartdate().trim().isEmpty())
                && (inputBean.getEnddate() != null || !inputBean.getEnddate().trim().isEmpty())) {
            try {
                Date fromDate = Common.convertStringtoDate3(inputBean.getStartdate());
                Date toDate = Common.convertStringtoDate3(inputBean.getEnddate());

                if (fromDate.after(toDate)) {
                    message = MessageVarList.PROMOTION_MGT_INVALID_DATERANGE;
                }
            } catch (Exception ex) {
            }
        }

        return message;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getSubregioncode() == null || inputBean.getSubregioncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONCODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getSubregioncode())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_SUBREGIONCODE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_STATUS;
        } else if (inputBean.getPromotionsCategories() != null && inputBean.getPromotionsCategories().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PROMOTIONCATEGORY;
        } else if (inputBean.getSubregionname() == null || inputBean.getSubregionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONNAME;
        } else if (inputBean.getSubreginaddress() == null || inputBean.getSubreginaddress().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONADDRESS;
        } else if (inputBean.getFreetext() == null || inputBean.getFreetext().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_FREETEXT;
        } else if (inputBean.getInfo() == null || inputBean.getInfo().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_INFO;
//        } else if (inputBean.getTradinghours()== null || inputBean.getTradinghours().trim().isEmpty()) {
//            message = MessageVarList.BRANCH_MGT_EMPTY_MOBILE;
        } else if (inputBean.getLatitude() == null || inputBean.getLatitude().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_LATITUDE;
        } else if (!Validation.isValidFloatValue(inputBean.getLatitude().trim())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_LATITUDE;
        } else if (inputBean.getLongitude() == null || inputBean.getLongitude().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_LONGITUDE;
        } else if (!Validation.isValidFloatValue(inputBean.getLongitude().trim())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_LONGITUDE;
//        } else if (inputBean.getRadius() == null || inputBean.getRadius().trim().isEmpty()) {
//                    message = MessageVarList.PROMOTION_MGT_EMPTY_RADIOUS;
//        } else if (!Validation.isValidFloatValue(inputBean.getRadius().trim())) {
//                    message = MessageVarList.PROMOTION_MGT_EMPTY_RADIOUS;
        } else if (inputBean.getPromoconditions() == null || inputBean.getPromoconditions().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PROMOCONDITION;
        } else if (inputBean.getPhoneno() == null || inputBean.getPhoneno().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PHONENO;
//        } else if (inputBean.getMerchantwebsite() == null || inputBean.getMerchantwebsite().trim().isEmpty()) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_MERCHANTWEBSITE;
//        } else if (inputBean.getImage() == null || inputBean.getImage().trim().isEmpty()) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_IMAGE;
        } else if (inputBean.getRegioncode() == null || inputBean.getRegioncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_REGIONCODE;
        } else if (inputBean.getRegionname() == null || inputBean.getRegionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_REGIONNAME;
        } else if (inputBean.getMasterregoncode() == null || inputBean.getMasterregoncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_MASTERREGIONCODE;
        } else if (inputBean.getMasterregionname() == null || inputBean.getMasterregionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_MASTERREGIONNAME;
        } else if (inputBean.getCardtype() == null || inputBean.getCardtype().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_CARDTYPE;
        } else if (inputBean.getCardimage() == null || inputBean.getCardimage().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_CARDIMAGE;
        } else if (inputBean.getStartdate() == null || inputBean.getStartdate().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_STARTDATE;
        } else if (inputBean.getEnddate() == null || inputBean.getEnddate().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_ENDDATE;
        } else if ((inputBean.getMobileImgFileName() != null && !inputBean.getMobileImgFileName().trim().isEmpty())
                && (inputBean.getFirebaseImgMobUrl() == null || inputBean.getFirebaseImgMobUrl().trim().isEmpty())) {
            message = MessageVarList.PROMOTION_MOBILE_IMAGE_URL_EMPTY;
        } else if (inputBean.getHasImage().equals("IMG")) {
            message = MessageVarList.PROMOTION_MOBILE_IMAGE_FIREBASE;
        } else if ((inputBean.getStartdate() != null || !inputBean.getStartdate().trim().isEmpty())
                && (inputBean.getEnddate() != null || !inputBean.getEnddate().trim().isEmpty())) {
            try {
                Date fromDate = Common.convertStringtoDate3(inputBean.getStartdate());
                Date toDate = Common.convertStringtoDate3(inputBean.getEnddate());

                if (fromDate.after(toDate)) {
                    message = MessageVarList.PROMOTION_MGT_INVALID_DATERANGE;
                }
            } catch (Exception ex) {
            }
        }

        return message;
    }

    private String validateDefImgUpdate() {
        String message = "";
//        if (inputBean.getPromoid() == null || inputBean.getPromoid().trim().isEmpty()) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_PROMOID;
//        } else 
        if (inputBean.getHasImage().equals("NOIMG") && (inputBean.getFirebaseImgMobUrl() == null || inputBean.getFirebaseImgMobUrl().trim().isEmpty())) {
            message = MessageVarList.PROMOTION_MOBILE_DEFAULT_IMAGE_EMPTY;
        } else if (inputBean.getHasImage().equals("IMG")) {
            message = MessageVarList.PROMOTION_MOBILE_IMAGE_FIREBASE;
        }

        return message;
    }

    private String validateUpload() throws Exception {
        String message = "";
        FileReader isr = null;
        BufferedReader br = null;
        Session hSession = null;
        File csvFile = null;

        try {

            String[] parts = new String[0];
            int countrecord = 1;
            int succesrec = 0;
            String thisLine = null;
            boolean getline = false;
            String token = "";
            PromotionMgtDAO dao = new PromotionMgtDAO();

            csvFile = inputBean.getConXL();
            isr = new FileReader(csvFile);
            br = new BufferedReader(isr);
            hSession = HibernateInit.sessionFactory.openSession();

            while ((thisLine = br.readLine()) != null) {
                if (thisLine.trim().equals("")) {
                    continue;
                } else {
                    if (getline) {
                        token = thisLine;

//                        System.err.println(token);
                        try {
                            parts = token.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", 21);
                            inputBean.setSubregioncode(Common.removeCsvDoubleQuotation(parts[0].trim()));
                            inputBean.setSubregionname(Common.removeCsvDoubleQuotation(parts[1].trim()));
                            inputBean.setPromotionsCategories(Common.removeCsvDoubleQuotation(parts[2].trim()));
                            inputBean.setFreetext(Common.removeCsvDoubleQuotation(parts[3].trim()));
                            inputBean.setInfo(Common.removeCsvDoubleQuotation(parts[4].trim()));
                            inputBean.setTradinghours(Common.removeCsvDoubleQuotation(parts[5]));
                            inputBean.setLatitude(Common.removeCsvDoubleQuotation(parts[6].trim()));
                            inputBean.setLongitude(Common.removeCsvDoubleQuotation(parts[7].trim()));
//                            inputBean.setRadius(Common.removeCsvDoubleQuotation(parts[8].trim()));
                            inputBean.setPromoconditions(Common.removeCsvDoubleQuotation(parts[8].trim()));
                            inputBean.setPhoneno(Common.removeCsvDoubleQuotation(parts[9].trim()));
                            inputBean.setMerchantwebsite(Common.removeCsvDoubleQuotation(parts[10].trim()));
                            inputBean.setRegioncode(Common.removeCsvDoubleQuotation(parts[11].trim()));
                            inputBean.setRegionname(Common.removeCsvDoubleQuotation(parts[12].trim()));
                            inputBean.setMasterregoncode(Common.removeCsvDoubleQuotation(parts[13].trim()));
                            inputBean.setMasterregionname(Common.removeCsvDoubleQuotation(parts[14].trim()));
                            inputBean.setCardtype(Common.removeCsvDoubleQuotation(parts[15].trim()));
                            inputBean.setCardimage(Common.removeCsvDoubleQuotation(parts[16].trim()));
                            inputBean.setStartdate(Common.removeCsvDoubleQuotation(parts[17].trim()));
                            inputBean.setEnddate(Common.removeCsvDoubleQuotation(parts[18].trim()));
                            inputBean.setIspushnotification(Common.removeCsvDoubleQuotation(parts[19].trim()));
                            inputBean.setSubreginaddress(Common.removeCsvDoubleQuotation(parts[20].trim()));
                            inputBean.setStatus(CommonVarList.STATUS_ACTIVE);

                        } catch (Exception ee) {
                            message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                            break;
                        }
                        countrecord++;
                        if (parts.length >= 21 && message.isEmpty()) {
                            message = dao.validateInputsForCSV(inputBean);
                            if (message.isEmpty()) {
                                message = dao.validateUpload(inputBean, hSession);
                                if (message.isEmpty()) {
                                    succesrec++;
                                } else {
                                    message = message + " at line number " + countrecord;
//                                            + ",success count :" + succesrec;
                                    break;
                                }
                            } else {
                                message = message + " at line number " + countrecord;
//                                        + ",success count :" + succesrec;
                                break;
                            }

                        } else {
                            message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                            break;
                        }
                    } else {
                        getline = true;
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }

            if (hSession != null) {
                hSession.flush();
                hSession.close();
            }

        }
        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public String getFile(String file) {
        String msgEx = "";
        if (file == null) {
            msgEx = "Please choose a file to upload.";
        } else {
            msgEx = Validation.isCSV(file);
        }
        return msgEx;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

}
