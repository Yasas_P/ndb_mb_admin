package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.FAQBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FAQInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FAQPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.FAQDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Faq;
import com.epic.ndb.util.mapping.FaqTemp;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.SectionVarList;

//
/**
 *
 * @author jayathissa_d
 */
public class FAQAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    FAQInputBean inputBean = new FAQInputBean();

    /**
     *
     * @return
     */
    public Object getModel() {
        return inputBean;
    }

    /**
     *
     * @return
     */
    public String execute() {
        System.out.println("called FAQAction : execute");
        return SUCCESS;
    }

    /**
     *
     * @param method
     * @param userRole
     * @return
     */
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.FAQ_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("Delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("ViewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("viewPopupDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.FAQ_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVdual(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    /**
     *
     * @return
     */
    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called FAQAction :view");

        } catch (Exception ex) {
            addActionError("FAQ " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     *
     * @return
     */
    public String List() {
        System.out.println("called FAQAction: List");
        try {
            //if (inputBean.isSearch()) {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            FAQDAO dao = new FAQDAO();
            List<FAQBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("ID", inputBean.getIdSearch())
                        + checkEmptyorNullString("Question", inputBean.getQuestionSearch())
                        + checkEmptyorNullString("Answer", inputBean.getAnswerSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.FAQ_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "FAQ management search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" FAQ " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    /**
     *
     * @return
     */
    public String ApproveList() {
        System.out.println("called FAQAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            FAQDAO dao = new FAQDAO();
            List<FAQPendBean> dataList = dao.getPendingFAQList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.FAQ_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("FAQ " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    /**
     *
     * @return
     */
    public String ViewPopup() {
        String result = "viewpopup";
        System.out.println("called FAQAction : ViewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called FAQAction :ViewPopup");

        } catch (Exception ex) {
            addActionError("FAQ " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     *
     * @return
     */
    public String Add() {
        System.out.println("called FAQAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            FAQDAO dao = new FAQDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getId() + "|"
                        + inputBean.getStatus() + "|"
                        + inputBean.getQuestion() + "|"
                        + inputBean.getAnswer();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.FAQ_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to add FAQ (ID: " + inputBean.getId() + ") ", null, null, newv);

                message = dao.insertFAQ(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " FAQ ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("FAQ " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getId() == null || inputBean.getId().trim().isEmpty()) {
            message = MessageVarList.FAQ_MGT_EMPTY_ID;
        } else if (!Validation.isNumeric(inputBean.getId())) {
            message = MessageVarList.FAQ_MGT_INVALID_ID;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().isEmpty()) {
            message = MessageVarList.FAQ_MGT_EMPTY_STATUS;
        } else if (inputBean.getQuestion() == null || inputBean.getQuestion().trim().isEmpty()) {
            message = MessageVarList.FAQ_MGT_EMPTY_QUESTION;
        } else if (inputBean.getAnswer() == null || inputBean.getAnswer().trim().isEmpty()) {
            message = MessageVarList.FAQ_MGT_EMPTY_ANSWER;
        }

        return message;
    }

    /**
     *
     * @return
     */
    public String Delete() {
        System.out.println("called FAQAction : Delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            FAQDAO dao = new FAQDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.FAQ_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete FAQ(ID: " + inputBean.getId() + ") ", null);
            message = dao.deleteFAQ(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " FAQ ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
//            inputBean.setMessage(MessageVarList.COMMON_ERROR_DELETE);
        }
        return retType;
    }

    /**
     *
     * @return
     */
    public String Detail() {
        System.out.println("called FAQAction: Detail");
        Faq faq = null;
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {

                FAQDAO dao = new FAQDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                faq = dao.findFAQById(inputBean.getId());

                inputBean.setId(faq.getId().toString());
                inputBean.setQuestion(faq.getQuestion());
                inputBean.setAnswer(faq.getAnswer());
                inputBean.setStatus(faq.getStatus().getStatuscode());
                inputBean.setOldvalue(faq.getId().toString() + "|"
                        + faq.getStatus().getStatuscode() + "|"
                        + faq.getQuestion() + "|"
                        + faq.getAnswer());

            } else {
                inputBean.setMessage("Empty FAQ ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("FAQ" + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    /**
     *
     * @return
     */
    public String Update() {

        System.out.println("called FAQAction : update");
        String retType = "message";

        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                FAQDAO dao = new FAQDAO();
                inputBean.setId(inputBean.getId());

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getId() + "|"
                            + inputBean.getStatus() + "|"
                            + inputBean.getQuestion() + "|"
                            + inputBean.getAnswer();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);
                    if (!newv.equals(oldVal)) {
                        String newValWithActState = inputBean.getId() + "|"
                            + CommonVarList.STATUS_ACTIVE + "|"
                            + inputBean.getQuestion() + "|"
                            + inputBean.getAnswer();
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.FAQ_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update FAQ(ID: " + inputBean.getId() + ") ", null, oldVal, newv);
                            message = dao.updateFAQ(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " FAQ ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("FAQ " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getId() == null || inputBean.getId().trim().isEmpty()) {
            message = MessageVarList.FAQ_MGT_EMPTY_ID;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().isEmpty()) {
            message = MessageVarList.FAQ_MGT_EMPTY_STATUS;
        } else if (inputBean.getQuestion() == null || inputBean.getQuestion().trim().isEmpty()) {
            message = MessageVarList.FAQ_MGT_EMPTY_QUESTION;
        } else if (inputBean.getAnswer() == null || inputBean.getAnswer().trim().isEmpty()) {
            message = MessageVarList.FAQ_MGT_EMPTY_ANSWER;
        }
        return message;
    }

    /**
     *
     * @return
     */
    public String Confirm() {
        System.out.println("called FAQAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                FAQDAO dao = new FAQDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.FAQ_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmFAQ(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage("FAQ " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    /**
     *
     * @return
     */
    public String Reject() {
        System.out.println("called FAQAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                FAQDAO dao = new FAQDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.FAQ_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectFAQ(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" FAQ " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    /**
     *
     * @return
     */
    public String Find() {
        System.out.println("called FAQAction: Find");
        Faq faq = null;
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {

                FAQDAO dao = new FAQDAO();

                faq = dao.findFAQById(inputBean.getId());

                inputBean.setId(faq.getId().toString());
                inputBean.setQuestion(faq.getQuestion());
                inputBean.setAnswer(faq.getAnswer());
                inputBean.setStatus(faq.getStatus().getStatuscode());

            } else {
                inputBean.setMessage("Empty FAQ ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("FAQ " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    /**
     *
     * @return
     */
    public String viewPopupDetail() {
        System.out.println("called FAQAction: viewPopupDetail");
        Faq faq = null;
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {

                FAQDAO dao = new FAQDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                faq = dao.findFAQById(inputBean.getId());

                inputBean.setId(faq.getId().toString());
                inputBean.setQuestion(faq.getQuestion());
                inputBean.setAnswer(faq.getAnswer());
                inputBean.setStatus(faq.getStatus().getStatuscode());
            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("FAQ " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "viewpopupdetail";
    }

    /**
     *
     * @return
     */
    public String viewPend() {
        System.out.println("called FAQAction: viewPend");
        FaqTemp faqTemp = null;
        Faq faq = null;
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {

                FAQDAO dao = new FAQDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

                faqTemp = dao.findFAQTempById(inputBean.getId());
                inputBean.setTask(faqTemp.getTask().getDescription());
                        
                FAQBean newValBean =new FAQBean();
                newValBean.setId(faqTemp.getId().toString());
                newValBean.setQuestion(faqTemp.getQuestion());
                newValBean.setAnswer(faqTemp.getAnswer());
                newValBean.setStatus(faqTemp.getStatus().getStatuscode());
                inputBean.setNewFaq(newValBean);
                        
                faq = dao.findFAQById(inputBean.getId());
                FAQBean oldValBean =new FAQBean();
                oldValBean.setId(faq.getId().toString());
                oldValBean.setQuestion(faq.getQuestion());
                oldValBean.setAnswer(faq.getAnswer());
                oldValBean.setStatus(faq.getStatus().getStatuscode());
                inputBean.setOldFaq(oldValBean);
                
            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("FAQ " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FAQAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "viewPend";
    }

}
