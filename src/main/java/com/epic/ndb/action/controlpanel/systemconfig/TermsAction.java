/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.CategoryBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TermsBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TermsConditionPendBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TermsInputBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.TermsDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class TermsAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    TermsInputBean inputBean = new TermsInputBean();

    @Override
    public String execute() throws Exception {
        System.out.println("called TermsAction : execute");
        return SUCCESS;
    }

    public TermsInputBean getModel() {
        return inputBean;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.TERMS_PAGE;
        String task = null;
        if ("View".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("Addnew".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("Delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("FindVersionList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("ViewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("LoadVersion".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("LoadTempVersion".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }

        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {

        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.TERMS_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVdual(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);

    }

    private List<CategoryBean> getCategoryList() {
        List<CategoryBean> cat = new ArrayList<CategoryBean>();

        CategoryBean category = new CategoryBean();
        category.setKey("NORMAL");
        category.setValue("Normal Payment");
        cat.add(category);
        category = new CategoryBean();
        category.setKey("SMALL");
        category.setValue("Small Payment");
        cat.add(category);

        return cat;
    }

    public String View() {

        String result = "view";

        try {

            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();

            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
            inputBean.setCategoryList(this.getCategoryList());
            inputBean.setVersionMap(dao.getVersionList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

            System.out.println("called TermsAction :view");

        } catch (Exception ex) {
            addActionError("Terms " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(TermsAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String FindVersionList() {
        try {
            CommonDAO dao = new CommonDAO();
            inputBean.setVersionMap(dao.getVersionList());
        } catch (Exception ex) {
            inputBean.setMessage("Terms " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(TermsAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "find";
    }

    public String LoadVersion() {
        System.out.println("called TermsAction: LoadVersion");
        TermsBean terms = null;

        try {
            TermsDAO dao = new TermsDAO();

            terms = dao.findTermsById(inputBean.getVersionno());
            inputBean.setDescription(terms.getTerms());
            inputBean.setVersionno(terms.getVersionno());
            inputBean.setStatus(terms.getStatus());
            inputBean.setStatusAct(terms.getStatus());
            inputBean.setCategory(terms.getCategory());

            inputBean.setOldvalue(inputBean.getVersionno() + "|"
                    + terms.getTerms() + "|"
                    + terms.getStatus());

        } catch (Exception ex) {
            inputBean.setMessage("Terms " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(TermsAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "find";

    }

    public String LoadTempVersion() {
        System.out.println("called TermsAction: LoadTempVersion");
        TermsBean terms = null;

        try {
            TermsDAO dao = new TermsDAO();

            terms = dao.findTermsTempById(inputBean.getVersionno());
            inputBean.setDescription(terms.getTerms());
            inputBean.setVersionno(terms.getVersionno());
            inputBean.setStatus(terms.getStatus());
            inputBean.setStatusAct(terms.getStatus());
            inputBean.setCategory(terms.getCategory());

        } catch (Exception ex) {
            inputBean.setMessage("Terms " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(TermsAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "find";

    }

    public String Update() {

        System.out.println("called TermsAction : update");
        String retType = "message";
        TermsBean terms = null;

        try {
            String message = this.validateUpdateInputs();

            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                TermsDAO dao = new TermsDAO();
                terms = dao.findTermsById(inputBean.getVersionno());

                if (inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && !terms.getTerms().equals(inputBean.getDescription())) {
                    addActionError(MessageVarList.TERMS_EMPTY_DESCRIPTION_CHANGE_INACTIVE_STATE);
                    inputBean.setMessage(MessageVarList.TERMS_EMPTY_DESCRIPTION_CHANGE_INACTIVE_STATE);
                } else {

                    StringBuilder auditDescription = new StringBuilder();

                    /**
                     * for adult audit description
                     */
                    if (terms.getTerms().equals(inputBean.getDescription())) {

                        auditDescription.append("Requested to update terms and conditions version ( version number : ")
                                .append(inputBean.getVersionno())
                                .append(" ) without modifications");
                    } else {

                        auditDescription.append("Requested to update terms and conditions version ( version number : ")
                                .append(inputBean.getVersionno())
                                .append(" ) with modifications");
                    }

                    String newV = inputBean.getVersionno()+ "|"
                            + inputBean.getDescription() + "|"
                            + inputBean.getStatus();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newV.trim());
                    System.out.println("oldVal :" + oldVal.trim());

                    if (!newV.trim().equals(oldVal.trim())) {
                        String newValWithActState = inputBean.getVersionno()+ "|"
                            + inputBean.getDescription() + "|"
                            + CommonVarList.STATUS_ACTIVE;
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.TERMS_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, auditDescription.toString(), null, null, null);
                            message = dao.updateTerms(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " terms and conditions ");
                                inputBean.setMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " terms and conditions ");
                            } else {
                                addActionError(message);
                                inputBean.setMessage(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                }
            } else {
                addActionError(message);
            }
        } catch (Exception ex) {
            Logger.getLogger(TermsAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Terms " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    public String Add() {
        System.out.println("called TermsAction : add");
        String result = "messageadd";

        try {

            HttpServletRequest request = ServletActionContext.getRequest();
            TermsDAO dao = new TermsDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                StringBuilder auditDescription = new StringBuilder();

                /**
                 * for audit description
                 */
                auditDescription.append("Requested to add terms and conditions version ( version number : ")
                        .append(inputBean.getVersionnoadd())
                        .append(" ) ");

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.TERMS_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, auditDescription.toString(), null, null, null);

                message = dao.insertTerms(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " terms and conditions ");
                } else {
                    addActionError(message);
                }

            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Terms " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(TermsAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String Delete() {
        System.out.println("called TermsAction : delete");
        String retType = "delete";
        String message = "";

        try {
            message = this.validateDeleteInputs();

            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                TermsDAO dao = new TermsDAO();
                StringBuilder auditDescription = new StringBuilder();

                auditDescription.append("Requested to delete terms and conditions version ( version number : ")
                        .append(inputBean.getVersionno())
                        .append(" )");

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.TERMS_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, auditDescription.toString(), null, null, null);
                message = dao.deleteTerms(inputBean, audit);

                if (message.isEmpty()) {
                    inputBean.setMessage(MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " terms and conditions ");
                } else {
                    inputBean.setErrorMessage(message);
                }
            } else {
                inputBean.setErrorMessage(message);
            }

        } catch (Exception ex) {
            Logger.getLogger(TermsAction.class.getName()).log(Level.SEVERE, null, ex);
            inputBean.setErrorMessage("Terms " + MessageVarList.COMMON_ERROR_DELETE);
        }

        return retType;
    }

    private String validateUpdateInputs() throws Exception {
        String message = "";

        if (inputBean.getVersionno() == null || inputBean.getVersionno().trim().isEmpty()) {
            message = MessageVarList.TERMS_EMPTY_VERSION;
        } else if (inputBean.getStatus() == null && !inputBean.getVersionno().isEmpty()) {
            inputBean.setStatus("ACT");
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.TERMS_EMPTY_STATUS;
        } else if (inputBean.getDescriptiontxt() == null || inputBean.getDescriptiontxt().trim().isEmpty()) {
            message = MessageVarList.TERMS_EMPTY_DESCRIPTION;
        }
        return message;
    }

    private String validateDeleteInputs() throws Exception {
        String message = "";

        if (inputBean.getVersionno() == null || inputBean.getVersionno().trim().isEmpty()) {
            message = MessageVarList.TERMS_EMPTY_VERSION;
        }
        return message;
    }

    private String validateInputs() throws Exception {
        String message = "";
        if (inputBean.getVersionnoadd() == null || inputBean.getVersionnoadd().trim().isEmpty()) {
            message = MessageVarList.TERMS_EMPTY_VERSION;
        } else if (inputBean.getStatusadd() == null || inputBean.getStatusadd().trim().isEmpty()) {
            message = MessageVarList.TERMS_EMPTY_STATUS;
        } else if (inputBean.getDescriptionaddtxt() == null || inputBean.getDescriptionaddtxt().trim().isEmpty()) {
            message = MessageVarList.TERMS_EMPTY_DESCRIPTION;
        }
        return message;
    }

    public String ApproveList() {
        System.out.println("called TermsAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            TermsDAO dao = new TermsDAO();
            List<TermsConditionPendBean> dataList = dao.getPendingTermsList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.TERMS_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending terms and conditions list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(TermsAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Terms " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String Confirm() {
        System.out.println("called TermsAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                TermsDAO dao = new TermsDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.TERMS_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmTerms(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrorMessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Terms " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called TermsAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                TermsDAO dao = new TermsDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.TERMS_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectTerms(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrorMessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(SegmentAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Terms " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }
}
