/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.ProductTypeBean;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductTypeInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductTypePendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.ProductTypeDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.ProductType;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class ProductTypeAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    ProductTypeInputBean inputBean = new ProductTypeInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.PRODUCT_TYPE_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.PRODUCT_TYPE_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVgenerate(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setProductcategoryList(dao.getProductCategoryList());
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called ProductTypeAction : view");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product type ");
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called ProductTypeAction : List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            ProductTypeDAO dao = new ProductTypeDAO();
            List<ProductTypeBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "Product type search using ["
                        + checkEmptyorNullString("Product type", inputBean.getProductTypeSearch())
                        + checkEmptyorNullString("Product name", inputBean.getProductNameSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + checkEmptyorNullString("Product Category", inputBean.getProductcategorySearch())
                        + "] parameters ";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PRODUCT_TYPE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, searchParameters, null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);

                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.PRODUCT_TYPE_SEARCH_BEAN, inputBean);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product type ");
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called ProductTypeAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            ProductTypeDAO dao = new ProductTypeDAO();
            List<ProductTypePendBean> dataList = dao.getPendingProductTypeList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PRODUCT_TYPE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product type ");
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called ProductTypeAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setProductcategoryList(dao.getProductCategoryList());
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product type ");
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String add() {

        System.out.println("called ProductTypeAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            ProductTypeDAO dao = new ProductTypeDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getProductType().trim() + "|" + inputBean.getProductName().trim() + "|" + inputBean.getStatus().trim() + "|" + inputBean.getProductcategory().trim();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.PRODUCT_TYPE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Requested to add product type ( product type: " + inputBean.getProductType() + ")", null, null, newv);

                message = dao.insertProductType(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + "product type ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product type");
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called ProductTypeAction: Detail");
        ProductType productType = null;
        try {
            if (inputBean.getProductType() != null && !inputBean.getProductType().isEmpty()) {

                ProductTypeDAO dao = new ProductTypeDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setProductcategoryList(commonDAO.getProductCategoryList());
                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                productType = dao.findProductTypeById(inputBean.getProductType());

                inputBean.setProductName(productType.getProductName());
                if (productType.getStatus() != null) {
                    inputBean.setStatus(productType.getStatus().getStatuscode());
                }
                if (productType.getProductcategory() != null) {
                    inputBean.setProductcategory(productType.getProductcategory().getCode());
                }

                inputBean.setOldvalue(inputBean.getProductType() + "|"
                        + productType.getProductName() + "|"
                        + productType.getStatus().getStatuscode() + "|"
                        + productType.getProductcategory().getCode());

            } else {
                addActionError("Empty product type.");
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product type ");
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String find() {
        System.out.println("called ProductTypeAction: find");
        ProductType productType = null;
        try {
            if (inputBean.getProductType() != null && !inputBean.getProductType().isEmpty()) {

                ProductTypeDAO dao = new ProductTypeDAO();

                productType = dao.findProductTypeById(inputBean.getProductType());

                inputBean.setProductName(productType.getProductName());
                if (productType.getStatus() != null) {
                    inputBean.setStatus(productType.getStatus().getStatuscode());
                }
                if (productType.getProductcategory() != null) {
                    inputBean.setProductcategory(productType.getProductcategory().getCode());
                }

            } else {
                inputBean.setMessage("Empty product type.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " product type ");
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String update() {

        System.out.println("called ProductTypeAction : update");
        String retType = "message";

        try {
            if (inputBean.getProductType() != null && !inputBean.getProductType().isEmpty()) {
                ProductTypeDAO dao = new ProductTypeDAO();

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getProductType().trim() + "|" + inputBean.getProductName().trim() + "|" + inputBean.getStatus().trim() + "|" + inputBean.getProductcategory();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) {
                        String newValWithActState = inputBean.getProductType().trim() + "|" + inputBean.getProductName().trim() + "|" + CommonVarList.STATUS_ACTIVE + "|" + inputBean.getProductcategory();
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.PRODUCT_TYPE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update product type (product type: " + inputBean.getProductType() + ")", null, oldVal, newv);
                            message = dao.updateProductType(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " product type ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " product type");
        }
        return retType;
    }

    public String delete() {

        System.out.println("called ProductTypeAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            ProductTypeDAO dao = new ProductTypeDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.PRODUCT_TYPE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete product type (product type: " + inputBean.getProductType() + ")", null);
            message = dao.deleteProductType(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + "product type ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(ProductTypeDAO.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called ProductTypeAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                ProductTypeDAO dao = new ProductTypeDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.PRODUCT_TYPE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmProductType(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " product type");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called ProductTypeAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                ProductTypeDAO dao = new ProductTypeDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.PRODUCT_TYPE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectProductType(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(ProductTypeAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " product type");
        }
        return retType;
    }

    public String reportGenerate() {

        System.out.println("called ProductTypeAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                ProductTypeDAO dao = new ProductTypeDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    ProductTypeInputBean searchBean = (ProductTypeInputBean) session.getAttribute(SessionVarlist.PRODUCT_TYPE_SEARCH_BEAN);
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new ProductTypeInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Product_Type_Report.csv");
                        setContentLength(sb.length());

                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.PRODUCT_TYPE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Product type csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + "detail csv report");
                    Logger
                            .getLogger(ProductTypeAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ProductTypeAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product type");

            return "message";
        }
        return retMsg;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getProductType() == null || inputBean.getProductType().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_TYPE_EMPTY_PRODUCT_TYPE;
        } else if (!Validation.isSpecailCharacter(inputBean.getProductType())) {
            message = MessageVarList.PRODUCT_TYPE_INVALID_PRODUCT_TYPE;
        } else if (inputBean.getProductName() == null || inputBean.getProductName().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_TYPE_EMPTY_PRODUCT_NAME;
//        } else if (!Validation.isSpecailCharacter(inputBean.getProductName())) {
//            message = MessageVarList.PRODUCT_TYPE_INVALID_PRODUCT_NAME;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_TYPE_EMPTY_STATUS;
        } else if (inputBean.getProductcategory() == null || inputBean.getProductcategory().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_TYPE_EMPTY_PRODUCT_CATEGORY;
        }
        return message;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getProductType() == null || inputBean.getProductType().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_TYPE_EMPTY_PRODUCT_TYPE;
        } else if (!Validation.isSpecailCharacter(inputBean.getProductType())) {
            message = MessageVarList.PRODUCT_TYPE_INVALID_PRODUCT_TYPE;
        } else if (inputBean.getProductName() == null || inputBean.getProductName().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_TYPE_EMPTY_PRODUCT_NAME;
//        } else if (!Validation.isSpecailCharacter(inputBean.getProductName())) {
//            message = MessageVarList.PRODUCT_TYPE_INVALID_PRODUCT_NAME;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_TYPE_EMPTY_STATUS;
        } else if (inputBean.getProductcategory() == null || inputBean.getProductcategory().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_TYPE_EMPTY_PRODUCT_CATEGORY;
        }
        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }
}
