/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.FeeConfigBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FeeConfigInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FeeConfigPendBean;
import com.epic.ndb.dao.controlpanel.systemconfig.FeeConfigurationDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.FeeConfiguration;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class FeeConfigAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    FeeConfigInputBean inputBean = new FeeConfigInputBean();

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called FeeConfigAction : execute");
        return SUCCESS;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.FEE_CONFIG_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("activate".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("Detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.FEE_CONFIG_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);

    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called FeeConfigAction :view");

        } catch (Exception ex) {
            addActionError("Fee Configuration " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FeeConfigAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called FeeConfigAction: List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            FeeConfigurationDAO dao = new FeeConfigurationDAO();
            List<FeeConfigBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("Parameter Code", inputBean.getParamCodeSearch())
                        + checkEmptyorNullString("Description", inputBean.getDescriptionSearch())
                        + checkEmptyorNullString("Parameter Value", inputBean.getParamValSearch())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.FEE_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Fee and schedule configuration management search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
        } catch (Exception e) {
            Logger.getLogger(FeeConfigAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" Fee configuration " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String ApproveList() {
        System.out.println("called FeeConfigAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            FeeConfigurationDAO dao = new FeeConfigurationDAO();
            List<FeeConfigPendBean> dataList = dao.getPendingFeeConfigList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.FEE_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending fee and schedule configuration list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
        } catch (Exception e) {
            Logger.getLogger(FeeConfigAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Fee configuration" + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String Detail() {
        System.out.println("called FeeConfigAction: Detail");
        FeeConfiguration feeconfig = null;
        try {
            if (inputBean.getParamCode() != null && !inputBean.getParamCode().isEmpty()) {

                FeeConfigurationDAO dao = new FeeConfigurationDAO();

                feeconfig = dao.findFeeConfigById(inputBean.getParamCode());

                inputBean.setParamCode(feeconfig.getParamcode());
                inputBean.setDescription(feeconfig.getDescription());
                inputBean.setParamVal(feeconfig.getParamvalue());

                inputBean.setOldvalue(feeconfig.getParamcode() + "|"
                        + feeconfig.getDescription() + "|"
                        + feeconfig.getParamvalue());

            } else {
                inputBean.setMessage("Empty parameter code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Fee configuration" + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FeeConfigAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String Update() {

        System.out.println("called FeeConfigAction : update");
        String retType = "message";

        try {
            if (inputBean.getParamCode() != null && !inputBean.getParamCode().isEmpty()) {
                FeeConfigurationDAO dao = new FeeConfigurationDAO();
                inputBean.setParamCode(inputBean.getParamCode());

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getParamCode() + "|"
                            + inputBean.getDescription() + "|"
                            + inputBean.getParamVal();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) {

                        Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.FEE_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update fee and schedule configuration (parameter code: " + inputBean.getParamCode() + ") ", null, oldVal, newv);
                        message = dao.updateFeeConfig(inputBean, audit);

                        if (message.isEmpty()) {
                            addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " fee and schedule configuration ");
                        } else {
                            addActionError(message);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(FeeConfigAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Fee configuration " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getParamCode() == null || inputBean.getParamCode().trim().isEmpty()) {
            message = MessageVarList.FEE_CONFIG_EMPTY_PARAM_CODE;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.FEE_CONFIG_EMPTY_DESCRIPTION;
        } else if (inputBean.getParamVal() != null && inputBean.getParamVal().isEmpty()) {
            message = MessageVarList.FEE_CONFIG_EMPTY_PARAM_VAL;
        } else if (!Validation.isSpecailCharacter(inputBean.getParamCode())) {
            message = MessageVarList.FEE_CONFIG_INVALID_PARAM_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getDescription())) {
            message = MessageVarList.FEE_CONFIG_INVALID_DESCRIPTION;
        }

        return message;
    }

    public String Confirm() {
        System.out.println("called FeeConfigAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            FeeConfigurationDAO dao = new FeeConfigurationDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.FEE_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", null);
            message = dao.confirmFeeConfig(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation approved successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(FeeConfigAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Fee configuration " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called FeeConfigAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            FeeConfigurationDAO dao = new FeeConfigurationDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.FEE_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", null);
            message = dao.rejectFeeConfig(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation rejected successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(FeeConfigAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Fee configuration " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    public String Find() {
        System.out.println("called FeeConfigAction: Find");
        FeeConfiguration puc = null;
        try {
            if (inputBean.getParamCode() != null && !inputBean.getParamCode().isEmpty()) {

                FeeConfigurationDAO dao = new FeeConfigurationDAO();

                puc = dao.findFeeConfigById(inputBean.getParamCode());

                inputBean.setParamCode(puc.getParamcode());
                inputBean.setDescription(puc.getDescription());
                inputBean.setParamVal(puc.getParamvalue());

            } else {
                inputBean.setMessage("Empty fee configuration.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Fee configuration " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(FeeConfigAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }
}
