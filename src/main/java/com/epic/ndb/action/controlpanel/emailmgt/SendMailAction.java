/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.emailmgt;

import com.epic.ndb.bean.controlpanel.emailmgt.SendMailBean;
import com.epic.ndb.bean.controlpanel.emailmgt.SendMailInputBean;
import com.epic.ndb.bean.controlpanel.emailmgt.SendMailPendBean;
import com.epic.ndb.bean.customermanagement.KeyValueBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.emailmgt.SendMailDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;

/**
 *
 * @author sivaganesan_t
 */
public class SendMailAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    SendMailBean audata = new SendMailBean();
    Map parameterMap = new HashMap();
    private InputStream inputStream = null;
    private String fileName;

    SendMailInputBean inputBean = new SendMailInputBean();

    public SendMailBean getAudata() {
        return audata;
    }

    public Map getParameterMap() {
        return parameterMap;
    }

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called SendMailAction : execute");
        return SUCCESS;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.SENDEMAIL_MGT_PAGE, request);

        inputBean.setVsearch(true);
        inputBean.setVsend(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVdual(true);
        inputBean.setVgenerate(true);
        inputBean.setVgenerateview(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEND_TASK)) {
                    inputBean.setVsend(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                    inputBean.setVgenerateview(false);
                }
            }
        }
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.SENDEMAIL_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detailview".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("send".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("uploadCid".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("template".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("send".equals(method)) {
            task = TaskVarList.SEND_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("individualReport".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    public String view() {

        System.out.println("called SendMailAction :view");
        String result = "view";

        try {
            CommonDAO dao = new CommonDAO();

            this.applyUserPrivileges();
            inputBean.setRecipientList(this.getRecipientList());
            inputBean.setSegmentTypeList(dao.getSegmentTypeList());
        } catch (Exception ex) {
            addActionError("Send Message " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String list() {
        System.out.println("called SendMailAction: List");
        try {
            if (inputBean.isSearch()) {
                int rows = inputBean.getRows();
                int page = inputBean.getPage();
                int to = (rows * page);
                int from = to - rows;
                long records = 0;
                String orderBy = "";

                if (!inputBean.getSidx().isEmpty()) {
                    orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
                }

                SendMailDAO dao = new SendMailDAO();
                List<SendMailBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

                /**
                 * for search audit
                 */
                if (inputBean.isSearch() && from == 0) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String searchParameters = "["
                            + checkEmptyorNullString("From Date", inputBean.getFdate_s())
                            + checkEmptyorNullString("To Date", inputBean.getTodate_s())
                            + checkEmptyorNullString("Recipient", inputBean.getToUser_s())
                            + checkEmptyorNullString("CID", inputBean.getCif_s())
                            + checkEmptyorNullString("Segment", inputBean.getSegment_s())
                            + "]";

                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SENDEMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Send message search using " + searchParameters + " parameters ", null);
                    SystemAuditDAO sysdao = new SystemAuditDAO();
                    sysdao.saveAudit(audit);
                }

                if (!dataList.isEmpty()) {
                    records = dataList.get(0).getFullCount();
                    inputBean.setRecords(records);
                    inputBean.setGridModel(dataList);
                    int total = (int) Math.ceil((double) records / (double) rows);
                    inputBean.setTotal(total);
                    
                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.SENDMAILMANAGEMENT_SEARCHBEAN, inputBean);
                } else {
                    inputBean.setRecords(0L);
                    inputBean.setTotal(0);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " send message");
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called SendMailAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();

            SendMailDAO dao = new SendMailDAO();
            List<SendMailPendBean> dataList = dao.getPendingSendEmailList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SENDEMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + "send message");
        }
        return "list";
    }

    public String detailview() {
        System.out.println("called SendMailAction: detailview");

        try {
            this.applyUserPrivileges();

            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                SendMailDAO dao = new SendMailDAO();
                SendMailBean sendMail = dao.getSendMsgByID(inputBean.getId());
                inputBean.setSendMsg(sendMail);
                if (sendMail.getToUserId().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT)) {
                    inputBean.setSegmentList(dao.getSegmentListById(inputBean.getId()));
                } else if (sendMail.getToUserId().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL)) {
                    inputBean.setCidList(dao.getCusListById(inputBean.getId()));
                }
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.SEND_MESSAGE_IND_BEAN, sendMail);
            } else {
                inputBean.setMessage("Empty ID.");
                addActionError("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Send mail " + MessageVarList.COMMON_ERROR_PROCESS);
            addActionError("Send mail " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detailview";

    }     
    
    public String reportGenerate() {

        System.out.println("called SendMailAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
           if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                SendMailDAO dao = new SendMailDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    SendMailInputBean searchBean = (SendMailInputBean) session.getAttribute(SessionVarlist.SENDMAILMANAGEMENT_SEARCHBEAN);

                    if (searchBean != null) {
                        sb = dao.makeCSVReportSql(searchBean);
                    } else {
                        sb = dao.makeCSVReportSql(new SendMailInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Send_Message_Report.csv");
//                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.SENDEMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Send message csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(SendMailAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Send message ");

            return "message";
        }
        return retMsg;
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called SendMailAction : ViewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setServiceCategoryList(dao.getInboxServiceCategoryList());
            inputBean.setRecipientList(this.getRecipientList());
            inputBean.setSegmentList(dao.getSegmentTypeActiveKeyValueList());

        } catch (Exception ex) {
            addActionError("Send message " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String uploadCid() {
        System.out.println("called SendMailAction : uploadCid");
        String errorMsg = null;
        MultiPartRequestWrapper request = (MultiPartRequestWrapper) ServletActionContext.getRequest();

        try {
            String message = "";
            if (!request.getFiles("file")[0].equals(null)) {
                File file = request.getFiles("file")[0];
                String filename = request.getFileNames("file")[0];
                message = this.getFile(filename);
                if (message.isEmpty()) {
                    errorMsg = this.validateUpload(file);
                    if (errorMsg != null && !errorMsg.isEmpty()) {
                        inputBean.setErrormessage(errorMsg);
                    }
                } else {
                    inputBean.setErrormessage(message);
                    addActionError(message);
                }
            } else {
                inputBean.setErrormessage(MessageVarList.SEND_MESSAGE_NO_UPLOADED_FILE);
                addActionError(MessageVarList.SEND_MESSAGE_NO_UPLOADED_FILE);
            }

        } catch (Exception ex) {
            inputBean.setErrormessage(MessageVarList.FILE_UPLOAD_ERROR);
            addActionError(MessageVarList.FILE_UPLOAD_ERROR);
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "uploadCid";
    }

    public String send() {
        System.out.println("called SendMailAction :send");
        String result = "message";
        try {
            String message = this.validateSend();
            if (message.isEmpty()) {
                SendMailDAO dao = new SendMailDAO();

                HttpSession session = ServletActionContext.getRequest().getSession(false);
                List<KeyValueBean> listOfUser = (List<KeyValueBean>) session.getAttribute(SessionVarlist.SEND_MESSAGE_USERLIST);
                inputBean.setCidList(listOfUser);

                HttpServletRequest request = ServletActionContext.getRequest();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEND_TASK, PageVarList.SENDEMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Requested to send message ", null, null, "");
                message = dao.sendMessage(inputBean, audit);

                if (message.isEmpty()) {
//                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_SEND_TASK_PENDING + "message ");
                    addActionMessage( "Message "+MessageVarList.COMMON_SUCC_SEND);
                } else {
                    inputBean.setMessage(message);
                    addActionError(message);
                }
            } else {
                inputBean.setMessage(message);
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String template() {

        System.out.println("called SendMailAction: template");
        String retType = "csv";
        FileWriter fileWriter = null;
        BufferedWriter bw = null;
        PrintWriter out = null;
        try {
            ServletContext context = ServletActionContext.getServletContext();
            String destPath = context.getRealPath("/resouces/csv_temp/sendmail");
            File fileToDownload = new File(destPath, "sendmessagecid.csv");

            try {
                fileWriter = new FileWriter(fileToDownload);
                bw = new BufferedWriter(fileWriter);
                out = new PrintWriter(bw);

                out.println("CID");
                out.println("XXXXX12");
                 
            } catch (Exception e) {
                retType = "message";
                addActionError(MessageVarList.COMMON_ERROR_PROCESS + " send message");
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (fileWriter != null) {
                        fileWriter.close();
                    }
                } catch (Exception e) {
                }
            }

            setInputStream(new FileInputStream(fileToDownload));
            setFileName(fileToDownload.getName());
//            setContentLength(fileToDownload.length());

        } catch (Exception e) {
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " send message");
            retType = "message";
        }

        return retType;
    }

    public String confirm() {
        System.out.println("called SendMailAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                SendMailDAO dao = new SendMailDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.SENDEMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmSendMsg(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setErrormessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String reject() {
        System.out.println("called SendMailAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                SendMailDAO dao = new SendMailDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.SENDEMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectSendMsg(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setErrormessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String viewPend() {
        System.out.println("called SendMailAction: viewPend");
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                SendMailDAO dao = new SendMailDAO();
                SendMailPendBean sendMailPend = dao.getSendMsgTempByID(inputBean.getId());
                inputBean.setSendMsgTemp(sendMailPend);
                if (sendMailPend.getToUserId().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT)) {
                    inputBean.setSegmentList(dao.getTempSegmentListById(inputBean.getId()));
                } else if (sendMailPend.getToUserId().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL)) {
                    inputBean.setCidList(dao.getTempCusListById(inputBean.getId()));
                }
            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Send mail " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "viewPend";
    }

    public String individualReport() {

        System.out.println("called SendMailAction : individualReport");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy 'at' HH:mm a");
        try {
            cal.setTime(CommonDAO.getSystemDateLogin());

            HttpServletRequest request = ServletActionContext.getRequest();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.SENDEMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Send Message individual report generated", null);
            SystemAuditDAO auditDao = new SystemAuditDAO();
            auditDao.saveAudit(audit);

            //get image path
            ServletContext context = ServletActionContext.getServletContext();
            String imgPath = context.getRealPath("/resouces/images/ndb_bank_logo.png");

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            audata = (SendMailBean) session.getAttribute(SessionVarlist.SEND_MESSAGE_IND_BEAN);
            SendMailDAO dao = new SendMailDAO();
            if (audata.getToUserId().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT)) {
                audata.setSegments(dao.getSegmentsById(audata.getId()));
                if(audata.getMapPointer()!=null && !audata.getMapPointer().isEmpty()){
                    audata.setCifs(dao.getCifByMapPointer(audata.getMapPointer()));
                }else{
                    audata.setCifs("--");
                }
            } else if (audata.getToUserId().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL)) {
                audata.setCifs(dao.getCustomersById(audata.getId()));
                audata.setSegments("--");
            } else {
                if(audata.getMapPointer()!=null && !audata.getMapPointer().isEmpty()){
                    audata.setCifs(dao.getCifByMapPointer(audata.getMapPointer()));
                }else{
                    audata.setCifs("--");
                }
                audata.setSegments("--");
            }

            parameterMap.put("bankaddressheader", CommonVarList.REPORT_ADD_HEADER);
            parameterMap.put("printeddate", sdf.format(cal.getTime()));
            parameterMap.put("bankaddress", CommonVarList.REPORT_ADDRESS);
            parameterMap.put("banktel", CommonVarList.REPORT_TEL);
            parameterMap.put("bankmail", CommonVarList.REPORT_MAIL);
            parameterMap.put("imageurl", imgPath);

        } catch (Exception e) {
            Logger.getLogger(SendMailAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " send mail");
            return "message";
        }
        return "report";
    }

    private List<KeyValueBean> getRecipientList() {

        List<KeyValueBean> recipientList = new ArrayList<KeyValueBean>();

        KeyValueBean onBoardType1 = new KeyValueBean();
        onBoardType1.setKey(CommonVarList.SEND_MESSAGE_TO_USER_ALL);
        onBoardType1.setValue("All");
        recipientList.add(onBoardType1);

        KeyValueBean onBoardType2 = new KeyValueBean();
        onBoardType2.setKey(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL);
        onBoardType2.setValue("Individual customers");
        recipientList.add(onBoardType2);

        KeyValueBean onBoardType3 = new KeyValueBean();
        onBoardType3.setKey(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT);
        onBoardType3.setValue("Segments");
        recipientList.add(onBoardType3);

        return recipientList;

    }

    public String getFile(String file) {
        String msgEx = "";
        if (file == null) {
            msgEx = "Please choose a file to upload.";
        } else {
            msgEx = Validation.isCSV(file);
        }
        return msgEx;
    }

    private String validateSend() {
        String message = "";
        if (inputBean.getServiceId() == null || inputBean.getServiceId().trim().isEmpty()) {
//            message = MessageVarList.SEND_MESSAGE_EMPTY_SERVICE;
            message = MessageVarList.SEND_MESSAGE_EMPTY_SUBJECT;
//        } else if (inputBean.getSubject()== null || inputBean.getSubject().trim().isEmpty()) {
//            message = MessageVarList.SEND_MESSAGE_EMPTY_SUBJECT;
        } else if (inputBean.getToUser() == null || inputBean.getToUser().trim().isEmpty()) {
            message = MessageVarList.SEND_MESSAGE_EMPTY_RECIPIENT;
        } else if (inputBean.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT) && (inputBean.getCurrentSegmentBox() == null || inputBean.getCurrentSegmentBox().isEmpty())) {
            message = MessageVarList.SEND_MESSAGE_EMPTY_SEGMENT;
        } else if (inputBean.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL) && (inputBean.getNewCidBox() == null || inputBean.getNewCidBox().isEmpty())) {
            message = MessageVarList.SEND_MESSAGE_EMPTY_CID;
        } else if (inputBean.getMessageEmail() == null || inputBean.getMessageEmail().trim().isEmpty()) {
            message = MessageVarList.SEND_MESSAGE_EMPTY_MESSAGE;
        }
        if(message.isEmpty()){
            SendMailDAO dao = new SendMailDAO();
            if(inputBean.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT) && !dao.isCustomerExistForSegmentList(inputBean.getCurrentSegmentBox())){
                message = MessageVarList.SEND_MESSAGE_NO_CUSTOMER_SEGMENT;
            }
        }
        return message;
    }

    private String validateUpload(File file) throws Exception {
        String message = "";
        FileReader isr = null;
        BufferedReader br = null;
        File csvFile = null;
        List<KeyValueBean> listOfUser = new ArrayList<KeyValueBean>();
        List<String> cifList = new ArrayList<String>();
        try {

            String[] parts = new String[0];
            int countrecord = 1;
            int succesrec = 0;
            String thisLine = null;
            boolean getline = false;
            String token = "";
            SendMailDAO dao = new SendMailDAO();

            csvFile = file;
            isr = new FileReader(csvFile);
            br = new BufferedReader(isr);

            while ((thisLine = br.readLine()) != null) {
                if (thisLine.trim().equals("")) {
                    continue;
                } else {
                    if (getline) {
                        token = thisLine;
                        String cif = "";
                        try {
                            parts = token.split("\\,");
                            cif = parts[0].trim();
                        } catch (Exception ee) {
                            message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                            break;
                        }
                        countrecord++;
                        if (parts.length >= 1 && cif != "" && message.isEmpty()) {
                            if (message.isEmpty()) {
                                if (!cifList.contains(cif)) {//for distinct cif list
                                    cifList.add(cif);
                                    Object messageOrBean = dao.getUseridCidBean(cif);
                                    if (messageOrBean instanceof KeyValueBean) {
                                        listOfUser.add((KeyValueBean) messageOrBean);
                                        succesrec++;
                                    } else if (messageOrBean instanceof String) {
                                        message = messageOrBean.toString() + " at line number " + countrecord;
                                        //                                            + ",success count :" + succesrec;
                                        break;
                                    } else {
                                        message = "Error occured at line number " + countrecord;
                                    }
                                }
                            } else {
                                message = message + " at line number " + countrecord;
//                                        + ",success count :" + succesrec;
                                break;
                            }

                        } else {
                            message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                            break;
                        }
                    } else {
                        getline = true;
                    }
                }
            }
            if (message == null || message.isEmpty()) {
                inputBean.setUserList(listOfUser);
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.SEND_MESSAGE_USERLIST, listOfUser);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
        }
        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
