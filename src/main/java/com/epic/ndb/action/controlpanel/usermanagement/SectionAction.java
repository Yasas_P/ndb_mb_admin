/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.usermanagement.SectionBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SectionInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SectionPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.dao.controlpanel.usermanagement.SectionDao;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.mapping.Section;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author jeevan
 */
public class SectionAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    SectionInputBean inputBean = new SectionInputBean();

    public Object getModel() {
        return inputBean;
    }

    public SectionAction() {
    }

    public String execute() throws Exception {
        System.out.println("Called Section Action: execute");
        return SUCCESS;
    }

    public String View() {
        String result = "view";

        try {
            this.applyUserPrivileges();
            CommonDAO dao = new CommonDAO();
//                inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

            System.out.println("called SectionAction: view");

        } catch (Exception e) {
            addActionError("Section " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    //start newly chnged
    public String activate() {
        System.out.println("called SectionAction : activate");
        String message = null;
        String retType = "activate";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SectionDao dao = new SectionDao();
            message = this.validateInputs();
            if (message.isEmpty()) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Section code " + inputBean.getSectionCode() + " updated", null);
                message = dao.activateSection(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Section " + MessageVarList.COMMON_SUCC_ACTIVATE;
                }
                inputBean.setMessage(message);
            } else {
                addActionError(message);
            }

        } catch (Exception e) {
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_ACTIVATE);
        }
        return retType;
    }

    //end newly changed
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.SECTION_MGT_PAGE;
        String section = null;

        if ("View".equals(method)) {
            section = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            section = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            section = TaskVarList.ADD_TASK;
        } else if ("Delete".equals(method)) {
            section = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            section = TaskVarList.VIEW_TASK;
        } else if ("ViewPopup".equals(method)) {
            section = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            section = TaskVarList.UPDATE_TASK;
        }//newly changed
        else if ("activate".equals(method)) {
            section = TaskVarList.UPDATE_TASK;
        } else if ("ApproveList".equals(method)) {
            section = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            section = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            section = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            section = TaskVarList.REJECT_TASK;
        }

        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(section, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.SECTION_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
//                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.LOGIN_TASK)) {
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String List() {
        System.out.println("called sectionAction: List");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            SectionDao dao = new SectionDao();
            List<SectionBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();
                String searchParameters = "["
                        + checkEmptyorNullString("Section Code", inputBean.getSectionCodeSearch())
                        + checkEmptyorNullString("Description", inputBean.getDescriptionSearch())
                        + checkEmptyorNullString("Sort Key", inputBean.getSortKeySearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + "]";

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Section management search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Section " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String Add() {
        System.out.println("called sectionAction: add");
        String result = "message";

        try {
            HttpServletRequest request = ServletActionContext.getRequest();
//            String statusVal = request.getParameter("activeStatus");

            SectionDao dao = new SectionDao();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getSectionCode() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getSortKey() + "|"
                        + inputBean.getStatus();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Requested to add section code (code : " + inputBean.getSectionCode() + ")", null, null, newv);
                message = dao.insertSection(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " section ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception e) {
            addActionError("Section " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
        }

        return result;
    }

    private String validateInputs() {
        String message = "";
        String url = "";

        if (inputBean.getSectionCode() == null || inputBean.getSectionCode().trim().isEmpty()) {
            message = MessageVarList.SECTION_CODE_EMPTY;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.SECTION_DESC_EMPTY;
        } else if (inputBean.getSortKey() == null || inputBean.getSortKey().trim().isEmpty()) {
            message = MessageVarList.SECTION_SORY_KEY_EMPTY;
        } else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.SECTION_STATUS_EMPTY;
        } else {
            try {
                new Integer(inputBean.getSortKey());
            } catch (Exception e) {
                message = MessageVarList.SECTION_SORT_KEY_INVALID;
            }
            try {

                CommonDAO dao = new CommonDAO();
                message = dao.getSectionSortKeyCount(inputBean.getSortKey());

            } catch (Exception e) {
                message = MessageVarList.SECTION_SORT_KEY_INVALID;
            }
        }

        return message;
    }

    private String validateUpdates() {
        String message = "";
        String url = "";

        if (inputBean.getSectionCode() == null || inputBean.getSectionCode().trim().isEmpty()) {
            message = MessageVarList.SECTION_CODE_EMPTY;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.SECTION_DESC_EMPTY;
        } else if (inputBean.getSortKey() == null || inputBean.getSortKey().trim().isEmpty()) {
            message = MessageVarList.SECTION_SORY_KEY_EMPTY;
        } else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.SECTION_STATUS_EMPTY;
        } else {

            try {
                new Integer(inputBean.getSortKey());
            } catch (Exception e) {
                message = MessageVarList.SECTION_SORT_KEY_INVALID;
            }
//            try {
//                Section sctn = null;
//                SectionDao sDao = new SectionDao();
//                sctn = sDao.findSectionById(inputBean.getSectionCode());
//
//                inputBean.setOldsortkey(sctn.getSortkey().toString());
//
//                System.err.println("Old " + inputBean.getOldsortkey());
//                System.err.println("New " + inputBean.getSortKey());
//                if (inputBean.getSortKey().equals(inputBean.getOldsortkey())) {
//
//                    CommonDAO dao = new CommonDAO();
//                    message = dao.getSectionSortKeyCountForUpdate(inputBean.getSortKey(), inputBean.getOldsortkey());
//                } else {
//                    message = MessageVarList.SECTION_SORT_KEY_ALREADY_EXISTS;
//                }
//
//            } catch (Exception e) {
//                message = MessageVarList.TASK_MGT_ERROR_SORTKEY_INVALID;
//            }

        }

        return message;
    }

    public String detail() {
        System.out.println("called SectionMgtAction : detail");
        Section tt = null;
        try {
            if (inputBean.getSectionCode() != null && !inputBean.getSectionCode().isEmpty()) {

                SectionDao dao = new SectionDao();
                CommonDAO commonDAO = new CommonDAO();
                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

                tt = dao.findSectionById(inputBean.getSectionCode());

                inputBean.setSectionCode(tt.getSectioncode());
                inputBean.setDescription(tt.getDescription());
                inputBean.setSortKey(tt.getSortkey().toString());
                inputBean.setOldsortkey(tt.getSortkey().toString());
                inputBean.setStatus(tt.getStatus().getStatuscode());

                inputBean.setOldvalue(inputBean.getSectionCode() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getSortKey() + "|"
                        + inputBean.getStatus());

            } else {
                inputBean.setMessage("Empty Section code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Section code  " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";

    }

    public String Update() {

        System.out.println("called sectionAction : update");
        String retType = "message";

        try {
            if (inputBean.getSectionCode() != null && !inputBean.getSectionCode().isEmpty()) {

                SectionDao dao = new SectionDao();

                //set username get in hidden fileds
                inputBean.setSectionCode(inputBean.getSectionCode());

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getSectionCode() + "|"
                            + inputBean.getDescription() + "|"
                            + inputBean.getSortKey() + "|"
                            + inputBean.getStatus();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) {
                        String newValWithActState =inputBean.getSectionCode() + "|"
                            + inputBean.getDescription() + "|"
                            + inputBean.getSortKey() + "|"
                            + CommonVarList.STATUS_ACTIVE; 
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){
                       
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Requested to update section code (code : " + inputBean.getSectionCode() + " )", null, oldVal, newv);
                            message = dao.updateSection(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " section ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
//                    HttpServletRequest request = ServletActionContext.getRequest();
//                    //SectionDao dao = new SectionDao();
//
//                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Section code " + inputBean.getSectionCode() + " updated", null, null, null);
//                    message = dao.updateSection(inputBean, audit);
//
//                    if (message.isEmpty()) {
//                        addActionMessage("Section " + MessageVarList.COMMON_SUCC_UPDATE);
//                    } else {
//                        addActionError(message);
//                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Section " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    public String Find() {
        System.out.println("called SectionAction: Find");
        Section section = null;

        try {
            if (inputBean.getSectionCode() != null && !inputBean.getSectionCode().isEmpty()) {

                SectionDao dao = new SectionDao();
                section = dao.findSectionById(inputBean.getSectionCode());

                inputBean.setSectionCode(section.getSectioncode());
                inputBean.setDescription(section.getDescription());
                inputBean.setSortKey(section.getSortkey().toString());
                inputBean.setOldsortkey(section.getSortkey().toString());
                inputBean.setStatus(section.getStatus().getStatuscode());

            } else {
                inputBean.setMessage("Empty section code");
            }

        } catch (Exception e) {
            inputBean.setMessage("Section " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return "find";
    }

    public String Delete() {
        System.out.println("called SectionAction: delete");
        String message = null;
        String retType = "delete";

        try {

            HttpServletRequest request = ServletActionContext.getRequest();
            SectionDao dao = new SectionDao();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Requested to delete section code (code : " + inputBean.getSectionCode() + " )", null);

            message = dao.deleteSection(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " section ";
            }
            inputBean.setMessage(message);

        } catch (Exception e) {
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
//            inputBean.setMessage(MessageVarList.COMMON_ERROR_DELETE);
        }
        return retType;
    }

    public String ViewPopup() {
        String result = "viewpopup";
        System.out.println("called SectionAction : ViewPopup");
        try {
            this.applyUserPrivileges();
            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception e) {
            addActionError("Section " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    public String ApproveList() {
        System.out.println("called SectionAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            SectionDao dao = new SectionDao();
            List<SectionPendBean> dataList = dao.getPendingSectionList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Pending section list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Section " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String Confirm() {
        System.out.println("called SectionAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SectionDao dao = new SectionDao();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, " ", null);
            message = dao.confirmSection(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation approved successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Section " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called SectionAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SectionDao dao = new SectionDao();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.SECTION_MGT_PAGE, SectionVarList.USERMANAGEMENT, " ", null);
            message = dao.rejectSection(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation rejected successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(SectionAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Section " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

}
