/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.PromotionCatBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PromotionCatInpuBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PromotionCatPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.PromotionCatDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.PromotionsCategories;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author eranga_j
 */
public class PromotionCatAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    PromotionCatInpuBean inputBean = new PromotionCatInpuBean();

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called PromotionCatAction : execute");
        return SUCCESS;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.PROMOTION_CAT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            System.out.println("called PromotionCatAction :View");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " promotion categories");
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called PromotionCatAction: List");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            PromotionCatDAO dao = new PromotionCatDAO();
            List<PromotionCatBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {
                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("Code ", inputBean.getS_catcode())
                        + checkEmptyorNullString("Description", inputBean.getS_description())
                        + checkEmptyorNullString("Status", inputBean.getS_status())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PROMOTION_CAT_PAGE, SectionVarList.SYSTEM_CONFIG, "Promotion categories search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " promotion categories ");
        }
        return "list";
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.PROMOTION_CAT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("Delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("ViewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    public String detail() {
        System.out.println("called PromotionCatAction : detail");
        PromotionsCategories tt = null;
        try {
            if (inputBean.getCatcode() != null && !inputBean.getCatcode().isEmpty()) {

                PromotionCatDAO dao = new PromotionCatDAO();
                CommonDAO commonDAO = new CommonDAO();
                
                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                
                tt = dao.findPromotionCategoryById(inputBean.getCatcode());

                inputBean.setCatcode(tt.getCatcode());
                inputBean.setCatname(tt.getCatname());
                inputBean.setStatus(tt.getStatus().getStatuscode());
               
                inputBean.setOldvalue(inputBean.getCatcode()+ "|"
                        + inputBean.getCatname() + "|"
                        + inputBean.getStatus() 
                );

            } else {
                inputBean.setMessage("Empty code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " promotions categories");
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";

    }

    public String ViewPopup() {
        String result = "viewpopup";
        System.out.println("called PromotionCatAction : ViewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                    
            System.out.println("called PromotionCatAction :viewpopup");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String Add() {
        System.out.println("called PromotionCatAction : Add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            PromotionCatDAO dao = new PromotionCatDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newV = inputBean.getCatcode()+ "|"
                        + inputBean.getCatname()+ "|"
                        + inputBean.getStatus();
                
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.PROMOTION_CAT_PAGE, SectionVarList.SYSTEM_CONFIG, "Requested to add promotion category ( Code: " + inputBean.getCatcode() + ") ", null, null, newV);
                message = dao.insertPromotionCategory(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " promotion categories ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Promotion Category " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getCatcode()== null || inputBean.getCatcode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_CAT_EMPTY_CODE;
        } else if (inputBean.getCatname()== null || inputBean.getCatname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_CAT_EMPTY_NAME;
        } else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.PROMOTION_CAT_EMPTY_STATUS;
        } else {
            if (!Validation.isSpecailCharacter(inputBean.getCatcode())) {
                message = MessageVarList.PROMOTION_CAT_INVALID_CODE;
//            } else if (!Validation.isSpecailCharacter(inputBean.getCatname())) {
//                message = MessageVarList.PROMOTION_CAT_INVALID_NAME;
            }
        }
        return message;
    }

    public String Find() {
        System.out.println("called PromotionCatAction : Find");
        PromotionsCategories tt = null;
        try {
            if (inputBean.getCatcode() != null && !inputBean.getCatcode().isEmpty()) {

                PromotionCatDAO dao = new PromotionCatDAO();

                tt = dao.findPromotionCategoryById(inputBean.getCatcode());

                inputBean.setCatcode(tt.getCatcode());
                inputBean.setCatname(tt.getCatname());
                inputBean.setStatus(tt.getStatus().getStatuscode());
                
                inputBean.setOldvalue(inputBean.getCatcode()+ "|" + inputBean.getCatname()+ "|" + inputBean.getStatus());

            } else {
                inputBean.setMessage("Empty code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " promotions categories");
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String Update() {

        System.out.println("called PromotionCatAction : Update");
        String retType = "message";

        try {
            if (inputBean.getCatcode()!= null && !inputBean.getCatcode().isEmpty()) {

                inputBean.setCatcode(inputBean.getCatcode());

                String message = this.validateInputs();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();
                    PromotionCatDAO dao = new PromotionCatDAO();

                    String newV = inputBean.getCatcode() + "|" + inputBean.getCatname()+ "|" + inputBean.getStatus();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newV);
                    System.out.println("oldVal :" + oldVal);

                    if (!newV.equals(oldVal)) {
                         String newValWithActState = inputBean.getCatcode()+ "|" + inputBean.getCatname()+ "|" + CommonVarList.STATUS_ACTIVE ;
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.PROMOTION_CAT_PAGE, SectionVarList.SYSTEM_CONFIG, "Requested to update promotion category ( Code: " + inputBean.getCatcode() + ") ", null, inputBean.getOldvalue(), newV);
                            message = dao.updatePromotionCategory(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " promotion categories ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " promotion categories ");
        }
        return retType;
    }

    public String Delete() {
        System.out.println("called PromotionCatAction : Delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            PromotionCatDAO dao = new PromotionCatDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.PROMOTION_CAT_PAGE, SectionVarList.SYSTEM_CONFIG, "Requested to delete promotion category ( Code: " + inputBean.getCatcode() + ") ", null);
            message = dao.deletePromotionCategory(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " promotion categories ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called PromotionCatAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                PromotionCatDAO dao = new PromotionCatDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.PROMOTION_CAT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmPromotionCategory(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " promotion categories ");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called PromotionCatAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                PromotionCatDAO dao = new PromotionCatDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.PROMOTION_CAT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectPromotionCategory(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " promotion categories");
        }
        return retType;
    }

    public String approveList() {
        System.out.println("called PromotionCatAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            PromotionCatDAO dao = new PromotionCatDAO();
            List<PromotionCatPendBean> dataList = dao.getPendingPromotionCategory(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PROMOTION_CAT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(PromotionCatAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + "  promotion categories");
        }
        return "list";
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

}
