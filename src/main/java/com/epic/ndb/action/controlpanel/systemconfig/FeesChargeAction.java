/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.FeesChargeBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FeesChargeInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FeesChargePendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.FeesChargeDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.FeesCharges;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class FeesChargeAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    FeesChargeInputBean inputBean = new FeesChargeInputBean();

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.FEES_CHARGE_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.FEES_CHARGE_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVdual(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setTransferTypeList(dao.getTransferTypeList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called FeesChargeAction : view");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " fee charge ");
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {

        System.out.println("called FeesChargeAction : List");

        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            FeesChargeDAO dao = new FeesChargeDAO();
            List<FeesChargeBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "Fee charge search using ["
                        + checkEmptyorNullString("Charge code", inputBean.getChargeCodeSearch())
                        + checkEmptyorNullString("Short name", inputBean.getShortNameSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + checkEmptyorNullString("Description", inputBean.getDescriptionSearch())
                        + checkEmptyorNullString("Charge amount", inputBean.getChargeAmountSearch())
                        + checkEmptyorNullString("Transfer type", inputBean.getTransferTypeSearch())
                        + "] parameters ";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.FEES_CHARGE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, searchParameters, null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
        } catch (Exception e) {
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " fee charge ");
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called FeesChargeAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            FeesChargeDAO dao = new FeesChargeDAO();
            List<FeesChargePendBean> dataList = dao.getPendingFeesChargeList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.FEES_CHARGE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " fee charge ");
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called FeesChargeAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setTransferTypeList(dao.getTransferTypeActiveList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " fee charge ");
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String add() {

        System.out.println("called FeesChargeAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            FeesChargeDAO dao = new FeesChargeDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getChargeCode().trim() + "|" + inputBean.getTransferType() + "|" + inputBean.getShortName().trim() + "|" + inputBean.getStatus().trim() + "|" + inputBean.getDescription().trim() + "|" + inputBean.getChargeAmount();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.FEES_CHARGE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Requested to add fee charge ( Charge code: " + inputBean.getChargeCode() + ", Transfer Type: " + inputBean.getTransferType() + " )", null, null, newv);

                message = dao.insertFeesCharge(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " fee charge ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " fee charge");
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called FeesChargeAction: Detail");
        FeesCharges feesCharges = null;
        try {
            if (inputBean.getChargeCode() != null && !inputBean.getChargeCode().isEmpty()) {

                FeesChargeDAO dao = new FeesChargeDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setTransferTypeList(commonDAO.getTransferTypeList());
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                feesCharges = dao.findFeesChargeById(inputBean.getChargeCode(), inputBean.getTransferType());

                inputBean.setShortName(feesCharges.getShortName());
                inputBean.setStatus(feesCharges.getStatus().getStatuscode());
                inputBean.setDescription(feesCharges.getDescription());
                inputBean.setChargeAmount(feesCharges.getChargeAmount().toString());
                inputBean.setTransferType(feesCharges.getTransferType().getTransferId());

                inputBean.setOldvalue(inputBean.getChargeCode() + "|"
                        + inputBean.getTransferType() + "|"
                        + feesCharges.getShortName() + "|"
                        + feesCharges.getStatus().getStatuscode() + "|"
                        + feesCharges.getDescription() + "|"
                        + feesCharges.getChargeAmount().toString());
            } else {
                addActionError("Empty charge code.");
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " fee charge ");
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String find() {
        System.out.println("called FeesChargeAction: find");
        FeesCharges feesCharges = null;
        try {
            if (inputBean.getChargeCode() != null && !inputBean.getChargeCode().isEmpty()) {

                FeesChargeDAO dao = new FeesChargeDAO();

                feesCharges = dao.findFeesChargeById(inputBean.getChargeCode(), inputBean.getTransferType());

                inputBean.setShortName(feesCharges.getShortName());
                inputBean.setStatus(feesCharges.getStatus().getStatuscode());
                inputBean.setDescription(feesCharges.getDescription());
                inputBean.setChargeAmount(feesCharges.getChargeAmount().toString());
            } else {
                addActionError("Empty charge code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " fee charge ");
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String update() {

        System.out.println("called FeesChargeAction : update");
        String retType = "message";

        try {
            inputBean.setTransferType(inputBean.getTransferTypeHidden());
            if (inputBean.getChargeCode() != null && !inputBean.getChargeCode().isEmpty()) {
                FeesChargeDAO dao = new FeesChargeDAO();

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getChargeCode().trim() + "|" + inputBean.getTransferType() + "|" + inputBean.getShortName().trim() + "|" + inputBean.getStatus().trim() + "|" + inputBean.getDescription().trim() + "|" + inputBean.getChargeAmount();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) {
                        String newValWithActState = inputBean.getChargeCode().trim() + "|" + inputBean.getTransferType() + "|" + inputBean.getShortName().trim() + "|" + CommonVarList.STATUS_ACTIVE + "|" + inputBean.getDescription().trim() + "|" + inputBean.getChargeAmount();
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.FEES_CHARGE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update fee charge ( Charge code: " + inputBean.getChargeCode() + ", Transfer Type: " + inputBean.getTransferType() + " )", null, oldVal, newv);
                            message = dao.updateFeesCharge(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " fee charge ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " fee charge");
        }
        return retType;
    }

    public String delete() {

        System.out.println("called FeesChargeAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            FeesChargeDAO dao = new FeesChargeDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.FEES_CHARGE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete fee charge ( Charge code: " + inputBean.getChargeCode() + ", Transfer Type: " + inputBean.getTransferType() + " )", null);
            message = dao.deleteFeesCharge(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + "fee charge ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String viewPend() {
        System.out.println("called FeesChargeAction : viewPend");
        String retType = "viewPend";
        String message = "";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                FeesChargeDAO dao = new FeesChargeDAO();
                message = dao.getPendOldNewValueOfFeesCharge(inputBean);
                if (message.isEmpty()) {
                } else {
                    addActionError(message);
                }
            } else {
                addActionError("Empty pending task ID.");
            }
        } catch (Exception e) {
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " fee charge");
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called FeesChargeAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                FeesChargeDAO dao = new FeesChargeDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.FEES_CHARGE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmFeesCharge(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " fee charge");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called FeesChargeAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                FeesChargeDAO dao = new FeesChargeDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.FEES_CHARGE_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectFeesCharge(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(FeesChargeAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " fee charge");
        }
        return retType;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getChargeCode() == null || inputBean.getChargeCode().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_CHARGE_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getChargeCode())) {
            message = MessageVarList.FEES_CHARGE_INVALID_CHARGE_CODE;
        } else if (inputBean.getTransferType() == null || inputBean.getTransferType().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_TRANSFER_TYPE;
        } else if (inputBean.getShortName() == null || inputBean.getShortName().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_SHORT_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getShortName())) {
            message = MessageVarList.FEES_CHARGE_INVALID_SHORT_NAME;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_STATUS;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_DESCRIPTION;
        } else if (!Validation.isSpecailCharacter(inputBean.getDescription())) {
            message = MessageVarList.FEES_CHARGE_INVALID_DESCRIPTION;
        } else if (inputBean.getChargeAmount() == null || inputBean.getChargeAmount().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_CHARGE_AMOUNT;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getChargeAmount())) {
            message = MessageVarList.FEES_CHARGE_INVALID_CHARGE_AMOUNT;
        }

        return message;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getChargeCode() == null || inputBean.getChargeCode().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_CHARGE_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getChargeCode())) {
            message = MessageVarList.FEES_CHARGE_INVALID_CHARGE_CODE;
        } else if (inputBean.getTransferType() == null || inputBean.getTransferType().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_TRANSFER_TYPE;
        } else if (inputBean.getShortName() == null || inputBean.getShortName().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_SHORT_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getShortName())) {
            message = MessageVarList.FEES_CHARGE_INVALID_SHORT_NAME;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_STATUS;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_DESCRIPTION;
        } else if (!Validation.isSpecailCharacter(inputBean.getDescription())) {
            message = MessageVarList.FEES_CHARGE_INVALID_DESCRIPTION;
        } else if (inputBean.getChargeAmount() == null || inputBean.getChargeAmount().trim().isEmpty()) {
            message = MessageVarList.FEES_CHARGE_EMPTY_CHARGE_AMOUNT;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getChargeAmount())) {
            message = MessageVarList.FEES_CHARGE_INVALID_CHARGE_AMOUNT;
        }

        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }
}
