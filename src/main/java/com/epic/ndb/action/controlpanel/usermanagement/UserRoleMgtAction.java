/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.usermanagement.UserRoleMgtBean;
import com.epic.ndb.bean.controlpanel.usermanagement.UserRoleMgtInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.UserRoleMgtPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.dao.controlpanel.usermanagement.UserRoleMgtDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.Userrole;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author asela
 */
public class UserRoleMgtAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    UserRoleMgtInputBean inputBean = new UserRoleMgtInputBean();

    public String execute() throws Exception {
        System.out.println("called UserRoleMgtAction : execute");
        return SUCCESS;
    }

    public UserRoleMgtInputBean getModel() {
        return inputBean;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.USER_ROLE_MGT_PAGE;
        String task = null;
        if ("View".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        }//newly changed
        else if ("activate".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("ViewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }

        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    public String View() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
//                inputBean.setUserRoleLevelList(dao.getUserLevelList());
            inputBean.setDefaultUser(CommonVarList.LEVEL_01);
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

            System.out.println("called UserRoleMgtAction :view");

        } catch (Exception ex) {
            addActionError("User role " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String ViewPopup() {
        String result = "viewpopup";
        System.out.println("called UserRoleMgtAction : ViewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
//                inputBean.setUserRoleLevelList(dao.getUserLevelList());
            inputBean.setDefaultUser(CommonVarList.LEVEL_01);
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

            System.out.println("called UserRoleMgtAction :View");

        } catch (Exception ex) {
            addActionError("UserRole " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    //start newly chnged
    public String activate() {
        System.out.println("called UserRoleMgtAction : activate");
        String message = null;
        String retType = "activate";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            UserRoleMgtDAO dao = new UserRoleMgtDAO();
            message = this.validateUpdateInputs();
            if (message.isEmpty()) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.USER_ROLE_MGT_PAGE, SectionVarList.USERMANAGEMENT, "User role code " + inputBean.getUserRoleCode() + " updated", null);
                message = dao.activateUrole(inputBean, audit);
                if (message.isEmpty()) {
                    message = "User Role " + MessageVarList.COMMON_SUCC_ACTIVATE;
                }
                inputBean.setMessage(message);
            } else {
                addActionError(message);
            }

        } catch (Exception e) {
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_ACTIVATE);
        }
        return retType;
    }

    //end newly changed
    public String Add() {
        System.out.println("called UserRoleMgtAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            UserRoleMgtDAO dao = new UserRoleMgtDAO();
            String message = this.validateInsertsInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getUserRoleCode() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getStatus();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.USER_ROLE_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Requested to add user role (code : " + inputBean.getUserRoleCode() + ")", null, null, newv);
                message = dao.insertPage(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " user role ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("User role " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String Detail() {
        System.out.println("called UserRoleMgtAction : detail");
        Userrole tt = null;
        try {
            if (inputBean.getUserRoleCode() != null && !inputBean.getUserRoleCode().isEmpty()) {

                UserRoleMgtDAO dao = new UserRoleMgtDAO();
                CommonDAO commonDAO = new CommonDAO();
                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setDefaultUser(CommonVarList.LEVEL_01);
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                tt = dao.findUserRoleById(inputBean.getUserRoleCode());

                inputBean.setUserRoleCode(tt.getUserrolecode());
                inputBean.setDescription(tt.getDescription());
                inputBean.setStatus(tt.getStatus().getStatuscode());

                inputBean.setOldvalue(inputBean.getUserRoleCode() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getStatus());

            } else {
                inputBean.setMessage("Empty User Role code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("User Role code  " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";

    }

    public String Update() {

        System.out.println("called UserRoleMgtAction : update");
        String retType = "message";

        try {
            if (inputBean.getUserRoleCode() != null && !inputBean.getUserRoleCode().isEmpty()) {

                UserRoleMgtDAO dao = new UserRoleMgtDAO();

                //set username get in hidden fileds
                inputBean.setUserRoleCode(inputBean.getUserRoleCode());

                String message = this.validateUpdateInputs();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getUserRoleCode() + "|"
                            + inputBean.getDescription() + "|"
                            + inputBean.getStatus();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) {
                        String newValWithActState =inputBean.getUserRoleCode() + "|"
                            + inputBean.getDescription() + "|"
                            + CommonVarList.STATUS_ACTIVE; 
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.USER_ROLE_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Requested to update user role(code : " + inputBean.getUserRoleCode() + ")", null, oldVal, newv);
                            message = dao.updateUserRoleMgt(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " user role ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }

            }
        } catch (Exception ex) {
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("User role " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    public String Delete() {
        System.out.println("called UserRoleMgtAction : Delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            UserRoleMgtDAO dao = new UserRoleMgtDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.USER_ROLE_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Requested to delete user role (code :" + inputBean.getUserRoleCode() + ")", null);
            message = dao.deleteUserRoleMgt(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " user role ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String List() {
        System.out.println("called UserRoleMgtAction: List");
        try {
            //if (inputBean.isSearch()) {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            UserRoleMgtDAO dao = new UserRoleMgtDAO();
            List<UserRoleMgtBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("User Role Code", inputBean.getUserRoleCodeSearch())
                        + checkEmptyorNullString("Description", inputBean.getDescriptionSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        //    + checkEmptyorNullString("User Level", inputBean.getUserLevelSearch().toString())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.USER_ROLE_MGT_PAGE, SectionVarList.USERMANAGEMENT, "User role management search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " User role");
        }
        return "list";
    }

    public String Find() {
        System.out.println("called UserRoleMgtAction: Find");
        Userrole userrole = null;
        try {
            if (inputBean.getUserRoleCode() != null && !inputBean.getUserRoleCode().isEmpty()) {

                UserRoleMgtDAO dao = new UserRoleMgtDAO();

                userrole = dao.findUserRoleById(inputBean.getUserRoleCode());

                inputBean.setUserRoleCode(userrole.getUserrolecode());
                inputBean.setDescription(userrole.getDescription());
//                inputBean.setUserRoleLevel(Integer.toString(userrole.getUserlevel().getLevelid()));
                inputBean.setStatus(userrole.getStatus().getStatuscode());

            } else {
                inputBean.setMessage("Empty User role code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("User role " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.USER_ROLE_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
//                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.LOGIN_TASK)) {
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    private String validateInsertsInputs() {
        String message = "";
        if (inputBean.getUserRoleCode() == null || inputBean.getUserRoleCode().trim().isEmpty()) {
            message = MessageVarList.USER_ROLE_MGT_EMPTY_USER_ROLE_CODE;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.USER_ROLE_MGT_EMPTY_DESCRIPTION;
        } //        else if (inputBean.getUserRoleLevel() == null || inputBean.getUserRoleLevel().trim().isEmpty()) {
        //            message = MessageVarList.USER_ROLE_MGT_EMPTY_USER_ROLE_LEVEL;
        //        } 
        else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.USER_ROLE_MGT_EMPTY_STATUS;
        } else {
            if (!Validation.isSpecailCharacter(inputBean.getUserRoleCode())) {
                message = MessageVarList.USER_ROLE_MGT_ERROR_USER_ROLE_CODE_INVALID;
            } else if (!Validation.isSpecailCharacter(inputBean.getDescription())) {
                message = MessageVarList.USER_ROLE_MGT_ERROR_DESC_INVALID;
            }
        }
        return message;
    }

    private String validateUpdateInputs() {
        String message = "";
        if (inputBean.getUserRoleCode() == null || inputBean.getUserRoleCode().trim().isEmpty()) {
            message = MessageVarList.USER_ROLE_MGT_EMPTY_USER_ROLE_CODE;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.USER_ROLE_MGT_EMPTY_DESCRIPTION;
        } //        else if (inputBean.getUserRoleLevel() == null || inputBean.getUserRoleLevel().trim().isEmpty()) {
        //            message = MessageVarList.USER_ROLE_MGT_EMPTY_USER_ROLE_LEVEL;
        //        } 
        else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.USER_ROLE_MGT_EMPTY_STATUS;
        } else {
            if (!Validation.isSpecailCharacter(inputBean.getUserRoleCode())) {
                message = MessageVarList.USER_ROLE_MGT_ERROR_USER_ROLE_CODE_INVALID;
            } else if (!Validation.isSpecailCharacter(inputBean.getDescription())) {
                message = MessageVarList.USER_ROLE_MGT_ERROR_DESC_INVALID;
            } else {
//                try {
//                    Integer.parseInt(inputBean.getUserRoleLevel());
//                } catch (Exception e) {
//                    message = MessageVarList.USER_ROLE_MGT_ERROR_USER_ROLE_LEVEL_INVALID;
//                }
            }
        }
        return message;
    }

    public String ApproveList() {
        System.out.println("called UserRoleAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            UserRoleMgtDAO dao = new UserRoleMgtDAO();
            List<UserRoleMgtPendBean> dataList = dao.getPendingUserRoleList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.USER_ROLE_MGT_PAGE, SectionVarList.USERMANAGEMENT, "Pending user role list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("User role" + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String Confirm() {
        System.out.println("called UserRoleMgtAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            UserRoleMgtDAO dao = new UserRoleMgtDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.USER_ROLE_MGT_PAGE, SectionVarList.USERMANAGEMENT, " ", null);
            message = dao.confirmUserRole(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation approved successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" User role " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called UserRoleMgtAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            UserRoleMgtDAO dao = new UserRoleMgtDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.USER_ROLE_MGT_PAGE, SectionVarList.USERMANAGEMENT, " ", null);
            message = dao.rejectUserRole(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation rejected successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(UserRoleMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" User role " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

}
