/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.emailmgt;

import com.epic.ndb.bean.analytics.ChannelTypeBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailManagementBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailManagementInputBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailManagementPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.emailmgt.EmailManagementDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.mapping.InboxAttachment;
import com.epic.ndb.util.mapping.InboxAttachmentTemp;
import com.epic.ndb.util.mapping.InboxMessage;
import com.epic.ndb.util.mapping.InboxMessageTemp;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author prathibha_w
 */
public class EmailManagementAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {
    EmailManagementBean audata = new EmailManagementBean();
    Map parameterMap = new HashMap();
    private InputStream inputStream = null;
    private InputStream fileInputStream = null;
    private String fileName;
//    private long contentLength;
    EmailManagementInputBean inputBean = new EmailManagementInputBean();


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Object getModel() {
        return inputBean;
    }
    
    public EmailManagementBean getAudata() {
        return audata;
    }

    public Map getParameterMap() {
        return parameterMap;
    }

    public String execute() {
        System.out.println("called EmailManagementAction : execute");
        return SUCCESS;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.EMAIL_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVupdatestatus(true);
        inputBean.setVsearch(true);
        inputBean.setVdownload(true);
        inputBean.setVreply(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVdual(true);
        inputBean.setVgenerateview(true);
        inputBean.setVgenerate(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.STATUS_UPDATE_TASK)) {
                    inputBean.setVupdatestatus(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DOWNLOAD_TASK)) {
                    inputBean.setVdownload(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REPLY_TASK)) {
                    inputBean.setVreply(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                    inputBean.setVgenerateview(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.EMAIL_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detailList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detailview".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detailedit".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.STATUS_UPDATE_TASK;
        } else if ("individualReport".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        } else if ("download".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("downloadTemp".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("reply".equals(method)) {
            task = TaskVarList.REPLY_TASK;
        } else if ("reply2".equals(method)) {
            task = TaskVarList.REPLY_TASK;
        } else if ("mailCount".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    public String view() {

        System.out.println("called EmailManagementAction :view");
        String result = "view";

        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            EmailManagementDAO daos = new EmailManagementDAO();

            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_EMAIL));
            inputBean.setAdminStatusList(dao.getReadUnreadStatus());
            inputBean.setUnreadCount(daos.getUnreadMsgCount());
            inputBean.setMailStatusList(dao.getInboxEditStatusListForNewStatus());
            inputBean.setChannelTypeList(this.getChannelTypeList());
        } catch (Exception ex) {
            addActionError("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String mailCount() {

        System.out.println("called EmailManagementAction :mailCount");
        String result = "list";

        try {
            EmailManagementDAO dao = new EmailManagementDAO();
            inputBean.setUnreadCount(dao.getUnreadMsgCount());

        } catch (Exception ex) {
            addActionError("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String list() {
        System.out.println("called EmailManagementAction: List");
        try {
            if (inputBean.isSearch()) {
                int rows = inputBean.getRows();
                int page = inputBean.getPage();
                int to = (rows * page);
                int from = to - rows;
                long records = 0;
                String orderBy = "";

                if (!inputBean.getSidx().isEmpty()) {
                    orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
                }

                EmailManagementDAO dao = new EmailManagementDAO();
                List<EmailManagementBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

                /**
                 * for search audit
                 */
                if (inputBean.isSearch() && from == 0) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String searchParameters = "["
                            + checkEmptyorNullString("CID", inputBean.getCif_s())
                            + checkEmptyorNullString("User Name", inputBean.getUserName_s())
                            + checkEmptyorNullString("Status", inputBean.getStatus_s())
                            + checkEmptyorNullString("Admin Status", inputBean.getAdminStatus_s())
                            + checkEmptyorNullString("Mobile Status", inputBean.getMobileStatus_s())
                            + checkEmptyorNullString("Mail Status", inputBean.getMailStatus_s())
                            + checkEmptyorNullString("From Date", inputBean.getFdate_s())
                            + checkEmptyorNullString("To Date", inputBean.getTodate_s())
                            + checkEmptyorNullString("Channel Type", inputBean.getChannelType_s())
                            + "]";

                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Email management search using " + searchParameters + " parameters ", null);
                    SystemAuditDAO sysdao = new SystemAuditDAO();
                    sysdao.saveAudit(audit);
                }

                if (!dataList.isEmpty()) {
                    records = dataList.get(0).getFullCount();
                    inputBean.setRecords(records);
                    inputBean.setGridModel(dataList);
                    int total = (int) Math.ceil((double) records / (double) rows);
                    inputBean.setTotal(total);
                    
                     HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.EMAILMANAGEMENT_SEARCHBEAN, inputBean);
                } else {
                    inputBean.setRecords(0L);
                    inputBean.setTotal(0);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Email mgt");
        }
        return "list";
    }
    
    public String approveList() {
        System.out.println("called EmailManagementAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();


            EmailManagementDAO dao = new EmailManagementDAO();
            List<EmailManagementPendBean> dataList = dao.getPendingEmailList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError( MessageVarList.COMMON_ERROR_PROCESS +"Email mgt");
        }
        return "list";
    }
    
    public String detail() {
        System.out.println("called EmailManagementAction: detail");
        InboxMessage IBM = null;
        InboxAttachment IAM = null;
        List<InboxAttachment> IAML = new ArrayList<InboxAttachment>();
        Systemuser sysUser = new Systemuser();
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                EmailManagementDAO dao = new EmailManagementDAO();

                HttpServletRequest request = ServletActionContext.getRequest();
                String newV = inputBean.getAdminStatus();

                HttpSession session = ServletActionContext.getRequest().getSession(true);
                sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Email id " + inputBean.getId() + " updated", null, inputBean.getOldvalue(), newV);
                String msg = dao.updateReadStatus(inputBean.getId(), audit);

                if (msg.isEmpty()) {
                    inputBean.setMessage("Success read");
                } else {
                    inputBean.setMessage("no record");
                }

                IAM = dao.findAttacgedImg(inputBean.getId());
                IAML = dao.findAttacgedImgList(inputBean.getId());
                IBM = dao.findInboxMsg(inputBean.getId());

                if (IBM != null) {
                    inputBean.setSubject(IBM.getSubject());
                    inputBean.setUserId(IBM.getSwtMobileUser().getId());
                    inputBean.setUserName(IBM.getSwtMobileUser().getUsername());
                    inputBean.setMessageEmail(IBM.getMessage());
                    inputBean.setStatus(IBM.getStatus().getStatuscode());
                    inputBean.setCreatedDate(IBM.getCreatedDate().toString());
                    inputBean.setRecipientName(sysUser.getFullname());
                    inputBean.setUserCif(IBM.getSwtMobileUser().getCif());
//                    inputBean.setMessageEmailReply("");
                } else {
                    inputBean.setMessage("No record ");
                }
                if (IAM != null) {
                    inputBean.setFileFormat_db(IAM.getFileFormat());
                    inputBean.setFileName_db(IAM.getFileName());
                    inputBean.setEmailId(Long.toString(IAM.getInboxMessage().getId()));
                    inputBean.setAttachmentId(IAM.getId().toString());
                    inputBean.setHasAttachment(1);
                    inputBean.setAttachmentList(IAML);
                    inputBean.setAttachmentSize(IAML.size());
                } else {
                    inputBean.setHasAttachment(0);
                    inputBean.setAttachmentSize(0);
                }

            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";

    }

    public String detailList() {
        System.out.println("called EmailManagementAction: detailList");
        InboxMessage IBM = null;
        List<InboxAttachment> IAML = new ArrayList<InboxAttachment>();
        List<InboxMessage> IBML = new ArrayList<InboxMessage>();
        List<EmailManagementInputBean> inputBeanList = new ArrayList<EmailManagementInputBean>();

        try {
            this.applyUserPrivileges();

            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                EmailManagementDAO dao = new EmailManagementDAO();

                HttpServletRequest request = ServletActionContext.getRequest();
                String newV = inputBean.getAdminStatus();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Email id " + inputBean.getId() + " updated", null, inputBean.getOldvalue(), newV);
                String msg = dao.updateReadStatus(inputBean.getId(), audit);

                if (msg.isEmpty()) {
                    inputBean.setMessage("Success read");
                } else {
                    inputBean.setMessage("no record");
                }

                IBM = dao.findInboxMsg(inputBean.getId());
                if (IBM != null) {
                    inputBean.setSubject(IBM.getSubject());
                }
                if(dao.isInprogressInboxMsgExistForRefID(IBM.getRefId())){
                    inputBean.setMailStatus(CommonVarList.STATUS_INBOX_INPROGRESS);
                }else{
                    inputBean.setMailStatus(IBM.getMailStatus().getStatuscode());
                }
                IBML = dao.findInboxMsgListByRefID(IBM.getRefId());

                int i = 0;
                for (InboxMessage inboxMessage : IBML) {

                    IAML = dao.findAttacgedImgList(Long.toString(inboxMessage.getId()));

                    EmailManagementInputBean inputBean = new EmailManagementInputBean();

                    inputBean.setUserId(inboxMessage.getSwtMobileUser().getId());
                    inputBean.setMessageEmail(inboxMessage.getMessage());
                    inputBean.setStatus(inboxMessage.getStatus().getStatuscode());
                    try{
                      inputBean.setCreatedDate(inboxMessage.getCreatedDate().toString().substring(0, 19));
                    }catch(ArrayIndexOutOfBoundsException aex){
                        inputBean.setCreatedDate(inboxMessage.getCreatedDate().toString());
                    }catch(Exception ex){
                      inputBean.setCreatedDate("--");  
                    }

                    if (inboxMessage.getReplierUsername()==null || inboxMessage.getReplierUsername().isEmpty()) {
                        inputBean.setUserName(inboxMessage.getSwtMobileUser().getUsername());
                        inputBean.setRecipientName("Bank");
                        inputBean.setUserCif("< CID : " + inboxMessage.getSwtMobileUser().getCif() + " >");
                        inputBean.setIsReply(0);
                    } else {
                        inputBean.setUserName(inboxMessage.getReplierUsername());
                        inputBean.setRecipientName(inboxMessage.getSwtMobileUser().getUsername());
                        inputBean.setUserCif(" ");
                        inputBean.setIsReply(1);
                    }

                    if (IAML != null) {
                        inputBean.setAttachmentList(IAML);
                        inputBean.setAttachmentSize(IAML.size());
                        inputBean.setHasAttachment(1);
                    } else {
                        inputBean.setHasAttachment(0);
                        inputBean.setAttachmentSize(0);
                    }

                    inputBean.setCountiterate(i);
                    inputBeanList.add(inputBean);

                }

                inputBean.setInputBeanList(inputBeanList);

            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detaillist";

    }
    
    public String detailview() {
        System.out.println("called EmailManagementAction: detailview");
        EmailManagementBean inboxDetailbean=null;
        List<InboxAttachment> IAML = new ArrayList<InboxAttachment>();
        try {
            this.applyUserPrivileges();

            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                EmailManagementDAO dao = new EmailManagementDAO();

                HttpServletRequest request = ServletActionContext.getRequest();
                String newV = inputBean.getAdminStatus();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Email id " + inputBean.getId() + " updated", null, inputBean.getOldvalue(), newV);
                String msg = dao.updateReadStatus(inputBean.getId(), audit);

                if (msg.isEmpty()) {
                    inputBean.setMessage("Success read");
                } else {
                    inputBean.setMessage("no record");
                }

                inboxDetailbean = dao.findInboxMsgDetailById(inputBean.getId());
                inputBean.setDataBean(inboxDetailbean);
                
                IAML = dao.findAttacgedImgList(inputBean.getId());
                if (IAML != null) {
                    inputBean.setAttachmentList(IAML);
                    inputBean.setAttachmentSize(IAML.size());
                    inputBean.setHasAttachment(1);
                }
                
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.IND_INBOX_SEARCHBEAN, inboxDetailbean);

            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detailview";

    }
    
    public String detailedit() {
        System.out.println("called EmailManagementAction: detailedit");
        EmailManagementBean inboxDetailbean=null;
        try {
            this.applyUserPrivileges();

            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                CommonDAO comDao = new CommonDAO();
                EmailManagementDAO dao = new EmailManagementDAO();

                HttpServletRequest request = ServletActionContext.getRequest();
                String newV = inputBean.getAdminStatus();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Email id " + inputBean.getId() + " updated", null, inputBean.getOldvalue(), newV);
                String msg = dao.updateReadStatus(inputBean.getId(), audit);

                if (msg.isEmpty()) {
                    inputBean.setMessage("Success read");
                } else {
                    inputBean.setMessage("no record");
                }

                inboxDetailbean = dao.findInboxMsgDetailById(inputBean.getId());
                inputBean.setDataBean(inboxDetailbean);
                if(inboxDetailbean.getMailStatus().equals(CommonVarList.STATUS_INBOX_NEW)){
                    inputBean.setMailStatusList(comDao.getInboxEditStatusListForNewStatus());
                }else{
                    inputBean.setMailStatusList(comDao.getInboxEditStatusListForInprogressStatus());
                }
                
            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detailedit";

    }
    
    public String find() {
        System.out.println("called EmailManagementAction: find");
        EmailManagementBean inboxDetailbean=null;
        try {
            this.applyUserPrivileges();

            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                EmailManagementDAO dao = new EmailManagementDAO();


                inboxDetailbean = dao.findInboxMsgDetailById(inputBean.getId());
                inputBean.setMailStatus(inboxDetailbean.getMailStatus());
                
            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }
    
    public String update() {

        System.out.println("called EmailManagementAction : update");
        String retType = "message";

        try {
            if (inputBean.getId()!= null && !inputBean.getId().isEmpty()) {
                EmailManagementDAO dao = new EmailManagementDAO();

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    EmailManagementBean inboxDetailbean = dao.findInboxMsgDetailById(inputBean.getId());

                    String oldVal = inboxDetailbean.getId()+ "|"
                            + inboxDetailbean.getMailStatus();

                    String newv = inputBean.getId()+ "|"
                            + inputBean.getMailStatus();

                    if (!newv.equals(oldVal)) {
                        Systemaudit audit = Common.makeAudittrace(request, TaskVarList.STATUS_UPDATE_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Update inbox message status ( ID : " + inputBean.getId()+ " )", null, oldVal, newv);
                        message = dao.updateMessage(inputBean, audit);

                        if (message.isEmpty()) {
//                            addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " inbox message ");
                            addActionMessage("Inbox message "+MessageVarList.COMMON_SUCC_UPDATE);
                        } else {
                            addActionError(message);
                        }
                        
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError( MessageVarList.COMMON_ERROR_UPDATE +" email management");
        }
        return retType;
    }

    public String individualReport() {

        System.out.println("called EmailManagementAction : individualReport");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy 'at' HH:mm a");
        try {
            cal.setTime(CommonDAO.getSystemDateLogin());

            HttpServletRequest request = ServletActionContext.getRequest();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Email mangemnt individual report generated", null);
            SystemAuditDAO dao = new SystemAuditDAO();
            dao.saveAudit(audit);

            //get image path
            ServletContext context = ServletActionContext.getServletContext();
            String imgPath = context.getRealPath("/resouces/images/ndb_bank_logo.png");

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            audata = (EmailManagementBean) session.getAttribute(SessionVarlist.IND_INBOX_SEARCHBEAN);

            parameterMap.put("bankaddressheader", CommonVarList.REPORT_ADD_HEADER);
            parameterMap.put("printeddate", sdf.format(cal.getTime()));
            parameterMap.put("bankaddress", CommonVarList.REPORT_ADDRESS);
            parameterMap.put("banktel", CommonVarList.REPORT_TEL);
            parameterMap.put("bankmail", CommonVarList.REPORT_MAIL);
            parameterMap.put("imageurl", imgPath);

        } catch (Exception e) {
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " email management");
            return "message";
        }
        return "report";
    }
    
    public String download() {

        System.out.println("called EmailManagementAction : attachment download");
        String retType = "message";

        try {

            EmailManagementDAO dao = new EmailManagementDAO();

            System.out.println("file id " + inputBean.getFileId());

            try {
                InboxAttachment attachment = dao.getfile(inputBean.getFileId());

                fileName = attachment.getFileName();
                String file = attachment.getAttachmentFile();
                byte[] fileDecoded = java.util.Base64.getDecoder().decode(file);
                setFileInputStream(new ByteArrayInputStream(fileDecoded));

                retType = "download";

            } catch (Exception e) {
                addActionError("Attachement file download failed !");
            }

        } catch (Exception ex) {
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return retType;
    }
    
    public String downloadTemp() {

        System.out.println("called EmailManagementAction : Temporary attachment download");
        String retType = "message";

        try {
//            HttpServletRequest request = ServletActionContext.getRequest();

            EmailManagementDAO dao = new EmailManagementDAO();

            System.out.println("file id " + inputBean.getFileId());

            try {
                InboxAttachmentTemp attachment = dao.getfileTemp(inputBean.getFileId());

                fileName = attachment.getFileName();
                String file = attachment.getAttachmentFile();
                byte[] fileDecoded = java.util.Base64.getDecoder().decode(file);
                fileInputStream = new ByteArrayInputStream(fileDecoded);
//                setContentLength(fileDecoded.length);
                
//                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DOWNLOAD_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Email attachment downloaded", null);

//                SystemAuditDAO sysdao = new SystemAuditDAO();
//                sysdao.saveAudit(audit);

                retType = "download";

//            } catch (FileNotFoundException ex) {
//                addActionError("Attachement file doesn't exists !");
            } catch (Exception e) {
                addActionError("Attachement file download failed !");
            }

        } catch (Exception ex) {
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return retType;
    }

    public String reply() {

        System.out.println("called EmailManagementAction :reply");
        String result = "message";

        try {

            EmailManagementDAO dao = new EmailManagementDAO();

            HttpServletRequest request = ServletActionContext.getRequest();
            String newV = inputBean.getMessageEmailReply();

            System.out.println("id " + inputBean.getId());
            System.out.println("mes " + inputBean.getMessageEmailReply());

            if (!inputBean.getMessageEmailReply().isEmpty() && inputBean.getMessageEmailReply() != null) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Email id " + inputBean.getId() + " updated", null, null, newV);
                String msg = dao.sendReply(inputBean.getMessageEmailReply(), inputBean.getId(), audit);

                if (msg.isEmpty()) {
                    addActionMessage("Successfully send reply");
                } else {
                    addActionError("Error occered when repling email");
                }
            } else {
                addActionError("Enter Message to reply");
            }

        } catch (Exception ex) {
            addActionError("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String reply2() {

        System.out.println("called EmailManagementAction :reply2");
        String result = "message2";
        InboxMessage IBM = null;
        List<InboxAttachment> IAML = new ArrayList<InboxAttachment>();
//        Systemuser sysUser = new Systemuser();
        try {
            String message = this.validateReply2();
            if (message.isEmpty()) {
                EmailManagementDAO dao = new EmailManagementDAO();

                HttpServletRequest request = ServletActionContext.getRequest();
                String newV = inputBean.getMessageEmailReply();

//                HttpSession session = ServletActionContext.getRequest().getSession(true);
//                sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

                if (!inputBean.getMessageEmailReply().isEmpty() && inputBean.getMessageEmailReply() != null) {
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REPLY_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Requested to reply to Email id " + inputBean.getId() + " ", null, null, newV);
                    String[] msg = dao.sendReply2(inputBean, audit);

                    if (msg[0].isEmpty()) {
                        addActionMessage("Success");
                        inputBean.setMessage("S1");
//                        IBM = dao.findInboxMsg(inputBean.getId());
                        IBM = dao.findInboxMsg(msg[1]);
                        IAML = dao.findAttacgedImgList(msg[1]);

                        inputBean.setUserId(IBM.getSwtMobileUser().getId());
                        inputBean.setUserName(IBM.getReplierUsername());
                        try{
                            inputBean.setCreatedDate(IBM.getCreatedDate().toString().substring(0, 19));
                        }catch(ArrayIndexOutOfBoundsException aex){
                            inputBean.setCreatedDate(IBM.getCreatedDate().toString());
                        }catch(Exception ex){
                            inputBean.setCreatedDate("--");  
                        }
                        inputBean.setRecipientName(IBM.getSwtMobileUser().getUsername());
                        inputBean.setUserCif("");
                        inputBean.setHasAttachment(0);
                        inputBean.setIsReply(1);
                        inputBean.setAttachmentSize(0);
                        inputBean.setMessageEmail(inputBean.getMessageEmailReply());

                        if (IAML != null) {
                            inputBean.setAttachmentList(IAML);
                            inputBean.setAttachmentSize(IAML.size());
                            inputBean.setHasAttachment(1);
                        } else {
                            inputBean.setHasAttachment(0);
                        }

                    } else {
                        inputBean.setMessage(msg[0]);
                        
                        addActionError("Error");
                    }
                } else {
                    inputBean.setMessage("E3");
                    addActionError("Enter");
                }
            } else {
                inputBean.setMessage(message);
                addActionError("Error");
            }

        } catch (Exception ex) {
            addActionError("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String getOS_Type() {

        String osType = "";
        String osName = "";
        osName = System.getProperty("os.name", "").toLowerCase();

        // For WINDOWS
        if (osName.contains("windows")) {
            osType = "WINDOWS";
        } else if (osName.contains("linux")) {
            osType = "LINUX";
        } else if (osName.contains("sunos")) {
            osType = "SUNOS";
        }

        return osType;
    }

    private String validateReply2() {
        String message = "";

        ArrayList<String> acceptedFileFormats = new ArrayList<String>();

        acceptedFileFormats.add("jpg");
        acceptedFileFormats.add("jpeg");
        acceptedFileFormats.add("png");
        acceptedFileFormats.add("tif");
        acceptedFileFormats.add("docx");
        acceptedFileFormats.add("doc");
        acceptedFileFormats.add("xls");
        acceptedFileFormats.add("xlsx");
        acceptedFileFormats.add("csv");
        for (String uploadFileName : inputBean.getFilesUploadFileName()) {
            if (!acceptedFileFormats.contains(FilenameUtils.getExtension(uploadFileName.toLowerCase()))) {
                message = "E2";
                break;
            }
        }
        if(message.isEmpty()){
            if (inputBean.getFilesUploadFileName().size() > 0) { 
                for (File file : inputBean.getFilesUpload()) {
                     if(file.length()>10485760){//10MP
                        message = "E6";
                        break;
                     }
                }
            }
        }
        return message;
    }
    
    public String confirm() {
        System.out.println("called EmailManagementAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                EmailManagementDAO dao = new EmailManagementDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmInboxMsg(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setErrormessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }
    
    public String reject() {
        System.out.println("called EmailManagementAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                EmailManagementDAO dao = new EmailManagementDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectInboxMsg(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setErrormessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }
    
    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }
    
    public String viewPend() {
        System.out.println("called EmailManagementAction: viewPend");
        InboxMessageTemp IBMT = null;
        List<InboxAttachment> IAML = new ArrayList<InboxAttachment>();
        List<InboxAttachmentTemp> IAMLT = new ArrayList<InboxAttachmentTemp>();
        List<InboxMessage> IBML = new ArrayList<InboxMessage>();
        List<EmailManagementInputBean> inputBeanList = new ArrayList<EmailManagementInputBean>();
//        Systemuser sysUser = new Systemuser();
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                EmailManagementDAO dao = new EmailManagementDAO();
                
//                HttpSession session = ServletActionContext.getRequest().getSession(true);
//                sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

                IBMT = dao.findInboxMsgTemp(inputBean.getId());
                if (IBMT != null) {
                    inputBean.setSubject(IBMT.getSubject());
                }

                IBML = dao.findInboxMsgListByRefID(IBMT.getRefId());

                int i = 0;
                for (InboxMessage inboxMessage : IBML) {

                    IAML = dao.findAttacgedImgList(Long.toString(inboxMessage.getId()));

                    EmailManagementInputBean inputBean = new EmailManagementInputBean();

                    inputBean.setUserId(inboxMessage.getSwtMobileUser().getId());
                    inputBean.setMessageEmail(inboxMessage.getMessage());
                    inputBean.setStatus(inboxMessage.getStatus().getStatuscode());
                    try{
                      inputBean.setCreatedDate(inboxMessage.getCreatedDate().toString().substring(0, 19));
                    }catch(ArrayIndexOutOfBoundsException aex){
                        inputBean.setCreatedDate(inboxMessage.getCreatedDate().toString());
                    }catch(Exception ex){
                      inputBean.setCreatedDate("--");  
                    }

//                    if (("13").equals(inboxMessage.getStatus().getStatuscode())) {
                    if (inboxMessage.getMaker()==null || inboxMessage.getMaker().isEmpty()) {
                        inputBean.setUserName(inboxMessage.getSwtMobileUser().getUsername());
//                        inputBean.setRecipientName(sysUser.getFullname());
                        inputBean.setRecipientName("Bank");
                        inputBean.setUserCif("< CID : " + inboxMessage.getSwtMobileUser().getCif() + " >");
                        inputBean.setIsReply(0);
                    } else {
//                        inputBean.setUserName(sysUser.getFullname());
                        inputBean.setUserName(inboxMessage.getReplierUsername());
                        inputBean.setRecipientName(inboxMessage.getSwtMobileUser().getUsername());
//                        inputBean.setUserCif("< " + sysUser.getEmail() + " >");
                        inputBean.setUserCif(" ");
                        inputBean.setIsReply(1);
                    }

                    if (IAML != null) {
                        inputBean.setAttachmentList(IAML);
                        inputBean.setAttachmentSize(IAML.size());
                        inputBean.setHasAttachment(1);
                    } else {
                        inputBean.setHasAttachment(0);
                        inputBean.setAttachmentSize(0);
                    }

                    inputBean.setCountiterate(i);
                    inputBeanList.add(inputBean);

                }

                inputBean.setInputBeanList(inputBeanList);
                
                //--------------new value-----------------------
//                IBMT =dao.findInboxMsgTempByRefID(IBM.getRefId());
                inputBean.setUserId(IBMT.getSwtMobileUser().getId());
                inputBean.setMessageEmail(IBMT.getMessage());
                inputBean.setStatus(IBMT.getStatus().getStatuscode());
                inputBean.setCreatedDate(IBMT.getCreatedDate().toString());
                try{
                    inputBean.setCreatedDate(IBMT.getCreatedDate().toString().substring(0, 19));
                }catch(ArrayIndexOutOfBoundsException aex){
                    inputBean.setCreatedDate(IBMT.getCreatedDate().toString());
                }catch(Exception ex){
                    inputBean.setCreatedDate("--");  
                }
//                if (("13").equals(IBMT.getStatus().getStatuscode())) {
//                        inputBean.setUserName(IBMT.getSwtMobileUser().getUsername());
////                        inputBean.setRecipientName(sysUser.getFullname());
//                        inputBean.setRecipientName("Bank");
//                        inputBean.setUserCif("< cif : " + IBMT.getSwtMobileUser().getCif() + " >");
//                        inputBean.setIsReply(0);
//                } else {
//                    inputBean.setUserName(sysUser.getFullname());
                    inputBean.setUserName(IBMT.getReplierUsername());
                    inputBean.setRecipientName(IBMT.getSwtMobileUser().getUsername());
//                    inputBean.setUserCif("< " + sysUser.getEmail() + " >");
                    inputBean.setUserCif(" ");
                    inputBean.setIsReply(1);
//                }
                
                 IAMLT = dao.findAttacgedImgTempList(Long.toString(IBMT.getId()));
                 if (IAMLT != null) {
                    inputBean.setAttachmentTempList(IAMLT);
                    inputBean.setAttachmentSize(IAMLT.size());
                    inputBean.setHasAttachment(1);
                } else {
                    inputBean.setHasAttachment(0);
                    inputBean.setAttachmentSize(0);
                }
            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Email Management " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "viewPend";
    }
    
     private String validateUpdates() {
        String message = "";
        if (inputBean.getMailStatus()== null || inputBean.getMailStatus().trim().isEmpty()) {
            message = MessageVarList.INBOX_MESSAGE_EMPTY_MAIL_STATUS;
        }
        return message;
    }
     
     public String reportGenerate() {

        System.out.println("called CustomerSearchAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
           if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                EmailManagementDAO dao = new EmailManagementDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    EmailManagementInputBean searchBean = (EmailManagementInputBean) session.getAttribute(SessionVarlist.EMAILMANAGEMENT_SEARCHBEAN);

                    if (searchBean != null) {
                        sb = dao.makeCSVReportSql(searchBean);
                    } else {
                        sb = dao.makeCSVReportSql(new EmailManagementInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Inbox_Message_Report.csv");
//                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.EMAIL_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "InboxMessage csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger.getLogger(EmailManagementAction.class.getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(EmailManagementAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Email mgt ");

            return "message";
        }
        return retMsg;
    }
     
    private List<ChannelTypeBean> getChannelTypeList() {

        List<ChannelTypeBean> channelTypeList = new ArrayList<ChannelTypeBean>();

        ChannelTypeBean channeltype1 = new ChannelTypeBean();
        channeltype1.setKey("2");
        channeltype1.setValue("Internet Banking");
        channelTypeList.add(channeltype1);

        ChannelTypeBean channeltype2 = new ChannelTypeBean();
        channeltype2.setKey("1");
        channeltype2.setValue("Mobile Banking");
        channelTypeList.add(channeltype2);

        return channelTypeList;

    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

//    public long getContentLength() {
//        return contentLength;
//    }
//
//    public void setContentLength(long contentLength) {
//        this.contentLength = contentLength;
//    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }
}
