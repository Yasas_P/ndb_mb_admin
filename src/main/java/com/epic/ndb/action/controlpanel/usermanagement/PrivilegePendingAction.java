/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.usermanagement.PendingActionBean;
import com.epic.ndb.bean.controlpanel.usermanagement.PrivilegePendingDataBean;
import com.epic.ndb.bean.controlpanel.usermanagement.PrivilegePendingInputBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.dao.controlpanel.usermanagement.PrivilegePendingActionDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author dilanka_w
 */
public class PrivilegePendingAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    PrivilegePendingInputBean inputBean = new PrivilegePendingInputBean();

    public Object getModel() {
        return inputBean;
    }

    @Override
    public String execute() {
        System.out.println("called PrivilegePendingAction : execute");
        return SUCCESS;
    }

    public String view() {

        System.out.println("called PrivilegePendingAction :view");
        String result = "view";

        try {

            HttpSession session = ServletActionContext.getRequest().getSession(false);

            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setUserRoleList(dao.getUserRoleList(CommonVarList.STATUS_ACTIVE));

            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception ex) {
            addActionError("Privilege pending action " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PrivilegePendingAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String list() {
        System.out.println("called PrivilegePendingAction: List");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            Systemuser sysUser = ((Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER));
            inputBean.setSysuserrole(sysUser.getUserrole().getUserrolecode());
            inputBean.setSysusername(sysUser.getUsername());

            PrivilegePendingActionDAO dao = new PrivilegePendingActionDAO();

            List<PendingActionBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);
            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                /**
                 * for audit
                 */
                StringBuilder searchParameters = new StringBuilder();
                searchParameters.append("[")
                        .append(checkEmptyorNullString("Username", inputBean.getPkeySearch()))
                        .append(checkEmptyorNullString("Created User", inputBean.getUsernameSearch()))
                        .append("]");

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PRIVILEGE_PENDING_ACTION_PAGE, SectionVarList.USERMANAGEMENT, "Pending action search using " + searchParameters.toString() + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }

//            CommonDAO commonDao = new CommonDAO();
//            session.setAttribute(SessionVarlist.APPROVALCOUNTSUM, commonDao.getApprovalCountSum(sysUser.getUserrole().getUserrolecode(), sysUser.getUsername()).toString());
//            session.setAttribute(SessionVarlist.APPROVALCOUNT_PRIVILEGE, commonDao.getPrivilegeApprovalCount(sysUser.getUserrole().getUserrolecode(), sysUser.getUsername()).toString());

        } catch (Exception e) {
            Logger.getLogger(PrivilegePendingAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " privilege pending action");
        }
        return "list";
    }

    public String viewDual() {

        System.out.println("called UserPendingAction :viewDual");
        String result = "viewprivilege";

        try {
            HttpSession session = ServletActionContext.getRequest().getSession(true);

            /*
             set approve reject privileges
             */
            Boolean vapprove = (Boolean) session.getAttribute(SessionVarlist.DUALAUTH_APPROVE_PRIVILEGES);
            inputBean.setVapprove(vapprove);

            Boolean vreject = (Boolean) session.getAttribute(SessionVarlist.DUALAUTH_REJECT_PRIVILEGES);
            inputBean.setVreject(vreject);

            //--------------------------------------------------------------------------------
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {

                inputBean.setId(inputBean.getId());
                inputBean.setViewuserrole(inputBean.getId());
                inputBean.setCreateduser(inputBean.getCreateduser());

            } else {
                inputBean.setMessage("Empty ID.");
            }

        } catch (Exception ex) {
            addActionError("Privilege pending action " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PrivilegePendingAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String listDual() {
        System.out.println("called PrivilegePendingAction: listDual");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;

            PrivilegePendingActionDAO dao = new PrivilegePendingActionDAO();

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            Systemuser sysUser = ((Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER));

            List<PrivilegePendingDataBean> dataList = dao.getPendingtaskListByPkeyAndCreatedUser(inputBean, sysUser.getUsername(), to, from);
            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("Section", inputBean.getSectionSearchView())
                        + checkEmptyorNullString("Page", inputBean.getPageSearchView())
                        + checkEmptyorNullString("Task", inputBean.getTaskSearchView())
                        + "]";

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PRIVILEGE_PENDING_ACTION_PAGE, SectionVarList.USERMANAGEMENT, "Pending task search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPending(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }

        } catch (Exception e) {
            Logger.getLogger(PrivilegePendingAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " privilege pending action");
        }
        return "list";
    }

//    public String getApprovalCount() {
//
//        System.out.println("called PrivilegePendingAction : getApprovalCount");
//        String retType = "appcount";
//
//        String approvalCount = "";
//        String approvalCountPrivilege = "";
//
//        try {
//
//            HttpSession session = ServletActionContext.getRequest().getSession(true);
//            Systemuser sysUser = ((Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER));
//
//            CommonDAO commonDao = new CommonDAO();
//
//            approvalCount = commonDao.getApprovalCountSum(sysUser.getUserrole().getUserrolecode(), sysUser.getUsername()).toString();
//            approvalCountPrivilege = commonDao.getPrivilegeApprovalCount(sysUser.getUserrole().getUserrolecode(), sysUser.getUsername()).toString();
//
//            session.setAttribute(SessionVarlist.APPROVALCOUNTSUM, approvalCount);
//            session.setAttribute(SessionVarlist.APPROVALCOUNT_PRIVILEGE, approvalCountPrivilege);
//
//            inputBean.setApprovalCount(approvalCount);
//            inputBean.setAppcountprivilege(approvalCountPrivilege);
//
//        } catch (Exception e) {
//            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " privilege pending action");
//            Logger.getLogger(PrivilegePendingAction.class.getName()).log(Level.SEVERE, null, e);
//            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " privilege pending action");
//        }
//        return retType;
//    }

    public String Approve() {

        System.out.println("called PrivilegePendingAction : Approve");
        String message = "";
        String retType = "message";
        try {

            HttpServletRequest request = ServletActionContext.getRequest();
            PrivilegePendingActionDAO dao = new PrivilegePendingActionDAO();

            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.PRIVILEGE_PENDING_ACTION_PAGE, SectionVarList.USERMANAGEMENT, "", null);

//            message = this.validateApproveReject();
//
//            if (message.isEmpty()) {
            message = dao.approvePrivilege(inputBean, audit);

            if (message.isEmpty()) {
                addActionMessage(MessageVarList.COMMON_APPROVED_SUCCESS);
            } else {
                addActionError(message);
            }
//            } else {
//                addActionError(message);
//            }
        } catch (Exception e) {
            Logger.getLogger(PrivilegePendingAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_APPROVED);
        }
        return retType;
    }

    public String Reject() {

        System.out.println("called PrivilegePendingAction : Reject");
        String message = "";
        String retType = "message";
        try {

            HttpServletRequest request = ServletActionContext.getRequest();
            PrivilegePendingActionDAO dao = new PrivilegePendingActionDAO();

            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.PRIVILEGE_PENDING_ACTION_PAGE, SectionVarList.USERMANAGEMENT, "", null);

            message = this.validateApproveReject();

            if (message.isEmpty()) {

                message = dao.rejectPrivilege(inputBean, audit);
                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_REJECTED_SUCCESS);
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception e) {
            Logger.getLogger(PrivilegePendingAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    private String validateApproveReject() throws Exception {
        String message = "";
        try {

            if (inputBean.getComment() == null || inputBean.getComment().trim().isEmpty()) {
                message = MessageVarList.COMMON_REMARK_COMMENT;
            }
        } catch (Exception e) {
            throw e;
        }
        return message;
    }

    private void applyUserPrivileges() {

        HttpSession session = ServletActionContext.getRequest().getSession(true);

        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.PRIVILEGE_PENDING_ACTION_PAGE, request);

        inputBean.setVsearch(true);
        session.setAttribute(SessionVarlist.DUALAUTH_APPROVE_PRIVILEGES, false);
        session.setAttribute(SessionVarlist.DUALAUTH_REJECT_PRIVILEGES, false);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {

                if (task.getTaskcode().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    session.setAttribute(SessionVarlist.DUALAUTH_APPROVE_PRIVILEGES, true);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    session.setAttribute(SessionVarlist.DUALAUTH_REJECT_PRIVILEGES, true);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                }
            }
        }

    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.PRIVILEGE_PENDING_ACTION_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("listDual".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewDual".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Approve".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("getApprovalCount".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }
}
