/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.ProductMatrixBean;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductMatrixInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductMatrixPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.ProductMatrixBackUpDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.ProductMatrix;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class ProductMatrixBackUpAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    ProductMatrixInputBean inputBean = new ProductMatrixInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    private String serverPath;

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.PRODUCT_MATRIX_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupcsv".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("pendCsvDownloade".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("template".equals(method)) {
            task = TaskVarList.UPLOAD_TASK;
        } else if ("upload".equals(method)) {
            task = TaskVarList.UPLOAD_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.PRODUCT_MATRIX_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVupload(true);
        inputBean.setVdual(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPLOAD_TASK)) {
                    inputBean.setVupload(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setProductCurrencyList(dao.getProductCurrencyList());
            inputBean.setProductTypeList(dao.getProductTypeList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called ProductMatrixAction : view");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product matrix ");
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called ProductMatrixAction : List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
            List<ProductMatrixBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "Product Matrix search using ["
                        + checkEmptyorNullString("Debit product type", inputBean.getDebitProductTypeSearch())
                        + checkEmptyorNullString("Credit product type", inputBean.getCreditProductTypeSearch())
                        + checkEmptyorNullString("Debit currency", inputBean.getDebitCurrencySearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + "] parameters ";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, searchParameters, null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product matrix ");
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called ProductMatrixAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
            List<ProductMatrixPendBean> dataList = dao.getPendingProductMatrixList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product matrix ");
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called ProductMatrixAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO commonDAO = new CommonDAO();

            inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setProductCurrencyList(commonDAO.getProductCurrencyActiveList());
            inputBean.setProductTypeList(commonDAO.getProductTypeActiveList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
            dao.findProductCurrencyList(inputBean);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product matrix ");
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String viewPopupcsv() {
        String result = "viewpopupcsv";
        System.out.println("called ProductMatrixAction : viewPopupcsv");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception e) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + "product matrix ");
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    public String upload() {
        System.out.println("called ProductMatrixAction : upload");
        String result = "messagecsv";
        ServletContext context = ServletActionContext.getServletContext();

//        this.serverPath = context.getRealPath("/resouces/csv_temp/atm_locations");
        if (Common.getOS_Type().equals("WINDOWS")) {
            this.setServerPath(context.getRealPath("/resouces/csv_temp/productmatrix"));
        } else if (Common.getOS_Type().equals("LINUX")) {
            this.setServerPath("/app_conf/Epic/csv_temp/productmatrix");
        }
        try {
            if (inputBean.getHiddenId() != null) {

                HttpServletRequest request = ServletActionContext.getRequest();
                ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();

                String message = "";

                DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                Date date = new Date();

                message = this.getFile(inputBean.getConXLFileName()); // get file

                inputBean.setConXLFileName(dateFormat.format(date) + inputBean.getConXLFileName());

                File directory = new File(getServerPath());

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                if (-1 != inputBean.getConXLFileName().lastIndexOf("\\")) {
                    inputBean.setConXLFileName(inputBean.getConXLFileName().substring(inputBean.getConXLFileName().lastIndexOf("\\") + 1));
                }
                File filetoCreate = new File(getServerPath(), inputBean.getConXLFileName());
//                    if (isfileexists==true) {            //set to database
                if (filetoCreate.exists()) {                //set to local 
                    addActionError("File already exists.");
                    System.err.println("File already exists.");
                } else {

                }

                if (message.isEmpty()) {

                    if (inputBean.getConXL() == null) {
                    } else {
                        message = this.validateUpload();
                        if (message.isEmpty()) {
                            FileUtils.copyFile(inputBean.getConXL(), filetoCreate);
                            //                        System.out.println(filetoCreate.getPath());
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPLOAD_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Requested to upload product matrix ", null, null, null);
                            message = dao.uploadProductMatrix(inputBean, audit);
                        }
                    }

                }

                if (message.isEmpty()) {
                    addActionMessage("File uploaded successfully");
                    System.err.println("File uploaded successfully");

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + "product matrix");
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, ex);

        }
        return result;
    }

    public String add() {

        System.out.println("called ProductMatrixAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {
                StringBuilder stringBuilderNew = new StringBuilder();
                stringBuilderNew.append(inputBean.getDebitProductType().trim())
                        .append("|").append(inputBean.getCreditProductType().trim())
                        .append("|").append(inputBean.getStatus())
                        .append("|").append(inputBean.getDebitCurrency().trim())
                        .append("|");
                String stringSeparator = "";
                for (String creditCurrency : inputBean.getCurrentCreditCurrencyBox()) {
                    stringBuilderNew.append(stringSeparator).append(creditCurrency);
                    stringSeparator = ",";
                }

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Requested to add product matrix (  debit product type: " + inputBean.getDebitProductType() + ", credit product type " + inputBean.getCreditProductType() + ")", null, null, stringBuilderNew.toString());

                message = dao.insertProductMatrix(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + "product matrix ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product matrix");
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called ProductMatrixAction: detail");
        ProductMatrix productMatrix = null;
        try {
            if (inputBean.getDebitProductType() != null && !inputBean.getDebitProductType().isEmpty()) {
                if (inputBean.getCreditProductType() != null && !inputBean.getCreditProductType().isEmpty()) {

                    ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
                    CommonDAO commonDAO = new CommonDAO();

                    inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                    inputBean.setProductCurrencyList(commonDAO.getProductCurrencyActiveList());
                    inputBean.setProductTypeList(commonDAO.getProductTypeList());
                    inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                    productMatrix = dao.findProductMatrixById(inputBean.getDebitProductType(), inputBean.getCreditProductType());

                    inputBean.setStatus(productMatrix.getStatus().getStatuscode());
                    inputBean.setDebitCurrency(productMatrix.getProductCurrency().getCurrencyCode());

                    dao.getProductCurrencyListById(inputBean);
                } else {
                    addActionError("Empty credit product type.");
                }
            } else {
                addActionError("Empty debit product type.");
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product matrix ");
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String find() {
        System.out.println("called ProductMatrixAction: find");
        ProductMatrix productMatrix = null;
        try {
            if (inputBean.getDebitProductType() != null && !inputBean.getDebitProductType().isEmpty()) {
                if (inputBean.getCreditProductType() != null && !inputBean.getCreditProductType().isEmpty()) {

                    ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();

                    productMatrix = dao.findProductMatrixById(inputBean.getDebitProductType(), inputBean.getCreditProductType());

                    inputBean.setStatus(productMatrix.getStatus().getStatuscode());
                    inputBean.setDebitCurrency(productMatrix.getProductCurrency().getCurrencyCode());

                    dao.getProductCurrencyListById(inputBean);
                } else {
                    addActionError("Empty credit product type.");
                }
            } else {
                addActionError("Empty debit product type.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " product matrix ");
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String update() {

        System.out.println("called ProductMatrixAction : update");
        String retType = "message";

        try {
            ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();

            if (inputBean.getDebitProductTypeHidden() != null && !inputBean.getDebitProductTypeHidden().trim().isEmpty()) {
                inputBean.setDebitProductType(inputBean.getDebitProductTypeHidden());
            }
            if (inputBean.getCreditProductTypeHidden() != null && !inputBean.getCreditProductTypeHidden().trim().isEmpty()) {
                inputBean.setCreditProductType(inputBean.getCreditProductTypeHidden());
            }
            String message = this.validateUpdates();

            if (message.isEmpty()) {

                HttpServletRequest request = ServletActionContext.getRequest();

                StringBuilder stringBuilderNew = new StringBuilder();
                stringBuilderNew.append(inputBean.getDebitProductType().trim())
                        .append("|").append(inputBean.getCreditProductType().trim())
                        .append("|").append(inputBean.getStatus())
                        .append("|").append(inputBean.getDebitCurrency().trim())
                        .append("|");
                String stringSeparator = "";
                for (String creditCurrency : inputBean.getCurrentCreditCurrencyBox()) {
                    stringBuilderNew.append(stringSeparator).append(creditCurrency);
                    stringSeparator = ",";
                }
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update product matrix (  debit product type: " + inputBean.getDebitProductType() + ", credit product type " + inputBean.getCreditProductType() + ")", null, null, stringBuilderNew.toString());
                message = dao.updateProductMatrix(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " product matrix ");
                } else {
                    addActionError(message);
                }

            } else {
                addActionError(message);
            }
        } catch (Exception ex) {
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " product matrix ");
        }
        return retType;
    }

    public String delete() {

        System.out.println("called ProductMatrixAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete product matrix (  debit product type: " + inputBean.getDebitProductType() + ", credit product type " + inputBean.getCreditProductType() + ")", null);
            message = dao.deleteProductMatrix(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " product matrix ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called ProductMatrixAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmProductMatrix(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " product matrix");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called ProductMatrixAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectProductMatrix(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " product matrix");
        }
        return retType;
    }

    public String template() {

        System.out.println("called ProductMatrixAction: template");
        String retType = "csv";
        FileWriter fileWriter = null;
        BufferedWriter bw = null;
        PrintWriter out = null;
        try {
            ServletContext context = ServletActionContext.getServletContext();
            String destPath = context.getRealPath("/resouces/csv_temp/productmatrix");
            File fileToDownload = new File(destPath, "productmatrix.csv");

            try {
                fileWriter = new FileWriter(fileToDownload);
                bw = new BufferedWriter(fileWriter);
                out = new PrintWriter(bw);

                out.println("Debit product type,Credit product type,Debit currency code,Credit currency code(s)");
                out.println("6000,1310,LKR,LKR/GBP/CHF/AUD");

//                 
            } catch (Exception e) {
                retType = "message";
                addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product matrix");
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (fileWriter != null) {
                        fileWriter.close();
                    }
                } catch (Exception e) {
                }
            }

            setInputStream(new FileInputStream(fileToDownload));
            setFileName(fileToDownload.getName());
            setContentLength(fileToDownload.length());

        } catch (Exception e) {
            Logger.getLogger(ProductMatrixAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " product matrix");
            retType = "message";
        }

        return retType;
    }

    public String pendCsvDownloade() {
        System.out.println("called ProductMatrixAction : pendCsvDownloade");
        String message = null;
        String retType = "pendcsvdownloade";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.VIEW_PENDING_TASK, PageVarList.PRODUCT_MATRIX_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Pending product matrix csv file downloaded by pending task id " + inputBean.getId(), null);
                message = dao.pendProductMatrixCsvDownloade(inputBean, audit);
                if (!message.isEmpty()) {
                    addActionError(message);
                    retType = "message";
                } else {
                    setInputStream(inputBean.getFileInputStream());
                    setContentLength(inputBean.getFileLength());
                }
            } else {
                addActionError("Empty pending task ID.");
                retType = "message";
            }
        } catch (Exception e) {
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Promotion Management " + MessageVarList.COMMON_ERROR_PROCESS);
            retType = "message";
        }
        return retType;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getDebitProductType() == null || inputBean.getDebitProductType().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_DEBIT_PRODUCT_TYPE;
        } else if (inputBean.getCreditProductType() == null || inputBean.getCreditProductType().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_CREDIT_PRODUCT_TYPE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_STATUS;
        } else if (inputBean.getDebitCurrency() == null || inputBean.getDebitCurrency().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_DEBIT_CURRENCY;
        } else if (inputBean.getCurrentCreditCurrencyBox() == null || inputBean.getCurrentCreditCurrencyBox().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_CREDIT_CURRENCY;
        }
        return message;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getDebitProductType() == null || inputBean.getDebitProductType().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_DEBIT_PRODUCT_TYPE;
        } else if (inputBean.getCreditProductType() == null || inputBean.getCreditProductType().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_CREDIT_PRODUCT_TYPE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_STATUS;
        } else if (inputBean.getDebitCurrency() == null || inputBean.getDebitCurrency().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_DEBIT_CURRENCY;
        } else if (inputBean.getCurrentCreditCurrencyBox() == null || inputBean.getCurrentCreditCurrencyBox().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_CREDIT_CURRENCY;
        }
        return message;
    }

    private String validateUpload() throws Exception {
        String message = "";
        FileReader isr = null;
        BufferedReader br = null;
        Session hSession = null;
        File csvFile = null;
        String[] penCreditCurrencyArray = null;

        try {

            String[] parts = new String[0];
            int countrecord = 1;
            int succesrec = 0;
            String thisLine = null;
            boolean getline = false;
            String token = "";
            ProductMatrixBackUpDAO dao = new ProductMatrixBackUpDAO();

            csvFile = inputBean.getConXL();
            isr = new FileReader(csvFile);
            br = new BufferedReader(isr);
            hSession = HibernateInit.sessionFactory.openSession();

            while ((thisLine = br.readLine()) != null) {
                if (thisLine.trim().equals("")) {
                    continue;
                } else {
                    if (getline) {
                        token = thisLine;

//                        System.err.println(token);
                        try {
                            parts = token.split("\\,", 4);
                            inputBean.setDebitProductType(parts[0].trim());
                            inputBean.setCreditProductType(parts[1].trim());
                            inputBean.setDebitCurrency(parts[2].trim());

                            penCreditCurrencyArray = parts[3].replace(" ", "").split("\\/");
                            List<String> creditCurrencyList = new ArrayList<String>(Arrays.asList(penCreditCurrencyArray));
                            inputBean.setCurrentCreditCurrencyBox(dao.getuniqueList(creditCurrencyList));

                            inputBean.setStatus(CommonVarList.STATUS_ACTIVE);

                        } catch (Exception ee) {
                            message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                            break;
                        }
                        countrecord++;
                        if (parts.length >= 3 && message.isEmpty()) {
                            message = dao.validateInputsForCSV(inputBean);
                            if (message.isEmpty()) {
                                message = dao.validateUpload(inputBean, hSession);
                                if (message.isEmpty()) {
                                    succesrec++;
                                } else {
                                    message = message + " at line number " + countrecord;
//                                            + ",success count :" + succesrec;
                                    break;
                                }
                            } else {
                                message = message + " at line number " + countrecord;
//                                        + ",success count :" + succesrec;
                                break;
                            }

                        } else {
                            message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                            break;
                        }
                    } else {
                        getline = true;
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }

            if (hSession != null) {
                hSession.flush();
                hSession.close();
            }

        }
        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public String getFile(String file) {
        String msgEx = "";
        if (file == null) {
            msgEx = "Please choose a file to upload.";
        } else {
            msgEx = Validation.isCSV(file);
        }
        return msgEx;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }
}
