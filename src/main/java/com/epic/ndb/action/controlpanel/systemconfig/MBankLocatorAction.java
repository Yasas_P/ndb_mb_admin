/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.MBankLocatorBean;
import com.epic.ndb.bean.controlpanel.systemconfig.MBankLocatorInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.MBankLocatorPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.MBankLocatorDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.MBankLocator;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class MBankLocatorAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    MBankLocatorInputBean inputBean = new MBankLocatorInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    private String serverPath;

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.M_BANK_LOCATOR_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupcsv".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("template".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("upload".equals(method)) {
            task = TaskVarList.UPLOAD_TASK;
        } else if ("pendCsvDownloade".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.M_BANK_LOCATOR_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVdual(true);
        inputBean.setVgenerate(true);
        inputBean.setVgenerateview(true);
        inputBean.setVupload(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                    inputBean.setVgenerateview(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPLOAD_TASK)) {
                    inputBean.setVupload(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setChannelTypeList(dao.getChannelTypeList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called MBankLocatorAction :view");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator");
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called MBankLocatorAction: List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            MBankLocatorDAO dao = new MBankLocatorDAO();
            List<MBankLocatorBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "Mobile bank locator search using ["
                        + checkEmptyorNullString("Locator ID", inputBean.getLocatoridSearch())
                        + checkEmptyorNullString("Channel Type", inputBean.getChannelTypeSearch())
                        //                        + checkEmptyorNullString("Channel Subtype", inputBean.getChannelSubTypeSearch())
                        + checkEmptyorNullString("Country Code", inputBean.getCountryCodeSearch())
                        + checkEmptyorNullString("Country", inputBean.getCountrySearch())
                        + checkEmptyorNullString("Master Region", inputBean.getMasterRegionSearch())
                        //                        + checkEmptyorNullString("Region", inputBean.getRegionSearch())
                        + checkEmptyorNullString("Subregion", inputBean.getSubRegionSearch())
                        + checkEmptyorNullString("Name", inputBean.getNameSearch())
                        + checkEmptyorNullString("Address", inputBean.getAddressSearch())
                        //                        + checkEmptyorNullString("Postal code", inputBean.getPostalCodeSearch())
                        + checkEmptyorNullString("Contacts ", inputBean.getContactsSearch())
                        + checkEmptyorNullString("Opening Hours From Mon To Fri ", inputBean.getOpeningHsMonFriSearch())
                        //                        + checkEmptyorNullString("Opening Hours at Sat ", inputBean.getOpeningHsSatSearch())
                        //                        + checkEmptyorNullString("Opening Hours at Sun ", inputBean.getOpeningHsSunHolSearch())
                        + checkEmptyorNullString("Geocode ", inputBean.getGeoCodeSearch())
                        //                        + checkEmptyorNullString("Location Tag ", inputBean.getLocationTagSearch())
                        + checkEmptyorNullString("Language ", inputBean.getLanguageSearch())
                        + checkEmptyorNullString("Status ", inputBean.getStatusSearch())
                        + "] parameters ";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, searchParameters, null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);

                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.M_BANK_LOCATOR_SEARCH_BEAN, inputBean);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator ");
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called MBankLocatorAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            MBankLocatorDAO dao = new MBankLocatorDAO();
            List<MBankLocatorPendBean> dataList = dao.getPendingMBankLocatorList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator ");
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called MBankLocatorAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setChannelTypeList(dao.getChannelTypeList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator ");
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String viewPopupcsv() {
        String result = "viewpopupcsv";
        System.out.println("called MBankLocatorAction : viewPopupcsv");
        try {
            this.applyUserPrivileges();
            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception e) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator");
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    public String upload() {
        System.out.println("called MBankLocatorAction : upload");
        String result = "messagecsv";
        ServletContext context = ServletActionContext.getServletContext();

//        this.serverPath = context.getRealPath("/resouces/csv_temp/atm_locations");
        if (Common.getOS_Type().equals("WINDOWS")) {
            this.setServerPath(context.getRealPath("/resouces/csv_temp/mbanklocator"));
        } else if (Common.getOS_Type().equals("LINUX")) {
            this.setServerPath("/app_conf/Epic/csv_temp/mbanklocator");
        }
        try {
            if (inputBean.getHiddenId() != null) {

                HttpServletRequest request = ServletActionContext.getRequest();
                MBankLocatorDAO dao = new MBankLocatorDAO();

                String message = "";

                DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                Date date = new Date();

                message = this.getFile(inputBean.getConXLFileName()); // get file

                inputBean.setConXLFileName(dateFormat.format(date) + inputBean.getConXLFileName());

                File directory = new File(getServerPath());

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                if (-1 != inputBean.getConXLFileName().lastIndexOf("\\")) {
                    inputBean.setConXLFileName(inputBean.getConXLFileName().substring(inputBean.getConXLFileName().lastIndexOf("\\") + 1));
                }
                File filetoCreate = new File(getServerPath(), inputBean.getConXLFileName());
//                    if (isfileexists==true) {            //set to database
                if (filetoCreate.exists()) {                //set to local 
                    addActionError("File already exists.");
                    System.err.println("File already exists.");
                } else {

                }

                if (message.isEmpty()) {

                    if (inputBean.getConXL() == null) {
                    } else {
                        message = this.validateUpload();
                        if (message.isEmpty()) {
                            FileUtils.copyFile(inputBean.getConXL(), filetoCreate);
                            //                        System.out.println(filetoCreate.getPath());
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPLOAD_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to upload  mobile bank locator(s) ", null, null, null);
                            message = dao.uploadMBankLocator(inputBean, audit);
                        }
                    }
                }

                if (message.isEmpty()) {
                    addActionMessage("File uploaded successfully");
                    System.err.println("File uploaded successfully");

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            addActionError(" mobile bank locator  " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(PromotionMgtAction.class.getName()).log(Level.SEVERE, null, ex);

        }
        return result;
    }

    public String pendCsvDownloade() {
        System.out.println("called MBankLocatorAction : pendCsvDownloade");
        String message = null;
        String retType = "pendcsvdownloade";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                MBankLocatorDAO dao = new MBankLocatorDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.VIEW_PENDING_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Pending mobile bank locator csv file downloaded by pending task id " + inputBean.getId(), null);
                message = dao.pendMBankLocatorCsvDownloade(inputBean, audit);
                if (!message.isEmpty()) {
                    addActionError(message);
                    retType = "message";
                } else {
                    setInputStream(inputBean.getFileInputStream());
                    setContentLength(inputBean.getFileLength());
                }
            } else {
                addActionError("Empty pending task ID.");
                retType = "message";
            }
        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator  ");
            retType = "message";
        }
        return retType;
    }

    public String viewPend() {
        System.out.println("called MBankLocatorAction : viewPend");
        String message = null;
        String retType = "viewPend";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {

                CommonDAO comDao = new CommonDAO();
                inputBean.setStatusList(comDao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setChannelTypeList(comDao.getChannelTypeList());
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                MBankLocatorDAO dao = new MBankLocatorDAO();
                message = dao.viwPendMBankLocator(inputBean);
                if (!message.isEmpty()) {
                    addActionError(message);
                }
            } else {
                addActionError("Empty pending task ID.");
            }
        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator  ");
        }
        return retType;
    }

    public String add() {

        System.out.println("called MBankLocatorAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            MBankLocatorDAO dao = new MBankLocatorDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getLocatorcode() + "|" + inputBean.getStatus() + "|" + inputBean.getChannelType() + "|" + inputBean.getCountryCode()
                        + "|" + inputBean.getCountry() + "|" + inputBean.getMasterRegion() + "|" + inputBean.getSubRegion()
                        + "|" + inputBean.getName() + "|" + inputBean.getAddress() + "|" + inputBean.getPostalCode() + "|" + inputBean.getContacts() + "|" + inputBean.getFax() + "|" + inputBean.getOpeningHsMonFri()
                        + "|" + inputBean.getOpeningHsSat() + "|" + inputBean.getOpeningHsSunHol() + "|" + inputBean.getHolidayBanking() + "|" + inputBean.getLongitude() + "|" + inputBean.getLatitude() + "|" + inputBean.getLocationTag() + "|" + inputBean.getLanguage()
                        + "|" + inputBean.getAtmOnLocation() + "|" + inputBean.getCrmOnLocation() + "|" + inputBean.getPawningOnLocation() + "|" + inputBean.getSafeDepositLockers() + "|" + inputBean.getLeasingDesk365() + "|" + inputBean.getPrvCentre() + "|" + inputBean.getBranchlessBanking();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to add mobile bank locator ( locator code : " + inputBean.getLocatorcode() + " )", null, null, newv);

                message = dao.insertMBankLocator(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " mobile bank locator ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator");
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called MBankLocatorAction: Detail");
        MBankLocator mBankLocator = null;
        try {
            if (inputBean.getLocatorid() != null && !inputBean.getLocatorid().isEmpty()) {

                MBankLocatorDAO dao = new MBankLocatorDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setChannelTypeList(commonDAO.getChannelTypeList());
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                mBankLocator = dao.findMBankLocatorById(inputBean.getLocatorid());

                inputBean.setLocatorcode(mBankLocator.getLocatorcode());
                inputBean.setStatus(mBankLocator.getStatus());
                inputBean.setChannelType(mBankLocator.getChannelType().getChannelType());
                inputBean.setCountryCode(mBankLocator.getCountryCode());
                inputBean.setCountry(mBankLocator.getCountry());
                inputBean.setMasterRegion(mBankLocator.getMasterRegion());
                inputBean.setSubRegion(mBankLocator.getSubRegion());
                inputBean.setName(mBankLocator.getName());
                inputBean.setAddress(mBankLocator.getAddress());
                inputBean.setPostalCode(mBankLocator.getPostalCode());
                inputBean.setContacts(mBankLocator.getContacts());
                inputBean.setFax(mBankLocator.getFax());
                inputBean.setOpeningHsMonFri(mBankLocator.getOpeningHsMonFri());
                inputBean.setOpeningHsSat(mBankLocator.getOpeningHsSat());
                inputBean.setOpeningHsSunHol(mBankLocator.getOpeningHsSunHol());
                inputBean.setHolidayBanking(mBankLocator.getHolidayBanking());
                inputBean.setLongitude(mBankLocator.getLongitude().toString());
                inputBean.setLatitude(mBankLocator.getLatitude().toString());
                inputBean.setLocationTag(mBankLocator.getLocationTag());
                inputBean.setLanguage(mBankLocator.getLanguage());
                inputBean.setAtmOnLocation(mBankLocator.getAtmOnLocation());
                inputBean.setCrmOnLocation(mBankLocator.getCrmOnLocation());
                inputBean.setPawningOnLocation(mBankLocator.getPawningOnLocation());
                inputBean.setSafeDepositLockers(mBankLocator.getSafeDepositLockers());
                inputBean.setLeasingDesk365(mBankLocator.getLeasingDesk365());
                inputBean.setPrvCentre(mBankLocator.getPrvCentre());
                inputBean.setBranchlessBanking(mBankLocator.getBranchlessBanking());

            } else {
                inputBean.setMessage("Empty locator ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator");
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String viewPopupDetail() {
        System.out.println("called MBankLocatorAction: viewPopupDetail");
        MBankLocator mBankLocator = null;
        try {
            if (inputBean.getLocatorid() != null && !inputBean.getLocatorid().isEmpty()) {

                MBankLocatorDAO dao = new MBankLocatorDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setChannelTypeList(commonDAO.getChannelTypeList());
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                mBankLocator = dao.findMBankLocatorById(inputBean.getLocatorid());

                inputBean.setLocatorcode(mBankLocator.getLocatorcode());
                inputBean.setStatus(mBankLocator.getStatus());
                inputBean.setChannelType(mBankLocator.getChannelType().getChannelType());
                inputBean.setCountryCode(mBankLocator.getCountryCode());
                inputBean.setCountry(mBankLocator.getCountry());
                inputBean.setMasterRegion(mBankLocator.getMasterRegion());
                inputBean.setSubRegion(mBankLocator.getSubRegion());
                inputBean.setName(mBankLocator.getName());
                inputBean.setAddress(mBankLocator.getAddress());
                inputBean.setPostalCode(mBankLocator.getPostalCode());
                inputBean.setContacts(mBankLocator.getContacts());
                inputBean.setFax(mBankLocator.getFax());
                inputBean.setOpeningHsMonFri(mBankLocator.getOpeningHsMonFri());
                inputBean.setOpeningHsSat(mBankLocator.getOpeningHsSat());
                inputBean.setOpeningHsSunHol(mBankLocator.getOpeningHsSunHol());
                inputBean.setHolidayBanking(mBankLocator.getHolidayBanking());
                inputBean.setLongitude(mBankLocator.getLongitude().toString());
                inputBean.setLatitude(mBankLocator.getLatitude().toString());
                inputBean.setLocationTag(mBankLocator.getLocationTag());
                inputBean.setLanguage(mBankLocator.getLanguage());
                inputBean.setAtmOnLocation(mBankLocator.getAtmOnLocation());
                inputBean.setCrmOnLocation(mBankLocator.getCrmOnLocation());
                inputBean.setPawningOnLocation(mBankLocator.getPawningOnLocation());
                inputBean.setSafeDepositLockers(mBankLocator.getSafeDepositLockers());
                inputBean.setLeasingDesk365(mBankLocator.getLeasingDesk365());
                inputBean.setPrvCentre(mBankLocator.getPrvCentre());
                inputBean.setBranchlessBanking(mBankLocator.getBranchlessBanking());

            } else {
                inputBean.setMessage("Empty locator ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator");
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "viewpopupdetail";
    }

    public String find() {
        System.out.println("called MBankLocatorAction: find");
        MBankLocator mBankLocator = null;
        try {
            if (inputBean.getLocatorid() != null && !inputBean.getLocatorid().isEmpty()) {

                MBankLocatorDAO dao = new MBankLocatorDAO();

                mBankLocator = dao.findMBankLocatorById(inputBean.getLocatorid());

                inputBean.setLocatorcode(mBankLocator.getLocatorcode());
                inputBean.setStatus(mBankLocator.getStatus());
                inputBean.setChannelType(mBankLocator.getChannelType().getChannelType());
                inputBean.setCountryCode(mBankLocator.getCountryCode());
                inputBean.setCountry(mBankLocator.getCountry());
                inputBean.setMasterRegion(mBankLocator.getMasterRegion());
                inputBean.setSubRegion(mBankLocator.getSubRegion());
                inputBean.setName(mBankLocator.getName());
                inputBean.setAddress(mBankLocator.getAddress());
                inputBean.setPostalCode(mBankLocator.getPostalCode());
                inputBean.setContacts(mBankLocator.getContacts());
                inputBean.setFax(mBankLocator.getFax());
                inputBean.setOpeningHsMonFri(mBankLocator.getOpeningHsMonFri());
                inputBean.setOpeningHsSat(mBankLocator.getOpeningHsSat());
                inputBean.setOpeningHsSunHol(mBankLocator.getOpeningHsSunHol());
                inputBean.setHolidayBanking(mBankLocator.getHolidayBanking());
                inputBean.setLongitude(mBankLocator.getLongitude().toString());
                inputBean.setLatitude(mBankLocator.getLatitude().toString());
                inputBean.setLocationTag(mBankLocator.getLocationTag());
                inputBean.setLanguage(mBankLocator.getLanguage());
                inputBean.setAtmOnLocation(mBankLocator.getAtmOnLocation());
                inputBean.setCrmOnLocation(mBankLocator.getCrmOnLocation());
                inputBean.setPawningOnLocation(mBankLocator.getPawningOnLocation());
                inputBean.setSafeDepositLockers(mBankLocator.getSafeDepositLockers());
                inputBean.setLeasingDesk365(mBankLocator.getLeasingDesk365());
                inputBean.setPrvCentre(mBankLocator.getPrvCentre());
                inputBean.setBranchlessBanking(mBankLocator.getBranchlessBanking());

            } else {
                inputBean.setMessage("Empty locator ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator");
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String update() {

        System.out.println("called MBankLocatorAction : update");
        String retType = "message";

        try {
            if (inputBean.getLocatorid() != null && !inputBean.getLocatorid().isEmpty()) {
                MBankLocatorDAO dao = new MBankLocatorDAO();

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    MBankLocator mBankLocator = dao.findMBankLocatorById(inputBean.getLocatorid());

                    String address = "";
                    String postalCode = "";
                    String contacts = "";
                    String fax = "";
                    String openingHsMonFri = "";
                    String openingHsSat = "";
                    String openingHsSunHol = "";
                    String holidayBanking = "";
                    String locationTag = "";
                    String atmOnLocation = "";
                    String crmOnLocation = "";
                    String pawningOnLocation = "";
                    String safeDepositLockers = "";
                    String leasingDesk365 = "";
                    String prvCentre = "";
                    String branchlessBanking = "";

                    if (mBankLocator.getAddress() != null && !mBankLocator.getAddress().isEmpty()) {
                        address = mBankLocator.getAddress();
                    }
                    if (mBankLocator.getPostalCode() != null && !mBankLocator.getPostalCode().isEmpty()) {
                        postalCode = mBankLocator.getPostalCode();
                    }
                    if (mBankLocator.getContacts() != null && !mBankLocator.getContacts().isEmpty()) {
                        contacts = mBankLocator.getContacts();
                    }
                    if (mBankLocator.getFax() != null && !mBankLocator.getFax().isEmpty()) {
                        fax = mBankLocator.getFax();
                    }
                    if (mBankLocator.getOpeningHsMonFri() != null && !mBankLocator.getOpeningHsMonFri().isEmpty()) {
                        openingHsMonFri = mBankLocator.getOpeningHsMonFri();
                    }
                    if (mBankLocator.getOpeningHsSat() != null && !mBankLocator.getOpeningHsSat().isEmpty()) {
                        openingHsSat = mBankLocator.getOpeningHsSat();
                    }
                    if (mBankLocator.getOpeningHsSunHol() != null && !mBankLocator.getOpeningHsSunHol().isEmpty()) {
                        openingHsSunHol = mBankLocator.getOpeningHsSunHol();
                    }
                    if (mBankLocator.getHolidayBanking() != null && !mBankLocator.getHolidayBanking().isEmpty()) {
                        holidayBanking = mBankLocator.getHolidayBanking();
                    }
                    if (mBankLocator.getLocationTag() != null && !mBankLocator.getLocationTag().isEmpty()) {
                        locationTag = mBankLocator.getLocationTag();
                    }
                    if (mBankLocator.getAtmOnLocation() != null && !mBankLocator.getAtmOnLocation().isEmpty()) {
                        atmOnLocation = mBankLocator.getAtmOnLocation();
                    }
                    if (mBankLocator.getCrmOnLocation() != null && !mBankLocator.getCrmOnLocation().isEmpty()) {
                        crmOnLocation = mBankLocator.getCrmOnLocation();
                    }
                    if (mBankLocator.getPawningOnLocation() != null && !mBankLocator.getPawningOnLocation().isEmpty()) {
                        pawningOnLocation = mBankLocator.getPawningOnLocation();
                    }
                    if (mBankLocator.getSafeDepositLockers() != null && !mBankLocator.getSafeDepositLockers().isEmpty()) {
                        safeDepositLockers = mBankLocator.getSafeDepositLockers();
                    }
                    if (mBankLocator.getLeasingDesk365() != null && !mBankLocator.getLeasingDesk365().isEmpty()) {
                        leasingDesk365 = mBankLocator.getLeasingDesk365();
                    }
                    if (mBankLocator.getLeasingDesk365() != null && !mBankLocator.getLeasingDesk365().isEmpty()) {
                        leasingDesk365 = mBankLocator.getLeasingDesk365();
                    }
                    if (mBankLocator.getPrvCentre() != null && !mBankLocator.getPrvCentre().isEmpty()) {
                        prvCentre = mBankLocator.getPrvCentre();
                    }
                    if (mBankLocator.getBranchlessBanking() != null && !mBankLocator.getBranchlessBanking().isEmpty()) {
                        branchlessBanking = mBankLocator.getBranchlessBanking();
                    }

                    String oldVal = mBankLocator.getLocatorid() + "|" + mBankLocator.getLocatorcode() + "|" + mBankLocator.getStatus() + "|" + mBankLocator.getChannelType().getChannelType() + "|" + mBankLocator.getCountryCode()
                            + "|" + mBankLocator.getCountry() + "|" + mBankLocator.getMasterRegion() + "|" + mBankLocator.getSubRegion()
                            + "|" + mBankLocator.getName() + "|" + address + "|" + postalCode + "|" + contacts + "|" + fax + "|" + openingHsMonFri
                            + "|" + openingHsSat + "|" + openingHsSunHol + "|" + holidayBanking + "|" + mBankLocator.getLongitude().toString() + "|" + mBankLocator.getLatitude().toString() + "|" + locationTag + "|" + mBankLocator.getLanguage()
                            + "|" + atmOnLocation + "|" + crmOnLocation + "|" + pawningOnLocation + "|" + safeDepositLockers + "|" + leasingDesk365 + "|" + prvCentre + "|" + branchlessBanking;

                    String newv = inputBean.getLocatorid() + "|" + inputBean.getLocatorcode() + "|" + inputBean.getStatus() + "|" + inputBean.getChannelType() + "|" + inputBean.getCountryCode()
                            + "|" + inputBean.getCountry() + "|" + inputBean.getMasterRegion() + "|" + inputBean.getSubRegion()
                            + "|" + inputBean.getName() + "|" + inputBean.getAddress() + "|" + inputBean.getPostalCode() + "|" + inputBean.getContacts() + "|" + inputBean.getFax() + "|" + inputBean.getOpeningHsMonFri()
                            + "|" + inputBean.getOpeningHsSat() + "|" + inputBean.getOpeningHsSunHol() + "|" + inputBean.getHolidayBanking() + "|" + inputBean.getLongitude() + "|" + inputBean.getLatitude() + "|" + inputBean.getLocationTag() + "|" + inputBean.getLanguage()
                            + "|" + inputBean.getAtmOnLocation() + "|" + inputBean.getCrmOnLocation() + "|" + inputBean.getPawningOnLocation() + "|" + inputBean.getSafeDepositLockers() + "|" + inputBean.getLeasingDesk365() + "|" + inputBean.getPrvCentre() + "|" + inputBean.getBranchlessBanking();

                    System.out.println("OLD value :" + oldVal);
                    System.out.println("NEW value :" + newv);

                    if (!newv.equals(oldVal)) {
                        String newValWithActState = inputBean.getLocatorid() + "|" + inputBean.getLocatorcode() + "|" + CommonVarList.STATUS_ACTIVE + "|" + inputBean.getChannelType() + "|" + inputBean.getCountryCode()
                            + "|" + inputBean.getCountry() + "|" + inputBean.getMasterRegion() + "|" + inputBean.getSubRegion()
                            + "|" + inputBean.getName() + "|" + inputBean.getAddress() + "|" + inputBean.getPostalCode() + "|" + inputBean.getContacts() + "|" + inputBean.getFax() + "|" + inputBean.getOpeningHsMonFri()
                            + "|" + inputBean.getOpeningHsSat() + "|" + inputBean.getOpeningHsSunHol() + "|" + inputBean.getHolidayBanking() + "|" + inputBean.getLongitude() + "|" + inputBean.getLatitude() + "|" + inputBean.getLocationTag() + "|" + inputBean.getLanguage()
                            + "|" + inputBean.getAtmOnLocation() + "|" + inputBean.getCrmOnLocation() + "|" + inputBean.getPawningOnLocation() + "|" + inputBean.getSafeDepositLockers() + "|" + inputBean.getLeasingDesk365() + "|" + inputBean.getPrvCentre() + "|" + inputBean.getBranchlessBanking();
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update mobile bank locator ( locator code : " + inputBean.getLocatorcode() + " )", null, null, newv);
                            message = dao.updateMBankLocator(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " mobile bank locator ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " mobile bank locator");
        }
        return retType;
    }

    public String delete() {

        System.out.println("called MBankLocatorAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            MBankLocatorDAO dao = new MBankLocatorDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete mobile bank locator ( locator code : " + inputBean.getLocatorcode() + " )", null);
            message = dao.deleteMBankLocator(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + "mobile bank locator ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called MBankLocatorAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                MBankLocatorDAO dao = new MBankLocatorDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmMBankLocator(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " mobile bank locator");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called MBankLocatorAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                MBankLocatorDAO dao = new MBankLocatorDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectMBankLocator(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " mobile bank locator");
        }
        return retType;
    }

    public String template() {

        System.out.println("called MBankLocatorAction: template");
        String retType = "csv";
        FileWriter fileWriter = null;
        BufferedWriter bw = null;
        PrintWriter out = null;
        try {
            ServletContext context = ServletActionContext.getServletContext();
            String destPath = context.getRealPath("/resouces/csv_temp/mbanklocator");
            File fileToDownload = new File(destPath, "mbanklocator.csv");

            try {
                fileWriter = new FileWriter(fileToDownload);
                bw = new BufferedWriter(fileWriter);
                out = new PrintWriter(bw);

                out.println("Locator Code,Channel Type,Country Code,Country,Master Region,Subregion,Name,Postal Code,Contacts,Fax,Opening Hours Mon to Fri,Opening Hours at Sat,Opening Hours at Sun,Holiday Banking,Latitude,Longitude,Location Tag,Language,ATM On Location,CRM On Location,Pawning On Location,Safe Deposit Lockers,Leasing Desk 365 Days,PRV Centre,Branchless Banking,Address");
                out.println("090,Branch,SL,Srilanka,Western,Colombo,Battaramulla,10120,0112 885928/0112 885929,0112 885299,Monday to Friday 9.00am to 4.00pm,Saturday Banking from 9.00am to 1.00pm,365 Day Banking ( 9.00pm to 1.00pm),365 Day Banking ( 9.00pm to 1.00pm),6.873262,79.887582,,English,Yes,Yes,Yes,Yes,Yes,Yes,Yes,No 245 Main Street Battaramulla");

//                 
            } catch (Exception e) {
                retType = "message";
                addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator. ");
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (fileWriter != null) {
                        fileWriter.close();
                    }
                } catch (Exception e) {
                }
            }

            setInputStream(new FileInputStream(fileToDownload));
            setFileName(fileToDownload.getName());
            setContentLength(fileToDownload.length());

        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator ");
            retType = "message";
        }

        return retType;
    }

    public String reportGenerate() {

        System.out.println("called MBankLocatorAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;

        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                MBankLocatorDAO dao = new MBankLocatorDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;

                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    MBankLocatorInputBean searchBean = (MBankLocatorInputBean) session.getAttribute(SessionVarlist.M_BANK_LOCATOR_SEARCH_BEAN);

                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new MBankLocatorInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Mobile_Bank_Locator_Report.csv");
                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.M_BANK_LOCATOR_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " mobile bank locator csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger
                            .getLogger(MBankLocatorAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(MBankLocatorAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mobile bank locator");

            return "message";
        }
        return retMsg;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getLocatorcode() == null || inputBean.getLocatorcode().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LOCATORCODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getLocatorcode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LOCATORCODE;
        } else if (inputBean.getChannelType() == null || inputBean.getChannelType().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CHANNELTYPE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_STATUS;
//        } else if (inputBean.getChannelSubType()!= null && inputBean.getChannelSubType().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CHANNELSUBTYPE;
        } else if (inputBean.getCountryCode() == null || inputBean.getCountryCode().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_COUNTRYCODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getCountryCode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_COUNTRYCODE;
        } else if (inputBean.getCountry() == null || inputBean.getCountry().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_COUNTRY;
        } else if (!Validation.isSpecailCharacter(inputBean.getCountry())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_COUNTRY;
        } else if (inputBean.getMasterRegion() == null || inputBean.getMasterRegion().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_MASTERREGION;
        } else if (!Validation.isSpecailCharacter(inputBean.getMasterRegion())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_MASTERREGION;
//        } else if (inputBean.getRegion()== null || inputBean.getRegion().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_RERION;
        } else if (inputBean.getSubRegion() == null || inputBean.getSubRegion().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_SUBREGION;
        } else if (!Validation.isSpecailCharacter(inputBean.getSubRegion())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_SUBREGION;
        } else if (inputBean.getName() == null || inputBean.getName().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_NAME;
//        } else if (!Validation.isSpecailCharacter(inputBean.getName())) {
//            message = MessageVarList.M_BANK_LOCATOR_INVALID_NAME;
//        } else if (inputBean.getAddress() == null || inputBean.getAddress().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_ADDRESS;
//        } else if (inputBean.getPostalCode()== null || inputBean.getPostalCode().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_POSTALCODE;
        } else if (inputBean.getPostalCode() != null && !inputBean.getPostalCode().trim().isEmpty() && !Validation.isSpecailCharacter(inputBean.getPostalCode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_POSTALCODE;
//        } else if (inputBean.getContacts() == null || inputBean.getContacts().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CONTACTS;
//        } else if ( inputBean.getContacts()!= null && !inputBean.getContacts().trim().isEmpty() && !Validation.areMobileNumers(inputBean.getContacts().trim())) {
//            message = MessageVarList.M_BANK_LOCATOR_INVALID_CONTACTS;
        } else if (inputBean.getFax() != null && !inputBean.getFax().trim().isEmpty() && !Validation.isFaxNumber(inputBean.getFax().trim())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_FAX;
//        } else if (inputBean.getOpeningHsMonFri() == null || inputBean.getOpeningHsMonFri().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_MON_FRI;
//        } else if (inputBean.getOpeningHsSat()== null || inputBean.getOpeningHsSat().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_SAT;
//        } else if (inputBean.getOpeningHsSunHol()== null || inputBean.getOpeningHsSunHol().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_SUN;
//        } else if (inputBean.getGeoCode() == null || inputBean.getGeoCode().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_GEOCODE;
        } else if (inputBean.getLatitude() == null || inputBean.getLatitude().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LATITUDE;
        } else if (Validation.isLongitudeLatitude(inputBean.getLatitude())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LATITUDE;
        } else if (inputBean.getLongitude() == null || inputBean.getLongitude().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LONGITUDE;
        } else if (Validation.isLongitudeLatitude(inputBean.getLongitude())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LONGITUDE;
//        } else if (inputBean.getLocationTag()== null || inputBean.getLocationTag().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LOCATIONTAG;
        } else if (inputBean.getLanguage() == null || inputBean.getLanguage().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LANGUAGE;
        } else if (!Validation.isSpecailCharacter(inputBean.getLanguage())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LANGUAGE;
        }

        return message;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getLocatorid() == null || inputBean.getLocatorid().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LOCATORID;
        } else if (!Validation.isSpecailCharacter(inputBean.getLocatorcode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LOCATORCODE;
        } else if (inputBean.getLocatorcode() == null || inputBean.getLocatorcode().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LOCATORCODE;
        } else if (inputBean.getChannelType() == null || inputBean.getChannelType().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CHANNELTYPE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_STATUS;
//        } else if (inputBean.getChannelSubType()!= null && inputBean.getChannelSubType().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CHANNELSUBTYPE;
        } else if (inputBean.getCountryCode() == null || inputBean.getCountryCode().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_COUNTRYCODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getCountryCode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_COUNTRYCODE;
        } else if (inputBean.getCountry() == null || inputBean.getCountry().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_COUNTRY;
        } else if (!Validation.isSpecailCharacter(inputBean.getCountry())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_COUNTRY;
        } else if (inputBean.getMasterRegion() == null || inputBean.getMasterRegion().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_MASTERREGION;
        } else if (!Validation.isSpecailCharacter(inputBean.getMasterRegion())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_MASTERREGION;
//        } else if (inputBean.getRegion()== null || inputBean.getRegion().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_RERION;
        } else if (inputBean.getSubRegion() == null || inputBean.getSubRegion().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_SUBREGION;
        } else if (!Validation.isSpecailCharacter(inputBean.getSubRegion())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_SUBREGION;
        } else if (inputBean.getName() == null || inputBean.getName().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_NAME;
//        } else if (!Validation.isSpecailCharacter(inputBean.getName())) {
//            message = MessageVarList.M_BANK_LOCATOR_INVALID_NAME;
//        } else if (inputBean.getAddress() == null || inputBean.getAddress().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_ADDRESS;
//        } else if (inputBean.getPostalCode()== null || inputBean.getPostalCode().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_POSTALCODE;
        } else if (inputBean.getPostalCode() != null && !inputBean.getPostalCode().trim().isEmpty() && !Validation.isSpecailCharacter(inputBean.getPostalCode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_POSTALCODE;
//        } else if (inputBean.getContacts() == null || inputBean.getContacts().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CONTACTS;
//        } else if ( inputBean.getContacts()!= null && !inputBean.getContacts().trim().isEmpty() && !Validation.areMobileNumers(inputBean.getContacts().trim())) {
//            message = MessageVarList.M_BANK_LOCATOR_INVALID_CONTACTS;
        } else if (inputBean.getFax() != null && !inputBean.getFax().trim().isEmpty() && !Validation.isFaxNumber(inputBean.getFax().trim())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_FAX;
//        } else if (inputBean.getOpeningHsMonFri() == null || inputBean.getOpeningHsMonFri().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_MON_FRI;
//        } else if (inputBean.getOpeningHsSat()== null || inputBean.getOpeningHsSat().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_SAT;
//        } else if (inputBean.getOpeningHsSunHol()== null || inputBean.getOpeningHsSunHol().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_SUN;
//        } else if (inputBean.getGeoCode() == null || inputBean.getGeoCode().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_GEOCODE;
        } else if (inputBean.getLatitude() == null || inputBean.getLatitude().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LATITUDE;
        } else if (Validation.isLongitudeLatitude(inputBean.getLatitude())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LATITUDE;
        } else if (inputBean.getLongitude() == null || inputBean.getLongitude().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LONGITUDE;
        } else if (Validation.isLongitudeLatitude(inputBean.getLongitude())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LONGITUDE;
//        } else if (inputBean.getLocationTag()== null || inputBean.getLocationTag().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LOCATIONTAG;
        } else if (inputBean.getLanguage() == null || inputBean.getLanguage().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LANGUAGE;
        } else if (!Validation.isSpecailCharacter(inputBean.getLanguage())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LANGUAGE;
        }

        return message;
    }

    private String validateUpload() throws Exception {
        String message = "";
        FileReader isr = null;
        BufferedReader br = null;
        Session hSession = null;
        File csvFile = null;

        try {

            String[] parts = new String[0];
            int countrecord = 1;
            int succesrec = 0;
            String thisLine = null;
            boolean getline = false;
            String token = "";
            MBankLocatorDAO dao = new MBankLocatorDAO();

            csvFile = inputBean.getConXL();
            isr = new FileReader(csvFile);
            br = new BufferedReader(isr);
            hSession = HibernateInit.sessionFactory.openSession();

            while ((thisLine = br.readLine()) != null) {
                if (thisLine.trim().equals("")) {
                    continue;
                } else {
                    if (getline) {
                        token = thisLine;

//                        System.err.println(token);
                        try {
                            parts = token.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", 26);
                            inputBean.setLocatorcode(Common.removeCsvDoubleQuotation(parts[0]));
                            inputBean.setChannelType(Common.removeCsvDoubleQuotation(parts[1]));
                            inputBean.setCountryCode(Common.removeCsvDoubleQuotation(parts[2]));
                            inputBean.setCountry(Common.removeCsvDoubleQuotation(parts[3]));
                            inputBean.setMasterRegion(Common.removeCsvDoubleQuotation(parts[4]));
                            inputBean.setSubRegion(Common.removeCsvDoubleQuotation(parts[5]));
                            inputBean.setName(Common.removeCsvDoubleQuotation(parts[6]));
                            inputBean.setPostalCode(Common.removeCsvDoubleQuotation(parts[7]));
                            inputBean.setContacts(Common.removeCsvDoubleQuotation(parts[8]));
                            inputBean.setFax(Common.removeCsvDoubleQuotation(parts[9]));
                            inputBean.setOpeningHsMonFri(Common.removeCsvDoubleQuotation(parts[10]));
                            inputBean.setOpeningHsSat(Common.removeCsvDoubleQuotation(parts[11]));
                            inputBean.setOpeningHsSunHol(Common.removeCsvDoubleQuotation(parts[12]));
                            inputBean.setHolidayBanking(Common.removeCsvDoubleQuotation(parts[13]));
                            inputBean.setLatitude(Common.removeCsvDoubleQuotation(parts[14]));
                            inputBean.setLongitude(Common.removeCsvDoubleQuotation(parts[15]));
                            inputBean.setLocationTag(Common.removeCsvDoubleQuotation(parts[16]));
                            inputBean.setLanguage(Common.removeCsvDoubleQuotation(parts[17]));
                            inputBean.setAtmOnLocation(Common.removeCsvDoubleQuotation(parts[18]));
                            inputBean.setCrmOnLocation(Common.removeCsvDoubleQuotation(parts[19]));
                            inputBean.setPawningOnLocation(Common.removeCsvDoubleQuotation(parts[20]));
                            inputBean.setSafeDepositLockers(Common.removeCsvDoubleQuotation(parts[21]));
                            inputBean.setLeasingDesk365(Common.removeCsvDoubleQuotation(parts[22]));
                            inputBean.setPrvCentre(Common.removeCsvDoubleQuotation(parts[23]));
                            inputBean.setBranchlessBanking(Common.removeCsvDoubleQuotation(parts[24]));
                            inputBean.setAddress(Common.removeCsvDoubleQuotation(parts[25]));

                            inputBean.setStatus(CommonVarList.STATUS_ACTIVE);

                        } catch (Exception ee) {
                            message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                            break;
                        }
                        countrecord++;
                        if (parts.length >= 3 && message.isEmpty()) {
                            message = dao.validateInputsForCSV(inputBean);
                            if (message.isEmpty()) {
                                message = dao.validateUpload(inputBean, hSession);
                                if (message.isEmpty()) {
                                    succesrec++;
                                } else {
                                    message = message + " at line number " + countrecord;
//                                            + ",success count :" + succesrec;
                                    break;
                                }
                            } else {
                                message = message + " at line number " + countrecord;
//                                        + ",success count :" + succesrec;
                                break;
                            }

                        } else {
                            message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                            break;
                        }
                    } else {
                        getline = true;
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (hSession != null) {
                hSession.flush();
                hSession.close();
            }
        }
        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public String getFile(String file) {
        String msgEx = "";
        if (file == null) {
            msgEx = "Please choose a file to upload.";
        } else {
            msgEx = Validation.isCSV(file);
        }
        return msgEx;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

}
