/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.action.controlpanel.systemconfig.restrequest.AccountListRequest;
import com.epic.ndb.bean.controlpanel.systemconfig.DelistedContractBean;
import com.epic.ndb.bean.controlpanel.systemconfig.DelistedContractInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.DelistedContractPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.DelistedContractDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class DelistedContractAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    DelistedContractInputBean inputBean = new DelistedContractInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    private String serverPath;

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called DelistedContractAction : execute");
        return SUCCESS;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.DELISTED_CONTRACT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("assign".equals(method)) {
            task = TaskVarList.ASSIGN_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("cusAccountInq".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupcsv".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("template".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("upload".equals(method)) {
            task = TaskVarList.UPLOAD_TASK;
        } else if ("pendCsvDownloade".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.DELISTED_CONTRACT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVassign(true);
        inputBean.setVupload(true);
        inputBean.setVdual(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.ASSIGN_TASK)) {
                    inputBean.setVassign(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPLOAD_TASK)) {
                    inputBean.setVupload(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setDelistedCategoryList(dao.getDelistedCategory());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called DelistedContractAction :view");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " delisted contract");
            Logger.getLogger(BillCategoryAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String list() {
        System.out.println("called DelistedContractAction: list");
        try {
            //if (inputBean.isSearch()) {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            DelistedContractDAO dao = new DelistedContractDAO();
            List<DelistedContractBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("CID", inputBean.getCidSearch())
                        //                        + checkEmptyorNullString("Delisted Category", inputBean.getDelistedCategorySearch())
                        + checkEmptyorNullString("Contract Number", inputBean.getContractNumberSearch())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.DELISTED_CONTRACT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Delisted contract search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" Delisted contract " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called DelistedContractAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            DelistedContractDAO dao = new DelistedContractDAO();
            List<DelistedContractPendBean> dataList = dao.getPendingDelistedContractList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.DELISTED_CONTRACT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Delisted contract " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called DelistedContractAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setDelistedCategoryList(dao.getDelistedCategoryActive());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called DelistedContractAction :viewPopup");

        } catch (Exception ex) {
            addActionError("Delisted contract " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String cusAccountInq() {

        String message = "";
        System.out.println("called CustomerSearchAction : cusAccountInq");

        AccountListRequest accListRequest = new AccountListRequest();

        try {

            message = this.validateAccountInq();

            if (message.isEmpty()) {
                message = accListRequest.getAccountList(inputBean);

                if (!message.isEmpty()) {
                    inputBean.setMessage(message);

                } else {
                    DelistedContractDAO dao = new DelistedContractDAO();

                    List<String> currentContractNumList = dao.getDelistedContractNumberByCID(inputBean.getCid());
                    inputBean.setCurrentContractNumberBox(currentContractNumList);

                    List<String> tempNewontractNumList = inputBean.getNewContractNumberBox();

                    for (String currentContact : currentContractNumList) {
                        if (tempNewontractNumList.contains(currentContact)) {
                            inputBean.getNewContractNumberBox().remove(currentContact);
                        }
                    }
                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.DELISTED_CONTRACT_CID, inputBean.getCid());
                }
            } else {
                inputBean.setMessage(message);
            }

        } catch (Exception e) {
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " delisted contract");
        }

        return "cusaccountinq";
    }

    public String assign() {

        System.out.println("called DelistedContractAction : assign");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            DelistedContractDAO dao = new DelistedContractDAO();
            String message = "";

//            message = this.validateInputs();
            if (message.isEmpty()) {
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                inputBean.setCid((String) session.getAttribute(SessionVarlist.DELISTED_CONTRACT_CID));
                StringBuilder stringBuilderNew = new StringBuilder();
                stringBuilderNew.append(inputBean.getCid().trim())
                        .append("|").append(CommonVarList.DELISTED_CATEGORY_ACCOUNT_CODE)
                        .append("|");
                String stringSeparator = "";//for first no separator
                for (String contractNumber : inputBean.getCurrentContractNumberBox()) {
                    stringBuilderNew.append(stringSeparator).append(contractNumber);
                    stringSeparator = ",";//more than one need separator
                }

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ASSIGN_TASK, PageVarList.DELISTED_CONTRACT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Requested to assign delisted contract (CID: " + inputBean.getCid() + ") ", null, null, stringBuilderNew.toString());

                message = dao.insertDelistedContract(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ASSIGN_TASK_PENDING + "delisted contract ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " delisted contract");
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String delete() {

        System.out.println("called DelistedContractAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            DelistedContractDAO dao = new DelistedContractDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.DELISTED_CONTRACT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete delisted contract (  CID: " + inputBean.getCid() + ", delisted category: " + inputBean.getDelistedCategory() + ", contract number: " + inputBean.getContractNumber() + ")", null);
            message = dao.deleteDelistedContract(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " delisted contract ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called DelistedContractAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                DelistedContractDAO dao = new DelistedContractDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.DELISTED_CONTRACT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmDelistedContract(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " delisted contract");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called DelistedContractAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                DelistedContractDAO dao = new DelistedContractDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.DELISTED_CONTRACT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectDelistedContract(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " delisted contract");
        }
        return retType;
    }

    private String validateAccountInq() {
        String message = "";

        if (inputBean.getCid() == null || inputBean.getCid().trim().isEmpty()) {
            message = MessageVarList.DELISTED_CONTRACT_EMPTY_CID;
        }

        return message;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getCurrentContractNumberBox() == null || inputBean.getCurrentContractNumberBox().isEmpty()) {
            message = MessageVarList.DELISTED_CONTRACT_EMPTY_CONTRACT_NUMBER;
        }
        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public String viewPopupcsv() {
        String result = "viewpopupcsv";
        System.out.println("called DelistedContractAction : viewPopupcsv");
        try {
            this.applyUserPrivileges();

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception e) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " delisted contract");
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    public String template() {

        System.out.println("called DelistedContractAction: template");
        String retType = "csv";
        FileWriter fileWriter = null;
        BufferedWriter bw = null;
        PrintWriter out = null;
        try {
            ServletContext context = ServletActionContext.getServletContext();
            String destPath = context.getRealPath("/resouces/csv_temp/delistedcontract");
            File fileToDownload = new File(destPath, "delistedcontract.csv");

            try {
                fileWriter = new FileWriter(fileToDownload);
                bw = new BufferedWriter(fileWriter);
                out = new PrintWriter(bw);

                out.println("CID,Delisted Category(ACCOUNT/CARD/INVESTMENT/LEASE/LOAN),Contract Number");
                out.println("796,ACCOUNT,101000012654");

//                 
            } catch (Exception e) {
                retType = "message";
                addActionError(MessageVarList.COMMON_ERROR_PROCESS + " delisted contract");
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (fileWriter != null) {
                        fileWriter.close();
                    }
                } catch (Exception e) {
                }
            }

            setInputStream(new FileInputStream(fileToDownload));
            setFileName(fileToDownload.getName());
            setContentLength(fileToDownload.length());

        } catch (Exception e) {
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " delisted contract");
            retType = "message";
        }

        return retType;
    }

    public String upload() {
        System.out.println("called DelistedContractAction : upload");
        String result = "messagecsv";
        ServletContext context = ServletActionContext.getServletContext();

        if (Common.getOS_Type().equals("WINDOWS")) {
            this.setServerPath(context.getRealPath("/resouces/csv_temp/delistedcontract"));
        } else if (Common.getOS_Type().equals("LINUX")) {
            this.setServerPath("/app_conf/Epic/csv_temp/delistedcontract");
        }
        try {
            if (inputBean.getHiddenId() != null) {

                HttpServletRequest request = ServletActionContext.getRequest();
                DelistedContractDAO dao = new DelistedContractDAO();

                String message = "";

                DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                Date date = new Date();

                message = this.getFile(inputBean.getConXLFileName()); // get file

                inputBean.setConXLFileName(dateFormat.format(date) + inputBean.getConXLFileName());

                File directory = new File(getServerPath());

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                if (-1 != inputBean.getConXLFileName().lastIndexOf("\\")) {
                    inputBean.setConXLFileName(inputBean.getConXLFileName().substring(inputBean.getConXLFileName().lastIndexOf("\\") + 1));
                }
                File filetoCreate = new File(getServerPath(), inputBean.getConXLFileName());
//                    if (isfileexists==true) {            //set to database
                if (filetoCreate.exists()) {                //set to local 
                    addActionError("File already exists.");
                    System.err.println("File already exists.");
                } else {

                }

                if (message.isEmpty()) {

                    if (inputBean.getConXL() == null) {
                    } else {
                        message = this.validateUpload();
                        if (message.isEmpty()) {
                            FileUtils.copyFile(inputBean.getConXL(), filetoCreate);
                            //                        System.out.println(filetoCreate.getPath());
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPLOAD_TASK, PageVarList.DELISTED_CONTRACT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Requested to upload delisted contract ", null, null, null);
                            message = dao.uploadDelistedContract(inputBean, audit);
                        }
                    }

                }

                if (message.isEmpty()) {
                    addActionMessage("File uploaded successfully");
                    System.err.println("File uploaded successfully");

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " delisted contract");
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, ex);

        }
        return result;
    }

    public String pendCsvDownloade() {
        System.out.println("called DelistedContractAction : pendCsvDownloade");
        String message = null;
        String retType = "pendcsvdownloade";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                DelistedContractDAO dao = new DelistedContractDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.VIEW_PENDING_TASK, PageVarList.DELISTED_CONTRACT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Pending delisted contract csv file downloaded by pending task id " + inputBean.getId(), null);
                message = dao.pendDelistedContractCsvDownloade(inputBean, audit);
                if (!message.isEmpty()) {
                    addActionError(message);
                    retType = "message";
                } else {
                    setInputStream(inputBean.getFileInputStream());
                    setContentLength(inputBean.getFileLength());
                }
            } else {
                addActionError("Empty pending task ID.");
                retType = "message";
            }
        } catch (Exception e) {
            Logger.getLogger(DelistedContractAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " delisted contract");
            retType = "message";
        }
        return retType;
    }

    private String validateUpload() throws Exception {
        String message = "";
        FileReader isr = null;
        BufferedReader br = null;
        Session hSession = null;
        File csvFile = null;

        try {

            String[] parts = new String[0];
            int countrecord = 1;
            int succesrec = 0;
            String thisLine = null;
            boolean getline = false;
            String token = "";
            DelistedContractDAO dao = new DelistedContractDAO();

            csvFile = inputBean.getConXL();
            isr = new FileReader(csvFile);
            br = new BufferedReader(isr);
            hSession = HibernateInit.sessionFactory.openSession();

            while ((thisLine = br.readLine()) != null) {
                if (thisLine.trim().equals("")) {
                    continue;
                } else {
                    if (getline) {
                        token = thisLine;

//                        System.err.println(token);
                        try {
                            parts = token.split("\\,", 3);
                            inputBean.setCid(parts[0].trim());
                            inputBean.setDelistedCategory(parts[1].trim());
                            inputBean.setContractNumber(parts[2].trim());
                        } catch (Exception ee) {
                            message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                            break;
                        }
                        countrecord++;
                        if (parts.length >= 3 && message.isEmpty()) {
                            message = dao.validateInputsForCSV(inputBean);
                            if (message.isEmpty()) {
                                message = dao.validateUpload(inputBean, hSession);
                                if (message.isEmpty()) {
                                    succesrec++;
                                } else {
                                    message = message + " at line number " + countrecord;
//                                            + ",success count :" + succesrec;
                                    break;
                                }
                            } else {
                                message = message + " at line number " + countrecord;
//                                        + ",success count :" + succesrec;
                                break;
                            }

                        } else {
                            message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                            break;
                        }
                    } else {
                        getline = true;
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (hSession != null) {
                hSession.flush();
                hSession.close();
            }

        }
        return message;
    }

    public String getFile(String file) {
        String msgEx = "";
        if (file == null) {
            msgEx = "Please choose a file to upload.";
        } else {
            msgEx = Validation.isCSV(file);
        }
        return msgEx;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }
}
