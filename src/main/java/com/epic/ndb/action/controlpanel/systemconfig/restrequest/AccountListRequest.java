/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig.restrequest;

import com.epic.ndb.bean.controlpanel.systemconfig.ContractDetailBean;
import com.epic.ndb.bean.controlpanel.systemconfig.DelistedContractInputBean;
import com.epic.ndb.bean.customermanagement.SwtConfigBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.varlist.CommonVarList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sivaganesan_t
 */
public class AccountListRequest {
    public String getAccountList(DelistedContractInputBean inputbean) throws IOException, Exception {
        String response = "";
        String message = "";
        URL url = null;
        HttpURLConnection urlConnection = null;
        String urnDetail = "";
        String urlDetail = "";
        BufferedWriter bWriter = null;
        BufferedReader bReader = null;
        String request = "";
        try {
            
            CommonDAO dao =new CommonDAO();
            
            SwtConfigBean swtConfigBean= dao.getSwitchConfig();
            
            urnDetail= dao.findCommonConfigById(CommonVarList.COMMON_CONFIG_ACCOUNT_LIST_URN).getParamvalue();
            urlDetail=swtConfigBean.getNtbRestBaseUrl()+urnDetail;
            
            url = new URL(urlDetail);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Authorization", swtConfigBean.getRestAutorization());

            request = "{\n"
                    + "   \"mode\":" + 2 + ",\n"
//                    + "    \"accountNumber\":\"" + "421689038888" + "\"\n"
                    + "    \"cif\":\"" + inputbean.getCid()+ "\"\n"
                    + "}";

            System.out.println("request              : " + request);

            bWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
            bWriter.write(request);
            bWriter.flush();

            // Get the response
            bReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while (null != ((line = bReader.readLine()))) {
                response += line;
            }

            System.out.println("reponse              : " + response);

            message =getResponseToAccountList(inputbean,response);
            
        } catch (IOException e) {
            message="Fail to create connection";
            System.err.println("Fail to create connection"+e.getMessage());
        } catch (Exception ex) {
            throw ex;
        }
        return message;
    }

    private String getResponseToAccountList(DelistedContractInputBean inputbean,String switchResponse) throws Exception {
        List<String> accountList = new ArrayList<String>();
        String accInfoResponseError = "";
         List<ContractDetailBean> contractDetailList = new ArrayList<ContractDetailBean>();
        try {
            
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(switchResponse);
            
            String accInfoResponse = ((String) obj.get("responsecode"));
            
            if (accInfoResponse!=null && (accInfoResponse.equals("00") || accInfoResponse.equals("000"))) {
                
                JSONArray accountlist = (JSONArray) obj.get("accountList");
                
                for (Object o : accountlist) {
                    if (o instanceof JSONObject) {

                        JSONObject account =  (JSONObject) o;
                        
                        String account_number = (String) account.get("Accountnumber");
                        String account_status = (String) account.get("Account_status");
                        String available_balance = (String) account.get("Available_balance");
                        String account_type = (String) account.get("Account_type");
                        String accountcurrency = (String) account.get("Accountcurrency");
                        String accountproduct = (String) account.get("Accountproduct");
                        String account_name = (String) account.get("Account_name");
                        
                        ContractDetailBean contractDetailBean =new ContractDetailBean();
                        
                        contractDetailBean.setAccName(account_name);
                        contractDetailBean.setAccNumber(account_number);
                        contractDetailBean.setAccProduct(accountproduct);
                        contractDetailBean.setAccStatus(account_status);
                        contractDetailBean.setAccType(account_type);
                        contractDetailBean.setAvailBalance(available_balance);
                        contractDetailBean.setCurrency(accountcurrency);

                        contractDetailList.add(contractDetailBean);
                        accountList.add(account_number);
                        System.out.println("--------------" + account_number);
                    }
                }
                inputbean.setContractDatailList(contractDetailList);
                inputbean.setNewContractNumberBox(accountList);
            } else {
                accInfoResponseError = ((String) obj.get("responseerror"));
            }
        } catch (Exception e) {
            throw e;
        }
        return accInfoResponseError;
    }
}
