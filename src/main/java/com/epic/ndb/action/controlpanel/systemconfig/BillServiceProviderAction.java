/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.BillServiceProviderBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BillServiceProviderInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BillServiceProviderPendBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.BillServiceProviderDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.mapping.BillCustomField;
import com.epic.ndb.util.mapping.BillServiceProvider;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author prathibha_w
 */
public class BillServiceProviderAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    BillServiceProviderInputBean inputBean = new BillServiceProviderInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called BillServiceProviderAction : execute");
        return SUCCESS;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.BILLER_SERVICE_PROVIDER_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("Delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("activate".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("ViewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupDetail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.BILLER_SERVICE_PROVIDER_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVupload(true);
        inputBean.setVdual(true);
        inputBean.setVgenerate(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPLOAD_TASK)) {
                    inputBean.setVupload(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
            inputBean.setCategoryList(dao.getBillerCategoryList());
            inputBean.setBillerTypeList(dao.getBillBillerTypeList());
            inputBean.setFieldTypeList(dao.getBillFieldTypeList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called BillServiceProviderAction :view");

        } catch (Exception ex) {
            addActionError("Bill Service Provider " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called BillServiceProviderAction: List");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            BillServiceProviderDAO dao = new BillServiceProviderDAO();
            List<BillServiceProviderBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("Provider ID", inputBean.getProviderIdSearch())
                        + checkEmptyorNullString("Provider Name", inputBean.getProviderNameSearch())
                        + checkEmptyorNullString("Description", inputBean.getDescriptionSearch())
                        + checkEmptyorNullString("Bill Category", inputBean.getCategoryCodeSearch())
                        + checkEmptyorNullString("Display Name", inputBean.getDisplayNameSearch())
                        + checkEmptyorNullString("Collection Account", inputBean.getCollectionAccountSearch())
                        + checkEmptyorNullString("Bill Type Code", inputBean.getBillerTypeCodeSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.BILLER_SERVICE_PROVIDER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Bill service provider search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" Bill Service Provider " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String ApproveList() {
        System.out.println("called BillServiceProviderAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            BillServiceProviderDAO dao = new BillServiceProviderDAO();
            List<BillServiceProviderPendBean> dataList = dao.getPendingSPList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.BILLER_SERVICE_PROVIDER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
                session.setAttribute(SessionVarlist.BILL_SERVICE_PROVIDER_SEARCH_BEAN, inputBean);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }

        } catch (Exception e) {
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Bill Service Provider " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String ViewPopup() {
        String result = "viewpopup";
        System.out.println("called BillServiceProviderAction : ViewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setRegexList(dao.getRegexList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
            inputBean.setCategoryList(dao.getBillerCategoryListActiveList());
            inputBean.setBillerTypeList(dao.getBillBillerTypeList());
            inputBean.setFieldTypeList(dao.getBillFieldTypeList());
            inputBean.setCurrencyList(dao.getProductCurrencyList());
            inputBean.setFeeChargeList(dao.getFeeChargeListByTransferType(CommonVarList.BILL_SERVICE_PROVIDER_TRANSFER_TYPE));
            inputBean.setProductTypeList(this.getProductTypeList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called BillServiceProviderAction :ViewPopup");

        } catch (Exception ex) {
            addActionError("Bill Service Provider " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String Add() {
        System.out.println("called BillServiceProviderAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            BillServiceProviderDAO dao = new BillServiceProviderDAO();

            String message = this.validateInputs();

            if (message.isEmpty()) {

                String cus_fields = "";
                if (inputBean.isHavingCustomField()) {
                    cus_fields = "|" + inputBean.getFieldName() + "|"
                            + inputBean.getFieldIdType() + "|"
                            + inputBean.getFieldVal() + "|"
                            + inputBean.getFieldLen() + "|"
                            + inputBean.getFieldValidation() + "|"
                            + inputBean.getPlaceHolder();
                }
                String newv = inputBean.getProviderId() + "|"
                        + inputBean.getProviderName() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getCategoryCode() + "|"
                        + inputBean.getImageUrl() + "|"
                        + inputBean.getDisplayName() + "|"
                        + inputBean.getCollectionAccount() + "|"
                        + inputBean.getBankCode() + "|" + "|"//for commet payType
                        //                        + inputBean.getPayType() + "|"
                        + inputBean.getBillerTypeCode() + "|"
                        + inputBean.getStatus() + "|"
                        + inputBean.getCurrency() + "|"
                        + inputBean.getFeeCharge() + "|"
                        + inputBean.getProductType() + "|"
                        + inputBean.getMobPrefix() + "|"
                        + inputBean.getBillerValCode() + "|"
                        + inputBean.getValidationStatus() + "|"
                        //                        + inputBean.getPlaceHolder() + "|"
                        // fields if add

                        + inputBean.isHavingCustomField()
                        + cus_fields;

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.BILLER_SERVICE_PROVIDER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to add bill service provider ( provider ID : " + inputBean.getProviderId() + " )", null, null, newv);

                message = dao.insertBillSP(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " bill service provider ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Bill Service Provider " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private String validateInputs() throws Exception {

        CommonDAO cmd = new CommonDAO();
        ParameterUserCommon c = cmd.findCommonConfigById(CommonVarList.NDB_BANK_CODE_CEFT);

        String message = "";
        if (inputBean.getProviderId() == null || inputBean.getProviderId().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_ID;
        } else if (inputBean.getProviderName() == null || inputBean.getProviderName().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_NAME;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.BILLER_CATEGORY_MGT_EMPTY_DESCRIPTIION;
        } else if (inputBean.getCategoryCode() == null || inputBean.getCategoryCode().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_CATEGORY_CODE;
        } else if (inputBean.getDisplayName() == null || inputBean.getDisplayName().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_DISPLAY_NAME;
        } else if (inputBean.getBillerTypeCode() != null && inputBean.getBillerTypeCode().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_BILLER_TYPE_CODE;
        } else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_STATUS;
        } else if (inputBean.getCurrency() != null && inputBean.getCurrency().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_CURRENCY;
        } else if (inputBean.getProductType() != null && inputBean.getProductType().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_PRODUCT_TYPE;
        } else if (inputBean.getMobileImgFileName() == null || inputBean.getMobileImgFileName().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_IMAGE;
        } else if (inputBean.getHasImage().equals("IMG")) {
            message = MessageVarList.BILLER_SERVICE_IMAGE_FIREBASE_UPLOAD;
        } else if (inputBean.getBillerTypeCode().equals("MOB") && (inputBean.getMobPrefix() == null || inputBean.getMobPrefix().isEmpty())) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_MOB_PREFIX;
        } else if (inputBean.getBankCode().equals(c.getParamvalue())) {
            if (inputBean.getCollectionAccount() != null && inputBean.getCollectionAccount().isEmpty()) {
                message = MessageVarList.BILLER_SERVICE_IMAGE_COLLECTION_ACCOUNT;
            }
        }

        if (message.isEmpty() && inputBean.isHavingCustomField()) {
            if (inputBean.getFieldName() == null || inputBean.getFieldName().trim().isEmpty()) {
                message = MessageVarList.BILLER_SP_EMPTY_FIELD_NAME;
            } else if (inputBean.getFieldIdType() == null || inputBean.getFieldIdType().trim().isEmpty()) {
                message = MessageVarList.BILLER_SP_EMPTY_FIELD_TYPE;
            } else if ((inputBean.getFieldIdType().equals("2") || inputBean.getFieldIdType().equals("3")) && (inputBean.getFieldVal() == null || inputBean.getFieldVal().trim().isEmpty())) {
                message = MessageVarList.BILLER_SP_EMPTY_FIELD_VAL;
            } else if (inputBean.getFieldIdType().equals("1")) {
                if (inputBean.getFieldLenMin() == null || inputBean.getFieldLenMin().trim().isEmpty()) {
                    message = MessageVarList.BILLER_SP_EMPTY_FIELD_LEN_MIN;
                } else if (inputBean.getFieldLen() == null || inputBean.getFieldLen().trim().isEmpty()) {
                    message = MessageVarList.BILLER_SP_EMPTY_FIELD_LEN;
                } else if (inputBean.getFieldValidation() == null || inputBean.getFieldValidation().trim().isEmpty()) {
                    message = MessageVarList.BILLER_SP_EMPTY_FIELD_VALIDATION;
                } else {
                    BigDecimal minLen = new BigDecimal(inputBean.getFieldLenMin());
                    BigDecimal maxLen = new BigDecimal(inputBean.getFieldLen());
                    if (minLen.compareTo(maxLen) > 0) {
                        message = MessageVarList.BILLER_SP_INVALID_FIELD_LEN_MIN_LESS_MAX;
                    } else {
                        String fieldVal = inputBean.getFieldValidation() + "{" + inputBean.getFieldLenMin().trim() + "," + inputBean.getFieldLen().trim() + "}$";
                        inputBean.setFieldValidation(fieldVal);
                    }
                }
            }
        }

        return message;
    }

    public String Delete() {
        System.out.println("called BillServiceProviderAction : Delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            BillServiceProviderDAO dao = new BillServiceProviderDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.BILLER_SERVICE_PROVIDER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete bill service provider ( provider ID : " + inputBean.getProviderId() + " )", null);
            message = dao.deleteBillSP(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " bill service provider ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String Detail() {
        System.out.println("called BillServiceProviderAction: Detail");
        BillServiceProvider billsp = null;
        BillCustomField cusfield = null;
        try {
            if (inputBean.getProviderId() != null && !inputBean.getProviderId().isEmpty()) {

                BillServiceProviderDAO dao = new BillServiceProviderDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setRegexList(commonDAO.getRegexList());
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
                inputBean.setCategoryList(commonDAO.getBillerCategoryListActiveList());
                inputBean.setBillerTypeList(commonDAO.getBillBillerTypeList());
                inputBean.setFieldTypeList(commonDAO.getBillFieldTypeList());
                inputBean.setCurrencyList(commonDAO.getProductCurrencyList());
                inputBean.setFeeChargeList(commonDAO.getFeeChargeListByTransferType(CommonVarList.BILL_SERVICE_PROVIDER_TRANSFER_TYPE));
                inputBean.setProductTypeList(this.getProductTypeList());

                billsp = dao.findBillSPById(inputBean.getProviderId());

                cusfield = dao.findCusFieldByProviderId(inputBean.getProviderId());

                inputBean.setProviderName(billsp.getProviderName());
                inputBean.setDescription(billsp.getDescription());
                inputBean.setCategoryCode(billsp.getBillCategory().getBillercatcode());
                inputBean.setImageUrl(billsp.getImageUrl());
                inputBean.setDisplayName(billsp.getDisplayName());
                inputBean.setCollectionAccount(billsp.getCollectionAccount());
                inputBean.setBankCode(billsp.getBankCode());
//                inputBean.setPayType(billsp.getPayType());
                inputBean.setBillerTypeCode(billsp.getBillBillerType().getBillerTypeCode());
                inputBean.setStatus(billsp.getStatus().getStatuscode());
                inputBean.setCurrency(billsp.getCurrency().getCurrencyCode());
                inputBean.setFeeCharge(billsp.getChargeCode());
                inputBean.setProductType(billsp.getProductType().toString());
                if(billsp.getBillerValCode() != null && !billsp.getBillerValCode().isEmpty()){
                    inputBean.setBillerValCode(billsp.getBillerValCode());
                }else{
                    inputBean.setBillerValCode("");
                }
                if(billsp.getValidationStatus()!= null){
                    inputBean.setValidationStatus(billsp.getValidationStatus().getStatuscode());
                }else{
                    inputBean.setValidationStatus("");
                }
                if (billsp.getMobPrefix() != null && !billsp.getMobPrefix().isEmpty()) {
                    inputBean.setMobPrefix(billsp.getMobPrefix());
                } else {
                    inputBean.setMobPrefix("");
                }
//                inputBean.setPlaceHolder(billsp.getPlaceHolder());
                inputBean.setHavingCustomField(billsp.getIsHavingCustomField());

                String fieldValidationOld = "";
                String fieldLengthOld = "";
                String fieldValueOld = "";
                String placeHolderOld = "";

                if (billsp.getIsHavingCustomField()) {
                    inputBean.setFieldName(cusfield.getFieldName());
                    inputBean.setFieldIdType(Integer.toString(cusfield.getBillFieldType().getId()));
                    if (inputBean.getFieldIdType().equals("3") || inputBean.getFieldIdType().equals("2")) {
                        inputBean.setFieldVal(cusfield.getFieldValue());
                        fieldValueOld = cusfield.getFieldValue();//for old value
                    } else if (inputBean.getFieldIdType().equals("1")) {
                        inputBean.setFieldLen(cusfield.getFieldLength());
                        fieldLengthOld = cusfield.getFieldLength();//for old Value
                        //                    inputBean.setFieldValidation(cusfield.getFieldValidation());
                        if (cusfield.getFieldValidation() != null && !cusfield.getFieldValidation().isEmpty()) {
                            fieldValidationOld = cusfield.getFieldValidation();//for old Value
                            int fieldValidationEndIndex = cusfield.getFieldValidation().indexOf("{");
                            int fieldMinLengthStartIndex = cusfield.getFieldValidation().indexOf("{") + 1;
                            int fieldMinLengthEndIndex = cusfield.getFieldValidation().indexOf(",");
                            String fieldValidation = cusfield.getFieldValidation().substring(0, fieldValidationEndIndex);
                            String fieldMinLength = cusfield.getFieldValidation().substring(fieldMinLengthStartIndex, fieldMinLengthEndIndex);
                            inputBean.setFieldValidation(fieldValidation);
                            inputBean.setFieldLenMin(fieldMinLength);
                        }
                        if (cusfield.getPlaceHolder() != null) {
                            inputBean.setPlaceHolder(cusfield.getPlaceHolder());
                            placeHolderOld = cusfield.getPlaceHolder();
                        }
                    }
                }
//                String placeholder = "";
//                if (inputBean.getPlaceHolder() != null && !inputBean.getPlaceHolder().isEmpty()) {
//                    placeholder = inputBean.getPlaceHolder();
//                }
                String collectionAccount = "";
                if (inputBean.getCollectionAccount() != null && !inputBean.getCollectionAccount().isEmpty()) {
                    collectionAccount = inputBean.getCollectionAccount();
                }
                String bankCode = "";
                if (inputBean.getBankCode() != null && !inputBean.getBankCode().isEmpty()) {
                    bankCode = inputBean.getBankCode();
                }

                String feeCharge = "";
                if (inputBean.getFeeCharge() != null && !inputBean.getFeeCharge().isEmpty()) {
                    feeCharge = inputBean.getFeeCharge();
                }

                String cus_fields = "";
                if (inputBean.isHavingCustomField()) {
                    cus_fields = "|" + inputBean.getFieldName() + "|"
                            + inputBean.getFieldIdType() + "|"
                            + fieldValueOld + "|"
                            + fieldLengthOld + "|"
                            + fieldValidationOld + "|"
                            + placeHolderOld;
                }

                inputBean.setOldvalue(
                        inputBean.getProviderId() + "|"
                        + inputBean.getProviderName() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getCategoryCode() + "|"
                        + inputBean.getImageUrl() + "|"
                        + inputBean.getDisplayName() + "|"
                        + collectionAccount + "|"
                        + bankCode + "|" + "|"//for commet payType
                        //                        + inputBean.getPayType() + "|"
                        + inputBean.getBillerTypeCode() + "|"
                        + inputBean.getStatus() + "|"
                        + inputBean.getCurrency() + "|"
                        + feeCharge + "|"
                        + inputBean.getProductType() + "|"
                        + inputBean.getMobPrefix() + "|"
                        + inputBean.getBillerValCode()+ "|"
                        + inputBean.getValidationStatus()+ "|"
                        //                        + placeholder + "|"
                        // fields if add
                        + inputBean.isHavingCustomField()
                        + cus_fields);

            } else {
                inputBean.setMessage("Empty provider ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Bill Service Provider " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String viewPopupDetail() {
        System.out.println("called BillServiceProviderAction: viewPopupDetail");
        BillServiceProvider billsp = null;
        BillCustomField cusfield = null;
        try {
            if (inputBean.getProviderId() != null && !inputBean.getProviderId().isEmpty()) {

                BillServiceProviderDAO dao = new BillServiceProviderDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setRegexList(commonDAO.getRegexList());
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
                inputBean.setCategoryList(commonDAO.getBillerCategoryListActiveList());
                inputBean.setBillerTypeList(commonDAO.getBillBillerTypeList());
                inputBean.setFieldTypeList(commonDAO.getBillFieldTypeList());
                inputBean.setCurrencyList(commonDAO.getProductCurrencyList());
                inputBean.setFeeChargeList(commonDAO.getFeeChargeListByTransferType(CommonVarList.BILL_SERVICE_PROVIDER_TRANSFER_TYPE));
                inputBean.setProductTypeList(this.getProductTypeList());

                billsp = dao.findBillSPById(inputBean.getProviderId());

                cusfield = dao.findCusFieldByProviderId(inputBean.getProviderId());

                inputBean.setProviderName(billsp.getProviderName());
                inputBean.setDescription(billsp.getDescription());
                inputBean.setCategoryCode(billsp.getBillCategory().getBillercatcode());
                inputBean.setImageUrl(billsp.getImageUrl());
                inputBean.setDisplayName(billsp.getDisplayName());
                inputBean.setCollectionAccount(billsp.getCollectionAccount());
                inputBean.setBankCode(billsp.getBankCode());
//                inputBean.setPayType(billsp.getPayType());
                inputBean.setBillerTypeCode(billsp.getBillBillerType().getBillerTypeCode());
                inputBean.setStatus(billsp.getStatus().getStatuscode());
                inputBean.setCurrency(billsp.getCurrency().getCurrencyCode());
                inputBean.setFeeCharge(billsp.getChargeCode());
                inputBean.setProductType(billsp.getProductType().toString());

                if (billsp.getBillerValCode() != null && !billsp.getBillerValCode().isEmpty()) {
                    inputBean.setBillerValCode(billsp.getBillerValCode());
                } else {
                    inputBean.setBillerValCode("");
                }

                if (billsp.getValidationStatus()!= null ) {
                    inputBean.setValidationStatus(billsp.getValidationStatus().getStatuscode());
                } else {
                    inputBean.setValidationStatus("");
                }
                if (billsp.getMobPrefix() != null && !billsp.getMobPrefix().isEmpty()) {
                    inputBean.setMobPrefix(billsp.getMobPrefix());
                } else {
                    inputBean.setMobPrefix("");
                }
//                inputBean.setPlaceHolder(billsp.getPlaceHolder());
                inputBean.setHavingCustomField(billsp.getIsHavingCustomField());

                String fieldValidationOld = "";
                String fieldLengthOld = "";
                String fieldValueOld = "";
                String placeHolderOld = "";

                if (billsp.getIsHavingCustomField()) {
                    inputBean.setFieldName(cusfield.getFieldName());
                    inputBean.setFieldIdType(Integer.toString(cusfield.getBillFieldType().getId()));
                    if (inputBean.getFieldIdType().equals("3") || inputBean.getFieldIdType().equals("2")) {
                        inputBean.setFieldVal(cusfield.getFieldValue());
                        fieldValueOld = cusfield.getFieldValue();//for old value
                    } else if (inputBean.getFieldIdType().equals("1")) {
                        inputBean.setFieldLen(cusfield.getFieldLength());
                        fieldLengthOld = cusfield.getFieldLength();//for old Value
                        //                    inputBean.setFieldValidation(cusfield.getFieldValidation());
                        if (cusfield.getFieldValidation() != null && !cusfield.getFieldValidation().isEmpty()) {
                            fieldValidationOld = cusfield.getFieldValidation();//for old Value
                            int fieldValidationEndIndex = cusfield.getFieldValidation().indexOf("{");
                            int fieldMinLengthStartIndex = cusfield.getFieldValidation().indexOf("{") + 1;
                            int fieldMinLengthEndIndex = cusfield.getFieldValidation().indexOf(",");
                            String fieldValidation = cusfield.getFieldValidation().substring(0, fieldValidationEndIndex);
                            String fieldMinLength = cusfield.getFieldValidation().substring(fieldMinLengthStartIndex, fieldMinLengthEndIndex);
                            inputBean.setFieldValidation(fieldValidation);
                            inputBean.setFieldLenMin(fieldMinLength);
                        }
                        if (cusfield.getPlaceHolder() != null) {
                            inputBean.setPlaceHolder(cusfield.getPlaceHolder());
                            placeHolderOld = cusfield.getPlaceHolder();
                        }
                    }
                }
//                String placeholder = "";
//                if (inputBean.getPlaceHolder() != null && !inputBean.getPlaceHolder().isEmpty()) {
//                    placeholder = inputBean.getPlaceHolder();
//                }
                String collectionAccount = "";
                if (inputBean.getCollectionAccount() != null && !inputBean.getCollectionAccount().isEmpty()) {
                    collectionAccount = inputBean.getCollectionAccount();
                }
                String bankCode = "";
                if (inputBean.getBankCode() != null && !inputBean.getBankCode().isEmpty()) {
                    bankCode = inputBean.getBankCode();
                }

                String feeCharge = "";
                if (inputBean.getFeeCharge() != null && !inputBean.getFeeCharge().isEmpty()) {
                    feeCharge = inputBean.getFeeCharge();
                }

                String cus_fields = "";
                if (inputBean.isHavingCustomField()) {
                    cus_fields = "|" + inputBean.getFieldName() + "|"
                            + inputBean.getFieldIdType() + "|"
                            + fieldValueOld + "|"
                            + fieldLengthOld + "|"
                            + fieldValidationOld + "|"
                            + placeHolderOld;
                }

                inputBean.setOldvalue(
                        inputBean.getProviderId() + "|"
                        + inputBean.getProviderName() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getCategoryCode() + "|"
                        + inputBean.getImageUrl() + "|"
                        + inputBean.getDisplayName() + "|"
                        + collectionAccount + "|"
                        + bankCode + "|" + "|"//for commet payType
                        //                        + inputBean.getPayType() + "|"
                        + inputBean.getBillerTypeCode() + "|"
                        + inputBean.getStatus() + "|"
                        + inputBean.getCurrency() + "|"
                        + feeCharge + "|"
                        + inputBean.getProductType() + "|"
                        + inputBean.getMobPrefix() + "|"
                        + inputBean.getBillerValCode()+ "|"
                        + inputBean.getValidationStatus()+ "|"
                        //                        + placeholder + "|"
                        // fields if add
                        + inputBean.isHavingCustomField()
                        + cus_fields);

            } else {
                inputBean.setMessage("Empty provider ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Bill Service Provider " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "viewpopupdetail";
    }

    public String Update() {

        System.out.println("called BillServiceProviderAction : update");
        String retType = "message";

        try {
            if (inputBean.getProviderId() != null && !inputBean.getProviderId().isEmpty()) {
                BillServiceProviderDAO dao = new BillServiceProviderDAO();

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String placeholder = "";
                    if (inputBean.getPlaceHolder() != null && !inputBean.getPlaceHolder().isEmpty()) {
                        placeholder = inputBean.getPlaceHolder();
                    }

                    String collectionAccount = "";
                    if (inputBean.getCollectionAccount() != null && !inputBean.getCollectionAccount().isEmpty()) {
                        collectionAccount = inputBean.getCollectionAccount();
                    }
                    String bankCode = "";
                    if (inputBean.getBankCode() != null && !inputBean.getBankCode().isEmpty()) {
                        bankCode = inputBean.getBankCode();
                    }
                    String feeCharge = "";
                    if (inputBean.getFeeCharge() != null && !inputBean.getFeeCharge().isEmpty()) {
                        feeCharge = inputBean.getFeeCharge();
                    }

                    String cus_fields = "";
                    if (inputBean.isHavingCustomField()) {
                        cus_fields = "|" + inputBean.getFieldName() + "|"
                                + inputBean.getFieldIdType() + "|"
                                + inputBean.getFieldVal() + "|"
                                + inputBean.getFieldLen() + "|"
                                + inputBean.getFieldValidation() + "|"
                                + placeholder;
                    }
                    String newv = inputBean.getProviderId() + "|"
                            + inputBean.getProviderName() + "|"
                            + inputBean.getDescription() + "|"
                            + inputBean.getCategoryCode() + "|"
                            + inputBean.getImageUrl() + "|"
                            + inputBean.getDisplayName() + "|"
                            + collectionAccount + "|"
                            + bankCode + "|" + "|"//for commet payType
                            //                            + inputBean.getPayType() + "|"
                            + inputBean.getBillerTypeCode() + "|"
                            + inputBean.getStatus() + "|"
                            + inputBean.getCurrency() + "|"
                            + feeCharge + "|"
                            + inputBean.getProductType() + "|"
                            + inputBean.getMobPrefix() + "|"
                            + inputBean.getBillerValCode() + "|"
                            + inputBean.getValidationStatus()+ "|"
                            //                            + placeholder + "|"
                            // fields if add
                            + inputBean.isHavingCustomField()
                            + cus_fields;

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) {
                        String newValWithActState = inputBean.getProviderId() + "|"
                                + inputBean.getProviderName() + "|"
                                + inputBean.getDescription() + "|"
                                + inputBean.getCategoryCode() + "|"
                                + inputBean.getImageUrl() + "|"
                                + inputBean.getDisplayName() + "|"
                                + collectionAccount + "|"
                                + bankCode + "|" + "|"//for commet payType
                                + inputBean.getBillerTypeCode() + "|"
                                + CommonVarList.STATUS_ACTIVE + "|"
                                + inputBean.getCurrency() + "|"
                                + feeCharge + "|"
                                + inputBean.getProductType() + "|"
                                + inputBean.getMobPrefix() + "|"
                                + inputBean.getBillerValCode() + "|"
                                + inputBean.getValidationStatus()+ "|"
                                + inputBean.isHavingCustomField()
                                + cus_fields;
                        if (inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) || (inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))) {

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.BILLER_SERVICE_PROVIDER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update bill service provider ( provider ID : " + inputBean.getProviderId() + " )", null, null, newv);
                            message = dao.updateBillSP(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " bill service provider ");
                            } else {
                                addActionError(message);
                            }
                        } else {
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Bill Service Provider " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    private String validateUpdates() throws Exception {

        CommonDAO cmd = new CommonDAO();
        ParameterUserCommon c = cmd.findCommonConfigById(CommonVarList.NDB_BANK_CODE_CEFT);

        String message = "";
        if (inputBean.getProviderId() == null || inputBean.getProviderId().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_ID;
        } else if (inputBean.getProviderName() == null || inputBean.getProviderName().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_NAME;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.BILLER_CATEGORY_MGT_EMPTY_DESCRIPTIION;
        } else if (inputBean.getCategoryCode() == null || inputBean.getCategoryCode().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_CATEGORY_CODE;
        } else if (inputBean.getDisplayName() == null || inputBean.getDisplayName().trim().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_DISPLAY_NAME;
        } else if (inputBean.getBillerTypeCode() != null && inputBean.getBillerTypeCode().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_BILLER_TYPE_CODE;
        } else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_STATUS;
        } else if (inputBean.getCurrency() != null && inputBean.getCurrency().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_CURRENCY;
        } else if (inputBean.getProductType() != null && inputBean.getProductType().isEmpty()) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_PRODUCT_TYPE;
        } else if (inputBean.getHasImage().equals("NOIMG") && (inputBean.getImageUrl() == null || inputBean.getImageUrl().trim().isEmpty())) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_IMAGE;
        } else if (inputBean.getHasImage().equals("IMG")) {
            message = MessageVarList.BILLER_SERVICE_IMAGE_FIREBASE_UPLOAD;
        } else if (inputBean.getBillerTypeCode().equals("MOB") && (inputBean.getMobPrefix() == null || inputBean.getMobPrefix().isEmpty())) {
            message = MessageVarList.BILLER_SERVICE_PROVIDER_EMPTY_MOB_PREFIX;
        } else if (inputBean.getBankCode().equals(c.getParamvalue())) {
            if (inputBean.getCollectionAccount() != null && inputBean.getCollectionAccount().isEmpty()) {
                message = MessageVarList.BILLER_SERVICE_IMAGE_COLLECTION_ACCOUNT;
            }
        }

        if (message.isEmpty() && inputBean.isHavingCustomField()) {
            if (inputBean.getFieldName() == null || inputBean.getFieldName().trim().isEmpty()) {
                message = MessageVarList.BILLER_SP_EMPTY_FIELD_NAME;
            } else if (inputBean.getFieldIdType() == null || inputBean.getFieldIdType().trim().isEmpty()) {
                message = MessageVarList.BILLER_SP_EMPTY_FIELD_TYPE;
            } else if ((inputBean.getFieldIdType().equals("2") || inputBean.getFieldIdType().equals("3")) && (inputBean.getFieldVal() == null || inputBean.getFieldVal().trim().isEmpty())) {
                message = MessageVarList.BILLER_SP_EMPTY_FIELD_VAL;
            } else if (inputBean.getFieldIdType().equals("1")) {
                if (inputBean.getFieldLenMin() == null || inputBean.getFieldLenMin().trim().isEmpty()) {
                    message = MessageVarList.BILLER_SP_EMPTY_FIELD_LEN_MIN;
                } else if (inputBean.getFieldLen() == null || inputBean.getFieldLen().trim().isEmpty()) {
                    message = MessageVarList.BILLER_SP_EMPTY_FIELD_LEN;
                } else if (inputBean.getFieldValidation() == null || inputBean.getFieldValidation().trim().isEmpty()) {
                    message = MessageVarList.BILLER_SP_EMPTY_FIELD_VALIDATION;
                } else {
                    BigDecimal minLen = new BigDecimal(inputBean.getFieldLenMin());
                    BigDecimal maxLen = new BigDecimal(inputBean.getFieldLen());
                    if (minLen.compareTo(maxLen) > 0) {
                        message = MessageVarList.BILLER_SP_INVALID_FIELD_LEN_MIN_LESS_MAX;
                    } else {
                        String fieldVal = inputBean.getFieldValidation() + "{" + inputBean.getFieldLenMin().trim() + "," + inputBean.getFieldLen().trim() + "}$";
                        inputBean.setFieldValidation(fieldVal);
                    }
                }
            }
        }

        return message;
    }

    public String Confirm() {
        System.out.println("called BillServiceProviderAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                BillServiceProviderDAO dao = new BillServiceProviderDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.BILLER_SERVICE_PROVIDER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmBillSP(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Bill Service Provider " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called BillServiceProviderAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                BillServiceProviderDAO dao = new BillServiceProviderDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.BILLER_SERVICE_PROVIDER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectBillSP(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Bill Service Provider " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    public String Find() {
        System.out.println("called BillServiceProviderAction: Find");
        BillServiceProvider billsp = null;
        BillCustomField cusfield = null;
        try {
            if (inputBean.getProviderId() != null && !inputBean.getProviderId().isEmpty()) {

                BillServiceProviderDAO dao = new BillServiceProviderDAO();

                billsp = dao.findBillSPById(inputBean.getProviderId());
                cusfield = dao.findCusFieldByProviderId(inputBean.getProviderId());

                inputBean.setProviderName(billsp.getProviderName());
                inputBean.setDescription(billsp.getDescription());
                inputBean.setCategoryCode(billsp.getBillCategory().getBillercatcode());
                inputBean.setImageUrl(billsp.getImageUrl());
                inputBean.setDisplayName(billsp.getDisplayName());
                inputBean.setCollectionAccount(billsp.getCollectionAccount());
                inputBean.setBankCode(billsp.getBankCode());
                inputBean.setPayType(billsp.getPayType());
                inputBean.setBillerTypeCode(billsp.getBillBillerType().getBillerTypeCode());
                inputBean.setStatus(billsp.getStatus().getStatuscode());
                inputBean.setCurrency(billsp.getCurrency().getCurrencyCode());
                inputBean.setFeeCharge(billsp.getChargeCode());
                inputBean.setProductType(billsp.getProductType().toString());
                if(billsp.getBillerValCode() != null && !billsp.getBillerValCode().isEmpty()){
                    inputBean.setBillerValCode(billsp.getBillerValCode());
                }else{
                    inputBean.setBillerValCode("");
                }
                if(billsp.getValidationStatus()!= null){
                    inputBean.setValidationStatus(billsp.getValidationStatus().getStatuscode());
                }else{
                    inputBean.setValidationStatus("");
                }
                if (billsp.getMobPrefix() != null && !billsp.getMobPrefix().isEmpty()) {
                    inputBean.setMobPrefix(billsp.getMobPrefix());
                } else {
                    inputBean.setMobPrefix("");
                }
//                inputBean.setPlaceHolder(billsp.getPlaceHolder());
                inputBean.setHavingCustomField(billsp.getIsHavingCustomField());

                if (billsp.getIsHavingCustomField()) {
                    inputBean.setFieldName(cusfield.getFieldName());
                    inputBean.setFieldIdType(Integer.toString(cusfield.getBillFieldType().getId()));
                    if (inputBean.getFieldIdType().equals("3") || inputBean.getFieldIdType().equals("2")) {
                        inputBean.setFieldVal(cusfield.getFieldValue());
                    } else if (inputBean.getFieldIdType().equals("1")) {
                        inputBean.setFieldLen(cusfield.getFieldLength());
                        //                    inputBean.setFieldValidation(cusfield.getFieldValidation());
                        if (cusfield.getFieldValidation() != null && !cusfield.getFieldValidation().isEmpty()) {
                            int fieldValidationEndIndex = cusfield.getFieldValidation().indexOf("{");
                            int fieldMinLengthStartIndex = cusfield.getFieldValidation().indexOf("{") + 1;
                            int fieldMinLengthEndIndex = cusfield.getFieldValidation().indexOf(",");
                            String fieldValidation = cusfield.getFieldValidation().substring(0, fieldValidationEndIndex);
                            String fieldMinLength = cusfield.getFieldValidation().substring(fieldMinLengthStartIndex, fieldMinLengthEndIndex);
                            inputBean.setFieldValidation(fieldValidation);
                            inputBean.setFieldLenMin(fieldMinLength);
                        }
                        inputBean.setPlaceHolder(cusfield.getPlaceHolder());
                    }
                }

            } else {
                inputBean.setMessage("Empty bill category code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Bill Category " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BillCategoryAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public String viewPend() {
        System.out.println("called BillServiceProviderAction : viewPend");
        String message = null;
        String retType = "viewPend";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                BillServiceProviderDAO dao = new BillServiceProviderDAO();
                message = dao.viwPendBillSP(inputBean);
                if (!message.isEmpty()) {
                    addActionError(message);
                }
            } else {
                addActionError("Empty pending task ID.");
            }
        } catch (Exception e) {
            Logger.getLogger(BillServiceProviderAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" Bill Service Provider " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return retType;
    }

    public String reportGenerate() {

        System.out.println("called BillServiceProviderAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                BillServiceProviderDAO dao = new BillServiceProviderDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    BillServiceProviderInputBean searchBean = (BillServiceProviderInputBean) session.getAttribute(SessionVarlist.BILL_SERVICE_PROVIDER_SEARCH_BEAN);
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new BillServiceProviderInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Bill_Service_Provider_Report.csv");
                        setContentLength(sb.length());

                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.BILLER_SERVICE_PROVIDER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Bill Service Provider csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " detail csv report");
                    Logger
                            .getLogger(BillServiceProviderAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(BillServiceProviderAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " bill service provider");

            return "message";
        }
        return retMsg;
    }

    private List<CommonKeyVal> getProductTypeList() {

        List<CommonKeyVal> productTypeList = new ArrayList<CommonKeyVal>();

        CommonKeyVal onBoardType1 = new CommonKeyVal();
        onBoardType1.setKey("0");
        onBoardType1.setValue("Others");
        productTypeList.add(onBoardType1);

        CommonKeyVal onBoardType2 = new CommonKeyVal();
        onBoardType2.setKey("1");
        onBoardType2.setValue("NDB");
        productTypeList.add(onBoardType2);

        return productTypeList;

    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }
}
