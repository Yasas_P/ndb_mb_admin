package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.CommonConfigBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CommonConfigInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CommonConfigPendBean;
import com.epic.ndb.dao.controlpanel.systemconfig.CommonConfigDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author jayathissa_d
 */
public class CommonConfigAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    CommonConfigInputBean inputBean = new CommonConfigInputBean();

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called CommonConfigAction : execute");
        return SUCCESS;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.COMMON_CONFIG_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("activate".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("Detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.COMMON_CONFIG_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
//                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.LOGIN_TASK)) {
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            //    CommonDAO dao = new CommonDAO();
            //   inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            //    inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
            
            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called CommonConfigAction :view");

        } catch (Exception ex) {
            addActionError("Common Configuration " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CommonConfigAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called CommonConfigAction: List");
        try {
            //if (inputBean.isSearch()) {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            CommonConfigDAO dao = new CommonConfigDAO();
            List<CommonConfigBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("Parameter Code", inputBean.getParamCodeSearch())
                        + checkEmptyorNullString("Description", inputBean.getDescriptionSearch())
                        + checkEmptyorNullString("Parameter Value", inputBean.getParamValSearch())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.COMMON_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Common configuration management search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(CommonConfigAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" Common configuration " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String ApproveList() {
        System.out.println("called CommonConfigAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            CommonConfigDAO dao = new CommonConfigDAO();
            List<CommonConfigPendBean> dataList = dao.getPendingCommonConfigList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.COMMON_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending common configuration list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(CommonConfigAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Common configuration" + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String Detail() {
        System.out.println("called CommonConfigAction: Detail");
        ParameterUserCommon muparam = null;
        try {
            if (inputBean.getParamCode() != null && !inputBean.getParamCode().isEmpty()) {

                CommonConfigDAO dao = new CommonConfigDAO();
                //    CommonDAO commonDAO = new CommonDAO();

                //    inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                //    inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
                muparam = dao.findCommonConfigById(inputBean.getParamCode());

                inputBean.setParamCode(muparam.getParamcode());
                inputBean.setDescription(muparam.getDescription());
                inputBean.setParamVal(muparam.getParamvalue());

                inputBean.setOldvalue(inputBean.getParamCode() + "|"
                        + inputBean.getDescription() + "|"
                        + inputBean.getParamVal());

            } else {
                inputBean.setMessage("Empty parameter code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Common configuration" + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CommonConfigAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String Update() {

        System.out.println("called CommonConfigAction : update");
        String retType = "message";

        try {
            if (inputBean.getParamCode() != null && !inputBean.getParamCode().isEmpty()) {
                CommonConfigDAO dao = new CommonConfigDAO();
                //set username get in hidden fileds
                inputBean.setParamCode(inputBean.getParamCode());

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getParamCode() + "|"
                            + inputBean.getDescription() + "|"
                            + inputBean.getParamVal();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) {

                        Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.COMMON_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update common configuration (parameter code: " + inputBean.getParamCode() + ") ", null, oldVal, newv);
                        message = dao.updateCommonConfig(inputBean, audit);

                        if (message.isEmpty()) {
                            addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " common configuration ");
                        } else {
                            addActionError(message);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CommonConfigAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Common configuration " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getParamCode() == null || inputBean.getParamCode().trim().isEmpty()) {
            message = MessageVarList.COMMON_CONFIG_EMPTY_PARAM_CODE;
        } else if (inputBean.getDescription() == null || inputBean.getDescription().trim().isEmpty()) {
            message = MessageVarList.COMMON_CONFIG_EMPTY_DESCRIPTION;
        } else if (inputBean.getParamVal() != null && inputBean.getParamVal().isEmpty()) {
            message = MessageVarList.COMMON_CONFIG_EMPTY_PARAM_VAL;
        } else if (!Validation.isSpecailCharacter(inputBean.getParamCode())) {
            message = MessageVarList.COMMON_CONFIG_INVALID_PARAM_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getDescription())) {
            message = MessageVarList.COMMON_CONFIG_INVALID_DESCRIPTION;
        }

        return message;
    }

    public String Confirm() {
        System.out.println("called CommonConfigAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            CommonConfigDAO dao = new CommonConfigDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.COMMON_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", null);
            message = dao.confirmCommonConfig(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation approved successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(CommonConfigAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Common configuration " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called CommonConfigAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            CommonConfigDAO dao = new CommonConfigDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.COMMON_CONFIG_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", null);
            message = dao.rejectCommonConfig(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation rejected successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(CommonConfigAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Common configuration " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    public String Find() {
        System.out.println("called CommonConfigAction: Find");
        ParameterUserCommon puc = null;
        try {
            if (inputBean.getParamCode() != null && !inputBean.getParamCode().isEmpty()) {

                CommonConfigDAO dao = new CommonConfigDAO();

                puc = dao.findCommonConfigById(inputBean.getParamCode());

                inputBean.setParamCode(puc.getParamcode());
                inputBean.setDescription(puc.getDescription());
                inputBean.setParamVal(puc.getParamvalue());

            } else {
                inputBean.setMessage("Empty common configuration.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Common configuration " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(ParameterUserCommon.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

}
