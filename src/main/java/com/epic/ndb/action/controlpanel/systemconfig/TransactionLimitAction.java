/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.TransactionLimitBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TransactionLimitInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TransactionLimitPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.TransactionLimitDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.TransactionLimit;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class TransactionLimitAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    TransactionLimitInputBean inputBean = new TransactionLimitInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.TRANSACTION_LIMIT_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupcsv".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("pendCsvDownloade".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("template".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("upload".equals(method)) {
            task = TaskVarList.UPLOAD_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.TRANSACTION_LIMIT_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVupload(true);
        inputBean.setVdual(true);
        inputBean.setVgenerate(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPLOAD_TASK)) {
                    inputBean.setVupload(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setTransferTypeList(dao.getTransferTypeList());
            inputBean.setSegmentTypeList(dao.getSegmentTypeList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called TransactionLimitAction : view");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit ");
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called TransactionLimitAction : List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            TransactionLimitDAO dao = new TransactionLimitDAO();
            List<TransactionLimitBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "Transaction limit search using ["
                        + checkEmptyorNullString("Transfer Type", inputBean.getTransferTypeSearch())
                        + checkEmptyorNullString("Segment Type", inputBean.getSegmentTypeSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + checkEmptyorNullString("Default Limit", inputBean.getDefaultLimitSearch())
                        + checkEmptyorNullString("Tran limit min", inputBean.getTranLimitMinSearch())
                        + checkEmptyorNullString("Tran limit max", inputBean.getTranLimitMaxSearch())
                        + checkEmptyorNullString("Daily limit min", inputBean.getDailyLimitMinSearch())
                        + checkEmptyorNullString("Daily limit max", inputBean.getDailyLimitMaxSearch())
                        + "] parameters ";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.TRANSACTION_LIMIT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, searchParameters, null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);

                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.TRANSACTION_LIMIT_SEARCH_BEAN, inputBean);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit ");
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called TransactionLimitAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            TransactionLimitDAO dao = new TransactionLimitDAO();
            List<TransactionLimitPendBean> dataList = dao.getPendingTransactionLimitList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.TRANSACTION_LIMIT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit ");
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called TransactionLimitAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setTransferTypeList(dao.getTransferTypeActiveList());
            inputBean.setSegmentTypeList(dao.getSegmentTypeActiveList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit ");
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String add() {

        System.out.println("called TransactionLimitAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            TransactionLimitDAO dao = new TransactionLimitDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getTransferType().trim() + "|" + inputBean.getSegmentType().trim() + "|" + inputBean.getStatus().trim()
                        + "|" + inputBean.getDefaultLimit().trim() + "|" + inputBean.getTranLimitMin().trim() + "|" + inputBean.getTranLimitMax().trim() + "|" + inputBean.getDailyLimitMin().trim() + "|" + inputBean.getDailyLimitMax().trim();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.TRANSACTION_LIMIT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Requested to add transaction limit (transfer type : " + inputBean.getTransferType() + ", segment type: " + inputBean.getSegmentType() + ")", null, null, newv);

                message = dao.insertTransactionLimit(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + "transaction limit ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit");
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called TransactionLimitAction: detail");
        TransactionLimit transactionLimit = null;
        try {
            if (inputBean.getTransferType() != null && !inputBean.getTransferType().isEmpty()) {
                if (inputBean.getSegmentType() != null && !inputBean.getSegmentType().isEmpty()) {

                    TransactionLimitDAO dao = new TransactionLimitDAO();
                    CommonDAO commonDAO = new CommonDAO();

                    inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                    inputBean.setTransferTypeList(commonDAO.getTransferTypeList());
                    inputBean.setSegmentTypeList(commonDAO.getSegmentTypeList());

                    transactionLimit = dao.findTransactionLimitById(inputBean.getTransferType(), inputBean.getSegmentType());

                    inputBean.setStatus(transactionLimit.getStatus().getStatuscode());
                    inputBean.setDefaultLimit(transactionLimit.getDefaultLimit().toString());
                    inputBean.setTranLimitMin(transactionLimit.getTranLimitMin().toString());
                    inputBean.setTranLimitMax(transactionLimit.getTranLimitMax().toString());
                    inputBean.setDailyLimitMin(transactionLimit.getDailyLimitMin().toString());
                    inputBean.setDailyLimitMax(transactionLimit.getDailyLimitMax().toString());

                    inputBean.setOldvalue(inputBean.getTransferType() + "|"
                            + inputBean.getSegmentType() + "|"
                            + transactionLimit.getStatus().getStatuscode() + "|"
                            + transactionLimit.getDefaultLimit().toString() + "|"
                            + transactionLimit.getTranLimitMin().toString() + "|"
                            + transactionLimit.getTranLimitMax().toString() + "|"
                            + transactionLimit.getDailyLimitMin().toString() + "|"
                            + transactionLimit.getDailyLimitMax().toString());

                } else {
                    addActionError("Empty segment type.");
                }
            } else {
                addActionError("Empty transfer type.");
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit ");
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String find() {
        System.out.println("called TransactionLimitAction: find");
        TransactionLimit transactionLimit = null;
        try {
            if (inputBean.getTransferType() != null && !inputBean.getTransferType().isEmpty()) {
                if (inputBean.getSegmentType() != null && !inputBean.getSegmentType().isEmpty()) {

                    TransactionLimitDAO dao = new TransactionLimitDAO();

                    transactionLimit = dao.findTransactionLimitById(inputBean.getTransferType(), inputBean.getSegmentType());

                    inputBean.setStatus(transactionLimit.getStatus().getStatuscode());
                    inputBean.setDefaultLimit(transactionLimit.getDefaultLimit().toString());
                    inputBean.setTranLimitMin(transactionLimit.getTranLimitMin().toString());
                    inputBean.setTranLimitMax(transactionLimit.getTranLimitMax().toString());
                    inputBean.setDailyLimitMin(transactionLimit.getDailyLimitMin().toString());
                    inputBean.setDailyLimitMax(transactionLimit.getDailyLimitMax().toString());
                } else {
                    addActionError("Empty segment type.");
                }
            } else {
                addActionError("Empty transfer type.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit ");
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String update() {

        System.out.println("called TransactionLimitAction : update");
        String retType = "message";

        try {
            TransactionLimitDAO dao = new TransactionLimitDAO();

            if (inputBean.getTransferTypeHidden() != null && !inputBean.getTransferTypeHidden().trim().isEmpty()) {
                inputBean.setTransferType(inputBean.getTransferTypeHidden());
            }
            if (inputBean.getSegmentTypeHidden() != null && !inputBean.getSegmentTypeHidden().trim().isEmpty()) {
                inputBean.setSegmentType(inputBean.getSegmentTypeHidden());
            }
            String message = this.validateUpdates();

            if (message.isEmpty()) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String newv = inputBean.getTransferType().trim() + "|" + inputBean.getSegmentType().trim() + "|" + inputBean.getStatus().trim()
                        + "|" + inputBean.getDefaultLimit().trim() + "|" + inputBean.getTranLimitMin().trim() + "|" + inputBean.getTranLimitMax().trim() + "|" + inputBean.getDailyLimitMin().trim() + "|" + inputBean.getDailyLimitMax().trim();

                String oldVal = inputBean.getOldvalue();

                System.out.println("newV   :" + newv);
                System.out.println("oldVal :" + oldVal);

                if (!newv.equals(oldVal)) {
                    String newValWithActState = inputBean.getTransferType().trim() + "|" + inputBean.getSegmentType().trim() + "|" + CommonVarList.STATUS_ACTIVE
                        + "|" + inputBean.getDefaultLimit().trim() + "|" + inputBean.getTranLimitMin().trim() + "|" + inputBean.getTranLimitMax().trim() + "|" + inputBean.getDailyLimitMin().trim() + "|" + inputBean.getDailyLimitMax().trim();
                    if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                        Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.TRANSACTION_LIMIT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update transaction limit (transfer type : " + inputBean.getTransferType() + ", segment type: " + inputBean.getSegmentType() + ")", null, oldVal, newv);
                        message = dao.updateTransactionLimit(inputBean, audit);

                        if (message.isEmpty()) {
                            addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " transaction limit ");
                        } else {
                            addActionError(message);
                        }
                    }else{
                        addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                    }
                } else {
                    addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                }
            } else {
                addActionError(message);
            }
        } catch (Exception ex) {
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " transaction limit ");
        }
        return retType;
    }

    public String delete() {

        System.out.println("called TransactionLimitAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            TransactionLimitDAO dao = new TransactionLimitDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.TRANSACTION_LIMIT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete transaction limit (transfer type : " + inputBean.getTransferType() + ", segment type: " + inputBean.getSegmentType() + ")", null);
            message = dao.deleteTransactionLimit(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " transaction limit";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called TransactionLimitAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                TransactionLimitDAO dao = new TransactionLimitDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.TRANSACTION_LIMIT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmTransactionLimit(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " transaction limit ");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called TransactionLimitAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                TransactionLimitDAO dao = new TransactionLimitDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.TRANSACTION_LIMIT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectTransactionLimit(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " transaction limit");
        }
        return retType;
    }

    public String reportGenerate() {

        System.out.println("called TransactionLimitAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                TransactionLimitDAO dao = new TransactionLimitDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    TransactionLimitInputBean searchBean = (TransactionLimitInputBean) session.getAttribute(SessionVarlist.TRANSACTION_LIMIT_SEARCH_BEAN);
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new TransactionLimitInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Transaction_Limit_Report.csv");
                        setContentLength(sb.length());

                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.TRANSACTION_LIMIT_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Transaction Limit csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " detail csv report");
                    Logger
                            .getLogger(TransactionLimitAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(TransactionLimitAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit ");

            return "message";
        }
        return retMsg;
    }

    public String viewPend() {
        System.out.println("called TransactionLimitAction : viewPend");
        String retType = "viewPend";
        String message = "";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                TransactionLimitDAO dao = new TransactionLimitDAO();
                message = dao.getPendOldNewValueOfTransactionLimit(inputBean);
                if (message.isEmpty()) {
                } else {
                    addActionError(message);
                }
            } else {
                addActionError("Empty pending task ID.");
            }
        } catch (Exception e) {
            Logger.getLogger(TransactionLimitAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " transaction limit ");
        }
        return retType;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getTransferType() == null || inputBean.getTransferType().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_TRANSFER_TYPE;
        } else if (inputBean.getSegmentType() == null || inputBean.getSegmentType().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_SEGMENT_TYPE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_STATUS;
        } else if (inputBean.getDefaultLimit() == null || inputBean.getDefaultLimit().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_DEFAULT_LIMIT;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getDefaultLimit())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_DEFAULT_LIMIT;
        } else if (inputBean.getTranLimitMin() == null || inputBean.getTranLimitMin().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_TRAN_LIMIT_MIN;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getTranLimitMin())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN;
        } else if (inputBean.getTranLimitMax() == null || inputBean.getTranLimitMax().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_TRAN_LIMIT_MAX;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getTranLimitMax())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MAX;
        } else if (inputBean.getDailyLimitMin() == null || inputBean.getDailyLimitMin().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_DAILY_LIMIT_MIN;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getDailyLimitMin())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MIN;
        } else if (inputBean.getDailyLimitMax() == null || inputBean.getDailyLimitMax().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_DAILY_LIMIT_MAX;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getDailyLimitMax())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MAX;
        } else {
            BigDecimal defLimit = new BigDecimal(inputBean.getDefaultLimit());
            BigDecimal tranlimitMin = new BigDecimal(inputBean.getTranLimitMin());
            BigDecimal tranlimitMax = new BigDecimal(inputBean.getTranLimitMax());
            BigDecimal dailylimitMin = new BigDecimal(inputBean.getDailyLimitMin());
            BigDecimal dailylimitMax = new BigDecimal(inputBean.getDailyLimitMax());

            if (tranlimitMin.compareTo(tranlimitMax) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN_LESS_MAX;
            } else if (dailylimitMin.compareTo(dailylimitMax) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MIN_LESS_MAX;
            } else if (defLimit.compareTo(dailylimitMax) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_DEF_LIMIT_LESS_DAILY_LIMIT_MAX;
            } else if (dailylimitMin.compareTo(defLimit) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_DEF_LIMIT_GERTER_DAILY_LIMIT_MIN;
            } else if (dailylimitMin.compareTo(tranlimitMin) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN_GERTER_DAILY_LIMIT_MIN;
            } else if (tranlimitMax.compareTo(dailylimitMax) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MAX_LESS_DAILY_LIMIT_MAX;
            }
        }

        return message;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getTransferType() == null || inputBean.getTransferType().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_TRANSFER_TYPE;
        } else if (inputBean.getSegmentType() == null || inputBean.getSegmentType().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_SEGMENT_TYPE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_STATUS;
        } else if (inputBean.getDefaultLimit() == null || inputBean.getDefaultLimit().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_DEFAULT_LIMIT;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getDefaultLimit())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_DEFAULT_LIMIT;
        } else if (inputBean.getTranLimitMin() == null || inputBean.getTranLimitMin().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_TRAN_LIMIT_MIN;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getTranLimitMin())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN;
        } else if (inputBean.getTranLimitMax() == null || inputBean.getTranLimitMax().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_TRAN_LIMIT_MAX;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getTranLimitMax())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MAX;
        } else if (inputBean.getDailyLimitMin() == null || inputBean.getDailyLimitMin().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_DAILY_LIMIT_MIN;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getDailyLimitMin())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MIN;
        } else if (inputBean.getDailyLimitMax() == null || inputBean.getDailyLimitMax().trim().isEmpty()) {
            message = MessageVarList.TRANSACTION_LIMIT_EMPTY_DAILY_LIMIT_MAX;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getDailyLimitMax())) {
            message = MessageVarList.TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MAX;
        } else {
            BigDecimal defLimit = new BigDecimal(inputBean.getDefaultLimit());
            BigDecimal tranlimitMin = new BigDecimal(inputBean.getTranLimitMin());
            BigDecimal tranlimitMax = new BigDecimal(inputBean.getTranLimitMax());
            BigDecimal dailylimitMin = new BigDecimal(inputBean.getDailyLimitMin());
            BigDecimal dailylimitMax = new BigDecimal(inputBean.getDailyLimitMax());

            if (tranlimitMin.compareTo(tranlimitMax) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN_LESS_MAX;
            } else if (dailylimitMin.compareTo(dailylimitMax) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_DAILY_LIMIT_MIN_LESS_MAX;
            } else if (defLimit.compareTo(dailylimitMax) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_DEF_LIMIT_LESS_DAILY_LIMIT_MAX;
            } else if (dailylimitMin.compareTo(defLimit) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_DEF_LIMIT_GERTER_DAILY_LIMIT_MIN;
            } else if (dailylimitMin.compareTo(tranlimitMin) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MIN_GERTER_DAILY_LIMIT_MIN;
            } else if (tranlimitMax.compareTo(dailylimitMax) > 0) {
                message = MessageVarList.TRANSACTION_LIMIT_INVALID_TRAN_LIMIT_MAX_LESS_DAILY_LIMIT_MAX;
            }
        }

        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }
}
