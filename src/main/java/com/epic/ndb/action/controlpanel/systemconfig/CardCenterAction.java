/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.CardCenterBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CardCenterInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CardCenterPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.CardCenterDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.OtherBank;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class CardCenterAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    CardCenterInputBean inputBean = new CardCenterInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.CARD_CENTER_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.CARD_CENTER_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVgenerate(true);
        inputBean.setVgenerateview(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                    inputBean.setVgenerateview(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
            inputBean.setFundTranModeMap(this.getFundTransferModeMap());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called CardCenterAction : view");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other bank ");
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called CardCenterAction : List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            CardCenterDAO dao = new CardCenterDAO();
            List<CardCenterBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "Other bank search using ["
                        + checkEmptyorNullString("Bank code", inputBean.getBankcodeSearch())
                        + checkEmptyorNullString("Bank name", inputBean.getBanknameSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + checkEmptyorNullString("Fund Tran Mode", inputBean.getFundTranModeSearch())
                        + checkEmptyorNullString("Card code center", inputBean.getCardCodeCenterSearch())
                        + "] parameters ";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.CARD_CENTER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, searchParameters, null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.CARD_CENTER_SEARCH_BEAN, inputBean);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other bank ");
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called CardCenterAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            CardCenterDAO dao = new CardCenterDAO();
            List<CardCenterPendBean> dataList = dao.getPendingCardCenterList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.CARD_CENTER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other bank ");
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called CardCenterAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
            inputBean.setFundTranModeMap(this.getFundTransferModeMap());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other bank ");
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String add() {

        System.out.println("called CardCenterAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            CardCenterDAO dao = new CardCenterDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getBankcode().trim() + "|" + inputBean.getBankname().trim() + "|" + inputBean.getStatus().trim() + "|" + inputBean.getFundTranMode().trim() + "|" + inputBean.getShortCode().trim() + "|" + inputBean.getCardCodeCenter().trim();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.CARD_CENTER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to add other bank ( bank code : " + inputBean.getBankcode() + " )", null, null, newv);

                message = dao.insertCardCenter(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + "other bank ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other bank");
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called CardCenterAction: Detail");
        OtherBank cardCenter = null;
        try {
            if (inputBean.getBankcode() != null && !inputBean.getBankcode().isEmpty()) {

                CardCenterDAO dao = new CardCenterDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);
                inputBean.setFundTranModeMap(this.getFundTransferModeMap());

                cardCenter = dao.findCardCenterById(inputBean.getBankcode());

                inputBean.setBankname(cardCenter.getBankname());
                inputBean.setStatus(cardCenter.getStatus().getStatuscode());
                inputBean.setFundTranMode(cardCenter.getFundTranMode());
                inputBean.setShortCode(cardCenter.getShortCode());
                inputBean.setCardCodeCenter(cardCenter.getCardCodeCenter());

                String cardCenterCode = "";

                if (cardCenter.getCardCodeCenter() != null && !cardCenter.getCardCodeCenter().isEmpty()) {
                    cardCenterCode = cardCenter.getCardCodeCenter();
                }

                inputBean.setOldvalue(cardCenter.getBankcode() + "|"
                        + cardCenter.getBankname() + "|"
                        + cardCenter.getStatus().getStatuscode() + "|"
                        + cardCenter.getFundTranMode() + "|"
                        + cardCenter.getShortCode() + "|"
                        + cardCenterCode);
            } else {
                addActionError("Empty bank code.");
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other bank ");
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String find() {
        System.out.println("called CardCenterAction: find");
        OtherBank cardCenter = null;
        try {
            if (inputBean.getBankcode() != null && !inputBean.getBankcode().isEmpty()) {

                CardCenterDAO dao = new CardCenterDAO();

                cardCenter = dao.findCardCenterById(inputBean.getBankcode());

                inputBean.setBankname(cardCenter.getBankname());
                inputBean.setStatus(cardCenter.getStatus().getStatuscode());
                inputBean.setFundTranMode(cardCenter.getFundTranMode());
                inputBean.setShortCode(cardCenter.getShortCode());
                inputBean.setCardCodeCenter(cardCenter.getCardCodeCenter());

            } else {
                inputBean.setMessage("Empty bank code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " other bank ");
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String update() {

        System.out.println("called CardCenterAction : update");
        String retType = "message";

        try {
            if (inputBean.getBankcode() != null && !inputBean.getBankcode().isEmpty()) {
                CardCenterDAO dao = new CardCenterDAO();

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String cardCenterCode = "";

                    if (inputBean.getCardCodeCenter() != null && !inputBean.getCardCodeCenter().isEmpty()) {
                        cardCenterCode = inputBean.getCardCodeCenter();
                    }

                    String newv = inputBean.getBankcode().trim() + "|"
                            + inputBean.getBankname().trim() + "|"
                            + inputBean.getStatus().trim() + "|"
                            + inputBean.getFundTranMode().trim() + "|"
                            + inputBean.getShortCode().trim() + "|"
                            + cardCenterCode;

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newv);
                    System.out.println("oldVal :" + oldVal);

                    if (!newv.equals(oldVal)) { 
                        String newValWithActState = inputBean.getBankcode().trim() + "|"
                            + inputBean.getBankname().trim() + "|"
                            + CommonVarList.STATUS_ACTIVE + "|"
                            + inputBean.getFundTranMode().trim() + "|"
                            + inputBean.getShortCode().trim() + "|"
                            + cardCenterCode;
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.CARD_CENTER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update other bank ( bank code : " + inputBean.getBankcode() + " )", null, oldVal, newv);
                            message = dao.updateCardCenter(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " other bank ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " other bank");
        }
        return retType;
    }

    public String delete() {

        System.out.println("called CardCenterAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            CardCenterDAO dao = new CardCenterDAO();

            message = this.validateDelete();

            if (message.isEmpty()) {

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.CARD_CENTER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete other bank ( bank code : " + inputBean.getBankcode() + " )", null);
                message = dao.deleteCardCenter(inputBean, audit);

                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + "other bank ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called CardCenterAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                CardCenterDAO dao = new CardCenterDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.CARD_CENTER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmCardCenter(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " other bank");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called CardCenterAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                CardCenterDAO dao = new CardCenterDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.CARD_CENTER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectCardCenter(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(CardCenterAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " other bank");
        }
        return retType;
    }

    public String reportGenerate() {

        System.out.println("called CardCenterAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                CardCenterDAO dao = new CardCenterDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    CardCenterInputBean searchBean = (CardCenterInputBean) session.getAttribute(SessionVarlist.CARD_CENTER_SEARCH_BEAN);
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new CardCenterInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Other_Bank_Report.csv");
                        setContentLength(sb.length());

                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.CARD_CENTER_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Other bank csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " detail csv report");
                    Logger
                            .getLogger(CardCenterAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CardCenterAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other bank");

            return "message";
        }
        return retMsg;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getBankcode() == null || inputBean.getBankcode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBankcode())) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_CODE;
        } else if (inputBean.getBankname() == null || inputBean.getBankname().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getBankname())) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_NAME;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_STATUS;
        } else if (inputBean.getFundTranMode() == null || inputBean.getFundTranMode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_FUND_TRAN_MODE;
        } else if (inputBean.getShortCode() == null || inputBean.getShortCode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_SHORT_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getShortCode())) {
            message = MessageVarList.CARD_CENTER_INVALID_SHORT_CODE;
//        } else if (inputBean.getCardCodeCenter()== null || inputBean.getCardCodeCenter().trim().isEmpty()) {
//            message = MessageVarList.CARD_CENTER_EMPTY_CARD_CODE_CENTER;
        } else if (inputBean.getCardCodeCenter() != null && !inputBean.getCardCodeCenter().trim().isEmpty() && !Validation.isSpecailCharacter(inputBean.getCardCodeCenter())) {
            message = MessageVarList.CARD_CENTER_INVALID_CARD_CODE_CENTER;
        }

        return message;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getBankcode() == null || inputBean.getBankcode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBankcode())) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_CODE;
        } else if (inputBean.getBankname() == null || inputBean.getBankname().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getBankname())) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_NAME;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_STATUS;
        } else if (inputBean.getFundTranMode() == null || inputBean.getFundTranMode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_FUND_TRAN_MODE;
        } else if (inputBean.getShortCode() == null || inputBean.getShortCode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_SHORT_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getShortCode())) {
            message = MessageVarList.CARD_CENTER_INVALID_SHORT_CODE;
//        } else if (inputBean.getCardCodeCenter()== null || inputBean.getCardCodeCenter().trim().isEmpty()) {
//            message = MessageVarList.CARD_CENTER_EMPTY_CARD_CODE_CENTER;
        } else if (inputBean.getCardCodeCenter() != null && !inputBean.getCardCodeCenter().trim().isEmpty() && !Validation.isSpecailCharacter(inputBean.getCardCodeCenter())) {
            message = MessageVarList.CARD_CENTER_INVALID_CARD_CODE_CENTER;
        }

        return message;
    }

    private String validateDelete() throws Exception {
        CardCenterDAO dao = new CardCenterDAO();
        String message = "";
        if (inputBean.getBankcode() == null || inputBean.getBankcode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_CODE;
        } else if (dao.isRecordUse(inputBean.getBankcode())) {
            message = MessageVarList.CARD_CENTER_BANK_CODE_USE;
        }

        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private HashMap<String, String> getFundTransferModeMap() {

        HashMap<String, String> fundTransferModeMap = new HashMap<String, String>();

        fundTransferModeMap.put("CEFT", "CEFT");
        fundTransferModeMap.put("SLIPS", "SLIPS");
        fundTransferModeMap.put("BOTH", "BOTH");

        return fundTransferModeMap;

    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }
}
