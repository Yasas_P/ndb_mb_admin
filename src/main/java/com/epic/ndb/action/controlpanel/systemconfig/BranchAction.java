package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.BranchBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BranchInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BranchPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.BranchDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Branch;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author jayathissa_d
 */
public class BranchAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    BranchInputBean inputBean = new BranchInputBean();

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called BranchAction : execute");
        return SUCCESS;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.BRANCH_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("Delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("ViewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.BRANCH_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called BranchAction :view");

        } catch (Exception ex) {
            addActionError("Branch " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called BranchAction: List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            BranchDAO dao = new BranchDAO();
            List<BranchBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("Branch code", inputBean.getBranchCodeSearch())
                        + checkEmptyorNullString("Branch name", inputBean.getBranchNameSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + checkEmptyorNullString("Manager name", inputBean.getManagernameSearch())
                        + checkEmptyorNullString("Mobile", inputBean.getMobileSearch())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Branch management search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" Branch " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String ApproveList() {
        System.out.println("called BranchAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            BranchDAO dao = new BranchDAO();
            List<BranchPendBean> dataList = dao.getPendingBranchList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Branch" + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String ViewPopup() {
        String result = "viewpopup";
        System.out.println("called BranchAction : ViewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called BranchAction :ViewPopup");

        } catch (Exception ex) {
            addActionError("Branch " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String Add() {

        System.out.println("called BranchAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            BranchDAO dao = new BranchDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getBranchCode() + "|"
                        + inputBean.getBranchName() + "|"
                        + inputBean.getManagername() + "|"
                        + inputBean.getMobile() + "|"
                        + inputBean.getStatus();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Branch " + inputBean.getBranchCode() + " added", null, null, newv);

                message = dao.insertBranch(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + "branch ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Branch " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getBranchCode() == null || inputBean.getBranchCode().trim().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_BRANCH_CODE;
        } else if (inputBean.getBranchName() == null || inputBean.getBranchName().trim().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_BRANCH_NAME;
        } else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_STATUS;
        } else if (inputBean.getManagername() == null || inputBean.getManagername().trim().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_MANAGER_NAME;
        } else if (inputBean.getMobile() == null || inputBean.getMobile().trim().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_MOBILE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchCode())) {
            message = MessageVarList.BRANCH_MGT_INVALID_BRANCH_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchName())) {
            message = MessageVarList.BRANCH_MGT_INVALID_BRANCH_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getManagername())) {
            message = MessageVarList.BRANCH_MGT_INVALID_MANAGER_NAME;
        } else if ((inputBean.getMobile() != null && !inputBean.getMobile().trim().isEmpty()) && !Validation.isNumeric(inputBean.getMobile().substring(1).trim())) {
            message = MessageVarList.BRANCH_MGT_INVALID_MOBILE;
        } else if (!Validation.validAnyPhoneno(inputBean.getMobile().trim())) {
            message = MessageVarList.BRANCH_MGT_INVALID_MOBILE;
        }

        return message;
    }

    public String Delete() {

        System.out.println("called BranchAction : Delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            BranchDAO dao = new BranchDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Branch " + inputBean.getBranchCode() + " deleted", null);
            message = dao.deleteBranch(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + "branch ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String Detail() {
        System.out.println("called BranchAction: Detail");
        Branch txntype = null;
        try {
            if (inputBean.getBranchCode() != null && !inputBean.getBranchCode().isEmpty()) {

                BranchDAO dao = new BranchDAO();
                CommonDAO commonDAO = new CommonDAO();

                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                txntype = dao.findBranchById(inputBean.getBranchCode());

                inputBean.setBranchCode(txntype.getBranchcode());
                inputBean.setBranchName(txntype.getBranchname());
                inputBean.setStatus(txntype.getStatus().getStatuscode());
                inputBean.setManagername(txntype.getManagername());
                inputBean.setMobile(txntype.getMobile());

            } else {
                inputBean.setMessage("Empty branch code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Branch code" + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String Update() {

        System.out.println("called BranchAction : update");
        String retType = "message";

        try {
            if (inputBean.getBranchCode() != null && !inputBean.getBranchCode().isEmpty()) {
                BranchDAO dao = new BranchDAO();
                //set username get in hidden fileds
                inputBean.setBranchCode(inputBean.getBranchCode());

                String message = this.validateUpdates();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String newv = inputBean.getBranchCode() + "|"
                            + inputBean.getBranchName() + "|"
                            + inputBean.getStatus() + "|"
                            + inputBean.getManagername() + "|"
                            + inputBean.getMobile();

                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Branch " + inputBean.getBranchCode() + " updated", null, null, newv);
                    message = dao.updateBranch(inputBean, audit);

                    if (message.isEmpty()) {
                        addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + "branch ");
                    } else {
                        addActionError(message);
                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("Branch " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getBranchCode() == null || inputBean.getBranchCode().trim().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_BRANCH_CODE;
        } else if (inputBean.getBranchName() == null || inputBean.getBranchName().trim().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_BRANCH_NAME;
        } else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_STATUS;
        } else if (inputBean.getManagername() == null || inputBean.getManagername().trim().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_MANAGER_NAME;
        } else if (inputBean.getMobile() == null || inputBean.getMobile().trim().isEmpty()) {
            message = MessageVarList.BRANCH_MGT_EMPTY_MOBILE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchCode())) {
            message = MessageVarList.BRANCH_MGT_INVALID_BRANCH_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchName())) {
            message = MessageVarList.BRANCH_MGT_INVALID_BRANCH_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getManagername())) {
            message = MessageVarList.BRANCH_MGT_INVALID_MANAGER_NAME;
        } else if (!Validation.isNumeric(inputBean.getMobile().substring(1).trim())) {
            message = MessageVarList.BRANCH_MGT_INVALID_MOBILE;
        } else if (!Validation.validAnyPhoneno(inputBean.getMobile().trim())) {
            message = MessageVarList.BRANCH_MGT_INVALID_MOBILE;
        }

        return message;
    }

    public String Confirm() {
        System.out.println("called BranchAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            BranchDAO dao = new BranchDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", null);
            message = dao.confirmBranch(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation approved successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Branch " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called BranchAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            BranchDAO dao = new BranchDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", null);
            message = dao.rejectBranch(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation rejected successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" Branch " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    public String Find() {
        System.out.println("called BranchAction: Find");
        Branch txntype = null;
        try {
            if (inputBean.getBranchCode() != null && !inputBean.getBranchCode().isEmpty()) {

                BranchDAO dao = new BranchDAO();

                txntype = dao.findBranchById(inputBean.getBranchCode());

                inputBean.setBranchCode(txntype.getBranchcode());
                inputBean.setBranchName(txntype.getBranchname());
                inputBean.setStatus(txntype.getStatus().getStatuscode());
                inputBean.setManagername(txntype.getManagername());
                inputBean.setMobile(txntype.getMobile());

            } else {
                inputBean.setMessage("Empty branch code.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("Branch " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(BranchAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

}
