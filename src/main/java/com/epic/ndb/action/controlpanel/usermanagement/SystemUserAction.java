/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.systemconfig.BranchListBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SystemUserDataBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SystemUserInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SystemUserPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.PasswordPolicyDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.dao.controlpanel.usermanagement.SystemUserDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Branch;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.mapping.Passwordpolicy;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author asitha_l
 */
public class SystemUserAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    SystemUserInputBean inputBean = new SystemUserInputBean();

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setUserroleList(dao.getALLUserList(CommonVarList.SYS_MERCHANT));

            //set branch list
            SystemUserDAO sudao = new SystemUserDAO();
            List<BranchListBean> branchlist = sudao.getBranchBeanList();
            inputBean.setBranchList(branchlist);

            // set password expiry period (inserted)
            PasswordPolicyDAO passPolicydao = new PasswordPolicyDAO();
            Calendar cal = Calendar.getInstance();
            Passwordpolicy passPolicy = passPolicydao.getPasswordPolicyDetails();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            cal.setTime(CommonDAO.getSystemDateLogin());
            cal.add(Calendar.DAY_OF_MONTH, passPolicy.getPasswordexpiryperiod());

            // set expiry date to session
            HttpServletRequest request = ServletActionContext.getRequest();
            request.getSession(false).setAttribute(SessionVarlist.PAS_EXPIRY_PERIOD, formatter.format(cal.getTime()));

            inputBean.setExpirydate(formatter.format(cal.getTime()));

            inputBean.setPwtooltip(passPolicydao.generateToolTipMessage(passPolicy));

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

            System.out.println("called SystemUserAction :view");

        } catch (Exception ex) {
            addActionError("System user " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String changepassword() {
        System.out.println("called SystemUserAction :changePassword");
        String retType = "changepassword";
        try {
            SystemUserDAO dao = new SystemUserDAO();
            String[] parts = inputBean.getUsernameUserrole().split("\\|");

            inputBean.setUsername(parts[0]);
            inputBean.setHusername(parts[0]);
            inputBean.setUserrole(parts[1]);

            PasswordPolicyDAO passPolicydao = new PasswordPolicyDAO();
            Passwordpolicy passPolicy = passPolicydao.getPasswordPolicyDetails();
            inputBean.setPwtooltip(passPolicydao.generateToolTipMessage(passPolicy));

        } catch (Exception ex) {
            addActionError(" Passsword Reset " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retType;
    }

    public String updatechangepassword() {
        System.out.println("called SystemUserAction :updatechangepassword");
        String retType = "message";

        System.err.println(inputBean.getCnewpwd());
        System.err.println(inputBean.getCrenewpwd());
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            String message = this.validateChangePasswordInputs();

            if (message.isEmpty()) {
                SystemUserDAO dao = new SystemUserDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, "Password of " + inputBean.getHusername() + " reset ", null);
                inputBean.setCrenewpwd(Common.makeHash(inputBean.getCrenewpwd().trim()));
                message = dao.updatePasswordReset(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.RESET_PAS_SUCCESS);
                } else {
                    addActionError(message);
                }

            } else {
                addActionError(message);
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " Password Reset");
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retType;
    }

    public String list() {
        System.out.println("called SystemUserAction: list");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;

            String sortIndex = "";
            String sortOrder = "";
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                sortIndex = inputBean.getSidx();
                sortOrder = inputBean.getSord();
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            SystemUserDAO dao = new SystemUserDAO();
//            List<SystemUserDataBean> dataList = dao.getSearchList(inputBean, rows, from, sortIndex, sortOrder);
            HttpServletRequest request = ServletActionContext.getRequest();
            List<SystemUserDataBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy, request);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                String searchParameters = "["
                        + checkEmptyorNullString("Username", inputBean.getUsername())
                        + checkEmptyorNullString("Full Name", inputBean.getFullname())
                        + checkEmptyorNullString("Service ID", inputBean.getServiceid())
                        + checkEmptyorNullString("NIC", inputBean.getNic())
                        + checkEmptyorNullString("Email", inputBean.getEmail())
                        + checkEmptyorNullString("Contact Number", inputBean.getContactno())
                        + checkEmptyorNullString("User Role", inputBean.getUserrole())
                        + checkEmptyorNullString("Status", inputBean.getStatus())
                        + checkEmptyorNullString("Branch", inputBean.getBranch())
                        + "]";

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, "User management search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {

                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);

            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }

        } catch (Exception e) {
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE,
                    null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS
                    + " System User Action");
        }
        return "list";
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.SYSTEM_USER, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVpasswordreset(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
//                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.LOGIN_TASK)) {
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.PAS_RESET_TASK)) {
                    inputBean.setVpasswordreset(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);

    }

    public String add() {
        System.out.println("called SystemUserAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SystemUserDAO dao = new SystemUserDAO();

            String message = this.validateInputs();

            if (message.isEmpty()) {

                String serid_audit = "";
                String add1_audit = "";
                String city_audit = "";
                String email_audit = "";

                if (inputBean.getServiceid() == null || inputBean.getServiceid().isEmpty()) {
                    serid_audit = "";
                } else {
                    serid_audit = inputBean.getServiceid();
                }
                if (inputBean.getAddress1() == null || inputBean.getAddress1().isEmpty()) {
                    add1_audit = "";
                } else {
                    add1_audit = inputBean.getAddress1();
                }
                if (inputBean.getCity() == null || inputBean.getCity().isEmpty()) {
                    city_audit = "";
                } else {
                    city_audit = inputBean.getCity();
                }
                if (inputBean.getEmail() == null || inputBean.getEmail().isEmpty()) {
                    email_audit = "";
                } else {
                    email_audit = inputBean.getEmail();
                }

                String newV = inputBean.getUsername() + "|"
                        + inputBean.getFullname() + "|"
                        + inputBean.getUserrole() + "|"
                        + inputBean.getStatus() + "|"
                        + email_audit + "|"
                        + add1_audit + "|"
                        + city_audit + "|"
                        + serid_audit + "|"
                        + inputBean.getBranch();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, "Requested to add system user ( username : " + inputBean.getUsername() + " )", null, null, newV);
                message = dao.insertSystemUser(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " system user ");
                } else {
                    addActionError(message);
                }

            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("System user " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String update() {

        System.out.println("called SystemUserAction : update");
        String retType = "message";

        try {
            if (inputBean.getUsername() != null && !inputBean.getUsername().isEmpty()) {

                //set username get in hidden fileds
                inputBean.setUsername(inputBean.getUsername());
                SystemUserDAO dao = new SystemUserDAO();

                String message = this.validateUpdateInputs();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();
                    //SystemUserDAO dao = new SystemUserDAO();

                    String serid_audit = "";
                    String add1_audit = "";
                    String city_audit = "";
                    String email_audit = "";

                    if (inputBean.getServiceid() == null || inputBean.getServiceid().isEmpty()) {
                        serid_audit = "";
                    } else {
                        serid_audit = inputBean.getServiceid();
                    }
                    if (inputBean.getAddress1() == null || inputBean.getAddress1().isEmpty()) {
                        add1_audit = "";
                    } else {
                        add1_audit = inputBean.getAddress1();
                    }
                    if (inputBean.getCity() == null || inputBean.getCity().isEmpty()) {
                        city_audit = "";
                    } else {
                        city_audit = inputBean.getCity();
                    }
                    if (inputBean.getEmail() == null || inputBean.getEmail().isEmpty()) {
                        email_audit = "";
                    } else {
                        email_audit = inputBean.getEmail();
                    }

                    String newV = inputBean.getUsername() + "|"
                            + inputBean.getFullname() + "|"
                            + inputBean.getUserrole() + "|"
                            + inputBean.getStatus() + "|"
                            + email_audit + "|"
                            + add1_audit + "|"
                            + city_audit + "|"
                            + serid_audit + "|"
                            + inputBean.getBranch();

                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newV);
                    System.out.println("oldVal :" + oldVal);

                    if (!newV.equals(oldVal)) {
                        String newValWithActState = inputBean.getUsername() + "|"
                            + inputBean.getFullname() + "|"
                            + inputBean.getUserrole() + "|"
                            + CommonVarList.STATUS_ACTIVE + "|"
                            + email_audit + "|"
                            + add1_audit + "|"
                            + city_audit + "|"
                            + serid_audit + "|"
                            + inputBean.getBranch();
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){
                            
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, "Requested to update system user ( username : " + inputBean.getUsername() + " )", null, inputBean.getOldvalue(), newV);
                            message = dao.updateSystemUser(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " system user ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError("System user " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    public String delete() {
        System.out.println("called SystemUserAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            Systemuser cuUser = ((Systemuser) request.getSession(false).getAttribute(SessionVarlist.SYSTEMUSER));
            SystemUserDAO dao = new SystemUserDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, "Requested to delete system user ( username : " + inputBean.getUsername() + " )", null);
            message = dao.deleteSystemUser(inputBean, cuUser, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " system user ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
//            inputBean.setMessage(MessageVarList.COMMON_ERROR_DELETE);
        }
        return retType;
    }

    public String findDualAuthUsers() {
        System.out.println("called SystemUserAction : findDualAuthUsers");
        try {

            if (inputBean.getUserrole() != null && !inputBean.getUserrole().isEmpty()) {

                SystemUserDAO dao = new SystemUserDAO();

                int currUserLevel = dao.getCurrUsersUserLevel(inputBean.getUserrole());

                dao.findUsersByUserRole(inputBean, currUserLevel);

            } else {
                inputBean.setMessage("");
            }

        } catch (Exception ex) {
            inputBean.setMessage("System user " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "findDualAuthUsers";

    }

    public String find() {
        System.out.println("called UserManagementAction: find");
        Systemuser user = new Systemuser();
        try {
            if (inputBean.getUsername() != null && !inputBean.getUsername().isEmpty()) {

                SystemUserDAO dao = new SystemUserDAO();

                user = dao.getSystemUserByUserName(inputBean.getUsername());

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    inputBean.setUsername(user.getUsername());
                } catch (NullPointerException e) {
                    inputBean.setUsername("");
                }

//                try {
//                    inputBean.setExpirydate(sdf.format(user.getExpirydate()));
//                } catch (NullPointerException e) {
//                    inputBean.setExpirydate("");
//                }
                try {
                    inputBean.setUserrole(user.getUserrole().getUserrolecode());
                } catch (NullPointerException e) {
                    inputBean.setUserrole("");
                }

//                try {
////                    inputBean.setDualauthuser(user.getDualauthuserrole().getUsername());
//                    inputBean.setDualauthuser(user.getDualauthuserrole());
//                } catch (NullPointerException e) {
//                    inputBean.setDualauthuser("");
//                }
                try {
                    inputBean.setStatus(user.getStatus().getStatuscode());
                } catch (NullPointerException e) {
                    inputBean.setStatus("");
                }

                try {
//                    inputBean.setBranch(user.getBranch().getBranchcode());
                    inputBean.setBranch(user.getBranch());
                } catch (NullPointerException e) {
                    inputBean.setBranch("");
                }

                try {
                    inputBean.setFullname(user.getFullname());
                } catch (NullPointerException e) {
                    inputBean.setFullname("");
                }

                try {
                    inputBean.setServiceid(user.getEmpid());
                } catch (NullPointerException e) {
                    inputBean.setServiceid("");
                }

                try {
                    inputBean.setAddress1(user.getAddress());
                } catch (NullPointerException e) {
                    inputBean.setAddress1("");
                }

//                try {
//                    inputBean.setAddress2(user.getAddress2());
//                } catch (NullPointerException e) {
//                    inputBean.setAddress2("");
//                }
                try {
                    inputBean.setCity(user.getCity());
                } catch (NullPointerException e) {
                    inputBean.setCity("");
                }

                try {
                    inputBean.setContactno(user.getMobile());
                } catch (NullPointerException e) {
                    inputBean.setContactno("");
                }

                try {
                    inputBean.setEmail(user.getEmail());
                } catch (NullPointerException e) {
                    inputBean.setEmail("");
                }

                try {
                    inputBean.setNic(user.getNic());
                } catch (NullPointerException e) {
                    inputBean.setNic("");
                }

//                try {
//                    inputBean.setDateofbirth(sdf.format(user.getDateofbirth()));
//                } catch (NullPointerException e) {
//                    inputBean.setDateofbirth("");
//                }
                String serid_audit = "";
                String add1_audit = "";
//                String add2_audit = "";
                String city_audit = "";

                if (inputBean.getServiceid() == null || inputBean.getServiceid().isEmpty()) {
                    serid_audit = "";
                } else {
                    serid_audit = inputBean.getServiceid();
                }
                if (inputBean.getAddress1() == null || inputBean.getAddress1().isEmpty()) {
                    add1_audit = "";
                } else {
                    add1_audit = inputBean.getAddress1();
                }
//                if (inputBean.getAddress2() == null || inputBean.getAddress2().isEmpty()) {
//                    add2_audit = "";
//                } else {
//                    add2_audit = inputBean.getAddress2();
//                }
                if (inputBean.getCity() == null || inputBean.getCity().isEmpty()) {
                    city_audit = "";
                } else {
                    city_audit = inputBean.getCity();
                }

                inputBean.setOldvalue(inputBean.getUsername()
                        //                        + "|" + inputBean.getExpirydate() 
                        + "|" + inputBean.getUserrole()
                        + "|" + "admin|" + inputBean.getStatus() + "|" + inputBean.getFullname()
                        + "|" + inputBean.getContactno() + "|" + inputBean.getEmail() + "|" + inputBean.getNic()
                        //                        + "|" + inputBean.getDateofbirth() 
                        + "|" + serid_audit + "|" + add1_audit + "|" + city_audit + "|" + inputBean.getBranch());

            } else {
                inputBean.setMessage("Empty system Unique ID.");

                // set password expiry period (inserted)
                PasswordPolicyDAO passPolicydao = new PasswordPolicyDAO();
                Calendar cal = Calendar.getInstance();
                Passwordpolicy passPolicy = passPolicydao.getPasswordPolicyDetails();
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                cal.setTime(CommonDAO.getSystemDateLogin());
                cal.add(Calendar.DAY_OF_MONTH, passPolicy.getPasswordexpiryperiod());
                inputBean.setExpirydate(formatter.format(cal.getTime()));

            }
        } catch (Exception ex) {
            inputBean.setMessage("System user " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "find";
    }

    public Object getModel() {
        return inputBean;
    }

    public boolean checkAccess(String method, String userRole) {

        boolean status = false;
        String page = PageVarList.SYSTEM_USER;
        String task = null;

        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("ApproveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("findDualAuthUsers".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("changepassword".equals(method)) {
            task = TaskVarList.PAS_RESET_TASK;
        } else if ("updatechangepassword".equals(method)) {
            task = TaskVarList.PAS_RESET_TASK;
        } else if ("viewpopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("activate".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("getBdayFromNIC".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("Reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    //start newly chnged
    public String activate() {
        System.out.println("called UserManagementAction : activate");
        String message = null;
        String retType = "activate";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SystemUserDAO dao = new SystemUserDAO();
            message = this.validateInputs();
            if (message.isEmpty()) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, "Username " + inputBean.getUsername() + " updated", null);
                message = dao.activateUser(inputBean, audit);
                if (message.isEmpty()) {
                    message = "User " + MessageVarList.COMMON_SUCC_ACTIVATE;
                }
                inputBean.setMessage(message);
            } else {
                addActionError(message);
            }

        } catch (Exception e) {
            Logger.getLogger(TaskAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_ACTIVATE);
        }
        return retType;
    }

    //end newly changed
    private String validateInputs() throws Exception {

        String message = "";
        try {
            if (inputBean.getUsername() == null || inputBean.getUsername().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_USERNAME;
            } else if (inputBean.getFullname() == null || inputBean.getFullname().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_FULLNAME;
            } else if (inputBean.getServiceid() == null || inputBean.getServiceid().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_EPF;
            } else if (inputBean.getUserrole() == null || inputBean.getUserrole().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_USERROLE;
            } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_STATUS;
            } else if (inputBean.getEmail() != null && !inputBean.getEmail().isEmpty() && !Validation.isValidEmail(inputBean.getEmail())) {
                message = MessageVarList.SYSUSER_MGT_INVALID_EMAIL;
            } else if (inputBean.getBranch() == null || inputBean.getBranch().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_BRANCH;
            }

        } catch (Exception e) {
            throw e;
        }
        return message;
    }

    private String checkPolicy(String password) throws Exception {
        String errorMsg = "";
        try {
            SystemUserDAO dao = new SystemUserDAO();

            Passwordpolicy passwordpolicy = dao.getPasswordPolicyDetails();
            if (passwordpolicy != null) {

                String msg = this.CheckPasswordValidity(password, passwordpolicy);

                if (!msg.equals("")) {
                    errorMsg = msg;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return errorMsg;
    }

    public String CheckPasswordValidity(String password, Passwordpolicy bean) throws Exception {
        String msg = "";
        boolean flag = true;
        Set<Character> set = new HashSet<Character>();
        List<Character> list = new ArrayList<Character>();
        int x = 0;
        try {

            if (password.length() < bean.getMinimumlength()) {
                flag = false;
                msg = MessageVarList.SYSUSER_PAS_TOO_SHORT + bean.getMinimumlength();
            } else if (password.length() > bean.getMaximumlength()) {
                flag = false;
                msg = MessageVarList.SYSUSER_PAS_TOO_LARGE + bean.getMaximumlength();
            }

            if (flag) {
                int digits = 0;
                int upper = 0;
                int lower = 0;
                int special = 0;

                for (int i = 0; i < password.length(); i++) {
                    char c = password.charAt(i);
                    list.add(c);
                    if (Character.isDigit(c)) {
                        digits++;
                    } else if (Character.isUpperCase(c)) {
                        upper++;
                    } else if (Character.isLowerCase(c)) {
                        lower++;
                    } else {
                        special++;
                    }
                }

                if (lower < bean.getMinimumlowercasecharacters().intValue()) {
                    msg = MessageVarList.SYSUSER_PAS_LESS_LOWER_CASE_CHARACTERS + bean.getMinimumlowercasecharacters();
                    flag = false;
                } else if (upper < bean.getMinimumuppercasecharacters().intValue()) {
                    msg = MessageVarList.SYSUSER_PAS_LESS_UPPER_CASE_CHARACTERS + bean.getMinimumuppercasecharacters();
                    flag = false;
                } else if (digits < bean.getMinimumnumericalcharacters().intValue()) {
                    msg = MessageVarList.SYSUSER_PAS_LESS_NUMERICAL_CHARACTERS + bean.getMinimumnumericalcharacters();
                    flag = false;
                } else if (special < bean.getMinimumspecialcharacters().intValue()) {
                    msg = MessageVarList.SYSUSER_PAS_LESS_SPACIAL_CHARACTERS + bean.getMinimumspecialcharacters();
                    flag = false;
                }
            }

            if (flag) {
                do {
                    Character[] rechar = list.toArray(new Character[0]);
                    list.clear();
                    set.clear();
                    for (Character c : rechar) {
                        if (!set.add(c)) {
                            list.add(c);
                        }
                    }
                    x++;
                    if (bean.getRepeatcharactersallow() < x) {
                        msg = MessageVarList.SYSUSER_PAS_MORE_CHAR_REPEAT + bean.getRepeatcharactersallow();
                        break;
                    }

                } while (!list.isEmpty());

            }

        } catch (Exception e) {
            throw e;
        }
        return msg;
    }

    private String validateUpdateInputs() throws Exception {
        String message = "";
        try {
            if (inputBean.getFullname() == null || inputBean.getFullname().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_FULLNAME;
            } else if (inputBean.getServiceid() == null || inputBean.getServiceid().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_EPF;
            } else if (inputBean.getUserrole() == null || inputBean.getUserrole().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_USERROLE;
            } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_STATUS;
            } else if (inputBean.getEmail() != null && !inputBean.getEmail().isEmpty() && !Validation.isValidEmail(inputBean.getEmail().trim())) {
                message = MessageVarList.SYSUSER_MGT_INVALID_EMAIL;
            } else if (inputBean.getBranch() == null || inputBean.getBranch().trim().isEmpty()) {
                message = MessageVarList.SYSUSER_MGT_EMPTY_BRANCH;
            }

        } catch (Exception e) {
            throw e;
        }
        return message;
    }

    private String validateChangePasswordInputs() throws Exception {
        String message = "";
        try {

            if (inputBean.getCnewpwd() == null || inputBean.getCnewpwd().trim().isEmpty()) {
                message = MessageVarList.PASRESET_EMPTY_NEW_PAS;
            } else if (inputBean.getCrenewpwd() == null || inputBean.getCrenewpwd().trim().isEmpty()) {
                message = MessageVarList.PASRESET_EMPTY_COM_PAS;
            } else if (!inputBean.getCnewpwd().trim().contentEquals(inputBean.getCrenewpwd().trim())) {
                message = MessageVarList.PASRESET_MATCH_PAS;
            }
            if (inputBean.getCnewpwd().trim().equals(inputBean.getCrenewpwd().trim())) {
                String msg = "";
                msg = this.checkPolicy(inputBean.getCnewpwd().trim());
                if (message.equals("")) {
                    message = msg;
                }
            }
        } catch (Exception e) {
            throw e;
        }
        return message;
    }

    public String viewpopup() {
        String result = "viewpopup";
        System.out.println("called SystemUser : ViewPopup");
        try {
            this.applyUserPrivileges();
            CommonDAO dao = new CommonDAO();
            SystemUserDAO sysdao = new SystemUserDAO();
            PasswordPolicyDAO passPolicydao = new PasswordPolicyDAO();
            Passwordpolicy passPolicy = passPolicydao.getPasswordPolicyDetails();
            ParameterUserCommon userparam = null;
            userparam = sysdao.setTooltip("PWTOOLTIP");

            //set branch list
            SystemUserDAO sudao = new SystemUserDAO();
            List<BranchListBean> branchlist = sudao.getBranchBeanList();
            inputBean.setBranchList(branchlist);

            inputBean.setPwtooltip(passPolicydao.generateToolTipMessage(passPolicy));
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setUserroleList(dao.getUserRoleListByStatus(CommonVarList.STATUS_ACTIVE, CommonVarList.SYS_MERCHANT));

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception e) {
            addActionError("System user " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    public String detail() {
        System.out.println("called SystemUser: detail");
        System.err.println("detail " + inputBean.getUsername());
        Systemuser user = null;

        try {
            if (inputBean.getUsername() != null && !inputBean.getUsername().isEmpty()) {

                SystemUserDAO dao = new SystemUserDAO();
                CommonDAO commonDAO = new CommonDAO();
                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                inputBean.setUserroleList(commonDAO.getALLUserList(CommonVarList.SYS_MERCHANT));

                //set branch list
                SystemUserDAO sudao = new SystemUserDAO();
                List<BranchListBean> branchlist = sudao.getBranchBeanList();
                inputBean.setBranchList(branchlist);

                user = dao.getSystemUserByUserName(inputBean.getUsername());

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                try {
                    inputBean.setUsername(user.getUsername());
                } catch (NullPointerException e) {
                    inputBean.setUsername("");
                }

                try {
                    inputBean.setUserrole(user.getUserrole().getUserrolecode());
                } catch (NullPointerException e) {
                    inputBean.setUserrole("");
                }

                try {
                    inputBean.setStatus(user.getStatus().getStatuscode());
                } catch (NullPointerException e) {
                    inputBean.setStatus("");
                }

                try {
                    inputBean.setBranch(user.getBranch());
                } catch (NullPointerException e) {
                    inputBean.setBranch("");
                }

                try {
                    inputBean.setFullname(user.getFullname());
                } catch (NullPointerException e) {
                    inputBean.setFullname("");
                }

                try {
                    inputBean.setServiceid(user.getEmpid());
                } catch (NullPointerException e) {
                    inputBean.setServiceid("");
                }

                try {
                    inputBean.setAddress1(user.getAddress());
                } catch (NullPointerException e) {
                    inputBean.setAddress1("");
                }

                try {
                    inputBean.setCity(user.getCity());
                } catch (NullPointerException e) {
                    inputBean.setCity("");
                }

                try {
                    inputBean.setContactno(user.getMobile());
                } catch (NullPointerException e) {
                    inputBean.setContactno("");
                }

                try {
                    inputBean.setEmail(user.getEmail());
                } catch (NullPointerException e) {
                    inputBean.setEmail("");
                }

                try {
                    inputBean.setNic(user.getNic());
                } catch (NullPointerException e) {
                    inputBean.setNic("");
                }

                String serid_audit = "";
                String add1_audit = "";
                String city_audit = "";
                String email_audit = "";

                if (inputBean.getServiceid() == null || inputBean.getServiceid().isEmpty()) {
                    serid_audit = "";
                } else {
                    serid_audit = inputBean.getServiceid();
                }
                if (inputBean.getAddress1() == null || inputBean.getAddress1().isEmpty()) {
                    add1_audit = "";
                } else {
                    add1_audit = inputBean.getAddress1();
                }

                if (inputBean.getEmail() == null || inputBean.getEmail().isEmpty()) {
                    email_audit = "";
                } else {
                    email_audit = inputBean.getEmail();
                }

                if (inputBean.getCity() == null || inputBean.getCity().isEmpty()) {
                    city_audit = "";
                } else {
                    city_audit = inputBean.getCity();
                }

                inputBean.setOldvalue(inputBean.getUsername() + "|"
                        + inputBean.getFullname() + "|"
                        + inputBean.getUserrole() + "|"
                        + inputBean.getStatus() + "|"
                        + email_audit + "|"
                        + add1_audit + "|"
                        + city_audit + "|"
                        + serid_audit + "|"
                        + inputBean.getBranch());

            } else {
                inputBean.setMessage("Empty ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage("System user " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";

    }

    public String getBdayFromNIC() {
        try {
            System.out.println("called SystemUser: getBdayFromNIC");
            if (!inputBean.getNic().isEmpty() && !Validation.isValidateNIC(inputBean.getNic())) {
                inputBean.setMessage(MessageVarList.SYSUSER_MGT_INVALID_NIC);
            } else {
                String id = inputBean.getNic();

                int mo = 0, da = 0;
                String months;
                String date;
                int month[] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
                int days;
                int year;
                int d;
                boolean leapYrFlag;
                if (id.length() == 10) {
                    year = 1900 + Integer.parseInt(id.substring(0, 2));
                    d = Integer.parseInt(id.substring(2, 5));
                } else {
                    year = Integer.parseInt(id.substring(0, 4));
                    d = Integer.parseInt(id.substring(4, 7));

                }

                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                    leapYrFlag = true;
                } else {
                    leapYrFlag = false;
                }
                if (d == 366 || d == 866) {
                    mo = 12;
                    da = 31;
                } else {
                    if (leapYrFlag == true) {
                        if ((d == 60) || (d == 560)) {
                            mo = 2;
                            da = 29;
                        } else {
                            if (d > 500) {
                                days = d - 500;
                            } else {
                                days = d;
                            }
                            for (int i = 0; i < 12; i++) {
                                if (days < month[i]) {
                                    mo = i + 1;
                                    da = days;
                                    break;
                                } else {
                                    days = days - month[i];
                                }
                            }

                        }
                    } else {
                        if ((d == 60) || (d == 560)) {
                            mo = 0;
                            da = 0;
                        } else {
                            if (d > 500) {
                                days = d - 500;
                            } else {
                                days = d;
                            }
                            for (int i = 0; i < 12; i++) {
                                if (days < month[i]) {
                                    mo = i + 1;
                                    da = days;
                                    break;
                                } else {
                                    days = days - month[i];
                                }
                            }

                        }
                    }
                }
                if (mo != 0 && da == 0) {
                    mo = mo - 1;
                    if (month[mo - 1] == 31) {
                        da = 31;
                    } else if (month[mo - 1] == 30) {
                        da = 30;
                    }
                }
                if (0 < mo && mo < 10) {
                    months = "0" + mo;
                } else {
                    months = String.valueOf(mo);
                }
                if (0 < da && da < 10) {
                    date = "0" + da;
                } else {
                    date = String.valueOf(da);
                }

                if (mo == 0 && da == 0) {
                    inputBean.setMessage(MessageVarList.SYSUSER_MGT_INVALID_NIC);
                } else if (year < 1900) {
                    inputBean.setMessage(MessageVarList.SYSUSER_MGT_INVALID_NIC_NO_SUCH_USER);
                } else {

                    //    System.out.println("Year : " + year + "\nMonth : " + mo + "\nDate : " + da);
                    String bday = year + "-" + months + "-" + date;
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date today = new Date();
                    //    System.out.println(bday);
                    if (dateFormat.parse(bday).before(today)) {
                        inputBean.setDateofbirth(bday);
                    } else {
                        inputBean.setMessage(MessageVarList.SYSUSER_MGT_INVALID_NIC_NO_SUCH_USER);
                    }
                }
            }
        } catch (Exception ex) {
//            System.out.println("Exception");
            inputBean.setMessage(MessageVarList.SYSUSER_MGT_INVALID_NIC);
        }
        return "getBday";

    }

    public String ApproveList() {
        System.out.println("called SystemUserAction: ApproveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            SystemUserDAO dao = new SystemUserDAO();
            List<SystemUserPendBean> dataList = dao.getPendingSystemUserList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("System user" + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return "list";
    }

    public String Confirm() {
        System.out.println("called SystemUserAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SystemUserDAO dao = new SystemUserDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, " ", null);
            message = dao.confirmSystemUser(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation approved successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" System user " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String Reject() {
        System.out.println("called SystemUserAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            SystemUserDAO dao = new SystemUserDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.SYSTEM_USER, SectionVarList.USERMANAGEMENT, " ", null);
            message = dao.rejectSystemUser(inputBean, audit);
            if (message.isEmpty()) {
                message = "Requested operation rejected successfully ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(SystemUserAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(" System user " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

}
