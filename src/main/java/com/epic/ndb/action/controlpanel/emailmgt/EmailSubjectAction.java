/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.emailmgt;

import com.epic.ndb.bean.controlpanel.emailmgt.EmailSubjectBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailSubjectInputBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailSubjectPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.emailmgt.EmailSubjectDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.InboxServiceCategory;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author chathuri_t
 */
public class EmailSubjectAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    EmailSubjectInputBean inputBean = new EmailSubjectInputBean();

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called EmailSubjectAction : execute");
        return SUCCESS;
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.MAIL_SUB_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("Delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("Find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("Update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("ViewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.MAIL_SUB_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            System.out.println("called EmailSubjectAction :View");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Email Subject");
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called EmailSubjectAction : detail");
        InboxServiceCategory tt = null;
        try {
            if (inputBean.getSubjectid()!= null && !inputBean.getSubjectid().isEmpty()) {

                EmailSubjectDAO dao = new EmailSubjectDAO();
                CommonDAO commonDAO = new CommonDAO();
                inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

                tt = dao.findMailSubjectById(inputBean.getSubjectid());

                inputBean.setSubjectid(String.valueOf(tt.getId()));
                inputBean.setService(tt.getService());
                inputBean.setStatus(tt.getStatus().getStatuscode());

                inputBean.setOldvalue(inputBean.getSubjectid() + "|" + inputBean.getService()+ "|" + inputBean.getStatus());

            } else {
                inputBean.setMessage("Empty Mail ID.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " Mail Subject");
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";

    }

    public String ViewPopup() {
        String result = "viewpopup";
        System.out.println("called EmailSubjectAction : ViewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

            System.out.println("called EmailSubjectAction :viewpopup");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String Add() {
        System.out.println("called EmailSubjectAction : Add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            EmailSubjectDAO dao = new EmailSubjectDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newV = inputBean.getSubjectid()+ "|"
                        + inputBean.getService()+ "|"
                        + inputBean.getStatus();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.MAIL_SUB_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Requested to add mail subject ( ID : " + inputBean.getSubjectid() + " )", null, null, newV);
                message = dao.insertMailSubject(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + " mail subject ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("Mail subject " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getSubjectid() == null || inputBean.getSubjectid().trim().isEmpty()) {
            message = MessageVarList.MAIL_SUBJECT_MGT_EMPTY_CODE;
        } else if (inputBean.getService()== null || inputBean.getService().trim().isEmpty()) {
            message = MessageVarList.MAIL_SUBJECT_MGT_EMPTY_DESCRIPTION;
        } else if (inputBean.getStatus() != null && inputBean.getStatus().isEmpty()) {
            message = MessageVarList.MAIL_SUBJECT_MGT_EMPTY_STATUS;
        } else {
            if (!Validation.isSpecailCharacter(inputBean.getSubjectid())) {
                message = MessageVarList.MAIL_SUBJECT_MGT_ERROR_TT_CODE_INVALID;
            } else if (!Validation.isSpecailCharacter(inputBean.getService())) {
                message = MessageVarList.MAIL_SUBJECT_MGT_ERROR_DESC_INVALID;
            }
        }
        return message;
    }

    public String List() {
        System.out.println("called EmailSubjectAction: List");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            EmailSubjectDAO dao = new EmailSubjectDAO();
            List<EmailSubjectBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {
                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "["
                        + checkEmptyorNullString("ID ", inputBean.getS_id())
                        + checkEmptyorNullString("Description", inputBean.getS_service())
                        + checkEmptyorNullString("Status", inputBean.getS_status())
                        + "]";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.MAIL_SUB_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Transfer type search using " + searchParameters + " parameters ", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
        } catch (Exception e) {
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mail subject ");
        }
        return "list";
    }

    public String Find() {
        System.out.println("called EmailSubjectAction : Find");
        InboxServiceCategory tt = null;
        try {
            if (inputBean.getSubjectid() != null && !inputBean.getSubjectid().isEmpty()) {

                EmailSubjectDAO dao = new EmailSubjectDAO();

                tt = dao.findMailSubjectById(inputBean.getSubjectid());

                inputBean.setSubjectid(String.valueOf(tt.getId()));
                inputBean.setService(tt.getService());
                inputBean.setStatus(tt.getStatus().getStatuscode());

                inputBean.setOldvalue(inputBean.getSubjectid() + "|" + inputBean.getService()+ "|" + inputBean.getStatus() );

            } else {
                inputBean.setMessage("Empty mail subject id.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " mial subject");
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String Update() {

        System.out.println("called EmailSubjectAction : Update");
        String retType = "message";

        try {
            if (inputBean.getSubjectid() != null && !inputBean.getSubjectid().isEmpty()) {

                inputBean.setSubjectid(inputBean.getSubjectid());

                String message = this.validateInputs();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();
                    EmailSubjectDAO dao = new EmailSubjectDAO();

                    String newV = inputBean.getSubjectid() + "|" + inputBean.getService() + "|" + inputBean.getStatus();
                    String oldVal = inputBean.getOldvalue();

                    System.out.println("newV   :" + newV);
                    System.out.println("oldVal :" + oldVal);

                    if (!newV.equals(oldVal)) {
                        String newValWithActState = inputBean.getSubjectid() + "|" + inputBean.getService()+ "|" + CommonVarList.STATUS_ACTIVE;
                        if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.MAIL_SUB_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Requested to update mail subject (ID : " + inputBean.getSubjectid() + " )", null, oldVal, newV);
                            message = dao.updateMailSubject(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " email subject ");
                            } else {
                                addActionError(message);
                            }
                        }else{
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " mail subject ");
        }
        return retType;
    }

    public String Delete() {
        System.out.println("called EmailSubjectAction : Delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            EmailSubjectDAO dao = new EmailSubjectDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.MAIL_SUB_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Requested to delete mail subject (ID : " + inputBean.getSubjectid() + " )", null);
            message = dao.deleteMailSubject(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " mail subject ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called EmailSubjectAction : Confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                EmailSubjectDAO dao = new EmailSubjectDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.MAIL_SUB_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmMailSubject(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " email subject");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called EmailSubjectAction : Reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                EmailSubjectDAO dao = new EmailSubjectDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.MAIL_SUB_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectMailSubject(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " email subject");
        }
        return retType;
    }

    public String approveList() {
        System.out.println("called EmailSubjectAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            EmailSubjectDAO dao = new EmailSubjectDAO();
            List<EmailSubjectPendBean> dataList = dao.getPendingMailSubjectList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.MAIL_SUB_MGT_PAGE, SectionVarList.EMAIL_MANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(EmailSubjectAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " mail subject ");
        }
        return "list";
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }
}
