/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.OtherBranchMgtBean;
import com.epic.ndb.bean.controlpanel.systemconfig.OtherBranchMgtInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.OtherBranchMgtPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.OtherBranchMgtDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.OtherBankBranch;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class OtherBranchMgtAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    OtherBranchMgtInputBean inputBean = new OtherBranchMgtInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    private String serverPath;

    @Override
    public Object getModel() {
        return inputBean;
    }

    @Override
    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.OTHER_BRANCH_MGT_PAGE;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("List".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("approveList".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("add".equals(method)) {
            task = TaskVarList.ADD_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("find".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("update".equals(method)) {
            task = TaskVarList.UPDATE_TASK;
        } else if ("viewPopup".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPopupcsv".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("pendCsvDownloade".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("template".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("upload".equals(method)) {
            task = TaskVarList.UPLOAD_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        }
        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.OTHER_BRANCH_MGT_PAGE, request);

        inputBean.setVadd(true);
        inputBean.setVdelete(true);
        inputBean.setVupdatelink(true);
        inputBean.setVsearch(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVupload(true);
        inputBean.setVdual(true);
        inputBean.setVgenerate(true);
        inputBean.setVgenerateview(true);

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_TASK)) {
                    inputBean.setVadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_TASK)) {
                    inputBean.setVupdatelink(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPLOAD_TASK)) {
                    inputBean.setVupload(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                    inputBean.setVgenerateview(false);
                }
            }
        }
        inputBean.setVupdatebutt(true);
    }

    public String view() {

        String result = "view";
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setBankList(dao.getOtherBankList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
            System.out.println("called OtherBranchMgtAction : view");

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch ");
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String List() {
        System.out.println("called OtherBranchMgtAction : List");
        try {
            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }
            OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
            List<OtherBranchMgtBean> dataList = dao.getSearchList(inputBean, rows, from, orderBy);

            /**
             * for search audit
             */
            if (inputBean.isSearch() && from == 0) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String searchParameters = "Other branch search using ["
                        + checkEmptyorNullString("Bank", inputBean.getBankSearch())
                        + checkEmptyorNullString("Branch code", inputBean.getBranchcodeSearch())
                        + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                        + checkEmptyorNullString("Branch name", inputBean.getBranchnameSearch())
                        + "] parameters ";
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, searchParameters, null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModel(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);

                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.OTHER_BRANCH_SEARCH_BEAN, inputBean);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch ");
        }
        return "list";
    }

    public String approveList() {
        System.out.println("called OtherBranchMgtAction: approveList");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";
            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpServletRequest request = ServletActionContext.getRequest();
            HttpSession session = request.getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            inputBean.setCurrentUser(sysUser.getUsername());

            OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
            List<OtherBranchMgtPendBean> dataList = dao.getPendingCardCenterList(inputBean, rows, from, orderBy);

            if (inputBean.isSearch() && from == 0) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Pending Task list", null);
                SystemAuditDAO sysdao = new SystemAuditDAO();
                sysdao.saveAudit(audit);
            }
            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }
            // }
        } catch (Exception e) {
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch ");
        }
        return "list";
    }

    public String viewPopup() {
        String result = "viewpopup";
        System.out.println("called OtherBranchMgtAction : viewPopup");
        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setBankList(dao.getOtherBankList());
            inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch ");
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String viewPopupcsv() {
        String result = "viewpopupcsv";
        System.out.println("called OtherBranchMgtAction : viewPopupcsv");
        try {
            this.applyUserPrivileges();
            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception e) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch");
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    public String upload() {
        System.out.println("called OtherBranchMgtAction : upload");
        String result = "messagecsv";
        ServletContext context = ServletActionContext.getServletContext();

//        this.serverPath = context.getRealPath("/resouces/csv_temp/atm_locations");
        if (Common.getOS_Type().equals("WINDOWS")) {
            this.setServerPath(context.getRealPath("/resouces/csv_temp/otherbranchmgt"));
        } else if (Common.getOS_Type().equals("LINUX")) {
            this.setServerPath("/app_conf/Epic/csv_temp/otherbranchmgt");
        }
        try {
            if (inputBean.getHiddenId() != null) {

                HttpServletRequest request = ServletActionContext.getRequest();
                OtherBranchMgtDAO dao = new OtherBranchMgtDAO();

                String message = "";

                DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
                Date date = new Date();

                message = this.getFile(inputBean.getConXLFileName()); // get file

                inputBean.setConXLFileName(dateFormat.format(date) + inputBean.getConXLFileName());

                File directory = new File(getServerPath());

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                if (-1 != inputBean.getConXLFileName().lastIndexOf("\\")) {
                    inputBean.setConXLFileName(inputBean.getConXLFileName().substring(inputBean.getConXLFileName().lastIndexOf("\\") + 1));
                }
                File filetoCreate = new File(getServerPath(), inputBean.getConXLFileName());
//                    if (isfileexists==true) {            //set to database
                if (filetoCreate.exists()) {                //set to local 
                    addActionError("File already exists.");
                    System.err.println("File already exists.");
                } else {

                }

                if (message.isEmpty()) {

                    if (inputBean.getConXL() == null) {
                    } else {
                        message = this.validateUpload();
                        if (message.isEmpty()) {
                            FileUtils.copyFile(inputBean.getConXL(), filetoCreate);
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPLOAD_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to upload  other branch(es) ", null, null, null);
                            message = dao.uploadOtherBranch(inputBean, audit);
                        }
                    }
                }
                if (message.isEmpty()) {
                    addActionMessage("File uploaded successfully");
                    System.err.println("File uploaded successfully");
                } else {
                    addActionError(message);
                }
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch");
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, ex);

        }
        return result;
    }

    public String pendCsvDownloade() {
        System.out.println("called OtherBranchMgtAction : pendCsvDownloade");
        String message = null;
        String retType = "pendcsvdownloade";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.VIEW_PENDING_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " Pending other branch csv file downloaded by pending task id " + inputBean.getId(), null);
                message = dao.pendPromotionCsvDownloade(inputBean, audit);
                if (!message.isEmpty()) {
                    addActionError(message);
                    retType = "message";
                } else {
                    setInputStream(inputBean.getFileInputStream());
                    setContentLength(inputBean.getFileLength());
                }
            } else {
                addActionError("Empty pending task ID.");
                retType = "message";
            }
            inputBean.setFileInputStream(null);
            inputBean.setFileLength(0);
        } catch (Exception e) {
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch");
            retType = "message";
            inputBean.setFileInputStream(null);
            inputBean.setFileLength(0);
        }
        return retType;
    }

    public String add() {

        System.out.println("called OtherBranchMgtAction : add");
        String result = "message";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
            String message = this.validateInputs();

            if (message.isEmpty()) {

                String newv = inputBean.getBank().trim() + "|" + inputBean.getBranchcode().trim() + "|" + inputBean.getStatus().trim() + "|" + inputBean.getBranchname().trim();

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to add other branch ( bank code : " + inputBean.getBank() + " , branch code : " + inputBean.getBranchcode() + " )", null, null, newv);

                message = dao.insertOtherBranch(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + "other branch ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch");
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {
        System.out.println("called OtherBranchMgtAction: Detail");
        OtherBankBranch branch = null;
        try {
            if (inputBean.getBank() != null && !inputBean.getBank().isEmpty()) {
                if (inputBean.getBank() != null && !inputBean.getBank().isEmpty()) {

                    OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
                    CommonDAO commonDAO = new CommonDAO();

                    inputBean.setStatusList(commonDAO.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));
                    inputBean.setBankList(commonDAO.getOtherBankList());
                    inputBean.setDefaultStatus(CommonVarList.STATUS_ACTIVE);

                    branch = dao.findOtherBranchById(inputBean.getBank(), inputBean.getBranchcode());

                    inputBean.setBranchname(branch.getBranchname());
                    inputBean.setStatus(branch.getStatus().getStatuscode());

                    inputBean.setOldvalue(inputBean.getBank() + "|"
                            + inputBean.getBranchcode() + "|"
                            + branch.getStatus().getStatuscode() + "|"
                            + branch.getBranchname());

                } else {
                    addActionError("Empty branch code.");
                }
            } else {
                addActionError("Empty bank.");
            }
        } catch (Exception ex) {
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch ");
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "detail";
    }

    public String find() {
        System.out.println("called OtherBranchMgtAction: find");
        OtherBankBranch branch = null;
        try {
            if (inputBean.getBank() != null && !inputBean.getBank().isEmpty()) {
                if (inputBean.getBank() != null && !inputBean.getBank().isEmpty()) {

                    OtherBranchMgtDAO dao = new OtherBranchMgtDAO();

                    branch = dao.findOtherBranchById(inputBean.getBank(), inputBean.getBranchcode());

                    inputBean.setBranchname(branch.getBranchname());
                    inputBean.setStatus(branch.getStatus().getStatuscode());
                } else {
                    inputBean.setMessage("Empty branch code.");
                }
            } else {
                inputBean.setMessage("Empty bank.");
            }
        } catch (Exception ex) {
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " other branch ");
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return "find";

    }

    public String update() {

        System.out.println("called OtherBranchMgtAction : update");
        String retType = "message";

        try {
            OtherBranchMgtDAO dao = new OtherBranchMgtDAO();

            if (inputBean.getBankhidden() != null && !inputBean.getBankhidden().trim().isEmpty()) {
                inputBean.setBank(inputBean.getBankhidden());
            }
            String message = this.validateUpdates();

            if (message.isEmpty()) {

                HttpServletRequest request = ServletActionContext.getRequest();

                String newv = inputBean.getBank().trim() + "|" + inputBean.getBranchcode().trim() + "|" + inputBean.getStatus().trim() + "|" + inputBean.getBranchname().trim();

                String oldVal = inputBean.getOldvalue();

                System.out.println("newV   :" + newv);
                System.out.println("oldVal :" + oldVal);

                if (!newv.equals(oldVal)) {
                    String newValWithActState = inputBean.getBank().trim() + "|" + inputBean.getBranchcode().trim() + "|" + CommonVarList.STATUS_ACTIVE + "|" + inputBean.getBranchname().trim();   
                    if(inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) ||(inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldVal.equals(newValWithActState))){

                        Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to update other branch ( bank code : " + inputBean.getBank() + " , branch code :" + inputBean.getBranchcode() + " )", null, null, newv);
                        message = dao.updateOtherBranch(inputBean, audit);

                        if (message.isEmpty()) {
                            addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " other branch ");
                        } else {
                            addActionError(message);
                        }
                    }else{
                        addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                    }
                } else {
                    addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                }

            } else {
                addActionError(message);
            }
        } catch (Exception ex) {
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, ex);
            addActionError(MessageVarList.COMMON_ERROR_UPDATE + " other branch");
        }
        return retType;
    }

    public String delete() {

        System.out.println("called OtherBranchMgtAction : delete");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Requested to delete other branch ( bank code : " + inputBean.getBank() + " , branch code : " + inputBean.getBranchcode() + " )", null);
            message = dao.deleteOtherBranch(inputBean, audit);
            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + "other branch ";
            }
            inputBean.setMessage(message);
        } catch (Exception e) {
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String confirm() {
        System.out.println("called OtherBranchMgtAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmOtherBranch(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_CONFIRM + " other branch");
        }
        return retType;
    }

    public String reject() {
        System.out.println("called OtherBranchMgtAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejectOtherBranch(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_REJECT + " other branch");
        }
        return retType;
    }

    public String template() {

        System.out.println("called OtherBranchMgtAction: template");
        String retType = "csv";
        FileWriter fileWriter = null;
        BufferedWriter bw = null;
        PrintWriter out = null;
        try {
            ServletContext context = ServletActionContext.getServletContext();
            String destPath = context.getRealPath("/resouces/csv_temp/otherbranchmgt");
            File fileToDownload = new File(destPath, "otherbranchmgt.csv");

            try {
                fileWriter = new FileWriter(fileToDownload);
                bw = new BufferedWriter(fileWriter);
                out = new PrintWriter(bw);

                out.println("Bank Code,Branch Code,Branch Name");
                out.println("7010,71,Test Branch");

//                 
            } catch (Exception e) {
                retType = "message";
                addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch");
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (fileWriter != null) {
                        fileWriter.close();
                    }
                } catch (Exception e) {
                }
            }

            setInputStream(new FileInputStream(fileToDownload));
            setFileName(fileToDownload.getName());
            setContentLength(fileToDownload.length());

        } catch (Exception e) {
            Logger.getLogger(OtherBranchMgtAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch");
            retType = "message";
        }

        return retType;
    }

    public String reportGenerate() {

        System.out.println("called OtherBranchMgtAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                OtherBranchMgtDAO dao = new OtherBranchMgtDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                //PrintWriter pw = new PrintWriter(new File("txnexplorer_report.csv"));

//                HttpServletResponse response = ServletActionContext.getResponse();
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    OtherBranchMgtInputBean searchBean = (OtherBranchMgtInputBean) session.getAttribute(SessionVarlist.OTHER_BRANCH_SEARCH_BEAN);
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new OtherBranchMgtInputBean());
                    }

                    try {
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Other_Branch_Report.csv");
                        setContentLength(sb.length());

                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.OTHER_BRANCH_MGT_PAGE, SectionVarList.SYSTEMCONFIGMANAGEMENT, "Other branch csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger
                            .getLogger(OtherBranchMgtAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(OtherBranchMgtAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " other branch");

            return "message";
        }
        return retMsg;
    }

    private String validateInputs() {
        String message = "";
        if (inputBean.getBank() == null || inputBean.getBank().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_CODE;
        } else if (inputBean.getBranchcode() == null || inputBean.getBranchcode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchcode())) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_CODE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_STATUS;
        } else if (inputBean.getBranchname() == null || inputBean.getBranchname().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchname())) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_NAME;
        }

        return message;
    }

    private String validateUpdates() {
        String message = "";
        if (inputBean.getBank() == null || inputBean.getBank().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_CODE;
        } else if (inputBean.getBranchcode() == null || inputBean.getBranchcode().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchcode())) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_CODE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_STATUS;
        } else if (inputBean.getBranchname() == null || inputBean.getBranchname().trim().isEmpty()) {
            message = MessageVarList.CARD_CENTER_EMPTY_BANK_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchname())) {
            message = MessageVarList.CARD_CENTER_INVALID_BANK_NAME;
        }

        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getFile(String file) {
        String msgEx = "";
        if (file == null) {
            msgEx = "Please choose a file to upload.";
        } else {
            msgEx = Validation.isCSV(file);
        }
        return msgEx;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

    private String validateUpload() throws Exception {
        String message = "";
        FileReader isr = null;
        BufferedReader br = null;
        Session hSession = null;
        File csvFile = null;

        try {

            String[] parts = new String[0];
            int countrecord = 1;
            int succesrec = 0;
            String thisLine = null;
            boolean getline = false;
            String token = "";
            OtherBranchMgtDAO dao = new OtherBranchMgtDAO();

            csvFile = inputBean.getConXL();
            isr = new FileReader(csvFile);
            br = new BufferedReader(isr);
            hSession = HibernateInit.sessionFactory.openSession();

            while ((thisLine = br.readLine()) != null) {
                if (thisLine.trim().equals("")) {
                    continue;
                } else {
                    if (getline) {
                        token = thisLine;

//                        System.err.println(token);
                        try {
                            parts = token.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", 3);
                            inputBean.setBank(Common.removeCsvDoubleQuotation(parts[0].trim()));
                            inputBean.setBranchcode(Common.removeCsvDoubleQuotation(parts[1].trim()));
                            inputBean.setBranchname(Common.removeCsvDoubleQuotation(parts[2].trim()));
                            inputBean.setStatus(CommonVarList.STATUS_ACTIVE);

                        } catch (Exception ee) {
                            message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                            break;
                        }
                        countrecord++;
                        if (parts.length >= 3 && message.isEmpty()) {
                            message = dao.validateInputsForCSV(inputBean);
                            if (message.isEmpty()) {
                                message = dao.validateUpload(inputBean, hSession);
                                if (message.isEmpty()) {
                                    succesrec++;
                                } else {
                                    message = message + " at line number " + countrecord;
//                                            + ",success count :" + succesrec;
                                    break;
                                }
                            } else {
                                message = message + " at line number " + countrecord;
//                                        + ",success count :" + succesrec;
                                break;
                            }

                        } else {
                            message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                            break;
                        }
                    } else {
                        getline = true;
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (hSession != null) {
                hSession.flush();
                hSession.close();
            }
        }
        return message;
    }

}
