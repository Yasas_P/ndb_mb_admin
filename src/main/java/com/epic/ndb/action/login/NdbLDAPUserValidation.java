/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.login;

import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.varlist.CommonVarList;
import java.util.Hashtable;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.SizeLimitExceededException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

/**
 *
 * @author TU-Nuwan
 */
public class NdbLDAPUserValidation {
    
    public Boolean validateLogin(String username, String password) throws Exception {
        
        
        System.out.println(CommonVarList.INITIAL_CONTEXT_FACTORY);
        System.out.println(CommonVarList.PROVIDER_URL);
        System.out.println(CommonVarList.SECURITY_AUTHENTICATION);
        System.out.println(CommonVarList.SECURITY_PRINCIPAL);
        System.out.println(CommonVarList.SECURITY_CREDENTIALS);
                

        Hashtable<String, String> env = new Hashtable<String, String>();
        
        CommonDAO dao =new CommonDAO();
//        String dominName = dao.findCommonConfigById(CommonVarList.AD_PROVIDER_URL).getParamValue();

        System.setProperty("javax.net.ssl.trustStorePassword", "changeit");
        env.put(Context.INITIAL_CONTEXT_FACTORY, CommonVarList.INITIAL_CONTEXT_FACTORY);
        env.put(Context.PROVIDER_URL, CommonVarList.PROVIDER_URL);
        env.put(Context.REFERRAL, "follow");
        env.put(Context.SECURITY_AUTHENTICATION, CommonVarList.SECURITY_AUTHENTICATION);
        env.put(Context.SECURITY_PRINCIPAL, CommonVarList.SECURITY_PRINCIPAL);
        env.put(Context.SECURITY_CREDENTIALS, StringUtils.newStringUtf8(Base64.decodeBase64(CommonVarList.SECURITY_CREDENTIALS)));

        DirContext ctx = null;
        NamingEnumeration<SearchResult> results = null;

        try {

            ctx = new InitialDirContext(env);
//            return true;

            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE); // Search Entire Subtree
            controls.setCountLimit(1);   //Sets the maximum number of entries to be returned as a result of the search
            controls.setTimeLimit(5000); // Sets the time limit of these SearchControls in milliseconds

            String searchString = "(&(sAMAccountName=" + username + ")(objectClass=user))";

            results = ctx.search("DC=ndblk,DC=int", searchString, controls);

            if (results.hasMore()) {
                System.out.println("Result 2 : >>>>>>>>>>" + results.hasMore());
                SearchResult result = (SearchResult) results.next();
                System.out.println("Result 3 : >>>>>>>>>>" + result);
                Attributes attrs = result.getAttributes();
                System.out.println("Result 4 : >>>>>>>>>>" + attrs);

                Attribute dnAttr = attrs.get("distinguishedName");
                String dn = (String) dnAttr.get();
                System.out.println("" + dn);
                // User Exists, Validate the Password
                env.put(Context.SECURITY_PRINCIPAL, dn);
                env.put(Context.SECURITY_CREDENTIALS, password);
                new InitialDirContext(env); // Exception will be thrown on Invalid case

                return true;
            } else {
                return false;
            }
        } catch (AuthenticationException e) { // Invalid Login
//            e.printStackTrace();
            System.out.println("AuthenticationException" + e);
            return false;
        } catch (NameNotFoundException e) { // The base context was not found.
            System.out.println("NameNotFoundException" + e);
            return false;
        } catch (SizeLimitExceededException e) {
            System.out.println("SizeLimitExceededException" + e);
            throw new RuntimeException("LDAP Query Limit Exceeded, adjust the query to bring back less records", e);
        } catch (NamingException e) {
            System.out.println("NamingException" + e);
//            throw new RuntimeException(e);
//            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (results != null) {
                try {
                    results.close();
                } catch (Exception e) {
                    /* Do Nothing */ }
            }

            if (ctx != null) {
                try {
                    ctx.close();
                } catch (Exception e) {
                    /* Do Nothing */ }
            }

        }

    }
    
    
    
}
