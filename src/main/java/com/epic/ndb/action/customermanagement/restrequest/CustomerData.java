/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.customermanagement.restrequest;

import com.epic.ndb.bean.customermanagement.CustomerBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchInputBean;
import com.epic.ndb.bean.customermanagement.SwtConfigBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.customermanagement.CustomerSearchDAO;
import com.epic.ndb.util.varlist.CommonVarList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sivaganesan_t
 */
public class CustomerData {
    
    public String getCustomerData(CustomerSearchInputBean inputbean) throws IOException, Exception {
        String response = "";
        String message = "";
        URL url = null;
        HttpURLConnection urlConnection = null;
        String urnDetail = "";
        String baseUrlDetail = "";
        String urlDetail = "";
        BufferedWriter bWriter = null;
        BufferedReader bReader = null;
        String request = "";
        try {
             CommonDAO dao =new CommonDAO();
            CustomerSearchDAO customerDao =new CustomerSearchDAO();
            
            SwtConfigBean swtConfigBean= customerDao.getSwitchConfig();
            
            urnDetail="common/customer_profile/"+inputbean.getCif()+"/";
            baseUrlDetail= dao.findCommonConfigById(CommonVarList.COMMON_CONFIG_REST_BASE_URL).getParamvalue();
            urlDetail=baseUrlDetail+urnDetail;
            System.out.println("-----------"+urlDetail);
            url = new URL(urlDetail);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoOutput(true);

            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Authorization", swtConfigBean.getRestAutorization());

//            request = "{\n"
//                    + "   \"mode\":" + 2 + ",\n"
////                    + "    \"accountNumber\":\"" + "421689038888" + "\"\n"
//                    + "    \"cif\":\"" + inputbean.getCif()+ "\",\n"
//                    + "    \"nic\":\"" + inputbean.getNic()+ "\"\n"
//                    + "}";
//            
//            System.out.println("request              : " + request);
//            
//            bWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
//            bWriter.write(request);
//            bWriter.flush();

            // Get the response
            bReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while (null != ((line = bReader.readLine()))) {
                response += line;
            }

            System.out.println("reponse              : " + response);
//            response="{\n" +
//                    "\"responsecode\":\"000\",\n" +
//                    "\"responseerror\":\"\",\n" +
//                    "\"mobileNumber\":\"0777336953\",\n" +
//                    "\"email\":\"pasindu.darshana@ndbsupport.int\",\n" +
//                    "\"mobileNumberSecond\":null,\n" +
//                    "\"emailSecond\":null,\n" +
//                    "\"address\":null,\n" +
//                    "\"segment\":\"Retail-HNW\",\n" +
//                    "\"name\":\"Short.name 430856\",\n" +
//                    "\"customerStatus\":\"ACT\",\n" +
//                    "\"customerCategory\":\"701\",\n" +
//                    "\"nic\":\"860970180V\",\n" +
//                    "\"accountOfficer\":\"9210\",\n" +
//                    "\"dob\":\"19630714\",\n" +
//                    "\"gender\":\"MALE\",\n" +
//                    "\"isMigratedUser\":\"false\",\n" +
//                    "\"isUserPasswordExpired\":\"false\",\n" +
//                    "\"isNotifyUserPasswordWarning\":\"false\", \n" +
//                    "\"daysLeft\":0,\n" +
//                    "\"userTempPasswordSent\":\"false\" \n" +
//                    "}";
            
            message =getResponseToCustomerData(inputbean,response);
        }catch (IOException e) {
            message="Fail to create connection";
            System.err.println("Fail to create connection"+e.getMessage());
        } catch (Exception ex) {
            throw ex;
        }
         return message;
    }
    
    private String getResponseToCustomerData(CustomerSearchInputBean inputbean,String switchResponse) throws Exception {
        CustomerBean customerData = new CustomerBean();
        CustomerBean customerDataSession = new CustomerBean();
        String accInfoResponseError = "";
        try {
            
            CommonDAO dao =new CommonDAO();
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(switchResponse);
            
            String accInfoResponse = ((String) obj.get("responsecode"));
            
            if (accInfoResponse!=null && (accInfoResponse.equals("00") || accInfoResponse.equals("000"))) {
                
                String mobileNumber = ((String) obj.get("mobileNumber"));
                if(mobileNumber!=null && !mobileNumber.isEmpty() && !mobileNumber.equalsIgnoreCase("null")){
                    customerData.setMobileNumber(mobileNumber.trim());
                    customerDataSession.setMobileNumber(mobileNumber.trim());
                }else{
                    customerData.setMobileNumber("--");
                    customerDataSession.setMobileNumber("");
                }
                String email = ((String) obj.get("email"));
                if(email!=null && !email.isEmpty() && !email.equalsIgnoreCase("null")){
                    customerData.setEmail(email.trim());
                    customerDataSession.setEmail(email.trim());
                }else{
                    customerData.setEmail("--");
                    customerDataSession.setEmail("");
                }
                String mobileNumberSecond = ((String) obj.get("mobileNumberSecond"));
                if(mobileNumberSecond!=null && !mobileNumberSecond.isEmpty() && !mobileNumberSecond.equalsIgnoreCase("null")){
                    customerData.setSecondaryMobile(mobileNumberSecond.trim());
                    customerDataSession.setSecondaryMobile(mobileNumberSecond.trim());
                }else{
                    customerData.setSecondaryMobile("--");
                    customerDataSession.setSecondaryMobile("");
                }
                String emailSecond = ((String) obj.get("emailSecond"));
                if(emailSecond!=null && !emailSecond.isEmpty() && !emailSecond.equalsIgnoreCase("null")){
                    customerData.setSecondaryEmail(emailSecond.trim());
                    customerDataSession.setSecondaryEmail(emailSecond.trim());
                }else{
                    customerData.setSecondaryEmail("--");
                    customerDataSession.setSecondaryEmail("");
                }
//                JSONObject addressObj = ((JSONObject) obj.get("permenent_address"));
//                if(addressObj!=null && !addressObj.isEmpty() ){
//                    StringBuilder addressTemp =new StringBuilder("");
//                    String address_1 = ((String) addressObj.get("address_1")); 
//                    if(address_1!=null && !address_1.isEmpty() && !address_1.equalsIgnoreCase("null")){
//                        addressTemp.append(address_1.trim());
//                    }
//                    String address_2 = ((String) addressObj.get("address_2"));
//                    if(address_2!=null && !address_2.isEmpty() && !address_2.equalsIgnoreCase("null")){
//                        addressTemp.append(address_2.trim());
//                    }
//                    String address_3 = ((String) addressObj.get("address_3"));
//                    if(address_3!=null && !address_3.isEmpty() && !address_3.equalsIgnoreCase("null")){
//                        addressTemp.append(address_3.trim());
//                    }
//                    if(addressTemp!=null && !addressTemp.equals("")){
//                        customerData.setPermanentAdd(addressTemp.toString());
//                        customerDataSession.setPermanentAdd(addressTemp.toString());
//                    }else{
//                        customerData.setPermanentAdd("--");
//                        customerDataSession.setPermanentAdd("");
//                    }
//                }else{
//                    customerData.setPermanentAdd("--");
//                    customerDataSession.setPermanentAdd("");
//                }
                
                customerData.setPermanentAdd("--");
                customerDataSession.setPermanentAdd("");
                String segment = ((String) obj.get("segment"));
                if(segment!=null && !segment.isEmpty() && !segment.equalsIgnoreCase("null")){
                    customerData.setSegmentType(segment.trim());
                    customerDataSession.setSegmentType(segment.trim());
                }else{
                    customerData.setSegmentType("--");
                    customerDataSession.setSegmentType("");
                }
                String name = ((String) obj.get("name"));
                if(name!=null && !name.isEmpty() && !name.equalsIgnoreCase("null")){
                    customerData.setCustomerName(name.trim());
                    customerDataSession.setCustomerName(name.trim());
                }else{
                    customerData.setCustomerName("--");
                    customerDataSession.setCustomerName("");
                }
                String customerStatus = ((String) obj.get("customerStatus"));
                if(customerStatus!=null && !customerStatus.isEmpty() && !customerStatus.equalsIgnoreCase("null")){
                    customerData.setStatus(customerStatus.trim());
                    customerDataSession.setStatus(customerStatus.trim());
                }else{
                    customerData.setStatus("--");
                    customerDataSession.setStatus("");
                }
                String customerCategory = ((String) obj.get("customerCategory"));
                if(customerCategory!=null && !customerCategory.isEmpty() && !customerCategory.equalsIgnoreCase("null")){
                    customerData.setCustomerCategory(customerCategory.trim());
                    customerDataSession.setCustomerCategory(customerCategory.trim());
                }else{
                    customerData.setCustomerCategory("--");
                    customerDataSession.setCustomerCategory("");
                }
                String nic = ((String) obj.get("nic"));
                if(nic!=null && !nic.isEmpty() && !nic.equalsIgnoreCase("null")){
                    customerData.setNic(nic.trim());
                    customerDataSession.setNic(nic.trim());
                }else{
                    customerData.setNic("--");
                    customerDataSession.setNic("");
                }
                String accountOfficer = ((String) obj.get("accountOfficer"));
                if(accountOfficer!=null && !accountOfficer.isEmpty() && !accountOfficer.equalsIgnoreCase("null")){
                    customerData.setAccountofficer(accountOfficer.trim());
                    customerDataSession.setAccountofficer(accountOfficer.trim());
                }else{
                    customerData.setAccountofficer("--");
                    customerDataSession.setAccountofficer("");
                }
                String dob = ((String) obj.get("dob"));
                if(dob!=null && !dob.isEmpty() && !dob.equalsIgnoreCase("null")){
                    customerData.setDob(dob.trim());
                    customerDataSession.setDob(dob.trim());
                }else{
                    customerData.setDob("--");
                    customerDataSession.setDob("");
                }
                String gender = ((String) obj.get("gender"));
                if(gender!=null && !gender.isEmpty() && !gender.equalsIgnoreCase("null")){
                    customerData.setGender(gender.trim());
                    customerDataSession.setGender(gender.trim());
                }else{
                    customerData.setGender("--");
                    customerDataSession.setGender("");
                }
                inputbean.setNewCustomerData(customerData);
                inputbean.setNewCustomerDataSession(customerDataSession);
            } else {
                accInfoResponseError = ((String) obj.get("responseerror"));
            }
        } catch (Exception e) {
            throw e;
        }
        return accInfoResponseError;
    }
}
