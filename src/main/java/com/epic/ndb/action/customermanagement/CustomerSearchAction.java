/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.customermanagement;

import com.epic.ndb.action.customermanagement.restrequest.AccountList;
import com.epic.ndb.action.customermanagement.restrequest.CardList;
import com.epic.ndb.action.customermanagement.restrequest.CardToken;
import com.epic.ndb.action.customermanagement.restrequest.CustomerData;
import com.epic.ndb.bean.analytics.ChannelTypeBean;
import com.epic.ndb.bean.customermanagement.AccountBean;
import com.epic.ndb.bean.customermanagement.CustomerBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchInputBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchLimitBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchPendBean;
import com.epic.ndb.bean.customermanagement.CustomerUpdateBean;
import com.epic.ndb.bean.customermanagement.KeyValueBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.controlpanel.systemconfig.SystemAuditDAO;
import com.epic.ndb.dao.customermanagement.CustomerSearchDAO;
import com.epic.ndb.util.common.AccessControlService;
import com.epic.ndb.util.common.Common;
import static com.epic.ndb.util.common.Common.checkEmptyorNullString;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.SwtMobileUser;
import com.epic.ndb.util.mapping.SwtUserDevice;
import com.epic.ndb.util.mapping.SwtUserTxnLimit;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.TransactionLimit;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.OracleMessage;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SectionVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author sivaganesan_t
 */
public class CustomerSearchAction extends ActionSupport implements ModelDriven<Object>, AccessControlService {

    CustomerSearchInputBean inputBean = new CustomerSearchInputBean();

    private InputStream inputStream = null;
    private String fileName;
    private long contentLength;

    public Object getModel() {
        return inputBean;
    }

    public String execute() {
        System.out.println("called CustomerSearchAction : execute");
        return SUCCESS;
    }

    private void applyUserPrivileges() {
        HttpServletRequest request = ServletActionContext.getRequest();
        List<Task> tasklist = new Common().getUserTaskListByPage(PageVarList.CUSTOMER_SEARCH, request);

        inputBean.setVdelete(true);
        inputBean.setVsearch(true);
        inputBean.setVviewcustomer(true);
        inputBean.setVstatuschange(true);
        inputBean.setVregister(true);
        inputBean.setVlimitchange(true);
        inputBean.setVreconfirm(true);
        inputBean.setVresetattempts(true);
        inputBean.setVinvalidsignup(true);
        inputBean.setVupdate(true);
        inputBean.setVviewreg(true);
        inputBean.setVconfirm(true);
        inputBean.setVreject(true);
        inputBean.setVgenerate(true);
        inputBean.setVgenerateview(true);
        inputBean.setVpinreset(true);
        inputBean.setVlimitadd(true);
        inputBean.setVlimitupdate(true);
        inputBean.setVlimitdelete(true);
        inputBean.setVprimaccupdate(true);
        inputBean.setVattemptreset(true);
        inputBean.setVdual(true);
        inputBean.setVpasswordreset(true);
        inputBean.setVconfeegenerate(true);
        inputBean.setVblock(true);
        inputBean.setVupdatecustomer(true);//T24 updated data

        if (tasklist != null && tasklist.size() > 0) {
            for (Task task : tasklist) {
                if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.SEARCH_TASK)) {
                    inputBean.setVsearch(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_TASK)) {
                    inputBean.setVdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CUSTOMER_STATUS_CHANGE_TASK)) {
                    inputBean.setVstatuschange(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CUSTOMER_UPDATE_TASK)) {
                    inputBean.setVupdate(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_CUSTOMER_TASK)) {
                    inputBean.setVviewcustomer(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CUSTOMER_LIMIT_CHANGE)) {
                    inputBean.setVlimitchange(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CONFIRM_TASK)) {
                    inputBean.setVconfirm(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.REJECT_TASK)) {
                    inputBean.setVreject(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.ADD_CUSTOMER_LIMIT)) {
                    inputBean.setVlimitadd(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.UPDATE_CUSTOMER_LIMIT)) {
                    inputBean.setVlimitupdate(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.DELETE_CUSTOMER_LIMIT)) {
                    inputBean.setVlimitdelete(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK)) {
                    inputBean.setVprimaccupdate(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CUSTOMER_ATTEMPTS_RESET)) {
                    inputBean.setVattemptreset(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CUS_BLOCK_APPLICATIONS)) {
                    inputBean.setVblock(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.GENERATE_TASK)) {
                    inputBean.setVgenerate(false);
                    inputBean.setVgenerateview(false);
                } else if (task.getTaskcode().equalsIgnoreCase(TaskVarList.CON_FEE_REPORT_GEN_TASK)) {
                    inputBean.setVconfeegenerate(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.VIEW_PENDING_TASK)) {
                    inputBean.setVdual(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CUSTOMER_PAS_RESET_TASK)) {
                    inputBean.setVpasswordreset(false);
                } else if (task.getTaskcode().toString().equalsIgnoreCase(TaskVarList.CUSTOMER_DATA_UPDATE_TASK)) {
                    inputBean.setVupdatecustomer(false);
                }

            }
        }
    }

    public boolean checkAccess(String method, String userRole) {
        boolean status = false;
        String page = PageVarList.CUSTOMER_SEARCH;
        String task = null;
        if ("view".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("list".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("listDevice".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("listDual".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("findDevice".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("detail".equals(method)) {
            task = TaskVarList.CUSTOMER_STATUS_CHANGE_TASK;
        } else if ("detailview".equals(method)) {
            task = TaskVarList.VIEW_CUSTOMER_TASK;
        } else if ("delete".equals(method)) {
            task = TaskVarList.DELETE_TASK;
        } else if ("viewLimitChange".equals(method)) {
            task = TaskVarList.CUSTOMER_LIMIT_CHANGE;
        } else if ("findTxnLimitGlobal".equals(method)) {
            task = TaskVarList.CUSTOMER_LIMIT_CHANGE;
        } else if ("findTxnLimit".equals(method)) {
            task = TaskVarList.CUSTOMER_LIMIT_CHANGE;
        } else if ("AddLimit".equals(method)) {
            task = TaskVarList.CUSTOMER_LIMIT_CHANGE;
        } else if ("UpdateLimit".equals(method)) {
            task = TaskVarList.CUSTOMER_LIMIT_CHANGE;
        } else if ("deleteLimit".equals(method)) {
            task = TaskVarList.CUSTOMER_LIMIT_CHANGE;
        } else if ("deviceDetailUpdate".equals(method)) {
            task = TaskVarList.CUSTOMER_UPDATE_TASK;
        } else if ("UpdateDeviceStatus".equals(method)) {
            task = TaskVarList.CUSTOMER_UPDATE_TASK;
        } else if ("primaryAccChange".equals(method)) {
            task = TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK;
        } else if ("cusAccountInq".equals(method)) {
            task = TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK;
        } else if ("updateAccount".equals(method)) {
            task = TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK;
        } else if ("UpdateCustomerStatus".equals(method)) {
            task = TaskVarList.CUSTOMER_STATUS_CHANGE_TASK;
        } else if ("confirm".equals(method)) {
            task = TaskVarList.CONFIRM_TASK;
        } else if ("reject".equals(method)) {
            task = TaskVarList.REJECT_TASK;
        } else if ("reportGenerate".equals(method)) {
            task = TaskVarList.GENERATE_TASK;
        } else if ("convenienceFeeGenerate".equals(method)) {
            task = TaskVarList.CON_FEE_REPORT_GEN_TASK;
        } else if ("findCustomerStatus".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("attemptReset".equals(method)) {
            task = TaskVarList.CUSTOMER_ATTEMPTS_RESET;
        } else if ("listLimit".equals(method)) {
            task = TaskVarList.VIEW_TASK;
        } else if ("viewPend".equals(method)) {
            task = TaskVarList.VIEW_PENDING_TASK;
        } else if ("passwordReset".equals(method)) {
            task = TaskVarList.CUSTOMER_PAS_RESET_TASK;
        } else if ("customerDetail".equals(method)) {
            task = TaskVarList.CUSTOMER_DATA_UPDATE_TASK;
        } else if ("customerInq".equals(method)) {
            task = TaskVarList.CUSTOMER_DATA_UPDATE_TASK;
        } else if ("updateCustomer".equals(method)) {
            task = TaskVarList.CUSTOMER_DATA_UPDATE_TASK;
        } else if ("blockApplications".equals(method)) {
            task = TaskVarList.CUS_BLOCK_APPLICATIONS;
        } else if ("viewBlocApp".equals(method)) {
            task = TaskVarList.CUS_BLOCK_APPLICATIONS;
        }

        if ("execute".equals(method)) {
            status = true;
        } else {
            HttpServletRequest request = ServletActionContext.getRequest();
            status = new Common().checkMethodAccess(task, page, userRole, request);
        }
        return status;
    }

    public String view() {

        System.out.println("called CustomerSearchAction :view");
        String result = "view";

        try {
            this.applyUserPrivileges();

            CommonDAO dao = new CommonDAO();
            inputBean.setStatusList(dao.getDefultStatusCusList(CommonVarList.STATUS_CATEGORY_CUSTOMER, CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setWaveOffStatusList(dao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_AUTH));
            inputBean.setSegmentTypeList(dao.getSegmentTypeList());
            inputBean.setOnBoardChannelList(this.getChannelTypeList());
            inputBean.setOnBoardTypeList(this.getOnBoardTypeList());
            inputBean.setDefaultAccountTypeList(this.getDefaultAccountTypeList());

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            if (session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD) != null && session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) != null) {
                if ((Integer) session.getAttribute(SessionVarlist.ONLY_SHOW_ONTIME) == 0) {
                    session.setAttribute(SessionVarlist.ONLY_SHOW_ONTIME, 1);
                    addActionError((String) session.getAttribute(SessionVarlist.MIN_PAS_CHANGE_PERIOD));
                }
            }

        } catch (Exception ex) {
            addActionError("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String list() {
        System.out.println("called CustomerSearchAction: List");
        try {
            if (inputBean.isSearch()) {
                int rows = inputBean.getRows();
                int page = inputBean.getPage();
                int to = (rows * page);
                int from = to - rows;
                long records = 0;
                String orderBy = "";

                if (!inputBean.getSidx().isEmpty()) {
                    orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
                } else {
                    orderBy = " order by U.CREATEDTIME DESC ";
                }

                CustomerSearchDAO dao = new CustomerSearchDAO();
                List<CustomerSearchBean> dataList = dao.getSearchList(inputBean, to, from, orderBy);

                /**
                 * for search audit
                 */
                if (inputBean.isSearch() && from == 0) {

                    HttpServletRequest request = ServletActionContext.getRequest();

                    String searchParameters = "["
                            + checkEmptyorNullString("From Date", inputBean.getFromDate())
                            + checkEmptyorNullString("To Date", inputBean.getToDate())
                            + checkEmptyorNullString("CID", inputBean.getCifSearch())
                            + checkEmptyorNullString("User Name", inputBean.getUsernameSearch())
                            + checkEmptyorNullString("NIC", inputBean.getNicSearch())
                            + checkEmptyorNullString("Status", inputBean.getStatusSearch())
                            + checkEmptyorNullString("First Name", inputBean.getFirstnameSearch())
                            + checkEmptyorNullString("Last Name", inputBean.getLastnameSearch())
                            + checkEmptyorNullString("Mobile", inputBean.getMobilenoSearch())
                            + checkEmptyorNullString("Email", inputBean.getEmailSearch())
                            + checkEmptyorNullString("Segment Type", inputBean.getSegmentTypeSearch())
                            + checkEmptyorNullString("Waive Off", inputBean.getWaveoffSearch())
                            + checkEmptyorNullString("Primary Type", inputBean.getDefTypeSearch())
                            + checkEmptyorNullString("Primary Acc/Card Number", inputBean.getDefAccNoSearch())
                            + checkEmptyorNullString("OnBoard Channel Type", inputBean.getOnBoardChannelSearch())
                            + checkEmptyorNullString("OnBoard Type", inputBean.getOnBoardTypeSearch())
                            + checkEmptyorNullString("Customer Category", inputBean.getCustomerCategorySearch())
                            + "]";

                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.SEARCH_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Customer management search using " + searchParameters + " parameters ", null);
                    SystemAuditDAO sysdao = new SystemAuditDAO();
                    sysdao.saveAudit(audit);
                }

                if (!dataList.isEmpty()) {
                    records = dataList.get(0).getFullCount();
                    inputBean.setRecords(records);
                    inputBean.setGridModel(dataList);
                    int total = (int) Math.ceil((double) records / (double) rows);
                    inputBean.setTotal(total);

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    session.setAttribute(SessionVarlist.CUSTOMERSEARCH_SEARCHBEAN, inputBean);
                } else {
                    inputBean.setRecords(0L);
                    inputBean.setTotal(0);
                }
            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " customer");
        }
        return "list";
    }

    public String listDual() {
        System.out.println("called CustomerSearchAction: listDual");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";

            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            }

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);
            inputBean.setCurrentUser(sysUser.getUsername());

            CustomerSearchDAO dao = new CustomerSearchDAO();
            List<CustomerSearchPendBean> dataList = dao.getSearchPendList(inputBean, rows, from, orderBy);

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelPend(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " customer");
        }
        return "listdual";
    }

    public String listDevice() {
        System.out.println("called CustomerSearchAction: listDevice");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";

            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            } else {
                orderBy = " order by u.createdTime DESC ";
            }

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

            CustomerSearchDAO dao = new CustomerSearchDAO();
            List<CustomerSearchBean> dataList = dao.getDeviceList(inputBean, to, from, orderBy);

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelDevice(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);
            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " customer");
        }
        return "list";
    }

    public String deviceDetailUpdate() {

        System.out.println("called CustomerSearchAction :detailUpdate");
        String result = "devicedetailupdate";

        try {

            CustomerSearchDAO dao = new CustomerSearchDAO();
            CommonDAO cdao = new CommonDAO();

            SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());

            inputBean.setStatusList(cdao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            try {
                inputBean.setCif(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCif("--");
            }

            try {
                inputBean.setNic(mobUser.getNic().toString());
            } catch (NullPointerException e) {
                inputBean.setNic("--");
            }

            inputBean.setUserid(inputBean.getUserid());

        } catch (Exception ex) {
            inputBean.setMessage("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detail() {

        System.out.println("called CustomerSearchAction :detail");
        String result = "detail";

        try {
            this.applyUserPrivileges();
            CustomerSearchDAO dao = new CustomerSearchDAO();
            CommonDAO comdao = new CommonDAO();

            inputBean.setCusStatusList(comdao.getStatusLIstByCategoryCode(CommonVarList.STATUS_CATEGORY_GENERAL));
            inputBean.setWaveOffStatusList(comdao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_AUTH));

            SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());
            Date date = dao.getLastLoginDateByUserID(inputBean.getUserid());

            try {
                inputBean.setLastlogindate(date.toString().substring(0, 19));
            } catch (NullPointerException e) {
                inputBean.setLastlogindate("--");
            }

            try {
                inputBean.setId(mobUser.getId().toString());
            } catch (NullPointerException e) {
                inputBean.setId("--");
            }
            try {
                inputBean.setDaonDeviceId(mobUser.getDaonDeviceId().toString());
            } catch (NullPointerException e) {
                inputBean.setDaonDeviceId("--");
            }
            try {
                inputBean.setNic(mobUser.getNic().toString());
            } catch (NullPointerException e) {
                inputBean.setNic("--");
            }

            try {
                inputBean.setDob(mobUser.getDob().toString());
            } catch (NullPointerException e) {
                inputBean.setDob("--");
            }
            try {
                inputBean.setCif(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCif("--");
            }
            try {
                inputBean.setCifhidden(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCifhidden("--");
            }

            try {
                inputBean.setUsername(mobUser.getUsername().toString());
            } catch (NullPointerException e) {
                inputBean.setUsername("--");
            }

            try {
                inputBean.setEmail(mobUser.getEmail().toString());
            } catch (NullPointerException e) {
                inputBean.setEmail("--");
            }
            try {
                inputBean.setMobileNumber(mobUser.getMobileNumber().toString());
            } catch (NullPointerException e) {
                inputBean.setMobileNumber("--");
            }
            try {
                inputBean.setLoginAttempts(mobUser.getLoginAttempts().toString());
                inputBean.setLoginAttemptStatus(dao.getUserBlocktatusInAction(mobUser.getLoginAttempts().toString()));
            } catch (NullPointerException e) {
                inputBean.setLoginAttempts("--");
                inputBean.setLoginAttemptStatus("--");
            }
            try {
                inputBean.setLastupdatedtime(mobUser.getLastupdatedtime().toString());
            } catch (NullPointerException e) {
                inputBean.setLastupdatedtime("--");
            }
            try {
                inputBean.setCreatedtime(mobUser.getCreatedtime().toString());
            } catch (NullPointerException e) {
                inputBean.setCreatedtime("--");
            }

            try {
                inputBean.setStatus(mobUser.getStatus().getStatuscode());
            } catch (NullPointerException e) {
                inputBean.setStatus("--");
            }
            try {
                inputBean.setUserTraceNumber(mobUser.getUserTraceNumber().toString());
            } catch (NullPointerException e) {
                inputBean.setUserTraceNumber("--");
            }
            try {
                inputBean.setSecondaryEmail(mobUser.getSecondaryEmail().toString());
            } catch (NullPointerException e) {
                inputBean.setSecondaryEmail("--");
            }
            try {
                inputBean.setSecondaryMobile(mobUser.getSecondaryMobile().toString());
            } catch (NullPointerException e) {
                inputBean.setSecondaryMobile("--");
            }
            try {
                inputBean.setPermanentAdd(mobUser.getPermanentAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setPermanentAdd("--");
            }
            try {
                inputBean.setCorrespondenceAdd(mobUser.getCorrespondenceAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setCorrespondenceAdd("--");
            }
            try {
                inputBean.setOfficeAdd(mobUser.getOfficeAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setOfficeAdd("--");
            }
            try {
                inputBean.setGender(mobUser.getGender().toString());
            } catch (NullPointerException e) {
                inputBean.setGender("--");
            }
            try {
                inputBean.setCustomerName(mobUser.getCustomerName().toString());
            } catch (NullPointerException e) {
                inputBean.setCustomerName("--");
            }
            try {
                inputBean.setCusSegmentType(mobUser.getSegmentType().getDescription().toString());
            } catch (Exception e) {
                inputBean.setCusSegmentType("--");
            }
            try {
                inputBean.setOnBoardChannel(dao.getChannelTypeDesByCode(mobUser.getOnBoardChannel().toString()));
            } catch (Exception e) {
                inputBean.setOnBoardChannel("--");
            }
            try {
                inputBean.setOnBoardType(dao.getOnBordedTypeDesByCode(mobUser.getOnBoardType().toString()));
            } catch (Exception e) {
                inputBean.setOnBoardType("--");
            }
            try {
                inputBean.setCustomerCategory(mobUser.getCustomerCategory().toString());
            } catch (Exception e) {
                inputBean.setCustomerCategory("--");
            }
            try {
                inputBean.setLoginStatus(dao.getLoginStatusDesByCode(mobUser.getFirstTimeLoginExpected().toString()));
            } catch (Exception e) {
                inputBean.setLoginStatus("--");
            }
            try {
                inputBean.setDefType(dao.getDefaultTypeDesByCode(mobUser.getDefaultAccType().toString()));
            } catch (Exception e) {
                inputBean.setDefType("--");
            }
            try {
                inputBean.setDefAccNo(mobUser.getDefaultAccNo().toString());
            } catch (Exception e) {
                inputBean.setDefAccNo("--");
            }
            try {
                inputBean.setFinalFeeDudDay(mobUser.getFinalFeeDedYear().toString().substring(0, 19));
            } catch (Exception e) {
                inputBean.setFinalFeeDudDay("--");
            }
            try {
                inputBean.setLastPassUpdateTime(mobUser.getLastPasswordUpdatedDate().toString().substring(0, 19));
            } catch (Exception e) {
                inputBean.setLastPassUpdateTime("--");
            }
            try {
                inputBean.setAccountofficer(mobUser.getActofficer().toString());
            } catch (Exception e) {
                inputBean.setAccountofficer("--");
            }
            try {
                inputBean.setRemark(mobUser.getRemark().toString());
            } catch (Exception e) {
                inputBean.setRemark("--");
            }
            try {
                inputBean.setCreatedtime(mobUser.getCreatedtime().toString().substring(0, 19));
            } catch (Exception e) {
                inputBean.setCreatedtime("--");
            }
            try {
                inputBean.setLastupdatedtime(mobUser.getLastupdatedtime().toString().substring(0, 19));
            } catch (Exception e) {
                inputBean.setLastupdatedtime("--");
            }
            try {
                inputBean.setChecker(mobUser.getChecker().toString());
            } catch (Exception e) {
                inputBean.setChecker("--");
            }
            try {
                inputBean.setMaker(mobUser.getMaker().toString());
            } catch (Exception e) {
                inputBean.setMaker("--");
            }
            try {
                if ((mobUser.getUsername() == null || mobUser.getUsername().toString().isEmpty())
                        && mobUser.getStatus().getStatuscode().equals(CommonVarList.STATUS_ACTIVE)) {
                    inputBean.setStatus(CommonVarList.CUSTOMER_PEND_STATUS);
                } else {
                    inputBean.setStatus(mobUser.getStatus().getStatuscode().toString());
                }
//                    inputBean.setStatus(mobUser.getStatus().getStatuscode().toString());

//                    String statusCategory = mobUser.getStatus().getStatuscategory().getCategorycode();
                String statusCategory = comdao.getStatusCategoryCodeByStatusCode(inputBean.getStatus());
                inputBean.setCusStatusList(comdao.getStatusLIstByCategoryCode(statusCategory));
                inputBean.setStatusCategory(statusCategory);
            } catch (Exception e) {
                //                inputBean.setCusStatusList(comdao.getStatusLIstByCategoryCode(mobUser.getStatus().getStatuscategory().getCategorycode()));
            }
            try {
                inputBean.setWaveoff(mobUser.getWaveoff().getStatuscode());
            } catch (Exception e) {
            }
            try {
                inputBean.setChargeAmount(mobUser.getChargeAmount().toString());
            } catch (Exception e) {
            }

        } catch (Exception ex) {
            inputBean.setMessage("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String detailview() {

        System.out.println("called CustomerSearchAction :detailview");
        String result = "detailview";

        try {
            this.applyUserPrivileges();
            CustomerSearchDAO dao = new CustomerSearchDAO();

            SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());
            Date date = dao.getLastLoginDateByUserID(inputBean.getUserid());

            try {
                inputBean.setLastlogindate(date.toString().substring(0, 19));
            } catch (NullPointerException e) {
                inputBean.setLastlogindate("--");
            }

            try {
                inputBean.setId(mobUser.getId().toString());
            } catch (NullPointerException e) {
                inputBean.setId("--");
            }
            try {
                inputBean.setDaonDeviceId(mobUser.getDaonDeviceId().toString());
            } catch (NullPointerException e) {
                inputBean.setDaonDeviceId("--");
            }
            try {
                inputBean.setNic(mobUser.getNic().toString());
            } catch (NullPointerException e) {
                inputBean.setNic("--");
            }

            try {
                inputBean.setDob(mobUser.getDob().toString());
            } catch (NullPointerException e) {
                inputBean.setDob("--");
            }
            try {
                inputBean.setCif(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCif("--");
            }
            try {
                inputBean.setCifhidden(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCifhidden("--");
            }

            try {
                inputBean.setUsername(mobUser.getUsername().toString());
            } catch (NullPointerException e) {
                inputBean.setUsername("--");
            }

            try {
                inputBean.setEmail(mobUser.getEmail().toString());
            } catch (NullPointerException e) {
                inputBean.setEmail("--");
            }
            try {
                inputBean.setMobileNumber(mobUser.getMobileNumber().toString());
            } catch (NullPointerException e) {
                inputBean.setMobileNumber("--");
            }
            try {
                inputBean.setLoginAttempts(mobUser.getLoginAttempts().toString());
                inputBean.setLoginAttemptStatus(dao.getUserBlocktatusInAction(mobUser.getLoginAttempts().toString()));
            } catch (NullPointerException e) {
                inputBean.setLoginAttempts("--");
                inputBean.setLoginAttemptStatus("--");
            }
            try {
                inputBean.setLastupdatedtime(mobUser.getLastupdatedtime().toString());
            } catch (NullPointerException e) {
                inputBean.setLastupdatedtime("--");
            }
            try {
                inputBean.setCreatedtime(mobUser.getCreatedtime().toString());
            } catch (NullPointerException e) {
                inputBean.setCreatedtime("--");
            }

            try {
                if ((mobUser.getUsername() == null || mobUser.getUsername().toString().isEmpty())
                        && mobUser.getStatus().getStatuscode().equals(CommonVarList.STATUS_ACTIVE)) {
                    inputBean.setStatus(CommonVarList.CUSTOMER_PEND_STATUS_DES);
                } else {
                    inputBean.setStatus(mobUser.getStatus().getDescription());
                }
            } catch (NullPointerException e) {
                inputBean.setStatus("--");
            }

            try {
                inputBean.setUserTraceNumber(mobUser.getUserTraceNumber().toString());
            } catch (NullPointerException e) {
                inputBean.setUserTraceNumber("--");
            }
            try {
                inputBean.setSecondaryEmail(mobUser.getSecondaryEmail().toString());
            } catch (NullPointerException e) {
                inputBean.setSecondaryEmail("--");
            }
            try {
                inputBean.setSecondaryMobile(mobUser.getSecondaryMobile().toString());
            } catch (NullPointerException e) {
                inputBean.setSecondaryMobile("--");
            }
            try {
                inputBean.setPermanentAdd(mobUser.getPermanentAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setPermanentAdd("--");
            }
            try {
                inputBean.setCorrespondenceAdd(mobUser.getCorrespondenceAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setCorrespondenceAdd("--");
            }
            try {
                inputBean.setOfficeAdd(mobUser.getOfficeAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setOfficeAdd("--");
            }
            try {
                inputBean.setGender(mobUser.getGender().toString());
            } catch (NullPointerException e) {
                inputBean.setGender("--");
            }
            try {
                inputBean.setCustomerName(mobUser.getCustomerName().toString());
            } catch (NullPointerException e) {
                inputBean.setCustomerName("--");
            }
            try {
                inputBean.setCusSegmentType(mobUser.getSegmentType().getDescription().toString());
            } catch (Exception e) {
                inputBean.setCusSegmentType("--");
            }
            try {
                inputBean.setOnBoardChannel(dao.getChannelTypeDesByCode(mobUser.getOnBoardChannel().toString()));
            } catch (Exception e) {
                inputBean.setOnBoardChannel("--");
            }
            try {
                inputBean.setOnBoardType(dao.getOnBordedTypeDesByCode(mobUser.getOnBoardType().toString()));
            } catch (Exception e) {
                inputBean.setOnBoardType("--");
            }
            try {
                inputBean.setCustomerCategory(mobUser.getCustomerCategory().toString());
            } catch (Exception e) {
                inputBean.setCustomerCategory("--");
            }
            try {
                inputBean.setLoginStatus(dao.getLoginStatusDesByCode(mobUser.getFirstTimeLoginExpected().toString()));
            } catch (Exception e) {
                inputBean.setLoginStatus("--");
            }
            try {
                inputBean.setDefType(dao.getDefaultTypeDesByCode(mobUser.getDefaultAccType().toString()));
            } catch (Exception e) {
                inputBean.setDefType("--");
            }
            try {
                inputBean.setDefAccNo(mobUser.getDefaultAccNo().toString());
            } catch (Exception e) {
                inputBean.setDefAccNo("--");
            }
            try {
                inputBean.setFinalFeeDudDay(mobUser.getFinalFeeDedYear().toString().substring(0, 19));
            } catch (Exception e) {
                inputBean.setFinalFeeDudDay("--");
            }
            try {
                inputBean.setLastPassUpdateTime(mobUser.getLastPasswordUpdatedDate().toString().substring(0, 19));
            } catch (Exception e) {
                inputBean.setLastPassUpdateTime("--");
            }
            try {
                inputBean.setAccountofficer(mobUser.getActofficer().toString());
            } catch (Exception e) {
                inputBean.setAccountofficer("--");
            }
            try {
                inputBean.setRemark(mobUser.getRemark().toString());
            } catch (Exception e) {
                inputBean.setRemark("--");
            }
            try {
                inputBean.setCreatedtime(mobUser.getCreatedtime().toString().substring(0, 19));
            } catch (Exception e) {
                inputBean.setCreatedtime("--");
            }
            try {
                inputBean.setLastupdatedtime(mobUser.getLastupdatedtime().toString().substring(0, 19));
            } catch (Exception e) {
                inputBean.setLastupdatedtime("--");
            }
            try {
                inputBean.setChecker(mobUser.getChecker().toString());
            } catch (Exception e) {
                inputBean.setChecker("--");
            }
            try {
                inputBean.setMaker(mobUser.getMaker().toString());
            } catch (Exception e) {
                inputBean.setMaker("--");
            }
            try {
                inputBean.setWaveoff(mobUser.getWaveoff().getDescription());
            } catch (Exception e) {
                inputBean.setWaveoff("--");
            }
            try {
                inputBean.setChargeAmount(mobUser.getChargeAmount().toString());
            } catch (Exception e) {
                inputBean.setChargeAmount("--");
            }

        } catch (Exception ex) {
            inputBean.setMessage("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String primaryAccChange() {

        System.out.println("called CustomerSearchAction :primaryAccChange");
        String result = "primaryaccchange";

        try {
            this.applyUserPrivileges();
            CustomerSearchDAO dao = new CustomerSearchDAO();
            SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());

            inputBean.setDefaultAccountTypeList(this.getDefaultAccountTypeList());
            try {
                inputBean.setCif(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCif("--");
            }

            try {
                inputBean.setUsername(mobUser.getUsername().toString());
            } catch (NullPointerException e) {
                inputBean.setUsername("--");
            }

            try {
                inputBean.setDefaultAccountOld(mobUser.getDefaultAccNo().toString());
            } catch (NullPointerException e) {
                inputBean.setDefaultAccountOld("--");
            }

        } catch (Exception ex) {
            inputBean.setMessage("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String viewBlocApp() {

        System.out.println("called CustomerSearchAction :viewBlocApp");
        String result = "applicationstatus";

        try {

//            this.applyUserPrivileges();
            CommonDAO Cdao = new CommonDAO();
            inputBean.setStatusList(Cdao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            CustomerSearchDAO dao = new CustomerSearchDAO();
            SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());

            inputBean.setUserid(inputBean.getUserid());

            try {
                inputBean.setCif(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCif("--");
            }

            try {
                inputBean.setUsername(mobUser.getUsername().toString());
            } catch (NullPointerException e) {
                inputBean.setUsername("--");
            }

            try {
                inputBean.setStatus(mobUser.getStatus().getStatuscode().toString());
            } catch (NullPointerException e) {
                inputBean.setStatus("--");
            }
            try {
                inputBean.setStatusByMbStatus(mobUser.getStatusByMbStatus().getStatuscode().toString());
            } catch (NullPointerException e) {
                inputBean.setStatusByMbStatus("--");
            }
            try {
                inputBean.setStatusByIbStatus(mobUser.getStatusByIbStatus().getStatuscode().toString());
            } catch (NullPointerException e) {
                inputBean.setStatusByIbStatus("--");
            }

        } catch (Exception ex) {
            inputBean.setMessage("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String customerDetail() {

        System.out.println("called CustomerSearchAction :detailCustomerData");
        String result = "detailCustomerData";

        try {
            this.applyUserPrivileges();
            CustomerSearchDAO dao = new CustomerSearchDAO();
            SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());

            try {
                inputBean.setCif(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCif("--");
            }
            try {
                inputBean.setUsername(mobUser.getUsername().toString());
            } catch (NullPointerException e) {
                inputBean.setUsername("--");
            }

        } catch (Exception ex) {
            inputBean.setMessage("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String findCustomerStatus() {

        System.out.println("called findCustomerStatus :find");
        String result = "findcustomerstatus";

        try {
            CommonDAO comdao = new CommonDAO();
            CustomerSearchDAO dao = new CustomerSearchDAO();

            SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());

            try {
                inputBean.setNic(mobUser.getNic().toString());
            } catch (NullPointerException e) {
                inputBean.setNic("--");
            }

            try {
                inputBean.setDob(mobUser.getDob().toString());
            } catch (NullPointerException e) {
                inputBean.setDob("--");
            }
            try {
                inputBean.setCif(mobUser.getCif().toString());
            } catch (NullPointerException e) {
                inputBean.setCif("--");
            }

            try {
                inputBean.setUsername(mobUser.getUsername().toString());
            } catch (NullPointerException e) {
                inputBean.setUsername("--");
            }

            try {
                inputBean.setEmail(mobUser.getEmail().toString());
            } catch (NullPointerException e) {
                inputBean.setEmail("--");
            }
            try {
                inputBean.setMobileNumber(mobUser.getMobileNumber().toString());
            } catch (NullPointerException e) {
                inputBean.setMobileNumber("--");
            }
            try {
                inputBean.setLoginAttempts(mobUser.getLoginAttempts().toString());
            } catch (NullPointerException e) {
                inputBean.setLoginAttempts("--");
            }
            try {
                inputBean.setLastupdatedtime(mobUser.getLastupdatedtime().toString());
            } catch (NullPointerException e) {
                inputBean.setLastupdatedtime("--");
            }
            try {
                inputBean.setCreatedtime(mobUser.getCreatedtime().toString());
            } catch (NullPointerException e) {
                inputBean.setCreatedtime("--");
            }

            try {
                inputBean.setStatus(mobUser.getStatus().getStatuscode());
            } catch (NullPointerException e) {
                inputBean.setStatus("--");
            }

            try {
                inputBean.setCusSegmentType(mobUser.getSegmentType().getSegmentcode());
            } catch (NullPointerException e) {
                inputBean.setCusSegmentType("--");
            }
            try {
                inputBean.setUserTraceNumber(mobUser.getUserTraceNumber().toString());
            } catch (NullPointerException e) {
                inputBean.setUserTraceNumber("--");
            }
            try {
                inputBean.setSecondaryEmail(mobUser.getSecondaryEmail().toString());
            } catch (NullPointerException e) {
                inputBean.setSecondaryEmail("--");
            }
            try {
                inputBean.setSecondaryMobile(mobUser.getSecondaryMobile().toString());
            } catch (NullPointerException e) {
                inputBean.setSecondaryMobile("--");
            }
            try {
                inputBean.setPermanentAdd(mobUser.getPermanentAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setPermanentAdd("--");
            }
            try {
                inputBean.setCorrespondenceAdd(mobUser.getCorrespondenceAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setCorrespondenceAdd("--");
            }
            try {
                inputBean.setOfficeAdd(mobUser.getOfficeAddress().toString());
            } catch (NullPointerException e) {
                inputBean.setOfficeAdd("--");
            }
            try {
                inputBean.setGender(mobUser.getGender().toString());
            } catch (NullPointerException e) {
                inputBean.setGender("--");
            }
            try {
                if ((mobUser.getUsername() == null || mobUser.getUsername().toString().isEmpty())
                        && mobUser.getStatus().getStatuscode().equals(CommonVarList.STATUS_ACTIVE)) {
                    inputBean.setStatus(CommonVarList.CUSTOMER_PEND_STATUS);
                } else {
                    inputBean.setStatus(mobUser.getStatus().getStatuscode().toString());
                }
                String statusCategory = comdao.getStatusCategoryCodeByStatusCode(inputBean.getStatus());
                inputBean.setStatusCategory(statusCategory);
            } catch (NullPointerException e) {
//                inputBean.setCusStatusList(comdao.getStatusLIstByCategoryCode(mobUser.getStatus().getStatuscategory().getCategorycode()));
            }
            try {
                inputBean.setWaveoff(mobUser.getWaveoff().getStatuscode());
            } catch (Exception e) {
            }
            try {
                inputBean.setChargeAmount(mobUser.getChargeAmount().toString());
            } catch (Exception e) {
            }
            try {
                inputBean.setLastlogindate(mobUser.getLastupdatedtime().toString().substring(0, 19));
            } catch (NullPointerException e) {
                inputBean.setLastlogindate("--");
            }
            try {
                inputBean.setStatusByMbStatus(mobUser.getStatusByMbStatus().getStatuscode());
            } catch (NullPointerException e) {
                inputBean.setStatusByMbStatus("--");
            }
            try {
                inputBean.setStatusByIbStatus(mobUser.getStatusByIbStatus().getStatuscode());
            } catch (NullPointerException e) {
                inputBean.setStatusByIbStatus("--");
            }

        } catch (Exception ex) {
            inputBean.setMessage("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger
                    .getLogger(CustomerSearchAction.class
                            .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String UpdateCustomerStatus() {

        System.out.println("called CustomerSearchAction :updateCustomerStatus");
        String result = "message";
        String message = "";

        try {

            HttpServletRequest request = ServletActionContext.getRequest();

            if (inputBean.getUserid() != null || !inputBean.getUserid().isEmpty()) {

                message = this.validateUpdateCustomerStatusInputs();
                if (message.isEmpty()) {

                    CustomerSearchDAO dao = new CustomerSearchDAO();

                    String newV = inputBean.getUserid() + "|"
                            + inputBean.getStatus() + "|"
                            + inputBean.getWaveoff() + "|"
                            + inputBean.getChargeAmount();

                    //for oldvalue
                    SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());
                    String chargeAmountOld ="";
                    if(mobUser.getChargeAmount()!=null){
                        chargeAmountOld= mobUser.getChargeAmount().toString();
                    }
                    String oldV = inputBean.getUserid() + "|"
                            + mobUser.getStatus().getStatuscode() + "|"
                            + mobUser.getWaveoff().getStatuscode() + "|"
                            + chargeAmountOld;

                    if (!newV.equals(oldV)) {
                        String newValWithActState = inputBean.getUserid() + "|"
                                + CommonVarList.STATUS_ACTIVE + "|"
                                + inputBean.getWaveoff() + "|"
                                + inputBean.getChargeAmount();
                        if (inputBean.getStatus().equals(CommonVarList.STATUS_ACTIVE) || (inputBean.getStatus().equals(CommonVarList.STATUS_DEACTIVE) && oldV.equals(newValWithActState))) {

                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUSTOMER_STATUS_CHANGE_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Requested to update customer (CID : '" + mobUser.getCif() + "') ", null, oldV, newV);

                            message = dao.updateCustomerStatus(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " customer ");
                            } else {
                                addActionError(message);
                            }
                        } else {
                            addActionError(MessageVarList.COMMON_INACTIVE_RECORD_DETAIL_UPDATE);
                        }
                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            } else {
                addActionError("Customer ID cannot be empty.");
            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Customer search action " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return result;
    }

    public String blockApplications() {

        System.out.println("called CustomerSearchAction : blocApplications");
        String result = "message";
        String message = "";

        try {

            HttpServletRequest request = ServletActionContext.getRequest();

            if (inputBean.getUserid() != null || !inputBean.getUserid().isEmpty()) {

                message = this.validateBlockAppicationInputs();

                if (message.isEmpty()) {

                    CustomerSearchDAO dao = new CustomerSearchDAO();

                    String newV = inputBean.getUserid() + "|"
                            + inputBean.getStatusByMbStatus() + "|"
                            + inputBean.getStatusByIbStatus();

                    //for oldvalue
                    SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());
                    String oldV = inputBean.getUserid() + "|"
                            + mobUser.getStatusByMbStatus().getStatuscode() + "|"
                            + mobUser.getStatusByIbStatus().getStatuscode();

                    if (!newV.equals(oldV)) {

                        Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUS_BLOCK_APPLICATIONS, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Requested to change applications status of customer (CID : '" + mobUser.getCif() + "') ", null, oldV, newV);

                        message = dao.updateCustomerAppStatus(inputBean, audit);

                        if (message.isEmpty()) {
                            addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " customer ");
                        } else {
                            addActionError(message);
                        }

                    } else {
                        addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                    }

                } else {
                    addActionError(message);
                }
            } else {
                addActionError("Customer ID cannot be empty.");
            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Customer search action " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return result;
    }

    public String attemptReset() {

        System.out.println("called CustomerSearchAction :attemptRest");
        String result = "resetattempts";
        String message = "";

        try {

            HttpServletRequest request = ServletActionContext.getRequest();

            if (inputBean.getUserid() != null || !inputBean.getUserid().isEmpty()) {

                CustomerSearchDAO dao = new CustomerSearchDAO();
                //for CID
                SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());

                String newV = inputBean.getUserid() + "|0";
                String oldV = inputBean.getUserid();

//                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUSTOMER_ATTEMPTS_RESET, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Requested to reset attempts of customer (CID : '" + mobUser.getCif() + "')", null, oldV, newV);
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUSTOMER_ATTEMPTS_RESET, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Reset attempts of customer (CID : '" + mobUser.getCif() + "')", null, oldV, newV);

                message = dao.resetCustomerAttempts(inputBean, audit);

                if (message.isEmpty()) {
//                    inputBean.setMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " customer attempts reset ");
//                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " customer attempts reset ");
                    inputBean.setMessage("Customer attempts has been reset successfully");
                    addActionMessage("Customer attempts has been reset successfully");
                } else {
                    inputBean.setMessage(message);
                    addActionError(message);
                }
            } else {
                inputBean.setMessage("Customer ID cannot be empty.");
                addActionError("Customer ID cannot be empty.");
            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Customer search action " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return result;
    }

    public String passwordReset() {

        System.out.println("called CustomerSearchAction :passwordReset");
        String result = "passwordreset";
        String message = "";

        try {

            HttpServletRequest request = ServletActionContext.getRequest();

            if (inputBean.getUserid() != null || !inputBean.getUserid().isEmpty()) {

                CustomerSearchDAO dao = new CustomerSearchDAO();

                //for CID
                SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());

                String newV = inputBean.getUserid();
                String oldV = inputBean.getUserid();

//                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUSTOMER_PAS_RESET_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Requested to reset password of customer (CID : '" + mobUser.getCif() + "')", null, oldV, newV);
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUSTOMER_PAS_RESET_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Reset password of customer (CID : '" + mobUser.getCif() + "')", null, oldV, newV);

                message = dao.resetCustomerPassword(inputBean, audit);

                if (message.isEmpty()) {
//                    inputBean.setMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " customer password reset ");
//                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " customer password reset ");
                    inputBean.setMessage("Customer password has been reset successfully");
                    addActionMessage("Customer password has been reset successfully");
                } else {
                    inputBean.setMessage(message);
                    addActionError(message);
                }
            } else {
                inputBean.setMessage("Customer ID cannot be empty.");
                addActionError("Customer ID cannot be empty.");
            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError("Customer search action " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return result;
    }

    public String confirm() {
        System.out.println("called CustomerSearchAction : confirm");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateConfirm();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                CustomerSearchDAO dao = new CustomerSearchDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CONFIRM_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, " ", inputBean.getRemark());
                message = dao.confirmCustomer(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation approved successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage("Customer search action " + MessageVarList.COMMON_ERROR_CONFIRM);
        }
        return retType;
    }

    public String reject() {
        System.out.println("called CustomerSearchAction : reject");
        String message = null;
        String retType = "delete";
        try {
            message = this.validateReject();
            if (message.isEmpty()) {
                HttpServletRequest request = ServletActionContext.getRequest();
                CustomerSearchDAO dao = new CustomerSearchDAO();
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.REJECT_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, " ", inputBean.getRemark());
                message = dao.rejecteCustomer(inputBean, audit);
                if (message.isEmpty()) {
                    message = "Requested operation rejected successfully ";
                }
                inputBean.setMessage(message);
            } else {
                inputBean.setErrormessage(message);
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage("Customer search action " + MessageVarList.COMMON_ERROR_REJECT);
        }
        return retType;
    }

    private String validateUpdateCustomerStatusInputs() {
        String message = "";

        if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_STATUS;
        } else if (inputBean.getWaveoff() == null || inputBean.getWaveoff().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_WAVE_OFF_STATUS;
        } else if (inputBean.getChargeAmount() == null || inputBean.getChargeAmount().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_CHARGE_AMOUNT;
        } else if (!Validation.isAlsoCurrencyValue(inputBean.getChargeAmount())) {
            message = MessageVarList.CUSTOMER_SEARCH_INVALID_CHARGE_AMOUNT;
        }

        return message;
    }

    private String validateBlockAppicationInputs() {
        String message = "";

        if (inputBean.getStatusByMbStatus() == null || inputBean.getStatusByMbStatus().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_MOB_STATUS;
        } else if (inputBean.getStatusByIbStatus() == null || inputBean.getStatusByIbStatus().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_IB_STATUS;
        }
        return message;
    }

    private String validateConfirm() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    private String validateReject() {
        String message = "";
        if (inputBean.getRemark() == null || inputBean.getRemark().trim().isEmpty()) {
            message = MessageVarList.COMMON_EMPTY_REMARK;
        }

        return message;
    }

    public String findDevice() {

        System.out.println("called CustomerSearchAction :findDevice");

        String result = "finddevice";

        try {
            CustomerSearchDAO dao = new CustomerSearchDAO();

            SwtUserDevice device = dao.getDeviceByDeviceID(inputBean.getDeviceid());

            try {
                inputBean.setDeviceNickName(device.getDeviceNickName().toString());
            } catch (NullPointerException e) {
                inputBean.setDeviceNickName("--");
            }

            try {
                inputBean.setDeviceBuildNumber(device.getDeviceBuildNumber().toString());
            } catch (NullPointerException e) {
                inputBean.setDeviceBuildNumber("--");
            }
            try {
                inputBean.setDeviceManufacture(device.getDeviceManufacture().toString());
            } catch (NullPointerException e) {
                inputBean.setDeviceManufacture("--");
            }
            try {
                inputBean.setDeviceModel(device.getDeviceModel().toString());
            } catch (NullPointerException e) {
                inputBean.setDeviceModel("--");
            }

            try {
                inputBean.setStatus(device.getStatus().getStatuscode());
            } catch (NullPointerException e) {
                inputBean.setStatus("--");
            }

            try {
                inputBean.setFullname("--");
            } catch (NullPointerException e) {
                inputBean.setFullname("--");
            }

        } catch (Exception ex) {
            inputBean.setMessage("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger
                    .getLogger(CustomerSearchAction.class
                            .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String UpdateDeviceStatus() {

        System.out.println("called CustomerSearchAction : UpdateDeviceStatus");
        String retType = "message";

        try {

            System.out.println("Unique ID " + inputBean.getDeviceid());
            if (inputBean.getUserid() != null || !inputBean.getUserid().isEmpty()) {
                if (inputBean.getDeviceid() != null && !inputBean.getDeviceid().isEmpty()) {

                    String message = this.validateDeviceInputs();

                    if (message.isEmpty()) {

                        HttpServletRequest request = ServletActionContext.getRequest();

                        CustomerSearchDAO dao = new CustomerSearchDAO();

                        String newV = inputBean.getUserid() + "|"
                                + inputBean.getDeviceid() + "|"
                                + inputBean.getDevicestatus();

                        //for old value
                        SwtUserDevice device = dao.getDeviceByDeviceID(inputBean.getDeviceid());
                        String oldV = inputBean.getUserid() + "|"
                                + inputBean.getDeviceid() + "|"
                                + device.getStatus().getStatuscode();
                        if (!newV.equals(oldV)) {
                            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUSTOMER_UPDATE_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Requested to update status of customer device ID '" + inputBean.getDeviceid() + "' ", null, oldV, newV);

                            message = dao.updateDeviceStatus(inputBean, audit);

                            if (message.isEmpty()) {
                                addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + " device status ");
                            } else {
                                addActionError(message);
                            }
                        } else {
                            addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                        }
                    } else {
                        addActionError(message);
                    }
                } else {
                    addActionError("Device ID cannot be empty");
                }
            } else {
                addActionError("Customer ID cannot be empty.");
            }
        } catch (Exception ex) {
            Logger.getLogger(CustomerSearchAction.class
                    .getName()).log(Level.SEVERE, null, ex);
            addActionError("Customer SearchAction " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    private String validateDeviceInputs() {
        String message = "";

        if (inputBean.getDevicestatus() == null || inputBean.getDevicestatus().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_DEVICE_STATUS_EMPTY;
        }

        return message;
    }

    public String reportGenerate() {

        System.out.println("called CustomerSearchAction : reportGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("exel")) {
                System.err.println("EXEL printing");
                CustomerSearchDAO dao = new CustomerSearchDAO();
                retMsg = "excelreport";
                ByteArrayOutputStream outputStream = null;
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    CustomerSearchInputBean searchBean = (CustomerSearchInputBean) session.getAttribute(SessionVarlist.CUSTOMERSEARCH_SEARCHBEAN);
//                    Audittrace audittrace = Common.makeAudittrace(request, TaskVarList.REPORT_TASK, PageVarList.EXCEPTIONS_RPT_PAGE, this.getSearchParam() + " excel report viewed", null);
//                    Object object = new Object();
                    Object object = dao.generateExcelReport(searchBean);
                    if (object instanceof SXSSFWorkbook) {
                        SXSSFWorkbook workbook = (SXSSFWorkbook) object;
                        outputStream = new ByteArrayOutputStream();
                        workbook.write(outputStream);
                        inputBean.setExcelStream(new ByteArrayInputStream(outputStream.toByteArray()));
                        setContentLength(outputStream.toByteArray().length);
                        System.out.println("------" + outputStream.toByteArray());
                    } else if (object instanceof ByteArrayOutputStream) {
                        outputStream = (ByteArrayOutputStream) object;
                        inputBean.setZipStream(new ByteArrayInputStream(outputStream.toByteArray()));
                        retMsg = "zip";
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.SYSTEM_AUDIT_PAGE, SectionVarList.SYSTEM_AUDIT, "System audit excel report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
//                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail excel report");
                    Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
//                    this.loadPageData();
                    retMsg = "view";
                    throw e;
                } finally {
                    try {
                        if (outputStream != null) {
                            outputStream.flush();
                            outputStream.close();
                        }

                    } catch (IOException ex) {
                        //do nothing
                    }
                }
            } else if (inputBean.getReporttype().trim().equalsIgnoreCase("csv")) {

                CustomerSearchDAO dao = new CustomerSearchDAO();
                retMsg = "csvreport";
                StringBuffer sb = null;
                //PrintWriter pw = new PrintWriter(new File("txnexplorer_report.csv"));

//                HttpServletResponse response = ServletActionContext.getResponse();
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    CustomerSearchInputBean searchBean = (CustomerSearchInputBean) session.getAttribute(SessionVarlist.CUSTOMERSEARCH_SEARCHBEAN);
//                    Audittrace audittrace = Common.makeAudittrace(request, TaskVarList.REPORT_TASK, PageVarList.EXCEPTIONS_RPT_PAGE, this.getSearchParam() + " excel report viewed", null);
//                    Object object = new Object();
                    if (searchBean != null) {
                        sb = dao.makeCSVReport(searchBean);
                    } else {
                        sb = dao.makeCSVReport(new CustomerSearchInputBean());
                    }

//                    response.setContentType("text/csv");
//                    response.setHeader("Content-Disposition", "attachment; filename=\"Customer_Search_Report.csv\"");
                    try {
//                        OutputStream outputStream = response.getOutputStream();
//                        String outputResult = sb.toString();
//                        outputStream.write(outputResult.getBytes());
//                        outputStream.flush();
//                        outputStream.close();
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Customer_Search_Report.csv");
                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.GENERATE_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Customer search csv report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " exception detail csv report");
                    Logger
                            .getLogger(CustomerSearchAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Customer search");

            return "message";
        }
        return retMsg;
    }

    public String convenienceFeeGenerate() {

        System.out.println("called CustomerSearchAction : convenienceFeeGenerate");
//        Session hSession = null;
        String retMsg = "view";
        InputStream inputStream = null;
        try {
            if (inputBean.getReporttype().trim().equalsIgnoreCase("fee")) {

                CustomerSearchDAO dao = new CustomerSearchDAO();
                retMsg = "feereport";
                StringBuffer sb = null;
                //PrintWriter pw = new PrintWriter(new File("txnexplorer_report.csv"));

//                HttpServletResponse response = ServletActionContext.getResponse();
                try {

                    HttpSession session = ServletActionContext.getRequest().getSession(false);
                    CustomerSearchInputBean searchBean = (CustomerSearchInputBean) session.getAttribute(SessionVarlist.CUSTOMERSEARCH_SEARCHBEAN);
//                    Audittrace audittrace = Common.makeAudittrace(request, TaskVarList.REPORT_TASK, PageVarList.EXCEPTIONS_RPT_PAGE, this.getSearchParam() + " excel report viewed", null);
//                    Object object = new Object();
                    if (searchBean != null) {
                        sb = dao.makeFeeReport(searchBean);
                    } else {
                        sb = dao.makeFeeReport(new CustomerSearchInputBean());
                    }

//                    response.setContentType("text/csv");
//                    response.setHeader("Content-Disposition", "attachment; filename=\"Customer_Search_Report.csv\"");
                    try {
//                        OutputStream outputStream = response.getOutputStream();
//                        String outputResult = sb.toString();
//                        outputStream.write(outputResult.getBytes());
//                        outputStream.flush();
//                        outputStream.close();
                        inputStream = new ByteArrayInputStream(sb.toString().getBytes());
                        setInputStream(inputStream);
                        setFileName("Convenience_Fee_Report.csv");
                        setContentLength(sb.length());
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    }

                    HttpServletRequest request = ServletActionContext.getRequest();
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CON_FEE_REPORT_GEN_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, " Convenience fee report generated ", null);
                    CommonDAO.saveAudit(audit);

                } catch (Exception e) {
                    addActionError(MessageVarList.COMMON_ERROR_PROCESS + " convenience fee report generation");
                    Logger
                            .getLogger(CustomerSearchAction.class
                                    .getName()).log(Level.SEVERE, null, e);
                    retMsg = "view";
                    throw e;

                } finally {
                    try {
                        if (inputStream != null) {
                            inputStream.close();
                        }

                    } catch (Exception ex) {
                        //do nothing
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " Customer search");

            return "message";
        }
        return retMsg;
    }

    public String viewLimitChange() {

        System.out.println("called CustomerSearchAction :viewLimitChange");
        String result = "viewlimit";

        try {

            this.applyUserPrivileges();

            CommonDAO commondao = new CommonDAO();
            CustomerSearchDAO dao = new CustomerSearchDAO();
            inputBean.setTransactiontypeList(commondao.getTransfertypeListByStatus(CommonVarList.STATUS_ACTIVE));
            inputBean.setStatusList(commondao.getDefultStatusList(CommonVarList.STATUS_CATEGORY_GENERAL));

            if (inputBean.getUserid() != null && !inputBean.getUserid().isEmpty()) {

                inputBean.setCiflimit((dao.getCustomerByUserID(inputBean.getUserid())).getCif());
                try {
                    inputBean.setCusSegmentTypelimit((dao.getCustomerByUserID(inputBean.getUserid())).getSegmentType().getSegmentcode());
                    inputBean.setUserid(inputBean.getUserid());

                } catch (NullPointerException ee) {
                    addActionError("Customer Segment cannot be empty");
                }
            } else {
                addActionError("Unique ID cannot be empty");
            }

        } catch (Exception ex) {
            addActionError("Customer search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger
                    .getLogger(CustomerSearchAction.class
                            .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String listLimit() {

        System.out.println("called CustomerSearchAction: listLimit");
        try {

            int rows = inputBean.getRows();
            int page = inputBean.getPage();
            int to = (rows * page);
            int from = to - rows;
            long records = 0;
            String orderBy = "";

            if (!inputBean.getSidx().isEmpty()) {
                orderBy = " order by " + inputBean.getSidx() + " " + inputBean.getSord();
            } else {
                orderBy = " order by u.createdTime DESC ";
                CustomerSearchDAO dao = new CustomerSearchDAO();

                inputBean.setCusSegmentTypelimit((dao.getCustomerByUserID(inputBean.getUserid())).getSegmentType().getSegmentcode());
                inputBean.setUserid(inputBean.getUserid());

            }

            CustomerSearchDAO dao = new CustomerSearchDAO();
            List<CustomerSearchLimitBean> dataList = dao.mobUserLimitListByUserID(inputBean.getUserid(), rows, from);

            if (!dataList.isEmpty()) {
                records = dataList.get(0).getFullCount();
                inputBean.setRecords(records);
                inputBean.setGridModelLimit(dataList);
                int total = (int) Math.ceil((double) records / (double) rows);
                inputBean.setTotal(total);
            } else {
                inputBean.setRecords(0L);
                inputBean.setTotal(0);

            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class
                    .getName()).log(Level.SEVERE, null, e);
            addActionError(MessageVarList.COMMON_ERROR_PROCESS + " customer");
        }
        return "list";
    }

    public String findTxnLimitGlobal() {

        System.out.println("called CustomerSearchAction : findTxnLimitGlobal");

        try {
            if (inputBean.getTxntype() != null && !inputBean.getTxntype().isEmpty()) {

                CommonDAO cmd = new CommonDAO();

                TransactionLimit tranLimit = new TransactionLimit();

                System.out.println("trntype" + inputBean.getTxntype());
                System.out.println("segmenttype" + inputBean.getCusSegmentTypelimit());

                tranLimit = cmd.getTxnLimits(inputBean.getTxntype(), inputBean.getCusSegmentTypelimit());
                if (tranLimit != null) {
                    inputBean.setGlobalVal(tranLimit.getDefaultLimit().toString());
                } else {
                    inputBean.setMessage("Global limt not defined for selected user segment,transaction type combination ");
                }
            }
        } catch (Exception ex) {
            inputBean.setMessage("Customer search action  " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger
                    .getLogger(CustomerSearchAction.class
                            .getName()).log(Level.SEVERE, null, ex);
        }

        return "findlimit";
    }

    public String AddLimit() {
        System.out.println("called CustomerSearchAction : AddLimit");
        String result = "message";

        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            CustomerSearchDAO dao = new CustomerSearchDAO();

            String message = this.validateLimitInputs();

            if (message.isEmpty()) {

                String newV = inputBean.getUserid() + "|"
                        + inputBean.getTxntypelimit() + "|"
                        + inputBean.getUserLimit() + "|"
                        + inputBean.getStatuslimit() + "|";

                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.ADD_CUSTOMER_LIMIT, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Transaction type limit " + inputBean.getTxntypelimit() + " added", null, null, newV);

                message = dao.insertUseTxnLimit(inputBean, audit);

                if (message.isEmpty()) {
//                    inputBean.setMessagelimit("Transaction wise limit " + MessageVarList.COMMON_SUCC_INSERT);
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_ADD_TASK_PENDING + "transaction wise limit ");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(message);
            }

        } catch (Exception ex) {
            addActionError("CustomerSearch " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger
                    .getLogger(CustomerSearchAction.class
                            .getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String UpdateLimit() {

        System.out.println("called CustomerSearchAction : Update");
        String retType = "message";

        try {

            System.out.println("Unique ID " + inputBean.getUserid());

            if (inputBean.getUserid() != null && !inputBean.getUserid().isEmpty()) {

                String message = this.validateLimitUpdate();

                if (message.isEmpty()) {

                    HttpServletRequest request = ServletActionContext.getRequest();
                    CustomerSearchDAO dao = new CustomerSearchDAO();

                    System.out.println("Trantype" + inputBean.getTxntype());

                    String newV = inputBean.getUserid() + "|"
                            + inputBean.getTxntype() + "|"
                            + inputBean.getUserLimit() + "|"
                            + inputBean.getStatuslimit() + "|";

                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.UPDATE_CUSTOMER_LIMIT, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Transaction type limit " + inputBean.getTxntypelimit() + " updated", null, null, newV);

                    message = dao.updateUseTxnLimit(inputBean, audit);

                    if (message.isEmpty()) {
                        addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + "transaction wise limit ");
                    } else {
                        addActionError(message);
                    }

                } else {
                    addActionError(message);
                }
            } else {
                addActionError("Unique ID cannot be empty");

            }
        } catch (Exception ex) {
            Logger.getLogger(CustomerSearchAction.class
                    .getName()).log(Level.SEVERE, null, ex);
            addActionError("Customer SearchAction " + MessageVarList.COMMON_ERROR_UPDATE);
        }
        return retType;
    }

    public String deleteLimit() {

        System.out.println("called CustomerSearchAction : deleteLimit");
        String message = null;
        String retType = "delete";
        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            CustomerSearchDAO dao = new CustomerSearchDAO();

            Systemaudit audit = Common.makeAudittrace(request, TaskVarList.DELETE_CUSTOMER_LIMIT, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Transactiion type " + inputBean.getTxntype() + " Deleted", null);

            message = dao.deleteUseTxnLimit(inputBean, audit);

            if (message.isEmpty()) {
                message = MessageVarList.COMMON_SUCCESS_CREAT_DELETE_TASK_PENDING + " transaction limit ";
            }
            inputBean.setMessage(message);

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class
                    .getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(OracleMessage.getMessege(e.getMessage()));
        }
        return retType;
    }

    public String findTxnLimit() {

        System.out.println("called CustomerSearchAction : findTxnLimit");
        List<CustomerSearchLimitBean> tmpDataList = null;

        try {
            if (inputBean.getTxntype() != null && !inputBean.getTxntype().isEmpty()) {

                CommonDAO cmd = new CommonDAO();
                CustomerSearchDAO dao = new CustomerSearchDAO();
                TransactionLimit tranLimit = new TransactionLimit();

                System.out.println("trntype" + inputBean.getTxntype());
                System.out.println("segmenttype" + inputBean.getCusSegmentTypelimit());

                tranLimit = cmd.getTxnLimits(inputBean.getTxntype(), inputBean.getCusSegmentTypelimit());

                inputBean.setGlobalVal(tranLimit.getDefaultLimit().toString());

                SwtUserTxnLimit limit = dao.findLimitTypeById(inputBean);

                inputBean.setTxntype(limit.getId().getTxnLimitType());
                inputBean.setUserid(limit.getId().getUserid().toString());
                inputBean.setStatus(limit.getStatus());
                inputBean.setUserLimit(String.valueOf(limit.getUserLimit()));
            }
        } catch (Exception ex) {
            inputBean.setMessage("Customer search action  " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger
                    .getLogger(CustomerSearchAction.class
                            .getName()).log(Level.SEVERE, null, ex);
        }

        return "findlimit";
    }

    public String cusAccountInq() {

        String message = "";
        System.out.println("called CustomerSearchAction : cusAccountInq");

        CustomerSearchDAO dao = new CustomerSearchDAO();

        try {

            message = this.validateAccountInq();

            if (message.isEmpty()) {
                if (inputBean.getDefType().equals(CommonVarList.CUSTOMER_DEF_TYPE_ACC)) {
                    AccountList accListREST = new AccountList();
                    message = accListREST.getAccountList(inputBean);
                } else if (inputBean.getDefType().equals(CommonVarList.CUSTOMER_DEF_TYPE_CARD)) {
                    SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());
                    inputBean.setNic(mobUser.getNic());
                    CardList cardListREST = new CardList();
                    message = cardListREST.getCardList(inputBean);
                }
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                session.setAttribute(SessionVarlist.CUSTOMER_SEARCH_ACC_CARD_LIST, inputBean.getAccountList());

                if (message != null && !message.isEmpty()) {
                    inputBean.setMessage(message);

                }
            } else {
                inputBean.setMessage(message);
            }

        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " Customer search action");
        }

        return "cusaccountinq";
    }
    
    public String customerInq() {

        String message = "";
        System.out.println("called CustomerSearchAction : customerInq");

        try {
            CustomerData customerDataREST = new CustomerData();
            message = customerDataREST.getCustomerData(inputBean);

            HttpSession session = ServletActionContext.getRequest().getSession(false);
            session.setAttribute(SessionVarlist.CUSTOMER_SEARCH_CUSTOMER_DATA, inputBean.getNewCustomerDataSession());
            inputBean.setNewCustomerDataSession(null);
            if (message != null && !message.isEmpty()) {
                inputBean.setMessage(message);

            } else {
                CustomerSearchDAO dao = new CustomerSearchDAO();
                SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());
                inputBean.setOldCustomerData(this.getCustomerDataByMobUser(mobUser, "--"));
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            inputBean.setMessage(MessageVarList.COMMON_ERROR_PROCESS + " Customer search action");
        }

        return "customerinq";
    }

    public String updateAccount() {
        System.out.println("called CustomerSearchAction : updateAccount");
        String result = "message";
        String message = "";
        String newV = "";

        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            CustomerSearchDAO dao = new CustomerSearchDAO();
            CommonDAO comdao = new CommonDAO();
            message = this.validateupdateAccount();
            if (message.isEmpty()) {
                String accNo = "";
                HttpSession session = ServletActionContext.getRequest().getSession(false);
                List<AccountBean> accountList = (List<AccountBean>) session.getAttribute(SessionVarlist.CUSTOMER_SEARCH_ACC_CARD_LIST);
                if (inputBean.getDefTypeHidden().equals(CommonVarList.CUSTOMER_DEF_TYPE_ACC)) {
                    for (AccountBean account : accountList) {
                        if (account.getAccountNumber().equals(inputBean.getDefaultAccountnew())) {
                            inputBean.setAccountproduct(account.getAccountproduct());
                            break;
                        }

                    }
                    accNo = inputBean.getDefaultAccountnew();
                } else if (inputBean.getDefTypeHidden().equals(CommonVarList.CUSTOMER_DEF_TYPE_CARD)) {
                    for (AccountBean account : accountList) {
                        if (account.getAccountNumber().equals(inputBean.getDefaultAccountnew())) {
                            inputBean.setNameoncard(account.getNameoncard());
                            break;
                        }

                    }
                    accNo = comdao.maskCardNumber(inputBean.getDefaultAccountnew());
                }
                CardToken cardTokenREST = new CardToken();
                cardTokenREST.cardTokenRequest(inputBean);
                //------------------------------get Old/New value------------------------------------------
                newV = inputBean.getUserid() + "|"
                        + accNo + "|"
                        + inputBean.getDefTypeHidden();

                SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());
                String defaultAccNo = "";
                try {
                    if (mobUser.getDefaultAccType() != null && mobUser.getDefaultAccType().toString().equals("2")) {
                        defaultAccNo = mobUser.getDefaultCardNo().toString();
                    } else {
                        defaultAccNo = mobUser.getDefaultAccNo().toString();
                    }
//                    inputBean.setDefaultAccountOld(mobUser.getDefaultAccNo().toString());
                } catch (NullPointerException e) {
                }
                String oldV = inputBean.getUserid() + "|"
                        + defaultAccNo + "|"
                        + mobUser.getDefaultAccType();

                if (!newV.equals(oldV)) {
                    Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Requested to update primary account of customer  (CID :  '" + mobUser.getCif() + "') ", null, oldV, newV);

                    message = dao.updateCustomerAccounts(inputBean, audit);

                    if (message.isEmpty()) {
                        addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + "primary account");
                    } else {
                        addActionError(message);
                    }
                } else {
                    addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
                }
            } else {
                addActionError(message);
            }
        } catch (Exception ex) {
            addActionError("Customer Search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String updateCustomer() {
        System.out.println("called CustomerSearchAction : updateCustomer");
        String result = "message";
        String message = "";
        String newV = "";

        try {
            HttpServletRequest request = ServletActionContext.getRequest();
            CustomerSearchDAO dao = new CustomerSearchDAO();
            HttpSession session = ServletActionContext.getRequest().getSession(false);
            CustomerBean customerNewData = (CustomerBean) session.getAttribute(SessionVarlist.CUSTOMER_SEARCH_CUSTOMER_DATA);

            //------------------------------get Old/New value------------------------------------------
            newV = inputBean.getUserid() + "|"
                    + customerNewData.getCustomerName() + "|"
//                    + customerNewData.getStatus() + "|"
                    + customerNewData.getSegmentType()+ "|"
                    + customerNewData.getCustomerCategory()+ "|"
                    + customerNewData.getNic()+ "|"
                    + customerNewData.getDob()+ "|"
                    + customerNewData.getGender()+ "|"
                    + customerNewData.getMobileNumber()+ "|"
                    + customerNewData.getEmail()+ "|"
                    + customerNewData.getSecondaryMobile()+ "|"
                    + customerNewData.getSecondaryEmail()+ "||"
//                    + customerNewData.getPermanentAdd()+ "|"
                    + customerNewData.getAccountofficer();

            SwtMobileUser mobUser = dao.getCustomerByUserID(inputBean.getUserid());
            CustomerBean customerOldData = this.getCustomerDataByMobUser(mobUser, "");

            String oldV = inputBean.getUserid() + "|"
                    + customerOldData.getCustomerName() + "|"
//                    + customerOldData.getStatus() + "|"
                    + customerOldData.getSegmentType()+ "|"
                    + customerOldData.getCustomerCategory()+ "|"
                    + customerOldData.getNic()+ "|"
                    + customerOldData.getDob()+ "|"
                    + customerOldData.getGender()+ "|"
                    + customerOldData.getMobileNumber()+ "|"
                    + customerOldData.getEmail()+ "|"
                    + customerOldData.getSecondaryMobile()+ "|"
                    + customerOldData.getSecondaryEmail()+ "||"
//                    + customerOldData.getPermanentAdd()+ "|"
                    + customerOldData.getAccountofficer();
            System.out.println("Old Val " + oldV);
            System.out.println("New Val " + newV);
            if (!newV.equals(oldV)) {
                Systemaudit audit = Common.makeAudittrace(request, TaskVarList.CUSTOMER_DATA_UPDATE_TASK, PageVarList.CUSTOMER_SEARCH, SectionVarList.CUSTOMER_MANAGEMENT, "Requested to update customer data  (CID :  '" + mobUser.getCif() + "') ", null, oldV, newV);

                message = dao.updateCustomerData(inputBean, audit);

                if (message.isEmpty()) {
                    addActionMessage(MessageVarList.COMMON_SUCCESS_CREAT_UPDATE_TASK_PENDING + "customer data");
                } else {
                    addActionError(message);
                }
            } else {
                addActionError(MessageVarList.CUSTOMER_SEARCH_RECORD_NOCHANGE);
            }

        } catch (Exception ex) {
            addActionError("Customer Search " + MessageVarList.COMMON_ERROR_PROCESS);
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public String viewPend() {
        System.out.println("called CustomerSearchAction : viewPend");
        String retType = "viewPend";
        try {
            if (inputBean.getId() != null && !inputBean.getId().isEmpty()) {
                CustomerSearchDAO dao = new CustomerSearchDAO();
                CustomerUpdateBean pendOldNewBean = dao.getPendOldNewValueOfCustomer(inputBean);
                if (pendOldNewBean != null) {
                    inputBean.setPendOldNew(pendOldNewBean);
                } else {
                    addActionError("Record not exist.");
                    inputBean.setPendOldNew(new CustomerUpdateBean());
                }
            } else {
                addActionError("Empty pending task ID.");
            }
        } catch (Exception e) {
            Logger.getLogger(CustomerSearchAction.class.getName()).log(Level.SEVERE, null, e);
            addActionError(" Customer Search " + MessageVarList.COMMON_ERROR_PROCESS);
        }
        return retType;
    }

    private String validateupdateAccount() {
        String message = "";
        if (inputBean.getUserid() == null || inputBean.getUserid().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_CUSTOMER_ID;
        } else if (inputBean.getDefaultAccountnew() == null || inputBean.getDefaultAccountnew().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_PRIMARY_ACCOUNT;
        }

        return message;
    }

    private String validateAccountInq() {
        String message = "";

//        if (inputBean.getDefaultAccountOld()== null || inputBean.getDefaultAccountOld().trim().isEmpty()) {
//            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_ACCOUNT_NO_INQ;
//        }
        if (inputBean.getCif() == null || inputBean.getCif().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_CIF;
        } else if (inputBean.getDefType() == null || inputBean.getDefType().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_SEARCH_EMPTY_PRIMARY_TYTPE;
        }

        return message;
    }

    private String validateLimitInputs() throws Exception {

        String message = "";

        if (inputBean.getUserid() == null || inputBean.getUserid().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_LIMIT_CIF_EMPTY;
        } else if (inputBean.getTxntypelimit() == null || inputBean.getTxntypelimit().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_LIMIT_TRANSACTIONTYPE_EMPTY;
        } else if (inputBean.getUserLimit() == null || inputBean.getUserLimit().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_LIMIT_USERLIMIT_EMPTY;
        } else if (inputBean.getStatuslimit() == null || inputBean.getStatuslimit().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_LIMIT_STATUS_EMPTY;
        } else if ((inputBean.getUserLimit() != null || !inputBean.getUserLimit().trim().isEmpty()) && (!Validation.isValidFloatValue(inputBean.getUserLimit().trim()))) {
            message = MessageVarList.CUSTOMER_LIMIT_USERLIMIT_INVALID_VAL;
        } else if (inputBean.getTxntype() != null && inputBean.getCusSegmentTypelimit() != null) {
            TransactionLimit tranLimit = new TransactionLimit();
            CommonDAO cmd = new CommonDAO();

            System.out.println("Txn Type " + inputBean.getTxntypelimit());
            System.out.println("Segment Type " + inputBean.getCusSegmentTypelimit());

            tranLimit = cmd.getTxnLimits(inputBean.getTxntypelimit(), inputBean.getCusSegmentTypelimit());

            if ((Double.parseDouble(inputBean.getUserLimit())) > Double.parseDouble(tranLimit.getDefaultLimit().toString())) {
                message = MessageVarList.CUSTOMER_LIMIT_USERLIMIT_INVALID;
            }
        }
        return message;
    }

    private String validateLimitUpdate() throws Exception {

        String message = "";

        if (inputBean.getUserid() == null || inputBean.getUserid().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_LIMIT_CIF_EMPTY;
        } else if (inputBean.getTxntype() == null || inputBean.getTxntype().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_LIMIT_TRANSACTIONTYPE_EMPTY;
        } else if (inputBean.getUserLimit() == null || inputBean.getUserLimit().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_LIMIT_USERLIMIT_EMPTY;
        } else if (inputBean.getStatuslimit() == null || inputBean.getStatuslimit().trim().isEmpty()) {
            message = MessageVarList.CUSTOMER_LIMIT_STATUS_EMPTY;
        } else if ((inputBean.getUserLimit() != null || !inputBean.getUserLimit().trim().isEmpty()) && (!Validation.isValidFloatValue(inputBean.getUserLimit().trim()))) {
            message = MessageVarList.CUSTOMER_LIMIT_USERLIMIT_INVALID_VAL;
        } else if (inputBean.getTxntype() != null && inputBean.getCusSegmentTypelimit() != null) {
            TransactionLimit tranLimit = new TransactionLimit();
            CommonDAO cmd = new CommonDAO();

            System.out.println("Trn Type" + inputBean.getTxntype());
            System.out.println("Segment Type" + inputBean.getCusSegmentTypelimit());

            tranLimit = cmd.getTxnLimits(inputBean.getTxntype(), inputBean.getCusSegmentTypelimit());

            if ((Double.parseDouble(inputBean.getUserLimit())) > Double.parseDouble(tranLimit.getDefaultLimit().toString())) {
                message = MessageVarList.CUSTOMER_LIMIT_USERLIMIT_INVALID;
            }
        }
        return message;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    private List<ChannelTypeBean> getChannelTypeList() {

        List<ChannelTypeBean> channelTypeList = new ArrayList<ChannelTypeBean>();

        ChannelTypeBean channeltype1 = new ChannelTypeBean();
        channeltype1.setKey("2");
        channeltype1.setValue("Internet Banking");
        channelTypeList.add(channeltype1);

        ChannelTypeBean channeltype2 = new ChannelTypeBean();
        channeltype2.setKey("1");
        channeltype2.setValue("Mobile Banking");
        channelTypeList.add(channeltype2);

        return channelTypeList;

    }

    private List<KeyValueBean> getOnBoardTypeList() {

        List<KeyValueBean> channelTypeList = new ArrayList<KeyValueBean>();

        KeyValueBean onBoardType1 = new KeyValueBean();
        onBoardType1.setKey("1");
        onBoardType1.setValue("Account");
        channelTypeList.add(onBoardType1);

        KeyValueBean onBoardType2 = new KeyValueBean();
        onBoardType2.setKey("2");
        onBoardType2.setValue("Card");
        channelTypeList.add(onBoardType2);
        
        KeyValueBean onBoardType3 = new KeyValueBean();
        onBoardType3.setKey("3");
        onBoardType3.setValue("Migrated User");
        channelTypeList.add(onBoardType3);

        return channelTypeList;

    }

    private List<KeyValueBean> getDefaultAccountTypeList() {

        List<KeyValueBean> channelTypeList = new ArrayList<KeyValueBean>();

        KeyValueBean onBoardType1 = new KeyValueBean();
        onBoardType1.setKey("1");
        onBoardType1.setValue("Account");
        channelTypeList.add(onBoardType1);

        KeyValueBean onBoardType2 = new KeyValueBean();
        onBoardType2.setKey("2");
        onBoardType2.setValue("Card");
        channelTypeList.add(onBoardType2);

        return channelTypeList;

    }

    private CustomerBean getCustomerDataByMobUser(SwtMobileUser mobUser, String nullOrEmptyReplaceVal) {
        CustomerBean customer = new CustomerBean();
        try {
            customer.setEmail(mobUser.getEmail().toString());
        } catch (NullPointerException e) {
            customer.setEmail(nullOrEmptyReplaceVal);
        }
        try {
            customer.setMobileNumber(mobUser.getMobileNumber().toString());
        } catch (NullPointerException e) {
            customer.setMobileNumber(nullOrEmptyReplaceVal);
        }
        try {
            customer.setSecondaryEmail(mobUser.getSecondaryEmail().toString());
        } catch (NullPointerException e) {
            customer.setSecondaryEmail(nullOrEmptyReplaceVal);
        }
        try {
            customer.setSecondaryMobile(mobUser.getSecondaryMobile().toString());
        } catch (NullPointerException e) {
            customer.setSecondaryMobile(nullOrEmptyReplaceVal);
        }
        try {
            customer.setPermanentAdd(mobUser.getPermanentAddress().toString());
        } catch (NullPointerException e) {
            customer.setPermanentAdd(nullOrEmptyReplaceVal);
        }
        try {
            customer.setSegmentType(mobUser.getSegmentType().getSegmentcode().toString());
        } catch (Exception e) {
            customer.setSegmentType(nullOrEmptyReplaceVal);
        }
        try {
            customer.setCustomerName(mobUser.getCustomerName().toString());
        } catch (NullPointerException e) {
            customer.setCustomerName(nullOrEmptyReplaceVal);
        }

        try {
            customer.setCustomerCategory(mobUser.getCustomerCategory().toString());
        } catch (Exception e) {
            customer.setCustomerCategory(nullOrEmptyReplaceVal);
        }
        try {
            customer.setNic(mobUser.getNic().toString());
        } catch (NullPointerException e) {
            customer.setNic(nullOrEmptyReplaceVal);
        }
        try {
            customer.setDob(mobUser.getDob().toString());
        } catch (NullPointerException e) {
            customer.setDob(nullOrEmptyReplaceVal);
        }
        try {
            customer.setGender(mobUser.getGender().toString());
        } catch (NullPointerException e) {
            customer.setGender(nullOrEmptyReplaceVal);
        }
        try {
            customer.setAccountofficer(mobUser.getActofficer().toString());
        } catch (NullPointerException e) {
            customer.setAccountofficer(nullOrEmptyReplaceVal);
        }
        return customer;
    }

}
