/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.customermanagement.restrequest;

import com.epic.ndb.bean.customermanagement.CustomerSearchInputBean;
import com.epic.ndb.bean.customermanagement.SwtConfigBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.customermanagement.CustomerSearchDAO;
import com.epic.ndb.util.varlist.CommonVarList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sivaganesan_t
 */
public class CardToken {

    public String cardTokenRequest(CustomerSearchInputBean inputbean) throws Exception {
        String response = "";
        String message = "";
        URL url = null;
        HttpURLConnection urlConnection = null;
        String urnDetail = "";
        String baseUrlDetail = "";
        String urlDetail = "";
        BufferedWriter bWriter = null;
        BufferedReader bReader = null;
        String request = "";
        try {
            CommonDAO dao = new CommonDAO();
            CustomerSearchDAO customerDao = new CustomerSearchDAO();

            SwtConfigBean swtConfigBean = customerDao.getSwitchConfig();

            urnDetail = "tsp/card_token_generation/";
            baseUrlDetail = dao.findCommonConfigById(CommonVarList.COMMON_CONFIG_REST_BASE_URL).getParamvalue();
            urlDetail = baseUrlDetail + urnDetail;

            url = new URL(urlDetail);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Authorization", swtConfigBean.getRestAutorization());

            request = "{\n"
                    + " \"cardnumber\":\"" + inputbean.getDefaultAccountnew() + "\",\n"
                    + "	\"nameoncard\":\"" + inputbean.getNameoncard() + "\",\n"
                    + "	\"cvv\":\"111\",\n"
                    + "	\"expiry\":\"0000\",\n"
                    + "	\"cardassociation\":\"VISA\",\n"
                    + "	\"tokentype\":\"CARD\",\n"
                    + "	\"bankcode\":\"00001\""
                    + "}";

            System.out.println("request              : " + request);

            bWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
            bWriter.write(request);
            bWriter.flush();

            // Get the response
            bReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while (null != ((line = bReader.readLine()))) {
                response += line;
            }

            System.out.println("reponse              : " + response);
            message =getResponseToToken(inputbean,response);
        } catch (IOException e) {
            message = "Fail to create connection";
            System.err.println("Fail to create connection" + e.getMessage());
//            message =getResponseToToken(inputbean,"{\"txnid\":\"1028032810382103801\",\"token\":\"123213213\",\"expdate\":\"1232131\",\"responsecode\":\"000\",\"responseerror\":\"\"}");//----------------------need to comment hard coded
        } catch (Exception ex) {
            throw ex; //----------------------need to un comment
//            message =getResponseToToken(inputbean,"{\"txnid\":\"1028032810382103801\",\"token\":\"123213213\",\"expdate\":\"1232131\",\"responsecode\":\"000\",\"responseerror\":\"\"}");//----------------------need to comment hard coded
        }
        return message;
    }
    private String getResponseToToken(CustomerSearchInputBean inputbean,String switchResponse) throws Exception {
        String accInfoResponseError = "";
        try {
            
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(switchResponse);
            
            String accInfoResponse = ((String) obj.get("responsecode"));
            
            if (accInfoResponse!=null && (accInfoResponse.equals("00") || accInfoResponse.equals("000"))) {
                String token = (String)obj.get("token");
                inputbean.setCardToken(token);
            } else {
                accInfoResponseError = ((String) obj.get("responseerror"));
            }
        } catch (Exception e) {
            throw e;
        }
        return accInfoResponseError;
    }

}
