/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.action.customermanagement.restrequest;

import com.epic.ndb.bean.customermanagement.AccountBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchInputBean;
import com.epic.ndb.bean.customermanagement.SwtConfigBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.dao.customermanagement.CustomerSearchDAO;
import com.epic.ndb.util.varlist.CommonVarList;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sivaganesan_t
 */
public class CardList {
    
     public String getCardList(CustomerSearchInputBean inputbean) throws IOException, Exception {
        String response = "";
        String message = "";
        URL url = null;
        HttpURLConnection urlConnection = null;
        String urnDetail = "";
        String baseUrlDetail = "";
        String urlDetail = "";
        BufferedWriter bWriter = null;
        BufferedReader bReader = null;
        String request = "";
        try {
             CommonDAO dao =new CommonDAO();
            CustomerSearchDAO customerDao =new CustomerSearchDAO();
            
            SwtConfigBean swtConfigBean= customerDao.getSwitchConfig();
            
//            urnDetail= dao.findCommonConfigById(CommonVarList.COMMON_CONFIG_ACCOUNT_LIST_URN).getParamvalue();
            urnDetail="common/card_detail";
            baseUrlDetail= dao.findCommonConfigById(CommonVarList.COMMON_CONFIG_REST_BASE_URL).getParamvalue();
            urlDetail=baseUrlDetail+urnDetail;
            
            url = new URL(urlDetail);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);

            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Authorization", swtConfigBean.getRestAutorization());

            request = "{\n"
                    + "   \"mode\":" + 2 + ",\n"
//                    + "    \"accountNumber\":\"" + "421689038888" + "\"\n"
                    + "    \"cif\":\"" + inputbean.getCif()+ "\",\n"
                    + "    \"nic\":\"" + inputbean.getNic()+ "\"\n"
                    + "}";

            System.out.println("request              : " + request);

            bWriter = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream()));
            bWriter.write(request);
            bWriter.flush();

            // Get the response
            bReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            while (null != ((line = bReader.readLine()))) {
                response += line;
            }

            System.out.println("reponse              : " + response);

            message =getResponseToAccountList(inputbean,response);
        }catch (IOException e) {
            message="Fail to create connection";
            System.err.println("Fail to create connection"+e.getMessage());
        } catch (Exception ex) {
            throw ex;
        }
         return message;
     }
     
     private String getResponseToAccountList(CustomerSearchInputBean inputbean,String switchResponse) throws Exception {
        List<AccountBean> accountList = new ArrayList<AccountBean>();
        String accInfoResponseError = "";
        try {
            
            CommonDAO dao =new CommonDAO();
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(switchResponse);
            
            String accInfoResponse = ((String) obj.get("responsecode"));
            
            if (accInfoResponse!=null && (accInfoResponse.equals("00") || accInfoResponse.equals("000"))) {
                
                JSONArray cardList = (JSONArray) obj.get("cardList");
                
                for (Object o : cardList) {
                    if (o instanceof JSONObject) {
                        
                        JSONObject card =  (JSONObject) o;
                          
                        AccountBean accountBean = new AccountBean();

                        JSONObject genaralDetails = (JSONObject) card.get("genaralDetails");
                        
                        String cardStatus =(String)genaralDetails.get("cardStatus");
                        String cardType =(String)genaralDetails.get("cardType");
                        if( cardStatus!=null && cardStatus.equals("1") && cardType!=null && cardType.equals("CREDIT")){//only for active credit cards
                            
                            String card_number = (String) genaralDetails.get("card_number");
                            accountBean.setAccountNumber(card_number);
                            accountBean.setAccountMaskedNumber(dao.maskCardNumber(card_number));
                            try{
                                String nameOnCardTemp = (String)genaralDetails.get("nameOnCard");
                                if(nameOnCardTemp!=null && !nameOnCardTemp.isEmpty() && !nameOnCardTemp.equals("null") ){
                                    accountBean.setNameoncard(nameOnCardTemp);
                                }else{
                                    String cardIssuer = (String)genaralDetails.get("cardIssuer");
                                    accountBean.setNameoncard(cardIssuer);
                                }
                            }catch(Exception e){
                                String cardIssuer = (String)genaralDetails.get("cardIssuer");
                                accountBean.setNameoncard(cardIssuer);
                            }
                            accountList.add(accountBean);
                        }
                            
                    }
                }
                inputbean.setAccountList(accountList);
            } else {
                accInfoResponseError = ((String) obj.get("responseerror"));
            }
        } catch (Exception e) {
            throw e;
        }
        return accInfoResponseError;
    }
}
