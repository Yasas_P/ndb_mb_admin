/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.common;

import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.bean.controlpanel.systemconfig.TermsVersionBean;
import com.epic.ndb.bean.customermanagement.KeyValueBean;
import com.epic.ndb.bean.customermanagement.SwtConfigBean;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.BillBillerType;
import com.epic.ndb.util.mapping.BillFieldType;
import com.epic.ndb.util.mapping.BillServiceProvider;
import com.epic.ndb.util.mapping.BillerCategory;
import com.epic.ndb.util.mapping.Branch;
import com.epic.ndb.util.mapping.ChannelType;
import com.epic.ndb.util.mapping.DelistedCategory;
import com.epic.ndb.util.mapping.FeesCharges;
import com.epic.ndb.util.mapping.InboxServiceCategory;
import com.epic.ndb.util.mapping.OtherBank;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.mapping.ProductCurrency;
import com.epic.ndb.util.mapping.ProductType;
import com.epic.ndb.util.mapping.Productcategory;
import com.epic.ndb.util.mapping.PromotionsCards;
import com.epic.ndb.util.mapping.PromotionsCategories;
import com.epic.ndb.util.mapping.RegularExpression;
import com.epic.ndb.util.mapping.SchedulePayType;
import com.epic.ndb.util.mapping.Section;
import com.epic.ndb.util.mapping.Sectionpage;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.SwtMtConfiguration;
import com.epic.ndb.util.mapping.SwtResponseCodes;
import com.epic.ndb.util.mapping.SwtTxnType;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.TransactionLimit;
import com.epic.ndb.util.mapping.TransferType;
import com.epic.ndb.util.mapping.Userlevel;
import com.epic.ndb.util.mapping.Userrole;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author chanuka
 */
public class CommonDAO {

    public static Date getSystemDate(Session session) throws Exception {
        Date sysDateTime = null;
        try {

            String sql = "select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') as a from dual";
            Query query = session.createSQLQuery(sql);
            List l = query.list();
            String stime = (String) l.get(0);
            sysDateTime = Timestamp.valueOf(stime);

        } catch (Exception e) {
            e.printStackTrace();            
            throw e;
        } finally {
        }
        return sysDateTime;
    }

    public static Date getSystemDateLogin() throws Exception {
        Date sysDateTime = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "select to_char(sysdate, 'yyyy-mm-dd hh24:mi:ss') from dual";
            Query query = session.createSQLQuery(sql);
            List l = query.list();
            String stime = (String) l.get(0);
            sysDateTime = Timestamp.valueOf(stime);

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return sysDateTime;
    }

    // get page description
    public Page getPageDescription(String pageCode) throws Exception {

        Page pageBean = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Page as s where s.pagecode =:pagecode";
            Query query = session.createQuery(sql).setString("pagecode", pageCode);
            pageBean = (Page) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return pageBean;
    }

    public Section getSectionOfPage(String pageCode, String userRole)
            throws Exception {

        Section sectionBean = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "select s.section from Sectionpage as s where s.id.pagecode =:pagecode and s.id.userrolecode=:userrolecode ";
            Query query = session.createQuery(sql)
                    .setString("pagecode", pageCode)
                    .setString("userrolecode", userRole);
            sectionBean = (Section) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return sectionBean;
    }

    // get status list
    public List<Status> DefultStatusList(String statusCode)
            throws Exception {

        List<Status> statusList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscategory.categorycode=:statuscategorycode order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString(
                    "statuscategorycode", statusCode);
            statusList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return statusList;
    }

    // get status list
    public String getSectionByRoleAndPage(String userrole, String page)
            throws Exception {

        String section = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Sectionpage as s where s.id.userrolecode=:userrolecode and s.id.pagecode=:pagecode";
            Query query = session.createQuery(sql).setString(
                    "userrolecode", userrole).setString(
                            "pagecode", page);

            Sectionpage Sectionpage = (Sectionpage) query.list().get(0);
            section = Sectionpage.getId().getSectioncode();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return section;
    }

    public List<Status> getDefultStatusList2(String statusCode)
            throws Exception {

        List<Status> statusList = null;
        Session session = null;
        Status st = new Status();
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscategory.categorycode=:statuscategorycode order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString(
                    "statuscategorycode", statusCode);
            statusList = query.list();
            st.setStatuscode("CWDL");
            st.setDescription("Customer Wallet deleted");
            statusList.add(st);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return statusList;
    }

    public List<SwtTxnType> getTxnTypeActCodeList()
            throws Exception {

        List<SwtTxnType> txnType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtTxnType as s where s.status.statuscode=:status order by s.description asc order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("status", CommonVarList.STATUS_ACTIVE);
            txnType = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return txnType;
    }

    // get user role
    public List<Userrole> getUserRoleList(String statusCode)
            throws Exception {

        List<Userrole> userroleList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Userrole as s where s.status.statuscode =:statuscode order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString(
                    "statuscode", statusCode);
            userroleList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userroleList;
    }

//    public List<Transactiontype> gettranstypeList() throws Exception {
//
//        List<Transactiontype> currencyList = new ArrayList<Transactiontype>();
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Transactiontype as s order by Upper(s.description) asc";
//            Query query = session.createQuery(sql);
//            currencyList = query.list();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return currencyList;
//    }
    // get txn type list
//    public List<Transactiontype> getTxnTypeList(String statusCode)
//            throws Exception {
//
//        List<Transactiontype> txnTypeList = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
////            String sql = "from Transactiontype as s where s.riskRequired =:status";
//            String sql = "from Transactiontype as s order by Upper(s.description) asc";
////            Query query = session.createQuery(sql).setString("status", statusCode);
//            Query query = session.createQuery(sql);
//            txnTypeList = query.list();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnTypeList;
//    }
//    public List<Transactiontype> getActiveTxnTypeList(String statusCode)
//            throws Exception {
//
//        List<Transactiontype> txnTypeList = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
////            String sql = "from Transactiontype as s where s.riskRequired =:status";
//            String sql = "from Transactiontype as s where s.status.statuscode=:code order by Upper(s.description) asc";
////            Query query = session.createQuery(sql).setString("status", statusCode);
//            Query query = session.createQuery(sql).setString("code", statusCode);
//            txnTypeList = query.list();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnTypeList;
//    }
//    public List<Transactiontype> getActiveTxnTypeListByRiskRequired(String statusCode, String riskRequired)
//            throws Exception {
//
//        List<Transactiontype> txnTypeList = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Transactiontype as s where s.status.statuscode=:code and riskRequired=:riskRequired order by Upper(s.description) asc";
//            Query query = session.createQuery(sql).setString("code", statusCode).setString("riskRequired", riskRequired);
//            txnTypeList = query.list();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnTypeList;
//    }
//    public List<Transactiontype> getActiveTxnTypeListByAcqRiskRequired(String statusCode, String acqRiskRequired)
//            throws Exception {
//
//        List<Transactiontype> txnTypeList = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Transactiontype as s where s.status.statuscode=:code and acqRiskRequired=:acqRiskRequired order by Upper(s.description) asc";
//            Query query = session.createQuery(sql).setString("code", statusCode).setString("acqRiskRequired", acqRiskRequired);
//            txnTypeList = query.list();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnTypeList;
//    }
    public List<Userrole> getUserRoleList() throws Exception {

        List<Userrole> userroleList = new ArrayList<Userrole>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Userrole as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            userroleList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userroleList;
    }

    public List<Userrole> getUserRoleListByStatus(String status) throws Exception {

        List<Userrole> userroleList = new ArrayList<Userrole>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Userrole as u where u.status =:status order by Upper(u.description) asc";
            Query query = session.createQuery(sql).setString("status", status);
            userroleList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userroleList;
    }

    public List<Userrole> getUserRoleListByStatus(String status, String userrolecode) throws Exception {

        List<Userrole> userroleList = new ArrayList<Userrole>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Userrole as u where u.status =:status and u.userrolecode !=:userrolecode order by Upper(u.description) asc";
            Query query = session.createQuery(sql).setString("status", status).setString("userrolecode", userrolecode);
            userroleList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userroleList;
    }

    public List<Userrole> getALLUserList()
            throws Exception {

        List<Userrole> userRoleList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Userrole as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            userRoleList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userRoleList;
    }

    public List<Userrole> getALLUserList(String userrolecode)
            throws Exception {

        List<Userrole> userRoleList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Userrole as s where s.userrolecode !=:userrolecode order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("userrolecode", userrolecode);
            userRoleList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userRoleList;
    }

    //get userlevellist
    public List<Userlevel> getUserLevelList() throws Exception {
        List<Userlevel> userLevelList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Userlevel as u order by Upper(u.description) asc";
            Query query = session.createQuery(sql);
            userLevelList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userLevelList;
    }

    public List<Systemuser> getUserList() throws Exception {
        // TODO Auto-generated method stub

        List<Systemuser> userRoleList = new ArrayList<Systemuser>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Systemuser u order by Upper(u.username) asc";
            Query query = session.createQuery(sql);
            userRoleList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userRoleList;
    }

    public List<Section> getSectionList() throws Exception {
        List<Section> sectionList = new ArrayList<Section>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String hql = "from Section s order by Upper(s.description) asc ";
            Query query = session.createQuery(hql);
            sectionList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return sectionList;
    }

    public List<Page> getPageList() throws Exception {
        List<Page> pageList = new ArrayList<Page>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String hql = "from Page as p order by Upper(p.description) asc";
            Query query = session.createQuery(hql);
            pageList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return pageList;
    }

    public List<Task> getTaskList() throws Exception {
        List<Task> taskList = new ArrayList<Task>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String hql = "from Task as t order by Upper(t.description) asc";
            Query query = session.createQuery(hql);
            taskList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return taskList;
    }

    // use JNDI for connection
    public static Connection getConnection() throws Exception {
        Connection con = null;
        try {
            InitialContext context = new InitialContext();
            DataSource dataSource = (DataSource) context.lookup(CommonVarList.JNDI_REPORT_CONNECTION);
            con = dataSource.getConnection();
        } catch (Exception e) {
            throw e;
        }
        return con;
    }

    public String getStatusByprefix(String srefix) throws Exception {
        Status st = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            st = (Status) session.get(Status.class, srefix);
        } catch (Exception he) {
            throw he;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return st.getDescription();
    }

    public String getSectionByprefix(String appprefix) throws Exception {
        Section sec = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            sec = (Section) session.get(Section.class, appprefix);
        } catch (Exception he) {
            throw he;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return sec.getDescription();
    }

    public String getUserRoleByprefix(String appprefix) throws Exception {

        Userrole userRole = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            userRole = (Userrole) session.get(Userrole.class, appprefix);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userRole.getDescription();
    }

    public String getPageByprefix(String appprefix) throws Exception {
        Page page = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            page = (Page) session.get(Page.class, appprefix);
        } catch (Exception he) {
            throw he;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return page.getDescription();
    }

    public String getTaskByprefix(String appprefix) throws Exception {
        Task task = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            task = (Task) session.get(Task.class, appprefix);
        } catch (Exception he) {
            throw he;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return task.getDescription();
    }

    public static String saveAudit(Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            txn = session.beginTransaction();
            audit.setCreatetime(sysDate);
            audit.setLastupdatedtime(sysDate);

            session.save(audit);

            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<SwtTxnType> getTxnTypeCodeList()
            throws Exception {

        List<SwtTxnType> txnType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtTxnType as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            txnType = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return txnType;
    }
    public List<SwtTxnType> getFilteredTxnTypeListByCode()
            throws Exception {

        List<SwtTxnType> txnType = null;
        Session session = null;
        try {
            List<String> txnTypeCodeList = new ArrayList<String>();
//            txnTypeCodeList.add("49");
            txnTypeCodeList.add("26");
            txnTypeCodeList.add("27");
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtTxnType as s where s.typecode in (:typeCodeList) order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setParameterList("typeCodeList", txnTypeCodeList);
            txnType = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return txnType;
    }
    public List<SwtResponseCodes> getResponseCodeList()
            throws Exception {

        List<SwtResponseCodes> response = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtResponseCodes as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            response = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return response;
    }

    // get txn type list
//    public List<Transactiontype> getAllTxnTypes()
//            throws Exception {
//
//        List<Transactiontype> txnTypeList = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Transactiontype as s order by Upper(s.description) asc";
//            Query query = session.createQuery(sql);
//            txnTypeList = query.list();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnTypeList;
//    }
//    public Transactiontype getTxnDescription(String txnTypeCode) throws Exception {
//
//        Transactiontype txnType = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Transactiontype as s where s.typecode =:typecode";
//            Query query = session.createQuery(sql).setString("typecode", txnTypeCode);
//            txnType = (Transactiontype) query.list().get(0);
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnType;
//    }
//    public String getTxnCodeByDescription(String description) throws Exception {
//
//        String txnTypeCode = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Transactiontype as s where s.description =:description";
//            Query query = session.createQuery(sql).setString("description", description);
//            txnTypeCode = ((Transactiontype) query.list().get(0)).getTransactiontypecode();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnTypeCode;
//    }
    public String getTaskSortKeyCount(String sortkey) throws Exception {

        String des = null;
//        boolean sortkey_valid;
        long count;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Task as s where s.sortkey =:sortkey";
            Query query = session.createQuery(sql).setString("sortkey", sortkey);
            count = query.list().size();
            if (count == 0) {
                des = "";
            } else {

                des = MessageVarList.TASK_MGT_SORTKEY_ALREADY_EXSISTS;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return des;
    }

    public String getTaskSortKeyCountUpdate(String sortkey, String oldsortkey) throws Exception {

        String des = null;
//        boolean sortkey_valid;
        long count;
//        long count2;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Task as u where u.sortkey=:sortkey and u.sortkey!=:oldsortkey";
//          
            Query query = session.createQuery(sql).setString("sortkey", sortkey).setString("oldsortkey", oldsortkey);
            count = query.list().size();

            if (count == 0) {
                des = "";
            } else {

                des = MessageVarList.TASK_MGT_SORTKEY_ALREADY_EXSISTS;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return des;
    }

    public String getPageSortKeyCountUpdate(String sortkey, String oldsortkey) throws Exception {

        String des = null;
//        boolean sortkey_valid;
        long count;
//        long count2;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Page as u where u.sortkey=:sortkey and u.sortkey!=:oldsortkey";
//          
            Query query = session.createQuery(sql).setString("sortkey", sortkey).setString("oldsortkey", oldsortkey);
            count = query.list().size();

            if (count == 0) {
                des = "";
            } else {

                des = MessageVarList.TASK_MGT_SORTKEY_ALREADY_EXSISTS;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return des;
    }

    public String getSectionSortKeyCountUpdate(String sortkey, String oldsortkey) throws Exception {

        String des = null;
//        boolean sortkey_valid;
        long count;
//        long count2;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Section as u where u.sortkey=:sortkey and u.sortkey!=:oldsortkey";
//          
            Query query = session.createQuery(sql).setString("sortkey", sortkey).setString("oldsortkey", oldsortkey);
            count = query.list().size();

            if (count == 0) {
                des = "";
            } else {

                des = MessageVarList.SECTION_SORT_KEY_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return des;
    }

    public String getPageSortKeyCount(String sortkey) throws Exception {

        String des = "";
//        boolean sortkey_valid;
        long count;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Page as s where s.sortkey =:sortkey";
            Query query = session.createQuery(sql).setString("sortkey", sortkey);
            count = query.list().size();
            if (count == 0) {
                des = "";
            } else {

                des = MessageVarList.PAGE_MGT_ERROR_SORT_KEY_ALREADY_EXSITS;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return des;
    }

    public String getSectionSortKeyCount(String sortkey) throws Exception {

        String des = "";
//        boolean sortkey_valid;
        long count;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Section as s where s.sortkey =:sortkey";
            Query query = session.createQuery(sql).setString("sortkey", sortkey);
            count = query.list().size();
            if (count == 0) {
                des = "";
            } else {

                des = MessageVarList.SECTION_SORT_KEY_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return des;
    }

    public String getSectionSortKeyCountForUpdate(String sortkey, String oldsortkey) throws Exception {

        String des = null;
//        boolean sortkey_valid;
        long count;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Section as s where s.sortkey =:sortkey and s.sortkey!=:oldsortkey";
            Query query = session.createQuery(sql).setString("sortkey", sortkey).setString("oldsortkey", oldsortkey);
            count = query.list().size();
            if (count == 0) {
                des = "";
            } else {

                des = MessageVarList.SECTION_SORT_KEY_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return des;
    }

    public String isTaskSortKeyExsits(String sortkey) throws Exception {

        String txnTypeCode = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Task as s where s.sortkey =:sortkey";
            Query query = session.createQuery(sql).setString("sortkey", sortkey);
            txnTypeCode = ((Task) query.list().get(0)).getSortkey().toString();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return txnTypeCode;
    }

//    public List<Transactiontype> getTransactionTypeList() throws Exception {
//        List<Transactiontype> transactiontypeList = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Transactiontype as u order by Upper(u.description) asc";
//            Query query = session.createQuery(sql);
//            transactiontypeList = query.list();
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return transactiontypeList;
//    }
//    public String getDescriptionByTxnCode(String typecode) throws Exception {
//
//        String description = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Transactiontype as s where s.typecode =:typecode";
//            Query query = session.createQuery(sql).setString("typecode", typecode);
//            description = ((Transactiontype) query.list().get(0)).getDescription();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return description;
//    }
    public Status getStatusByCode(String statusCode) throws Exception {

        Status status = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscode =:statuscode ";
            Query query = session.createQuery(sql).setString("statuscode", statusCode);
            status = (Status) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return status;
    }
    
    public String getStatusCategoryCodeByStatusCode(String statusCode) throws Exception {

        Status status = null;
        Session session = null;
        String catCode="";
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscode =:statuscode ";
            Query query = session.createQuery(sql).setString("statuscode", statusCode);
            status = (Status) query.list().get(0);
            catCode=status.getStatuscategory().getCategorycode();
            
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return catCode;
    }

    public String getStatusCodeByDescription(String status) throws Exception {

        String code = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "select STATUSCODE from STATUS where DESCRIPTION=:description";
            Query query = session.createSQLQuery(sql).setParameter("description", status);
            code = (String) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return code;
    }

    public List<Currency> getCurrencyList() throws Exception {

        List<Currency> currencyList = new ArrayList<Currency>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Currency as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            currencyList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return currencyList;
    }

    public List<Status> getDefultStatusList(String statusCode)
            throws Exception {

        List<Status> statusList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscategory.categorycode=:statuscategorycode order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString(
                    "statuscategorycode", statusCode);
            statusList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return statusList;
    }
    
    public List<Status> getInboxEditStatusListForNewStatus()
            throws Exception {

        List<Status> statusList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscategory.categorycode=:statuscategorycode and s.statuscode not in ('NORP') order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString(
                    "statuscategorycode", CommonVarList.STATUS_CATEGORY_INBOX);
            statusList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return statusList;
    }
    
    public List<Status> getInboxEditStatusListForInprogressStatus()
            throws Exception {

        List<Status> statusList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscategory.categorycode=:statuscategorycode and s.statuscode not in ('NORP','NEW') order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString(
                    "statuscategorycode", CommonVarList.STATUS_CATEGORY_INBOX);
            statusList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return statusList;
    }

    public List<BillerCategory> getBillerCategoryList() throws Exception {

        List<BillerCategory> billerList = new ArrayList<BillerCategory>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from BillerCategory as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            billerList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return billerList;
    }
    
    public List<BillServiceProvider> getBilServiceProviderList() throws Exception {

        List<BillServiceProvider> billServiceProviderList = new ArrayList<BillServiceProvider>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from BillServiceProvider as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            billServiceProviderList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return billServiceProviderList;
    }
    
    public List<BillerCategory> getBillerCategoryListActiveList() throws Exception {

        List<BillerCategory> billerList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from BillerCategory as s where s.status=:status order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("status", CommonVarList.STATUS_ACTIVE);
            billerList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return billerList;
    }

    public List<BillBillerType> getBillBillerTypeList() throws Exception {

        List<BillBillerType> billerTypeList = new ArrayList<BillBillerType>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from BillBillerType as s order by Upper(s.billerTypeDescription) asc";
            Query query = session.createQuery(sql);
            billerTypeList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return billerTypeList;
    }

    public List<BillFieldType> getBillFieldTypeList() throws Exception {

        List<BillFieldType> fieldTypeList = new ArrayList<BillFieldType>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from BillFieldType as s order by Upper(s.name) asc";
            Query query = session.createQuery(sql);
            fieldTypeList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return fieldTypeList;
    }

    public List<Status> getStatusLIstByCategoryCode(String categorycode)
            throws Exception {

        List<Status> statusList = null;
        Session session = null;
        Status st = new Status();
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscategory.categorycode=:categorycode order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString(
                    "categorycode", categorycode);
            statusList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return statusList;
    }

    public List<Status> getDefultStatusCusList(String statusCodeCus, String statusCodeDef)
            throws Exception {

        List<Status> statusList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscategory.categorycode=:statuscategorycode OR s.statuscategory.categorycode=:statusCodeDef order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString(
                    "statuscategorycode", statusCodeCus).setString("statusCodeDef", statusCodeDef);
            statusList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return statusList;
    }

//    public List<Txntype> getAllTxnTypeList()throws Exception {
//
//        List<Txntype> txnTypeList = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Txntype as s order by Upper(s.description) asc";
//            Query query = session.createQuery(sql);
//            txnTypeList = query.list();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnTypeList;
//    }
//    public List<Txncode> getAllTxnCodeList() throws Exception {
//
//        List<Txncode> txnCodeList = null;
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Txncode as s order by Upper(s.description) asc";
//            Query query = session.createQuery(sql);
//            txnCodeList = query.list();
//
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return txnCodeList;
//    } 
//    
    public List<Branch> getBranchList() throws Exception {

        List<Branch> branchList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Branch as s order by Upper(s.branchname) asc";
            Query query = session.createQuery(sql);
            branchList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return branchList;
    }

    public String getBranchByprefix(String srefix) throws Exception {
        Branch br = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            br = (Branch) session.get(Branch.class, srefix);
        } catch (Exception he) {
            throw he;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return br.getBranchname();
    }

    public ParameterUserCommon findCommonConfigById(String paramCode) throws Exception {
        ParameterUserCommon commonconfig = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from ParameterUserCommon as u where u.paramcode=:paramcode";
            Query query = session.createQuery(sql).setString("paramcode", paramCode);
            commonconfig = (ParameterUserCommon) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return commonconfig;

    }

    public List<PromotionsCards> getPromotionCardList() throws Exception {

        List<PromotionsCards> promotionsCardsList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from PromotionsCards as s order by Upper(s.subregioncode) asc";
            Query query = session.createQuery(sql);
            promotionsCardsList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return promotionsCardsList;
    }

    public List<PromotionsCategories> getPromotionCategoriesList() throws Exception {

        List<PromotionsCategories> promotionsCategoriesList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from PromotionsCategories as s order by Upper(s.catname) asc";
            Query query = session.createQuery(sql);
            promotionsCategoriesList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return promotionsCategoriesList;
    }

    public List<ChannelType> getChannelTypeList() throws Exception {

        List<ChannelType> channelTypeList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from ChannelType as s order by Upper(s.channelType) asc";
            Query query = session.createQuery(sql);
            channelTypeList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return channelTypeList;
    }

    public List<OtherBank> getOtherBankList() throws Exception {

        List<OtherBank> otherBankList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from OtherBank as s order by Upper(s.bankname) asc";
            Query query = session.createQuery(sql);
            otherBankList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return otherBankList;
    }

    public List<TransferType> getTransferTypeList() throws Exception {

        List<TransferType> transferTypeList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from TransferType as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            transferTypeList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return transferTypeList;
    }
    public List<TransferType> getTransferTypeActiveList() throws Exception {

        List<TransferType> transferTypeList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from TransferType as s where s.status=:status order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("status", CommonVarList.STATUS_ACTIVE);
            transferTypeList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return transferTypeList;
    }

    public List<SegmentType> getSegmentTypeList() throws Exception {

        List<SegmentType> segmentType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SegmentType as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            segmentType = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return segmentType;
    }
    public List<SegmentType> getSegmentTypeActiveList() throws Exception {

        List<SegmentType> segmentType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SegmentType as s where s.status=:status order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("status", CommonVarList.STATUS_ACTIVE);
            segmentType = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return segmentType;
    }

    public List<ProductCurrency> getProductCurrencyList()throws Exception {

        List<ProductCurrency> productCurrency = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from ProductCurrency as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            productCurrency = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return productCurrency;
    }
    public List<ProductCurrency> getProductCurrencyActiveList()throws Exception {

        List<ProductCurrency> productCurrency = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from ProductCurrency as s where s.status.statuscode=:status order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("status", CommonVarList.STATUS_ACTIVE);
            productCurrency = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return productCurrency;
    }
    public List<ProductType> getProductTypeActiveList()throws Exception {

        List<ProductType> productType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from ProductType as s where s.status.statuscode=:status order by Upper(s.productName) asc";
            Query query = session.createQuery(sql).setString("status", CommonVarList.STATUS_ACTIVE);
            productType = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return productType;
    }
    
    public List<ProductType> getProductTypeList()throws Exception {

        List<ProductType> productType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from ProductType as s order by Upper(s.productName) asc";
            Query query = session.createQuery(sql);
            productType = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return productType;
    }
    
    public List<TransferType> getTransfertypeListByStatus(String status) throws Exception {

        List<TransferType> tranList = new ArrayList<TransferType>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from TransferType as s where s.status =:status order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("status", status);
            tranList = query.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return tranList;
    }
    
    public TransactionLimit getTxnLimits(String trantype,String segmtype) throws Exception {

        TransactionLimit txnLimit = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from TransactionLimit as s where s.id.segmtype=:segmtype and s.id.transfertype=:transfertype ";
            Query query = session.createQuery(sql).setString("transfertype", trantype).setString("segmtype", segmtype);
            if( !query.list().isEmpty()){
                txnLimit = (TransactionLimit) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return txnLimit;
    }
    
     public String getSwtTxnDesByCode(String typecode) throws Exception {
        SwtTxnType txntype = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtTxnType s where s.typecode=:typecode ";
            Query query = session.createQuery(sql).setString("typecode", typecode);
            txntype = (SwtTxnType) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return txntype.getDescription();
     }
     public String getResponseDesByCode(String code) throws Exception {
        SwtResponseCodes responseCode = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtResponseCodes s where s.code=:code ";
            Query query = session.createQuery(sql).setString("code", code);
            responseCode = (SwtResponseCodes) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return responseCode.getDescription();
     }
     
     public List<TermsVersionBean> getVersionList() throws Exception {
        Session session = null;
        List<TermsVersionBean> userLevel = new ArrayList<TermsVersionBean>();

        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "select "
                    + "u.versionNo, "
                    + "u.status "
                    + "from TermsCondition u order by Upper(u.versionNo) desc ";
            Query query = session.createQuery(sql);
            List<Object[]> objectArrList = (List<Object[]>) query.list();
            if (objectArrList.size() > 0) {

                for (Object[] objArr : objectArrList) {
                    TermsVersionBean bean = new TermsVersionBean();
                    try {
                        bean.setKey(objArr[0].toString());
                    } catch (NullPointerException npe) {
                        bean.setKey("--");
                    }
                    try {
                        bean.setValue(objArr[0].toString());
                    } catch (NullPointerException npe) {
                        bean.setKey("--");
                    }
                    userLevel.add(bean);
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userLevel;
    }
     
     public List<Productcategory> getProductCategoryList()
            throws Exception {

        List<Productcategory> productcategory = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Productcategory as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            productcategory = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return productcategory;
    }
     
     public List<CommonKeyVal> getFeeChargeListByTransferType(String transferType) throws Exception {

        List<CommonKeyVal> feesChargesList = new ArrayList<CommonKeyVal>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from FeesCharges as s where s.id.transferType =:transferType order by Upper(s.id.chargeCode) asc";
            Query query = session.createQuery(sql).setString("transferType", transferType);
            Iterator it = query.iterate();;
            while (it.hasNext()) {
                CommonKeyVal bean = new CommonKeyVal();
                FeesCharges feesCharge = (FeesCharges) it.next();
                
                try {
                    bean.setKey(feesCharge.getId().getChargeCode());
                } catch (Exception npe) {
                    bean.setKey("");
                }
                try {
                    bean.setValue(feesCharge.getDescription());
                } catch (Exception npe) {
                    bean.setValue("");
                }
                feesChargesList.add(bean);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return feesChargesList;
    }
     
     public List<DelistedCategory> getDelistedCategory()
            throws Exception {

        List<DelistedCategory> categoryList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from DelistedCategory as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            categoryList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return categoryList;
    }
     public List<DelistedCategory> getDelistedCategoryActive()
            throws Exception {

        List<DelistedCategory> categoryList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from DelistedCategory as s where s.status.statuscode =:statuscode order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("statuscode", CommonVarList.STATUS_ACTIVE);
            categoryList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return categoryList;
    }
     
    public SwtConfigBean getSwitchConfig() throws Exception {
        SwtMtConfiguration swtConfig = null;
        Session session = null;
        SwtConfigBean swtConfigBean = new SwtConfigBean();
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtMtConfiguration as mb where mb.id=:id";
            Query query = session.createQuery(sql);
            query.setLong("id", CommonVarList.SWT_CONFIG_ID);

            swtConfig = (SwtMtConfiguration) query.list().get(0);

            swtConfigBean.setId(String.valueOf(swtConfig.getId()));
            swtConfigBean.setNtbRestBaseUrl(swtConfig.getNtbRestBaseUrl());
            swtConfigBean.setRestAutorization(swtConfig.getRestAutorization());
            swtConfigBean.setRestContentType(swtConfig.getRestContentType());

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return swtConfigBean;
    }
    
    public BigDecimal getIDByPromoID(Session session,String code) throws Exception {

        BigDecimal id = null;
        try {
            String sql = "select ID from NOTIFICATION where PROMOTION_ID=:code";
            Query query = session.createSQLQuery(sql).setParameter("code", code);
            id = (BigDecimal) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
        }
        return id;
    }
    
    public List<RegularExpression> getRegexList()
            throws Exception {

        List<RegularExpression> regexList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from RegularExpression as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            regexList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return regexList;
    }
    
    public static String maskCardNumber(String card_number ){
        String final_card = null;
        if (card_number!=null){
            final_card   = card_number;
            int length = card_number.length();
            if (length > 4) {
                String card_first = card_number.substring(0,4);
                String card_middle = card_number.substring(4, length - 4);
                String card_last = card_number.substring(length-4,length);
                String maskedTxt = "";
                for (int i = 0; i < card_middle.length(); i++) {
                    maskedTxt+="*";
                }
                final_card =  card_first + maskedTxt + card_last;

            }
        }

        return final_card;
    }
    
    public List<SchedulePayType> getSchedulePayTypeList() throws Exception {

        List<SchedulePayType> payType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SchedulePayType as s order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            payType = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return payType;
    }
    
    public String getStatusDesByCode(String code) throws Exception {
        Status status = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status s where s.statuscode=:statuscode ";
            Query query = session.createQuery(sql).setString("statuscode", code);
            status = (Status) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return status.getDescription();
     }
    
    public String getSchedulePayTypeDesByCode(String code) throws Exception {
        SchedulePayType schedulePayType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SchedulePayType s where s.payType=:payType ";
            Query query = session.createQuery(sql).setString("payType", code);
            schedulePayType = (SchedulePayType) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return schedulePayType.getDescription();
     }
    
    public List<Status> getReadUnreadStatus()
            throws Exception {

        List<Status> statusList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscode in ('5','6') order by Upper(s.description) asc";
            Query query = session.createQuery(sql);
            statusList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            try {
                session.flush();
                session.close();
            } catch (Exception e) {
                throw e;
            }
        }
        return statusList;
    } 
    
    public List<InboxServiceCategory> getInboxServiceCategoryList() throws Exception {

        List<InboxServiceCategory> serviceCategory = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from InboxServiceCategory as s where s.status='ACT' order by Upper(s.service) asc";
            Query query = session.createQuery(sql);
            serviceCategory = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return serviceCategory;
    }
    
    public List<KeyValueBean> getSegmentTypeActiveKeyValueList() throws Exception {

        List<SegmentType> segmentTypeList = null;
        List<KeyValueBean> segmentKeyValueList = new ArrayList<KeyValueBean>() ;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SegmentType as s where s.status=:status order by Upper(s.description) asc";
            Query query = session.createQuery(sql).setString("status", CommonVarList.STATUS_ACTIVE);
            segmentTypeList = query.list();
            for(SegmentType segment :segmentTypeList){
                KeyValueBean segmentKeyValue =new KeyValueBean();
                segmentKeyValue.setKey(segment.getSegmentcode());
                segmentKeyValue.setValue(segment.getDescription());
                segmentKeyValueList.add(segmentKeyValue);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return segmentKeyValueList;
    }
}
