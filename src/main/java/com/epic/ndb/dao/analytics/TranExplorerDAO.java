package com.epic.ndb.dao.analytics;

import com.epic.ndb.bean.analytics.TranExplorerBean;
import com.epic.ndb.bean.analytics.TranExplorerInputBean;
import com.epic.ndb.bean.analytics.TranExplorerSummeryBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.ExcelCommon;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.PartialList;
import com.epic.ndb.util.mapping.SwtTxnType;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTAxDataSource;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBarChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBarSer;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTBoolean;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTCatAx;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTChart;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTCrosses;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTLegend;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumDataSource;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTNumRef;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTPlotArea;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTScaling;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTSerTx;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTStrRef;
import org.openxmlformats.schemas.drawingml.x2006.chart.CTValAx;
import org.openxmlformats.schemas.drawingml.x2006.chart.STAxPos;
import org.openxmlformats.schemas.drawingml.x2006.chart.STBarDir;
import org.openxmlformats.schemas.drawingml.x2006.chart.STLegendPos;
import org.openxmlformats.schemas.drawingml.x2006.chart.STOrientation;
import org.openxmlformats.schemas.drawingml.x2006.chart.STTickLblPos;

/**
 *
 * @author jayathissa_d
 */
public class TranExplorerDAO {

    private final int headerRowCount = 18;
    private String TXN_COUNT_SQL = "SELECT "
            + "COUNT(TRANSACTION_ID) "
            + "FROM SWT_TRANSACTION U "
            + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
            + "WHERE ";
    private String TRANEXPLORER_ORDER_BY_SQL = " order by U.LOCAL_TIME DESC ";

    public PartialList<TranExplorerBean> getSearchList(TranExplorerInputBean inputBean, int rows, int from, String sortIndex, String sortOrder) throws Exception {
        List<TranExplorerBean> dataBeanList = new ArrayList<TranExplorerBean>();
        Session session = null;
        int count = 0;
        if ("".equals(sortIndex.trim())) {
            sortIndex = null;
        }
        String CUSTOMER_SQL_SEARCH = "SELECT "
                + "U.TRANSACTION_ID, "//0
                + "T.DESCRIPTION TXN_TYPE, "//1          
                + "U.FROM_ACCOUNT_NUMBER, "//2
                + "U.TO_ACCOUNT_NUMBER, "//3
                + "U.AMOUNT, "//4
                //                + "U.CUST_CIF, "//5
                //                + "U.CUST_NIC, "//6
                //                + "U.CUST_NAME, "//7
                //                + "U.CUST_EMAIL, "//8
                //                + "U.ADDRESS_1, "//9 
                + "M.CIF, "//5
                + "M.NIC, "//6
                + "M.CUSTOMER_NAME, "//7
                + "M.EMAIL, "//8
                + "M.PERMANENT_ADDRESS, "//9 
                + "ST.DESCRIPTION STATUS, "//10 
                + "U.LEASE_MODEL, "//11
                + "U.LEASE_TYPE, "//12
                + "U.NTB_REQUEST_TYPE, "//13
                + "U.POSTED_METHOD, "//14
                + "U.RESPONCE_CODE, "//15
                //                + "S.DESCRIPTION RESPONSE,"
                + "M.MOBILE_NUMBER, "//16
                + "U.SERVICE_FEE, "//17
                + "U.PAYEE_NAME, "//18
                + "U.BILL_CATEGORY_NAME, "//19
                + "U.BILL_PROVIDER_NAME, "//20
                + "U.BILLER_NAME, "//21
                + "U.MERCHANT_TYPE_NAME, "//22
                + "U.MERCHANT_NAME, "//23
                + "U.REDEEM_POINTS, "//24
                + "U.REDEEM_VOUCHER_ID, "//25
                + "U.LEASE_AMOUNT, "//26
                + "U.LEASE_AVD_PAYMENT, "//27
                + "U.LEASE_SELL_PRICE, "//28
                + "U.CHEQUE_NUMBER, "//29
                + "U.FROM_DATE, "//30
                + "U.TO_DATE, "//31
                + "U.CARD_NUMBER, "//32
                + "U.STATEMENT_FROM, "//33
                + "U.STATEMENT_TO, "//34
                + "U.CURRENCY_CODE, "//35
                + "U.BANK_NAME, "//36
                + "U.BRANCH_NAME, "//37
                + "U.REMARKS, "//38
                + "U.ACCOUNT_NO, "//39
                + "U.COLLECT_FROM_TYPE, "//40
                + "U.COLLECTION_BRANCH_ID, "//41
                + "U.PREFERRED_EMAIL, "//42
                + "U.REQUEST_DATE, "//43
                + "U.LASTUPDATED, "//44                
                + "U.LOCAL_TIME, "//45
                + "U.USER_ID, "//46
                + "U.CHANNEL_TYPE, "//47
                + "U.DEVICE_ID, "//48
                + "M.CUSTOMER_CATEGORY, "//49
                + "U.APP_ID, "//50
                + "U.BILL_REFFRENCE_NUMBER, "//51
                + "TM.DESCRIPTION TXN_MODE_DES, "//52
                + "U.TRAN_REF_NO, "//53
                //                + "U.REQUEST_ID, "//53
                + "U.ERR_DESC, "//54
                + "M.USERNAME, "//55
                + "D.DEVICE_MANUFACTURE, "//56
                + "D.DEVICE_BUILD_NUMBER, "//57
                + "PT.DESCRIPTION PAY_TYPE_DES, "//58
                + "U.TYPE PAY_TYPE, "//59
                + "U.T24_RESPONSE, "//60
                + "U.T24_REFERENCE, "//61
                + "U.IBL_RESPONSE, "//62
                + "U.IBL_REFERENCE, "//63
                + "U.IS_ONE_TIME, "//64
                + "CAST(U.TRANSACTION_TYPE AS VARCHAR2(3)), "//65
                + "U.IS_PAYTO_MOBILE ,"//66
                + "U.IS_SCHEDULE "//67
                + "FROM SWT_TRANSACTION U "
                + "LEFT OUTER JOIN SWT_RESPONSE_CODES S ON S.CODE = U.RESPONCE_CODE "
                + "LEFT OUTER JOIN SWT_TXN_TYPE T ON T.TYPECODE = U.TRANSACTION_TYPE "
                + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
                + "LEFT OUTER JOIN STATUS ST ON ST.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN TRANSACTION_MODE TM ON TM.TRAN_TYPE = U.TRANSACTION_TYPE AND TM.TRAN_MODE = U.TXN_MODE "
                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID AND D.USERID = U.USER_ID "
                + "LEFT OUTER JOIN TXN_PAY_TYPE PT ON PT.TRAN_TYPE = U.TRANSACTION_TYPE AND PT.PAY_TYPE = U.TYPE "
                //                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID "
                + "WHERE ";
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String where1 = this.makeWhereClause(inputBean);
            String sqlCount = this.TXN_COUNT_SQL + where1;
//            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = CUSTOMER_SQL_SEARCH + where1 + this.TRANEXPLORER_ORDER_BY_SQL;
//                System.out.println(sql);

                Query query = session.createSQLQuery(sql);
                query.setMaxResults(rows);
                query.setFirstResult(from);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    dataBeanList = new ArrayList<TranExplorerBean>();

                    for (Object[] objArr : objectArrList) {

                        TranExplorerBean dataBean = new TranExplorerBean();

                        //set data values to beanlist
                        try {

                            if (objArr[0] == null) {
                                dataBean.setTxnId("--");
                            } else {
                                dataBean.setTxnId(objArr[0].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnId("--");
                        }

                        try {
                            if (objArr[1] == null) {
                                dataBean.setTxnType("--");
                            } else {;
                                dataBean.setTxnType(objArr[1].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnType("--");
                        }

                        try {
                            if (objArr[2] == null) {
                                dataBean.setFromAccNo("--");
                            } else {
                                dataBean.setFromAccNo(objArr[2].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromAccNo("--");
                        }
                        try {
                            if (objArr[3] == null) {
                                dataBean.setToAccNo("--");
                            } else {
                                dataBean.setToAccNo(objArr[3].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToAccNo("--");
                        }

                        try {
                            if (objArr[4] == null) {
                                dataBean.setAmount("--");
                            } else {
                                dataBean.setAmount(objArr[4].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAmount("--");
                        }

                        try {
                            if (objArr[5] == null) {
                                dataBean.setCustCif("--");
                            } else {
                                dataBean.setCustCif(objArr[5].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustCif("--");
                        }

                        try {
                            if (objArr[6] == null) {
                                dataBean.setNic("--");
                            } else {
                                dataBean.setNic(objArr[6].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNic("--");
                        }

                        try {
                            if (objArr[7] == null) {
                                dataBean.setCustName("--");
                            } else {
                                dataBean.setCustName(objArr[7].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustName("--");
                        }

                        try {
                            if (objArr[8] == null) {
                                dataBean.setCustEmail("--");
                            } else {
                                dataBean.setCustEmail(objArr[8].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustEmail("--");
                        }

                        try {
                            if (objArr[9] == null) {
                                dataBean.setAddress1("--");
                            } else {
                                dataBean.setAddress1(objArr[9].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAddress1("--");
                        }

                        try {
                            if (objArr[10] == null) {
                                dataBean.setStatus("--");
                            } else {
                                dataBean.setStatus(objArr[10].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatus("--");
                        }

                        try {
                            if (objArr[11] == null) {
                                dataBean.setLeasingModel("--");
                            } else {
                                dataBean.setLeasingModel(objArr[11].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingModel("--");
                        }

                        try {
                            if (objArr[12] == null) {
                                dataBean.setLeasingTypes("--");
                            } else {
                                dataBean.setLeasingTypes(objArr[12].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingTypes("--");
                        }

                        try {
                            if (objArr[13] == null) {
                                dataBean.setNtbRequest("--");
                            } else {
                                dataBean.setNtbRequest(objArr[13].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNtbRequest("--");
                        }

                        try {
                            if (objArr[14] == null) {
                                dataBean.setPostedMethod("--");
                            } else {
                                dataBean.setPostedMethod(objArr[14].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPostedMethod("--");
                        }

                        try {
                            if (objArr[15] == null) {
                                dataBean.setResponseCodes("Failure");
                            } else {
                                if (objArr[15].equals("000")) {
                                    dataBean.setResponseCodes("Success");
                                } else {
                                    dataBean.setResponseCodes("Failure");
                                }
//                                dataBean.setResponseCodes(objArr[15].toString());
                                //dataBean.setResponseCodes(objArr[15].getSwtResponseCodes().getDescription());        
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setResponseCodes("Failure");
                        }

                        try {
                            if (objArr[16] == null) {
                                dataBean.setMobileNumber("--");
                            } else {
                                dataBean.setMobileNumber(objArr[16].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMobileNumber("--");
                        }

                        try {
                            if (objArr[17] == null) {
                                dataBean.setServiceFee("--");
                            } else {
                                dataBean.setServiceFee(objArr[17].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setServiceFee("--");
                        }

                        try {
                            if (objArr[18] == null) {
                                dataBean.setPayeeName("--");
                            } else {
                                dataBean.setPayeeName(objArr[18].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPayeeName("--");
                        }

                        try {
                            if (objArr[19] == null) {
                                dataBean.setBillCategoryName("--");
                            } else {
                                dataBean.setBillCategoryName(objArr[19].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillCategoryName("--");
                        }

                        try {
                            if (objArr[20] == null) {
                                dataBean.setBillProviderName("--");
                            } else {
                                dataBean.setBillProviderName(objArr[20].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillProviderName("--");
                        }

                        try {
                            if (objArr[21] == null) {
                                dataBean.setBillerName("--");
                            } else {
                                dataBean.setBillerName(objArr[21].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillerName("--");
                        }
                        try {
                            if (objArr[22] == null) {
                                dataBean.setMerchantTypeName("--");
                            } else {
                                dataBean.setMerchantTypeName(objArr[22].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantTypeName("--");
                        }

                        try {
                            if (objArr[23] == null) {
                                dataBean.setMerchantName("--");
                            } else {
                                dataBean.setMerchantName(objArr[23].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantName("--");
                        }

                        try {
                            if (objArr[24] == null) {
                                dataBean.setRedeemPoints("--");
                            } else {
                                dataBean.setRedeemPoints(objArr[24].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemPoints("--");
                        }

                        try {
                            if (objArr[25] == null) {
                                dataBean.setRedeemVoucherId("--");
                            } else {
                                dataBean.setRedeemVoucherId(objArr[25].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemVoucherId("--");
                        }

                        try {
                            if (objArr[26] == null) {
                                dataBean.setLeaseAmount("--");
                            } else {
                                dataBean.setLeaseAmount(objArr[26].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAmount("--");
                        }

                        try {
                            if (objArr[27] == null) {
                                dataBean.setLeaseAvdPayment("--");
                            } else {
                                dataBean.setLeaseAvdPayment(objArr[27].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAvdPayment("--");
                        }

                        try {
                            if (objArr[28] == null) {
                                dataBean.setLeaseSellPrice("--");
                            } else {
                                dataBean.setLeaseSellPrice(objArr[28].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseSellPrice("--");
                        }

                        try {
                            if (objArr[29] == null) {
                                dataBean.setChequeNumber("--");
                            } else {
                                dataBean.setChequeNumber(objArr[29].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setChequeNumber("--");
                        }
                        try {
                            if (objArr[30] == null) {
                                dataBean.setFromDate("--");
                            } else {
                                dataBean.setFromDate(objArr[30].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromDate("--");
                        }

                        try {
                            if (objArr[31] == null) {
                                dataBean.setToDate("--");
                            } else {
                                dataBean.setToDate(objArr[31].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToDate("--");
                        }

                        try {
                            if (objArr[32] == null) {
                                dataBean.setCardNumber("--");
                            } else {
//                                dataBean.setCardNumber(toMaskCardNumber(objArr[32].toString()));
                                dataBean.setCardNumber(objArr[32].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCardNumber("--");
                        }

                        try {
                            if (objArr[33] == null) {
                                dataBean.setStatementFrom("--");
                            } else {
                                dataBean.setStatementFrom(objArr[33].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementFrom("--");
                        }

                        try {
                            if (objArr[34] == null) {
                                dataBean.setStatementTo("--");
                            } else {
                                dataBean.setStatementTo(objArr[34].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementTo("--");
                        }

                        try {
                            if (objArr[35] == null) {
                                dataBean.setCurrencyCode("--");
                            } else {
                                dataBean.setCurrencyCode(objArr[35].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCurrencyCode("--");
                        }

                        try {
                            if (objArr[36] == null) {
                                dataBean.setBankName("--");
                            } else {
                                dataBean.setBankName(objArr[36].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBankName("--");
                        }

                        try {
                            if (objArr[37] == null) {
                                dataBean.setBranchName("--");
                            } else {
                                dataBean.setBranchName(objArr[37].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBranchName("--");
                        }
                        try {
                            if (objArr[38] == null) {
                                dataBean.setRemarks("--");
                            } else {
                                dataBean.setRemarks(objArr[38].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRemarks("--");
                        }

                        try {
                            if (objArr[39] == null) {
                                dataBean.setAccountNo("--");
                            } else {
                                dataBean.setAccountNo(objArr[39].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAccountNo("--");
                        }

                        try {
                            if (objArr[40] == null) {
                                dataBean.setCollectFromType("--");
                            } else {
                                dataBean.setCollectFromType(objArr[40].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectFromType("--");
                        }

                        try {
                            if (objArr[41] == null) {
                                dataBean.setCollectionBranchId("--");
                            } else {
                                dataBean.setCollectionBranchId(objArr[41].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectionBranchId("--");
                        }

                        try {
                            if (objArr[42] == null) {
                                dataBean.setPreferredEmail("--");
                            } else {
                                dataBean.setPreferredEmail(objArr[42].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPreferredEmail("--");
                        }

                        try {
                            if (objArr[43] == null) {
                                dataBean.setRequestDate("--");
                            } else {
                                dataBean.setRequestDate(objArr[43].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRequestDate("--");
                        }

                        try {
                            if (objArr[44] == null) {
                                dataBean.setLastupdated("--");
                            } else {
                                dataBean.setLastupdated(objArr[44].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLastupdated("--");
                        }
                        try {
                            if (objArr[45] == null) {
                                dataBean.setLocalTime("--");
                            } else {
                                dataBean.setLocalTime(objArr[45].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLocalTime("--");
                        }
                        try {
                            if (objArr[46] == null) {
                                dataBean.setUserId("--");
                            } else {
                                dataBean.setUserId(objArr[46].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setUserId("--");
                        }
                        try {
                            if (objArr[47] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[47].toString());
                                dataBean.setChannelType(channelTypeDes);
                            } else {
                                dataBean.setChannelType("--");
                            }
                        } catch (Exception e) {
                            dataBean.setChannelType("--");
                        }
                        try {
                            dataBean.setDeviceId(objArr[48].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceId("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        try {
                            if (objArr[49] != null && objArr[49].equals("703")) {
                                dataBean.setStaffStatus("Staff");
                            } else {
                                dataBean.setStaffStatus("Non Staff");
                            }
                        } catch (NullPointerException e) {
                            dataBean.setStaffStatus("--");
                        }

                        try {
                            dataBean.setAppId(objArr[50].toString());
                        } catch (NullPointerException e) {
                            dataBean.setAppId("--");
                        }
                        try {
                            dataBean.setBillRefNo(objArr[51].toString());
                        } catch (NullPointerException e) {
                            dataBean.setBillRefNo("--");
                        }
//                        try {
////                            String txn_type="0";
////                            if(objArr[55]!=null){
////                                txn_type=objArr[55].toString();
////                            }     
////                            dataBean.setTxnMode( getTxnModeDesByCode(txn_type,objArr[52].toString()));
//                            dataBean.setTxnMode(objArr[52].toString());
//                        } catch (NullPointerException e) {
//                            dataBean.setTxnMode("--");
//                        }
                        if (objArr[65] != null && objArr[65].toString().equalsIgnoreCase("27")) {
                            try {
                                dataBean.setPayTypeDes(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setPayTypeDes("Accounts");
                            }
                            try {
                                dataBean.setTxnMode(objArr[58].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                        } else {
                            try {
                                dataBean.setTxnMode(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                            try {
                                dataBean.setPayTypeDes(objArr[58].toString());
                            } catch (NullPointerException e) {
                                dataBean.setPayTypeDes("Accounts");
                            }
                        }
                        try {
                            dataBean.setTranRefNo(objArr[53].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTranRefNo("--");
                        }
                        try {
                            dataBean.setErrDesc(objArr[54].toString());
                        } catch (NullPointerException e) {
                            dataBean.setErrDesc("--");
                        }
                        try {
                            dataBean.setUserName(objArr[55].toString());
                        } catch (NullPointerException e) {
                            dataBean.setUserName("--");
                        }
                        try {
                            dataBean.setDeviceManufacture(objArr[56].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceManufacture("--");
                        }
                        try {
                            dataBean.setDeviceBuildNumber(objArr[57].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceBuildNumber("--");
                        }
//                        try {
//                            dataBean.setPayTypeDes(objArr[58].toString());
//                        } catch (NullPointerException e) {
//                            dataBean.setPayTypeDes("--");
//                        }
//                        try {
//                            dataBean.setPayType(objArr[59].toString());
//                        } catch (NullPointerException e) {
//                            dataBean.setPayType("--");
//                        }
                        try {
                            if (objArr[60].toString().equals("000") || objArr[60].toString().equals("00")) {
                                dataBean.setT24TranStatus("Success");
                            } else {
                                dataBean.setT24TranStatus(objArr[60].toString());
                            }
                        } catch (NullPointerException e) {
                            dataBean.setT24TranStatus("--");
                        }
                        try {
                            dataBean.setT24TranReference(objArr[61].toString());
                        } catch (NullPointerException e) {
                            dataBean.setT24TranReference("--");
                        }
                        try {
                            if (objArr[62].toString().equals("000") || objArr[62].toString().equals("00")) {
                                dataBean.setIBLTranStatus("Success");
                            } else {
                                dataBean.setIBLTranStatus(objArr[62].toString());
                            }
                        } catch (NullPointerException e) {
                            dataBean.setIBLTranStatus("--");
                        }
                        try {
                            dataBean.setIBLReference(objArr[63].toString());
                        } catch (NullPointerException e) {
                            dataBean.setIBLReference("--");
                        }
                        try {
                            dataBean.setIsOneTime(this.getIsOneTimeDesByCode(objArr[64].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsOneTime("--");
                        }
                        try {
                            dataBean.setIsPayToMobile(this.getIsPayToMobile(objArr[66].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsPayToMobile("No");
                        }
                        try {
                            dataBean.setIsSchedule(this.getIsSchedule(objArr[67].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsSchedule("No");
                        }

                        dataBeanList.add(dataBean);
                    }
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }

        PartialList<TranExplorerBean> list = new PartialList<TranExplorerBean>();

        list.setList(dataBeanList);
        list.setFullCount(count);

        return list;
    }

    private String makeWhereClause(TranExplorerInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 0);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME >='" + datef + "'";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME <'" + datef + "'";
        }

        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += " and lower(M.NIC) LIKE lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(M.CIF) LIKE lower('%" + inputBean.getCif().trim() + "%')";
        }

        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += " and lower(U.TRANSACTION_TYPE) LIKE lower('%" + inputBean.getTxnType().trim() + "%')";
        }
//        if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty()) {
//            where += " and lower(U.RESPONCE_CODE) LIKE lower('%" + inputBean.getResponseCode().trim() + "%')";
//        }
        if (inputBean.getCustomerCategory() != null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(M.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getChannelType() != null && !inputBean.getChannelType().isEmpty()) {
            where += " and lower(U.CHANNEL_TYPE) LIKE lower('%" + inputBean.getChannelType().trim() + "%')";
        }
        if (inputBean.getCurrencyCode() != null && !inputBean.getCurrencyCode().isEmpty()) {
            where += " and lower(U.CURRENCY_CODE) LIKE lower('%" + inputBean.getCurrencyCode().trim() + "%')";
        }
        if (inputBean.getTranRefNo() != null && !inputBean.getTranRefNo().isEmpty()) {
            where += " and lower(U.T24_REFERENCE) LIKE lower('%" + inputBean.getTranRefNo().trim() + "%')";
        }
        if (inputBean.getIblRefNo() != null && !inputBean.getIblRefNo().isEmpty()) {
            where += " and lower(U.IBL_REFERENCE) LIKE lower('%" + inputBean.getIblRefNo().trim() + "%')";
        }
        if (inputBean.getBillCategoryName() != null && !inputBean.getBillCategoryName().isEmpty()) {
            where += " and lower(U.BILL_CATEGORY_NAME) LIKE lower('%" + inputBean.getBillCategoryName().trim() + "%')";
        }
        if (inputBean.getBillProviderName() != null && !inputBean.getBillProviderName().isEmpty()) {
            where += " and lower(U.BILL_PROVIDER_NAME) LIKE lower('%" + inputBean.getBillProviderName().trim() + "%')";
        }
        if (inputBean.getFromAccNo()!= null && !inputBean.getFromAccNo().isEmpty()) {
            where += " and lower(U.FROM_ACCOUNT_NUMBER) LIKE lower('%" + inputBean.getFromAccNo().trim() + "%')";
        }
        if (inputBean.getToAccNo()!= null && !inputBean.getToAccNo().isEmpty()) {
            where += " and lower(U.TO_ACCOUNT_NUMBER) LIKE lower('%" + inputBean.getToAccNo().trim() + "%')";
        }
        if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
            if (inputBean.getStatus().equals("SUCC")) {
                where += " and U.RESPONCE_CODE ='000'";
            } else if (inputBean.getStatus().equals("FAIL")) {
                where += " and (U.RESPONCE_CODE !='000' OR U.RESPONCE_CODE IS null )";
            }
        }
        if (inputBean.getStaffStatus() != null && !inputBean.getStaffStatus().isEmpty()) {
            if (inputBean.getStaffStatus().equals("STAFF")) {
                where += " and M.CUSTOMER_CATEGORY IN ('703')";
            } else if (inputBean.getStaffStatus().equals("NONSTAFF")) {
                where += " and  M.CUSTOMER_CATEGORY NOT IN ('703')";
            }
        }

//        where += " and U.TRANSACTION_TYPE IN ( '49','26','27') ";
        where += " and U.TRANSACTION_TYPE IN ( '26','27') ";
        return where;
    }

    public SwtTxnType getTxnTypeById(String txnid) throws Exception {
        SwtTxnType ttype = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtTxnType as tt where tt.typecode=:typecode";
            Query query = session.createQuery(sql);
            query.setString("typecode", txnid);

            ttype = (SwtTxnType) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return ttype;
    }

    public StringBuffer makeCSVReport(TranExplorerInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;

        String TXN_SQL_CSV = "SELECT "
                + "U.TRANSACTION_ID, "//0
                + "T.DESCRIPTION TXN_TYPE, "//1          
                + "U.FROM_ACCOUNT_NUMBER, "//2
                + "U.TO_ACCOUNT_NUMBER, "//3
                + "U.AMOUNT, "//4
                //                + "U.CUST_CIF, "//5
                //                + "U.CUST_NIC, "//6
                //                + "U.CUST_NAME, "//7
                //                + "U.CUST_EMAIL, "//8
                //                + "U.ADDRESS_1, "//9 
                + "M.CIF, "//5
                + "M.NIC, "//6
                + "M.CUSTOMER_NAME, "//7
                + "M.EMAIL, "//8
                + "M.PERMANENT_ADDRESS, "//9
                + "ST.DESCRIPTION STATUS, "//10 
                + "U.LEASE_MODEL, "//11
                + "U.LEASE_TYPE, "//12
                + "U.NTB_REQUEST_TYPE, "//13
                + "U.POSTED_METHOD, "//14
                + "U.RESPONCE_CODE, "//15
                //                + "S.DESCRIPTION RESPONSE,"
                + "M.MOBILE_NUMBER, "//16
                + "U.SERVICE_FEE, "//17
                + "U.PAYEE_NAME, "//18
                + "U.BILL_CATEGORY_NAME, "//19
                + "U.BILL_PROVIDER_NAME, "//20
                + "U.BILLER_NAME, "//21
                + "U.MERCHANT_TYPE_NAME, "//22
                + "U.MERCHANT_NAME, "//23
                + "U.REDEEM_POINTS, "//24
                + "U.REDEEM_VOUCHER_ID, "//25
                + "U.LEASE_AMOUNT, "//26
                + "U.LEASE_AVD_PAYMENT, "//27
                + "U.LEASE_SELL_PRICE, "//28
                + "U.CHEQUE_NUMBER, "//29
                + "U.FROM_DATE, "//30
                + "U.TO_DATE, "//31
                + "U.CARD_NUMBER, "//32
                + "U.STATEMENT_FROM, "//33
                + "U.STATEMENT_TO, "//34
                + "U.CURRENCY_CODE, "//35
                + "U.BANK_NAME, "//36
                + "U.BRANCH_NAME, "//37
                + "U.REMARKS, "//38
                + "U.ACCOUNT_NO, "//39
                + "U.COLLECT_FROM_TYPE, "//40
                + "U.COLLECTION_BRANCH_ID, "//41
                + "U.PREFERRED_EMAIL, "//42
                + "U.REQUEST_DATE, "//43
                + "U.LASTUPDATED, "//44                
                + "U.LOCAL_TIME, "//45
                + "U.USER_ID, "//46
                + "U.CHANNEL_TYPE, "//47
                + "U.DEVICE_ID, "//48
                + "M.CUSTOMER_CATEGORY, "//49
                + "U.APP_ID, "//50
                + "U.BILL_REFFRENCE_NUMBER, "//51
                + "TM.DESCRIPTION TXN_MODE_DES, "//52
                + "U.TRAN_REF_NO, "//53
                + "U.ERR_DESC, "//54
                + "M.USERNAME, "//55
                + "D.DEVICE_MANUFACTURE, "//56
                + "D.DEVICE_BUILD_NUMBER, "//57
                + "PT.DESCRIPTION PAY_TYPE_DES, "//58
                + "U.TYPE PAY_TYPE, "//59
                + "U.T24_RESPONSE, "//60
                + "U.T24_REFERENCE, "//61
                + "U.IBL_RESPONSE, "//62
                + "U.IBL_REFERENCE, "//63
                + "U.IS_ONE_TIME, "//64
                + "CAST(U.TRANSACTION_TYPE AS VARCHAR2(3)), "//65
                + "U.IS_PAYTO_MOBILE, "//66
                + "U.IS_SCHEDULE "//67
                //                + "D.DEVICE_MODEL "//55
                + "FROM SWT_TRANSACTION U "
                + "LEFT OUTER JOIN SWT_RESPONSE_CODES S ON S.CODE = U.RESPONCE_CODE "
                + "LEFT OUTER JOIN SWT_TXN_TYPE T ON T.TYPECODE = U.TRANSACTION_TYPE "
                + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
                + "LEFT OUTER JOIN STATUS ST ON ST.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN TRANSACTION_MODE TM ON TM.TRAN_TYPE = U.TRANSACTION_TYPE AND TM.TRAN_MODE = U.TXN_MODE "
                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID AND D.USERID = U.USER_ID "
                + "LEFT OUTER JOIN TXN_PAY_TYPE PT ON PT.TRAN_TYPE = U.TRANSACTION_TYPE AND PT.PAY_TYPE = U.TYPE "
                //                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID "
                + "WHERE ";

        try {
            session = HibernateInit.sessionFactory.openSession();
            int count = 0;
            String where1 = this.makeWhereClauseForCSV(inputBean);
            String sqlCount = this.TXN_COUNT_SQL + where1;
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = TXN_SQL_CSV + where1 + this.TRANEXPLORER_ORDER_BY_SQL;
                System.out.println(sql);

                Query query = session.createSQLQuery(sql);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    content = new StringBuffer();
                    List<TranExplorerBean> beanlist = new ArrayList<TranExplorerBean>();

                    for (Object[] objArr : objectArrList) {

                        TranExplorerBean dataBean = new TranExplorerBean();

                        //set data values to beanlist
                        try {

                            if (objArr[0] == null) {
                                dataBean.setTxnId("--");
                            } else {
                                dataBean.setTxnId(objArr[0].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnId("--");
                        }

                        try {
                            if (objArr[1] == null) {
                                dataBean.setTxnType("--");
                            } else {;
                                dataBean.setTxnType(objArr[1].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnType("--");
                        }

                        try {
                            if (objArr[2] == null) {
                                dataBean.setFromAccNo("--");
                            } else {
                                dataBean.setFromAccNo(objArr[2].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromAccNo("--");
                        }
                        try {
                            if (objArr[3] == null) {
                                dataBean.setToAccNo("--");
                            } else {
                                dataBean.setToAccNo(objArr[3].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToAccNo("--");
                        }

                        try {
                            if (objArr[4] == null) {
                                dataBean.setAmount("--");
                            } else {
                                dataBean.setAmount(objArr[4].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAmount("--");
                        }

                        try {
                            if (objArr[5] == null) {
                                dataBean.setCustCif("--");
                            } else {
                                dataBean.setCustCif(objArr[5].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustCif("--");
                        }

                        try {
                            if (objArr[6] == null) {
                                dataBean.setNic("--");
                            } else {
                                dataBean.setNic(objArr[6].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNic("--");
                        }

                        try {
                            if (objArr[7] == null) {
                                dataBean.setCustName("--");
                            } else {
                                dataBean.setCustName(objArr[7].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustName("--");
                        }

                        try {
                            if (objArr[8] == null) {
                                dataBean.setCustEmail("--");
                            } else {
                                dataBean.setCustEmail(objArr[8].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustEmail("--");
                        }

                        try {
                            if (objArr[9] == null) {
                                dataBean.setAddress1("--");
                            } else {
                                dataBean.setAddress1(objArr[9].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAddress1("--");
                        }

                        try {
                            if (objArr[10] == null) {
                                dataBean.setStatus("--");
                            } else {
                                dataBean.setStatus(objArr[10].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatus("--");
                        }

                        try {
                            if (objArr[11] == null) {
                                dataBean.setLeasingModel("--");
                            } else {
                                dataBean.setLeasingModel(objArr[11].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingModel("--");
                        }

                        try {
                            if (objArr[12] == null) {
                                dataBean.setLeasingTypes("--");
                            } else {
                                dataBean.setLeasingTypes(objArr[12].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingTypes("--");
                        }

                        try {
                            if (objArr[13] == null) {
                                dataBean.setNtbRequest("--");
                            } else {
                                dataBean.setNtbRequest(objArr[13].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNtbRequest("--");
                        }

                        try {
                            if (objArr[14] == null) {
                                dataBean.setPostedMethod("--");
                            } else {
                                dataBean.setPostedMethod(objArr[14].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPostedMethod("--");
                        }

                        try {
                            if (objArr[15] == null) {
                                dataBean.setResponseCodes("Failure");
                            } else {
                                if (objArr[15].equals("000")) {
                                    dataBean.setResponseCodes("Success");
                                } else {
                                    dataBean.setResponseCodes("Failure");
                                }
//                                dataBean.setResponseCodes(objArr[15].toString());
                                //dataBean.setResponseCodes(objArr[15].getSwtResponseCodes().getDescription());        
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setResponseCodes("Failure");
                        }

                        try {
                            if (objArr[16] == null) {
                                dataBean.setMobileNumber("--");
                            } else {
                                dataBean.setMobileNumber(objArr[16].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMobileNumber("--");
                        }

                        try {
                            if (objArr[17] == null) {
                                dataBean.setServiceFee("--");
                            } else {
                                dataBean.setServiceFee(objArr[17].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setServiceFee("--");
                        }

                        try {
                            if (objArr[18] == null) {
                                dataBean.setPayeeName("--");
                            } else {
                                dataBean.setPayeeName(objArr[18].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPayeeName("--");
                        }

                        try {
                            if (objArr[19] == null) {
                                dataBean.setBillCategoryName("--");
                            } else {
                                dataBean.setBillCategoryName(objArr[19].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillCategoryName("--");
                        }

                        try {
                            if (objArr[20] == null) {
                                dataBean.setBillProviderName("--");
                            } else {
                                dataBean.setBillProviderName(objArr[20].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillProviderName("--");
                        }

                        try {
                            if (objArr[21] == null) {
                                dataBean.setBillerName("--");
                            } else {
                                dataBean.setBillerName(objArr[21].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillerName("--");
                        }
                        try {
                            if (objArr[22] == null) {
                                dataBean.setMerchantTypeName("--");
                            } else {
                                dataBean.setMerchantTypeName(objArr[22].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantTypeName("--");
                        }

                        try {
                            if (objArr[23] == null) {
                                dataBean.setMerchantName("--");
                            } else {
                                dataBean.setMerchantName(objArr[23].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantName("--");
                        }

                        try {
                            if (objArr[24] == null) {
                                dataBean.setRedeemPoints("--");
                            } else {
                                dataBean.setRedeemPoints(objArr[24].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemPoints("--");
                        }

                        try {
                            if (objArr[25] == null) {
                                dataBean.setRedeemVoucherId("--");
                            } else {
                                dataBean.setRedeemVoucherId(objArr[25].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemVoucherId("--");
                        }

                        try {
                            if (objArr[26] == null) {
                                dataBean.setLeaseAmount("--");
                            } else {
                                dataBean.setLeaseAmount(objArr[26].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAmount("--");
                        }

                        try {
                            if (objArr[27] == null) {
                                dataBean.setLeaseAvdPayment("--");
                            } else {
                                dataBean.setLeaseAvdPayment(objArr[27].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAvdPayment("--");
                        }

                        try {
                            if (objArr[28] == null) {
                                dataBean.setLeaseSellPrice("--");
                            } else {
                                dataBean.setLeaseSellPrice(objArr[28].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseSellPrice("--");
                        }

                        try {
                            if (objArr[29] == null) {
                                dataBean.setChequeNumber("--");
                            } else {
                                dataBean.setChequeNumber(objArr[29].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setChequeNumber("--");
                        }
                        try {
                            if (objArr[30] == null) {
                                dataBean.setFromDate("--");
                            } else {
                                dataBean.setFromDate(objArr[30].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromDate("--");
                        }

                        try {
                            if (objArr[31] == null) {
                                dataBean.setToDate("--");
                            } else {
                                dataBean.setToDate(objArr[31].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToDate("--");
                        }

                        try {
                            if (objArr[32] == null) {
                                dataBean.setCardNumber("--");
                            } else {
//                                dataBean.setCardNumber(toMaskCardNumber(objArr[32].toString()));
                                dataBean.setCardNumber(objArr[32].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCardNumber("--");
                        }

                        try {
                            if (objArr[33] == null) {
                                dataBean.setStatementFrom("--");
                            } else {
                                dataBean.setStatementFrom(objArr[33].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementFrom("--");
                        }

                        try {
                            if (objArr[34] == null) {
                                dataBean.setStatementTo("--");
                            } else {
                                dataBean.setStatementTo(objArr[34].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementTo("--");
                        }

                        try {
                            if (objArr[35] == null) {
                                dataBean.setCurrencyCode("--");
                            } else {
                                dataBean.setCurrencyCode(objArr[35].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCurrencyCode("--");
                        }

                        try {
                            if (objArr[36] == null) {
                                dataBean.setBankName("--");
                            } else {
                                dataBean.setBankName(objArr[36].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBankName("--");
                        }

                        try {
                            if (objArr[37] == null) {
                                dataBean.setBranchName("--");
                            } else {
                                dataBean.setBranchName(objArr[37].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBranchName("--");
                        }
                        try {
                            if (objArr[38] == null) {
                                dataBean.setRemarks("--");
                            } else {
                                dataBean.setRemarks(objArr[38].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRemarks("--");
                        }

                        try {
                            if (objArr[39] == null) {
                                dataBean.setAccountNo("--");
                            } else {
                                dataBean.setAccountNo(objArr[39].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAccountNo("--");
                        }

                        try {
                            if (objArr[40] == null) {
                                dataBean.setCollectFromType("--");
                            } else {
                                dataBean.setCollectFromType(objArr[40].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectFromType("--");
                        }

                        try {
                            if (objArr[41] == null) {
                                dataBean.setCollectionBranchId("--");
                            } else {
                                dataBean.setCollectionBranchId(objArr[41].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectionBranchId("--");
                        }

                        try {
                            if (objArr[42] == null) {
                                dataBean.setPreferredEmail("--");
                            } else {
                                dataBean.setPreferredEmail(objArr[42].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPreferredEmail("--");
                        }

                        try {
                            if (objArr[43] == null) {
                                dataBean.setRequestDate("--");
                            } else {
                                dataBean.setRequestDate(objArr[43].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRequestDate("--");
                        }

                        try {
                            if (objArr[44] == null) {
                                dataBean.setLastupdated("--");
                            } else {
                                dataBean.setLastupdated(objArr[44].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLastupdated("--");
                        }
                        try {
                            if (objArr[45] == null) {
                                dataBean.setLocalTime("--");
                            } else {
                                dataBean.setLocalTime(objArr[45].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLocalTime("--");
                        }
                        try {
                            if (objArr[46] == null) {
                                dataBean.setUserId("--");
                            } else {
                                dataBean.setUserId(objArr[46].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setUserId("--");
                        }
                        try {
                            if (objArr[47] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[47].toString());
                                dataBean.setChannelType(channelTypeDes);
                            } else {
                                dataBean.setChannelType("--");
                            }
                        } catch (Exception e) {
                            dataBean.setChannelType("--");
                        }
                        try {
                            dataBean.setDeviceId(objArr[48].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceId("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        try {
                            if (objArr[49] != null && objArr[49].equals("703")) {
                                dataBean.setStaffStatus("Staff");
                            } else {
                                dataBean.setStaffStatus("Non Staff");
                            }
                        } catch (NullPointerException e) {
                            dataBean.setStaffStatus("--");
                        }

                        try {
                            dataBean.setAppId(objArr[50].toString());
                        } catch (NullPointerException e) {
                            dataBean.setAppId("--");
                        }
                        try {
                            dataBean.setBillRefNo(objArr[51].toString());
                        } catch (NullPointerException e) {
                            dataBean.setBillRefNo("--");
                        }
//                        try {
////                            String txn_type="0";
////                            if(objArr[55]!=null){
////                                txn_type=objArr[55].toString();
////                            }   
////                            dataBean.setTxnMode( getTxnModeDesByCode(txn_type,objArr[52].toString()));
//                            dataBean.setTxnMode(objArr[52].toString());
//                        } catch (NullPointerException e) {
//                            dataBean.setTxnMode("--");
//                        }
                        if (objArr[65] != null && objArr[65].toString().equalsIgnoreCase("27")) {
                            try {
                                dataBean.setPayTypeDes(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setPayTypeDes("Accounts");
                            }
                            try {
                                dataBean.setTxnMode(objArr[58].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                        } else {
                            try {
                                dataBean.setTxnMode(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                            try {
                                dataBean.setPayTypeDes(objArr[58].toString());
                            } catch (NullPointerException e) {
                                dataBean.setPayTypeDes("Accounts");
                            }
                        }
                        try {
                            dataBean.setTranRefNo(objArr[53].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTranRefNo("--");
                        }
//                        if (objArr[15] != null && !objArr[15].equals("")) {
//                            dataBean.setT24TranStatus(objArr[15].toString());
//                        } else {
//                            dataBean.setT24TranStatus("--");
//                        }

//                        try {
//                            dataBean.setPayTypeDes(objArr[58].toString());
//                        } catch (NullPointerException e) {
//                            dataBean.setPayTypeDes("--");
//                        }
//                        try {
//                            dataBean.setPayType(objArr[59].toString());
//                        } catch (NullPointerException e) {
//                            dataBean.setPayType("--");
//                        }
//                        dataBean.setT24TranReference("--");
//                        dataBean.setIBLReference("--");
////                        if (objArr[15] != null && objArr[15].equals("000") && objArr[54] != null && !objArr[54].equals("")) {
//                        if (objArr[54] != null && !objArr[54].equals("")) {
//                            if (objArr[60].toString().equalsIgnoreCase("26") && objArr[15] != null && objArr[15].equals("000")) {
//                                String txnMode = objArr[61].toString();
//                                if (txnMode.equalsIgnoreCase("1")) {
//                                    dataBean.setT24TranReference(objArr[54].toString());
//                                } else if (txnMode.equalsIgnoreCase("2")) {
//                                    dataBean.setT24TranReference(objArr[54].toString());
//                                } else if (txnMode.equalsIgnoreCase("3")) {
//                                    dataBean.setIBLReference(objArr[54].toString());
//                                } else if (txnMode.equalsIgnoreCase("4")) {
//                                    dataBean.setT24TranReference(objArr[54].toString());
//                                } else if (txnMode.equalsIgnoreCase("5")) {
//                                    dataBean.setT24TranReference(objArr[54].toString());
//                                }
//                            } else if (objArr[60].toString().equalsIgnoreCase("27")) {
//
//                                String txnMode = objArr[61].toString();
//                                if (dataBean.getPayType().equalsIgnoreCase("1")) {
//                                    String[] refNos = objArr[54].toString().split("\\|");
//                                    for (String refNo : refNos) {
//                                        if (refNo.toLowerCase().contains("T24".toLowerCase())) {
//                                            dataBean.setT24TranReference(refNo);
//                                        } else if (refNo.toLowerCase().contains("IBL".toLowerCase())) {
//                                            dataBean.setIBLReference(refNo);
//                                        }
//                                    }
//                                } else if (dataBean.getPayType().equalsIgnoreCase("2")) {
//                                    String[] refNos = objArr[54].toString().split("\\|");
//                                    for (String refNo : refNos) {
//                                        if (refNo.toLowerCase().contains("T24".toLowerCase())) {
//                                            dataBean.setT24TranReference(refNo);
//                                        } else if (refNo.toLowerCase().contains("IBL".toLowerCase())) {
//                                            dataBean.setIBLReference(refNo);
//                                        }
//                                    }
//
//                                } else if (dataBean.getPayType().equalsIgnoreCase("3") && objArr[15] != null && objArr[15].equals("000")) {
//
//                                    dataBean.setIBLReference(objArr[54].toString());
//
//                                } else if (dataBean.getPayType().equalsIgnoreCase("4") && txnMode.equalsIgnoreCase("1") && objArr[15] != null && objArr[15].equals("000")) {
//
//                                    dataBean.setIBLReference(objArr[54].toString());
//                                } else if (dataBean.getPayType().equalsIgnoreCase("4") && txnMode.equalsIgnoreCase("2")) {
//
//                                    String[] refNos = objArr[54].toString().split("\\|");
//                                    for (String refNo : refNos) {
//                                        if (refNo.toLowerCase().contains("T24".toLowerCase())) {
//                                            dataBean.setT24TranReference(refNo);
//                                        } else if (refNo.toLowerCase().contains("IBL".toLowerCase())) {
//                                            dataBean.setIBLReference(refNo);
//                                        }
//                                    }
//
//                                } else if (dataBean.getPayType().equalsIgnoreCase("5") && txnMode.equalsIgnoreCase("1") && objArr[15] != null && objArr[15].equals("000")) {
//
//                                    dataBean.setIBLReference(objArr[54].toString());
//
//                                } else if (dataBean.getPayType().equalsIgnoreCase("5") && txnMode.equalsIgnoreCase("2")) {
//
//                                    String[] refNos = objArr[54].toString().split("\\|");
//                                    for (String refNo : refNos) {
//                                        if (refNo.toLowerCase().contains("T24".toLowerCase())) {
//                                            dataBean.setT24TranReference(refNo);
//                                        } else if (refNo.toLowerCase().contains("IBL".toLowerCase())) {
//                                            dataBean.setIBLReference(refNo);
//                                        }
//                                    }
//                                }
//                            }
//                        }
                        try {
                            dataBean.setErrDesc(objArr[54].toString());
                        } catch (NullPointerException e) {
                            dataBean.setErrDesc("--");
                        }

                        try {
                            dataBean.setUserName(objArr[55].toString());
                        } catch (NullPointerException e) {
                            dataBean.setUserName("--");
                        }
                        try {
                            dataBean.setDeviceManufacture(objArr[56].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceManufacture("--");
                        }
                        try {
                            dataBean.setDeviceBuildNumber(objArr[57].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceBuildNumber("--");
                        }
                        try {
                            if (objArr[60].toString().equals("000") || objArr[60].toString().equals("00")) {
                                dataBean.setT24TranStatus("Success");
                            } else {
                                dataBean.setT24TranStatus(objArr[60].toString());
                            }
                        } catch (NullPointerException e) {
                            dataBean.setT24TranStatus("--");
                        }
                        try {
                            dataBean.setT24TranReference(objArr[61].toString());
                        } catch (NullPointerException e) {
                            dataBean.setT24TranReference("--");
                        }
                        try {
                            if (objArr[62].toString().equals("000") || objArr[62].toString().equals("00")) {
                                dataBean.setIBLTranStatus("Success");
                            } else {
                                dataBean.setIBLTranStatus(objArr[62].toString());
                            }
                        } catch (NullPointerException e) {
                            dataBean.setIBLTranStatus("--");
                        }
                        try {
                            dataBean.setIBLReference(objArr[63].toString());
                        } catch (NullPointerException e) {
                            dataBean.setIBLReference("--");
                        }
                        try {
                            dataBean.setIsOneTime(this.getIsOneTimeDesByCode(objArr[64].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsOneTime("--");
                        }
                        try {
                            dataBean.setIsPayToMobile(this.getIsPayToMobile(objArr[66].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsPayToMobile("No");
                        }
                        try {
                            dataBean.setIsSchedule(this.getIsSchedule(objArr[67].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsSchedule("No");
                        }

                        beanlist.add(dataBean);
                    }

                    //write column headers to csv file
                    content.append("Transaction ID");
                    content.append(',');
//                    content.append("Transaction Type");
//                    content.append(',');
//                    content.append("Transaction Reference Number");
//                    content.append(',');
                    content.append("Transaction Time");
                    content.append(',');
                    content.append("Transaction Mode");
                    content.append(',');
                    content.append("Pay Type");
                    content.append(',');
                    content.append("Pay To Mobile");
                    content.append(',');
                    content.append("One Time/Registered");
                    content.append(',');
                    content.append("Schedule");
                    content.append(',');
                    content.append("From AccountNumber");
                    content.append(',');
                    content.append("To Account Number");
                    content.append(',');
//                    content.append("Card Number");
//                    content.append(',');
                    content.append("Currency Code");
                    content.append(',');
                    content.append("Amount");
                    content.append(',');
                    content.append("Channel Type");
                    content.append(',');
                    content.append("Status");
                    content.append(',');
                    content.append("T24 Status");
                    content.append(',');
                    content.append("T24 Reference");
                    content.append(',');
                    content.append("IBL Status");
                    content.append(',');
                    content.append("IBL Reference");
                    content.append(',');
//                    content.append("Response Codes");
//                    content.append(',');
                    content.append("Error Description");
                    content.append(',');
//                    content.append("Unique ID");
//                    content.append(',');
                    content.append("Customer CID");
                    content.append(',');
                    content.append("Customer Category");
                    content.append(',');
                    content.append("Staff/Non Staff");
                    content.append(',');
                    content.append("NIC");
                    content.append(',');
                    content.append("User Name");
                    content.append(',');
                    content.append("Customer Name");
                    content.append(',');
//                    content.append("Customer Email");
//                    content.append(',');
                    content.append("Mobile Number");
                    content.append(',');
//                    content.append("Address");
//                    content.append(',');
                    content.append("App ID");
                    content.append(',');
                    content.append("Device Manufacture");
                    content.append(',');
                    content.append("Device Build Number");
                    content.append(',');
//                    content.append("Device ID");
//                    content.append(',');
                    content.append("Bill Reference Number");
                    content.append(',');
                    content.append("Bill Category Name");
                    content.append(',');
                    content.append("Bill Provider Name");
                    content.append(',');
                    content.append("Biller Name");
                    content.append(',');
                    content.append("Bank Name");
                    content.append(',');
                    content.append("Branch Name");
                    content.append(',');
                    content.append("Remarks");
                    content.append(',');
                    content.append("Payee Name");
                    content.append(',');
                    content.append("Service Fee");
//                    content.append(',');
//                    content.append("Response Code");
//                    content.append(',');
//                    content.append("T24 Transaction Reference");
//                    content.append(',');
//                    content.append("IBL Reference");

//                    content.append(',');
//                    content.append("Leasin Model");
//                    content.append(',');
//                    content.append("Leasing Type");
//                    content.append(',');
//                    content.append("NDB Request");
//                    content.append(',');
//                    content.append("Posted Method");
//                    content.append(',');
//                    content.append("Merchant Type Name");
//                    content.append(',');
//                    content.append("Merchant Name");
//                    content.append(',');
//                    content.append("Redeem Points");
//                    content.append(',');
//                    content.append("Redeem Voucher ID");
//                    content.append(',');
//                    content.append("Lease Amount");
//                    content.append(',');
//                    content.append("Lease Avd Payment");
//                    content.append(',');
//                    content.append("Lease Sell Price");
//                    content.append(',');
//                    content.append("Cheque Number");
//                    content.append(',');
//                    content.append("From Date");
//                    content.append(',');
//                    content.append("To Date");
//                    content.append(',');
//                    content.append("Statement From");
//                    content.append(',');
//                    content.append("Statement To");
//                    content.append(',');
//                    content.append("Account No");
//                    content.append(',');
//                    content.append("Collect From Type");
//                    content.append(',');
//                    content.append("Collection Branch ID");
//                    content.append(',');
//                    content.append("Preferred Email");
//                    content.append(',');
//                    content.append("Request Date");
//                    content.append(',');
//                    content.append("Last Updated");
                    content.append('\n');

                    //write data values to csv file
                    for (TranExplorerBean dataBean : beanlist) {
                        try {
                            if (dataBean.getTxnId() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getTxnId());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

//                        try {
//                            if (dataBean.getTxnType() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getTxnType());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getTranRefNo() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getTranRefNo());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {//22
                            if (dataBean.getLocalTime() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLocalTime());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getTxnMode() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getTxnMode());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getPayTypeDes() == null) {
                                content.append("Accounts");
                                content.append(',');
                            } else {
                                content.append(dataBean.getPayTypeDes());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("Accounts");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIsPayToMobile() == null) {
                                content.append("No");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIsPayToMobile());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("No");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIsOneTime() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIsOneTime());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIsSchedule() == null) {
                                content.append("No");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIsSchedule());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("No");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getFromAccNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getFromAccNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getToAccNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getToAccNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getCardNumber() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCardNumber());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getCurrencyCode() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCurrencyCode());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getAmount() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getAmount());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getChannelType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getChannelType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getStatus() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getStatus());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getResponseCodes() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getResponseCodes());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getT24TranStatus() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getT24TranStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getT24TranReference() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getT24TranReference());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIBLTranStatus() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIBLTranStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIBLReference() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIBLReference());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getErrDesc() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getErrDesc());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getUserId() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getUserId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getCustCif() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustCif());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerCategory() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getStaffStatus() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getStaffStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getNic() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getNic());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getUserName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getUserName()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getCustEmail() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCustEmail());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getMobileNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMobileNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getAddress1() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getAddress1());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getAppId() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getAppId());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDeviceManufacture() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDeviceManufacture());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDeviceBuildNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDeviceBuildNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getDeviceId() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getDeviceId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getBillRefNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillRefNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBillCategoryName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillCategoryName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBillProviderName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillProviderName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {//22
                            if (dataBean.getBillerName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillerName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBankName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBankName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBranchName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBranchName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getRemarks() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getRemarks()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getPayeeName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getPayeeName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getServiceFee() == null) {
                                content.append("--");
//                                content.append(',');
                            } else {
                                content.append(dataBean.getServiceFee());
//                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
//                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getT24TranStatus() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getT24TranStatus());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getT24TranReference() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getT24TranReference());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getIBLReference() == null) {
//                                content.append("--");
////                                content.append(',');
//                            } else {
//                                content.append(dataBean.getIBLReference());
////                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
////                            content.append(',');
//                        }

//                        try {
//                            if (dataBean.getLeasingModel() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeasingModel());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getLeasingTypes() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeasingTypes());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getNtbRequest() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getNtbRequest());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getPostedMethod() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getPostedMethod());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getMerchantTypeName() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getMerchantTypeName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getRedeemPoints() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getRedeemPoints());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getRedeemVoucherId() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getRedeemVoucherId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getLeaseAmount() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeaseAmount());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getLeaseAvdPayment() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeaseAvdPayment());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getLeaseSellPrice() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeaseSellPrice());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getChequeNumber() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getChequeNumber());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getFromDate() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getFromDate());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getToDate() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getToDate());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getStatementFrom() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getStatementFrom());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getStatementTo() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getStatementTo());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getAccountNo() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getAccountNo());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getCollectFromType() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCollectFromType());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getCollectionBranchId() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCollectionBranchId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getPreferredEmail() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getPreferredEmail());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getRequestDate() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getRequestDate());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {//22
//                            if (dataBean.getLastupdated() == null) {
//                                content.append("--");
//                            } else {
//                                content.append(dataBean.getLastupdated());
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                        }
                        content.append('\n');
                    }
                    content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
                    content.append('\n');

                    content.append("Transaction Type :");
                    if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty() && inputBean.getTxnType() != "-ALL-") {
                        String description = getTransactionTypeDescription(inputBean, session);
                        if (description != null) {
                            inputBean.setResponseDescription(description);
                            content.append(description);
                        }
                    } else {
                        inputBean.setResponseDescription("-ALL-");
                        content.append("-ALL-");
                    }
                    content.append('\n');

                    content.append("T24 Reference :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getTranRefNo()));
                    content.append('\n');

                    content.append("IBL Reference :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getIblRefNo()));
                    content.append('\n');

                    content.append("Channel Type :");
                    String channelTypeCode = Common.replaceEmptyorNullStringToALL(inputBean.getChannelType());
                    content.append(this.getChannelTypeDesByCode(channelTypeCode));
                    content.append('\n');

                    content.append("Status :");
                    if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
                        String description = getStatusDescription(inputBean.getStatus());
                        if (description != null) {
                            content.append(description);
                        } else {
                            content.append("-ALL-");
                        }
                    } else {
                        content.append("-ALL-");
                    }
                    content.append('\n');

//                    content.append("Status :");
//                    if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty() ) {
//                        try{
//                        Status status =(Status)session.get(Status.class, inputBean.getStatus());
//                        if (status != null) {
//                            content.append(status.getDescription());
//                        }else{
//                            content.append("-ALL-");
//                        }
//                        }catch (Exception e){
//                             content.append("-ALL-");
//                        }
//                    } else {
//                        content.append("-ALL-");
//                    }
//                    content.append('\n');
//                    
//                    content.append("Response :");
//                    if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty() && inputBean.getResponseCode() != "-ALL-") {
//                        String description = getResponeseDescription(inputBean, session);
//                        if (description != null) {
//                            inputBean.setResponseDescription(description);
//                            content.append(description);
//                        }
//                    } else {
//                        inputBean.setResponseDescription("-ALL-");
//                        content.append("-ALL-");
//                    }
//                    content.append('\n');
                    content.append("Currency Code :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCurrencyCode()));
                    content.append('\n');

                    content.append("Customer NIC :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getNic()));
                    content.append('\n');

                    content.append("Customer Category :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategory()));
                    content.append('\n');

                    content.append("Customer CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
                    content.append('\n');

                    content.append("Bill Category Name :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getBillCategoryName()));
                    content.append('\n');

                    content.append("Bill Provider Name :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getBillProviderName()));
                    content.append('\n');

                    content.append("Staff/Non Staff :");
                    if (inputBean.getStaffStatus() != null && !inputBean.getStaffStatus().isEmpty()) {
                        String description = getStaffStatusDescription(inputBean.getStaffStatus());
                        if (description != null) {
                            content.append(description);
                        } else {
                            content.append("-ALL-");
                        }
                    } else {
                        content.append("-ALL-");
                    }
                    content.append('\n');
                    
                    content.append("From Account Number :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromAccNo()));
                    content.append('\n');

                    content.append("To Account Number :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToAccNo()));
                    content.append('\n');

                }

            }

        } catch (Exception e) {
            throw e;
        } finally {

            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    private String getResponeseDescription(TranExplorerInputBean inputBean, Session session) throws ParseException {

        String rDescription = null;
        try {

            String sql = "select description FROM SwtResponseCodes as tt WHERE tt.code=:responsecode";
            Query query = session.createQuery(sql);
            query.setString("responsecode", inputBean.getResponseCode());

            rDescription = (String) query.list().get(0);

        } catch (Exception e) {
            throw e;
        }
        return rDescription;
    }

    private String getTransactionTypeDescription(TranExplorerInputBean inputBean, Session session) throws ParseException {

        String ttype = null;
        try {

//            session = HibernateInit.sessionFactory.openSession();
            String sql = "select description FROM SwtTxnType as tt WHERE tt.typecode=:txnType";
            Query query = session.createQuery(sql);
            query.setString("txnType", inputBean.getTxnType());

            ttype = (String) query.list().get(0);

        } catch (Exception e) {
            throw e;
        }
        return ttype;
    }

    private String makeWhereClauseForCSV(TranExplorerInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 0);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME >='" + datef + "'";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME <'" + datef + "'";
        }

        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += " and lower(M.NIC) LIKE lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(M.CIF) LIKE lower('%" + inputBean.getCif().trim() + "%')";
        }

        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += " and lower(U.TRANSACTION_TYPE) LIKE lower('%" + inputBean.getTxnType().trim() + "%')";
        }
//        if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty()) {
//            where += " and lower(U.RESPONCE_CODE) LIKE lower('%" + inputBean.getResponseCode().trim() + "%')";
//        }
        if (inputBean.getCustomerCategory() != null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(M.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getChannelType() != null && !inputBean.getChannelType().isEmpty()) {
            where += " and lower(U.CHANNEL_TYPE) LIKE lower('%" + inputBean.getChannelType().trim() + "%')";
        }
        if (inputBean.getCurrencyCode() != null && !inputBean.getCurrencyCode().isEmpty()) {
            where += " and lower(U.CURRENCY_CODE) LIKE lower('%" + inputBean.getCurrencyCode().trim() + "%')";
        }
        if (inputBean.getTranRefNo() != null && !inputBean.getTranRefNo().isEmpty()) {
            where += " and lower(U.T24_REFERENCE) LIKE lower('%" + inputBean.getTranRefNo().trim() + "%')";
        }
        if (inputBean.getIblRefNo() != null && !inputBean.getIblRefNo().isEmpty()) {
            where += " and lower(U.IBL_REFERENCE) LIKE lower('%" + inputBean.getIblRefNo().trim() + "%')";
        }
        if (inputBean.getBillCategoryName() != null && !inputBean.getBillCategoryName().isEmpty()) {
            where += " and lower(U.BILL_CATEGORY_NAME) LIKE lower('%" + inputBean.getBillCategoryName().trim() + "%')";
        }
        if (inputBean.getBillProviderName() != null && !inputBean.getBillProviderName().isEmpty()) {
            where += " and lower(U.BILL_PROVIDER_NAME) LIKE lower('%" + inputBean.getBillProviderName().trim() + "%')";
        }
        if (inputBean.getFromAccNo()!= null && !inputBean.getFromAccNo().isEmpty()) {
            where += " and lower(U.FROM_ACCOUNT_NUMBER) LIKE lower('%" + inputBean.getFromAccNo().trim() + "%')";
        }
        if (inputBean.getToAccNo()!= null && !inputBean.getToAccNo().isEmpty()) {
            where += " and lower(U.TO_ACCOUNT_NUMBER) LIKE lower('%" + inputBean.getToAccNo().trim() + "%')";
        }
//        if (inputBean.getStatus()!= null && !inputBean.getStatus().isEmpty()) {
//            where += " and U.STATUS ='" + inputBean.getStatus().trim() + "'";
//        }
        if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
            if (inputBean.getStatus().equals("SUCC")) {
                where += " and U.RESPONCE_CODE ='000'";
            } else if (inputBean.getStatus().equals("FAIL")) {
                where += " and (U.RESPONCE_CODE !='000' OR U.RESPONCE_CODE IS null )";
            }
        }
        if (inputBean.getStaffStatus() != null && !inputBean.getStaffStatus().isEmpty()) {
            if (inputBean.getStaffStatus().equals("STAFF")) {
                where += " and M.CUSTOMER_CATEGORY IN ('703')";
            } else if (inputBean.getStaffStatus().equals("NONSTAFF")) {
                where += " and  M.CUSTOMER_CATEGORY NOT IN ('703')";
            }
        }

//        where += " and U.TRANSACTION_TYPE IN ( '49','26','27') ";
        where += " and U.TRANSACTION_TYPE IN ( '26','27') ";
        return where;
    }

    public Object generateExcelReport(TranExplorerInputBean inputBean) throws Exception {
        Session session = null;
        Object returnObject = null;
        String CUSTOMER_SQL_SEARCH = "SELECT "
                + "U.TRANSACTION_ID, "//0
                + "T.DESCRIPTION TXN_TYPE, "//1          
                + "U.FROM_ACCOUNT_NUMBER, "//2
                + "U.TO_ACCOUNT_NUMBER, "//3
                + "U.AMOUNT, "//4
                //                + "U.CUST_CIF, "//5
                //                + "U.CUST_NIC, "//6
                //                + "U.CUST_NAME, "//7
                //                + "U.CUST_EMAIL, "//8
                //                + "U.ADDRESS_1, "//9 
                + "M.CIF, "//5
                + "M.NIC, "//6
                + "M.CUSTOMER_NAME, "//7
                + "M.EMAIL, "//8
                + "M.PERMANENT_ADDRESS, "//9 
                + "ST.DESCRIPTION STATUS, "//10 
                + "U.LEASE_MODEL, "//11
                + "U.LEASE_TYPE, "//12
                + "U.NTB_REQUEST_TYPE, "//13
                + "U.POSTED_METHOD, "//14
                + "U.RESPONCE_CODE, "//15
                //                + "S.DESCRIPTION RESPONSE,"
                + "M.MOBILE_NUMBER, "//16
                + "U.SERVICE_FEE, "//17
                + "U.PAYEE_NAME, "//18
                + "U.BILL_CATEGORY_NAME, "//19
                + "U.BILL_PROVIDER_NAME, "//20
                + "U.BILLER_NAME, "//21
                + "U.MERCHANT_TYPE_NAME, "//22
                + "U.MERCHANT_NAME, "//23
                + "U.REDEEM_POINTS, "//24
                + "U.REDEEM_VOUCHER_ID, "//25
                + "U.LEASE_AMOUNT, "//26
                + "U.LEASE_AVD_PAYMENT, "//27
                + "U.LEASE_SELL_PRICE, "//28
                + "U.CHEQUE_NUMBER, "//29
                + "U.FROM_DATE, "//30
                + "U.TO_DATE, "//31
                + "U.CARD_NUMBER, "//32
                + "U.STATEMENT_FROM, "//33
                + "U.STATEMENT_TO, "//34
                + "U.CURRENCY_CODE, "//35
                + "U.BANK_NAME, "//36
                + "U.BRANCH_NAME, "//37
                + "U.REMARKS, "//38
                + "U.ACCOUNT_NO, "//39
                + "U.COLLECT_FROM_TYPE, "//40
                + "U.COLLECTION_BRANCH_ID, "//41
                + "U.PREFERRED_EMAIL, "//42
                + "U.REQUEST_DATE, "//43
                + "U.LASTUPDATED, "//44                
                + "U.LOCAL_TIME, "//45
                + "U.USER_ID, "//46
                + "U.CHANNEL_TYPE, "//47
                + "U.DEVICE_ID, "//48
                + "M.CUSTOMER_CATEGORY, "//49
                + "U.APP_ID, "//50
                + "U.BILL_REFFRENCE_NUMBER, "//51
                + "TM.DESCRIPTION TXN_MODE_DES, "//52
                + "U.TRAN_REF_NO, "//53
                + "U.ERR_DESC, "//54
                + "M.USERNAME, "//55
                + "D.DEVICE_MANUFACTURE, "//56
                + "D.DEVICE_BUILD_NUMBER "//57
                //                + "CAST(U.TRANSACTION_TYPE AS VARCHAR2(3)) "//55
                //                + "D.DEVICE_MODEL "//55
                + "FROM SWT_TRANSACTION U "
                + "LEFT OUTER JOIN SWT_RESPONSE_CODES S ON S.CODE = U.RESPONCE_CODE "
                + "LEFT OUTER JOIN SWT_TXN_TYPE T ON T.TYPECODE = U.TRANSACTION_TYPE "
                + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
                + "LEFT OUTER JOIN STATUS ST ON ST.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN TRANSACTION_MODE TM ON TM.TRAN_TYPE = U.TRANSACTION_TYPE AND TM.TRAN_MODE = U.TXN_MODE "
                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID AND D.USERID = U.USER_ID "
                //                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID "
                + "WHERE ";

        try {

            String directory = ServletActionContext.getServletContext().getInitParameter("tmpreportpath");
            File file = new File(directory);
            if (file.exists()) {
                FileUtils.deleteDirectory(file);
            }

            session = HibernateInit.sessionFactory.openSession();

            int count = 0;
            String where1 = this.makeWhereClauseForExcel(inputBean);
            String sqlCount = this.TXN_COUNT_SQL + where1;
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);
//            queryCount = setDatesToQuery(queryCount, inputBean, session);

//            queryCount = setDatesToQuery(queryCount, inputBean, session);
            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }
//                System.err.println("Count is "+count);
            if (count > 0) {

                long maxRow = Long.parseLong(ServletActionContext.getServletContext().getInitParameter("numberofrowsperexcel"));
                SXSSFWorkbook workbook = this.createExcelTopSection(inputBean, session);
                Sheet sheet = workbook.getSheetAt(0);

                int currRow = headerRowCount;
                int fileCount = 0;

                currRow = this.createExcelTableHeaderSection(workbook, currRow);

                String sql = CUSTOMER_SQL_SEARCH + where1 + this.TRANEXPLORER_ORDER_BY_SQL;
                System.out.println(sql);
                int selectRow = Integer.parseInt(ServletActionContext.getServletContext().getInitParameter("numberofselectrows"));
                int numberOfTimes = count / selectRow;
                if ((count % selectRow) > 0) {
                    numberOfTimes += 1;
                }
                int from = 0;
                int listrownumber = 1;

                for (int i = 0; i < numberOfTimes; i++) {

                    Query query = session.createSQLQuery(sql);
                    query.setFirstResult(from);
                    query.setMaxResults(selectRow);

                    List<Object[]> objectArrList = (List<Object[]>) query.list();
                    if (objectArrList.size() > 0) {

                        for (Object[] objArr : objectArrList) {
                            TranExplorerBean dataBean = new TranExplorerBean();

                            try {

                                if (objArr[0] == null) {
                                    dataBean.setTxnId("--");
                                } else {
                                    dataBean.setTxnId(objArr[0].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setTxnId("--");
                            }

                            try {
                                if (objArr[1] == null) {
                                    dataBean.setTxnType("--");
                                } else {;
                                    dataBean.setTxnType(objArr[1].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setTxnType("--");
                            }

                            try {
                                if (objArr[2] == null) {
                                    dataBean.setFromAccNo("--");
                                } else {
                                    dataBean.setFromAccNo(objArr[2].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setFromAccNo("--");
                            }
                            try {
                                if (objArr[3] == null) {
                                    dataBean.setToAccNo("--");
                                } else {
                                    dataBean.setToAccNo(objArr[3].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setToAccNo("--");
                            }

                            try {
                                if (objArr[4] == null) {
                                    dataBean.setAmount("--");
                                } else {
                                    dataBean.setAmount(objArr[4].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setAmount("--");
                            }

                            try {
                                if (objArr[5] == null) {
                                    dataBean.setCustCif("--");
                                } else {
                                    dataBean.setCustCif(objArr[5].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setCustCif("--");
                            }

                            try {
                                if (objArr[6] == null) {
                                    dataBean.setNic("--");
                                } else {
                                    dataBean.setNic(objArr[6].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setNic("--");
                            }

                            try {
                                if (objArr[7] == null) {
                                    dataBean.setCustName("--");
                                } else {
                                    dataBean.setCustName(objArr[7].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setCustName("--");
                            }

                            try {
                                if (objArr[8] == null) {
                                    dataBean.setCustEmail("--");
                                } else {
                                    dataBean.setCustEmail(objArr[8].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setCustEmail("--");
                            }

                            try {
                                if (objArr[9] == null) {
                                    dataBean.setAddress1("--");
                                } else {
                                    dataBean.setAddress1(objArr[9].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setAddress1("--");
                            }

                            try {
                                if (objArr[10] == null) {
                                    dataBean.setStatus("--");
                                } else {
                                    dataBean.setStatus(objArr[10].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setStatus("--");
                            }

                            try {
                                if (objArr[11] == null) {
                                    dataBean.setLeasingModel("--");
                                } else {
                                    dataBean.setLeasingModel(objArr[11].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setLeasingModel("--");
                            }

                            try {
                                if (objArr[12] == null) {
                                    dataBean.setLeasingTypes("--");
                                } else {
                                    dataBean.setLeasingTypes(objArr[12].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setLeasingTypes("--");
                            }

                            try {
                                if (objArr[13] == null) {
                                    dataBean.setNtbRequest("--");
                                } else {
                                    dataBean.setNtbRequest(objArr[13].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setNtbRequest("--");
                            }

                            try {
                                if (objArr[14] == null) {
                                    dataBean.setPostedMethod("--");
                                } else {
                                    dataBean.setPostedMethod(objArr[14].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setPostedMethod("--");
                            }

                            try {
                                if (objArr[15] == null) {
                                    dataBean.setResponseCodes("Failure");
                                } else {
                                    if (objArr[15].equals("000")) {
                                        dataBean.setResponseCodes("Success");
                                    } else {
                                        dataBean.setResponseCodes("Failure");
                                    }
//                                dataBean.setResponseCodes(objArr[15].toString());
                                    //dataBean.setResponseCodes(objArr[15].getSwtResponseCodes().getDescription());        
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setResponseCodes("Failure");
                            }
                            try {
                                if (objArr[16] == null) {
                                    dataBean.setMobileNumber("--");
                                } else {
                                    dataBean.setMobileNumber(objArr[16].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setMobileNumber("--");
                            }

                            try {
                                if (objArr[17] == null) {
                                    dataBean.setServiceFee("--");
                                } else {
                                    dataBean.setServiceFee(objArr[17].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setServiceFee("--");
                            }

                            try {
                                if (objArr[18] == null) {
                                    dataBean.setPayeeName("--");
                                } else {
                                    dataBean.setPayeeName(objArr[18].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setPayeeName("--");
                            }

                            try {
                                if (objArr[19] == null) {
                                    dataBean.setBillCategoryName("--");
                                } else {
                                    dataBean.setBillCategoryName(objArr[19].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setBillCategoryName("--");
                            }

                            try {
                                if (objArr[20] == null) {
                                    dataBean.setBillProviderName("--");
                                } else {
                                    dataBean.setBillProviderName(objArr[20].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setBillProviderName("--");
                            }

                            try {
                                if (objArr[21] == null) {
                                    dataBean.setBillerName("--");
                                } else {
                                    dataBean.setBillerName(objArr[21].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setBillerName("--");
                            }
                            try {
                                if (objArr[22] == null) {
                                    dataBean.setMerchantTypeName("--");
                                } else {
                                    dataBean.setMerchantTypeName(objArr[22].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setMerchantTypeName("--");
                            }

                            try {
                                if (objArr[23] == null) {
                                    dataBean.setMerchantName("--");
                                } else {
                                    dataBean.setMerchantName(objArr[23].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setMerchantName("--");
                            }

                            try {
                                if (objArr[24] == null) {
                                    dataBean.setRedeemPoints("--");
                                } else {
                                    dataBean.setRedeemPoints(objArr[24].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setRedeemPoints("--");
                            }

                            try {
                                if (objArr[25] == null) {
                                    dataBean.setRedeemVoucherId("--");
                                } else {
                                    dataBean.setRedeemVoucherId(objArr[25].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setRedeemVoucherId("--");
                            }

                            try {
                                if (objArr[26] == null) {
                                    dataBean.setLeaseAmount("--");
                                } else {
                                    dataBean.setLeaseAmount(objArr[26].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setLeaseAmount("--");
                            }

                            try {
                                if (objArr[27] == null) {
                                    dataBean.setLeaseAvdPayment("--");
                                } else {
                                    dataBean.setLeaseAvdPayment(objArr[27].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setLeaseAvdPayment("--");
                            }

                            try {
                                if (objArr[28] == null) {
                                    dataBean.setLeaseSellPrice("--");
                                } else {
                                    dataBean.setLeaseSellPrice(objArr[28].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setLeaseSellPrice("--");
                            }

                            try {
                                if (objArr[29] == null) {
                                    dataBean.setChequeNumber("--");
                                } else {
                                    dataBean.setChequeNumber(objArr[29].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setChequeNumber("--");
                            }
                            try {
                                if (objArr[30] == null) {
                                    dataBean.setFromDate("--");
                                } else {
                                    dataBean.setFromDate(objArr[30].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setFromDate("--");
                            }

                            try {
                                if (objArr[31] == null) {
                                    dataBean.setToDate("--");
                                } else {
                                    dataBean.setToDate(objArr[31].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setToDate("--");
                            }

                            try {
                                if (objArr[32] == null) {
                                    dataBean.setCardNumber("--");
                                } else {
//                                dataBean.setCardNumber(toMaskCardNumber(objArr[32].toString()));
                                    dataBean.setCardNumber(objArr[32].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setCardNumber("--");
                            }

                            try {
                                if (objArr[33] == null) {
                                    dataBean.setStatementFrom("--");
                                } else {
                                    dataBean.setStatementFrom(objArr[33].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setStatementFrom("--");
                            }

                            try {
                                if (objArr[34] == null) {
                                    dataBean.setStatementTo("--");
                                } else {
                                    dataBean.setStatementTo(objArr[34].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setStatementTo("--");
                            }

                            try {
                                if (objArr[35] == null) {
                                    dataBean.setCurrencyCode("--");
                                } else {
                                    dataBean.setCurrencyCode(objArr[35].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setCurrencyCode("--");
                            }

                            try {
                                if (objArr[36] == null) {
                                    dataBean.setBankName("--");
                                } else {
                                    dataBean.setBankName(objArr[36].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setBankName("--");
                            }

                            try {
                                if (objArr[37] == null) {
                                    dataBean.setBranchName("--");
                                } else {
                                    dataBean.setBranchName(objArr[37].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setBranchName("--");
                            }
                            try {
                                if (objArr[38] == null) {
                                    dataBean.setRemarks("--");
                                } else {
                                    dataBean.setRemarks(objArr[38].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setRemarks("--");
                            }

                            try {
                                if (objArr[39] == null) {
                                    dataBean.setAccountNo("--");
                                } else {
                                    dataBean.setAccountNo(objArr[39].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setAccountNo("--");
                            }

                            try {
                                if (objArr[40] == null) {
                                    dataBean.setCollectFromType("--");
                                } else {
                                    dataBean.setCollectFromType(objArr[40].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setCollectFromType("--");
                            }

                            try {
                                if (objArr[41] == null) {
                                    dataBean.setCollectionBranchId("--");
                                } else {
                                    dataBean.setCollectionBranchId(objArr[41].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setCollectionBranchId("--");
                            }

                            try {
                                if (objArr[42] == null) {
                                    dataBean.setPreferredEmail("--");
                                } else {
                                    dataBean.setPreferredEmail(objArr[42].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setPreferredEmail("--");
                            }

                            try {
                                if (objArr[43] == null) {
                                    dataBean.setRequestDate("--");
                                } else {
                                    dataBean.setRequestDate(objArr[43].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setRequestDate("--");
                            }

                            try {
                                if (objArr[44] == null) {
                                    dataBean.setLastupdated("--");
                                } else {
                                    dataBean.setLastupdated(objArr[44].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setLastupdated("--");
                            }
                            try {
                                if (objArr[45] == null) {
                                    dataBean.setLocalTime("--");
                                } else {
                                    dataBean.setLocalTime(objArr[45].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setLocalTime("--");
                            }
                            try {
                                if (objArr[46] == null) {
                                    dataBean.setUserId("--");
                                } else {
                                    dataBean.setUserId(objArr[46].toString());
                                }
                            } catch (NullPointerException npe) {
                                dataBean.setUserId("--");
                            }
                            try {
                                if (objArr[47] != null) {
                                    String channelTypeDes = this.getChannelTypeDesByCode(objArr[47].toString());
                                    dataBean.setChannelType(channelTypeDes);
                                } else {
                                    dataBean.setChannelType("--");
                                }
                            } catch (Exception e) {
                                dataBean.setChannelType("--");
                            }
                            try {
                                dataBean.setDeviceId(objArr[48].toString());
                            } catch (NullPointerException e) {
                                dataBean.setDeviceId("--");
                            }
                            try {
                                dataBean.setCustomerCategory(objArr[49].toString());
                            } catch (NullPointerException e) {
                                dataBean.setCustomerCategory("--");
                            }
                            try {
                                dataBean.setCustomerCategory(objArr[49].toString());
                            } catch (NullPointerException e) {
                                dataBean.setCustomerCategory("--");
                            }

                            try {
                                dataBean.setAppId(objArr[50].toString());
                            } catch (NullPointerException e) {
                                dataBean.setAppId("--");
                            }
                            try {
                                dataBean.setBillRefNo(objArr[51].toString());
                            } catch (NullPointerException e) {
                                dataBean.setBillRefNo("--");
                            }
                            try {
//                            String txn_type="0";
//                            if(objArr[55]!=null){
//                                txn_type=objArr[55].toString();
//                            } 
//                            dataBean.setTxnMode( getTxnModeDesByCode(txn_type,objArr[52].toString()));
                                dataBean.setTxnMode(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                            try {
                                dataBean.setTranRefNo(objArr[53].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTranRefNo("--");
                            }
                            try {
                                dataBean.setErrDesc(objArr[54].toString());
                            } catch (NullPointerException e) {
                                dataBean.setErrDesc("--");
                            }
                            try {
                                dataBean.setUserName(objArr[55].toString());
                            } catch (NullPointerException e) {
                                dataBean.setUserName("--");
                            }
                            try {
                                dataBean.setDeviceManufacture(objArr[56].toString());
                            } catch (NullPointerException e) {
                                dataBean.setDeviceManufacture("--");
                            }
                            try {
                                dataBean.setDeviceBuildNumber(objArr[57].toString());
                            } catch (NullPointerException e) {
                                dataBean.setDeviceBuildNumber("--");
                            }

                            dataBean.setFullCount(count);

                            if (currRow + 1 > maxRow) {
                                fileCount++;
                                this.writeTemporaryFile(workbook, fileCount, directory);
                                workbook = this.createExcelTopSection(inputBean, session);
                                sheet = workbook.getSheetAt(0);
                                currRow = headerRowCount;
                                this.createExcelTableHeaderSection(workbook, currRow);
                            }
                            currRow = this.createExcelTableBodySection(workbook, dataBean, currRow, listrownumber);
                            listrownumber++;
                            if (currRow % 100 == 0) {
                                ((SXSSFSheet) sheet).flushRows(100); // retain 100 last rows and flush all others

                                // ((SXSSFSheet)sh).flushRows() is a shortcut for ((SXSSFSheet)sh).flushRows(0),
                                // this method flushes all rows
                            }
                        }
                    }
                    from = from + selectRow;
                }

                Date createdTime = CommonDAO.getSystemDate(session);
                this.createExcelBotomSection(workbook, currRow, count, createdTime);
                System.out.println("--------" + currRow);
                if (fileCount > 0) {
                    fileCount++;
                    this.writeTemporaryFile(workbook, fileCount, directory);
                    ByteArrayOutputStream outputStream = Common.zipFiles(file.listFiles());
                    returnObject = outputStream;
                    workbook.dispose();
                } else {
//                    for (int i = 0; i < columnCount; i++) {
//                        //to auto size all column in the sheet
//                        sheet.autoSizeColumn(i);
//                    }

                    returnObject = workbook;
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return returnObject;
    }

    private String makeWhereClauseForExcel(TranExplorerInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 0);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME >='" + datef + "'";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME <'" + datef + "'";
        }

        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += " and lower(M.NIC) LIKE lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(M.CIF) LIKE lower('%" + inputBean.getCif().trim() + "%')";
        }

        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += " and lower(U.TRANSACTION_TYPE) LIKE lower('%" + inputBean.getTxnType().trim() + "%')";
        }
//        if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty()) {
//            where += " and lower(U.RESPONCE_CODE) LIKE lower('%" + inputBean.getResponseCode().trim() + "%')";
//        }
        if (inputBean.getCustomerCategory() != null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(M.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getChannelType() != null && !inputBean.getChannelType().isEmpty()) {
            where += " and lower(U.CHANNEL_TYPE) LIKE lower('%" + inputBean.getChannelType().trim() + "%')";
        }
        if (inputBean.getCurrencyCode() != null && !inputBean.getCurrencyCode().isEmpty()) {
            where += " and lower(U.CURRENCY_CODE) LIKE lower('%" + inputBean.getCurrencyCode().trim() + "%')";
        }
        if (inputBean.getTranRefNo() != null && !inputBean.getTranRefNo().isEmpty()) {
            where += " and lower(U.TRAN_REF_NO) LIKE lower('%" + inputBean.getTranRefNo().trim() + "%')";
        }
        if (inputBean.getBillCategoryName() != null && !inputBean.getBillCategoryName().isEmpty()) {
            where += " and lower(U.BILL_CATEGORY_NAME) LIKE lower('%" + inputBean.getBillCategoryName().trim() + "%')";
        }
        if (inputBean.getBillProviderName() != null && !inputBean.getBillProviderName().isEmpty()) {
            where += " and lower(U.BILL_PROVIDER_NAME) LIKE lower('%" + inputBean.getBillProviderName().trim() + "%')";
        }
//        if (inputBean.getStatus()!= null && !inputBean.getStatus().isEmpty()) {
//            where += " and U.STATUS ='" + inputBean.getStatus().trim() + "'";
//        }
        if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
            if (inputBean.getStatus().equals("SUCC")) {
                where += " and U.RESPONCE_CODE ='000'";
            } else if (inputBean.getStatus().equals("FAIL")) {
                where += " and (U.RESPONCE_CODE !='000' OR U.RESPONCE_CODE IS null )";
            }
        }

        where += " and U.TRANSACTION_TYPE IN ( '49','26','27') ";
        return where;
    }

    private SXSSFWorkbook createExcelTopSection(TranExplorerInputBean inputBean, Session session) throws Exception {

        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
        Sheet sheet = workbook.createSheet("Transaction_Explorer_Report");

        CellStyle fontBoldedUnderlinedCell = ExcelCommon.getFontBoldedUnderlinedCell(workbook);

        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue("NDB MB Solution");
        cell.setCellStyle(fontBoldedUnderlinedCell);

        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Transaction Explorer Report");
        cell.setCellStyle(fontBoldedUnderlinedCell);

        row = sheet.createRow(4);
        cell = row.createCell(0);
        cell.setCellValue("From Date");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(5);
        cell = row.createCell(0);
        cell.setCellValue("To Date");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(6);
        cell = row.createCell(0);
        cell.setCellValue("Transaction Type");
        cell = row.createCell(1);
        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            SwtTxnType swtTxnType = (SwtTxnType) session.get(SwtTxnType.class, inputBean.getTxnType());
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(swtTxnType.getDescription()));
        } else {
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getTxnType()));
        }
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(7);
        cell = row.createCell(0);
        cell.setCellValue("Transaction Reference Number");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getTranRefNo()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(8);
        cell = row.createCell(0);
        cell.setCellValue("Channel Type");
        cell = row.createCell(1);
        String channelTypeCode = Common.replaceEmptyorNullStringToALL(inputBean.getChannelType());
        cell.setCellValue(this.getChannelTypeDesByCode(channelTypeCode));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(9);
        cell = row.createCell(0);
        cell.setCellValue("Status");
        cell = row.createCell(1);
        String description = this.getStatusDescription(inputBean.getStatus());
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(description));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

//        row = sheet.createRow(9);
//        cell = row.createCell(0);
//        cell.setCellValue("Status");
//        cell = row.createCell(1);
//        if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
//            Status status = (Status) session.get(Status.class, inputBean.getStatus());
//            cell.setCellValue(Common.replaceEmptyorNullStringToALL(status.getDescription()));
//        } else {
//            cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getStatus()));
//        }
//        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));
//        
//        row = sheet.createRow(10);
//        cell = row.createCell(0);
//        cell.setCellValue("Response");
//        cell = row.createCell(1);
//        if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty()) {
//            SwtResponseCodes swtResponseCodes = (SwtResponseCodes) session.get(SwtResponseCodes.class, inputBean.getResponseCode());
//            cell.setCellValue(Common.replaceEmptyorNullStringToALL(swtResponseCodes.getDescription()));
//        } else {
//            cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getResponseCode()));
//        }
//        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));
        row = sheet.createRow(10);
        cell = row.createCell(0);
        cell.setCellValue("Currency Code");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCurrencyCode()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(11);
        cell = row.createCell(0);
        cell.setCellValue("CID");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(12);
        cell = row.createCell(0);
        cell.setCellValue("Customer Category");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategory()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(13);
        cell = row.createCell(0);
        cell.setCellValue("NIC");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getNic()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(14);
        cell = row.createCell(0);
        cell.setCellValue("Bill Category Name");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getBillCategoryName()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(15);
        cell = row.createCell(0);
        cell.setCellValue("Bill Provider Name");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getBillProviderName()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

//        
        return workbook;
    }

    private int createExcelTableHeaderSection(SXSSFWorkbook workbook, int currrow) throws Exception {
        CellStyle columnHeaderCell = ExcelCommon.getColumnHeadeCell(workbook);
        Sheet sheet = workbook.getSheetAt(0);

        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue("No");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(1);
        cell.setCellValue("Transaction ID");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(2);
        cell.setCellValue("Transaction Type");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(3);
        cell.setCellValue("Transaction Reference Number");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(4);
        cell.setCellValue("Transaction Time");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(5);
        cell.setCellValue("Transaction Mode");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(6);
        cell.setCellValue("From AccountNumber");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(7);
        cell.setCellValue("To Account Number");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(8);
        cell.setCellValue("Card Number");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(9);
        cell.setCellValue("Currency Code");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(10);
        cell.setCellValue("Amount");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(11);
        cell.setCellValue("Channel Type");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(12);
        cell.setCellValue("Status");
        cell.setCellStyle(columnHeaderCell);

//        cell = row.createCell(13);
//        cell.setCellValue("Response Codes");
//        cell.setCellStyle(columnHeaderCell);
        cell = row.createCell(13);
        cell.setCellValue("Error Description");
        cell.setCellStyle(columnHeaderCell);

//        cell = row.createCell(15);
//        cell.setCellValue("Unique ID");
//        cell.setCellStyle(columnHeaderCell);
        cell = row.createCell(14);
        cell.setCellValue("CID");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(15);
        cell.setCellValue("Customer Category");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(16);
        cell.setCellValue("NIC");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(17);
        cell.setCellValue("User Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(18);
        cell.setCellValue("Customer Name");
        cell.setCellStyle(columnHeaderCell);

//        cell = row.createCell(21);
//        cell.setCellValue("Customer Email");
//        cell.setCellStyle(columnHeaderCell);
        cell = row.createCell(19);
        cell.setCellValue("Mobile Number");
        cell.setCellStyle(columnHeaderCell);

//        cell = row.createCell(23);
//        cell.setCellValue("Address");
//        cell.setCellStyle(columnHeaderCell);
        cell = row.createCell(20);
        cell.setCellValue("App ID");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(21);
        cell.setCellValue("Device Manufacture");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(22);
        cell.setCellValue("Device Build Number");
        cell.setCellStyle(columnHeaderCell);

//        cell = row.createCell(25);
//        cell.setCellValue("Device ID");
//        cell.setCellStyle(columnHeaderCell);
        cell = row.createCell(23);
        cell.setCellValue("Bill Reference Number");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(24);
        cell.setCellValue("Bill Category Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(25);
        cell.setCellValue("Bill Provider Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(26);
        cell.setCellValue("Biller Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(27);
        cell.setCellValue("Bank Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(28);
        cell.setCellValue("Branch Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(29);
        cell.setCellValue("Remarks");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(30);
        cell.setCellValue("Payee Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(31);
        cell.setCellValue("Service Fee");
        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(34);
//        cell.setCellValue("Leasin Model");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(35);
//        cell.setCellValue("Leasing Type");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(36);
//        cell.setCellValue("NDB Request");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(37);
//        cell.setCellValue("Posted Method");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(38);
//        cell.setCellValue("Merchant Type Name");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(39);
//        cell.setCellValue("Merchant Name");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(40);
//        cell.setCellValue("Redeem Point");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(41);
//        cell.setCellValue("Redeem Voucher ID");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(42);
//        cell.setCellValue("Lease Amount");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(43);
//        cell.setCellValue("Lease Avd Payment");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(44);
//        cell.setCellValue("Lease Sell Price");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(45);
//        cell.setCellValue("Cheque Number");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(46);
//        cell.setCellValue("From Date");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(47);
//        cell.setCellValue("To Date");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(48);
//        cell.setCellValue("Statement From");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(49);
//        cell.setCellValue("Statement To");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(50);
//        cell.setCellValue("Account No");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(51);
//        cell.setCellValue("Collect From Type");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(52);
//        cell.setCellValue("Collection Branch ID");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(53);
//        cell.setCellValue("Preferred Email");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(54);
//        cell.setCellValue("Request Date");
//        cell.setCellStyle(columnHeaderCell);
//
//        cell = row.createCell(55);
//        cell.setCellValue("Last Updated");
//        cell.setCellStyle(columnHeaderCell);

        return currrow;
    }

    private void writeTemporaryFile(SXSSFWorkbook workbook, int fileCount, String directory) throws Exception {
        File file;
        FileOutputStream outputStream = null;
        try {
            Sheet sheet = workbook.getSheetAt(0);
//            for (int i = 0; i < columnCount; i++) {
//                //to auto size all column in the sheet
////                sheet.autoSizeColumn(i);
//            }

            file = new File(directory);
            if (!file.exists()) {
                System.out.println("Directory created or not : " + file.mkdirs());
            }

            if (fileCount > 0) {
                file = new File(directory + File.separator + "Tranasaction Explorer Report_" + fileCount + ".xlsx");
            } else {
                file = new File(directory + File.separator + "Transaction Explorer Report.xlsx");
            }
            outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
        } catch (IOException e) {
            throw e;
        } finally {
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
        }
    }

    private int createExcelTableBodySection(SXSSFWorkbook workbook, TranExplorerBean dataBean, int currrow, int rownumber) throws Exception {
        Sheet sheet = workbook.getSheetAt(0);
        CellStyle rowColumnCell = ExcelCommon.getRowColumnCell(workbook);
        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue(rownumber);
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(1);
        cell.setCellValue(dataBean.getTxnId());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(2);
        cell.setCellValue(dataBean.getTxnType());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(3);
        cell.setCellValue(dataBean.getTranRefNo());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(4);
        cell.setCellValue(dataBean.getLocalTime());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(5);
        cell.setCellValue(dataBean.getTxnMode());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(6);
        cell.setCellValue(dataBean.getFromAccNo());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(7);
        cell.setCellValue(dataBean.getToAccNo());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(8);
        cell.setCellValue(dataBean.getCardNumber());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(9);
        cell.setCellValue(dataBean.getCurrencyCode());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(10);
        cell.setCellValue(dataBean.getAmount());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(11);
        cell.setCellValue(dataBean.getChannelType());
        cell.setCellStyle(rowColumnCell);

//        cell = row.createCell(12);
//        cell.setCellValue(dataBean.getStatus());
//        cell.setCellStyle(rowColumnCell);
        cell = row.createCell(12);
        cell.setCellValue(dataBean.getResponseCodes());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(13);
        cell.setCellValue(dataBean.getErrDesc());
        cell.setCellStyle(rowColumnCell);

//        cell = row.createCell(15);
//        cell.setCellValue(dataBean.getUserId());
//        cell.setCellStyle(rowColumnCell);
        cell = row.createCell(14);
        cell.setCellValue(dataBean.getCustCif());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(15);
        cell.setCellValue(dataBean.getCustomerCategory());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(16);
        cell.setCellValue(dataBean.getNic());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(17);
        cell.setCellValue(dataBean.getUserName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(18);
        cell.setCellValue(dataBean.getCustName());
        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(21);
//        cell.setCellValue(dataBean.getCustEmail());
//        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(19);
        cell.setCellValue(dataBean.getMobileNumber());
        cell.setCellStyle(rowColumnCell);

//        cell = row.createCell(23);
//        cell.setCellValue(dataBean.getAddress1());
//        cell.setCellStyle(rowColumnCell);
        cell = row.createCell(20);
        cell.setCellValue(dataBean.getAppId());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(21);
        cell.setCellValue(dataBean.getDeviceManufacture());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(22);
        cell.setCellValue(dataBean.getDeviceBuildNumber());
        cell.setCellStyle(rowColumnCell);

//        cell = row.createCell(24);
//        cell.setCellValue(dataBean.getDeviceId());
//        cell.setCellStyle(rowColumnCell);
        cell = row.createCell(23);
        cell.setCellValue(dataBean.getBillRefNo());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(24);
        cell.setCellValue(dataBean.getBillCategoryName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(25);
        cell.setCellValue(dataBean.getBillProviderName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(26);
        cell.setCellValue(dataBean.getBillerName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(27);
        cell.setCellValue(dataBean.getBankName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(28);
        cell.setCellValue(dataBean.getBranchName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(29);
        cell.setCellValue(dataBean.getRemarks());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(30);
        cell.setCellValue(dataBean.getPayeeName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(31);
        cell.setCellValue(dataBean.getServiceFee());
        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(34);
//        cell.setCellValue(dataBean.getLeasingModel());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(35);
//        cell.setCellValue(dataBean.getLeasingTypes());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(36);
//        cell.setCellValue(dataBean.getNtbRequest());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(37);
//        cell.setCellValue(dataBean.getPostedMethod());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(38);
//        cell.setCellValue(dataBean.getMerchantTypeName());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(39);
//        cell.setCellValue(dataBean.getMerchantName());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(40);
//        cell.setCellValue(dataBean.getRedeemPoints());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(41);
//        cell.setCellValue(dataBean.getRedeemVoucherId());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(42);
//        cell.setCellValue(dataBean.getLeaseAmount());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(43);
//        cell.setCellValue(dataBean.getLeaseAvdPayment());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(44);
//        cell.setCellValue(dataBean.getLeaseSellPrice());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(45);
//        cell.setCellValue(dataBean.getChequeNumber());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(46);
//        cell.setCellValue(dataBean.getFromDate());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(47);
//        cell.setCellValue(dataBean.getToDate());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(48);
//        cell.setCellValue(dataBean.getStatementFrom());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(49);
//        cell.setCellValue(dataBean.getStatementTo());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(50);
//        cell.setCellValue(dataBean.getAccountNo());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(51);
//        cell.setCellValue(dataBean.getCollectFromType());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(52);
//        cell.setCellValue(dataBean.getCollectionBranchId());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(53);
//        cell.setCellValue(dataBean.getPreferredEmail());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(54);
//        cell.setCellValue(dataBean.getRequestDate());
//        cell.setCellStyle(rowColumnCell);
//
//        cell = row.createCell(55);
//        cell.setCellValue(dataBean.getLastupdated());
//        cell.setCellStyle(rowColumnCell);

//        sheet.autoSizeColumn(9);
        return currrow;
    }

    private void createExcelBotomSection(SXSSFWorkbook workbook, int currrow, long count, Date date) throws Exception {

        CellStyle fontBoldedCell = ExcelCommon.getFontBoldedCell(workbook);
        Sheet sheet = workbook.getSheetAt(0);

        currrow++;
        Row row = sheet.createRow(currrow++);
        Cell cell = row.createCell(0);
        cell.setCellValue("Summary");
        cell.setCellStyle(fontBoldedCell);

        row = sheet.createRow(currrow++);
        cell = row.createCell(0);
        cell.setCellValue("Total Record Count");
        cell = row.createCell(1);
        cell.setCellValue(count);
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(currrow++);
        cell = row.createCell(0);
        cell.setCellValue("Report Created Time and Date");
        cell = row.createCell(1);
        cell.setCellValue(date.toString().substring(0, 19));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));
    }

    private HashMap<String, String> getChannelTypeMap() {

        HashMap<String, String> channelTypeMap = new HashMap<String, String>();

        channelTypeMap.put("2", "Internet Banking");
        channelTypeMap.put("1", "Mobile Banking");

        return channelTypeMap;

    }

    public String getChannelTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("2")) {
            des = "Internet Banking";
        } else if (code.equals("1")) {
            des = "Mobile Banking";
        } else {
            des = code;
        }
        return des;

    }

    public String getIsOneTimeDesByCode(String code) {
        String des = "--";
        if (code.equals("1")) {
            des = "One Time Transaction";
        } else if (code.equals("0")) {
            des = "Registered Transaction";
        } else {
            des = code;
        }
        return des;

    }

    public String getIsPayToMobile(String code) {
        String des = "No";
        if (code.equals("1")) {
            des = "Yes";
        }
        return des;

    }

    public String getIsSchedule(String code) {
        String des = "No";
        if (code.equals("1")) {
            des = "Yes";
        }
        return des;

    }

//    private String getTxnModeDesByCode(String txntype,String code) {
//        String des = "--";
//        if(txntype.equals("26")){
//            if (code.equals("1")) {
//                des = "Own Fund Transfer";
//            } else if (code.equals("2")) {
//                des = "Internal Fund Transfer";
//            } else if (code.equals("3")) {
//                des = "3rd Party CEFT Transfer";
//            } else if (code.equals("4")) {
//                des = "3rd Party SLIPS Transfer";
//            } else {
//                des = code;
//            }
//        }else if(txntype.equals("27")){
//            if (code.equals("1")) {
//                des = "Own Card Payments";
//            } else if (code.equals("2")) {
//                des = "Other NDB Card Payment";
//            } else if (code.equals("3")) {
//                des = "Other Bank Card Payment";
//            } else if (code.equals("4")) {
//                des = "Bill Payment";
//            } else if (code.equals("5")) {
//                des = "Mobile Reload";
//            } else {
//                des = code;
//            }
//        }else if(txntype.equals("49")){
//            if (code.equals("1")) {
//                des = "Card On";
//            } else if (code.equals("2")) {
//                des = "Card Off";
//            } else if (code.equals("3")) {
//                des = "Card Activate";
//            } else if (code.equals("4")) {
//                des = "Card Deactivate";
//            } else {
//                des = code;
//            }
//        }else{
//            des = code;
//        }
//        return des;
//
//    }
    private String toMaskCardNumber(String cardNumber) {

        StringBuilder maskCardNumber = new StringBuilder();
        String mask = "**********************";
        if (cardNumber.length() > 4) {
            int maskLength = cardNumber.length() - 4;
            maskCardNumber
                    .append(mask.substring(0, maskLength))
                    .append(cardNumber.substring(maskLength, cardNumber.length()));
        } else {
            maskCardNumber.append(cardNumber);
        }
        return maskCardNumber.toString();
    }

    public String getStatusDescription(String status) throws ParseException {

        String rDescription = null;

        if (status != null && !status.isEmpty()) {
            if (status.equals("SUCC")) {
                rDescription = "Success";
            } else if (status.equals("FAIL")) {
                rDescription = "Failure";
            }
        }
        return rDescription;
    }

    public String getStaffStatusDescription(String status) throws ParseException {

        String rDescription = null;

        if (status != null && !status.isEmpty()) {
            if (status.equals("STAFF")) {
                rDescription = "Staff";
            } else if (status.equals("NONSTAFF")) {
                rDescription = "Non Staff";
            }
        }
        return rDescription;
    }

    public Object generateSummeryExcelReport(TranExplorerInputBean inputBean) throws Exception {
        Session session = null;
        Object returnObject = null;
        String whereClause = " 1=1 ";

        try {
            session = HibernateInit.sessionFactory.openSession();

            whereClause = this.makeWhereClauseForExcelSummery(inputBean);

            String TXN_SUMMERY_SQL_SEARCH = "SELECT COUNT(TR.TRANSACTION_ID) no_of_tran, "
                    + "NVL(SUM(TR.AMOUNT),0) amount, "
                    + "TY.DESCRIPTION tran_type, "
                    + "CASE "
                    + "WHEN TR.IS_ONE_TIME =1 THEN 'One Time ' "
                    + "ELSE 'Registered ' "
                    + "END || 'Pay to Mobile' tran_mode, "
                    + "CASE "
                    + "WHEN COUNT(TR.TRANSACTION_ID) =0 THEN 'LKR' "
                    + "ELSE NVL(TR.CURRENCY_CODE, '--') "
                    + "END currency "
                    + "FROM "
                    + "TRANSACTION_MODE TM "
                    + "LEFT OUTER JOIN "
                    + "(SWT_TRANSACTION TR LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = TR.USER_ID) "
                    + "ON "
                    + "TM.TRAN_TYPE = TR.TRANSACTION_TYPE AND "
                    + "TM.TRAN_MODE = TR.TXN_MODE AND "
                    + whereClause
                    + "JOIN "
                    + "SWT_TXN_TYPE  TY "
                    + "ON "
                    + "TM.TRAN_TYPE = TY.TYPECODE "
                    + "WHERE 1=1 AND "
                    + "TM.TRAN_TYPE IN ('26') "
                    + "AND TR.IS_PAYTO_MOBILE = 1 "
                    + "GROUP BY TY.DESCRIPTION,TM.DESCRIPTION,TR.CURRENCY_CODE,TR.IS_ONE_TIME "
                    
                    + "UNION ALL "
                    
                    + "SELECT COUNT(TR.TRANSACTION_ID) no_of_tran, "
                    + "NVL(SUM(TR.AMOUNT),0) amount, "
                    + "TY.DESCRIPTION tran_type, "
                    + "CASE "
                    + "WHEN TR.IS_ONE_TIME =1 THEN 'One Time ' "
                    + "ELSE 'Registered ' "
                    + "END || TM.DESCRIPTION tran_mode, "
                    + "CASE "
                    + "WHEN COUNT(TR.TRANSACTION_ID) =0 THEN 'LKR' "
                    + "ELSE NVL(TR.CURRENCY_CODE, '--') "
                    + "END currency "
                    + "FROM "
                    + "TRANSACTION_MODE TM "
                    + "LEFT OUTER JOIN "
                    + "(SWT_TRANSACTION TR LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = TR.USER_ID ) "
                    + "ON "
                    + "TM.TRAN_TYPE = TR.TRANSACTION_TYPE AND "
                    + "TM.TRAN_MODE = TR.TXN_MODE  AND "
                    + whereClause
                    + "LEFT OUTER JOIN "
                    + "SWT_TXN_TYPE TY "
                    + "ON "
                    + "TM.TRAN_TYPE = TY.TYPECODE "
                    + "WHERE 1=1 AND "
                    + "TM.TRAN_TYPE IN ('26') AND "
                    + "( TR.IS_PAYTO_MOBILE != 1 OR TR.IS_PAYTO_MOBILE IS NULL ) "
                    + "GROUP BY TY.DESCRIPTION,TM.DESCRIPTION,TR.CURRENCY_CODE,TR.IS_ONE_TIME "
                    
                    + "UNION ALL "
                    
                    + "SELECT COUNT(TR.TRANSACTION_ID) no_of_tran, "
                    + "NVL(SUM(TR.AMOUNT),0) amount, "
                    + "TY.DESCRIPTION tran_type, "
                    + "CASE "
                    + "WHEN TR.IS_ONE_TIME =1 THEN 'One Time ' "
                    + "ELSE 'Registered ' "
                    + "END || TP.DESCRIPTION tran_mode, "
                    + "CASE "
                    + "WHEN COUNT(TR.TRANSACTION_ID) =0 THEN 'LKR' "
                    + "ELSE NVL(TR.CURRENCY_CODE, '--') "
                    + "END currency "
                    + "FROM "
                    + "TXN_PAY_TYPE TP "
                    + "LEFT OUTER JOIN "
                    + "(SWT_TRANSACTION TR LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = TR.USER_ID) "
                    + "ON "
                    + "TP.TRAN_TYPE = TR.TRANSACTION_TYPE AND "
                    + "TP.PAY_TYPE = TR.TYPE AND "
                    + whereClause
                    + "LEFT OUTER JOIN "
                    + "SWT_TXN_TYPE  TY "
                    + "ON "
                    + "TP.TRAN_TYPE = TY.TYPECODE "
                    + "WHERE 1=1 AND "
                    + "TP.TRAN_TYPE IN ('27')"
                    + "GROUP BY TY.DESCRIPTION,TP.DESCRIPTION,TR.CURRENCY_CODE,TR.IS_ONE_TIME "
                    + "ORDER BY tran_type DESC,tran_mode DESC";

            SXSSFWorkbook workbook = this.createExcelSummeryTopSection(inputBean, session);

//                int currRow = headerRowCount;
            int currRow = 22;

            int graphDateStartRow = currRow + 2;
            currRow = this.createExcelSummeryTableHeaderSection(workbook, currRow);

            String sql = TXN_SUMMERY_SQL_SEARCH;
//            System.out.println(sql);
            Query query = session.createSQLQuery(sql);

            List<Object[]> objectArrList = (List<Object[]>) query.list();
            List<String> currencyList = new ArrayList<String>();
            List<TranExplorerSummeryBean> summeryBeanList = new ArrayList<TranExplorerSummeryBean>();
            if (objectArrList.size() > 0) {

                for (Object[] objArr : objectArrList) {
                    TranExplorerSummeryBean dataBean = new TranExplorerSummeryBean();

                    dataBean.setTxnCount(objArr[0].toString());
                    dataBean.setTxnAmount(objArr[1].toString());
                    dataBean.setTxnType(objArr[2].toString());
                    dataBean.setTxnMode(objArr[3].toString());
                    dataBean.setCurrency(objArr[4].toString());
                    if (!currencyList.contains(dataBean.getCurrency())) {
                        currencyList.add(dataBean.getCurrency());
                    }
                    summeryBeanList.add(dataBean);

                    currRow = this.createExcelSummeryTableBodySection(workbook, dataBean, currRow);
                }

            }

            Date createdTime = CommonDAO.getSystemDate(session);
            this.createExcelSummeryBotomSection(workbook, currRow, createdTime);

            Sheet sheet = workbook.getSheetAt(0);
            XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
            XSSFClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, currRow + 5, 20, currRow + 35);

            XSSFChart chart = drawing.createChart(anchor);
            CTChart ctChart = chart.getCTChart();
            CTPlotArea ctPlotArea = ctChart.getPlotArea();
            CTBarChart ctBarChart = ctPlotArea.addNewBarChart();
            CTBoolean ctBoolean = ctBarChart.addNewVaryColors();
            ctBoolean.setVal(true);
            ctBarChart.setVaryColors(ctBoolean);
            ctBarChart.addNewBarDir().setVal(STBarDir.COL);

            int seriesId = 0;
            
            for (String currency : currencyList) {
                int currentRow = graphDateStartRow;
                String tempcatFormula = "";
                String tempdatasurceFormula = "";
                List<String> catList = new ArrayList<String>();
                seriesId++;
                CTBarSer ctBarSer = ctBarChart.addNewSer();
                CTSerTx ctSerTx = ctBarSer.addNewTx();
                CTStrRef ctStrRef = ctSerTx.addNewStrRef();
                ctStrRef.setF("\"" + currency + "\"");
                StringBuilder catFormulaBuild = new StringBuilder();
                StringBuilder datasurceFormulaBuild = new StringBuilder();
                int isCurrencyContain = 0;
                for (TranExplorerSummeryBean summeryBean : summeryBeanList) {
                    if (!catList.contains(summeryBean.getTxnMode())) {//is txn mode change in loop 
                        if (!catFormulaBuild.toString().equals("")) {//append for concatnate string character
                            catFormulaBuild.append(",");
                            datasurceFormulaBuild.append(",");
                        }
                        catFormulaBuild.append(tempcatFormula);//appeend temp data to formula
                        datasurceFormulaBuild.append(tempdatasurceFormula);//appeend temp data to formula

                        catList.add(summeryBean.getTxnMode());
                        isCurrencyContain = 0;//set to default
                    }

                    tempcatFormula = "Transaction_Summary_Report!$B$" + currentRow;
                    if (summeryBean.getCurrency().equals(currency)) {
                        tempdatasurceFormula = "Transaction_Summary_Report!$E$" + currentRow;
                        isCurrencyContain = 1;
                    } else if (isCurrencyContain == 0) {
                        tempdatasurceFormula = "Transaction_Summary_Report!$D$" + currentRow;//for zero
                    }
                    currentRow++;
                }
                if (!catFormulaBuild.toString().equals("")) {//append for concatnate string character
                    catFormulaBuild.append(",");
                    datasurceFormulaBuild.append(",");
                }
                catFormulaBuild.append(tempcatFormula);//appeend temp data to formula
                datasurceFormulaBuild.append(tempdatasurceFormula);//appeend temp data to formula
                
                ctBarSer.addNewIdx().setVal(seriesId);
                CTAxDataSource cttAxDataSource = ctBarSer.addNewCat();
                ctStrRef = cttAxDataSource.addNewStrRef();
                ctStrRef.setF(catFormulaBuild.toString());
                CTNumDataSource ctNumDataSource = ctBarSer.addNewVal();
                CTNumRef ctNumRef = ctNumDataSource.addNewNumRef();
                ctNumRef.setF(datasurceFormulaBuild.toString());

                //at least the border lines in Libreoffice Calc ;-)
                ctBarSer.addNewSpPr().addNewLn().addNewSolidFill().addNewSrgbClr().setVal(new byte[]{0, 0, 0});

            }

            //telling the BarChart that it has axes and giving them Ids
            ctBarChart.addNewAxId().setVal(123456);
            ctBarChart.addNewAxId().setVal(123457);

            //cat axis
            CTCatAx ctCatAx = ctPlotArea.addNewCatAx();
            ctCatAx.addNewAxId().setVal(123456); //id of the cat axis
            CTScaling ctScaling = ctCatAx.addNewScaling();
            ctScaling.addNewOrientation().setVal(STOrientation.MIN_MAX);
            ctCatAx.addNewDelete().setVal(false);
            ctCatAx.addNewAxPos().setVal(STAxPos.B);
            ctCatAx.addNewCrossAx().setVal(123457); //id of the val axis
            ctCatAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);

            //val axis
            CTValAx ctValAx = ctPlotArea.addNewValAx();
            ctValAx.addNewAxId().setVal(123457); //id of the val axis
            ctScaling = ctValAx.addNewScaling();
            ctScaling.addNewOrientation().setVal(STOrientation.MIN_MAX);
            ctValAx.addNewDelete().setVal(false);
            ctValAx.addNewAxPos().setVal(STAxPos.L);
            ctValAx.addNewCrossAx().setVal(123456); //id of the cat axis
            ctValAx.addNewTickLblPos().setVal(STTickLblPos.NEXT_TO);

//            //legend
            CTLegend ctLegend = ctChart.addNewLegend();
            ctLegend.addNewLegendPos().setVal(STLegendPos.B);
            ctLegend.addNewOverlay().setVal(false);

//            System.out.println(ctChart);

            returnObject = workbook;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return returnObject;
    }

    private String makeWhereClauseForExcelSummery(TranExplorerInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += "  AND TR.LOCAL_TIME >= TO_DATE('" + inputBean.getFromDate() + "','YYYY-MM-DD HH24:MI:SS') ";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            where += "  AND TR.LOCAL_TIME < TO_DATE('" + inputBean.getToDate() + "','YYYY-MM-DD HH24:MI:SS') +1 ";
        }
        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += "AND TR.CUST_NIC LIKE '%" + inputBean.getNic().trim() + "%' ";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " AND TR.CUST_CIF LIKE '%" + inputBean.getCif().trim() + "%' ";
        }
        if (inputBean.getStatus() == null || inputBean.getStatus().isEmpty()) {
            //do nothing
        } else if (inputBean.getStatus().equals("SUCC")) {
            where += "AND TR.RESPONCE_CODE ='000' ";
        } else if (inputBean.getStatus().equals("FAIL")) {
            where += "AND (TR.RESPONCE_CODE !='000' OR TR.RESPONCE_CODE IS null ) ";
        }
        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += "AND TR.TRANSACTION_TYPE LIKE '%" + inputBean.getTxnType().trim() + "%' ";
        }
        if (inputBean.getIblRefNo() != null && !inputBean.getIblRefNo().isEmpty()) {
            where += "AND TR.IBL_REFERENCE LIKE '%" + inputBean.getIblRefNo().trim() + "%' ";
        }
        if (inputBean.getTranRefNo() != null && !inputBean.getTranRefNo().isEmpty()) {
            where += "AND TR.T24_REFERENCE LIKE '%" + inputBean.getTranRefNo().trim() + "%' ";
        }
        if (inputBean.getChannelType() != null && !inputBean.getChannelType().isEmpty()) {
            where += "AND TR.CHANNEL_TYPE LIKE '%" + inputBean.getChannelType().trim() + "%' ";
        }
        if (inputBean.getCustomerCategory() != null && !inputBean.getCustomerCategory().isEmpty()) {
            where += "AND M.CUSTOMER_CATEGORY LIKE '%" + inputBean.getCustomerCategory().trim() + "%' ";
        }
        if (inputBean.getCurrencyCode() != null && !inputBean.getCurrencyCode().isEmpty()) {
            where += "AND TR.CURRENCY_CODE LIKE '%" + inputBean.getCurrencyCode() + "%' ";
        }
        if (inputBean.getBillCategoryName() != null && !inputBean.getBillCategoryName().isEmpty()) {
            where += "AND TR.BILL_CATEGORY_NAME LIKE '%" + inputBean.getBillCategoryName() + "%' ";
        }
        if (inputBean.getBillProviderName() != null && !inputBean.getBillProviderName().isEmpty()) {
            where += "AND TR.BILL_PROVIDER_NAME LIKE '%" + inputBean.getBillProviderName() + "%' ";
        }
        if (inputBean.getStaffStatus() == null || inputBean.getStaffStatus().isEmpty()) {
            //do nothing
        } else if (inputBean.getStaffStatus().equals("STAFF")) {
            where += "AND M.CUSTOMER_CATEGORY IN ('703') ";
        } else if (inputBean.getStaffStatus().equals("NONSTAFF")) {
            where += "AND M.CUSTOMER_CATEGORY NOT IN ('703') ";
        }
        if (inputBean.getFromAccNo()!= null && !inputBean.getFromAccNo().isEmpty()) {
            where += " and TR.FROM_ACCOUNT_NUMBER LIKE '%" + inputBean.getFromAccNo().trim() + "%'";
        }
        if (inputBean.getToAccNo()!= null && !inputBean.getToAccNo().isEmpty()) {
            where += " and TR.TO_ACCOUNT_NUMBER LIKE '%" + inputBean.getToAccNo().trim() + "%'";
        }

        return where;
    }

    private SXSSFWorkbook createExcelSummeryTopSection(TranExplorerInputBean inputBean, Session session) throws Exception {

        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
        Sheet sheet = workbook.createSheet("Transaction_Summary_Report");

        CellStyle fontBoldedUnderlinedCell = ExcelCommon.getFontBoldedUnderlinedCell(workbook);

        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue("NDB MB Solution");
        cell.setCellStyle(fontBoldedUnderlinedCell);

        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Transaction Summary Report");
        cell.setCellStyle(fontBoldedUnderlinedCell);

        row = sheet.createRow(4);
        cell = row.createCell(0);
        cell.setCellValue("From Date");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(5);
        cell = row.createCell(0);
        cell.setCellValue("To Date");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(6);
        cell = row.createCell(0);
        cell.setCellValue("Transaction Type");
        cell = row.createCell(1);
        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            SwtTxnType swtTxnType = (SwtTxnType) session.get(SwtTxnType.class, inputBean.getTxnType());
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(swtTxnType.getDescription()));
        } else {
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getTxnType()));
        }
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(7);
        cell = row.createCell(0);
        cell.setCellValue("T24 Reference Number");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getTranRefNo()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(8);
        cell = row.createCell(0);
        cell.setCellValue("IBL Reference Number");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getTranRefNo()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(9);
        cell = row.createCell(0);
        cell.setCellValue("Channel Type");
        cell = row.createCell(1);
        String channelTypeCode = Common.replaceEmptyorNullStringToALL(inputBean.getChannelType());
        cell.setCellValue(this.getChannelTypeDesByCode(channelTypeCode));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(10);
        cell = row.createCell(0);
        cell.setCellValue("Status");
        cell = row.createCell(1);
        String description = this.getStatusDescription(inputBean.getStatus());
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(description));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(11);
        cell = row.createCell(0);
        cell.setCellValue("Currency Code");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCurrencyCode()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(12);
        cell = row.createCell(0);
        cell.setCellValue("Customer Category");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategory()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(13);
        cell = row.createCell(0);
        cell.setCellValue("Customer CID");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(14);
        cell = row.createCell(0);
        cell.setCellValue("NIC");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getNic()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(15);
        cell = row.createCell(0);
        cell.setCellValue("Bill Category Name");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getBillCategoryName()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(16);
        cell = row.createCell(0);
        cell.setCellValue("Bill Provider Name");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getBillProviderName()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(17);
        cell = row.createCell(0);
        cell.setCellValue("Staff/Non Staff");
        cell = row.createCell(1);
        String staffStatus = "";
        if (inputBean.getStaffStatus() == null || inputBean.getStaffStatus().equals("")) {
            //nothing to do
        } else if (inputBean.getStaffStatus().equals("STAFF")) {
            staffStatus = "Staff";
        } else if (inputBean.getStaffStatus().equals("NONSTAFF")) {
            staffStatus = "Non Staff";
        }
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(staffStatus));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(18);
        cell = row.createCell(0);
        cell.setCellValue("From Account Number");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getFromAccNo()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        
        row = sheet.createRow(19);
        cell = row.createCell(0);
        cell.setCellValue("To Account Number");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getToAccNo()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        return workbook;
    }

    private int createExcelSummeryTableHeaderSection(SXSSFWorkbook workbook, int currrow) throws Exception {
        CellStyle columnHeaderCell = ExcelCommon.getColumnHeadeCell(workbook);
        Sheet sheet = workbook.getSheetAt(0);

        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue("Transaction Type");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(1);
        cell.setCellValue("Transaction Mode");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(2);
        cell.setCellValue("Transaction Count");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(3);
        cell.setCellValue("Currency");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(4);
        cell.setCellValue("Trancation Amount");
        cell.setCellStyle(columnHeaderCell);

        return currrow;
    }

    private int createExcelSummeryTableBodySection(SXSSFWorkbook workbook, TranExplorerSummeryBean dataBean, int currrow) throws Exception {
        Sheet sheet = workbook.getSheetAt(0);
        CellStyle rowColumnCell = ExcelCommon.getRowColumnCell(workbook);
        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue(dataBean.getTxnType());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(1);
        cell.setCellValue(dataBean.getTxnMode());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(2);
        cell.setCellValue(dataBean.getTxnCount());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(3);
        cell.setCellValue(dataBean.getCurrency());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(4);
        try {
            cell.setCellValue(Double.valueOf(dataBean.getTxnAmount()));
        } catch (NumberFormatException e) {
            cell.setCellValue(Double.valueOf(0));
        }
        CellStyle rowColumnCellStyleCurrency = ExcelCommon.getRowColumnCell(workbook);
        CreationHelper ch = workbook.getCreationHelper();
        rowColumnCellStyleCurrency.setDataFormat(ch.createDataFormat().getFormat("#,##0.00"));
        cell.setCellStyle(rowColumnCellStyleCurrency);

        return currrow;
    }

    private void createExcelSummeryBotomSection(SXSSFWorkbook workbook, int currrow, Date date) throws Exception {

        CellStyle fontBoldedCell = ExcelCommon.getFontBoldedCell(workbook);
        Sheet sheet = workbook.getSheetAt(0);

        currrow++;
        Row row = sheet.createRow(currrow++);
        Cell cell = row.createCell(0);
        cell.setCellValue("Summary");
        cell.setCellStyle(fontBoldedCell);

        row = sheet.createRow(currrow++);
        cell = row.createCell(0);
        cell.setCellValue("Report Created Time and Date");
        cell = row.createCell(1);
        cell.setCellValue(date.toString().substring(0, 19));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));
    }
}
