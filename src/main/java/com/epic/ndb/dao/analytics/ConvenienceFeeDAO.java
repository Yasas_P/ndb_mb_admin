/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.analytics;

import com.epic.ndb.bean.analytics.ConvenienceFeeBean;
import com.epic.ndb.bean.analytics.ConvenienceFeeInputBean;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.varlist.CommonVarList;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class ConvenienceFeeDAO {
    private String CONVENIENCE_FEE_COUNT_SQL = "SELECT "
            + "COUNT(U.ID) "
            + "FROM SWT_MOBILE_USER U "
            + "LEFT OUTER JOIN STATUS S ON S.STATUSCODE = U.STATUS "
            + "LEFT OUTER JOIN SEGMENT_TYPE SE ON SE.SEGMENTCODE = U.USER_SEGMENT "
            + "INNER JOIN SWT_FEE_STATUS F ON F.USER_ID = U.ID "
            + "WHERE ";
    private String CONVENIENCE_FEE_ORDER_BY_SQL = " order by F.TIMESTAMP DESC ";
    
     public List<ConvenienceFeeBean> getSearchList(ConvenienceFeeInputBean inputBean, int max, int first, String orderBy) throws Exception {
        List<ConvenienceFeeBean> dataList = new ArrayList<ConvenienceFeeBean>();
         Session session = null;

        String CONVENIENCE_FEE_SQL_SEARCH = "SELECT "
                + "U.ID, "//0
                + "U.CIF, "//1
                + "U.USERNAME, "//2
                + "SE.DESCRIPTION SEGMENT, "//3
                + "U.CUSTOMER_NAME, "//4
                + "U.MOBILE_NUMBER, "//5 
                + "U.ON_BOARD_CHANNEL, "//6
                + "U.ON_BOARD_TYPE, "//7 
                + "U.STATUS STATUS, "//8
                + "S.DESCRIPTION STATUSDES, "//9
                + "U.FIRST_TIME_LOGIN_EXPECTED, "//10
                + "U.CUSTOMER_CATEGORY, "//11
                + "U.DEFAULT_ACC_TYPE, "//12
                + "U.DEFAULT_ACC_NO, "//13
                + "F.CHARGED_AMOUNT , "//14   
                + "F.TIMESTAMP FEE_DEDUCT_DATE, "//15
                + "F.DESCRIPTION ERROR_DES, "//16   
                + "F.RESPONSE_CODE RES_CODE,  "//17

                + "row_number() over ( " + orderBy + " ) as r "
                + "FROM SWT_MOBILE_USER U "
                + "LEFT OUTER JOIN STATUS S ON S.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN SEGMENT_TYPE SE ON SE.SEGMENTCODE = U.USER_SEGMENT "
                + "INNER JOIN SWT_FEE_STATUS F ON F.USER_ID = U.ID "
                + "WHERE ";

        try {

            long count = 0;
            String where = this.makeWhereClauseForSearch(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = CONVENIENCE_FEE_COUNT_SQL + where;
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sqlSearch = " SELECT * from (" + CONVENIENCE_FEE_SQL_SEARCH + where + ") where r > " + first + " and r<= " + max;
                Query querySearch = session.createSQLQuery(sqlSearch);

                List<Object[]> objectArrList = (List<Object[]>) querySearch.list();

                if (objectArrList.size() > 0) {

                    for (Object[] objArr : objectArrList) {

                        ConvenienceFeeBean searchBean = new ConvenienceFeeBean();

                        try {
                            searchBean.setUserid(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserid("--");
                        }
                        try {
                            searchBean.setCif(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUsername(objArr[2].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUsername("--");
                        }
                        try {
                            searchBean.setPromotionMtSegmentType(objArr[3].toString());
                        } catch (Exception e) {
                            searchBean.setPromotionMtSegmentType("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setMobileNumber(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNumber("--");
                        }
                        try {
                            if (objArr[6] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[6].toString());
                                searchBean.setOnBoardChannel(channelTypeDes);
                            } else {
                                searchBean.setOnBoardChannel("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardChannel("--");
                        }
                        try {
                            if (objArr[7] != null) {
                                String onBordedTypeDes = this.getOnBordedTypeDesByCode(objArr[7].toString());
                                searchBean.setOnBoardType(onBordedTypeDes);
                            } else {
                                searchBean.setOnBoardType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardType("--");
                        }
                        try {
                            if ((objArr[4] == null || objArr[2].toString().isEmpty()) && objArr[8].toString().equals(CommonVarList.STATUS_ACTIVE)) {
                                searchBean.setStatusDes(CommonVarList.CUSTOMER_PEND_STATUS_DES);
                                searchBean.setStatus(CommonVarList.CUSTOMER_PEND_STATUS);
                            } else {
                                searchBean.setStatus(objArr[8].toString());
                                searchBean.setStatusDes(objArr[9].toString());
                            }
                        } catch (NullPointerException e) {
                            searchBean.setStatus("--");
                            searchBean.setStatusDes("--");
                        }
                        try {
                            if (objArr[10] != null) {
                                String loginStatusDes = this.getLoginStatusDesByCode(objArr[10].toString());
                                searchBean.setLoginStatus(loginStatusDes);
                            } else {
                                searchBean.setLoginStatus("--");
                            }
                        } catch (Exception e) {
                            searchBean.setLoginStatus("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[11].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            if (objArr[12] != null) {
                                String onDefaultTypeDes = this.getDefaultTypeDesByCode(objArr[12].toString());
                                searchBean.setDefType(onDefaultTypeDes);
                            } else {
                                searchBean.setDefType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setDefType("--");
                        }
                        try {
                            searchBean.setDefAccNo(objArr[13].toString());
                        } catch (NullPointerException e) {
                            searchBean.setDefAccNo("--");
                        }
                        try {
                            searchBean.setChargeAmount(objArr[14].toString());
                        } catch (NullPointerException e) {
                            searchBean.setChargeAmount("--");
                        }
                        try {
                            searchBean.setDate(objArr[15].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setDate("--");
                        }
                        try {
                            searchBean.setErrorMessage(objArr[16].toString());
                        } catch (NullPointerException e) {
                            searchBean.setErrorMessage("--");
                        }
                        try {
                            if (objArr[17] == null) {
                                searchBean.setFeeDeductStatus("Failure");
                            } else {
                                if (objArr[17].equals("000")) {
                                    searchBean.setFeeDeductStatus("Success");
                                } else {
                                    searchBean.setFeeDeductStatus("Failure");
                                }       
                            }
                        } catch (NullPointerException npe) {
                            searchBean.setFeeDeductStatus("Failure");
                        }

                        searchBean.setFullCount(count);
                        dataList.add(searchBean);
                    }

                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }
     
    public StringBuffer makeCSVReport(ConvenienceFeeInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;

        String CONVENIENCE_FEE_SQL_CSV = "SELECT "
                + "U.ID, "//0
                + "U.CIF, "//1
                + "U.USERNAME, "//2
                + "SE.DESCRIPTION SEGMENT, "//3
                + "U.CUSTOMER_NAME, "//4
                + "U.MOBILE_NUMBER, "//5 
                + "U.ON_BOARD_CHANNEL, "//6
                + "U.ON_BOARD_TYPE, "//7 
                + "U.STATUS STATUS, "//8
                + "S.DESCRIPTION STATUSDES, "//9
                + "U.FIRST_TIME_LOGIN_EXPECTED, "//10
                + "U.CUSTOMER_CATEGORY, "//11
                + "U.DEFAULT_ACC_TYPE, "//12
                + "U.DEFAULT_ACC_NO, "//13
                + "F.CHARGED_AMOUNT , "//14   
                + "F.TIMESTAMP FEE_DEDUCT_DATE, "//15
                + "F.DESCRIPTION ERROR_DES, "//16   
                + "F.RESPONSE_CODE RES_CODE  "//17
                + "FROM SWT_MOBILE_USER U "
                + "LEFT OUTER JOIN STATUS S ON S.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN SEGMENT_TYPE SE ON SE.SEGMENTCODE = U.USER_SEGMENT "
                + "INNER JOIN SWT_FEE_STATUS F ON F.USER_ID = U.ID "
                + "WHERE ";

        try {
            session = HibernateInit.sessionFactory.openSession();
            int count = 0;
            String where1 = this.makeWhereClauseForSearch(inputBean);
            String sqlCount = this.CONVENIENCE_FEE_COUNT_SQL + where1;
//            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = CONVENIENCE_FEE_SQL_CSV + where1;

                Query query = session.createSQLQuery(sql);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    content = new StringBuffer();
                    List<ConvenienceFeeBean> beanlist = new ArrayList<ConvenienceFeeBean>();

                    for (Object[] objArr : objectArrList) {

                        ConvenienceFeeBean searchBean = new ConvenienceFeeBean();

                        try {
                            searchBean.setUserid(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserid("--");
                        }
                        try {
                            searchBean.setCif(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUsername(objArr[2].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUsername("--");
                        }
                        try {
                            searchBean.setPromotionMtSegmentType(objArr[3].toString());
                        } catch (Exception e) {
                            searchBean.setPromotionMtSegmentType("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setMobileNumber(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNumber("--");
                        }
                        try {
                            if (objArr[6] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[6].toString());
                                searchBean.setOnBoardChannel(channelTypeDes);
                            } else {
                                searchBean.setOnBoardChannel("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardChannel("--");
                        }
                        try {
                            if (objArr[7] != null) {
                                String onBordedTypeDes = this.getOnBordedTypeDesByCode(objArr[7].toString());
                                searchBean.setOnBoardType(onBordedTypeDes);
                            } else {
                                searchBean.setOnBoardType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardType("--");
                        }
                        try {
                            if ((objArr[4] == null || objArr[2].toString().isEmpty()) && objArr[8].toString().equals(CommonVarList.STATUS_ACTIVE)) {
                                searchBean.setStatusDes(CommonVarList.CUSTOMER_PEND_STATUS_DES);
                                searchBean.setStatus(CommonVarList.CUSTOMER_PEND_STATUS);
                            } else {
                                searchBean.setStatus(objArr[8].toString());
                                searchBean.setStatusDes(objArr[9].toString());
                            }
                        } catch (NullPointerException e) {
                            searchBean.setStatus("--");
                            searchBean.setStatusDes("--");
                        }
                        try {
                            if (objArr[10] != null) {
                                String loginStatusDes = this.getLoginStatusDesByCode(objArr[10].toString());
                                searchBean.setLoginStatus(loginStatusDes);
                            } else {
                                searchBean.setLoginStatus("--");
                            }
                        } catch (Exception e) {
                            searchBean.setLoginStatus("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[11].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            if (objArr[12] != null) {
                                String onDefaultTypeDes = this.getDefaultTypeDesByCode(objArr[12].toString());
                                searchBean.setDefType(onDefaultTypeDes);
                            } else {
                                searchBean.setDefType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setDefType("--");
                        }
                        try {
                            searchBean.setDefAccNo(objArr[13].toString());
                        } catch (NullPointerException e) {
                            searchBean.setDefAccNo("--");
                        }
                        try {
                            searchBean.setChargeAmount(objArr[14].toString());
                        } catch (NullPointerException e) {
                            searchBean.setChargeAmount("--");
                        }
                        try {
                            searchBean.setDate(objArr[15].toString().substring(0, 19));
                        } catch (NullPointerException e) {
                            searchBean.setDate("--");
                        }
                        try {
                            searchBean.setErrorMessage(objArr[16].toString());
                        } catch (NullPointerException e) {
                            searchBean.setErrorMessage("--");
                        }
                        try {
                            if (objArr[17] == null) {
                                searchBean.setFeeDeductStatus("Failure");
                            } else {
                                if (objArr[17].equals("000")) {
                                    searchBean.setFeeDeductStatus("Success");
                                } else {
                                    searchBean.setFeeDeductStatus("Failure");
                                }       
                            }
                        } catch (NullPointerException npe) {
                            searchBean.setFeeDeductStatus("Failure");
                        }

                        beanlist.add(searchBean);
                    }

                    //write column headers to csv file
                    content.append("Unique ID");
                    content.append(',');
                    content.append("CID");
                    content.append(',');
                    content.append("User Name");
                    content.append(',');
                    content.append("User Segment");
                    content.append(',');
                    content.append("On Board Channel");
                    content.append(',');
                    content.append("On Board Type");
                    content.append(',');
                    content.append("Customer Category");
                    content.append(',');
                    content.append("User Status");
                    content.append(',');
                    content.append("Login Status");
                    content.append(',');
                    content.append("Primary Account Type");
                    content.append(',');
                    content.append("Primary Acc/Card Number");
                    content.append(',');
                    content.append("Customer Name");
                    content.append(',');
                    content.append("Mobile Number");
                    content.append(',');
                    content.append("Fee Amount");
                    content.append(',');
                    content.append("Fee Deduct Date");
                    content.append(',');
                    content.append("Fee Deduct Status");
                    content.append(',');
                    content.append("Error Message");

                    content.append('\n');

                    //write data values to csv file
                    for (ConvenienceFeeBean dataBean : beanlist) {
                        try {
                            if (dataBean.getUserid() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getUserid());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getCif() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCif());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getUsername() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getUsername()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getPromotionMtSegmentType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getPromotionMtSegmentType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getOnBoardChannel() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getOnBoardChannel());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getOnBoardType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getOnBoardType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerCategory() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getStatusDes() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getStatusDes());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getLoginStatus() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLoginStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDefType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDefType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDefAccNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDefAccNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getMobileNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMobileNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getChargeAmount()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getChargeAmount());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDate()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDate());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getFeeDeductStatus()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getFeeDeductStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getErrorMessage()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getErrorMessage());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        content.append('\n');
                    }
                    content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
                    content.append('\n');

                    content.append("CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCifSearch()));
                    content.append('\n');

                    content.append("User Name :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getUsernameSearch()));
                    content.append('\n');

                    content.append("User Segment :");
                    if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
                        SegmentType segmenttype = (SegmentType) session.get(SegmentType.class, inputBean.getSegmentTypeSearch());
                        content.append(Common.replaceEmptyorNullStringToALL(segmenttype.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getSegmentTypeSearch()));
                    }
                    content.append('\n');

                    content.append("On Board Channel :");
                    String onBoardChannel = Common.replaceEmptyorNullStringToALL(inputBean.getOnBoardChannelSearch());
                    content.append(this.getChannelTypeDesByCode(onBoardChannel));
                    content.append('\n');

                    content.append("On Board Channel :");
                    String onBoardType = Common.replaceEmptyorNullStringToALL(inputBean.getOnBoardTypeSearch());
                    content.append(this.getOnBordedTypeDesByCode(onBoardType));
                    content.append('\n');

                    content.append("Customer Category :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategorySearch()));
                    content.append('\n');

                    content.append("User Status :");
                    if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
                        Status status = (Status) session.get(Status.class, inputBean.getStatusSearch());
                        content.append(Common.replaceEmptyorNullStringToALL(status.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getStatusSearch()));
                    }
                    content.append('\n');

//                    content.append("Mobile :");
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getMobilenoSearch()));
//                    content.append('\n');

                    content.append("Primary Account Type :");
                    String defaultType = Common.replaceEmptyorNullStringToALL(inputBean.getDefTypeSearch());
                    content.append(this.getDefaultTypeDesByCode(defaultType));
                    content.append('\n');

                    content.append("Primary Account/Card Number :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getDefAccNoSearch()));
                    content.append('\n');
                    
                    content.append("Fee Deduct Status :");
                    if (inputBean.getFeeStatusSearch()!= null && !inputBean.getFeeStatusSearch().isEmpty()) {
                        String description = this.getFeeStatusDescription(inputBean.getFeeStatusSearch());
                        if (description != null) {
                            content.append(description);
                        } else {
                            content.append("-ALL-");
                        }
                    } else {
                        content.append("-ALL-");
                    }
                    content.append('\n');
                }

            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
    private String makeWhereClauseForSearch(ConvenienceFeeInputBean inputBean) throws ParseException, Exception {
        String where = "1=1";
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += " and lower(F.TIMESTAMP) >= to_timestamp( '" + inputBean.getFromDate() + "' , 'yy-mm-dd')";

        }

        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getToDate());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and lower(F.TIMESTAMP) <= to_timestamp( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getCifSearch() != null && !inputBean.getCifSearch().isEmpty()) {
            where += " and lower(U.CIF) like lower('%" + inputBean.getCifSearch().trim() + "%')";
        }

        if (inputBean.getUsernameSearch() != null && !inputBean.getUsernameSearch().isEmpty()) {
            where += " and lower(U.USERNAME) like lower('%" + inputBean.getUsernameSearch().trim() + "%')";
        }


        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            if (inputBean.getStatusSearch().equals(CommonVarList.STATUS_ACTIVE)) {
                where += " and U.STATUS = '" + inputBean.getStatusSearch() + "' and (U.USERNAME is not null or U.USERNAME != '')";
            } else if (inputBean.getStatusSearch().equals(CommonVarList.STATUS_CUSTOMER_PEND)) {
                where += " and U.STATUS = '" + CommonVarList.STATUS_ACTIVE + "' and (U.USERNAME is null or U.USERNAME = '')";
            } else {
                where += " and U.STATUS = '" + inputBean.getStatusSearch() + "'";
            }
        }
        if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
            where += " and lower(U.USER_SEGMENT) like lower('%" + inputBean.getSegmentTypeSearch().trim() + "%')";
        }
        if (inputBean.getOnBoardChannelSearch() != null && !inputBean.getOnBoardChannelSearch().isEmpty()) {
            where += " and U.ON_BOARD_CHANNEL = '" + inputBean.getOnBoardChannelSearch() + "'";
        }
        if (inputBean.getOnBoardTypeSearch() != null && !inputBean.getOnBoardTypeSearch().isEmpty()) {
            where += " and U.ON_BOARD_TYPE = '" + inputBean.getOnBoardTypeSearch() + "'";
        }
        if (inputBean.getCustomerCategorySearch() != null && !inputBean.getCustomerCategorySearch().isEmpty()) {
            where += " and lower(U.CUSTOMER_CATEGORY) like lower('%" + inputBean.getCustomerCategorySearch().trim() + "%')";
        }
        if (inputBean.getMobilenoSearch() != null && !inputBean.getMobilenoSearch().isEmpty()) {
            where += " and lower(U.MOBILE_NUMBER) like lower('%" + inputBean.getMobilenoSearch().trim() + "%')";
        }
        if (inputBean.getDefAccNoSearch()!= null && !inputBean.getDefAccNoSearch().isEmpty()) {
            where += " and lower(U.DEFAULT_ACC_NO) like lower('%" + inputBean.getDefAccNoSearch().trim() + "%')";
        }
        if (inputBean.getDefTypeSearch()!= null && !inputBean.getDefTypeSearch().isEmpty()) {
            where += " and U.DEFAULT_ACC_TYPE = '" + inputBean.getDefTypeSearch().trim() + "'";
        }
        if (inputBean.getFeeStatusSearch()!= null && !inputBean.getFeeStatusSearch().isEmpty()) {
            if (inputBean.getFeeStatusSearch().equals("SUCC")) {
                where += " and F.RESPONSE_CODE ='000'";
            } else if (inputBean.getFeeStatusSearch().equals("FAIL")) {
                where += " and (F.RESPONSE_CODE !='000' OR F.RESPONSE_CODE IS null )";
            }
        }

        return where;
    }
     
    private String getChannelTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("2")) {
            des = "Internet Banking";
        } else if (code.equals("1")) {
            des = "Mobile Banking";
        } else {
            des = code;
        }
        return des;

    }

    private String getOnBordedTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("1")) {
            des = "Account";
        } else if (code.equals("2")) {
            des = "Card";
        } else {
            des = code;
        }
        return des;

    }

    private String getDefaultTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("1")) {
            des = "Account";
        } else if (code.equals("2")) {
            des = "Card";
        } else {
            des = code;
        }
        return des;

    }

    private String getLoginStatusDesByCode(String code) {
        String des = "--";
        if (code.equals("0")) {
            des = "Success (Set default Account)";
        } else if (code.equals("1")) {
            des = "First time login expected";
        } else {
            des = code;
        }
        return des;

    }
    
    private String getFeeStatusDescription(String status) throws ParseException {

        String rDescription = null;

        if (status != null && !status.isEmpty()) {
            if (status.equals("SUCC")) {
                rDescription = "Success";
            } else if (status.equals("FAIL")) {
                rDescription = "Failure";
            }
        }
        return rDescription;
    }
}
