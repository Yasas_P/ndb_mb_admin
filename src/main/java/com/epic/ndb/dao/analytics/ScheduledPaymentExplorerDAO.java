/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.analytics;

import com.epic.ndb.bean.analytics.ScheduledPaymentExplorerBean;
import com.epic.ndb.bean.analytics.ScheduledPaymentExplorerInputBean;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.PartialList;
import com.epic.ndb.util.mapping.SwtTxnType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class ScheduledPaymentExplorerDAO {
    
    private final int headerRowCount = 18;
    private String TXN_COUNT_SQL = "SELECT "
            + "COUNT(TRANSACTION_ID) "
            + "FROM SWT_TRANSACTION U "
            + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
            + "WHERE ";
    private String TRANEXPLORER_ORDER_BY_SQL = " order by U.LOCAL_TIME DESC ";

    public PartialList<ScheduledPaymentExplorerBean> getSearchList(ScheduledPaymentExplorerInputBean inputBean, int rows, int from, String sortIndex, String sortOrder) throws Exception {
        List<ScheduledPaymentExplorerBean> dataBeanList = new ArrayList<ScheduledPaymentExplorerBean>();
        Session session = null;
        int count = 0;
        if ("".equals(sortIndex.trim())) {
            sortIndex = null;
        }
        String CUSTOMER_SQL_SEARCH = "SELECT "
                + "U.TRANSACTION_ID, "//0
                + "T.DESCRIPTION TXN_TYPE, "//1          
                + "U.FROM_ACCOUNT_NUMBER, "//2
                + "U.TO_ACCOUNT_NUMBER, "//3
                + "U.AMOUNT, "//4
                //                + "U.CUST_CIF, "//5
                //                + "U.CUST_NIC, "//6
                //                + "U.CUST_NAME, "//7
                //                + "U.CUST_EMAIL, "//8
                //                + "U.ADDRESS_1, "//9 
                + "M.CIF, "//5
                + "M.NIC, "//6
                + "M.CUSTOMER_NAME, "//7
                + "M.EMAIL, "//8
                + "M.PERMANENT_ADDRESS, "//9 
                + "ST.DESCRIPTION STATUS, "//10 
                + "U.LEASE_MODEL, "//11
                + "U.LEASE_TYPE, "//12
                + "U.NTB_REQUEST_TYPE, "//13
                + "U.POSTED_METHOD, "//14
                + "U.RESPONCE_CODE, "//15
                //                + "S.DESCRIPTION RESPONSE,"
                + "M.MOBILE_NUMBER, "//16
                + "U.SERVICE_FEE, "//17
                + "U.PAYEE_NAME, "//18
//                + "U.BILL_CATEGORY_NAME, "//19
//                + "U.BILL_PROVIDER_NAME, "//20
                + "BC.CATEGORY_NAME, "//19
                + "BS.PROVIDER_NAME, "//20
                + "U.BILLER_NAME, "//21
                + "U.MERCHANT_TYPE_NAME, "//22
                + "U.MERCHANT_NAME, "//23
                + "U.REDEEM_POINTS, "//24
                + "U.REDEEM_VOUCHER_ID, "//25
                + "U.LEASE_AMOUNT, "//26
                + "U.LEASE_AVD_PAYMENT, "//27
                + "U.LEASE_SELL_PRICE, "//28
                + "U.CHEQUE_NUMBER, "//29
                + "U.FROM_DATE, "//30
                + "U.TO_DATE, "//31
                + "U.CARD_NUMBER, "//32
                + "U.STATEMENT_FROM, "//33
                + "U.STATEMENT_TO, "//34
                + "U.CURRENCY_CODE, "//35
                + "U.BANK_NAME, "//36
                + "U.BRANCH_NAME, "//37
                + "U.REMARKS, "//38
                + "U.ACCOUNT_NO, "//39
                + "U.COLLECT_FROM_TYPE, "//40
                + "U.COLLECTION_BRANCH_ID, "//41
                + "U.PREFERRED_EMAIL, "//42
                + "U.REQUEST_DATE, "//43
                + "U.LASTUPDATED, "//44                
                + "U.LOCAL_TIME, "//45
                + "U.USER_ID, "//46
                + "U.CHANNEL_TYPE, "//47
                + "U.DEVICE_ID, "//48
                + "M.CUSTOMER_CATEGORY, "//49
                + "U.APP_ID, "//50
                + "U.BILL_REFFRENCE_NUMBER, "//51
                + "TM.DESCRIPTION TXN_MODE_DES, "//52
                + "U.TRAN_REF_NO, "//53
                //                + "U.REQUEST_ID, "//53
                + "U.ERR_DESC, "//54
                + "M.USERNAME, "//55
                + "D.DEVICE_MANUFACTURE, "//56
                + "D.DEVICE_BUILD_NUMBER, "//57
                + "PT.DESCRIPTION PAY_TYPE_DES, "//58
                + "U.TYPE PAY_TYPE, "//59
                + "U.T24_RESPONSE, "//60
                + "U.T24_REFERENCE, "//61
                + "U.IBL_RESPONSE, "//62
                + "U.IBL_REFERENCE, "//63
                + "U.IS_ONE_TIME, "//64
                + "CAST(U.TRANSACTION_TYPE AS VARCHAR2(3)), "//65
                + "U.SCHEDULE_ID, "//66
                + "U.IS_PAYTO_MOBILE "//67
                + "FROM SWT_TRANSACTION U "
                + "LEFT OUTER JOIN SWT_RESPONSE_CODES S ON S.CODE = U.RESPONCE_CODE "
                + "LEFT OUTER JOIN SWT_TXN_TYPE T ON T.TYPECODE = U.TRANSACTION_TYPE "
                + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
                + "LEFT OUTER JOIN STATUS ST ON ST.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN TRANSACTION_MODE TM ON TM.TRAN_TYPE = U.TRANSACTION_TYPE AND TM.TRAN_MODE = U.TXN_MODE "
                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID AND D.USERID = U.USER_ID "
                + "LEFT OUTER JOIN TXN_PAY_TYPE PT ON PT.TRAN_TYPE = U.TRANSACTION_TYPE AND PT.PAY_TYPE = U.TYPE "
                + "LEFT OUTER JOIN BILL_CATEGORY BC ON BC.CATEGORY_CODE = U.BILL_CATEGORY_ID "
                + "LEFT OUTER JOIN BILL_SERVICE_PROVIDER BS ON BS.PROVIDER_ID = U.BILL_PROVIDER_ID "
                //                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID "
                + "WHERE ";
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String where1 = this.makeWhereClause(inputBean);
            String sqlCount = this.TXN_COUNT_SQL + where1;
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = CUSTOMER_SQL_SEARCH + where1 + this.TRANEXPLORER_ORDER_BY_SQL;
                System.out.println(sql);

                Query query = session.createSQLQuery(sql);
                query.setMaxResults(rows);
                query.setFirstResult(from);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    dataBeanList = new ArrayList<ScheduledPaymentExplorerBean>();

                    for (Object[] objArr : objectArrList) {

                        ScheduledPaymentExplorerBean dataBean = new ScheduledPaymentExplorerBean();

                        //set data values to beanlist
                        try {

                            if (objArr[0] == null) {
                                dataBean.setTxnId("--");
                            } else {
                                dataBean.setTxnId(objArr[0].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnId("--");
                        }

                        try {
                            if (objArr[1] == null) {
                                dataBean.setTxnType("--");
                            } else {;
                                dataBean.setTxnType(objArr[1].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnType("--");
                        }

                        try {
                            if (objArr[2] == null) {
                                dataBean.setFromAccNo("--");
                            } else {
                                dataBean.setFromAccNo(objArr[2].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromAccNo("--");
                        }
                        try {
                            if (objArr[3] == null) {
                                dataBean.setToAccNo("--");
                            } else {
                                dataBean.setToAccNo(objArr[3].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToAccNo("--");
                        }

                        try {
                            if (objArr[4] == null) {
                                dataBean.setAmount("--");
                            } else {
                                dataBean.setAmount(objArr[4].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAmount("--");
                        }

                        try {
                            if (objArr[5] == null) {
                                dataBean.setCustCif("--");
                            } else {
                                dataBean.setCustCif(objArr[5].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustCif("--");
                        }

                        try {
                            if (objArr[6] == null) {
                                dataBean.setNic("--");
                            } else {
                                dataBean.setNic(objArr[6].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNic("--");
                        }

                        try {
                            if (objArr[7] == null) {
                                dataBean.setCustName("--");
                            } else {
                                dataBean.setCustName(objArr[7].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustName("--");
                        }

                        try {
                            if (objArr[8] == null) {
                                dataBean.setCustEmail("--");
                            } else {
                                dataBean.setCustEmail(objArr[8].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustEmail("--");
                        }

                        try {
                            if (objArr[9] == null) {
                                dataBean.setAddress1("--");
                            } else {
                                dataBean.setAddress1(objArr[9].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAddress1("--");
                        }

                        try {
                            if (objArr[10] == null) {
                                dataBean.setStatus("--");
                            } else {
                                dataBean.setStatus(objArr[10].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatus("--");
                        }

                        try {
                            if (objArr[11] == null) {
                                dataBean.setLeasingModel("--");
                            } else {
                                dataBean.setLeasingModel(objArr[11].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingModel("--");
                        }

                        try {
                            if (objArr[12] == null) {
                                dataBean.setLeasingTypes("--");
                            } else {
                                dataBean.setLeasingTypes(objArr[12].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingTypes("--");
                        }

                        try {
                            if (objArr[13] == null) {
                                dataBean.setNtbRequest("--");
                            } else {
                                dataBean.setNtbRequest(objArr[13].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNtbRequest("--");
                        }

                        try {
                            if (objArr[14] == null) {
                                dataBean.setPostedMethod("--");
                            } else {
                                dataBean.setPostedMethod(objArr[14].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPostedMethod("--");
                        }

                        try {
                            if (objArr[15] == null) {
                                dataBean.setResponseCodes("Failure");
                            } else {
                                if (objArr[15].equals("000")) {
                                    dataBean.setResponseCodes("Success");
                                } else {
                                    dataBean.setResponseCodes("Failure");
                                }
//                                dataBean.setResponseCodes(objArr[15].toString());
                                //dataBean.setResponseCodes(objArr[15].getSwtResponseCodes().getDescription());        
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setResponseCodes("Failure");
                        }

                        try {
                            if (objArr[16] == null) {
                                dataBean.setMobileNumber("--");
                            } else {
                                dataBean.setMobileNumber(objArr[16].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMobileNumber("--");
                        }

                        try {
                            if (objArr[17] == null) {
                                dataBean.setServiceFee("--");
                            } else {
                                dataBean.setServiceFee(objArr[17].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setServiceFee("--");
                        }

                        try {
                            if (objArr[18] == null) {
                                dataBean.setPayeeName("--");
                            } else {
                                dataBean.setPayeeName(objArr[18].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPayeeName("--");
                        }

                        try {
                            if (objArr[19] == null) {
                                dataBean.setBillCategoryName("--");
                            } else {
                                dataBean.setBillCategoryName(objArr[19].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillCategoryName("--");
                        }

                        try {
                            if (objArr[20] == null) {
                                dataBean.setBillProviderName("--");
                            } else {
                                dataBean.setBillProviderName(objArr[20].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillProviderName("--");
                        }

                        try {
                            if (objArr[21] == null) {
                                dataBean.setBillerName("--");
                            } else {
                                dataBean.setBillerName(objArr[21].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillerName("--");
                        }
                        try {
                            if (objArr[22] == null) {
                                dataBean.setMerchantTypeName("--");
                            } else {
                                dataBean.setMerchantTypeName(objArr[22].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantTypeName("--");
                        }

                        try {
                            if (objArr[23] == null) {
                                dataBean.setMerchantName("--");
                            } else {
                                dataBean.setMerchantName(objArr[23].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantName("--");
                        }

                        try {
                            if (objArr[24] == null) {
                                dataBean.setRedeemPoints("--");
                            } else {
                                dataBean.setRedeemPoints(objArr[24].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemPoints("--");
                        }

                        try {
                            if (objArr[25] == null) {
                                dataBean.setRedeemVoucherId("--");
                            } else {
                                dataBean.setRedeemVoucherId(objArr[25].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemVoucherId("--");
                        }

                        try {
                            if (objArr[26] == null) {
                                dataBean.setLeaseAmount("--");
                            } else {
                                dataBean.setLeaseAmount(objArr[26].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAmount("--");
                        }

                        try {
                            if (objArr[27] == null) {
                                dataBean.setLeaseAvdPayment("--");
                            } else {
                                dataBean.setLeaseAvdPayment(objArr[27].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAvdPayment("--");
                        }

                        try {
                            if (objArr[28] == null) {
                                dataBean.setLeaseSellPrice("--");
                            } else {
                                dataBean.setLeaseSellPrice(objArr[28].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseSellPrice("--");
                        }

                        try {
                            if (objArr[29] == null) {
                                dataBean.setChequeNumber("--");
                            } else {
                                dataBean.setChequeNumber(objArr[29].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setChequeNumber("--");
                        }
                        try {
                            if (objArr[30] == null) {
                                dataBean.setFromDate("--");
                            } else {
                                dataBean.setFromDate(objArr[30].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromDate("--");
                        }

                        try {
                            if (objArr[31] == null) {
                                dataBean.setToDate("--");
                            } else {
                                dataBean.setToDate(objArr[31].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToDate("--");
                        }

                        try {
                            if (objArr[32] == null) {
                                dataBean.setCardNumber("--");
                            } else {
//                                dataBean.setCardNumber(toMaskCardNumber(objArr[32].toString()));
                                dataBean.setCardNumber(objArr[32].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCardNumber("--");
                        }

                        try {
                            if (objArr[33] == null) {
                                dataBean.setStatementFrom("--");
                            } else {
                                dataBean.setStatementFrom(objArr[33].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementFrom("--");
                        }

                        try {
                            if (objArr[34] == null) {
                                dataBean.setStatementTo("--");
                            } else {
                                dataBean.setStatementTo(objArr[34].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementTo("--");
                        }

                        try {
                            if (objArr[35] == null) {
                                dataBean.setCurrencyCode("--");
                            } else {
                                dataBean.setCurrencyCode(objArr[35].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCurrencyCode("--");
                        }

                        try {
                            if (objArr[36] == null) {
                                dataBean.setBankName("--");
                            } else {
                                dataBean.setBankName(objArr[36].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBankName("--");
                        }

                        try {
                            if (objArr[37] == null) {
                                dataBean.setBranchName("--");
                            } else {
                                dataBean.setBranchName(objArr[37].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBranchName("--");
                        }
                        try {
                            if (objArr[38] == null) {
                                dataBean.setRemarks("--");
                            } else {
                                dataBean.setRemarks(objArr[38].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRemarks("--");
                        }

                        try {
                            if (objArr[39] == null) {
                                dataBean.setAccountNo("--");
                            } else {
                                dataBean.setAccountNo(objArr[39].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAccountNo("--");
                        }

                        try {
                            if (objArr[40] == null) {
                                dataBean.setCollectFromType("--");
                            } else {
                                dataBean.setCollectFromType(objArr[40].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectFromType("--");
                        }

                        try {
                            if (objArr[41] == null) {
                                dataBean.setCollectionBranchId("--");
                            } else {
                                dataBean.setCollectionBranchId(objArr[41].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectionBranchId("--");
                        }

                        try {
                            if (objArr[42] == null) {
                                dataBean.setPreferredEmail("--");
                            } else {
                                dataBean.setPreferredEmail(objArr[42].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPreferredEmail("--");
                        }

                        try {
                            if (objArr[43] == null) {
                                dataBean.setRequestDate("--");
                            } else {
                                dataBean.setRequestDate(objArr[43].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRequestDate("--");
                        }

                        try {
                            if (objArr[44] == null) {
                                dataBean.setLastupdated("--");
                            } else {
                                dataBean.setLastupdated(objArr[44].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLastupdated("--");
                        }
                        try {
                            if (objArr[45] == null) {
                                dataBean.setLocalTime("--");
                            } else {
                                dataBean.setLocalTime(objArr[45].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLocalTime("--");
                        }
                        try {
                            if (objArr[46] == null) {
                                dataBean.setUserId("--");
                            } else {
                                dataBean.setUserId(objArr[46].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setUserId("--");
                        }
                        try {
                            if (objArr[47] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[47].toString());
                                dataBean.setChannelType(channelTypeDes);
                            } else {
                                dataBean.setChannelType("--");
                            }
                        } catch (Exception e) {
                            dataBean.setChannelType("--");
                        }
                        try {
                            dataBean.setDeviceId(objArr[48].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceId("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        try {
                            if(objArr[49]!=null && objArr[49].equals("703")){
                                dataBean.setStaffStatus("Staff");
                            }else{
                                 dataBean.setStaffStatus("Non Staff");
                            }
                        } catch (NullPointerException e) {
                            dataBean.setStaffStatus("--");
                        }

                        try {
                            dataBean.setAppId(objArr[50].toString());
                        } catch (NullPointerException e) {
                            dataBean.setAppId("--");
                        }
                        try {
                            dataBean.setBillRefNo(objArr[51].toString());
                        } catch (NullPointerException e) {
                            dataBean.setBillRefNo("--");
                        }
                        if (objArr[65]!=null && objArr[65].toString().equalsIgnoreCase("27")) {
                            try {
                                dataBean.setPayTypeDes(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setPayTypeDes("Accounts");
                            }
                            try {
                                dataBean.setTxnMode(objArr[58].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                        }else{
                            try {
                                dataBean.setTxnMode(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                            try {
                                dataBean.setPayTypeDes(objArr[58].toString());
                            } catch (NullPointerException e) {
                                dataBean.setPayTypeDes("Accounts");
                            }
                        }
                        try {
                            dataBean.setTranRefNo(objArr[53].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTranRefNo("--");
                        }
                        try {
                            dataBean.setErrDesc(objArr[54].toString());
                        } catch (NullPointerException e) {
                            dataBean.setErrDesc("--");
                        }
                        try {
                            dataBean.setUserName(objArr[55].toString());
                        } catch (NullPointerException e) {
                            dataBean.setUserName("--");
                        }
                        try {
                            dataBean.setDeviceManufacture(objArr[56].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceManufacture("--");
                        }
                        try {
                            dataBean.setDeviceBuildNumber(objArr[57].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceBuildNumber("--");
                        }
                        try {
                            if(objArr[60].toString().equals("000") || objArr[60].toString().equals("00")) {
                                dataBean.setT24TranStatus("Success");
                            }else{
                                dataBean.setT24TranStatus(objArr[60].toString());
                            }
                        } catch (NullPointerException e) {
                            dataBean.setT24TranStatus("--");
                        }
                        try {
                            dataBean.setT24TranReference(objArr[61].toString());
                        } catch (NullPointerException e) {
                            dataBean.setT24TranReference("--");
                        }
                        try {
                             if(objArr[62].toString().equals("000") || objArr[62].toString().equals("00")) {
                                dataBean.setIBLTranStatus("Success");
                            }else{
                                dataBean.setIBLTranStatus(objArr[62].toString());
                            }
                        } catch (NullPointerException e) {
                            dataBean.setIBLTranStatus("--");
                        }
                        try {
                            dataBean.setIBLReference(objArr[63].toString());
                        } catch (NullPointerException e) {
                            dataBean.setIBLReference("--");
                        }
                        try {
                            dataBean.setIsOneTime(this.getIsOneTimeDesByCode(objArr[64].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsOneTime("--");
                        }
                        try {
                            dataBean.setScheduleId(objArr[66].toString());
                        } catch (NullPointerException e) {
                            dataBean.setScheduleId("--");
                        }
                        try {
                            dataBean.setIsPayToMobile(this.getIsPayToMobile(objArr[67].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsPayToMobile("NO");
                        }

                        dataBeanList.add(dataBean);
                    }
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }

        PartialList<ScheduledPaymentExplorerBean> list = new PartialList<ScheduledPaymentExplorerBean>();

        list.setList(dataBeanList);
        list.setFullCount(count);

        return list;
    }

    private String makeWhereClause(ScheduledPaymentExplorerInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 0);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME >='" + datef + "'";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME <'" + datef + "'";
        }

        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += " and lower(M.NIC) LIKE lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(M.CIF) LIKE lower('%" + inputBean.getCif().trim() + "%')";
        }

        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += " and lower(U.TRANSACTION_TYPE) LIKE lower('%" + inputBean.getTxnType().trim() + "%')";
        }
        if (inputBean.getCustomerCategory() != null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(M.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getChannelType() != null && !inputBean.getChannelType().isEmpty()) {
            where += " and lower(U.CHANNEL_TYPE) LIKE lower('%" + inputBean.getChannelType().trim() + "%')";
        }
        if (inputBean.getCurrencyCode() != null && !inputBean.getCurrencyCode().isEmpty()) {
            where += " and lower(U.CURRENCY_CODE) LIKE lower('%" + inputBean.getCurrencyCode().trim() + "%')";
        }
        if (inputBean.getTranRefNo() != null && !inputBean.getTranRefNo().isEmpty()) {
            where += " and lower(U.T24_REFERENCE) LIKE lower('%" + inputBean.getTranRefNo().trim() + "%')";
        }
        if (inputBean.getIblRefNo()!= null && !inputBean.getIblRefNo().isEmpty()) {
            where += " and lower(U.IBL_REFERENCE) LIKE lower('%" + inputBean.getIblRefNo().trim() + "%')";
        }
        if (inputBean.getBillCategoryName() != null && !inputBean.getBillCategoryName().isEmpty()) {
            where += " and U.BILL_CATEGORY_ID = '" + inputBean.getBillCategoryName().trim() + "'";
        }
        if (inputBean.getBillProviderName() != null && !inputBean.getBillProviderName().isEmpty()) {
            where += " and U.BILL_PROVIDER_ID = '" + inputBean.getBillProviderName().trim() + "'";
        }
        if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
            if (inputBean.getStatus().equals("SUCC")) {
                where += " and U.RESPONCE_CODE ='000'";
            } else if (inputBean.getStatus().equals("FAIL")) {
                where += " and (U.RESPONCE_CODE !='000' OR U.RESPONCE_CODE IS null )";
            }
        }
        if (inputBean.getStaffStatus()!= null && !inputBean.getStaffStatus().isEmpty()) {
            if (inputBean.getStaffStatus().equals("STAFF")) {
                where += " and M.CUSTOMER_CATEGORY IN ('703')";
            } else if (inputBean.getStaffStatus().equals("NONSTAFF")) {
                where += " and  M.CUSTOMER_CATEGORY NOT IN ('703')";
            }
        }

        where += " and U.TRANSACTION_TYPE IN ( '26','27') AND U.IS_SCHEDULE = 1 ";
        return where;
    }

    public SwtTxnType getTxnTypeById(String txnid) throws Exception {
        SwtTxnType ttype = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtTxnType as tt where tt.typecode=:typecode";
            Query query = session.createQuery(sql);
            query.setString("typecode", txnid);

            ttype = (SwtTxnType) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return ttype;
    }

    public StringBuffer makeCSVReport(ScheduledPaymentExplorerInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;

        String TXN_SQL_CSV = "SELECT "
                + "U.TRANSACTION_ID, "//0
                + "T.DESCRIPTION TXN_TYPE, "//1          
                + "U.FROM_ACCOUNT_NUMBER, "//2
                + "U.TO_ACCOUNT_NUMBER, "//3
                + "U.AMOUNT, "//4
                + "M.CIF, "//5
                + "M.NIC, "//6
                + "M.CUSTOMER_NAME, "//7
                + "M.EMAIL, "//8
                + "M.PERMANENT_ADDRESS, "//9
                + "ST.DESCRIPTION STATUS, "//10 
                + "U.LEASE_MODEL, "//11
                + "U.LEASE_TYPE, "//12
                + "U.NTB_REQUEST_TYPE, "//13
                + "U.POSTED_METHOD, "//14
                + "U.RESPONCE_CODE, "//15
                //                + "S.DESCRIPTION RESPONSE,"
                + "M.MOBILE_NUMBER, "//16
                + "U.SERVICE_FEE, "//17
                + "U.PAYEE_NAME, "//18
//                + "U.BILL_CATEGORY_NAME, "//19
//                + "U.BILL_PROVIDER_NAME, "//20
                + "BC.CATEGORY_NAME, "//19
                + "BS.PROVIDER_NAME, "//20
                + "U.BILLER_NAME, "//21
                + "U.MERCHANT_TYPE_NAME, "//22
                + "U.MERCHANT_NAME, "//23
                + "U.REDEEM_POINTS, "//24
                + "U.REDEEM_VOUCHER_ID, "//25
                + "U.LEASE_AMOUNT, "//26
                + "U.LEASE_AVD_PAYMENT, "//27
                + "U.LEASE_SELL_PRICE, "//28
                + "U.CHEQUE_NUMBER, "//29
                + "U.FROM_DATE, "//30
                + "U.TO_DATE, "//31
                + "U.CARD_NUMBER, "//32
                + "U.STATEMENT_FROM, "//33
                + "U.STATEMENT_TO, "//34
                + "U.CURRENCY_CODE, "//35
                + "U.BANK_NAME, "//36
                + "U.BRANCH_NAME, "//37
                + "U.REMARKS, "//38
                + "U.ACCOUNT_NO, "//39
                + "U.COLLECT_FROM_TYPE, "//40
                + "U.COLLECTION_BRANCH_ID, "//41
                + "U.PREFERRED_EMAIL, "//42
                + "U.REQUEST_DATE, "//43
                + "U.LASTUPDATED, "//44                
                + "U.LOCAL_TIME, "//45
                + "U.USER_ID, "//46
                + "U.CHANNEL_TYPE, "//47
                + "U.DEVICE_ID, "//48
                + "M.CUSTOMER_CATEGORY, "//49
                + "U.APP_ID, "//50
                + "U.BILL_REFFRENCE_NUMBER, "//51
                + "TM.DESCRIPTION TXN_MODE_DES, "//52
                + "U.TRAN_REF_NO, "//53
                + "U.ERR_DESC, "//54
                + "M.USERNAME, "//55
                + "D.DEVICE_MANUFACTURE, "//56
                + "D.DEVICE_BUILD_NUMBER, "//57
                + "PT.DESCRIPTION PAY_TYPE_DES, "//58
                + "U.TYPE PAY_TYPE, "//59
                + "U.T24_RESPONSE, "//60
                + "U.T24_REFERENCE, "//61
                + "U.IBL_RESPONSE, "//62
                + "U.IBL_REFERENCE, "//63
                + "U.IS_ONE_TIME, "//64
                + "CAST(U.TRANSACTION_TYPE AS VARCHAR2(3)), "//65
                + "U.SCHEDULE_ID, "//66
                + "U.IS_PAYTO_MOBILE "//67
                //                + "D.DEVICE_MODEL "//55
                + "FROM SWT_TRANSACTION U "
                + "LEFT OUTER JOIN SWT_RESPONSE_CODES S ON S.CODE = U.RESPONCE_CODE "
                + "LEFT OUTER JOIN SWT_TXN_TYPE T ON T.TYPECODE = U.TRANSACTION_TYPE "
                + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
                + "LEFT OUTER JOIN STATUS ST ON ST.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN TRANSACTION_MODE TM ON TM.TRAN_TYPE = U.TRANSACTION_TYPE AND TM.TRAN_MODE = U.TXN_MODE "
                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID AND D.USERID = U.USER_ID "
                + "LEFT OUTER JOIN TXN_PAY_TYPE PT ON PT.TRAN_TYPE = U.TRANSACTION_TYPE AND PT.PAY_TYPE = U.TYPE "
                + "LEFT OUTER JOIN BILL_CATEGORY BC ON BC.CATEGORY_CODE = U.BILL_CATEGORY_ID "
                + "LEFT OUTER JOIN BILL_SERVICE_PROVIDER BS ON BS.PROVIDER_ID = U.BILL_PROVIDER_ID "
                //                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID "
                + "WHERE ";

        try {
            session = HibernateInit.sessionFactory.openSession();
            int count = 0;
            String where1 = this.makeWhereClauseForCSV(inputBean);
            String sqlCount = this.TXN_COUNT_SQL + where1;
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = TXN_SQL_CSV + where1 + this.TRANEXPLORER_ORDER_BY_SQL;
                System.out.println(sql);

                Query query = session.createSQLQuery(sql);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    content = new StringBuffer();
                    List<ScheduledPaymentExplorerBean> beanlist = new ArrayList<ScheduledPaymentExplorerBean>();

                    for (Object[] objArr : objectArrList) {

                        ScheduledPaymentExplorerBean dataBean = new ScheduledPaymentExplorerBean();

                        //set data values to beanlist
                        try {

                            if (objArr[0] == null) {
                                dataBean.setTxnId("--");
                            } else {
                                dataBean.setTxnId(objArr[0].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnId("--");
                        }

                        try {
                            if (objArr[1] == null) {
                                dataBean.setTxnType("--");
                            } else {;
                                dataBean.setTxnType(objArr[1].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnType("--");
                        }

                        try {
                            if (objArr[2] == null) {
                                dataBean.setFromAccNo("--");
                            } else {
                                dataBean.setFromAccNo(objArr[2].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromAccNo("--");
                        }
                        try {
                            if (objArr[3] == null) {
                                dataBean.setToAccNo("--");
                            } else {
                                dataBean.setToAccNo(objArr[3].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToAccNo("--");
                        }

                        try {
                            if (objArr[4] == null) {
                                dataBean.setAmount("--");
                            } else {
                                dataBean.setAmount(objArr[4].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAmount("--");
                        }

                        try {
                            if (objArr[5] == null) {
                                dataBean.setCustCif("--");
                            } else {
                                dataBean.setCustCif(objArr[5].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustCif("--");
                        }

                        try {
                            if (objArr[6] == null) {
                                dataBean.setNic("--");
                            } else {
                                dataBean.setNic(objArr[6].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNic("--");
                        }

                        try {
                            if (objArr[7] == null) {
                                dataBean.setCustName("--");
                            } else {
                                dataBean.setCustName(objArr[7].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustName("--");
                        }

                        try {
                            if (objArr[8] == null) {
                                dataBean.setCustEmail("--");
                            } else {
                                dataBean.setCustEmail(objArr[8].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustEmail("--");
                        }

                        try {
                            if (objArr[9] == null) {
                                dataBean.setAddress1("--");
                            } else {
                                dataBean.setAddress1(objArr[9].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAddress1("--");
                        }

                        try {
                            if (objArr[10] == null) {
                                dataBean.setStatus("--");
                            } else {
                                dataBean.setStatus(objArr[10].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatus("--");
                        }

                        try {
                            if (objArr[11] == null) {
                                dataBean.setLeasingModel("--");
                            } else {
                                dataBean.setLeasingModel(objArr[11].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingModel("--");
                        }

                        try {
                            if (objArr[12] == null) {
                                dataBean.setLeasingTypes("--");
                            } else {
                                dataBean.setLeasingTypes(objArr[12].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingTypes("--");
                        }

                        try {
                            if (objArr[13] == null) {
                                dataBean.setNtbRequest("--");
                            } else {
                                dataBean.setNtbRequest(objArr[13].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNtbRequest("--");
                        }

                        try {
                            if (objArr[14] == null) {
                                dataBean.setPostedMethod("--");
                            } else {
                                dataBean.setPostedMethod(objArr[14].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPostedMethod("--");
                        }

                        try {
                            if (objArr[15] == null) {
                                dataBean.setResponseCodes("Failure");
                            } else {
                                if (objArr[15].equals("000")) {
                                    dataBean.setResponseCodes("Success");
                                } else {
                                    dataBean.setResponseCodes("Failure");
                                }
//                                dataBean.setResponseCodes(objArr[15].toString());
                                //dataBean.setResponseCodes(objArr[15].getSwtResponseCodes().getDescription());        
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setResponseCodes("Failure");
                        }

                        try {
                            if (objArr[16] == null) {
                                dataBean.setMobileNumber("--");
                            } else {
                                dataBean.setMobileNumber(objArr[16].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMobileNumber("--");
                        }

                        try {
                            if (objArr[17] == null) {
                                dataBean.setServiceFee("--");
                            } else {
                                dataBean.setServiceFee(objArr[17].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setServiceFee("--");
                        }

                        try {
                            if (objArr[18] == null) {
                                dataBean.setPayeeName("--");
                            } else {
                                dataBean.setPayeeName(objArr[18].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPayeeName("--");
                        }

                        try {
                            if (objArr[19] == null) {
                                dataBean.setBillCategoryName("--");
                            } else {
                                dataBean.setBillCategoryName(objArr[19].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillCategoryName("--");
                        }

                        try {
                            if (objArr[20] == null) {
                                dataBean.setBillProviderName("--");
                            } else {
                                dataBean.setBillProviderName(objArr[20].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillProviderName("--");
                        }

                        try {
                            if (objArr[21] == null) {
                                dataBean.setBillerName("--");
                            } else {
                                dataBean.setBillerName(objArr[21].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillerName("--");
                        }
                        try {
                            if (objArr[22] == null) {
                                dataBean.setMerchantTypeName("--");
                            } else {
                                dataBean.setMerchantTypeName(objArr[22].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantTypeName("--");
                        }

                        try {
                            if (objArr[23] == null) {
                                dataBean.setMerchantName("--");
                            } else {
                                dataBean.setMerchantName(objArr[23].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantName("--");
                        }

                        try {
                            if (objArr[24] == null) {
                                dataBean.setRedeemPoints("--");
                            } else {
                                dataBean.setRedeemPoints(objArr[24].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemPoints("--");
                        }

                        try {
                            if (objArr[25] == null) {
                                dataBean.setRedeemVoucherId("--");
                            } else {
                                dataBean.setRedeemVoucherId(objArr[25].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemVoucherId("--");
                        }

                        try {
                            if (objArr[26] == null) {
                                dataBean.setLeaseAmount("--");
                            } else {
                                dataBean.setLeaseAmount(objArr[26].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAmount("--");
                        }

                        try {
                            if (objArr[27] == null) {
                                dataBean.setLeaseAvdPayment("--");
                            } else {
                                dataBean.setLeaseAvdPayment(objArr[27].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAvdPayment("--");
                        }

                        try {
                            if (objArr[28] == null) {
                                dataBean.setLeaseSellPrice("--");
                            } else {
                                dataBean.setLeaseSellPrice(objArr[28].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseSellPrice("--");
                        }

                        try {
                            if (objArr[29] == null) {
                                dataBean.setChequeNumber("--");
                            } else {
                                dataBean.setChequeNumber(objArr[29].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setChequeNumber("--");
                        }
                        try {
                            if (objArr[30] == null) {
                                dataBean.setFromDate("--");
                            } else {
                                dataBean.setFromDate(objArr[30].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromDate("--");
                        }

                        try {
                            if (objArr[31] == null) {
                                dataBean.setToDate("--");
                            } else {
                                dataBean.setToDate(objArr[31].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToDate("--");
                        }

                        try {
                            if (objArr[32] == null) {
                                dataBean.setCardNumber("--");
                            } else {
//                                dataBean.setCardNumber(toMaskCardNumber(objArr[32].toString()));
                                dataBean.setCardNumber(objArr[32].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCardNumber("--");
                        }

                        try {
                            if (objArr[33] == null) {
                                dataBean.setStatementFrom("--");
                            } else {
                                dataBean.setStatementFrom(objArr[33].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementFrom("--");
                        }

                        try {
                            if (objArr[34] == null) {
                                dataBean.setStatementTo("--");
                            } else {
                                dataBean.setStatementTo(objArr[34].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementTo("--");
                        }

                        try {
                            if (objArr[35] == null) {
                                dataBean.setCurrencyCode("--");
                            } else {
                                dataBean.setCurrencyCode(objArr[35].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCurrencyCode("--");
                        }

                        try {
                            if (objArr[36] == null) {
                                dataBean.setBankName("--");
                            } else {
                                dataBean.setBankName(objArr[36].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBankName("--");
                        }

                        try {
                            if (objArr[37] == null) {
                                dataBean.setBranchName("--");
                            } else {
                                dataBean.setBranchName(objArr[37].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBranchName("--");
                        }
                        try {
                            if (objArr[38] == null) {
                                dataBean.setRemarks("--");
                            } else {
                                dataBean.setRemarks(objArr[38].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRemarks("--");
                        }

                        try {
                            if (objArr[39] == null) {
                                dataBean.setAccountNo("--");
                            } else {
                                dataBean.setAccountNo(objArr[39].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAccountNo("--");
                        }

                        try {
                            if (objArr[40] == null) {
                                dataBean.setCollectFromType("--");
                            } else {
                                dataBean.setCollectFromType(objArr[40].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectFromType("--");
                        }

                        try {
                            if (objArr[41] == null) {
                                dataBean.setCollectionBranchId("--");
                            } else {
                                dataBean.setCollectionBranchId(objArr[41].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectionBranchId("--");
                        }

                        try {
                            if (objArr[42] == null) {
                                dataBean.setPreferredEmail("--");
                            } else {
                                dataBean.setPreferredEmail(objArr[42].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPreferredEmail("--");
                        }

                        try {
                            if (objArr[43] == null) {
                                dataBean.setRequestDate("--");
                            } else {
                                dataBean.setRequestDate(objArr[43].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRequestDate("--");
                        }

                        try {
                            if (objArr[44] == null) {
                                dataBean.setLastupdated("--");
                            } else {
                                dataBean.setLastupdated(objArr[44].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLastupdated("--");
                        }
                        try {
                            if (objArr[45] == null) {
                                dataBean.setLocalTime("--");
                            } else {
                                dataBean.setLocalTime(objArr[45].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLocalTime("--");
                        }
                        try {
                            if (objArr[46] == null) {
                                dataBean.setUserId("--");
                            } else {
                                dataBean.setUserId(objArr[46].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setUserId("--");
                        }
                        try {
                            if (objArr[47] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[47].toString());
                                dataBean.setChannelType(channelTypeDes);
                            } else {
                                dataBean.setChannelType("--");
                            }
                        } catch (Exception e) {
                            dataBean.setChannelType("--");
                        }
                        try {
                            dataBean.setDeviceId(objArr[48].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceId("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        try {
                            if(objArr[49]!=null && objArr[49].equals("703")){
                                dataBean.setStaffStatus("Staff");
                            }else{
                                 dataBean.setStaffStatus("Non Staff");
                            }
                        } catch (NullPointerException e) {
                            dataBean.setStaffStatus("--");
                        }

                        try {
                            dataBean.setAppId(objArr[50].toString());
                        } catch (NullPointerException e) {
                            dataBean.setAppId("--");
                        }
                        try {
                            dataBean.setBillRefNo(objArr[51].toString());
                        } catch (NullPointerException e) {
                            dataBean.setBillRefNo("--");
                        }
                        if (objArr[65]!=null && objArr[65].toString().equalsIgnoreCase("27")) {
                            try {
                                dataBean.setPayTypeDes(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setPayTypeDes("Accounts");
                            }
                            try {
                                dataBean.setTxnMode(objArr[58].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                        }else{
                            try {
                                dataBean.setTxnMode(objArr[52].toString());
                            } catch (NullPointerException e) {
                                dataBean.setTxnMode("--");
                            }
                            try {
                                dataBean.setPayTypeDes(objArr[58].toString());
                            } catch (NullPointerException e) {
                                dataBean.setPayTypeDes("Accounts");
                            }
                        }
                        try {
                            dataBean.setTranRefNo(objArr[53].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTranRefNo("--");
                        }
                        try {
                            dataBean.setErrDesc(objArr[54].toString());
                        } catch (NullPointerException e) {
                            dataBean.setErrDesc("--");
                        }

                        try {
                            dataBean.setUserName(objArr[55].toString());
                        } catch (NullPointerException e) {
                            dataBean.setUserName("--");
                        }
                        try {
                            dataBean.setDeviceManufacture(objArr[56].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceManufacture("--");
                        }
                        try {
                            dataBean.setDeviceBuildNumber(objArr[57].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceBuildNumber("--");
                        }
                        try {
                            if(objArr[60].toString().equals("000") || objArr[60].toString().equals("00")) {
                                dataBean.setT24TranStatus("Success");
                            }else{
                                dataBean.setT24TranStatus(objArr[60].toString());
                            }
                        } catch (NullPointerException e) {
                            dataBean.setT24TranStatus("--");
                        }
                        try {
                            dataBean.setT24TranReference(objArr[61].toString());
                        } catch (NullPointerException e) {
                            dataBean.setT24TranReference("--");
                        }
                        try {
                            if(objArr[62].toString().equals("000") || objArr[62].toString().equals("00")) {
                                dataBean.setIBLTranStatus("Success");
                            }else{
                                dataBean.setIBLTranStatus(objArr[62].toString());
                            }
                        } catch (NullPointerException e) {
                            dataBean.setIBLTranStatus("--");
                        }
                        try {
                            dataBean.setIBLReference(objArr[63].toString());
                        } catch (NullPointerException e) {
                            dataBean.setIBLReference("--");
                        }
                        try {
                            dataBean.setIsOneTime(this.getIsOneTimeDesByCode(objArr[64].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsOneTime("--");
                        }
                        try {
                            dataBean.setScheduleId(objArr[66].toString());
                        } catch (NullPointerException e) {
                            dataBean.setScheduleId("--");
                        }
                        try {
                            dataBean.setIsPayToMobile(this.getIsPayToMobile(objArr[67].toString()));
                        } catch (NullPointerException e) {
                            dataBean.setIsPayToMobile("NO");
                        }

                        beanlist.add(dataBean);
                    }

                    //write column headers to csv file
                    content.append("Transaction ID");
                    content.append(',');
                    content.append("Schedule ID");
                    content.append(',');
                    content.append("Transaction Time");
                    content.append(',');
                    content.append("Transaction Mode");
                    content.append(',');
                    content.append("Pay Type");
                    content.append(',');
                    content.append("Pay To Mobile");
                    content.append(',');
                    content.append("One Time/Registered");
                    content.append(',');
                    content.append("From AccountNumber");
                    content.append(',');
                    content.append("To Account Number");
                    content.append(',');
                    content.append("Currency Code");
                    content.append(',');
                    content.append("Amount");
                    content.append(',');
                    content.append("Channel Type");
                    content.append(',');
                    content.append("Status");
                    content.append(',');
                    content.append("T24 Status");
                    content.append(',');
                    content.append("T24 Reference");
                    content.append(',');
                    content.append("IBL Status");
                    content.append(',');
                    content.append("IBL Reference");
                    content.append(',');
                    content.append("Error Description");
                    content.append(',');
                    content.append("Customer CID");
                    content.append(',');
                    content.append("Customer Category");
                    content.append(',');
                    content.append("Staff/Non Staff");
                    content.append(',');
                    content.append("NIC");
                    content.append(',');
                    content.append("User Name");
                    content.append(',');
                    content.append("Customer Name");
                    content.append(',');
                    content.append("Mobile Number");
                    content.append(',');
                    content.append("App ID");
                    content.append(',');
                    content.append("Device Manufacture");
                    content.append(',');
                    content.append("Device Build Number");
                    content.append(',');
                    content.append("Bill Reference Number");
                    content.append(',');
                    content.append("Bill Category Name");
                    content.append(',');
                    content.append("Bill Provider Name");
                    content.append(',');
                    content.append("Biller Name");
                    content.append(',');
                    content.append("Bank Name");
                    content.append(',');
                    content.append("Branch Name");
                    content.append(',');
                    content.append("Remarks");
                    content.append(',');
                    content.append("Payee Name");
                    content.append(',');
                    content.append("Service Fee");
                    content.append('\n');

                    //write data values to csv file
                    for (ScheduledPaymentExplorerBean dataBean : beanlist) {
                        try {
                            if (dataBean.getTxnId() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getTxnId());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getScheduleId()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getScheduleId());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {//22
                            if (dataBean.getLocalTime() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLocalTime());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getTxnMode() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getTxnMode());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getPayTypeDes() == null) {
                                content.append("Accounts");
                                content.append(',');
                            } else {
                                content.append(dataBean.getPayTypeDes());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("Accounts");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIsPayToMobile()== null) {
                                content.append("No");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIsPayToMobile());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("No");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIsOneTime() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIsOneTime());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getFromAccNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getFromAccNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getToAccNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getToAccNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCurrencyCode() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCurrencyCode());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getAmount() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getAmount());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getChannelType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getChannelType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getStatus() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getStatus());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getResponseCodes() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getResponseCodes());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getT24TranStatus()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getT24TranStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getT24TranReference()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getT24TranReference());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIBLTranStatus()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIBLTranStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getIBLReference()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getIBLReference());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getErrDesc() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getErrDesc());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustCif() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustCif());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerCategory() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getStaffStatus()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getStaffStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getNic() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getNic());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getUserName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getUserName()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getMobileNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMobileNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getAppId() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getAppId());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDeviceManufacture() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDeviceManufacture());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDeviceBuildNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDeviceBuildNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBillRefNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillRefNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBillCategoryName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillCategoryName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBillProviderName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillProviderName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {//22
                            if (dataBean.getBillerName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillerName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBankName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBankName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBranchName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBranchName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getRemarks() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getRemarks()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getPayeeName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getPayeeName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getServiceFee() == null) {
                                content.append("--");
//                                content.append(',');
                            } else {
                                content.append(dataBean.getServiceFee());
//                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
//                            content.append(',');
                        }
                        content.append('\n');
                    }
                    content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
                    content.append('\n');

                    content.append("Transaction Type :");
                    if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty() && inputBean.getTxnType() != "-ALL-") {
                        String description = getTransactionTypeDescription(inputBean, session);
                        if (description != null) {
                            inputBean.setResponseDescription(description);
                            content.append(description);
                        }
                    } else {
                        inputBean.setResponseDescription("-ALL-");
                        content.append("-ALL-");
                    }
                    content.append('\n');

                    content.append("T24 Reference :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getTranRefNo()));
                    content.append('\n');
                    
                    content.append("IBL Reference :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getIblRefNo()));
                    content.append('\n');

                    content.append("Channel Type :");
                    String channelTypeCode = Common.replaceEmptyorNullStringToALL(inputBean.getChannelType());
                    content.append(this.getChannelTypeDesByCode(channelTypeCode));
                    content.append('\n');

                    content.append("Status :");
                    if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
                        String description = getStatusDescription(inputBean.getStatus());
                        if (description != null) {
                            content.append(description);
                        } else {
                            content.append("-ALL-");
                        }
                    } else {
                        content.append("-ALL-");
                    }
                    content.append('\n');

                    content.append("Currency Code :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCurrencyCode()));
                    content.append('\n');

                    content.append("Customer NIC :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getNic()));
                    content.append('\n');

                    content.append("Customer Category :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategory()));
                    content.append('\n');

                    content.append("Customer CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
                    content.append('\n');

                    content.append("Bill Category Name :");
                    if (inputBean.getBillCategoryName() != null && !inputBean.getBillCategoryName().isEmpty() ) {
                        String description = getBillCategoryName(inputBean.getBillCategoryName(), session);
                        if (description != null) {
                            content.append(description);
                        }
                    } else {
                        content.append("-ALL-");
                    }
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getBillCategoryName()));
                    content.append('\n');

                    content.append("Bill Provider Name :");
                    if (inputBean.getBillProviderName() != null && !inputBean.getBillProviderName().isEmpty() ) {
                        String description = getBillProviderName(inputBean.getBillProviderName(), session);
                        if (description != null) {
                            content.append(description);
                        }
                    } else {
                        content.append("-ALL-");
                    }
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getBillProviderName()));
                    content.append('\n');
                    
                    content.append("Staff/Non Staff :");
                    if (inputBean.getStaffStatus()!= null && !inputBean.getStaffStatus().isEmpty()) {
                        String description = getStaffStatusDescription(inputBean.getStaffStatus());
                        if (description != null) {
                            content.append(description);
                        } else {
                            content.append("-ALL-");
                        }
                    } else {
                        content.append("-ALL-");
                    }
                    content.append('\n');

                }

            }

        } catch (Exception e) {
            throw e;
        } finally {
            
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    private String getTransactionTypeDescription(ScheduledPaymentExplorerInputBean inputBean, Session session) throws ParseException {

        String ttype = null;
        try {

            String sql = "select description FROM SwtTxnType as tt WHERE tt.typecode=:txnType";
            Query query = session.createQuery(sql);
            query.setString("txnType", inputBean.getTxnType());

            ttype = (String) query.list().get(0);

        } catch (Exception e) {
            throw e;
        }
        return ttype;
    }
    
    private String getBillProviderName(String billProviderCode, Session session) throws ParseException {

        String billProviderName = null;
        try {

            String sql = "select u.providerName from BillServiceProvider as u where u.providerId=:providerId";
            Query query = session.createQuery(sql);
            query.setString("providerId", billProviderCode);

            billProviderName = (String) query.list().get(0);

        } catch (Exception e) {
            throw e;
        }
        return billProviderName;
    }
    
    private String getBillCategoryName(String billCategoryCode, Session session) throws ParseException {

        String billCategoryName = null;
        try {

            String sql = "select u.name from BillerCategory as u where u.billercatcode=:billercatcode";
            Query query = session.createQuery(sql);
            query.setString("billercatcode", billCategoryCode);

            billCategoryName = (String) query.list().get(0);

        } catch (Exception e) {
            throw e;
        }
        return billCategoryName;
    }

    private String makeWhereClauseForCSV(ScheduledPaymentExplorerInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 0);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME >='" + datef + "'";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME <'" + datef + "'";
        }

        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += " and lower(M.NIC) LIKE lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(M.CIF) LIKE lower('%" + inputBean.getCif().trim() + "%')";
        }

        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += " and lower(U.TRANSACTION_TYPE) LIKE lower('%" + inputBean.getTxnType().trim() + "%')";
        }
//        if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty()) {
//            where += " and lower(U.RESPONCE_CODE) LIKE lower('%" + inputBean.getResponseCode().trim() + "%')";
//        }
        if (inputBean.getCustomerCategory() != null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(M.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getChannelType() != null && !inputBean.getChannelType().isEmpty()) {
            where += " and lower(U.CHANNEL_TYPE) LIKE lower('%" + inputBean.getChannelType().trim() + "%')";
        }
        if (inputBean.getCurrencyCode() != null && !inputBean.getCurrencyCode().isEmpty()) {
            where += " and lower(U.CURRENCY_CODE) LIKE lower('%" + inputBean.getCurrencyCode().trim() + "%')";
        }
        if (inputBean.getTranRefNo() != null && !inputBean.getTranRefNo().isEmpty()) {
            where += " and lower(U.T24_REFERENCE) LIKE lower('%" + inputBean.getTranRefNo().trim() + "%')";
        }
        if (inputBean.getIblRefNo()!= null && !inputBean.getIblRefNo().isEmpty()) {
            where += " and lower(U.IBL_REFERENCE) LIKE lower('%" + inputBean.getIblRefNo().trim() + "%')";
        }
        if (inputBean.getBillCategoryName() != null && !inputBean.getBillCategoryName().isEmpty()) {
            where += " and U.BILL_CATEGORY_ID = '" + inputBean.getBillCategoryName().trim() + "'";
        }
        if (inputBean.getBillProviderName() != null && !inputBean.getBillProviderName().isEmpty()) {
            where += " and U.BILL_PROVIDER_ID = '" + inputBean.getBillProviderName().trim() + "'";
        }
//        if (inputBean.getStatus()!= null && !inputBean.getStatus().isEmpty()) {
//            where += " and U.STATUS ='" + inputBean.getStatus().trim() + "'";
//        }
        if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
            if (inputBean.getStatus().equals("SUCC")) {
                where += " and U.RESPONCE_CODE ='000'";
            } else if (inputBean.getStatus().equals("FAIL")) {
                where += " and (U.RESPONCE_CODE !='000' OR U.RESPONCE_CODE IS null )";
            }
        }
        if (inputBean.getStaffStatus()!= null && !inputBean.getStaffStatus().isEmpty()) {
            if (inputBean.getStaffStatus().equals("STAFF")) {
                where += " and M.CUSTOMER_CATEGORY IN ('703')";
            } else if (inputBean.getStaffStatus().equals("NONSTAFF")) {
                where += " and  M.CUSTOMER_CATEGORY NOT IN ('703')";
            }
        }

//        where += " and U.TRANSACTION_TYPE IN ( '49','26','27') ";
        where += " and U.TRANSACTION_TYPE IN ( '26','27') AND U.IS_SCHEDULE = 1 ";
        return where;
    }

    public String getChannelTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("2")) {
            des = "Internet Banking";
        } else if (code.equals("1")) {
            des = "Mobile Banking";
        } else {
            des = code;
        }
        return des;

    }
    
    public String getIsOneTimeDesByCode(String code) {
        String des = "--";
        if (code.equals("1")) {
            des = "One Time Transaction";
        } else if (code.equals("0")) {
            des = "Registered Transaction";
        } else {
            des = code;
        }
        return des;

    }

    public String getStatusDescription(String status) throws ParseException {

        String rDescription = null;

        if (status != null && !status.isEmpty()) {
            if (status.equals("SUCC")) {
                rDescription = "Success";
            } else if (status.equals("FAIL")) {
                rDescription = "Failure";
            }
        }
        return rDescription;
    }
    
    public String getStaffStatusDescription(String status) throws ParseException {

        String rDescription = null;

        if (status != null && !status.isEmpty()) {
            if (status.equals("STAFF")) {
                rDescription = "Staff";
            } else if (status.equals("NONSTAFF")) {
                rDescription = "Non Staff";
            }
        }
        return rDescription;
    }
    
    public String getIsPayToMobile(String code) {
        String des = "No";
        if (code.equals("1")) {
            des = "Yes";
        }
        return des;

    }
}
