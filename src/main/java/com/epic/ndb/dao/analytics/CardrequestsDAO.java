package com.epic.ndb.dao.analytics;

import com.epic.ndb.bean.analytics.CardRequestsBean;
import com.epic.ndb.bean.analytics.CardRequestsInputBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.ExcelCommon;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.PartialList;
import com.epic.ndb.util.mapping.SwtTxnType;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author jayathissa_d
 */
public class CardrequestsDAO {

    private final int headerRowCount = 18;
    private String TXN_COUNT_SQL = "SELECT "
            + "COUNT(TRANSACTION_ID) "
            + "FROM SWT_TRANSACTION U "
            + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
            + "WHERE ";
    private String TRANEXPLORER_ORDER_BY_SQL = " order by U.LOCAL_TIME DESC ";

    public PartialList<CardRequestsBean> getSearchList(CardRequestsInputBean inputBean, int rows, int from, String sortIndex, String sortOrder) throws Exception {
        List<CardRequestsBean> dataBeanList = new ArrayList<CardRequestsBean>();
        Session session = null;
        int count = 0;
        if ("".equals(sortIndex.trim())) {
            sortIndex = null;
        }
        String CUSTOMER_SQL_SEARCH = "SELECT "
                + "U.TRANSACTION_ID, "//0
                + "T.DESCRIPTION TXN_TYPE, "//1          
                + "U.FROM_ACCOUNT_NUMBER, "//2
                + "U.TO_ACCOUNT_NUMBER, "//3
                + "U.AMOUNT, "//4
//                + "U.CUST_CIF, "//5
//                + "U.CUST_NIC, "//6
//                + "U.CUST_NAME, "//7
//                + "U.CUST_EMAIL, "//8
//                + "U.ADDRESS_1, "//9 
                + "M.CIF, "//5
                + "M.NIC, "//6
                + "M.CUSTOMER_NAME, "//7
                + "M.EMAIL, "//8
                + "M.PERMANENT_ADDRESS, "//9 
                + "ST.DESCRIPTION STATUS, "//10 
                + "U.LEASE_MODEL, "//11
                + "U.LEASE_TYPE, "//12
                + "U.NTB_REQUEST_TYPE, "//13
                + "U.POSTED_METHOD, "//14
                + "U.RESPONCE_CODE, "//15
//                + "S.DESCRIPTION RESPONSE,"
                + "M.MOBILE_NUMBER, "//16
                + "U.SERVICE_FEE, "//17
                + "U.PAYEE_NAME, "//18
                + "U.BILL_CATEGORY_NAME, "//19
                + "U.BILL_PROVIDER_NAME, "//20
                + "U.BILLER_NAME, "//21
                + "U.MERCHANT_TYPE_NAME, "//22
                + "U.MERCHANT_NAME, "//23
                + "U.REDEEM_POINTS, "//24
                + "U.REDEEM_VOUCHER_ID, "//25
                + "U.LEASE_AMOUNT, "//26
                + "U.LEASE_AVD_PAYMENT, "//27
                + "U.LEASE_SELL_PRICE, "//28
                + "U.CHEQUE_NUMBER, "//29
                + "U.FROM_DATE, "//30
                + "U.TO_DATE, "//31
                + "U.CARD_NUMBER, "//32
                + "U.STATEMENT_FROM, "//33
                + "U.STATEMENT_TO, "//34
                + "U.CURRENCY_CODE, "//35
                + "U.BANK_NAME, "//36
                + "U.BRANCH_NAME, "//37
                + "U.REMARKS, "//38
                + "U.ACCOUNT_NO, "//39
                + "U.COLLECT_FROM_TYPE, "//40
                + "U.COLLECTION_BRANCH_ID, "//41
                + "U.PREFERRED_EMAIL, "//42
                + "U.REQUEST_DATE, "//43
                + "U.LASTUPDATED, "//44                
                + "U.LOCAL_TIME, "//45
                + "U.USER_ID, "//46
                + "U.CHANNEL_TYPE, "//47
                + "U.DEVICE_ID, "//48
                + "M.CUSTOMER_CATEGORY, "//49
                + "U.APP_ID, "//50
                + "U.BILL_REFFRENCE_NUMBER, "//51
                + "TM.DESCRIPTION TXN_MODE_DES, "//52
                + "U.TRAN_REF_NO, "//53
//                + "U.REQUEST_ID, "//53
                + "U.ERR_DESC, "//54
                + "M.USERNAME, "//55
                + "D.DEVICE_MANUFACTURE, "//56
                + "D.DEVICE_BUILD_NUMBER "//57
//                + "CAST(U.TRANSACTION_TYPE AS VARCHAR2(3)) "//55
//                + "D.DEVICE_MODEL "//57
                + "FROM SWT_TRANSACTION U "
                + "LEFT OUTER JOIN SWT_RESPONSE_CODES S ON S.CODE = U.RESPONCE_CODE "
                + "LEFT OUTER JOIN SWT_TXN_TYPE T ON T.TYPECODE = U.TRANSACTION_TYPE "
                + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
                + "LEFT OUTER JOIN STATUS ST ON ST.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN TRANSACTION_MODE TM ON TM.TRAN_TYPE = U.TRANSACTION_TYPE AND TM.TRAN_MODE = U.TXN_MODE "
                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID AND D.USERID = U.USER_ID "
//                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID "
                + "WHERE ";
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            String where1 = this.makeWhereClause(inputBean);
            String sqlCount = this.TXN_COUNT_SQL + where1;
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = CUSTOMER_SQL_SEARCH + where1 + this.TRANEXPLORER_ORDER_BY_SQL;
                System.out.println(sql);

                Query query = session.createSQLQuery(sql);
                query.setMaxResults(rows);
                query.setFirstResult(from);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {
                    
                    dataBeanList = new ArrayList<CardRequestsBean>();
                    
                    for (Object[] objArr : objectArrList) {

                        CardRequestsBean dataBean = new CardRequestsBean();

                        //set data values to beanlist
                        try {

                            if (objArr[0] == null) {
                                dataBean.setTxnId("--");
                            } else {
                                dataBean.setTxnId(objArr[0].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnId("--");
                        }

                        try {
                            if (objArr[1] == null) {
                                dataBean.setTxnType("--");
                            } else {;
                                dataBean.setTxnType(objArr[1].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnType("--");
                        }

                        try {
                            if (objArr[2] == null) {
                                dataBean.setFromAccNo("--");
                            } else {
                                dataBean.setFromAccNo(objArr[2].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromAccNo("--");
                        }
                        try {
                            if (objArr[3] == null) {
                                dataBean.setToAccNo("--");
                            } else {
                                dataBean.setToAccNo(objArr[3].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToAccNo("--");
                        }

                        try {
                            if (objArr[4] == null) {
                                dataBean.setAmount("--");
                            } else {
                                dataBean.setAmount(objArr[4].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAmount("--");
                        }

                        try {
                            if (objArr[5] == null) {
                                dataBean.setCustCif("--");
                            } else {
                                dataBean.setCustCif(objArr[5].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustCif("--");
                        }

                        try {
                            if (objArr[6] == null) {
                                dataBean.setNic("--");
                            } else {
                                dataBean.setNic(objArr[6].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNic("--");
                        }

                        try {
                            if (objArr[7] == null) {
                                dataBean.setCustName("--");
                            } else {
                                dataBean.setCustName(objArr[7].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustName("--");
                        }

                        try {
                            if (objArr[8] == null) {
                                dataBean.setCustEmail("--");
                            } else {
                                dataBean.setCustEmail(objArr[8].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustEmail("--");
                        }

                        try {
                            if (objArr[9] == null) {
                                dataBean.setAddress1("--");
                            } else {
                                dataBean.setAddress1(objArr[9].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAddress1("--");
                        }

                        try {
                            if (objArr[10] == null) {
                                dataBean.setStatus("--");
                            } else {
                                dataBean.setStatus(objArr[10].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatus("--");
                        }

                        try {
                            if (objArr[11] == null) {
                                dataBean.setLeasingModel("--");
                            } else {
                                dataBean.setLeasingModel(objArr[11].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingModel("--");
                        }

                        try {
                            if (objArr[12] == null) {
                                dataBean.setLeasingTypes("--");
                            } else {
                                dataBean.setLeasingTypes(objArr[12].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingTypes("--");
                        }

                        try {
                            if (objArr[13] == null) {
                                dataBean.setNtbRequest("--");
                            } else {
                                dataBean.setNtbRequest(objArr[13].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNtbRequest("--");
                        }

                        try {
                            if (objArr[14] == null) {
                                dataBean.setPostedMethod("--");
                            } else {
                                dataBean.setPostedMethod(objArr[14].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPostedMethod("--");
                        }

                        try {
                            if (objArr[15] == null) {
                                dataBean.setResponseCodes("Failure");
                            } else {
                                if(objArr[15].equals("000")){
                                    dataBean.setResponseCodes("Success");
                                }else{
                                    dataBean.setResponseCodes("Failure");
                                }
//                                dataBean.setResponseCodes(objArr[15].toString());
                                //dataBean.setResponseCodes(objArr[15].getSwtResponseCodes().getDescription());        
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setResponseCodes("Failure");
                        }

                        try {
                            if (objArr[16] == null) {
                                dataBean.setMobileNumber("--");
                            } else {
                                dataBean.setMobileNumber(objArr[16].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMobileNumber("--");
                        }

                        try {
                            if (objArr[17] == null) {
                                dataBean.setServiceFee("--");
                            } else {
                                dataBean.setServiceFee(objArr[17].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setServiceFee("--");
                        }

                        try {
                            if (objArr[18] == null) {
                                dataBean.setPayeeName("--");
                            } else {
                                dataBean.setPayeeName(objArr[18].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPayeeName("--");
                        }

                        try {
                            if (objArr[19] == null) {
                                dataBean.setBillCategoryName("--");
                            } else {
                                dataBean.setBillCategoryName(objArr[19].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillCategoryName("--");
                        }

                        try {
                            if (objArr[20] == null) {
                                dataBean.setBillProviderName("--");
                            } else {
                                dataBean.setBillProviderName(objArr[20].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillProviderName("--");
                        }

                        try {
                            if (objArr[21] == null) {
                                dataBean.setBillerName("--");
                            } else {
                                dataBean.setBillerName(objArr[21].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillerName("--");
                        }
                        try {
                            if (objArr[22] == null) {
                                dataBean.setMerchantTypeName("--");
                            } else {
                                dataBean.setMerchantTypeName(objArr[22].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantTypeName("--");
                        }

                        try {
                            if (objArr[23] == null) {
                                dataBean.setMerchantName("--");
                            } else {
                                dataBean.setMerchantName(objArr[23].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantName("--");
                        }

                        try {
                            if (objArr[24] == null) {
                                dataBean.setRedeemPoints("--");
                            } else {
                                dataBean.setRedeemPoints(objArr[24].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemPoints("--");
                        }

                        try {
                            if (objArr[25] == null) {
                                dataBean.setRedeemVoucherId("--");
                            } else {
                                dataBean.setRedeemVoucherId(objArr[25].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemVoucherId("--");
                        }

                        try {
                            if (objArr[26] == null) {
                                dataBean.setLeaseAmount("--");
                            } else {
                                dataBean.setLeaseAmount(objArr[26].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAmount("--");
                        }

                        try {
                            if (objArr[27] == null) {
                                dataBean.setLeaseAvdPayment("--");
                            } else {
                                dataBean.setLeaseAvdPayment(objArr[27].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAvdPayment("--");
                        }

                        try {
                            if (objArr[28] == null) {
                                dataBean.setLeaseSellPrice("--");
                            } else {
                                dataBean.setLeaseSellPrice(objArr[28].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseSellPrice("--");
                        }

                        try {
                            if (objArr[29] == null) {
                                dataBean.setChequeNumber("--");
                            } else {
                                dataBean.setChequeNumber(objArr[29].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setChequeNumber("--");
                        }
                        try {
                            if (objArr[30] == null) {
                                dataBean.setFromDate("--");
                            } else {
                                dataBean.setFromDate(objArr[30].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromDate("--");
                        }

                        try {
                            if (objArr[31] == null) {
                                dataBean.setToDate("--");
                            } else {
                                dataBean.setToDate(objArr[31].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToDate("--");
                        }

                        try {
                            if (objArr[32] == null) {
                                dataBean.setCardNumber("--");
                            } else {
//                                dataBean.setCardNumber(toMaskCardNumber(objArr[32].toString()));
                                dataBean.setCardNumber(objArr[32].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCardNumber("--");
                        }

                        try {
                            if (objArr[33] == null) {
                                dataBean.setStatementFrom("--");
                            } else {
                                dataBean.setStatementFrom(objArr[33].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementFrom("--");
                        }

                        try {
                            if (objArr[34] == null) {
                                dataBean.setStatementTo("--");
                            } else {
                                dataBean.setStatementTo(objArr[34].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementTo("--");
                        }

                        try {
                            if (objArr[35] == null) {
                                dataBean.setCurrencyCode("--");
                            } else {
                                dataBean.setCurrencyCode(objArr[35].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCurrencyCode("--");
                        }

                        try {
                            if (objArr[36] == null) {
                                dataBean.setBankName("--");
                            } else {
                                dataBean.setBankName(objArr[36].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBankName("--");
                        }

                        try {
                            if (objArr[37] == null) {
                                dataBean.setBranchName("--");
                            } else {
                                dataBean.setBranchName(objArr[37].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBranchName("--");
                        }
                        try {
                            if (objArr[38] == null) {
                                dataBean.setRemarks("--");
                            } else {
                                dataBean.setRemarks(objArr[38].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRemarks("--");
                        }

                        try {
                            if (objArr[39] == null) {
                                dataBean.setAccountNo("--");
                            } else {
                                dataBean.setAccountNo(objArr[39].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAccountNo("--");
                        }

                        try {
                            if (objArr[40] == null) {
                                dataBean.setCollectFromType("--");
                            } else {
                                dataBean.setCollectFromType(objArr[40].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectFromType("--");
                        }

                        try {
                            if (objArr[41] == null) {
                                dataBean.setCollectionBranchId("--");
                            } else {
                                dataBean.setCollectionBranchId(objArr[41].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectionBranchId("--");
                        }

                        try {
                            if (objArr[42] == null) {
                                dataBean.setPreferredEmail("--");
                            } else {
                                dataBean.setPreferredEmail(objArr[42].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPreferredEmail("--");
                        }

                        try {
                            if (objArr[43] == null) {
                                dataBean.setRequestDate("--");
                            } else {
                                dataBean.setRequestDate(objArr[43].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRequestDate("--");
                        }

                        try {
                            if (objArr[44] == null) {
                                dataBean.setLastupdated("--");
                            } else {
                                dataBean.setLastupdated(objArr[44].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLastupdated("--");
                        }
                        try {
                            if (objArr[45] == null) {
                                dataBean.setLocalTime("--");
                            } else {
                                dataBean.setLocalTime(objArr[45].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLocalTime("--");
                        }
                        try {
                            if (objArr[46] == null) {
                                dataBean.setUserId("--");
                            } else {
                                dataBean.setUserId(objArr[46].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setUserId("--");
                        }
                        try {
                            if (objArr[47] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[47].toString());
                                dataBean.setChannelType(channelTypeDes);
                            } else {
                                dataBean.setChannelType("--");
                            }
                        } catch (Exception e) {
                            dataBean.setChannelType("--");
                        }
                        try {
                            dataBean.setDeviceId(objArr[48].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceId("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        
                        try {
                            dataBean.setAppId(objArr[50].toString());
                        } catch (NullPointerException e) {
                            dataBean.setAppId("--");
                        }
                        try {
                            dataBean.setBillRefNo(objArr[51].toString());
                        } catch (NullPointerException e) {
                            dataBean.setBillRefNo("--");
                        }
                        try {
//                            String txn_type="0";
//                            if(objArr[55]!=null){
//                                txn_type=objArr[55].toString();
//                            }     
//                            dataBean.setTxnMode( getTxnModeDesByCode(txn_type,objArr[52].toString()));
                            dataBean.setTxnMode(objArr[52].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTxnMode("--");
                        }
                        try {
                            dataBean.setTranRefNo(objArr[53].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTranRefNo("--");
                        }
                        try {
                            dataBean.setErrDesc(objArr[54].toString());
                        } catch (NullPointerException e) {
                            dataBean.setErrDesc("--");
                        }
                        try {
                            dataBean.setUserName(objArr[55].toString());
                        } catch (NullPointerException e) {
                            dataBean.setUserName("--");
                        }
                        try {
                            dataBean.setDeviceManufacture(objArr[56].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceManufacture("--");
                        }
                        try {
                            dataBean.setDeviceBuildNumber(objArr[57].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceBuildNumber("--");
                        }

                        dataBeanList.add(dataBean);
                    }
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }

        PartialList<CardRequestsBean> list = new PartialList<CardRequestsBean>();

        list.setList(dataBeanList);
        list.setFullCount(count);

        return list;
    }
    
    private String makeWhereClause(CardRequestsInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 0);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME >='" + datef + "'";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME <'" + datef + "'";
        }

        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += " and lower(M.NIC) LIKE lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(M.CIF) LIKE lower('%" + inputBean.getCif().trim() + "%')";
        }

        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += " and lower(U.TRANSACTION_TYPE) LIKE lower('%" + inputBean.getTxnType().trim() + "%')";
        }
//        if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty()) {
//            where += " and lower(U.RESPONCE_CODE) LIKE lower('%" + inputBean.getResponseCode().trim() + "%')";
//        }
        if (inputBean.getCustomerCategory()!= null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(M.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getChannelType()!= null && !inputBean.getChannelType().isEmpty()) {
            where += " and lower(U.CHANNEL_TYPE) LIKE lower('%" + inputBean.getChannelType().trim() + "%')";
        }
        if (inputBean.getCurrencyCode()!= null && !inputBean.getCurrencyCode().isEmpty()) {
            where += " and lower(U.CURRENCY_CODE) LIKE lower('%" + inputBean.getCurrencyCode().trim() + "%')";
        }
        if (inputBean.getTranRefNo()!= null && !inputBean.getTranRefNo().isEmpty()) {
            where += " and lower(U.TRAN_REF_NO) LIKE lower('%" + inputBean.getTranRefNo().trim() + "%')";
        }
        if (inputBean.getBillCategoryName()!= null && !inputBean.getBillCategoryName().isEmpty()) {
            where += " and lower(U.BILL_CATEGORY_NAME) LIKE lower('%" + inputBean.getBillCategoryName().trim() + "%')";
        }
        if (inputBean.getBillProviderName()!= null && !inputBean.getBillProviderName().isEmpty()) {
            where += " and lower(U.BILL_PROVIDER_NAME) LIKE lower('%" + inputBean.getBillProviderName().trim() + "%')";
        }
        if (inputBean.getStatus()!= null && !inputBean.getStatus().isEmpty()) {
            if(inputBean.getStatus().equals("SUCC")){
                where += " and U.RESPONCE_CODE ='000'";
            }else if(inputBean.getStatus().equals("FAIL")){
                where += " and (U.RESPONCE_CODE !='000' OR U.RESPONCE_CODE IS null )" ;
            }
        }

        where += " and U.TRANSACTION_TYPE IN ( '49') ";
        return where;
    }

    public SwtTxnType getTxnTypeById(String txnid) throws Exception {
        SwtTxnType ttype = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtTxnType as tt where tt.typecode=:typecode";
            Query query = session.createQuery(sql);
            query.setString("typecode", txnid);

            ttype = (SwtTxnType) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return ttype;
    }

    public StringBuffer makeCSVReport(CardRequestsInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;

        String TXN_SQL_CSV = "SELECT "
                + "U.TRANSACTION_ID, "//0
                + "T.DESCRIPTION TXN_TYPE, "//1          
                + "U.FROM_ACCOUNT_NUMBER, "//2
                + "U.TO_ACCOUNT_NUMBER, "//3
                + "U.AMOUNT, "//4
//                + "U.CUST_CIF, "//5
//                + "U.CUST_NIC, "//6
//                + "U.CUST_NAME, "//7
//                + "U.CUST_EMAIL, "//8
//                + "U.ADDRESS_1, "//9 
                + "M.CIF, "//5
                + "M.NIC, "//6
                + "M.CUSTOMER_NAME, "//7
                + "M.EMAIL, "//8
                + "M.PERMANENT_ADDRESS, "//9
                + "ST.DESCRIPTION STATUS, "//10 
                + "U.LEASE_MODEL, "//11
                + "U.LEASE_TYPE, "//12
                + "U.NTB_REQUEST_TYPE, "//13
                + "U.POSTED_METHOD, "//14
                + "U.RESPONCE_CODE, "//15
//                + "S.DESCRIPTION RESPONSE,"
                + "M.MOBILE_NUMBER, "//16
                + "U.SERVICE_FEE, "//17
                + "U.PAYEE_NAME, "//18
                + "U.BILL_CATEGORY_NAME, "//19
                + "U.BILL_PROVIDER_NAME, "//20
                + "U.BILLER_NAME, "//21
                + "U.MERCHANT_TYPE_NAME, "//22
                + "U.MERCHANT_NAME, "//23
                + "U.REDEEM_POINTS, "//24
                + "U.REDEEM_VOUCHER_ID, "//25
                + "U.LEASE_AMOUNT, "//26
                + "U.LEASE_AVD_PAYMENT, "//27
                + "U.LEASE_SELL_PRICE, "//28
                + "U.CHEQUE_NUMBER, "//29
                + "U.FROM_DATE, "//30
                + "U.TO_DATE, "//31
                + "U.CARD_NUMBER, "//32
                + "U.STATEMENT_FROM, "//33
                + "U.STATEMENT_TO, "//34
                + "U.CURRENCY_CODE, "//35
                + "U.BANK_NAME, "//36
                + "U.BRANCH_NAME, "//37
                + "U.REMARKS, "//38
                + "U.ACCOUNT_NO, "//39
                + "U.COLLECT_FROM_TYPE, "//40
                + "U.COLLECTION_BRANCH_ID, "//41
                + "U.PREFERRED_EMAIL, "//42
                + "U.REQUEST_DATE, "//43
                + "U.LASTUPDATED, "//44                
                + "U.LOCAL_TIME, "//45
                + "U.USER_ID, "//46
                + "U.CHANNEL_TYPE, "//47
                + "U.DEVICE_ID, "//48
                + "M.CUSTOMER_CATEGORY, "//49
                + "U.APP_ID, "//50
                + "U.BILL_REFFRENCE_NUMBER, "//51
                + "TM.DESCRIPTION TXN_MODE_DES, "//52
                + "U.TRAN_REF_NO, "//53
                + "U.ERR_DESC, "//54
                + "M.USERNAME, "//55
                + "D.DEVICE_MANUFACTURE, "//56
                + "D.DEVICE_BUILD_NUMBER "//57
//                + "CAST(U.TRANSACTION_TYPE AS VARCHAR2(3)) "//55
//                + "D.DEVICE_MODEL "//55
                + "FROM SWT_TRANSACTION U "
                + "LEFT OUTER JOIN SWT_RESPONSE_CODES S ON S.CODE = U.RESPONCE_CODE "
                + "LEFT OUTER JOIN SWT_TXN_TYPE T ON T.TYPECODE = U.TRANSACTION_TYPE "
                + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
                + "LEFT OUTER JOIN STATUS ST ON ST.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN TRANSACTION_MODE TM ON TM.TRAN_TYPE = U.TRANSACTION_TYPE AND TM.TRAN_MODE = U.TXN_MODE "
                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID AND D.USERID = U.USER_ID "
//                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID "
                + "WHERE ";

        try {
            session = HibernateInit.sessionFactory.openSession();
            int count = 0;
            String where1 = this.makeWhereClauseForCSV(inputBean);
            String sqlCount = this.TXN_COUNT_SQL + where1;
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = TXN_SQL_CSV + where1 + this.TRANEXPLORER_ORDER_BY_SQL;
                System.out.println(sql);

                Query query = session.createSQLQuery(sql);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    content = new StringBuffer();
                    List<CardRequestsBean> beanlist = new ArrayList<CardRequestsBean>();

                    for (Object[] objArr : objectArrList) {

                        CardRequestsBean dataBean = new CardRequestsBean();

                        //set data values to beanlist
                         try {

                            if (objArr[0] == null) {
                                dataBean.setTxnId("--");
                            } else {
                                dataBean.setTxnId(objArr[0].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnId("--");
                        }

                        try {
                            if (objArr[1] == null) {
                                dataBean.setTxnType("--");
                            } else {;
                                dataBean.setTxnType(objArr[1].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnType("--");
                        }

                        try {
                            if (objArr[2] == null) {
                                dataBean.setFromAccNo("--");
                            } else {
                                dataBean.setFromAccNo(objArr[2].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromAccNo("--");
                        }
                        try {
                            if (objArr[3] == null) {
                                dataBean.setToAccNo("--");
                            } else {
                                dataBean.setToAccNo(objArr[3].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToAccNo("--");
                        }

                        try {
                            if (objArr[4] == null) {
                                dataBean.setAmount("--");
                            } else {
                                dataBean.setAmount(objArr[4].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAmount("--");
                        }

                        try {
                            if (objArr[5] == null) {
                                dataBean.setCustCif("--");
                            } else {
                                dataBean.setCustCif(objArr[5].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustCif("--");
                        }

                        try {
                            if (objArr[6] == null) {
                                dataBean.setNic("--");
                            } else {
                                dataBean.setNic(objArr[6].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNic("--");
                        }

                        try {
                            if (objArr[7] == null) {
                                dataBean.setCustName("--");
                            } else {
                                dataBean.setCustName(objArr[7].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustName("--");
                        }

                        try {
                            if (objArr[8] == null) {
                                dataBean.setCustEmail("--");
                            } else {
                                dataBean.setCustEmail(objArr[8].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustEmail("--");
                        }

                        try {
                            if (objArr[9] == null) {
                                dataBean.setAddress1("--");
                            } else {
                                dataBean.setAddress1(objArr[9].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAddress1("--");
                        }

                        try {
                            if (objArr[10] == null) {
                                dataBean.setStatus("--");
                            } else {
                                dataBean.setStatus(objArr[10].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatus("--");
                        }

                        try {
                            if (objArr[11] == null) {
                                dataBean.setLeasingModel("--");
                            } else {
                                dataBean.setLeasingModel(objArr[11].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingModel("--");
                        }

                        try {
                            if (objArr[12] == null) {
                                dataBean.setLeasingTypes("--");
                            } else {
                                dataBean.setLeasingTypes(objArr[12].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingTypes("--");
                        }

                        try {
                            if (objArr[13] == null) {
                                dataBean.setNtbRequest("--");
                            } else {
                                dataBean.setNtbRequest(objArr[13].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNtbRequest("--");
                        }

                        try {
                            if (objArr[14] == null) {
                                dataBean.setPostedMethod("--");
                            } else {
                                dataBean.setPostedMethod(objArr[14].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPostedMethod("--");
                        }

                        try {
                            if (objArr[15] == null) {
                                dataBean.setResponseCodes("Failure");
                            } else {
                                if(objArr[15].equals("000")){
                                    dataBean.setResponseCodes("Success");
                                }else{
                                    dataBean.setResponseCodes("Failure");
                                }
//                                dataBean.setResponseCodes(objArr[15].toString());
                                //dataBean.setResponseCodes(objArr[15].getSwtResponseCodes().getDescription());        
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setResponseCodes("Failure");
                        }

                        try {
                            if (objArr[16] == null) {
                                dataBean.setMobileNumber("--");
                            } else {
                                dataBean.setMobileNumber(objArr[16].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMobileNumber("--");
                        }

                        try {
                            if (objArr[17] == null) {
                                dataBean.setServiceFee("--");
                            } else {
                                dataBean.setServiceFee(objArr[17].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setServiceFee("--");
                        }

                        try {
                            if (objArr[18] == null) {
                                dataBean.setPayeeName("--");
                            } else {
                                dataBean.setPayeeName(objArr[18].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPayeeName("--");
                        }

                        try {
                            if (objArr[19] == null) {
                                dataBean.setBillCategoryName("--");
                            } else {
                                dataBean.setBillCategoryName(objArr[19].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillCategoryName("--");
                        }

                        try {
                            if (objArr[20] == null) {
                                dataBean.setBillProviderName("--");
                            } else {
                                dataBean.setBillProviderName(objArr[20].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillProviderName("--");
                        }

                        try {
                            if (objArr[21] == null) {
                                dataBean.setBillerName("--");
                            } else {
                                dataBean.setBillerName(objArr[21].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillerName("--");
                        }
                        try {
                            if (objArr[22] == null) {
                                dataBean.setMerchantTypeName("--");
                            } else {
                                dataBean.setMerchantTypeName(objArr[22].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantTypeName("--");
                        }

                        try {
                            if (objArr[23] == null) {
                                dataBean.setMerchantName("--");
                            } else {
                                dataBean.setMerchantName(objArr[23].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantName("--");
                        }

                        try {
                            if (objArr[24] == null) {
                                dataBean.setRedeemPoints("--");
                            } else {
                                dataBean.setRedeemPoints(objArr[24].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemPoints("--");
                        }

                        try {
                            if (objArr[25] == null) {
                                dataBean.setRedeemVoucherId("--");
                            } else {
                                dataBean.setRedeemVoucherId(objArr[25].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemVoucherId("--");
                        }

                        try {
                            if (objArr[26] == null) {
                                dataBean.setLeaseAmount("--");
                            } else {
                                dataBean.setLeaseAmount(objArr[26].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAmount("--");
                        }

                        try {
                            if (objArr[27] == null) {
                                dataBean.setLeaseAvdPayment("--");
                            } else {
                                dataBean.setLeaseAvdPayment(objArr[27].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAvdPayment("--");
                        }

                        try {
                            if (objArr[28] == null) {
                                dataBean.setLeaseSellPrice("--");
                            } else {
                                dataBean.setLeaseSellPrice(objArr[28].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseSellPrice("--");
                        }

                        try {
                            if (objArr[29] == null) {
                                dataBean.setChequeNumber("--");
                            } else {
                                dataBean.setChequeNumber(objArr[29].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setChequeNumber("--");
                        }
                        try {
                            if (objArr[30] == null) {
                                dataBean.setFromDate("--");
                            } else {
                                dataBean.setFromDate(objArr[30].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromDate("--");
                        }

                        try {
                            if (objArr[31] == null) {
                                dataBean.setToDate("--");
                            } else {
                                dataBean.setToDate(objArr[31].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToDate("--");
                        }

                        try {
                            if (objArr[32] == null) {
                                dataBean.setCardNumber("--");
                            } else {
//                                dataBean.setCardNumber(toMaskCardNumber(objArr[32].toString()));
                                dataBean.setCardNumber(objArr[32].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCardNumber("--");
                        }

                        try {
                            if (objArr[33] == null) {
                                dataBean.setStatementFrom("--");
                            } else {
                                dataBean.setStatementFrom(objArr[33].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementFrom("--");
                        }

                        try {
                            if (objArr[34] == null) {
                                dataBean.setStatementTo("--");
                            } else {
                                dataBean.setStatementTo(objArr[34].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementTo("--");
                        }

                        try {
                            if (objArr[35] == null) {
                                dataBean.setCurrencyCode("--");
                            } else {
                                dataBean.setCurrencyCode(objArr[35].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCurrencyCode("--");
                        }

                        try {
                            if (objArr[36] == null) {
                                dataBean.setBankName("--");
                            } else {
                                dataBean.setBankName(objArr[36].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBankName("--");
                        }

                        try {
                            if (objArr[37] == null) {
                                dataBean.setBranchName("--");
                            } else {
                                dataBean.setBranchName(objArr[37].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBranchName("--");
                        }
                        try {
                            if (objArr[38] == null) {
                                dataBean.setRemarks("--");
                            } else {
                                dataBean.setRemarks(objArr[38].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRemarks("--");
                        }

                        try {
                            if (objArr[39] == null) {
                                dataBean.setAccountNo("--");
                            } else {
                                dataBean.setAccountNo(objArr[39].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAccountNo("--");
                        }

                        try {
                            if (objArr[40] == null) {
                                dataBean.setCollectFromType("--");
                            } else {
                                dataBean.setCollectFromType(objArr[40].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectFromType("--");
                        }

                        try {
                            if (objArr[41] == null) {
                                dataBean.setCollectionBranchId("--");
                            } else {
                                dataBean.setCollectionBranchId(objArr[41].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectionBranchId("--");
                        }

                        try {
                            if (objArr[42] == null) {
                                dataBean.setPreferredEmail("--");
                            } else {
                                dataBean.setPreferredEmail(objArr[42].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPreferredEmail("--");
                        }

                        try {
                            if (objArr[43] == null) {
                                dataBean.setRequestDate("--");
                            } else {
                                dataBean.setRequestDate(objArr[43].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRequestDate("--");
                        }

                        try {
                            if (objArr[44] == null) {
                                dataBean.setLastupdated("--");
                            } else {
                                dataBean.setLastupdated(objArr[44].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLastupdated("--");
                        }
                        try {
                            if (objArr[45] == null) {
                                dataBean.setLocalTime("--");
                            } else {
                                dataBean.setLocalTime(objArr[45].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLocalTime("--");
                        }
                        try {
                            if (objArr[46] == null) {
                                dataBean.setUserId("--");
                            } else {
                                dataBean.setUserId(objArr[46].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setUserId("--");
                        }
                        try {
                            if (objArr[47] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[47].toString());
                                dataBean.setChannelType(channelTypeDes);
                            } else {
                                dataBean.setChannelType("--");
                            }
                        } catch (Exception e) {
                            dataBean.setChannelType("--");
                        }
                        try {
                            dataBean.setDeviceId(objArr[48].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceId("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        
                        try {
                            dataBean.setAppId(objArr[50].toString());
                        } catch (NullPointerException e) {
                            dataBean.setAppId("--");
                        }
                        try {
                            dataBean.setBillRefNo(objArr[51].toString());
                        } catch (NullPointerException e) {
                            dataBean.setBillRefNo("--");
                        }
                        try {
//                            String txn_type="0";
//                            if(objArr[55]!=null){
//                                txn_type=objArr[55].toString();
//                            }   
//                            dataBean.setTxnMode( getTxnModeDesByCode(txn_type,objArr[52].toString()));
                            dataBean.setTxnMode( objArr[52].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTxnMode("--");
                        }
                        try {
                            dataBean.setTranRefNo(objArr[53].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTranRefNo("--");
                        }
                        try {
                            dataBean.setErrDesc(objArr[54].toString());
                        } catch (NullPointerException e) {
                            dataBean.setErrDesc("--");
                        }
                        try {
                            dataBean.setUserName(objArr[55].toString());
                        } catch (NullPointerException e) {
                            dataBean.setUserName("--");
                        }
                        try {
                            dataBean.setDeviceManufacture(objArr[56].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceManufacture("--");
                        }
                        try {
                            dataBean.setDeviceBuildNumber(objArr[57].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceBuildNumber("--");
                        }

                        beanlist.add(dataBean);
                    }

                    //write column headers to csv file
                    content.append("Customer CID");
                    content.append(',');
                    content.append("User Name");
                    content.append(',');
                    content.append("Customer Name");
                    content.append(',');
                    content.append("Mobile Number");
                    content.append(',');
                    content.append("Transaction Mode");
                    content.append(',');
                    content.append("Card Number");
                    content.append(',');
                    content.append("Channel Type");
                    content.append(',');
                    content.append("Status");
                    content.append(',');
                    content.append("Error Description");
                    content.append(',');
                    content.append("Transaction Time");
                    content.append('\n');

                    //write data values to csv file
                    for (CardRequestsBean dataBean : beanlist) {
                        try {
                            if (dataBean.getCustCif() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustCif());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getUserName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getUserName()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getMobileNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMobileNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getTxnMode()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getTxnMode());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCardNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCardNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getChannelType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getChannelType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getResponseCodes() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getResponseCodes());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getErrDesc()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getErrDesc());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {//22
                            if (dataBean.getLocalTime() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLocalTime());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//
//                        try {
//                            if (dataBean.getFromAccNo() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getFromAccNo());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getToAccNo() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getToAccNo());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getCurrencyCode() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCurrencyCode());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getAmount() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getAmount());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        
//                        try {
//                            if (dataBean.getUserId() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getUserId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getCustomerCategory()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCustomerCategory());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getNic() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getNic());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getUserName()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getUserName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getCustEmail() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCustEmail());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getAddress1() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getAddress1());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getAppId()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getAppId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getDeviceManufacture()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getDeviceManufacture());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getDeviceBuildNumber()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getDeviceBuildNumber());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getDeviceId() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getDeviceId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getBillRefNo()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getBillRefNo());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getBillCategoryName() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getBillCategoryName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getBillProviderName() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getBillProviderName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {//22
//                            if (dataBean.getBillerName() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getBillerName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getBankName() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getBankName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getBranchName() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getBranchName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getRemarks() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getRemarks());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getPayeeName() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getPayeeName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getServiceFee() == null) {
//                                content.append("--");
////                                content.append(',');
//                            } else {
//                                content.append(dataBean.getServiceFee());
////                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
////                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getLeasingModel() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeasingModel());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getLeasingTypes() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeasingTypes());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getNtbRequest() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getNtbRequest());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getPostedMethod() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getPostedMethod());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getMerchantTypeName() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getMerchantTypeName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getRedeemPoints() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getRedeemPoints());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getRedeemVoucherId() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getRedeemVoucherId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getLeaseAmount() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeaseAmount());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getLeaseAvdPayment() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeaseAvdPayment());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//
//                        try {
//                            if (dataBean.getLeaseSellPrice() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getLeaseSellPrice());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getChequeNumber() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getChequeNumber());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getFromDate() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getFromDate());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getToDate() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getToDate());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getStatementFrom() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getStatementFrom());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getStatementTo() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getStatementTo());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getAccountNo() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getAccountNo());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getCollectFromType() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCollectFromType());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getCollectionBranchId() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getCollectionBranchId());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getPreferredEmail() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getPreferredEmail());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {
//                            if (dataBean.getRequestDate() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getRequestDate());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
//                        try {//22
//                            if (dataBean.getLastupdated() == null) {
//                                content.append("--");
//                            } else {
//                                content.append(dataBean.getLastupdated());
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                        }

                        content.append('\n');
                    }
                    content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
                    content.append('\n');
                    
//                    content.append("Transaction Type :");
//                    if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty() && inputBean.getTxnType() != "-ALL-") {
//                        String description = getTransactionTypeDescription(inputBean, session);
//                        if (description != null) {
//                            inputBean.setResponseDescription(description);
//                            content.append(description);
//                        }
//                    } else {
//                        inputBean.setResponseDescription("-ALL-");
//                        content.append("-ALL-");
//                    }
//                    content.append('\n');
//
//                    content.append("Transaction Reference Number :");
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getTranRefNo()));
//                    content.append('\n');
//                    
                    content.append("Channel Type :");
                    String channelTypeCode =Common.replaceEmptyorNullStringToALL(inputBean.getChannelType());
                    content.append(this.getChannelTypeDesByCode(channelTypeCode));
                    content.append('\n');
                    
                    content.append("Status :");
                    if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty() ) {
                        String description = getStatusDescription(inputBean.getStatus());
                        if (description != null) {
                            content.append(description);
                        }else{
                            content.append("-ALL-");
                        }
                    } else {
                        content.append("-ALL-");
                    }
                    content.append('\n');
                    
//                    content.append("Status :");
//                    if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty() ) {
//                        try{
//                        Status status =(Status)session.get(Status.class, inputBean.getStatus());
//                        if (status != null) {
//                            content.append(status.getDescription());
//                        }else{
//                            content.append("-ALL-");
//                        }
//                        }catch (Exception e){
//                             content.append("-ALL-");
//                        }
//                    } else {
//                        content.append("-ALL-");
//                    }
//                    content.append('\n');
//                    
//                    content.append("Response :");
//                    if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty() && inputBean.getResponseCode() != "-ALL-") {
//                        String description = getResponeseDescription(inputBean, session);
//                        if (description != null) {
//                            inputBean.setResponseDescription(description);
//                            content.append(description);
//                        }
//                    } else {
//                        inputBean.setResponseDescription("-ALL-");
//                        content.append("-ALL-");
//                    }
//                    content.append('\n');
                    
//                    content.append("Currency Code :");
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCurrencyCode()));
//                    content.append('\n');
//                    
//                    content.append("Customer NIC :");
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getNic()));
//                    content.append('\n');
//                    
//                    content.append("Customer Category :");
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategory()));
//                    content.append('\n');

                    content.append("Customer CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
                    content.append('\n');
//
//                    content.append("Bill Category Name :");
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getBillCategoryName()));
//                    content.append('\n');
//
//                    content.append("Bill Provider Name :");
//                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getBillProviderName()));
//                    content.append('\n');


                }

            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return content;
    }

    private String getResponeseDescription(CardRequestsInputBean inputBean, Session session) throws ParseException {

        String rDescription = null;
        try {

            String sql = "select description FROM SwtResponseCodes as tt WHERE tt.code=:responsecode";
            Query query = session.createQuery(sql);
            query.setString("responsecode", inputBean.getResponseCode());

            rDescription = (String) query.list().get(0);

        } catch (Exception e) {
            throw e;
        }
        return rDescription;
    }

    private String getTransactionTypeDescription(CardRequestsInputBean inputBean, Session session) throws ParseException {

        String ttype = null;
        try {

            String sql = "select description FROM SwtTxnType as tt WHERE tt.typecode=:txnType";
            Query query = session.createQuery(sql);
            query.setString("txnType", inputBean.getTxnType());

            ttype = (String) query.list().get(0);

        } catch (Exception e) {
            throw e;
        }
        return ttype;
    }

    private String makeWhereClauseForCSV(CardRequestsInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 0);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME >='" + datef + "'";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME <'" + datef + "'";
        }

        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += " and lower(M.NIC) LIKE lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(M.CIF) LIKE lower('%" + inputBean.getCif().trim() + "%')";
        }

        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += " and lower(U.TRANSACTION_TYPE) LIKE lower('%" + inputBean.getTxnType().trim() + "%')";
        }
//        if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty()) {
//            where += " and lower(U.RESPONCE_CODE) LIKE lower('%" + inputBean.getResponseCode().trim() + "%')";
//        }
        if (inputBean.getCustomerCategory()!= null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(M.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getChannelType()!= null && !inputBean.getChannelType().isEmpty()) {
            where += " and lower(U.CHANNEL_TYPE) LIKE lower('%" + inputBean.getChannelType().trim() + "%')";
        }
        if (inputBean.getCurrencyCode()!= null && !inputBean.getCurrencyCode().isEmpty()) {
            where += " and lower(U.CURRENCY_CODE) LIKE lower('%" + inputBean.getCurrencyCode().trim() + "%')";
        }
        if (inputBean.getTranRefNo()!= null && !inputBean.getTranRefNo().isEmpty()) {
            where += " and lower(U.TRAN_REF_NO) LIKE lower('%" + inputBean.getTranRefNo().trim() + "%')";
        }
        if (inputBean.getBillCategoryName()!= null && !inputBean.getBillCategoryName().isEmpty()) {
            where += " and lower(U.BILL_CATEGORY_NAME) LIKE lower('%" + inputBean.getBillCategoryName().trim() + "%')";
        }
        if (inputBean.getBillProviderName()!= null && !inputBean.getBillProviderName().isEmpty()) {
            where += " and lower(U.BILL_PROVIDER_NAME) LIKE lower('%" + inputBean.getBillProviderName().trim() + "%')";
        }
//        if (inputBean.getStatus()!= null && !inputBean.getStatus().isEmpty()) {
//            where += " and U.STATUS ='" + inputBean.getStatus().trim() + "'";
//        }
         if (inputBean.getStatus()!= null && !inputBean.getStatus().isEmpty()) {
            if(inputBean.getStatus().equals("SUCC")){
                where += " and U.RESPONCE_CODE ='000'";
            }else if(inputBean.getStatus().equals("FAIL")){
                where += " and (U.RESPONCE_CODE !='000' OR U.RESPONCE_CODE IS null )";
            }
        }

        where += " and U.TRANSACTION_TYPE IN ( '49') ";
        return where;
    }

    public Object generateExcelReport(CardRequestsInputBean inputBean) throws Exception {
        Session session = null;
        Object returnObject = null;
        String CUSTOMER_SQL_SEARCH = "SELECT "
                + "U.TRANSACTION_ID, "//0
                + "T.DESCRIPTION TXN_TYPE, "//1          
                + "U.FROM_ACCOUNT_NUMBER, "//2
                + "U.TO_ACCOUNT_NUMBER, "//3
                + "U.AMOUNT, "//4
//                + "U.CUST_CIF, "//5
//                + "U.CUST_NIC, "//6
//                + "U.CUST_NAME, "//7
//                + "U.CUST_EMAIL, "//8
//                + "U.ADDRESS_1, "//9 
                + "M.CIF, "//5
                + "M.NIC, "//6
                + "M.CUSTOMER_NAME, "//7
                + "M.EMAIL, "//8
                + "M.PERMANENT_ADDRESS, "//9 
                + "ST.DESCRIPTION STATUS, "//10 
                + "U.LEASE_MODEL, "//11
                + "U.LEASE_TYPE, "//12
                + "U.NTB_REQUEST_TYPE, "//13
                + "U.POSTED_METHOD, "//14
                + "U.RESPONCE_CODE, "//15
//                + "S.DESCRIPTION RESPONSE,"
                + "M.MOBILE_NUMBER, "//16
                + "U.SERVICE_FEE, "//17
                + "U.PAYEE_NAME, "//18
                + "U.BILL_CATEGORY_NAME, "//19
                + "U.BILL_PROVIDER_NAME, "//20
                + "U.BILLER_NAME, "//21
                + "U.MERCHANT_TYPE_NAME, "//22
                + "U.MERCHANT_NAME, "//23
                + "U.REDEEM_POINTS, "//24
                + "U.REDEEM_VOUCHER_ID, "//25
                + "U.LEASE_AMOUNT, "//26
                + "U.LEASE_AVD_PAYMENT, "//27
                + "U.LEASE_SELL_PRICE, "//28
                + "U.CHEQUE_NUMBER, "//29
                + "U.FROM_DATE, "//30
                + "U.TO_DATE, "//31
                + "U.CARD_NUMBER, "//32
                + "U.STATEMENT_FROM, "//33
                + "U.STATEMENT_TO, "//34
                + "U.CURRENCY_CODE, "//35
                + "U.BANK_NAME, "//36
                + "U.BRANCH_NAME, "//37
                + "U.REMARKS, "//38
                + "U.ACCOUNT_NO, "//39
                + "U.COLLECT_FROM_TYPE, "//40
                + "U.COLLECTION_BRANCH_ID, "//41
                + "U.PREFERRED_EMAIL, "//42
                + "U.REQUEST_DATE, "//43
                + "U.LASTUPDATED, "//44                
                + "U.LOCAL_TIME, "//45
                + "U.USER_ID, "//46
                + "U.CHANNEL_TYPE, "//47
                + "U.DEVICE_ID, "//48
                + "M.CUSTOMER_CATEGORY, "//49
                + "U.APP_ID, "//50
                + "U.BILL_REFFRENCE_NUMBER, "//51
                + "TM.DESCRIPTION TXN_MODE_DES, "//52
                + "U.TRAN_REF_NO, "//53
                + "U.ERR_DESC, "//54
                + "M.USERNAME, "//55
                + "D.DEVICE_MANUFACTURE, "//56
                + "D.DEVICE_BUILD_NUMBER "//57
//                + "CAST(U.TRANSACTION_TYPE AS VARCHAR2(3)) "//55
//                + "D.DEVICE_MODEL "//55
                + "FROM SWT_TRANSACTION U "
                + "LEFT OUTER JOIN SWT_RESPONSE_CODES S ON S.CODE = U.RESPONCE_CODE "
                + "LEFT OUTER JOIN SWT_TXN_TYPE T ON T.TYPECODE = U.TRANSACTION_TYPE "
                + "LEFT OUTER JOIN SWT_MOBILE_USER M ON M.ID = U.USER_ID "
                + "LEFT OUTER JOIN STATUS ST ON ST.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN TRANSACTION_MODE TM ON TM.TRAN_TYPE = U.TRANSACTION_TYPE AND TM.TRAN_MODE = U.TXN_MODE "
                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID AND D.USERID = U.USER_ID "
//                + "LEFT OUTER JOIN SWT_USER_DEVICE D ON D.PUSH_SHA = U.DEVICE_ID "
                + "WHERE ";

        try {

            String directory = ServletActionContext.getServletContext().getInitParameter("tmpreportpath");
            File file = new File(directory);
            if (file.exists()) {
                FileUtils.deleteDirectory(file);
            }

            session = HibernateInit.sessionFactory.openSession();

            int count = 0;
            String where1 = this.makeWhereClauseForExcel(inputBean);
            String sqlCount = this.TXN_COUNT_SQL + where1;
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);
//            queryCount = setDatesToQuery(queryCount, inputBean, session);

//            queryCount = setDatesToQuery(queryCount, inputBean, session);
            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }
//                System.err.println("Count is "+count);
            if (count > 0) {

                long maxRow = Long.parseLong(ServletActionContext.getServletContext().getInitParameter("numberofrowsperexcel"));
                SXSSFWorkbook workbook = this.createExcelTopSection(inputBean, session);
                Sheet sheet = workbook.getSheetAt(0);

                int currRow = headerRowCount;
                int fileCount = 0;

                currRow = this.createExcelTableHeaderSection(workbook, currRow);

                String sql = CUSTOMER_SQL_SEARCH + where1 + this.TRANEXPLORER_ORDER_BY_SQL;
                System.out.println(sql);
                int selectRow = Integer.parseInt(ServletActionContext.getServletContext().getInitParameter("numberofselectrows"));
                int numberOfTimes = count / selectRow;
                if ((count % selectRow) > 0) {
                    numberOfTimes += 1;
                }
                int from = 0;
                int listrownumber = 1;

                for (int i = 0; i < numberOfTimes; i++) {

                    Query query = session.createSQLQuery(sql);
                    query.setFirstResult(from);
                    query.setMaxResults(selectRow);

                    List<Object[]> objectArrList = (List<Object[]>) query.list();
                    if (objectArrList.size() > 0) {

                        for (Object[] objArr : objectArrList) {
                            CardRequestsBean dataBean = new CardRequestsBean();

                             try {

                            if (objArr[0] == null) {
                                dataBean.setTxnId("--");
                            } else {
                                dataBean.setTxnId(objArr[0].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnId("--");
                        }

                        try {
                            if (objArr[1] == null) {
                                dataBean.setTxnType("--");
                            } else {;
                                dataBean.setTxnType(objArr[1].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setTxnType("--");
                        }

                        try {
                            if (objArr[2] == null) {
                                dataBean.setFromAccNo("--");
                            } else {
                                dataBean.setFromAccNo(objArr[2].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromAccNo("--");
                        }
                        try {
                            if (objArr[3] == null) {
                                dataBean.setToAccNo("--");
                            } else {
                                dataBean.setToAccNo(objArr[3].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToAccNo("--");
                        }

                        try {
                            if (objArr[4] == null) {
                                dataBean.setAmount("--");
                            } else {
                                dataBean.setAmount(objArr[4].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAmount("--");
                        }

                        try {
                            if (objArr[5] == null) {
                                dataBean.setCustCif("--");
                            } else {
                                dataBean.setCustCif(objArr[5].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustCif("--");
                        }

                        try {
                            if (objArr[6] == null) {
                                dataBean.setNic("--");
                            } else {
                                dataBean.setNic(objArr[6].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNic("--");
                        }

                        try {
                            if (objArr[7] == null) {
                                dataBean.setCustName("--");
                            } else {
                                dataBean.setCustName(objArr[7].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustName("--");
                        }

                        try {
                            if (objArr[8] == null) {
                                dataBean.setCustEmail("--");
                            } else {
                                dataBean.setCustEmail(objArr[8].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCustEmail("--");
                        }

                        try {
                            if (objArr[9] == null) {
                                dataBean.setAddress1("--");
                            } else {
                                dataBean.setAddress1(objArr[9].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAddress1("--");
                        }

                        try {
                            if (objArr[10] == null) {
                                dataBean.setStatus("--");
                            } else {
                                dataBean.setStatus(objArr[10].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatus("--");
                        }

                        try {
                            if (objArr[11] == null) {
                                dataBean.setLeasingModel("--");
                            } else {
                                dataBean.setLeasingModel(objArr[11].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingModel("--");
                        }

                        try {
                            if (objArr[12] == null) {
                                dataBean.setLeasingTypes("--");
                            } else {
                                dataBean.setLeasingTypes(objArr[12].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeasingTypes("--");
                        }

                        try {
                            if (objArr[13] == null) {
                                dataBean.setNtbRequest("--");
                            } else {
                                dataBean.setNtbRequest(objArr[13].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setNtbRequest("--");
                        }

                        try {
                            if (objArr[14] == null) {
                                dataBean.setPostedMethod("--");
                            } else {
                                dataBean.setPostedMethod(objArr[14].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPostedMethod("--");
                        }

                        try {
                            if (objArr[15] == null) {
                                dataBean.setResponseCodes("Failure");
                            } else {
                                if(objArr[15].equals("000")){
                                    dataBean.setResponseCodes("Success");
                                }else{
                                    dataBean.setResponseCodes("Failure");
                                }
//                                dataBean.setResponseCodes(objArr[15].toString());
                                //dataBean.setResponseCodes(objArr[15].getSwtResponseCodes().getDescription());        
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setResponseCodes("Failure");
                        }
                        try {
                            if (objArr[16] == null) {
                                dataBean.setMobileNumber("--");
                            } else {
                                dataBean.setMobileNumber(objArr[16].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMobileNumber("--");
                        }

                        try {
                            if (objArr[17] == null) {
                                dataBean.setServiceFee("--");
                            } else {
                                dataBean.setServiceFee(objArr[17].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setServiceFee("--");
                        }

                        try {
                            if (objArr[18] == null) {
                                dataBean.setPayeeName("--");
                            } else {
                                dataBean.setPayeeName(objArr[18].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPayeeName("--");
                        }

                        try {
                            if (objArr[19] == null) {
                                dataBean.setBillCategoryName("--");
                            } else {
                                dataBean.setBillCategoryName(objArr[19].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillCategoryName("--");
                        }

                        try {
                            if (objArr[20] == null) {
                                dataBean.setBillProviderName("--");
                            } else {
                                dataBean.setBillProviderName(objArr[20].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillProviderName("--");
                        }

                        try {
                            if (objArr[21] == null) {
                                dataBean.setBillerName("--");
                            } else {
                                dataBean.setBillerName(objArr[21].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBillerName("--");
                        }
                        try {
                            if (objArr[22] == null) {
                                dataBean.setMerchantTypeName("--");
                            } else {
                                dataBean.setMerchantTypeName(objArr[22].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantTypeName("--");
                        }

                        try {
                            if (objArr[23] == null) {
                                dataBean.setMerchantName("--");
                            } else {
                                dataBean.setMerchantName(objArr[23].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setMerchantName("--");
                        }

                        try {
                            if (objArr[24] == null) {
                                dataBean.setRedeemPoints("--");
                            } else {
                                dataBean.setRedeemPoints(objArr[24].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemPoints("--");
                        }

                        try {
                            if (objArr[25] == null) {
                                dataBean.setRedeemVoucherId("--");
                            } else {
                                dataBean.setRedeemVoucherId(objArr[25].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRedeemVoucherId("--");
                        }

                        try {
                            if (objArr[26] == null) {
                                dataBean.setLeaseAmount("--");
                            } else {
                                dataBean.setLeaseAmount(objArr[26].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAmount("--");
                        }

                        try {
                            if (objArr[27] == null) {
                                dataBean.setLeaseAvdPayment("--");
                            } else {
                                dataBean.setLeaseAvdPayment(objArr[27].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseAvdPayment("--");
                        }

                        try {
                            if (objArr[28] == null) {
                                dataBean.setLeaseSellPrice("--");
                            } else {
                                dataBean.setLeaseSellPrice(objArr[28].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLeaseSellPrice("--");
                        }

                        try {
                            if (objArr[29] == null) {
                                dataBean.setChequeNumber("--");
                            } else {
                                dataBean.setChequeNumber(objArr[29].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setChequeNumber("--");
                        }
                        try {
                            if (objArr[30] == null) {
                                dataBean.setFromDate("--");
                            } else {
                                dataBean.setFromDate(objArr[30].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setFromDate("--");
                        }

                        try {
                            if (objArr[31] == null) {
                                dataBean.setToDate("--");
                            } else {
                                dataBean.setToDate(objArr[31].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setToDate("--");
                        }

                        try {
                            if (objArr[32] == null) {
                                dataBean.setCardNumber("--");
                            } else {
//                                dataBean.setCardNumber(toMaskCardNumber(objArr[32].toString()));
                                dataBean.setCardNumber(objArr[32].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCardNumber("--");
                        }

                        try {
                            if (objArr[33] == null) {
                                dataBean.setStatementFrom("--");
                            } else {
                                dataBean.setStatementFrom(objArr[33].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementFrom("--");
                        }

                        try {
                            if (objArr[34] == null) {
                                dataBean.setStatementTo("--");
                            } else {
                                dataBean.setStatementTo(objArr[34].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setStatementTo("--");
                        }

                        try {
                            if (objArr[35] == null) {
                                dataBean.setCurrencyCode("--");
                            } else {
                                dataBean.setCurrencyCode(objArr[35].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCurrencyCode("--");
                        }

                        try {
                            if (objArr[36] == null) {
                                dataBean.setBankName("--");
                            } else {
                                dataBean.setBankName(objArr[36].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBankName("--");
                        }

                        try {
                            if (objArr[37] == null) {
                                dataBean.setBranchName("--");
                            } else {
                                dataBean.setBranchName(objArr[37].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setBranchName("--");
                        }
                        try {
                            if (objArr[38] == null) {
                                dataBean.setRemarks("--");
                            } else {
                                dataBean.setRemarks(objArr[38].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRemarks("--");
                        }

                        try {
                            if (objArr[39] == null) {
                                dataBean.setAccountNo("--");
                            } else {
                                dataBean.setAccountNo(objArr[39].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setAccountNo("--");
                        }

                        try {
                            if (objArr[40] == null) {
                                dataBean.setCollectFromType("--");
                            } else {
                                dataBean.setCollectFromType(objArr[40].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectFromType("--");
                        }

                        try {
                            if (objArr[41] == null) {
                                dataBean.setCollectionBranchId("--");
                            } else {
                                dataBean.setCollectionBranchId(objArr[41].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setCollectionBranchId("--");
                        }

                        try {
                            if (objArr[42] == null) {
                                dataBean.setPreferredEmail("--");
                            } else {
                                dataBean.setPreferredEmail(objArr[42].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setPreferredEmail("--");
                        }

                        try {
                            if (objArr[43] == null) {
                                dataBean.setRequestDate("--");
                            } else {
                                dataBean.setRequestDate(objArr[43].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setRequestDate("--");
                        }

                        try {
                            if (objArr[44] == null) {
                                dataBean.setLastupdated("--");
                            } else {
                                dataBean.setLastupdated(objArr[44].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLastupdated("--");
                        }
                        try {
                            if (objArr[45] == null) {
                                dataBean.setLocalTime("--");
                            } else {
                                dataBean.setLocalTime(objArr[45].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setLocalTime("--");
                        }
                        try {
                            if (objArr[46] == null) {
                                dataBean.setUserId("--");
                            } else {
                                dataBean.setUserId(objArr[46].toString());
                            }
                        } catch (NullPointerException npe) {
                            dataBean.setUserId("--");
                        }
                        try {
                            if (objArr[47] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[47].toString());
                                dataBean.setChannelType(channelTypeDes);
                            } else {
                                dataBean.setChannelType("--");
                            }
                        } catch (Exception e) {
                            dataBean.setChannelType("--");
                        }
                        try {
                            dataBean.setDeviceId(objArr[48].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceId("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        try {
                            dataBean.setCustomerCategory(objArr[49].toString());
                        } catch (NullPointerException e) {
                            dataBean.setCustomerCategory("--");
                        }
                        
                        try {
                            dataBean.setAppId(objArr[50].toString());
                        } catch (NullPointerException e) {
                            dataBean.setAppId("--");
                        }
                        try {
                            dataBean.setBillRefNo(objArr[51].toString());
                        } catch (NullPointerException e) {
                            dataBean.setBillRefNo("--");
                        }
                        try {
//                            String txn_type="0";
//                            if(objArr[55]!=null){
//                                txn_type=objArr[55].toString();
//                            } 
//                            dataBean.setTxnMode( getTxnModeDesByCode(txn_type,objArr[52].toString()));
                            dataBean.setTxnMode( objArr[52].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTxnMode("--");
                        }
                        try {
                            dataBean.setTranRefNo(objArr[53].toString());
                        } catch (NullPointerException e) {
                            dataBean.setTranRefNo("--");
                        }
                        try {
                            dataBean.setErrDesc(objArr[54].toString());
                        } catch (NullPointerException e) {
                            dataBean.setErrDesc("--");
                        }
                        try {
                            dataBean.setUserName(objArr[55].toString());
                        } catch (NullPointerException e) {
                            dataBean.setUserName("--");
                        }
                        try {
                            dataBean.setDeviceManufacture(objArr[56].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceManufacture("--");
                        }
                        try {
                            dataBean.setDeviceBuildNumber(objArr[57].toString());
                        } catch (NullPointerException e) {
                            dataBean.setDeviceBuildNumber("--");
                        }


                            dataBean.setFullCount(count);

                            if (currRow + 1 > maxRow) {
                                fileCount++;
                                this.writeTemporaryFile(workbook, fileCount, directory);
                                workbook = this.createExcelTopSection(inputBean, session);
                                sheet = workbook.getSheetAt(0);
                                currRow = headerRowCount;
                                this.createExcelTableHeaderSection(workbook, currRow);
                            }
                            currRow = this.createExcelTableBodySection(workbook, dataBean, currRow, listrownumber);
                            listrownumber++;
                            if (currRow % 100 == 0) {
                                ((SXSSFSheet) sheet).flushRows(100); // retain 100 last rows and flush all others

                                // ((SXSSFSheet)sh).flushRows() is a shortcut for ((SXSSFSheet)sh).flushRows(0),
                                // this method flushes all rows
                            }
                        }
                    }
                    from = from + selectRow;
                }

                Date createdTime = CommonDAO.getSystemDate(session);
                this.createExcelBotomSection(workbook, currRow, count, createdTime);
                System.out.println("--------" + currRow);
                if (fileCount > 0) {
                    fileCount++;
                    this.writeTemporaryFile(workbook, fileCount, directory);
                    ByteArrayOutputStream outputStream = Common.zipFiles(file.listFiles());
                    returnObject = outputStream;
                    workbook.dispose();
                } else {
//                    for (int i = 0; i < columnCount; i++) {
//                        //to auto size all column in the sheet
//                        sheet.autoSizeColumn(i);
//                    }

                    returnObject = workbook;
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return returnObject;
    }

    private String makeWhereClauseForExcel(CardRequestsInputBean inputBean) throws ParseException {
        String where = "1=1";

         if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 0);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME >='" + datef + "'";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            sdff.applyPattern("dd-MMM-yy");
            datef = sdff.format(cf.getTime());
            where += " and U.LOCAL_TIME <'" + datef + "'";
        }

        if (inputBean.getNic() != null && !inputBean.getNic().isEmpty()) {
            where += " and lower(M.NIC) LIKE lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(M.CIF) LIKE lower('%" + inputBean.getCif().trim() + "%')";
        }

        if (inputBean.getTxnType() != null && !inputBean.getTxnType().isEmpty()) {
            where += " and lower(U.TRANSACTION_TYPE) LIKE lower('%" + inputBean.getTxnType().trim() + "%')";
        }
//        if (inputBean.getResponseCode() != null && !inputBean.getResponseCode().isEmpty()) {
//            where += " and lower(U.RESPONCE_CODE) LIKE lower('%" + inputBean.getResponseCode().trim() + "%')";
//        }
        if (inputBean.getCustomerCategory()!= null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(M.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getChannelType()!= null && !inputBean.getChannelType().isEmpty()) {
            where += " and lower(U.CHANNEL_TYPE) LIKE lower('%" + inputBean.getChannelType().trim() + "%')";
        }
        if (inputBean.getCurrencyCode()!= null && !inputBean.getCurrencyCode().isEmpty()) {
            where += " and lower(U.CURRENCY_CODE) LIKE lower('%" + inputBean.getCurrencyCode().trim() + "%')";
        }
        if (inputBean.getTranRefNo()!= null && !inputBean.getTranRefNo().isEmpty()) {
            where += " and lower(U.TRAN_REF_NO) LIKE lower('%" + inputBean.getTranRefNo().trim() + "%')";
        }
        if (inputBean.getBillCategoryName()!= null && !inputBean.getBillCategoryName().isEmpty()) {
            where += " and lower(U.BILL_CATEGORY_NAME) LIKE lower('%" + inputBean.getBillCategoryName().trim() + "%')";
        }
        if (inputBean.getBillProviderName()!= null && !inputBean.getBillProviderName().isEmpty()) {
            where += " and lower(U.BILL_PROVIDER_NAME) LIKE lower('%" + inputBean.getBillProviderName().trim() + "%')";
        }
//        if (inputBean.getStatus()!= null && !inputBean.getStatus().isEmpty()) {
//            where += " and U.STATUS ='" + inputBean.getStatus().trim() + "'";
//        }
        if (inputBean.getStatus()!= null && !inputBean.getStatus().isEmpty()) {
            if(inputBean.getStatus().equals("SUCC")){
                where += " and U.RESPONCE_CODE ='000'";
            }else if(inputBean.getStatus().equals("FAIL")){
                where += " and (U.RESPONCE_CODE !='000' OR U.RESPONCE_CODE IS null )";
            }
        }

        where += " and U.TRANSACTION_TYPE IN ( '49') ";
        return where;
    }

    private SXSSFWorkbook createExcelTopSection(CardRequestsInputBean inputBean, Session session) throws Exception {

        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
        Sheet sheet = workbook.createSheet("Card_Requests_Report");

        CellStyle fontBoldedUnderlinedCell = ExcelCommon.getFontBoldedUnderlinedCell(workbook);

        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue("NDB MB Solution");
        cell.setCellStyle(fontBoldedUnderlinedCell);

        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Card Requests Report");
        cell.setCellStyle(fontBoldedUnderlinedCell);

        row = sheet.createRow(4);
        cell = row.createCell(0);
        cell.setCellValue("From Date");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(5);
        cell = row.createCell(0);
        cell.setCellValue("To Date");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(6);
        cell = row.createCell(0);
        cell.setCellValue("Channel Type");
        cell = row.createCell(1);
        String channelTypeCode =Common.replaceEmptyorNullStringToALL(inputBean.getChannelType());
        cell.setCellValue(this.getChannelTypeDesByCode(channelTypeCode));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));
      
        row = sheet.createRow(7);
        cell = row.createCell(0);
        cell.setCellValue("Status");
        cell = row.createCell(1);
        String description =this.getStatusDescription(inputBean.getStatus());
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(description));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));
     
        row = sheet.createRow(8);
        cell = row.createCell(0);
        cell.setCellValue("CID");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        return workbook;
    }

    private int createExcelTableHeaderSection(SXSSFWorkbook workbook, int currrow) throws Exception {
        CellStyle columnHeaderCell = ExcelCommon.getColumnHeadeCell(workbook);
        Sheet sheet = workbook.getSheetAt(0);

        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue("No");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(1);
        cell.setCellValue("CID");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(2);
        cell.setCellValue("Customer Name");
        cell.setCellStyle(columnHeaderCell);
        
        cell = row.createCell(3);
        cell.setCellValue("Mobile Number");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(4);
        cell.setCellValue("Transaction Mode");
        cell.setCellStyle(columnHeaderCell);
        
        cell = row.createCell(5);
        cell.setCellValue("Card Number");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(6);
        cell.setCellValue("Channel Type");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(7);
        cell.setCellValue("Status");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(8);
        cell.setCellValue("Error Description");
        cell.setCellStyle(columnHeaderCell);
        
        cell = row.createCell(9);
        cell.setCellValue("Transaction Time");
        cell.setCellStyle(columnHeaderCell);
        
        return currrow;
    }

    private void writeTemporaryFile(SXSSFWorkbook workbook, int fileCount, String directory) throws Exception {
        File file;
        FileOutputStream outputStream = null;
        try {
            Sheet sheet = workbook.getSheetAt(0);
//            for (int i = 0; i < columnCount; i++) {
//                //to auto size all column in the sheet
////                sheet.autoSizeColumn(i);
//            }

            file = new File(directory);
            if (!file.exists()) {
                System.out.println("Directory created or not : " + file.mkdirs());
            }

            if (fileCount > 0) {
                file = new File(directory + File.separator + "Card Requests Report_" + fileCount + ".xlsx");
            } else {
                file = new File(directory + File.separator + "Card Requests Report.xlsx");
            }
            outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
        } catch (IOException e) {
            throw e;
        } finally {
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
        }
    }

    private int createExcelTableBodySection(SXSSFWorkbook workbook, CardRequestsBean dataBean, int currrow, int rownumber) throws Exception {
        Sheet sheet = workbook.getSheetAt(0);
        CellStyle rowColumnCell = ExcelCommon.getRowColumnCell(workbook);
        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue(rownumber);
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(1);
        cell.setCellValue(dataBean.getCustCif());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(2);
        cell.setCellValue(dataBean.getCustName());
        cell.setCellStyle(rowColumnCell);
        
        cell = row.createCell(3);
        cell.setCellValue(dataBean.getMobileNumber());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(4);
        cell.setCellValue(dataBean.getTxnMode());
        cell.setCellStyle(rowColumnCell);
        
        cell = row.createCell(5);
        cell.setCellValue(dataBean.getCardNumber());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(6);
        cell.setCellValue(dataBean.getChannelType());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(7);
        cell.setCellValue(dataBean.getResponseCodes());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(8);
        cell.setCellValue(dataBean.getErrDesc());
        cell.setCellStyle(rowColumnCell);
        
        cell = row.createCell(9);
        cell.setCellValue(dataBean.getLocalTime());
        cell.setCellStyle(rowColumnCell);
        
        return currrow;
    }

    private void createExcelBotomSection(SXSSFWorkbook workbook, int currrow, long count, Date date) throws Exception {

        CellStyle fontBoldedCell = ExcelCommon.getFontBoldedCell(workbook);
        Sheet sheet = workbook.getSheetAt(0);

        currrow++;
        Row row = sheet.createRow(currrow++);
        Cell cell = row.createCell(0);
        cell.setCellValue("Summary");
        cell.setCellStyle(fontBoldedCell);

        row = sheet.createRow(currrow++);
        cell = row.createCell(0);
        cell.setCellValue("Total Record Count");
        cell = row.createCell(1);
        cell.setCellValue(count);
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(currrow++);
        cell = row.createCell(0);
        cell.setCellValue("Report Created Time and Date");
        cell = row.createCell(1);
        cell.setCellValue(date.toString().substring(0, 19));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));
    }

    private HashMap<String, String> getChannelTypeMap() {

        HashMap<String, String> channelTypeMap = new HashMap<String, String>();
        
        channelTypeMap.put("2", "Internet Banking");
        channelTypeMap.put("1", "Mobile Banking");

        return channelTypeMap;

    }

    public String getChannelTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("2")) {
            des = "Internet Banking";
        } else if (code.equals("1")) {
            des = "Mobile Banking";
        } else {
            des = code;
        }
        return des;

    }
    
//    private String getTxnModeDesByCode(String txntype,String code) {
//        String des = "--";
//        if(txntype.equals("26")){
//            if (code.equals("1")) {
//                des = "Own Fund Transfer";
//            } else if (code.equals("2")) {
//                des = "Internal Fund Transfer";
//            } else if (code.equals("3")) {
//                des = "3rd Party CEFT Transfer";
//            } else if (code.equals("4")) {
//                des = "3rd Party SLIPS Transfer";
//            } else {
//                des = code;
//            }
//        }else if(txntype.equals("27")){
//            if (code.equals("1")) {
//                des = "Own Card Payments";
//            } else if (code.equals("2")) {
//                des = "Other NDB Card Payment";
//            } else if (code.equals("3")) {
//                des = "Other Bank Card Payment";
//            } else if (code.equals("4")) {
//                des = "Bill Payment";
//            } else if (code.equals("5")) {
//                des = "Mobile Reload";
//            } else {
//                des = code;
//            }
//        }else if(txntype.equals("49")){
//            if (code.equals("1")) {
//                des = "Card On";
//            } else if (code.equals("2")) {
//                des = "Card Off";
//            } else if (code.equals("3")) {
//                des = "Card Activate";
//            } else if (code.equals("4")) {
//                des = "Card Deactivate";
//            } else {
//                des = code;
//            }
//        }else{
//            des = code;
//        }
//        return des;
//
//    }
    private String toMaskCardNumber(String cardNumber) {

        StringBuilder maskCardNumber = new StringBuilder();
        String mask = "**********************";
        if(cardNumber.length()>4){
            int maskLength = cardNumber.length() - 4;
            maskCardNumber
                    .append(mask.substring(0, maskLength))
                    .append(cardNumber.substring(maskLength, cardNumber.length()));
        }else{
            maskCardNumber.append(cardNumber);
        }
        return maskCardNumber.toString();
    }
    
    public String getStatusDescription(String status) throws ParseException {

        String rDescription = null;
        
        if(status!= null && !status.isEmpty()){
            if(status.equals("SUCC")){
                rDescription ="Success";
            }else if(status.equals("FAIL")){
                rDescription ="Failure";
            }
        }
        return rDescription;
    }
}
