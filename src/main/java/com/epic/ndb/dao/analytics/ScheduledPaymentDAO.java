/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.analytics;

import com.epic.ndb.bean.analytics.ScheduledPaymentBean;
import com.epic.ndb.bean.analytics.ScheduledPaymentInputBean;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.SwtSchedulePayment;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class ScheduledPaymentDAO {
    
    public List<ScheduledPaymentBean> getSearchList(ScheduledPaymentInputBean inputBean, int max, int first, String orderBy) throws Exception {
        List<ScheduledPaymentBean> dataList = new ArrayList<ScheduledPaymentBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = " order by u.lastUpdatedOn desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from SwtSchedulePayment as u where " + where;
            Query queryCount = session.createQuery(sqlCount);
            this.setParameter(queryCount, inputBean);
            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from SwtSchedulePayment u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                this.setParameter(querySearch, inputBean);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    ScheduledPaymentBean jcode = new ScheduledPaymentBean();
                    SwtSchedulePayment schedulePayment = (SwtSchedulePayment) it.next();

                    try {
                        jcode.setId(schedulePayment.getId().toString());
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setPayType(schedulePayment.getSchedulePayType().getDescription());
                    } catch (Exception e) {
                        jcode.setPayType("--");
                    }
                    try {
                        jcode.setFrequency(schedulePayment.getSwtScheduleFrequency().getDescription());
                    } catch (Exception e) {
                        jcode.setFrequency("--");
                    }
                    try {
                        jcode.setStatus(schedulePayment.getStatusByStatus().getDescription());
                    } catch (Exception e) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setScheduleStatus(schedulePayment.getStatusByScheduleStatus().getDescription());
                    } catch (Exception e) {
                        jcode.setScheduleStatus("--");
                    }
                    try {
                        jcode.setCif(schedulePayment.getSwtMobileUser().getCif());
                    } catch (Exception npe) {
                        jcode.setCif("--");
                    }
                    try {
                        jcode.setRecurringDate(schedulePayment.getRecurringDate().toString());
                    } catch (NullPointerException npe) {
                        jcode.setRecurringDate("--");
                    }
                    try {
                        jcode.setStartDate(schedulePayment.getStartDate().toString());
                    } catch (NullPointerException npe) {
                        jcode.setStartDate("--");
                    }
                    try {
                        jcode.setEndDate(schedulePayment.getEndDate().toString());
                    } catch (NullPointerException npe) {
                        jcode.setEndDate("--");
                    }
                    try {
                        jcode.setCreatedDate(schedulePayment.getCreatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setCreatedDate("--");
                    }
                    try {
                        jcode.setLastUpdatedOn(schedulePayment.getLastUpdatedOn().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setLastUpdatedOn("--");
                    }
                    try {
                        jcode.setLastRanDate(schedulePayment.getLastRanDate().toString());
                    } catch (NullPointerException npe) {
                        jcode.setLastRanDate("--");
                    }
                    try {
                        jcode.setNoOfInstallments(schedulePayment.getNoOfInstallments().toString());
                    } catch (NullPointerException npe) {
                        jcode.setNoOfInstallments("--");
                    }
                    try {
                        jcode.setNoOfRuns(schedulePayment.getNoOfRuns().toString());
                    } catch (NullPointerException npe) {
                        jcode.setNoOfRuns("--");
                    }
                    try {
                        jcode.setAmount(schedulePayment.getAmount().toString());
                    } catch (NullPointerException npe) {
                        jcode.setAmount("--");
                    }
                    try {
                        jcode.setFromAccount(schedulePayment.getFromAccount().toString());
                    } catch (NullPointerException npe) {
                        jcode.setFromAccount("--");
                    }
                    try {
                        jcode.setToAccount(schedulePayment.getToAccount().toString());
                    } catch (NullPointerException npe) {
                        jcode.setToAccount("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(ScheduledPaymentInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getCif()!= null && !inputBean.getCif().isEmpty()) {
            where += " and lower(u.swtMobileUser.cif) like lower('%" + inputBean.getCif().trim() + "%')";
        }
        if (inputBean.getPayType()!= null && !inputBean.getPayType().isEmpty()) {
            where += " and u.schedulePayType.payType = '" + inputBean.getPayType().trim() + "'";
        }
        if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
            where += " and u.statusByStatus.statuscode = '" + inputBean.getStatus().trim() + "'";
        }
        if (inputBean.getStatus() != null && !inputBean.getStatus().isEmpty()) {
            where += " and u.statusByStatus.statuscode = '" + inputBean.getStatus().trim() + "'";
        }
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += " and u.createdDate >=:fromDate";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            where += " and u.createdDate <:toDate";
        }
        return where;
    }
    private void setParameter(Query query,ScheduledPaymentInputBean inputBean) throws ParseException{
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Timestamp timeStampDate = new Timestamp(sdff.parse(datef).getTime());
            query.setTimestamp("fromDate", timeStampDate);
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // End date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            Timestamp timeStampDate = new Timestamp(cf.getTime().getTime());
            query.setTimestamp("toDate", timeStampDate);
        }
    }
}
