/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.analytics;

import com.epic.ndb.bean.analytics.ScheduledPaymentHistoryBean;
import com.epic.ndb.bean.analytics.ScheduledPaymentHistoryInputBean;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.SwtScheduleHistory;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class ScheduledPaymentHistoryDAO {
    
    public List<ScheduledPaymentHistoryBean> getSearchList(ScheduledPaymentHistoryInputBean inputBean, int max, int first, String orderBy) throws Exception {
        List<ScheduledPaymentHistoryBean> dataList = new ArrayList<ScheduledPaymentHistoryBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = " order by u.updated desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from SwtScheduleHistory as u where " + where;
            Query queryCount = session.createQuery(sqlCount);
            this.setParameter(queryCount, inputBean);
            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from SwtScheduleHistory u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                this.setParameter(querySearch, inputBean);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    ScheduledPaymentHistoryBean bean = new ScheduledPaymentHistoryBean();
                    SwtScheduleHistory schedulePayment = (SwtScheduleHistory) it.next();

                    try {
                        bean.setId(schedulePayment.getId().toString());
                    } catch (NullPointerException npe) {
                        bean.setId("--");
                    }
                    try {
                        bean.setScheduleId(schedulePayment.getSwtSchedulePayment().getId().toString());
                    } catch (Exception e) {
                        bean.setScheduleId("--");
                    }
                    try {
                        bean.setCif(schedulePayment.getSwtSchedulePayment().getSwtMobileUser().getCif());
                    } catch (Exception e) {
                        bean.setCif("--");
                    }
                    try {
                        bean.setDescription(schedulePayment.getDescription());
                    } catch (Exception e) {
                        bean.setDescription("--");
                    }
                    
                    try {
                        bean.setUpdateddate(schedulePayment.getUpdated().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        bean.setUpdateddate("--");
                    }
                    

                    bean.setFullCount(count);

                    dataList.add(bean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(ScheduledPaymentHistoryInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getCif()!= null && !inputBean.getCif().isEmpty()) {
            where += " and lower(u.swtSchedulePayment.swtMobileUser.cif) like lower('%" + inputBean.getCif().trim() + "%')";
        }
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += " and u.updated >=:fromDate";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            where += " and u.updated <:toDate";
        }
        return where;
    }
    private void setParameter(Query query,ScheduledPaymentHistoryInputBean inputBean) throws ParseException{
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            String datef = inputBean.getFromDate();  // Start date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Timestamp timeStampDate = new Timestamp(sdff.parse(datef).getTime());
            query.setTimestamp("fromDate", timeStampDate);
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            String datef = inputBean.getToDate();  // End date
            SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cf = Calendar.getInstance();
            cf.setTime(sdff.parse(datef));
            cf.add(Calendar.DATE, 1);
            Timestamp timeStampDate = new Timestamp(cf.getTime().getTime());
            query.setTimestamp("toDate", timeStampDate);
        }
    }
    
    public StringBuffer makeCSVReport(ScheduledPaymentHistoryInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
             content = new StringBuffer();
            //write column headers to csv file
            content.append("CID");
            content.append(',');
            content.append("Schedule ID");
            content.append(',');
            content.append("Description");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');
            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from SwtScheduleHistory as u where " + where;
            Query queryCount = session.createQuery(sqlCount);
            this.setParameter(queryCount, inputBean);
            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from SwtScheduleHistory u where " + where;
                Query querySearch = session.createQuery(sqlSearch);
                this.setParameter(querySearch, inputBean);
                
                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    SwtScheduleHistory schedulePayment = (SwtScheduleHistory) it.next();

                    try {
                        content.append(schedulePayment.getSwtSchedulePayment().getSwtMobileUser().getCif());
                    } catch (Exception e) {
                        content.append("--");
                    }
                    content.append(',');
                    try {
                        content.append(schedulePayment.getSwtSchedulePayment().getId().toString());
                    } catch (Exception e) {
                        content.append("--");
                    }
                    content.append(',');
                    try {
                        content.append(schedulePayment.getDescription());
                    } catch (Exception e) {
                        content.append("--");
                    }
                    content.append(',');
                    try {
                        content.append(schedulePayment.getUpdated().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        content.append("--");
                    }
                    content.append('\n');
                }
            }
             content.append('\n');
            //write column top to csv file
            content.append("From Date :");
            content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
            content.append('\n');

            content.append("To Date :");
            content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
            content.append('\n');

            content.append("CID :");
            content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
            content.append('\n');

        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
}
