/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.registeredbeneficiary;

import com.epic.ndb.bean.registeredbeneficiary.RegisteredBeneficiaryBean;
import com.epic.ndb.bean.registeredbeneficiary.RegisteredBeneficiaryInputBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.varlist.CommonVarList;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class PayeeExplorerDAO {
    private String PAYEE_COUNT_SQL = "SELECT "
            + "COUNT(P.PAYEE_DETAILS_ID) "
            + "FROM PAYEE_DETAILS P "
            + "LEFT OUTER JOIN SWT_MOBILE_USER U ON U.CIF = P.CIF "
            + "WHERE ";
    private String PAYEE_ORDER_BY_SQL = " order by P.CRT_DATE DESC ";
    
    public List<RegisteredBeneficiaryBean> getSearchList(RegisteredBeneficiaryInputBean inputBean, int max, int first, String orderBy) throws Exception {
        List<RegisteredBeneficiaryBean> dataList = new ArrayList<RegisteredBeneficiaryBean>();
         Session session = null;

        String PAYEE_SQL_SEARCH = "SELECT "
                + "U.ID, "//0
                + "U.CIF, "//1
                + "U.USERNAME, "//2
                + "U.CUSTOMER_NAME, "//3
                + "U.MOBILE_NUMBER, "//4 
                + "U.CUSTOMER_CATEGORY, "//5
                + "P.CRT_DATE, "//6
                + "P.PAYEE_ACC_NO, "//7
                + "P.ACCOUNT_TYPE_CODE, "//8
                + "P.NICKNAME, "//9
                + "P.BANK_CODE, "//10
                + "P.BANK_NAME, "//11
                + "P.BRANCH_NAME , "//12   

                + "row_number() over ( " + orderBy + " ) as r "
                + "FROM PAYEE_DETAILS P "
                + "LEFT OUTER JOIN SWT_MOBILE_USER U ON U.CIF = P.CIF "
                + "WHERE ";

        try {
            CommonDAO cmd = new CommonDAO();
            ParameterUserCommon c = cmd.findCommonConfigById(CommonVarList.NDB_BANK_CODE_SLIP);
            String ndbBankCode = c.getParamvalue();
            long count = 0;
            String where = this.makeWhereClauseForSearch(inputBean,ndbBankCode);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = PAYEE_COUNT_SQL + where;
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sqlSearch = " SELECT * from (" + PAYEE_SQL_SEARCH + where + ") where r > " + first + " and r<= " + max;
                Query querySearch = session.createSQLQuery(sqlSearch);

                List<Object[]> objectArrList = (List<Object[]>) querySearch.list();

                if (objectArrList.size() > 0) {

                    for (Object[] objArr : objectArrList) {

                        RegisteredBeneficiaryBean searchBean = new RegisteredBeneficiaryBean();

                        try {
                            searchBean.setUserId(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserId("--");
                        }
                        try {
                            searchBean.setCif(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUserName(objArr[2].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserName("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[3].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setMobileNo(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNo("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            searchBean.setRegistationDate(objArr[6].toString().substring(0, 19));
                        } catch (IndexOutOfBoundsException e) {
                            searchBean.setRegistationDate(objArr[6].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRegistationDate("--");
                        }
                        try {
                            searchBean.setRegistationBeneficiary(objArr[7].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRegistationBeneficiary("--");
                        }
                        try {
                            searchBean.setAccountType(objArr[8].toString());
                        } catch (NullPointerException e) {
                            searchBean.setAccountType("--");
                        }
                        try {
                            searchBean.setName(objArr[9].toString());
                        } catch (NullPointerException e) {
                            searchBean.setName("--");
                        }
                        try {
                            searchBean.setBeneficiaryType(this.getBeneficiaryBankCode(objArr[10].toString(), ndbBankCode));
                        } catch (NullPointerException e) {
                            searchBean.setBeneficiaryType("--");
                        }
                        try {
                            searchBean.setBankCode(objArr[10].toString());
                        } catch (NullPointerException e) {
                            searchBean.setBankCode("--");
                        }
                        try {
                            searchBean.setBankName(objArr[11].toString());
                        } catch (NullPointerException e) {
                            searchBean.setBankName("--");
                        }
                        try {
                            searchBean.setBranchName(objArr[12].toString());
                        } catch (NullPointerException e) {
                            searchBean.setBranchName("--");
                        }
                        
                        searchBean.setBillerCategory("--");
                        searchBean.setBiller("--");
                        
                        searchBean.setFullCount(count);
                        dataList.add(searchBean);
                    }

                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }
     
    public StringBuffer makeCSVReport(RegisteredBeneficiaryInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;

        String PAYEE_SQL_CSV = "SELECT "
                + "U.ID, "//0
                + "U.CIF, "//1
                + "U.USERNAME, "//2
                + "U.CUSTOMER_NAME, "//3
                + "U.MOBILE_NUMBER, "//4 
                + "U.CUSTOMER_CATEGORY, "//5
                + "P.CRT_DATE, "//6
                + "P.PAYEE_ACC_NO, "//7
                + "P.ACCOUNT_TYPE_CODE, "//8
                + "P.NICKNAME, "//9
                + "P.BANK_CODE, "//10
                + "P.BANK_NAME, "//11
                + "P.BRANCH_NAME  "//12   
                + "FROM PAYEE_DETAILS P "
                + "LEFT OUTER JOIN SWT_MOBILE_USER U ON U.CIF = P.CIF "
                + "WHERE ";

        try {
            CommonDAO cmd = new CommonDAO();
//            ParameterUserCommon c = cmd.findCommonConfigById(CommonVarList.NDB_BANK_CODE_CEFT);
            ParameterUserCommon c = cmd.findCommonConfigById(CommonVarList.NDB_BANK_CODE_SLIP);
            String ndbBankCode = c.getParamvalue();
            session = HibernateInit.sessionFactory.openSession();
            int count = 0;
            String where1 = this.makeWhereClauseForSearch(inputBean, ndbBankCode);
            String sqlCount = this.PAYEE_COUNT_SQL + where1;
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = PAYEE_SQL_CSV + where1;

                Query query = session.createSQLQuery(sql);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    content = new StringBuffer();
                    List<RegisteredBeneficiaryBean> beanlist = new ArrayList<RegisteredBeneficiaryBean>();

                    for (Object[] objArr : objectArrList) {

                        RegisteredBeneficiaryBean searchBean = new RegisteredBeneficiaryBean();

                        try {
                            searchBean.setUserId(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserId("--");
                        }
                        try {
                            searchBean.setCif(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUserName(objArr[2].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserName("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[3].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setMobileNo(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNo("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            searchBean.setRegistationDate(objArr[6].toString().substring(0, 19));
                        } catch (IndexOutOfBoundsException e) {
                            searchBean.setRegistationDate(objArr[6].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRegistationDate("--");
                        }
                        try {
                            searchBean.setRegistationBeneficiary(objArr[7].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRegistationBeneficiary("--");
                        }
                        try {
                            searchBean.setAccountType(objArr[8].toString());
                        } catch (NullPointerException e) {
                            searchBean.setAccountType("--");
                        }
                        try {
                            searchBean.setName(objArr[9].toString());
                        } catch (NullPointerException e) {
                            searchBean.setName("--");
                        }
                        try {
                            searchBean.setBeneficiaryType(this.getBeneficiaryBankCode(objArr[10].toString(), ndbBankCode));
                        } catch (NullPointerException e) {
                            searchBean.setBeneficiaryType("--");
                        }
                        try {
                            searchBean.setBankCode(objArr[10].toString());
                        } catch (NullPointerException e) {
                            searchBean.setBankCode("--");
                        }
                        try {
                            searchBean.setBankName(objArr[11].toString());
                        } catch (NullPointerException e) {
                            searchBean.setBankName("--");
                        }
                        try {
                            searchBean.setBranchName(objArr[12].toString());
                        } catch (NullPointerException e) {
                            searchBean.setBranchName("--");
                        }
                        
                        searchBean.setBillerCategory("--");
                        searchBean.setBiller("--");
                        

                        beanlist.add(searchBean);
                    }

                    //write column headers to csv file
                    content.append("Unique ID");
                    content.append(',');
                    content.append("CID");
                    content.append(',');
                    content.append("User Name");
                    content.append(',');
                    content.append("Customer Name");
                    content.append(',');
                    content.append("Customer Category");
                    content.append(',');
                    content.append("Mobile No");
                    content.append(',');
                    content.append("Registration Date");
                    content.append(',');
                    content.append("Beneficiary Type");
                    content.append(',');
                    content.append("Registration Beneficiary");
                    content.append(',');
                    content.append("Name");
                    content.append(',');
//                    content.append("Account Type");
//                    content.append(',');
                    content.append("Bank Code");
                    content.append(',');
//                    content.append("Bank Name");
//                    content.append(',');
                    content.append("Branch Name");
                    content.append(',');
                    content.append("Biller Category");
                    content.append(',');
                    content.append("Biller");

                    content.append('\n');

                    //write data values to csv file
                    for (RegisteredBeneficiaryBean dataBean : beanlist) {
                        try {
                            if (dataBean.getUserId() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getUserId());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getCif() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCif());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getUserName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getUserName()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerName()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerCategory()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getMobileNo()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMobileNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getRegistationDate()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getRegistationDate());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBeneficiaryType()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBeneficiaryType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getRegistationBeneficiary()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getRegistationBeneficiary());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getName()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getAccountType()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getAccountType());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getBankCode()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBankCode());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getBankName()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getBankName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getBranchName()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBranchName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBillerCategory()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBiller()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBiller());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        
                        content.append('\n');
                    }
                    content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
                    content.append('\n');

                    content.append("CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
                    content.append('\n');

                    content.append("Beneficiary Type :");
                    if (inputBean.getBeneficiaryType()!= null && !inputBean.getBeneficiaryType().isEmpty()) {
                        String description = this.getBeneficiaryDesByCode(inputBean.getBeneficiaryType());
                        if (description != null) {
                            content.append(description);
                        } else {
                            content.append("-ALL-");
                        }
                    } else {
                        content.append("-ALL-");
                    }
                    content.append('\n');
                    
                    content.append("Customer Category :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategory()));
                    content.append('\n');

                }

            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
     
    private String makeWhereClauseForSearch(RegisteredBeneficiaryInputBean inputBean,String ndbBankCode) throws ParseException, Exception {
        String where = "1=1";
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += " and lower(P.CRT_DATE) >= TO_DATE( '" + inputBean.getFromDate() + "' , 'yy-mm-dd')";

        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getToDate());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and lower(P.CRT_DATE) <= TO_DATE( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(U.CIF) like lower('%" + inputBean.getCif().trim() + "%')";
        }
        if (inputBean.getCustomerCategory()!= null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(U.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        if (inputBean.getBeneficiaryType()!= null && !inputBean.getBeneficiaryType().isEmpty()) {
            if (inputBean.getBeneficiaryType().equals("NDBBANKACC")) {
                where += " and P.BANK_CODE = '"+ndbBankCode.trim()+"'";
            } else if (inputBean.getBeneficiaryType().equals("OTHERBANKACC")) {
                where += " and P.BANK_CODE != '"+ndbBankCode.trim()+"'";
            }
        }
        

        return where;
    }
      
    public String getBeneficiaryBankCode(String bankCode,String ndbBankCode) {
        String des ;
        if (bankCode!=null && ndbBankCode.trim().equals(bankCode.trim())) {
            des = "Other NDB Account";
        } else {
            des = "Other Bank Account";
        }
        return des;

    }  
    public String getBeneficiaryDesByCode(String code) {
        String rDescription = null;
        
        if (code != null && !code.isEmpty()) {
            if (code.equals("NDBBANKACC")) {
                rDescription = "Other NDB Account";
            } else if (code.equals("OTHERBANKACC")) {
                rDescription = "Other Bank Account";
            }
        }
        return rDescription;

    }  
}
