/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.registeredbeneficiary;

import com.epic.ndb.bean.registeredbeneficiary.RegisteredBeneficiaryBean;
import com.epic.ndb.bean.registeredbeneficiary.RegisteredBeneficiaryInputBean;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author sivaganesan_t
 */
public class PayToMobileDAO {
    private String PAY_TO_MOB_COUNT_SQL = "SELECT "
            + "COUNT(P.ID) "
            + "FROM SWT_PAYTO_MOBILE P "
            + "LEFT OUTER JOIN SWT_MOBILE_USER U ON U.ID = P.USERID "
            + "LEFT OUTER JOIN SWT_MOBILE_USER B ON B.ID = P.PAYEE_MOBILE_USER_ID "
            + "WHERE ";
    private String PAY_TO_MOB_ORDER_BY_SQL = " order by P.CREATED_TIME DESC ";
      
    public List<RegisteredBeneficiaryBean> getSearchList(RegisteredBeneficiaryInputBean inputBean, int max, int first, String orderBy) throws Exception {
        List<RegisteredBeneficiaryBean> dataList = new ArrayList<RegisteredBeneficiaryBean>();
         Session session = null;

        String PAY_TO_MOB_SQL_SEARCH = "SELECT "
                + "U.ID, "//0
                + "U.CIF, "//1
                + "U.USERNAME, "//2
                + "U.CUSTOMER_NAME, "//3
                + "U.MOBILE_NUMBER, "//4 
                + "U.CUSTOMER_CATEGORY, "//5
                + "P.CREATED_TIME, "//6
                + "B.MOBILE_NUMBER REG_BENEFICI, "//7
                + "P.NICKNAME, "//8 

                + "row_number() over ( " + orderBy + " ) as r "
                + "FROM SWT_PAYTO_MOBILE P "
                + "LEFT OUTER JOIN SWT_MOBILE_USER U ON U.ID = P.USERID "
                + "LEFT OUTER JOIN SWT_MOBILE_USER B ON B.ID = P.PAYEE_MOBILE_USER_ID "
                + "WHERE ";

        try {
            long count = 0;
            String where = this.makeWhereClauseForSearch(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = PAY_TO_MOB_COUNT_SQL + where;
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sqlSearch = " SELECT * from (" + PAY_TO_MOB_SQL_SEARCH + where + ") where r > " + first + " and r<= " + max;
                Query querySearch = session.createSQLQuery(sqlSearch);

                List<Object[]> objectArrList = (List<Object[]>) querySearch.list();

                if (objectArrList.size() > 0) {

                    for (Object[] objArr : objectArrList) {

                        RegisteredBeneficiaryBean searchBean = new RegisteredBeneficiaryBean();

                        try {
                            searchBean.setUserId(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserId("--");
                        }
                        try {
                            searchBean.setCif(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUserName(objArr[2].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserName("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[3].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setMobileNo(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNo("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            searchBean.setRegistationDate(objArr[6].toString().substring(0, 19));
                        } catch (IndexOutOfBoundsException e) {
                            searchBean.setRegistationDate(objArr[6].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRegistationDate("--");
                        }
                        try {
                            searchBean.setRegistationBeneficiary(objArr[7].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRegistationBeneficiary("--");
                        }
                        
                        searchBean.setAccountType("--");
                      
                        try {
                            searchBean.setName(objArr[8].toString());
                        } catch (NullPointerException e) {
                            searchBean.setName("--");
                        }
                        
                        searchBean.setBeneficiaryType("Pay to Mobile");
                        searchBean.setBankCode("--");
//                        searchBean.setBankName("--");
                        searchBean.setBranchName("--");
                        searchBean.setBillerCategory("--");
                        searchBean.setBiller("--");
                        
                        searchBean.setFullCount(count);
                        dataList.add(searchBean);
                    }

                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }
    
    public StringBuffer makeCSVReport(RegisteredBeneficiaryInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;

        String PAY_TO_MOB_SQL_CSV = "SELECT "
                + "U.ID, "//0
                + "U.CIF, "//1
                + "U.USERNAME, "//2
                + "U.CUSTOMER_NAME, "//3
                + "U.MOBILE_NUMBER, "//4 
                + "U.CUSTOMER_CATEGORY, "//5
                + "P.CREATED_TIME, "//6
                + "B.MOBILE_NUMBER REG_BENEFICI, "//7
                + "P.NICKNAME "//8 
                + "FROM SWT_PAYTO_MOBILE P "
                + "LEFT OUTER JOIN SWT_MOBILE_USER U ON U.ID = P.USERID "
                + "LEFT OUTER JOIN SWT_MOBILE_USER B ON B.ID = P.PAYEE_MOBILE_USER_ID "
                + "WHERE ";

        try {
            session = HibernateInit.sessionFactory.openSession();
            int count = 0;
            String where1 = this.makeWhereClauseForSearch(inputBean);
            String sqlCount = this.PAY_TO_MOB_COUNT_SQL + where1;
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = PAY_TO_MOB_SQL_CSV + where1;

                Query query = session.createSQLQuery(sql);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    content = new StringBuffer();
                    List<RegisteredBeneficiaryBean> beanlist = new ArrayList<RegisteredBeneficiaryBean>();

                    for (Object[] objArr : objectArrList) {

                        RegisteredBeneficiaryBean searchBean = new RegisteredBeneficiaryBean();

                        try {
                            searchBean.setUserId(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserId("--");
                        }
                        try {
                            searchBean.setCif(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUserName(objArr[2].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserName("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[3].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setMobileNo(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNo("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            searchBean.setRegistationDate(objArr[6].toString().substring(0, 19));
                        } catch (IndexOutOfBoundsException e) {
                            searchBean.setRegistationDate(objArr[6].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRegistationDate("--");
                        }
                        try {
                            searchBean.setRegistationBeneficiary(objArr[7].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRegistationBeneficiary("--");
                        }
                        
                        searchBean.setAccountType("--");
                      
                        try {
                            searchBean.setName(objArr[8].toString());
                        } catch (NullPointerException e) {
                            searchBean.setName("--");
                        }
                        
                        searchBean.setBeneficiaryType("Pay to Mobile");
                        searchBean.setBankCode("--");
//                        searchBean.setBankName("--");
                        searchBean.setBranchName("--");
                        searchBean.setBillerCategory("--");
                        searchBean.setBiller("--");
                        

                        beanlist.add(searchBean);
                    }

                    //write column headers to csv file
                    content.append("Unique ID");
                    content.append(',');
                    content.append("CID");
                    content.append(',');
                    content.append("User Name");
                    content.append(',');
                    content.append("Customer Name");
                    content.append(',');
                    content.append("Customer Category");
                    content.append(',');
                    content.append("Mobile No");
                    content.append(',');
                    content.append("Registration Date");
                    content.append(',');
                    content.append("Beneficiary Type");
                    content.append(',');
                    content.append("Registration Beneficiary");
                    content.append(',');
                    content.append("Name");
                    content.append(',');
//                    content.append("Account Type");
//                    content.append(',');
                    content.append("Bank Code");
                    content.append(',');
//                    content.append("Bank Name");
//                    content.append(',');
                    content.append("Branch Name");
                    content.append(',');
                    content.append("Biller Category");
                    content.append(',');
                    content.append("Biller");

                    content.append('\n');

                    //write data values to csv file
                    for (RegisteredBeneficiaryBean dataBean : beanlist) {
                        try {
                            if (dataBean.getUserId() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getUserId());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getCif() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCif());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getUserName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getUserName()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerName()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerCategory()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getMobileNo()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMobileNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getRegistationDate()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getRegistationDate());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBeneficiaryType()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBeneficiaryType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getRegistationBeneficiary()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getRegistationBeneficiary());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getName()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getAccountType()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getAccountType());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getBankName()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBankName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBankCode()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBankCode());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getBranchName()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getBranchName());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getBillerCategory()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBillerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getBiller()== null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getBiller());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        
                        content.append('\n');
                    }
                    content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
                    content.append('\n');

                    content.append("CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif()));
                    content.append('\n');
                    
                    content.append("Customer Category :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategory()));
                    content.append('\n');

                }

            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
    
    private String makeWhereClauseForSearch(RegisteredBeneficiaryInputBean inputBean) throws ParseException, Exception {
        String where = "1=1";
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += " and lower(P.CREATED_TIME) >= to_timestamp( '" + inputBean.getFromDate() + "' , 'yy-mm-dd')";

        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getToDate());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and lower(P.CREATED_TIME) <= to_timestamp( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getCif() != null && !inputBean.getCif().isEmpty()) {
            where += " and lower(U.CIF) like lower('%" + inputBean.getCif().trim() + "%')";
        }
        if (inputBean.getCustomerCategory()!= null && !inputBean.getCustomerCategory().isEmpty()) {
            where += " and lower(U.CUSTOMER_CATEGORY) LIKE lower('%" + inputBean.getCustomerCategory().trim() + "%')";
        }
        

        return where;
    }
}
