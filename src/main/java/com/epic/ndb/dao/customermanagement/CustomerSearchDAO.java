/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.customermanagement;

import com.epic.ndb.bean.customermanagement.AlertBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchInputBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchLimitBean;
import com.epic.ndb.bean.customermanagement.CustomerSearchPendBean;
import com.epic.ndb.bean.customermanagement.CustomerUpdateBean;
import com.epic.ndb.bean.customermanagement.SwtConfigBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.ExcelCommon;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Sha256;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Passwordpolicy;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.SwtAlert;
import com.epic.ndb.util.mapping.SwtMobileUser;
import com.epic.ndb.util.mapping.SwtMtConfiguration;
import com.epic.ndb.util.mapping.SwtUserDevice;
import com.epic.ndb.util.mapping.SwtUserTxnLimit;
import com.epic.ndb.util.mapping.SwtUserTxnLimitId;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jpos.iso.ISOUtil;
import org.json.JSONObject;

/**
 *
 * @author sivaganesan_t
 */
public class CustomerSearchDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    private final int columnCount = 9;
    private final int headerRowCount = 18;

    private String CUSTOMER_COUNT_SQL = "SELECT "
            + "COUNT(ID) "
            + "FROM SWT_MOBILE_USER U "
            + "WHERE ";
    private String CUSTOMER_ORDER_BY_SQL = " order by U.CREATEDTIME DESC ";

    public List<CustomerSearchBean> getSearchList(CustomerSearchInputBean inputBean, int max, int first, String orderBy) throws Exception {
        List<CustomerSearchBean> dataList = new ArrayList<CustomerSearchBean>();
        Session session = null;

        String TXN_SQL_SEARCH = "SELECT "
                + "U.ID, "//0
                + "U.NIC, "//1
                + "U.DOB, "//2
                + "U.CIF, "//3
                + "U.USERNAME, "//4
                + "U.EMAIL, "//5
                + "U.MOBILE_NUMBER, "//6  
                + "U.LOGIN_ATTEMPTS, "//7   
                + "U.LASTUPDATEDTIME, "//8
                + "U.CREATEDTIME, "//9   
                + " U.STATUS STATUS, "//10
                + "S.DESCRIPTION STATUSDES, "//11
                + "SE.DESCRIPTION SEGMENT, "//12
                + "U.PROFILE_IMAGE, "//13
                + "U.PROF_IMAGE_TIMESTAMP, "//14
                + "U.OTP, "//15
                + "U.OTP_EXP_TIME, "//16
                + "U.MOBILE_PIN , "//17
                + "U.USER_TRACE_NUMBER, "//18
                + "U.SECONDARY_EMAIL, "//19
                + "U.SECONDARY_MOBILE, "//20
                + "U.REMARK, "//21
                + "U.MAKER, "//22
                + "U.CHECKER, "//23
                + "U.CUSTOMER_NAME, "//24
                + "U.LAST_LOGGEDIN_DATETIME, "//25
                + "U.ON_BOARD_CHANNEL, "//26
                + "U.ON_BOARD_TYPE, "//27
                + "U.CUSTOMER_CATEGORY, "//28
                + "U.DEFAULT_ACC_TYPE, "//29
                + "U.DEFAULT_ACC_NO, "//30
                + "U.FIRST_TIME_LOGIN_EXPECTED, "//31
                + "U.LAST_PASSWORD_UPDATED_DATE, "//32
                + "U.FINAL_FEE_DED_DAY, "//33
                + "W.DESCRIPTION WAIVEOFFDES, "//34
                + "U.CHARGE_AMOUNT , "//35
                + "U.ACTOFFICER , "//36
                + "U.PERMANENT_ADDRESS , "//37
                //                + "U.DEFAULT_CARD_NO , "//37

                + "row_number() over ( " + orderBy + " ) as r "
                + "FROM SWT_MOBILE_USER U "
                + "LEFT OUTER JOIN STATUS S ON S.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN STATUS W ON W.STATUSCODE = U.WAIVEOFF "
                + "LEFT OUTER JOIN SEGMENT_TYPE SE ON SE.SEGMENTCODE = U.USER_SEGMENT "
                + "WHERE ";

        try {

            long count = 0;
            String where = this.makeWhereClauseForSearch(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = CUSTOMER_COUNT_SQL + where;
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sqlSearch = " SELECT * from (" + TXN_SQL_SEARCH + where + this.CUSTOMER_ORDER_BY_SQL + ") where r > " + first + " and r<= " + max;
                Query querySearch = session.createSQLQuery(sqlSearch);

                List<Object[]> objectArrList = (List<Object[]>) querySearch.list();

                if (objectArrList.size() > 0) {

                    for (Object[] objArr : objectArrList) {

                        CustomerSearchBean searchBean = new CustomerSearchBean();

                        try {
                            searchBean.setUserid(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserid("--");
                        }

                        try {
                            searchBean.setNic(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setNic("--");
                        }
                        try {
                            searchBean.setDob(objArr[2].toString());
                        } catch (Exception e) {
                            searchBean.setDob("--");
                        }
                        try {
                            searchBean.setCif(objArr[3].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUsername(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUsername("--");
                        }
                        try {
                            searchBean.setEmail(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setEmail("--");
                        }
                        try {
                            searchBean.setMobileNumber(objArr[6].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNumber("--");
                        }
                        try {
                            searchBean.setLoginAttemptStatus(this.getUserBlocktatus(session, objArr[7].toString()));
                            searchBean.setLoginAttempts(objArr[7].toString());

                        } catch (NullPointerException e) {
                            searchBean.setLoginAttemptStatus("--");
                            searchBean.setLoginAttempts("--");
                        }
                        try {
                            searchBean.setLastupdatedtime(objArr[8].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastupdatedtime("--");
                        }
                        try {
                            searchBean.setCreatedtime(objArr[9].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setCreatedtime("--");
                        }
//                        try {
//                            searchBean.setStatus(objArr[10].toString());
//                        } catch (NullPointerException e) {
//                            searchBean.setStatus("--");
//                        }
                        try {
                            if ((objArr[4] == null || objArr[4].toString().isEmpty()) && objArr[10].toString().equals(CommonVarList.STATUS_ACTIVE)) {
                                searchBean.setStatusDes(CommonVarList.CUSTOMER_PEND_STATUS_DES);
                                searchBean.setStatus(CommonVarList.CUSTOMER_PEND_STATUS);
                            } else {
                                searchBean.setStatus(objArr[10].toString());
                                searchBean.setStatusDes(objArr[11].toString());
                            }
                        } catch (NullPointerException e) {
                            searchBean.setStatus("--");
                            searchBean.setStatusDes("--");
                        }
                        try {
                            searchBean.setPromotionMtSegmentType(objArr[12].toString());
                        } catch (Exception e) {
                            searchBean.setPromotionMtSegmentType("--");
                        }
                        try {
                            searchBean.setOtp(objArr[15].toString());
                        } catch (NullPointerException e) {
                            searchBean.setOtp("--");
                        }
                        try {
                            searchBean.setOtpExpTime(objArr[16].toString());
                        } catch (NullPointerException e) {
                            searchBean.setOtpExpTime("--");
                        }
                        try {
                            searchBean.setMobilePin(objArr[17].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobilePin("--");
                        }
                        try {
                            searchBean.setUserTraceNumber(objArr[18].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserTraceNumber("--");
                        }
                        try {
                            searchBean.setSecondaryEmail(objArr[19].toString());
                        } catch (NullPointerException e) {
                            searchBean.setSecondaryEmail("--");
                        }
                        try {
                            searchBean.setSecondaryMobile(objArr[20].toString());
                        } catch (NullPointerException e) {
                            searchBean.setSecondaryMobile("--");
                        }
                        try {
                            searchBean.setRemark(objArr[21].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRemark("--");
                        }
                        try {
                            searchBean.setMaker(objArr[22].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMaker("--");
                        }
                        try {
                            searchBean.setChecker(objArr[23].toString());
                        } catch (NullPointerException e) {
                            searchBean.setChecker("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[24].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setLastLoggedinDatetime(objArr[25].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastLoggedinDatetime("--");
                        }
                        try {
                            if (objArr[26] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[26].toString());
                                searchBean.setOnBoardChannel(channelTypeDes);
                            } else {
                                searchBean.setOnBoardChannel("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardChannel("--");
                        }
                        try {
                            if (objArr[27] != null) {
                                String onBordedTypeDes = this.getOnBordedTypeDesByCode(objArr[27].toString());
                                searchBean.setOnBoardType(onBordedTypeDes);
                            } else {
                                searchBean.setOnBoardType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardType("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[28].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            if (objArr[29] != null) {
                                String onDefaultTypeDes = this.getDefaultTypeDesByCode(objArr[29].toString());
                                searchBean.setDefType(onDefaultTypeDes);
                            } else {
                                searchBean.setDefType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setDefType("--");
                        }
                        try {
                            searchBean.setDefAccNo(objArr[30].toString());
                        } catch (NullPointerException e) {
                            searchBean.setDefAccNo("--");
                        }
                        try {
                            if (objArr[31] != null) {
                                String loginStatusDes = this.getLoginStatusDesByCode(objArr[31].toString());
                                searchBean.setLoginStatus(loginStatusDes);
                            } else {
                                searchBean.setLoginStatus("--");
                            }
                        } catch (Exception e) {
                            searchBean.setLoginStatus("--");
                        }
                        try {
                            searchBean.setLastPassUpdateTime(objArr[32].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastPassUpdateTime("--");
                        }
                        try {
                            searchBean.setFinalFeeDudDay(objArr[33].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setFinalFeeDudDay("--");
                        }
                        try {
                            searchBean.setWaveoff(objArr[34].toString());
                        } catch (NullPointerException e) {
                            searchBean.setWaveoff("--");
                        }
                        try {
                            searchBean.setChargeAmount(objArr[35].toString());
                        } catch (NullPointerException e) {
                            searchBean.setChargeAmount("--");
                        }
                        try {
                            searchBean.setAccountofficer(objArr[36].toString());
                        } catch (NullPointerException e) {
                            searchBean.setAccountofficer("--");
                        }
                        try {
                            searchBean.setPermanentaddress(objArr[37].toString());
                        } catch (NullPointerException e) {
                            searchBean.setPermanentaddress("--");
                        }

                        searchBean.setFullCount(count);
                        dataList.add(searchBean);
                    }

                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClauseForSearch(CustomerSearchInputBean inputBean) throws ParseException, Exception {
        String where = "1=1";
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += " and lower(U.CREATEDTIME) >= to_timestamp( '" + inputBean.getFromDate() + "' , 'yy-mm-dd')";

        }

        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getToDate());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and lower(U.CREATEDTIME) <= to_timestamp( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getCifSearch() != null && !inputBean.getCifSearch().isEmpty()) {
            where += " and lower(U.CIF) like lower('%" + inputBean.getCifSearch().trim() + "%')";
        }

        if (inputBean.getUsernameSearch() != null && !inputBean.getUsernameSearch().isEmpty()) {
            where += " and lower(U.USERNAME) like lower('%" + inputBean.getUsernameSearch().trim() + "%')";
        }

        if (inputBean.getNicSearch() != null && !inputBean.getNicSearch().isEmpty()) {
            where += " and lower(U.NIC) like lower('%" + inputBean.getNicSearch().trim() + "%')";
        }

        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            if (inputBean.getStatusSearch().equals(CommonVarList.STATUS_ACTIVE)) {
                where += " and U.STATUS = '" + inputBean.getStatusSearch() + "' and (U.USERNAME is not null or U.USERNAME != '')";
            } else if (inputBean.getStatusSearch().equals(CommonVarList.STATUS_CUSTOMER_PEND)) {
                where += " and U.STATUS = '" + CommonVarList.STATUS_ACTIVE + "' and (U.USERNAME is null or U.USERNAME = '')";
            } else {
                where += " and U.STATUS = '" + inputBean.getStatusSearch() + "'";
            }
        }
        if (inputBean.getMobilenoSearch() != null && !inputBean.getMobilenoSearch().isEmpty()) {
            where += " and lower(U.MOBILE_NUMBER) like lower('%" + inputBean.getMobilenoSearch().trim() + "%')";
        }

        if (inputBean.getEmailSearch() != null && !inputBean.getEmailSearch().isEmpty()) {
            where += " and lower(U.EMAIL) like lower('%" + inputBean.getEmailSearch().trim() + "%')";
        }

        if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
            where += " and lower(U.USER_SEGMENT) like lower('%" + inputBean.getSegmentTypeSearch().trim() + "%')";
        }
        if (inputBean.getOnBoardChannelSearch() != null && !inputBean.getOnBoardChannelSearch().isEmpty()) {
            where += " and U.ON_BOARD_CHANNEL = '" + inputBean.getOnBoardChannelSearch() + "'";
        }
        if (inputBean.getOnBoardTypeSearch() != null && !inputBean.getOnBoardTypeSearch().isEmpty()) {
            where += " and U.ON_BOARD_TYPE = '" + inputBean.getOnBoardTypeSearch() + "'";
        }
        if (inputBean.getCustomerCategorySearch() != null && !inputBean.getCustomerCategorySearch().isEmpty()) {
            where += " and lower(U.CUSTOMER_CATEGORY) like lower('%" + inputBean.getCustomerCategorySearch().trim() + "%')";
        }
        if (inputBean.getWaveoffSearch() != null && !inputBean.getWaveoffSearch().isEmpty()) {
            where += " and U.WAIVEOFF = '" + inputBean.getWaveoffSearch().trim() + "'";
        }
        if (inputBean.getDefTypeSearch() != null && !inputBean.getDefTypeSearch().isEmpty()) {
            where += " and U.DEFAULT_ACC_TYPE = '" + inputBean.getDefTypeSearch().trim() + "'";
        }
        if (inputBean.getDefAccNoSearch()!= null && !inputBean.getDefAccNoSearch().isEmpty()) {
            where += " and lower(U.DEFAULT_ACC_NO) like lower('%" + inputBean.getDefAccNoSearch().trim() + "%')";
        }

        return where;
    }

    public List<CustomerSearchPendBean> getSearchPendList(CustomerSearchInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<CustomerSearchPendBean> dataList = new ArrayList<CustomerSearchPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.CUSTOMER_SEARCH).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.CUSTOMER_SEARCH).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    CustomerSearchPendBean searchBean = new CustomerSearchPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        searchBean.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        searchBean.setId("--");
                    }
                    try {
                        searchBean.setOperation(pTask.getTask().getDescription().toString());
                        searchBean.setOperationcode(pTask.getTask().getTaskcode().toString());
                    } catch (NullPointerException npe) {
                        searchBean.setOperationcode("--");
                        searchBean.setOperation("--");
                    }
                    try {
                        searchBean.setUserid(pTask.getPKey().toString());
                    } catch (Exception ex) {
                        searchBean.setUserid("--");
                    }
                    try {
                        SwtMobileUser mobUser = (SwtMobileUser) session.get(SwtMobileUser.class, pTask.getPKey());
                        searchBean.setCif(mobUser.getCif().toString());
                    } catch (Exception ex) {
                        searchBean.setCif("--");
                    }
                    try {
                        searchBean.setFields(pTask.getFields().toString());
                    } catch (NullPointerException npe) {
                        searchBean.setFields("--");
                    } catch (Exception ex) {
                        searchBean.setFields("--");
                    }
                    try {
                        searchBean.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        searchBean.setStatus("--");
                    }
                    try {
                        searchBean.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        searchBean.setCreatetime("--");
                    }
                    try {
                        searchBean.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        searchBean.setCreateduser("--");
                    } catch (Exception ex) {
                        searchBean.setCreateduser("--");
                    }

                    searchBean.setFullCount(count);

                    dataList.add(searchBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public SwtMobileUser getCustomerByUserID(String userid) throws Exception {
        SwtMobileUser mobUser = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtMobileUser as mb where mb.id=:id";
            Query query = session.createQuery(sql);
            query.setString("id", userid);

            mobUser = (SwtMobileUser) query.list().get(0);

            //for Lazy intializing in hibernate
            mobUser.getStatus().getStatuscategory().getCategorycode();
            mobUser.getWaveoff().getDescription();
            if (mobUser.getSegmentType() != null) {
                mobUser.getSegmentType().getDescription();
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return mobUser;
    }

    public String updateCustomerStatus(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUserid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, inputBean.getUserid());
                if (u != null) {
//                    String oldV = u.getId() + "|"
//                            + u.getStatus().getStatuscode() + "|"
//                            + u.getWaveoff().getStatuscode() + "|"
//                            + u.getChargeAmount().toString();
//                    audit.setOldvalue(oldV);

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserid().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.CUSTOMER_STATUS_CHANGE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String updateCustomerAppStatus(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUserid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, inputBean.getUserid());
                if (u != null) {

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserid().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.CUS_BLOCK_APPLICATIONS);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String resetCustomerAttempts(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());
            //for remove dual auth
            SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, inputBean.getUserid());

            if (u != null) {
                //--------audit old value----------------
                String oldV = u.getId() + "|"
                        + u.getLoginAttempts();
                audit.setOldvalue(oldV);

                u.setLoginAttempts(new BigDecimal(BigInteger.ZERO));

                //-----------password reset start-------------
                String generatedString = RandomStringUtils.randomAlphanumeric(6);
                String hpassword = Sha256.Sha256(generatedString);

                byte[] salt = ISOUtil.hex2byte(u.getUserKey());
                String securePassword = getSecurePassword(hpassword, salt);

                u.setMobilePin(securePassword);
                u.setPasswordResetFlag("1");

                this.insertAlert(u.getMobileNumber(), generatedString);
                //-----------password reset end-------------

                u.setLastupdatedtime(timestampDate);
                u.setMaker(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.update(u);

                txn.commit();
              //for dual auth
//            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
//            Query query = session.createQuery(sql)
//                    .setString("PKey", inputBean.getUserid())
//                    .setString("pagecode", audit.getPagecode());
//
//            if (query.list().isEmpty()) {
//
//                SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, inputBean.getUserid());
//                if (u != null) {
//                    String oldV = u.getId() + "|"
//                            + u.getLoginAttempts();
//                    audit.setOldvalue(oldV);
//
//                    Pendingtask pendingtask = new Pendingtask();
//
//                    pendingtask.setPKey(inputBean.getUserid().trim());
//                    pendingtask.setFields(audit.getNewvalue());
//
//                    Task task = new Task();
//                    task.setTaskcode(TaskVarList.CUSTOMER_ATTEMPTS_RESET);
//                    pendingtask.setTask(task);
//
//                    Status st = new Status();
//                    st.setStatuscode(CommonVarList.STATUS_PENDING);
//                    pendingtask.setStatus(st);
//
//                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
//                    pendingtask.setPage(page);
//
//                    pendingtask.setInputterbranch(sysUser.getBranch());
//
//                    pendingtask.setCreatedtime(sysDate);
//                    pendingtask.setLastupdatedtime(sysDate);
//
//                    pendingtask.setCreateduser(audit.getLastupdateduser());
//
//                    audit.setCreatetime(sysDate);
//                    audit.setLastupdatedtime(sysDate);
//                    audit.setLastupdateduser(audit.getLastupdateduser());
//
//                    session.save(audit);
//                    session.save(pendingtask);
//
//                    txn.commit();
//                } else {
//                    message = MessageVarList.COMMON_NOT_EXISTS;
//                }
//            } else {
//                message = "pending available";
//            }
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }  
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String resetCustomerPassword(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            //for remove dual auth
            SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, inputBean.getUserid());
            if (u != null) {

                String generatedString = RandomStringUtils.randomAlphanumeric(6);
                String hpassword = Sha256.Sha256(generatedString);                       
                byte[] salt = ISOUtil.hex2byte(u.getUserKey());
                String securePassword = getSecurePassword(hpassword, salt);

                u.setMobilePin(securePassword);
                u.setPasswordResetFlag("1");

                u.setLoginAttempts(new BigDecimal(BigInteger.ZERO));//attempts count reset

                this.insertAlert(u.getMobileNumber(), generatedString);

                u.setMaker(audit.getLastupdateduser());
                u.setLastupdatedtime(timestampDate);
                
                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.update(u);
                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
            //for dual auth
//            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
//            Query query = session.createQuery(sql)
//                    .setString("PKey", inputBean.getUserid())
//                    .setString("pagecode", audit.getPagecode());
//
//            if (query.list().isEmpty()) {
//
//                SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, inputBean.getUserid());
//                if (u != null) {
//                    String oldV = u.getId();
//                    audit.setOldvalue(oldV);
//
//                    Pendingtask pendingtask = new Pendingtask();
//
//                    pendingtask.setPKey(inputBean.getUserid().trim());
//                    pendingtask.setFields(audit.getNewvalue());
//
//                    Task task = new Task();
//                    task.setTaskcode(TaskVarList.CUSTOMER_PAS_RESET_TASK);
//                    pendingtask.setTask(task);
//
//                    Status st = new Status();
//                    st.setStatuscode(CommonVarList.STATUS_PENDING);
//                    pendingtask.setStatus(st);
//
//                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
//                    pendingtask.setPage(page);
//
//                    pendingtask.setInputterbranch(sysUser.getBranch());
//
//                    pendingtask.setCreatedtime(sysDate);
//                    pendingtask.setLastupdatedtime(sysDate);
//
//                    pendingtask.setCreateduser(audit.getLastupdateduser());
//
//                    audit.setCreatetime(sysDate);
//                    audit.setLastupdatedtime(sysDate);
//                    audit.setLastupdateduser(audit.getLastupdateduser());
//
//                    session.save(audit);
//                    session.save(pendingtask);
//
//                    txn.commit();
//                } else {
//                    message = MessageVarList.COMMON_NOT_EXISTS;
//                }
//            } else {
//                message = "pending available";
//            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String updateCustomerAccounts(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUserid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, inputBean.getUserid());
                if (u != null) {
//                    String oldV = u.getId() + "|"
//                            + u.getDefaultAccNo();
//                    audit.setOldvalue(oldV);
                    String field = audit.getNewvalue();
                    if (inputBean.getDefTypeHidden().equals(CommonVarList.CUSTOMER_DEF_TYPE_ACC)) {
                        field += "|" + inputBean.getAccountproduct();
                    } else if (inputBean.getDefTypeHidden().equals(CommonVarList.CUSTOMER_DEF_TYPE_CARD)) {
                        field += "|" + inputBean.getCardToken();
                    }
                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserid().trim());
                    pendingtask.setFields(field);

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String updateCustomerData(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUserid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, inputBean.getUserid());
                if (u != null) {
                    String field = audit.getNewvalue();
                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserid().trim());
                    pendingtask.setFields(field);

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.CUSTOMER_DATA_UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String insertUseTxnLimit(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            SwtUserTxnLimitId id = new SwtUserTxnLimitId();
            id.setTxnLimitType(inputBean.getTxntypelimit());
            id.setUserid(inputBean.getUserid());
            if ((SwtUserTxnLimit) session.get(SwtUserTxnLimit.class, id) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getUserid())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserid().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_CUSTOMER_LIMIT);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();

                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String updateUseTxnLimit(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUserid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                SwtUserTxnLimitId id = new SwtUserTxnLimitId();
                id.setTxnLimitType(inputBean.getTxntype());
                id.setUserid(inputBean.getUserid());
                SwtUserTxnLimit u = (SwtUserTxnLimit) session.get(SwtUserTxnLimit.class, id);
                if (u != null) {
                    String oldV = u.getId().getUserid() + "|"
                            + u.getId().getTxnLimitType() + "|"
                            + u.getUserLimit() + "|"
                            + u.getStatus();
                    audit.setOldvalue(oldV);

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserid().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_CUSTOMER_LIMIT);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteUseTxnLimit(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUserid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                SwtUserTxnLimitId id = new SwtUserTxnLimitId();
                id.setTxnLimitType(inputBean.getTxntype());
                id.setUserid(inputBean.getUserid());
                SwtUserTxnLimit u = (SwtUserTxnLimit) session.get(SwtUserTxnLimit.class, id);
                if (u != null) {
                    String newV = u.getId().getUserid() + "|"
                            + u.getId().getTxnLimitType() + "|"
                            + u.getUserLimit() + "|"
                            + u.getStatus();
                    audit.setNewvalue(newV);

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserid().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.DELETE_CUSTOMER_LIMIT);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public Date getLastLoginDateByUserID(String userid) throws Exception {
        Date lastLoginDate = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "select max(mh.timestamp) from SwtMobileUserHistory as mh where mh.swtMobileUser.id=:id";
            Query query = session.createQuery(sql);
            query.setString("id", userid);

            lastLoginDate = (Date) query.list().get(0);
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return lastLoginDate;
    }

    public SwtUserTxnLimit findLimitTypeById(CustomerSearchInputBean inputBean) throws Exception {
        SwtUserTxnLimit ct = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from SwtUserTxnLimit as u where u.id.userid=:userid and u.id.txnLimitType=:txnLimitType ";
            Query query = session.createQuery(sql).setString("userid", inputBean.getUserid()).setString("txnLimitType", inputBean.getTxntype());
            ct = (SwtUserTxnLimit) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return ct;

    }

    public String confirmCustomer(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                String oldVal = "";
                String newVal = "";

                if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_STATUS_CHANGE_TASK)) {

                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        String oldChargeAmount="";
                        if( u.getChargeAmount()!=null){
                            oldChargeAmount = u.getChargeAmount().toString();
                        }
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + u.getStatus().getStatuscode() + "|"
                                + u.getWaveoff().getStatuscode() + "|"
                                + oldChargeAmount;

                        Status statusNew = (Status) session.get(Status.class, penArray[1]);
                        u.setStatus(statusNew);

                        if (penArray[1].equalsIgnoreCase(CommonVarList.STATUS_CUS_ACTIVE)) {
                            u.setLoginAttempts(BigDecimal.ZERO);
                        }

                        Status waiveOff = (Status) session.get(Status.class, penArray[2]);
                        u.setWaveoff(waiveOff);

                        u.setChargeAmount(new BigDecimal(penArray[3]));

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        //----------audit new value---------------
                        newVal = penArray[0].trim() + "|"
                                + u.getStatus().getStatuscode() + "|"
                                + u.getWaveoff().getStatuscode() + "|"
                                + u.getChargeAmount().toString();

                        session.update(u);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUS_BLOCK_APPLICATIONS)) {

                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + u.getStatusByMbStatus().getStatuscode() + "|"
                                + u.getStatusByIbStatus().getStatuscode();

                        Status mobStatusNew = (Status) session.get(Status.class, penArray[1]);
                        u.setStatusByMbStatus(mobStatusNew);

                        Status ibStatusNew = (Status) session.get(Status.class, penArray[2]);
                        u.setStatusByIbStatus(ibStatusNew);

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        //----------audit new value---------------
                        newVal = penArray[0].trim() + "|"
                                + u.getStatusByMbStatus().getStatuscode() + "|"
                                + u.getStatusByIbStatus().getStatuscode();

                        session.update(u);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_UPDATE_TASK)) {
                    SwtUserDevice u = (SwtUserDevice) session.get(SwtUserDevice.class, new BigDecimal(penArray[1]));

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + penArray[1].trim() + "|"
                                + u.getStatus().getStatuscode();

                        Status statusNew = (Status) session.get(Status.class, penArray[2]);
                        u.setStatus(statusNew);

                        if (penArray[2].equalsIgnoreCase(CommonVarList.STATUS_DEACTIVE)) {
                            statusNew.setStatuscode(CommonVarList.STATUS_DEACTIVE);
                            u.setDeviceStatus(statusNew);
                        }

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastUpdatedTime(timestampDate);

                        //--------audit new value----------------
                        newVal = penArray[0].trim() + "|"
                                + penArray[1].trim() + "|"
                                + u.getStatus().getStatuscode();

                        session.update(u);

                        //for CID in audit record 
                        SwtMobileUser mobUser = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + mobUser.getCif() + ",Device ID : " + penArray[1] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_ATTEMPTS_RESET)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + u.getLoginAttempts();

                        u.setLoginAttempts(new BigDecimal(BigInteger.ZERO));

                        //-----------password reset start-------------
                        String generatedString = RandomStringUtils.randomAlphanumeric(6);
//                        System.out.println("Alphanumeric password : " + generatedString);
                        String hpassword = Sha256.Sha256(generatedString);

                        byte[] salt = ISOUtil.hex2byte(u.getUserKey());
                        String securePassword = getSecurePassword(hpassword, salt);

//                        System.out.println("pass: " + generatedString + " securePass: " + securePassword);

                        u.setMobilePin(securePassword);
                        u.setPasswordResetFlag("1");

                        this.insertAlert(u.getMobileNumber(), generatedString);
                        //-----------password reset end-------------

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        //--------audit new value----------------
                        newVal = penArray[0].trim() + "|"
                                + u.getLoginAttempts();

                        session.update(u);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + u.getDefaultAccNo();

                        u.setDefaultAccNo(penArray[1].trim());
                        u.setDefaultAccType(new BigDecimal(penArray[2]));
                        if (penArray[2].equals(CommonVarList.CUSTOMER_DEF_TYPE_ACC)) {//for account
                            u.setDefaultAccProduct(penArray[3]);
                            u.setNonDebitable("YES");

                            //clear card data
                            u.setUserToken("");
                        } else if (penArray[2].equals(CommonVarList.CUSTOMER_DEF_TYPE_CARD)) {//for card
                            u.setUserToken(penArray[3]);

                            //clear account data
                            u.setDefaultAccProduct("");
                            u.setNonDebitable("");
                        }
                        u.setFirstTimeLoginExpected(BigDecimal.ZERO);

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        //--------audit old value----------------
                        newVal = penArray[0].trim() + "|"
                                + u.getDefaultAccNo();

                        session.update(u);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_CUSTOMER_LIMIT)) {

                    SwtUserTxnLimitId id = new SwtUserTxnLimitId();

                    id.setUserid(penArray[0].trim());
                    id.setTxnLimitType(penArray[1].trim());

                    SwtUserTxnLimit limit = new SwtUserTxnLimit();

                    limit.setUserLimit(penArray[2].trim());
                    limit.setStatus(penArray[3].trim());

                    limit.setMaker(pentask.getCreateduser());
                    limit.setChecker(audit.getLastupdateduser());
                    limit.setCreatedTime(sysDate);
                    limit.setLastUpdatedTime(sysDate);
                    limit.setId(id);

                    session.save(limit);
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer :  " + limit.getId().getUserid() + " txn type : " + limit.getId().getTxnLimitType() + " " + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_CUSTOMER_LIMIT)) {
                    SwtUserTxnLimitId id = new SwtUserTxnLimitId();

                    id.setUserid(penArray[0].trim());
                    id.setTxnLimitType(penArray[1].trim());

                    SwtUserTxnLimit u = (SwtUserTxnLimit) session.get(SwtUserTxnLimit.class, id);

                    if (u != null) {

                        u.setUserLimit(penArray[2].trim());
                        u.setStatus(penArray[3].trim());

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastUpdatedTime(sysDate);

                        session.update(u);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer :  " + u.getId().getUserid() + " txn type : " + u.getId().getTxnLimitType() + " " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_CUSTOMER_LIMIT)) {
                    SwtUserTxnLimitId id = new SwtUserTxnLimitId();

                    id.setUserid(penArray[0].trim());
                    id.setTxnLimitType(penArray[1].trim());

                    SwtUserTxnLimit u = (SwtUserTxnLimit) session.get(SwtUserTxnLimit.class, id);

                    if (u != null) {
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer :  " + u.getId().getUserid() + " txn type : " + u.getId().getTxnLimitType() + " " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_PAS_RESET_TASK)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());
                    if (u != null) {

                        String generatedString = RandomStringUtils.randomAlphanumeric(6);
//                        System.out.println("Alphanumeric password : " + generatedString);
                        String hpassword = Sha256.Sha256(generatedString);
//                        Random rand = new Random();
//                        String password = String.format("%04d", rand.nextInt(10000));                        
                        byte[] salt = ISOUtil.hex2byte(u.getUserKey());
                        String securePassword = getSecurePassword(hpassword, salt);

//                        System.out.println("pass: " + generatedString + " securePass: " + securePassword);

                        u.setMobilePin(securePassword);
                        u.setPasswordResetFlag("1");

                        u.setLoginAttempts(new BigDecimal(BigInteger.ZERO));//attempts count reset

                        this.insertAlert(u.getMobileNumber(), generatedString);

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        session.update(u);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_DATA_UPDATE_TASK)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());
                    if (u != null) {
                        //--------audit old value----------------
                        StringBuilder oldValtemp = new StringBuilder("");
                        try {
                            oldValtemp.append(u.getCustomerName().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
//                        try {
//                            oldValtemp.append(u.getStatus().getStatuscode().toString());
//                        } catch (Exception e) {}
//                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getSegmentType().getSegmentcode().toString());
                        } catch (Exception e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getCustomerCategory().toString());
                        } catch (Exception e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getNic().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getDob().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getGender().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getMobileNumber().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getEmail().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getSecondaryMobile().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getSecondaryEmail().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");

//                        try {
//                            oldValtemp.append(u.getPermanentAddress().toString());
//                        } catch (NullPointerException e) {}
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getActofficer().toString());
                        } catch (NullPointerException e) {
                        }

                        oldVal = penArray[0].trim() + "|"
                                + oldValtemp.toString();
                        //--------audit old value----------------
                        newVal = pentask.getFields();

                        try {
                            if (penArray[1] != null && !penArray[1].trim().isEmpty()) {
                                u.setCustomerName(penArray[1].trim());
                            } else {
                                u.setCustomerName(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setCustomerName(null);
                        }
//                        try {
//                            if (penArray[2] != null && !penArray[2].trim().isEmpty()) {
//                                Status statusNew = (Status) session.get(Status.class, penArray[2]);
//                                u.setStatus(statusNew);
//                            } else {
//                                u.setStatus(null);
//                            }
//                        } catch (Exception e) {
//                            u.setStatus(null);
//                        }
                        try {
                            if (penArray[2] != null && !penArray[2].trim().isEmpty()) {
                                SegmentType segmentTypeNew = (SegmentType) session.get(SegmentType.class, penArray[2]);
                                u.setSegmentType(segmentTypeNew);
                            } else {
                                u.setSegmentType(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setSegmentType(null);
                        }
                        try {
                            if (penArray[3] != null && !penArray[3].trim().isEmpty()) {
                                u.setCustomerCategory(penArray[3].trim());
                            } else {
                                u.setCustomerCategory(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setCustomerCategory(null);
                        }
                        try {
                            if (penArray[4] != null && !penArray[4].trim().isEmpty()) {
                                u.setNic(penArray[4].trim());
                            } else {
                                u.setNic(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setNic(null);
                        }
                        try {
                            if (penArray[5] != null && !penArray[5].trim().isEmpty()) {
                                u.setDob(penArray[5].trim());
                            } else {
                                u.setDob(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setDob(null);
                        }
                        try {
                            if (penArray[6] != null && !penArray[6].trim().isEmpty()) {
                                u.setGender(penArray[6].trim());
                            } else {
                                u.setGender(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setGender(null);
                        }
                        try {
                            if (penArray[7] != null && !penArray[7].trim().isEmpty()) {
                                u.setMobileNumber(penArray[7].trim());
                            } else {
                                u.setMobileNumber(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setMobileNumber(null);
                        }
                        try {
                            if (penArray[8] != null && !penArray[8].trim().isEmpty()) {
                                u.setEmail(penArray[8].trim());
                            } else {
                                u.setEmail(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setEmail(null);
                        }
                        try {
                            if (penArray[9] != null && !penArray[9].trim().isEmpty()) {
                                u.setSecondaryMobile(penArray[9].trim());
                            } else {
                                u.setSecondaryMobile(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setSecondaryMobile(null);
                        }
                        try {
                            if (penArray[10] != null && !penArray[10].trim().isEmpty()) {
                                u.setSecondaryEmail(penArray[10].trim());
                            } else {
                                u.setSecondaryEmail(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setSecondaryEmail(null);
                        }
//                        try {
//                            if (penArray[11] != null && !penArray[11].trim().isEmpty()) {
//                                u.setPermanentAddress(penArray[11].trim());
//                            } else {
//                                u.setPermanentAddress(null);
//                            }
//                        } catch (IndexOutOfBoundsException e) {
//                            u.setPermanentAddress(null);
//                        }
                        try {
                            if (penArray[12] != null && !penArray[12].trim().isEmpty()) {
                                u.setActofficer(penArray[12].trim());
                            } else {
                                u.setActofficer(null);
                            }
                        } catch (IndexOutOfBoundsException e) {
                            u.setActofficer(null);
                        }

                        session.update(u);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }
                audit.setOldvalue(oldVal);
                audit.setNewvalue(newVal);

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejecteCustomer(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask pentask = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());
                    penArray = pentask.getFields().split("\\|");
                }

                //---------------------audit old value---------------------
                String oldVal = "";
                if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_STATUS_CHANGE_TASK)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + u.getStatus().getStatuscode() + "|"
                                + u.getWaveoff().getStatuscode() + "|"
                                + u.getChargeAmount().toString();

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUS_BLOCK_APPLICATIONS)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + u.getStatusByMbStatus().getStatuscode() + "|"
                                + u.getStatusByIbStatus().getStatuscode();

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_UPDATE_TASK)) {
                    SwtUserDevice u = (SwtUserDevice) session.get(SwtUserDevice.class, new BigDecimal(penArray[1]));

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + penArray[1].trim() + "|"
                                + u.getStatus().getStatuscode();

                        audit.setOldvalue(oldVal);

                        //for CID in audit record 
                        SwtMobileUser mobUser = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                        audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + mobUser.getCif() + ",Device ID : " + penArray[1] + ")  inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_ATTEMPTS_RESET)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + u.getLoginAttempts();

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim() + "|"
                                + u.getDefaultAccNo();

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_PAS_RESET_TASK)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        oldVal = penArray[0].trim();

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_DATA_UPDATE_TASK)) {
                    SwtMobileUser u = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());

                    if (u != null) {
                        //--------audit old value----------------
                        StringBuilder oldValtemp = new StringBuilder("");
                        try {
                            oldValtemp.append(u.getCustomerName().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
//                        try {
//                            oldValtemp.append(u.getStatus().getStatuscode().toString());
//                        } catch (Exception e) {}
//                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getSegmentType().getSegmentcode().toString());
                        } catch (Exception e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getCustomerCategory().toString());
                        } catch (Exception e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getNic().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getDob().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getGender().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getMobileNumber().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getEmail().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getSecondaryMobile().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getSecondaryEmail().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getPermanentAddress().toString());
                        } catch (NullPointerException e) {
                        }
                        oldValtemp.append("|");
                        try {
                            oldValtemp.append(u.getActofficer().toString());
                        } catch (NullPointerException e) {
                        }

                        oldVal = penArray[0].trim() + "|"
                                + oldValtemp.toString();

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on customer search( CID : " + u.getCif() + ")  inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else {
                    audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on customer " + "inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                }
//                String userId = u.getPKey();
//                if (userId != null && !userId.isEmpty()) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on customer : " + userId + " " + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on customer " + audit.getDescription());
//                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(pentask);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<CustomerSearchBean> getDeviceList(CustomerSearchInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<CustomerSearchBean> dataList = new ArrayList<CustomerSearchBean>();
        Session session = null;
        SwtUserDevice swtuserdevice = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = " order by u.createdTime desc ";
            }

            long count = 0;
            session = HibernateInit.sessionFactory.openSession();

            if (inputBean.getUserid() != null) {
                String sqlCount = "select count(u.swtMobileUser.id) from SwtUserDevice as u where u.swtMobileUser.id=:userid";
                Query queryCount = session.createQuery(sqlCount).setString("userid", inputBean.getUserid());
                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();
            }

            if (count > 0) {

                String sqlSearch = "from SwtUserDevice as u where u.swtMobileUser.id=:userid" + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("userid", inputBean.getUserid());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    CustomerSearchBean bean = new CustomerSearchBean();
                    swtuserdevice = (SwtUserDevice) it.next();

                    try {
                        bean.setDeviceuserid(swtuserdevice.getSwtMobileUser().getId());
                    } catch (NullPointerException npe) {
                        bean.setDeviceuserid("--");
                    }
                    try {
                        bean.setDeviceid(swtuserdevice.getId().toString());
                    } catch (NullPointerException npe) {
                        bean.setDeviceid("--");
                    }
                    try {
                        bean.setDeviceNickName(swtuserdevice.getDeviceNickName().toString());
                    } catch (NullPointerException npe) {
                        bean.setDeviceNickName("--");
                    }
                    try {
                        bean.setDeviceBuildNumber(swtuserdevice.getDeviceBuildNumber().toString());
                    } catch (NullPointerException npe) {
                        bean.setDeviceBuildNumber("--");
                    }
                    try {
                        bean.setDeviceManufacture(swtuserdevice.getDeviceManufacture().toString());
                    } catch (NullPointerException npe) {
                        bean.setDeviceManufacture("--");
                    }
                    try {
                        bean.setDeviceModel(swtuserdevice.getDeviceModel().toString());
                    } catch (NullPointerException npe) {
                        bean.setDeviceModel("--");
                    }
                    try {
                        bean.setDeviceOsVersion(swtuserdevice.getDeviceOsVersion().toString());
                    } catch (NullPointerException npe) {
                        bean.setDeviceOsVersion("--");
                    }
                    try {
                        bean.setDeviceimie(swtuserdevice.getImie().toString());
                    } catch (NullPointerException npe) {
                        bean.setDeviceimie("--");
                    }
                    try {
                        bean.setDevicelastUpdatedTime(swtuserdevice.getLastUpdatedTime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        bean.setDevicelastUpdatedTime("--");
                    }
                    try {
                        bean.setCreatedtime(swtuserdevice.getCreatedTime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        bean.setCreatedtime("--");
                    }
                    try {
                        bean.setDevicepushId(swtuserdevice.getPushId().toString());
                    } catch (NullPointerException npe) {
                        bean.setDevicepushId("--");
                    }
                    try {
                        bean.setDevicepushSha(swtuserdevice.getPushSha().toString());
                    } catch (NullPointerException npe) {
                        bean.setDevicepushSha("--");
                    }
                    try {
                        bean.setDevicestatus(swtuserdevice.getStatus().getStatuscode());
                        bean.setDevicestatusDes(swtuserdevice.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        bean.setDevicestatus("--");
                        bean.setDevicestatusDes("--");
                    }
                    try {
                        bean.setChecker(swtuserdevice.getChecker().toString());
                    } catch (NullPointerException npe) {
                        bean.setChecker("--");
                    }
                    try {
                        bean.setMaker(swtuserdevice.getMaker().toString());
                    } catch (NullPointerException npe) {
                        bean.setMaker("--");
                    }
                    bean.setFullCount(count);
                    dataList.add(bean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public SwtUserDevice getDeviceByDeviceID(String userid) throws Exception {
        SwtUserDevice device = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtUserDevice as mb where mb.id=:id";
            Query query = session.createQuery(sql);
            query.setBigDecimal("id", new BigDecimal(userid));

            device = (SwtUserDevice) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return device;
    }

    public String updateDeviceStatus(CustomerSearchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUserid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                SwtUserDevice u = (SwtUserDevice) session.get(SwtUserDevice.class, new BigDecimal(inputBean.getDeviceid()));

                if (u != null) {

                    String oldV = inputBean.getUserid() + "|"
                            + u.getId() + "|"
                            + u.getStatus().getStatuscode();

                    audit.setOldvalue(oldV);

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserid().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.CUSTOMER_UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_SEARCH);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public StringBuffer makeCSVReport(CustomerSearchInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;

        String TXN_SQL_CSV = "SELECT "
                + "U.ID, "//0
                + "U.NIC, "//1
                + "U.DOB, "//2
                + "U.CIF, "//3
                + "U.USERNAME, "//4
                + "U.EMAIL, "//5
                + "U.MOBILE_NUMBER, "//6  
                + "U.LOGIN_ATTEMPTS, "//7   
                + "U.LASTUPDATEDTIME, "//8
                + "U.CREATEDTIME, "//9   
                + " U.STATUS STATUS, "//10
                + "S.DESCRIPTION STATUSDES, "//11
                + "SE.DESCRIPTION SEGMENT, "//12
                + "U.PROFILE_IMAGE, "//13
                + "U.PROF_IMAGE_TIMESTAMP, "//14
                + "U.OTP, "//15
                + "U.OTP_EXP_TIME, "//16
                + "U.MOBILE_PIN , "//17
                + "U.USER_TRACE_NUMBER, "//18
                + "U.SECONDARY_EMAIL, "//19
                + "U.SECONDARY_MOBILE, "//20
                + "U.REMARK, "//21
                + "U.MAKER, "//22
                + "U.CHECKER, "//23
                + "U.CUSTOMER_NAME, "//24
                + "U.LAST_LOGGEDIN_DATETIME, "//25
                + "U.ON_BOARD_CHANNEL, "//26
                + "U.ON_BOARD_TYPE, "//27
                + "U.CUSTOMER_CATEGORY, "//28
                + "U.DEFAULT_ACC_TYPE, "//29
                + "U.DEFAULT_ACC_NO, "//30
                + "U.FIRST_TIME_LOGIN_EXPECTED, "//31
                + "U.LAST_PASSWORD_UPDATED_DATE, "//32
                + "U.FINAL_FEE_DED_DAY, "//33
                + "W.DESCRIPTION WAIVEOFFDES, "//34
                + "U.CHARGE_AMOUNT, "//35
                + "U.ACTOFFICER, "//36
                + "U.PERMANENT_ADDRESS  "//37
                //                + "U.DEFAULT_CARD_NO "//37

                + "FROM SWT_MOBILE_USER U "
                + "LEFT OUTER JOIN STATUS S ON S.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN STATUS W ON W.STATUSCODE = U.WAIVEOFF "
                + "LEFT OUTER JOIN SEGMENT_TYPE SE ON SE.SEGMENTCODE = U.USER_SEGMENT "
                + "WHERE ";

        try {
            session = HibernateInit.sessionFactory.openSession();
            int count = 0;
            String where1 = this.makeWhereClauseForCSV(inputBean);
            String sqlCount = this.CUSTOMER_COUNT_SQL + where1;
//            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = TXN_SQL_CSV + where1+ this.CUSTOMER_ORDER_BY_SQL;

                Query query = session.createSQLQuery(sql);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    content = new StringBuffer();
                    List<CustomerSearchBean> beanlist = new ArrayList<CustomerSearchBean>();

                    for (Object[] objArr : objectArrList) {

                        CustomerSearchBean searchBean = new CustomerSearchBean();

                        try {
                            searchBean.setUserid(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserid("--");
                        }

                        try {
                            searchBean.setNic(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setNic("--");
                        }
                        try {
                            searchBean.setDob(objArr[2].toString());
                        } catch (Exception e) {
                            searchBean.setDob("--");
                        }
                        try {
                            searchBean.setCif(objArr[3].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUsername(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUsername("--");
                        }
                        try {
                            searchBean.setEmail(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setEmail("--");
                        }
                        try {
                            searchBean.setMobileNumber(objArr[6].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNumber("--");
                        }
                        try {
                            searchBean.setLoginAttempts(objArr[7].toString());
                            searchBean.setLoginAttemptStatus(this.getUserBlocktatus(session, objArr[7].toString()));
                        } catch (NullPointerException e) {
                            searchBean.setLoginAttempts("--");
                            searchBean.setLoginAttemptStatus("--");
                        }
                        try {
                            searchBean.setLastupdatedtime(objArr[8].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastupdatedtime("--");
                        }
                        try {
                            searchBean.setCreatedtime(objArr[9].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setCreatedtime("--");
                        }
                        try {
                            searchBean.setStatus(objArr[10].toString());
                        } catch (NullPointerException e) {
                            searchBean.setStatus("--");
                        }
//                        try {
//                            searchBean.setStatusDes(objArr[11].toString());
//                        } catch (NullPointerException e) {
//                            searchBean.setStatusDes("--");
//                        }
                        try {
                            if ((objArr[4] == null || objArr[4].toString().isEmpty()) && objArr[10].toString().equals(CommonVarList.STATUS_ACTIVE)) {
                                searchBean.setStatusDes(CommonVarList.CUSTOMER_PEND_STATUS_DES);
                            } else {
                                searchBean.setStatusDes(objArr[11].toString());
                            }
                        } catch (NullPointerException e) {
                            searchBean.setStatusDes("--");
                        }
                        try {
                            searchBean.setPromotionMtSegmentType(objArr[12].toString());
                        } catch (Exception e) {
                            searchBean.setPromotionMtSegmentType("--");
                        }
                        try {
                            searchBean.setOtp(objArr[15].toString());
                        } catch (NullPointerException e) {
                            searchBean.setOtp("--");
                        }
                        try {
                            searchBean.setOtpExpTime(objArr[16].toString());
                        } catch (NullPointerException e) {
                            searchBean.setOtpExpTime("--");
                        }
                        try {
                            searchBean.setMobilePin(objArr[17].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobilePin("--");
                        }
                        try {
                            searchBean.setUserTraceNumber(objArr[18].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserTraceNumber("--");
                        }
                        try {
                            searchBean.setSecondaryEmail(objArr[19].toString());
                        } catch (NullPointerException e) {
                            searchBean.setSecondaryEmail("--");
                        }
                        try {
                            searchBean.setSecondaryMobile(objArr[20].toString());
                        } catch (NullPointerException e) {
                            searchBean.setSecondaryMobile("--");
                        }
                        try {
                            searchBean.setRemark(objArr[21].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRemark("--");
                        }
                        try {
                            searchBean.setMaker(objArr[22].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMaker("--");
                        }
                        try {
                            searchBean.setChecker(objArr[23].toString());
                        } catch (NullPointerException e) {
                            searchBean.setChecker("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[24].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setLastLoggedinDatetime(objArr[25].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastLoggedinDatetime("--");
                        }
                        try {
                            if (objArr[26] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[26].toString());
                                searchBean.setOnBoardChannel(channelTypeDes);
                            } else {
                                searchBean.setOnBoardChannel("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardChannel("--");
                        }
                        try {
                            if (objArr[27] != null) {
                                String onBordedTypeDes = this.getOnBordedTypeDesByCode(objArr[27].toString());
                                searchBean.setOnBoardType(onBordedTypeDes);
                            } else {
                                searchBean.setOnBoardType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardType("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[28].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            if (objArr[29] != null) {
                                String onDefaultTypeDes = this.getDefaultTypeDesByCode(objArr[29].toString());
                                searchBean.setDefType(onDefaultTypeDes);
                            } else {
                                searchBean.setDefType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setDefType("--");
                        }
                        try {
                            searchBean.setDefAccNo(objArr[30].toString());
                        } catch (NullPointerException e) {
                            searchBean.setDefAccNo("--");
                        }
                        try {
                            if (objArr[31] != null) {
                                String loginStatusDes = this.getLoginStatusDesByCode(objArr[31].toString());
                                searchBean.setLoginStatus(loginStatusDes);
                            } else {
                                searchBean.setLoginStatus("--");
                            }
                        } catch (Exception e) {
                            searchBean.setLoginStatus("--");
                        }
                        try {
                            searchBean.setLastPassUpdateTime(objArr[32].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastPassUpdateTime("--");
                        }
                        try {
                            searchBean.setFinalFeeDudDay(objArr[33].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setFinalFeeDudDay("--");
                        }
                        try {
                            searchBean.setWaveoff(objArr[34].toString());
                        } catch (NullPointerException e) {
                            searchBean.setWaveoff("--");
                        }
                        try {
                            searchBean.setChargeAmount(objArr[35].toString());
                        } catch (NullPointerException e) {
                            searchBean.setChargeAmount("--");
                        }
                        try {
                            searchBean.setAccountofficer(objArr[36].toString());
                        } catch (NullPointerException e) {
                            searchBean.setAccountofficer("--");
                        }
                        try {
                            searchBean.setPermanentaddress(objArr[37].toString());
                        } catch (NullPointerException e) {
                            searchBean.setPermanentaddress("--");
                        }

                        beanlist.add(searchBean);
                    }

                    //write column headers to csv file
                    content.append("No");
                    content.append(',');
                    content.append("Unique ID");
                    content.append(',');
                    content.append("CID");
                    content.append(',');
                    content.append("User Name");
                    content.append(',');
                    content.append("User Segment");
                    content.append(',');
                    content.append("On Board Channel");
                    content.append(',');
                    content.append("On Board Type");
                    content.append(',');
                    content.append("Customer Category");
                    content.append(',');
                    content.append("Status");
                    content.append(',');
                    content.append("Waive Off");
                    content.append(',');
//                    content.append("Charge Amount");
//                    content.append(',');
                    content.append("Login Status");
                    content.append(',');
                    content.append("Primary Account Type");
                    content.append(',');
                    content.append("Primary Acc/Card Number");
                    content.append(',');
                    content.append("Customer Name");
                    content.append(',');
                    content.append("NIC");
                    content.append(',');
                    content.append("DOB");
                    content.append(',');
                    content.append("Mobile Number");
                    content.append(',');
                    content.append("Mobile Number(Secondary)");
                    content.append(',');
                    content.append("Email");
                    content.append(',');
                    content.append("Email(Secondary)");
                    content.append(',');
//                    content.append("Address");
//                    content.append(',');
                    content.append("Block Status");
                    content.append(',');
                    content.append("Invalid Login Attempts");
                    content.append(',');
                    content.append("Last Logged In Date And Time");
                    content.append(',');
                    content.append("Last Password Updated Date And Time");
                    content.append(',');
//                    content.append("Final Fee Deducted Date");
//                    content.append(',');
                    content.append("Account Officer");
                    content.append(',');
                    content.append("Remark");
                    content.append(',');
                    content.append("Maker");
                    content.append(',');
                    content.append("Checker");
                    content.append(',');
                    content.append("Created Date And Time");
                    content.append(',');
                    content.append("Last Updated Date And Time");

                    content.append('\n');

                    //write data values to csv file
                    int i = 1;
                    for (CustomerSearchBean dataBean : beanlist) {

                        content.append(i++);
                        content.append(',');

                        try {
                            if (dataBean.getUserid() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getUserid());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getCif() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCif());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getUsername() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getUsername()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getPromotionMtSegmentType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getPromotionMtSegmentType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getOnBoardChannel() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getOnBoardChannel());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getOnBoardType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getOnBoardType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerCategory() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getStatusDes() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getStatusDes());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getWaveoff() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getWaveoff());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getChargeAmount() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getChargeAmount());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getLoginStatus() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLoginStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDefType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDefType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDefAccNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDefAccNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getNic() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getNic());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getDob() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDob());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getMobileNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMobileNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getSecondaryMobile() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getSecondaryMobile());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getEmail() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getEmail());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getSecondaryEmail() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getSecondaryEmail());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getPermanentaddress()== null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getPermanentaddress());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getLoginAttemptStatus() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLoginAttemptStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getLoginAttempts() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLoginAttempts());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getLastLoggedinDatetime() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLastLoggedinDatetime());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getLastPassUpdateTime() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLastPassUpdateTime());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
//                        try {
//                            if (dataBean.getFinalFeeDudDay() == null) {
//                                content.append("--");
//                                content.append(',');
//                            } else {
//                                content.append(dataBean.getFinalFeeDudDay());
//                                content.append(',');
//                            }
//                        } catch (NullPointerException npe) {
//                            content.append("--");
//                            content.append(',');
//                        }
                        try {
                            if (dataBean.getAccountofficer() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getAccountofficer());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getRemark() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(dataBean.getRemark()));
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getMaker() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMaker());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getChecker() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getChecker());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCreatedtime() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCreatedtime());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getLastupdatedtime() == null) {
                                content.append("--");
//                                content.append(',');
                            } else {
                                content.append(dataBean.getLastupdatedtime());
//                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
//                            content.append(',');
                        }

                        content.append('\n');
                    }
                    content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
                    content.append('\n');

                    content.append("CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCifSearch()));
                    content.append('\n');

                    content.append("User Name :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getUsernameSearch()));
                    content.append('\n');

                    content.append("User Segment :");
                    if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
                        SegmentType segmenttype = (SegmentType) session.get(SegmentType.class, inputBean.getSegmentTypeSearch());
                        content.append(Common.replaceEmptyorNullStringToALL(segmenttype.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getSegmentTypeSearch()));
                    }
                    content.append('\n');

                    content.append("On Board Channel :");
                    String onBoardChannel = Common.replaceEmptyorNullStringToALL(inputBean.getOnBoardChannelSearch());
                    content.append(this.getChannelTypeDesByCode(onBoardChannel));
                    content.append('\n');

                    content.append("On Board Channel :");
                    String onBoardType = Common.replaceEmptyorNullStringToALL(inputBean.getOnBoardTypeSearch());
                    content.append(this.getOnBordedTypeDesByCode(onBoardType));
                    content.append('\n');

                    content.append("Customer Category :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategorySearch()));
                    content.append('\n');

                    content.append("Status :");
                    if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
                        Status status = (Status) session.get(Status.class, inputBean.getStatusSearch());
                        content.append(Common.replaceEmptyorNullStringToALL(status.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getStatusSearch()));
                    }
                    content.append('\n');

                    content.append("NIC :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getNicSearch()));
                    content.append('\n');

                    content.append("Mobile :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getMobilenoSearch()));
                    content.append('\n');

                    content.append("Email :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getEmailSearch()));
                    content.append('\n');

                    content.append("Waive Off :");
                    if (inputBean.getWaveoffSearch() != null && !inputBean.getWaveoffSearch().isEmpty()) {
                        Status waveOffStatus = (Status) session.get(Status.class, inputBean.getWaveoffSearch());
                        content.append(Common.replaceEmptyorNullStringToALL(waveOffStatus.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getWaveoffSearch()));
                    }
                    content.append('\n');

                    content.append("Primary Account Type :");
                    String defaultType = Common.replaceEmptyorNullStringToALL(inputBean.getDefTypeSearch());
                    content.append(this.getDefaultTypeDesByCode(defaultType));content.append('\n');

                    content.append("Primary Acc/Card Number :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getDefAccNoSearch()));
                    content.append('\n');

                    content.append('\n');
                    content.append('\n');
                    content.append("Summary");
                    content.append('\n');
                    content.append("Total Record Count :");
                    content.append(i - 1);
                    content.append('\n');

                    String currentDate = "";
                    Date createdTime = CommonDAO.getSystemDate(session);

                    content.append("Report Created Time and Date :");
                    content.append(createdTime.toString().substring(0, 19));
                    content.append('\n');
                }

            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    public StringBuffer makeFeeReport(CustomerSearchInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;

        String TXN_SQL_CSV = "SELECT "
                + "U.ID, "//0
                + "U.NIC, "//1
                + "U.DOB, "//2
                + "U.CIF, "//3
                + "U.USERNAME, "//4
                + "U.EMAIL, "//5
                + "U.MOBILE_NUMBER, "//6  
                + "U.LOGIN_ATTEMPTS, "//7   
                + "U.LASTUPDATEDTIME, "//8
                + "U.CREATEDTIME, "//9   
                + " U.STATUS STATUS, "//10
                + "S.DESCRIPTION STATUSDES, "//11
                + "SE.DESCRIPTION SEGMENT, "//12
                + "U.PROFILE_IMAGE, "//13
                + "U.PROF_IMAGE_TIMESTAMP, "//14
                + "U.OTP, "//15
                + "U.OTP_EXP_TIME, "//16
                + "U.MOBILE_PIN , "//17
                + "U.USER_TRACE_NUMBER, "//18
                + "U.SECONDARY_EMAIL, "//19
                + "U.SECONDARY_MOBILE, "//20
                + "U.REMARK, "//21
                + "U.MAKER, "//22
                + "U.CHECKER, "//23
                + "U.CUSTOMER_NAME, "//24
                + "U.LAST_LOGGEDIN_DATETIME, "//25
                + "U.ON_BOARD_CHANNEL, "//26
                + "U.ON_BOARD_TYPE, "//27
                + "U.CUSTOMER_CATEGORY, "//28
                + "U.DEFAULT_ACC_TYPE, "//29
                + "U.DEFAULT_ACC_NO, "//30
                + "U.FIRST_TIME_LOGIN_EXPECTED, "//31
                + "U.LAST_PASSWORD_UPDATED_DATE, "//32
                + "U.FINAL_FEE_DED_DAY, "//33
                + "W.DESCRIPTION WAIVEOFFDES, "//34
                + "U.CHARGE_AMOUNT, "//35
                + "U.ACTOFFICER "//36
                //                + "U.DEFAULT_CARD_NO "//37

                + "FROM SWT_MOBILE_USER U "
                + "LEFT OUTER JOIN STATUS S ON S.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN STATUS W ON W.STATUSCODE = U.WAIVEOFF "
                + "LEFT OUTER JOIN SEGMENT_TYPE SE ON SE.SEGMENTCODE = U.USER_SEGMENT "
                + "WHERE ";

        try {
            session = HibernateInit.sessionFactory.openSession();
            int count = 0;
            String where1 = this.makeWhereClauseForCSV(inputBean);
            String sqlCount = this.CUSTOMER_COUNT_SQL + where1;
//            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);

            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }

            if (count > 0) {

                String sql = TXN_SQL_CSV + where1;

                Query query = session.createSQLQuery(sql);

                List<Object[]> objectArrList = (List<Object[]>) query.list();
                if (objectArrList.size() > 0) {

                    content = new StringBuffer();
                    List<CustomerSearchBean> beanlist = new ArrayList<CustomerSearchBean>();

                    for (Object[] objArr : objectArrList) {

                        CustomerSearchBean searchBean = new CustomerSearchBean();

                        try {
                            searchBean.setUserid(objArr[0].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserid("--");
                        }

                        try {
                            searchBean.setNic(objArr[1].toString());
                        } catch (NullPointerException e) {
                            searchBean.setNic("--");
                        }
                        try {
                            searchBean.setDob(objArr[2].toString());
                        } catch (Exception e) {
                            searchBean.setDob("--");
                        }
                        try {
                            searchBean.setCif(objArr[3].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCif("--");
                        }
                        try {
                            searchBean.setUsername(objArr[4].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUsername("--");
                        }
                        try {
                            searchBean.setEmail(objArr[5].toString());
                        } catch (NullPointerException e) {
                            searchBean.setEmail("--");
                        }
                        try {
                            searchBean.setMobileNumber(objArr[6].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobileNumber("--");
                        }
                        try {
                            searchBean.setLoginAttempts(objArr[7].toString());
                            searchBean.setLoginAttemptStatus(this.getUserBlocktatus(session, objArr[7].toString()));
                        } catch (NullPointerException e) {
                            searchBean.setLoginAttempts("--");
                            searchBean.setLoginAttemptStatus("--");
                        }
                        try {
                            searchBean.setLastupdatedtime(objArr[8].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastupdatedtime("--");
                        }
                        try {
                            searchBean.setCreatedtime(objArr[9].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setCreatedtime("--");
                        }
                        try {
                            searchBean.setStatus(objArr[10].toString());
                        } catch (NullPointerException e) {
                            searchBean.setStatus("--");
                        }
//                        try {
//                            searchBean.setStatusDes(objArr[11].toString());
//                        } catch (NullPointerException e) {
//                            searchBean.setStatusDes("--");
//                        }
                        try {
                            if ((objArr[4] == null || objArr[4].toString().isEmpty()) && objArr[10].toString().equals(CommonVarList.STATUS_ACTIVE)) {
                                searchBean.setStatusDes(CommonVarList.CUSTOMER_PEND_STATUS_DES);
                            } else {
                                searchBean.setStatusDes(objArr[11].toString());
                            }
                        } catch (NullPointerException e) {
                            searchBean.setStatusDes("--");
                        }
                        try {
                            searchBean.setPromotionMtSegmentType(objArr[12].toString());
                        } catch (Exception e) {
                            searchBean.setPromotionMtSegmentType("--");
                        }
                        try {
                            searchBean.setOtp(objArr[15].toString());
                        } catch (NullPointerException e) {
                            searchBean.setOtp("--");
                        }
                        try {
                            searchBean.setOtpExpTime(objArr[16].toString());
                        } catch (NullPointerException e) {
                            searchBean.setOtpExpTime("--");
                        }
                        try {
                            searchBean.setMobilePin(objArr[17].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMobilePin("--");
                        }
                        try {
                            searchBean.setUserTraceNumber(objArr[18].toString());
                        } catch (NullPointerException e) {
                            searchBean.setUserTraceNumber("--");
                        }
                        try {
                            searchBean.setSecondaryEmail(objArr[19].toString());
                        } catch (NullPointerException e) {
                            searchBean.setSecondaryEmail("--");
                        }
                        try {
                            searchBean.setSecondaryMobile(objArr[20].toString());
                        } catch (NullPointerException e) {
                            searchBean.setSecondaryMobile("--");
                        }
                        try {
                            searchBean.setRemark(objArr[21].toString());
                        } catch (NullPointerException e) {
                            searchBean.setRemark("--");
                        }
                        try {
                            searchBean.setMaker(objArr[22].toString());
                        } catch (NullPointerException e) {
                            searchBean.setMaker("--");
                        }
                        try {
                            searchBean.setChecker(objArr[23].toString());
                        } catch (NullPointerException e) {
                            searchBean.setChecker("--");
                        }
                        try {
                            searchBean.setCustomerName(objArr[24].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerName("--");
                        }
                        try {
                            searchBean.setLastLoggedinDatetime(objArr[25].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastLoggedinDatetime("--");
                        }
                        try {
                            if (objArr[26] != null) {
                                String channelTypeDes = this.getChannelTypeDesByCode(objArr[26].toString());
                                searchBean.setOnBoardChannel(channelTypeDes);
                            } else {
                                searchBean.setOnBoardChannel("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardChannel("--");
                        }
                        try {
                            if (objArr[27] != null) {
                                String onBordedTypeDes = this.getOnBordedTypeDesByCode(objArr[27].toString());
                                searchBean.setOnBoardType(onBordedTypeDes);
                            } else {
                                searchBean.setOnBoardType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setOnBoardType("--");
                        }
                        try {
                            searchBean.setCustomerCategory(objArr[28].toString());
                        } catch (NullPointerException e) {
                            searchBean.setCustomerCategory("--");
                        }
                        try {
                            if (objArr[29] != null) {
                                String onDefaultTypeDes = this.getDefaultTypeDesByCode(objArr[29].toString());
                                searchBean.setDefType(onDefaultTypeDes);
                            } else {
                                searchBean.setDefType("--");
                            }
                        } catch (Exception e) {
                            searchBean.setDefType("--");
                        }
                        try {
                            searchBean.setDefAccNo(objArr[30].toString());
                        } catch (NullPointerException e) {
                            searchBean.setDefAccNo("--");
                        }
                        try {
                            if (objArr[31] != null) {
                                String loginStatusDes = this.getLoginStatusDesByCode(objArr[31].toString());
                                searchBean.setLoginStatus(loginStatusDes);
                            } else {
                                searchBean.setLoginStatus("--");
                            }
                        } catch (Exception e) {
                            searchBean.setLoginStatus("--");
                        }
                        try {
                            searchBean.setLastPassUpdateTime(objArr[32].toString().substring(0, 19));
                        } catch (Exception e) {
                            searchBean.setLastPassUpdateTime("--");
                        }
                        try {
                            searchBean.setFinalFeeDudDay(objArr[33].toString());
                        } catch (NullPointerException e) {
                            searchBean.setFinalFeeDudDay("--");
                        }
                        try {
                            searchBean.setWaveoff(objArr[34].toString());
                        } catch (NullPointerException e) {
                            searchBean.setWaveoff("--");
                        }
                        try {
                            searchBean.setChargeAmount(objArr[35].toString());
                        } catch (NullPointerException e) {
                            searchBean.setChargeAmount("--");
                        }
                        try {
                            searchBean.setAccountofficer(objArr[36].toString());
                        } catch (NullPointerException e) {
                            searchBean.setAccountofficer("--");
                        }

                        beanlist.add(searchBean);
                    }

                    //write column headers to csv file
                    content.append("Unique ID");
                    content.append(',');
                    content.append("CID");
                    content.append(',');
                    content.append("User Name");
                    content.append(',');
                    content.append("User Segment");
                    content.append(',');
                    content.append("Customer Name");
                    content.append(',');
                    content.append("Mobile Number");
                    content.append(',');
                    content.append("On Board Channel");
                    content.append(',');
                    content.append("On Board Type");
                    content.append(',');
                    content.append("Status");
                    content.append(',');
                    content.append("Login Status");
                    content.append(',');
                    content.append("Customer Category");
                    content.append(',');
                    content.append("Primary Account Type");
                    content.append(',');
                    content.append("Primary Acc/Card Number");
                    content.append(',');
                    content.append("Waive Off");
                    content.append(',');
                    content.append("Charge Amount");
                    content.append(',');
                    content.append("Fee Cycle Start Date");
                    content.append(',');
                    content.append("Created Date And Time");

                    content.append('\n');

                    //write data values to csv file
                    for (CustomerSearchBean dataBean : beanlist) {
                        try {
                            if (dataBean.getUserid() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getUserid());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getCif() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCif());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (dataBean.getUsername() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getUsername());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getPromotionMtSegmentType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getPromotionMtSegmentType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerName() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerName());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getMobileNumber() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getMobileNumber());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getOnBoardChannel() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getOnBoardChannel());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getOnBoardType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getOnBoardType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getStatusDes() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getStatusDes());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getLoginStatus() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getLoginStatus());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCustomerCategory() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getCustomerCategory());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDefType() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDefType());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getDefAccNo() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getDefAccNo());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getWaveoff() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getWaveoff());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getChargeAmount() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getChargeAmount());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getFinalFeeDudDay() == null) {
                                content.append("--");
                                content.append(',');
                            } else {
                                content.append(dataBean.getFinalFeeDudDay());
                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                        try {
                            if (dataBean.getCreatedtime() == null) {
                                content.append("--");
//                                content.append(',');
                            } else {
                                content.append(dataBean.getCreatedtime());
//                                content.append(',');
                            }
                        } catch (NullPointerException npe) {
                            content.append("--");
//                            content.append(',');
                        }
                        content.append('\n');
                    }
                    content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
                    content.append('\n');

                    content.append("CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCifSearch()));
                    content.append('\n');

                    content.append("User Name :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getUsernameSearch()));
                    content.append('\n');

                    content.append("User Segment :");
                    if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
                        SegmentType segmenttype = (SegmentType) session.get(SegmentType.class, inputBean.getSegmentTypeSearch());
                        content.append(Common.replaceEmptyorNullStringToALL(segmenttype.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getSegmentTypeSearch()));
                    }
                    content.append('\n');

                    content.append("On Board Channel :");
                    String onBoardChannel = Common.replaceEmptyorNullStringToALL(inputBean.getOnBoardChannelSearch());
                    content.append(this.getChannelTypeDesByCode(onBoardChannel));
                    content.append('\n');

                    content.append("On Board Channel :");
                    String onBoardType = Common.replaceEmptyorNullStringToALL(inputBean.getOnBoardTypeSearch());
                    content.append(this.getOnBordedTypeDesByCode(onBoardType));
                    content.append('\n');

                    content.append("Customer Category :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategorySearch()));
                    content.append('\n');

                    content.append("Status :");
                    if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
                        Status status = (Status) session.get(Status.class, inputBean.getStatusSearch());
                        content.append(Common.replaceEmptyorNullStringToALL(status.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getStatusSearch()));
                    }
                    content.append('\n');

                    content.append("NIC :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getNicSearch()));
                    content.append('\n');

                    content.append("Mobile :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getMobilenoSearch()));
                    content.append('\n');

                    content.append("Email :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getEmailSearch()));
                    content.append('\n');

                    content.append("Waive Off :");
                    if (inputBean.getWaveoffSearch() != null && !inputBean.getWaveoffSearch().isEmpty()) {
                        Status waveOffStatus = (Status) session.get(Status.class, inputBean.getWaveoffSearch());
                        content.append(Common.replaceEmptyorNullStringToALL(waveOffStatus.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getWaveoffSearch()));
                    }
                    content.append('\n');

                    content.append("Primary Account Type :");
                    String defaultType = Common.replaceEmptyorNullStringToALL(inputBean.getDefTypeSearch());
                    content.append(this.getDefaultTypeDesByCode(defaultType));
                    content.append('\n');

                    content.append("Primary Acc/Card Number :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getDefAccNoSearch()));

                }

            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    private String makeWhereClauseForCSV(CustomerSearchInputBean inputBean) throws ParseException {
        String where = "1=1";
        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += " and lower(U.CREATEDTIME) >= to_timestamp( '" + inputBean.getFromDate() + "' , 'yy-mm-dd')";
        }
        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getToDate());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and lower(U.CREATEDTIME) <= to_timestamp( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getCifSearch() != null && !inputBean.getCifSearch().isEmpty()) {
            where += " and lower(U.CIF) LIKE lower('%" + inputBean.getCifSearch().trim() + "%')";
        }
        if (inputBean.getUsernameSearch() != null && !inputBean.getUsernameSearch().isEmpty()) {
            where += " and lower(U.USERNAME) LIKE lower('%" + inputBean.getUsernameSearch().trim() + "%')";
        }

        if (inputBean.getNicSearch() != null && !inputBean.getNicSearch().isEmpty()) {
            where += " and lower(U.NIC) LIKE lower('%" + inputBean.getNicSearch().trim() + "%')";
        }

        if (inputBean.getMobilenoSearch() != null && !inputBean.getMobilenoSearch().isEmpty()) {
            where += " and lower(U.MOBILE_NUMBER) LIKE lower('%" + inputBean.getMobilenoSearch().trim() + "%')";
        }

        if (inputBean.getEmailSearch() != null && !inputBean.getEmailSearch().isEmpty()) {
            where += " and lower(U.EMAIL) LIKE lower('%" + inputBean.getEmailSearch().trim() + "%')";
        }

//        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
//
//            where += " and U.STATUS LIKE '%" + inputBean.getStatusSearch() + "%'";
//
//        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            if (inputBean.getStatusSearch().equals(CommonVarList.STATUS_ACTIVE)) {
                where += " and U.STATUS = '" + inputBean.getStatusSearch() + "' and (U.USERNAME is not null or U.USERNAME !='')";
            } else if (inputBean.getStatusSearch().equals(CommonVarList.STATUS_CUSTOMER_PEND)) {
                where += " and U.STATUS = '" + CommonVarList.STATUS_ACTIVE + "' and (U.USERNAME is null or U.USERNAME = '')";
            } else {
                where += " and U.STATUS = '" + inputBean.getStatusSearch() + "'";
            }
        }
        if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
            where += " and lower(U.USER_SEGMENT) like lower('%" + inputBean.getSegmentTypeSearch().trim() + "%')";
        }
        if (inputBean.getOnBoardChannelSearch() != null && !inputBean.getOnBoardChannelSearch().isEmpty()) {
            where += " and U.ON_BOARD_CHANNEL = '" + inputBean.getOnBoardChannelSearch() + "'";
        }
        if (inputBean.getOnBoardTypeSearch() != null && !inputBean.getOnBoardTypeSearch().isEmpty()) {
            where += " and U.ON_BOARD_TYPE = '" + inputBean.getOnBoardTypeSearch() + "'";
        }
        if (inputBean.getCustomerCategorySearch() != null && !inputBean.getCustomerCategorySearch().isEmpty()) {
            where += " and lower(U.CUSTOMER_CATEGORY) like lower('%" + inputBean.getCustomerCategorySearch().trim() + "%')";
        }
        if (inputBean.getWaveoffSearch() != null && !inputBean.getWaveoffSearch().isEmpty()) {
            where += " and U.WAIVEOFF = '" + inputBean.getWaveoffSearch().trim() + "'";
        }
        if (inputBean.getDefTypeSearch() != null && !inputBean.getDefTypeSearch().isEmpty()) {
            where += " and U.DEFAULT_ACC_TYPE = '" + inputBean.getDefTypeSearch().trim() + "'";
        }
        if (inputBean.getDefAccNoSearch()!= null && !inputBean.getDefAccNoSearch().isEmpty()) {
            where += " and lower(U.DEFAULT_ACC_NO) like lower('%" + inputBean.getDefAccNoSearch().trim() + "%')";
        }

        return where;
    }

    public Object generateExcelReport(CustomerSearchInputBean inputBean) throws Exception {
        Session session = null;
        Object returnObject = null;
        String CUSTOMER_SQL_SEARCH = "SELECT "
                + "U.ID, "//0
                + "U.NIC, "//1
                + "U.DOB, "//2
                + "U.CIF, "//3
                + "U.USERNAME, "//4
                + "U.EMAIL, "//5
                + "U.MOBILE_NUMBER, "//6  
                + "U.LOGIN_ATTEMPTS, "//7   
                + "U.LASTUPDATEDTIME, "//8
                + "U.CREATEDTIME, "//9   
                + " U.STATUS STATUS, "//10
                + "S.DESCRIPTION STATUSDES, "//11
                + "SE.DESCRIPTION SEGMENT, "//12
                + "U.PROFILE_IMAGE, "//13
                + "U.PROF_IMAGE_TIMESTAMP, "//14
                + "U.OTP, "//15
                + "U.OTP_EXP_TIME, "//16
                + "U.MOBILE_PIN , "//17
                + "U.USER_TRACE_NUMBER, "//18
                + "U.SECONDARY_EMAIL, "//19
                + "U.SECONDARY_MOBILE, "//20
                + "U.REMARK, "//21
                + "U.MAKER, "//22
                + "U.CHECKER, "//23
                + "U.CUSTOMER_NAME, "//24
                + "U.LAST_LOGGEDIN_DATETIME, "//25
                + "U.ON_BOARD_CHANNEL, "//26
                + "U.ON_BOARD_TYPE, "//27
                + "U.CUSTOMER_CATEGORY, "//28
                + "U.DEFAULT_ACC_TYPE, "//29
                + "U.DEFAULT_ACC_NO, "//30
                + "U.FIRST_TIME_LOGIN_EXPECTED, "//31
                + "U.LAST_PASSWORD_UPDATED_DATE, "//32
                + "U.FINAL_FEE_DED_DAY, "//33
                + "W.DESCRIPTION WAIVEOFFDES, "//34
                + "U.CHARGE_AMOUNT, "//35
                + "U.ACTOFFICER "//36
                //                + "U.DEFAULT_CARD_NO "//37

                + "FROM SWT_MOBILE_USER U "
                + "LEFT OUTER JOIN STATUS S ON S.STATUSCODE = U.STATUS "
                + "LEFT OUTER JOIN SEGMENT_TYPE SE ON SE.SEGMENTCODE = U.USER_SEGMENT "
                + "LEFT OUTER JOIN STATUS W ON W.STATUSCODE = U.WAIVEOFF "
                + "WHERE ";
        try {

            String directory = ServletActionContext.getServletContext().getInitParameter("tmpreportpath");
            File file = new File(directory);
            if (file.exists()) {
                FileUtils.deleteDirectory(file);
            }

            session = HibernateInit.sessionFactory.openSession();

            int count = 0;
            String where1 = this.makeWhereClauseForExcel(inputBean);
            String sqlCount = this.CUSTOMER_COUNT_SQL + where1;
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount);
//            queryCount = setDatesToQuery(queryCount, inputBean, session);

//            queryCount = setDatesToQuery(queryCount, inputBean, session);
            if (queryCount.uniqueResult() != null) {
                count = ((Number) queryCount.uniqueResult()).intValue();
            }
//                System.err.println("Count is "+count);
            if (count > 0) {

                long maxRow = Long.parseLong(ServletActionContext.getServletContext().getInitParameter("numberofrowsperexcel"));
                SXSSFWorkbook workbook = this.createExcelTopSection(inputBean, session);
                Sheet sheet = workbook.getSheetAt(0);

                int currRow = headerRowCount;
                int fileCount = 0;

                currRow = this.createExcelTableHeaderSection(workbook, currRow);

                String sql = CUSTOMER_SQL_SEARCH + where1 + this.CUSTOMER_ORDER_BY_SQL;
                System.out.println(sql);
                int selectRow = Integer.parseInt(ServletActionContext.getServletContext().getInitParameter("numberofselectrows"));
                int numberOfTimes = count / selectRow;
                if ((count % selectRow) > 0) {
                    numberOfTimes += 1;
                }
                int from = 0;
                int listrownumber = 1;

                for (int i = 0; i < numberOfTimes; i++) {

                    Query query = session.createSQLQuery(sql);
                    query.setFirstResult(from);
                    query.setMaxResults(selectRow);

                    List<Object[]> objectArrList = (List<Object[]>) query.list();
                    if (objectArrList.size() > 0) {

                        for (Object[] objArr : objectArrList) {
                            CustomerSearchBean searchBean = new CustomerSearchBean();

                            try {
                                searchBean.setUserid(objArr[0].toString());
                            } catch (NullPointerException e) {
                                searchBean.setUserid("--");
                            }

                            try {
                                searchBean.setNic(objArr[1].toString());
                            } catch (NullPointerException e) {
                                searchBean.setNic("--");
                            }
                            try {
                                searchBean.setDob(objArr[2].toString());
                            } catch (Exception e) {
                                searchBean.setDob("--");
                            }
                            try {
                                searchBean.setCif(objArr[3].toString());
                            } catch (NullPointerException e) {
                                searchBean.setCif("--");
                            }
                            try {
                                searchBean.setUsername(objArr[4].toString());
                            } catch (NullPointerException e) {
                                searchBean.setUsername("--");
                            }
                            try {
                                searchBean.setEmail(objArr[5].toString());
                            } catch (NullPointerException e) {
                                searchBean.setEmail("--");
                            }
                            try {
                                searchBean.setMobileNumber(objArr[6].toString());
                            } catch (NullPointerException e) {
                                searchBean.setMobileNumber("--");
                            }
                            try {
                                searchBean.setLoginAttempts(objArr[7].toString());
                                searchBean.setLoginAttemptStatus(this.getUserBlocktatus(session, objArr[7].toString()));
                            } catch (NullPointerException e) {
                                searchBean.setLoginAttempts("--");
                                searchBean.setLoginAttemptStatus("--");
                            }
                            try {
                                searchBean.setLastupdatedtime(objArr[8].toString().substring(0, 19));
                            } catch (Exception e) {
                                searchBean.setLastupdatedtime("--");
                            }
                            try {
                                searchBean.setCreatedtime(objArr[9].toString().substring(0, 19));
                            } catch (Exception e) {
                                searchBean.setCreatedtime("--");
                            }
                            try {
                                searchBean.setStatus(objArr[10].toString());
                            } catch (NullPointerException e) {
                                searchBean.setStatus("--");
                            }
//                            try {
//                                searchBean.setStatusDes(objArr[11].toString());
//                            } catch (NullPointerException e) {
//                                searchBean.setStatusDes("--");
//                            }
                            try {
                                if ((objArr[4] == null || objArr[4].toString().isEmpty()) && objArr[10].toString().equals(CommonVarList.STATUS_ACTIVE)) {
                                    searchBean.setStatusDes(CommonVarList.CUSTOMER_PEND_STATUS_DES);
                                } else {
                                    searchBean.setStatusDes(objArr[11].toString());
                                }
                            } catch (NullPointerException e) {
                                searchBean.setStatusDes("--");
                            }
                            try {
                                searchBean.setPromotionMtSegmentType(objArr[12].toString());
                            } catch (Exception e) {
                                searchBean.setPromotionMtSegmentType("--");
                            }
                            try {
                                searchBean.setOtp(objArr[15].toString());
                            } catch (NullPointerException e) {
                                searchBean.setOtp("--");
                            }
                            try {
                                searchBean.setOtpExpTime(objArr[16].toString());
                            } catch (NullPointerException e) {
                                searchBean.setOtpExpTime("--");
                            }
                            try {
                                searchBean.setMobilePin(objArr[17].toString());
                            } catch (NullPointerException e) {
                                searchBean.setMobilePin("--");
                            }
                            try {
                                searchBean.setUserTraceNumber(objArr[18].toString());
                            } catch (NullPointerException e) {
                                searchBean.setUserTraceNumber("--");
                            }
                            try {
                                searchBean.setSecondaryEmail(objArr[19].toString());
                            } catch (NullPointerException e) {
                                searchBean.setSecondaryEmail("--");
                            }
                            try {
                                searchBean.setSecondaryMobile(objArr[20].toString());
                            } catch (NullPointerException e) {
                                searchBean.setSecondaryMobile("--");
                            }
                            try {
                                searchBean.setRemark(objArr[21].toString());
                            } catch (NullPointerException e) {
                                searchBean.setRemark("--");
                            }
                            try {
                                searchBean.setMaker(objArr[22].toString());
                            } catch (NullPointerException e) {
                                searchBean.setMaker("--");
                            }
                            try {
                                searchBean.setChecker(objArr[23].toString());
                            } catch (NullPointerException e) {
                                searchBean.setChecker("--");
                            }
                            try {
                                searchBean.setCustomerName(objArr[24].toString());
                            } catch (NullPointerException e) {
                                searchBean.setCustomerName("--");
                            }
                            try {
                                searchBean.setLastLoggedinDatetime(objArr[25].toString().substring(0, 19));
                            } catch (Exception e) {
                                searchBean.setLastLoggedinDatetime("--");
                            }
                            try {
                                if (objArr[26] != null) {
                                    String channelTypeDes = this.getChannelTypeDesByCode(objArr[26].toString());
                                    searchBean.setOnBoardChannel(channelTypeDes);
                                } else {
                                    searchBean.setOnBoardChannel("--");
                                }
                            } catch (Exception e) {
                                searchBean.setOnBoardChannel("--");
                            }
                            try {
                                if (objArr[27] != null) {
                                    String onBordedTypeDes = this.getOnBordedTypeDesByCode(objArr[27].toString());
                                    searchBean.setOnBoardType(onBordedTypeDes);
                                } else {
                                    searchBean.setOnBoardType("--");
                                }
                            } catch (Exception e) {
                                searchBean.setOnBoardType("--");
                            }
                            try {
                                searchBean.setCustomerCategory(objArr[28].toString());
                            } catch (NullPointerException e) {
                                searchBean.setCustomerCategory("--");
                            }
                            try {
                                if (objArr[29] != null) {
                                    String onDefaultTypeDes = this.getDefaultTypeDesByCode(objArr[29].toString());
                                    searchBean.setDefType(onDefaultTypeDes);
                                } else {
                                    searchBean.setDefType("--");
                                }
                            } catch (Exception e) {
                                searchBean.setDefType("--");
                            }
                            try {
                                searchBean.setDefAccNo(objArr[30].toString());
                            } catch (NullPointerException e) {
                                searchBean.setDefAccNo("--");
                            }
                            try {
                                if (objArr[31] != null) {
                                    String loginStatusDes = this.getLoginStatusDesByCode(objArr[31].toString());
                                    searchBean.setLoginStatus(loginStatusDes);
                                } else {
                                    searchBean.setLoginStatus("--");
                                }
                            } catch (Exception e) {
                                searchBean.setLoginStatus("--");
                            }
                            try {
                                searchBean.setLastPassUpdateTime(objArr[32].toString().substring(0, 19));
                            } catch (Exception e) {
                                searchBean.setLastPassUpdateTime("--");
                            }
                            try {
                                searchBean.setFinalFeeDudDay(objArr[33].toString());
                            } catch (NullPointerException e) {
                                searchBean.setFinalFeeDudDay("--");
                            }
                            try {
                                searchBean.setWaveoff(objArr[34].toString());
                            } catch (NullPointerException e) {
                                searchBean.setWaveoff("--");
                            }
                            try {
                                searchBean.setChargeAmount(objArr[35].toString());
                            } catch (NullPointerException e) {
                                searchBean.setChargeAmount("--");
                            }
                            try {
                                searchBean.setAccountofficer(objArr[36].toString());
                            } catch (NullPointerException e) {
                                searchBean.setAccountofficer("--");
                            }
                            searchBean.setFullCount(count);

                            if (currRow + 1 > maxRow) {
                                fileCount++;
                                this.writeTemporaryFile(workbook, fileCount, directory);
                                workbook = this.createExcelTopSection(inputBean, session);
                                sheet = workbook.getSheetAt(0);
                                currRow = headerRowCount;
                                this.createExcelTableHeaderSection(workbook, currRow);
                            }
                            currRow = this.createExcelTableBodySection(workbook, searchBean, currRow, listrownumber);
                            listrownumber++;
                            if (currRow % 100 == 0) {
                                ((SXSSFSheet) sheet).flushRows(100); // retain 100 last rows and flush all others

                                // ((SXSSFSheet)sh).flushRows() is a shortcut for ((SXSSFSheet)sh).flushRows(0),
                                // this method flushes all rows
                            }
                        }
                    }
                    from = from + selectRow;
                }

                Date createdTime = CommonDAO.getSystemDate(session);
                this.createExcelBotomSection(workbook, currRow, count, createdTime);
                System.out.println("--------" + currRow);
                if (fileCount > 0) {
                    fileCount++;
                    this.writeTemporaryFile(workbook, fileCount, directory);
                    ByteArrayOutputStream outputStream = Common.zipFiles(file.listFiles());
                    returnObject = outputStream;
                    workbook.dispose();
                } else {
//                    for (int i = 0; i < columnCount; i++) {
//                        //to auto size all column in the sheet
//                        sheet.autoSizeColumn(i);
//                    }

                    returnObject = workbook;
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return returnObject;
    }

    private String makeWhereClauseForExcel(CustomerSearchInputBean inputBean) throws ParseException {
        String where = "1=1";

        if (inputBean.getFromDate() != null && !inputBean.getFromDate().isEmpty()) {
            where += " and lower(U.CREATEDTIME) >= to_timestamp( '" + inputBean.getFromDate() + "' , 'yy-mm-dd')";

        }

        if (inputBean.getToDate() != null && !inputBean.getToDate().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getToDate());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and lower(U.CREATEDTIME) <= to_timestamp( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getCifSearch() != null && !inputBean.getCifSearch().isEmpty()) {
            where += " and lower(U.CIF) like lower('%" + inputBean.getCifSearch().trim() + "%')";
        }

        if (inputBean.getUsernameSearch() != null && !inputBean.getUsernameSearch().isEmpty()) {
            where += " and lower(U.USERNAME) like lower('%" + inputBean.getUsernameSearch().trim() + "%')";
        }

        if (inputBean.getNicSearch() != null && !inputBean.getNicSearch().isEmpty()) {
            where += " and lower(U.NIC) like lower('%" + inputBean.getNicSearch().trim() + "%')";
        }

//        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
//            where += " and U.STATUS = '" + inputBean.getStatusSearch() + "'";
//        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            if (inputBean.getStatusSearch().equals(CommonVarList.STATUS_ACTIVE)) {
                where += " and U.STATUS = '" + inputBean.getStatusSearch() + "' and (U.USERNAME is not null or U.USERNAME !='')";
            } else if (inputBean.getStatusSearch().equals(CommonVarList.STATUS_CUSTOMER_PEND)) {
                where += " and U.STATUS = '" + CommonVarList.STATUS_ACTIVE + "' and (U.USERNAME is null or U.USERNAME = '')";
            } else {
                where += " and U.STATUS = '" + inputBean.getStatusSearch() + "'";
            }
        }

//        if (inputBean.getFirstnameSearch() != null && !inputBean.getFirstnameSearch().isEmpty()) {
//            where += " and lower(U.FIRSTNAME) like lower('%" + inputBean.getFirstnameSearch() + "%')";
//        }
//        if (inputBean.getLastnameSearch() != null && !inputBean.getLastnameSearch().isEmpty()) {
//            where += " and lower(U.LASTNAME) like lower('%" + inputBean.getLastnameSearch() + "%')";
//        }
        if (inputBean.getMobilenoSearch() != null && !inputBean.getMobilenoSearch().isEmpty()) {
            where += " and lower(U.MOBILE_NUMBER) like lower('%" + inputBean.getMobilenoSearch().trim() + "%')";
        }

        if (inputBean.getEmailSearch() != null && !inputBean.getEmailSearch().isEmpty()) {
            where += " and lower(U.EMAIL) like lower('%" + inputBean.getEmailSearch().trim() + "%')";
        }

        if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
            where += " and lower(U.USER_SEGMENT) like lower('%" + inputBean.getSegmentTypeSearch().trim() + "%')";
        }
        if (inputBean.getOnBoardChannelSearch() != null && !inputBean.getOnBoardChannelSearch().isEmpty()) {
            where += " and U.ON_BOARD_CHANNEL = '" + inputBean.getOnBoardChannelSearch() + "'";
        }
        if (inputBean.getOnBoardTypeSearch() != null && !inputBean.getOnBoardTypeSearch().isEmpty()) {
            where += " and U.ON_BOARD_TYPE = '" + inputBean.getOnBoardTypeSearch() + "'";
        }
        if (inputBean.getCustomerCategorySearch() != null && !inputBean.getCustomerCategorySearch().isEmpty()) {
            where += " and lower(U.CUSTOMER_CATEGORY) like lower('%" + inputBean.getCustomerCategorySearch().trim() + "%')";
        }
        if (inputBean.getWaveoffSearch() != null && !inputBean.getWaveoffSearch().isEmpty()) {
            where += " and U.WAIVEOFF = '" + inputBean.getWaveoffSearch().trim() + "'";
        }

        return where;
    }

    private SXSSFWorkbook createExcelTopSection(CustomerSearchInputBean inputBean, Session session) throws Exception {

        SXSSFWorkbook workbook = new SXSSFWorkbook(-1);
        Sheet sheet = workbook.createSheet("Customer_Search_Report");

        CellStyle fontBoldedUnderlinedCell = ExcelCommon.getFontBoldedUnderlinedCell(workbook);

        Row row = sheet.createRow(0);
        Cell cell = row.createCell(0);
        cell.setCellValue("NDB MB Solution");
        cell.setCellStyle(fontBoldedUnderlinedCell);

        row = sheet.createRow(2);
        cell = row.createCell(0);
        cell.setCellValue("Customer Search Report");
        cell.setCellStyle(fontBoldedUnderlinedCell);

        row = sheet.createRow(4);
        cell = row.createCell(0);
        cell.setCellValue("From Date");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getFromDate()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(5);
        cell = row.createCell(0);
        cell.setCellValue("To Date");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getToDate()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(6);
        cell = row.createCell(0);
        cell.setCellValue("CID");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCifSearch()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(7);
        cell = row.createCell(0);
        cell.setCellValue("User Name");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getUsernameSearch()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(8);
        cell = row.createCell(0);
        cell.setCellValue("User Segment");
        cell = row.createCell(1);
        if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
            SegmentType segmenttype = (SegmentType) session.get(SegmentType.class, inputBean.getSegmentTypeSearch());
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(segmenttype.getDescription()));
        } else {
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getSegmentTypeSearch()));
        }
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(9);
        cell = row.createCell(0);
        cell.setCellValue("On Board Channel");
        cell = row.createCell(1);
        String onBoardChannel = Common.replaceEmptyorNullStringToALL(inputBean.getOnBoardChannelSearch());
        cell.setCellValue(this.getChannelTypeDesByCode(onBoardChannel));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(10);
        cell = row.createCell(0);
        cell.setCellValue("On Board Type");
        cell = row.createCell(1);
        String onBoardType = Common.replaceEmptyorNullStringToALL(inputBean.getOnBoardTypeSearch());
        cell.setCellValue(this.getOnBordedTypeDesByCode(onBoardType));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(11);
        cell = row.createCell(0);
        cell.setCellValue("Customer Category");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getCustomerCategorySearch()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(12);
        cell = row.createCell(0);
        cell.setCellValue("Status");
        cell = row.createCell(1);
        cell = row.createCell(1);
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            Status status = (Status) session.get(Status.class, inputBean.getStatusSearch());
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(status.getDescription()));
        } else {
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getStatusSearch()));
        }
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(13);
        cell = row.createCell(0);
        cell.setCellValue("NIC");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getNicSearch()));

        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(14);
        cell = row.createCell(0);
        cell.setCellValue("Mobile");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getMobilenoSearch()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(15);
        cell = row.createCell(0);
        cell.setCellValue("Email");
        cell = row.createCell(1);
        cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getEmailSearch()));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(16);
        cell = row.createCell(0);
        cell.setCellValue("Waive Off");
        cell = row.createCell(1);
        cell = row.createCell(1);
        if (inputBean.getWaveoffSearch() != null && !inputBean.getWaveoffSearch().isEmpty()) {
            Status waveoffstatus = (Status) session.get(Status.class, inputBean.getWaveoffSearch());
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(waveoffstatus.getDescription()));
        } else {
            cell.setCellValue(Common.replaceEmptyorNullStringToALL(inputBean.getStatusSearch()));
        }
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

//        
        return workbook;
    }

    private int createExcelTableHeaderSection(SXSSFWorkbook workbook, int currrow) throws Exception {
        CellStyle columnHeaderCell = ExcelCommon.getColumnHeadeCell(workbook);
        Sheet sheet = workbook.getSheetAt(0);

        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue("No");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(1);
        cell.setCellValue("Unique ID");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(2);
        cell.setCellValue("CID");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(3);
        cell.setCellValue("User Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(4);
        cell.setCellValue("User Segment");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(5);
        cell.setCellValue("On Board Channel");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(6);
        cell.setCellValue("On Board Type");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(7);
        cell.setCellValue("Customer Category");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(8);
        cell.setCellValue("Status");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(9);
        cell.setCellValue("Waive Off");
        cell.setCellStyle(columnHeaderCell);

//        cell = row.createCell(10);
//        cell.setCellValue("Charge Amount");
//        cell.setCellStyle(columnHeaderCell);
        cell = row.createCell(10);
        cell.setCellValue("Login Status");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(11);
        cell.setCellValue("Primary Account Type");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(12);
        cell.setCellValue("Primary Acc/Card Number");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(13);
        cell.setCellValue("Customer Name");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(14);
        cell.setCellValue("NIC");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(15);
        cell.setCellValue("DOB");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(16);
        cell.setCellValue("Mobile Number");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(17);
        cell.setCellValue("Mobile Number(Secondary)");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(18);
        cell.setCellValue("Email");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(19);
        cell.setCellValue("Email(Secondary)");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(20);
        cell.setCellValue("Block Status");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(21);
        cell.setCellValue("Invalid Login Attempts");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(22);
        cell.setCellValue("Last Logged In Date And Time");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(23);
        cell.setCellValue("Last Password Updated Date And Time");
        cell.setCellStyle(columnHeaderCell);

//        cell = row.createCell(24);
//        cell.setCellValue("Final Fee Deducted Date");
//        cell.setCellStyle(columnHeaderCell);
        cell = row.createCell(24);
        cell.setCellValue("Account Officer");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(25);
        cell.setCellValue("Remark");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(26);
        cell.setCellValue("Maker");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(27);
        cell.setCellValue("Checker");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(28);
        cell.setCellValue("Created Date And Time");
        cell.setCellStyle(columnHeaderCell);

        cell = row.createCell(29);
        cell.setCellValue("Last Updated Date And Time");
        cell.setCellStyle(columnHeaderCell);

        return currrow;
    }

    private void writeTemporaryFile(SXSSFWorkbook workbook, int fileCount, String directory) throws Exception {
        File file;
        FileOutputStream outputStream = null;
        try {
            Sheet sheet = workbook.getSheetAt(0);
//            for (int i = 0; i < columnCount; i++) {
//                //to auto size all column in the sheet
////                sheet.autoSizeColumn(i);
//            }

            file = new File(directory);
            if (!file.exists()) {
                System.out.println("Directory created or not : " + file.mkdirs());
            }

            if (fileCount > 0) {
                file = new File(directory + File.separator + "Customer Search Report_" + fileCount + ".xlsx");
            } else {
                file = new File(directory + File.separator + "Customer Search Report.xlsx");
            }
            outputStream = new FileOutputStream(file);
            workbook.write(outputStream);
        } catch (IOException e) {
            throw e;
        } finally {
            if (outputStream != null) {
                outputStream.flush();
                outputStream.close();
            }
        }
    }

    private int createExcelTableBodySection(SXSSFWorkbook workbook, CustomerSearchBean dataBean, int currrow, int rownumber) throws Exception {
        Sheet sheet = workbook.getSheetAt(0);
        CellStyle rowColumnCell = ExcelCommon.getRowColumnCell(workbook);
        Row row = sheet.createRow(currrow++);

        Cell cell = row.createCell(0);
        cell.setCellValue(rownumber);
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(1);
        cell.setCellValue(dataBean.getUserid());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(2);
        cell.setCellValue(dataBean.getCif());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(3);
        cell.setCellValue(dataBean.getUsername());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(4);
        cell.setCellValue(dataBean.getPromotionMtSegmentType());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(5);
        cell.setCellValue(dataBean.getOnBoardChannel());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(6);
        cell.setCellValue(dataBean.getOnBoardType());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(7);
        cell.setCellValue(dataBean.getCustomerCategory());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(8);
        cell.setCellValue(dataBean.getStatusDes());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(9);
        cell.setCellValue(dataBean.getWaveoff());
        cell.setCellStyle(rowColumnCell);

//        cell = row.createCell(10);
//        cell.setCellValue(dataBean.getChargeAmount());
//        cell.setCellStyle(rowColumnCell);
        cell = row.createCell(10);
        cell.setCellValue(dataBean.getLoginStatus());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(11);
        cell.setCellValue(dataBean.getDefType());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(12);
        cell.setCellValue(dataBean.getDefAccNo());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(13);
        cell.setCellValue(dataBean.getCustomerName());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(14);
        cell.setCellValue(dataBean.getNic());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(15);
        cell.setCellValue(dataBean.getDob());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(16);
        cell.setCellValue(dataBean.getMobileNumber());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(17);
        cell.setCellValue(dataBean.getSecondaryMobile());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(18);
        cell.setCellValue(dataBean.getEmail());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(19);
        cell.setCellValue(dataBean.getSecondaryEmail());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(20);
        cell.setCellValue(dataBean.getLoginAttemptStatus());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(21);
        cell.setCellValue(dataBean.getLoginAttempts());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(22);
        cell.setCellValue(dataBean.getLastLoggedinDatetime());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(23);
        cell.setCellValue(dataBean.getLastPassUpdateTime());
        cell.setCellStyle(rowColumnCell);

//        cell = row.createCell(25);
//        cell.setCellValue(dataBean.getFinalFeeDudDay());
//        cell.setCellStyle(rowColumnCell);
        cell = row.createCell(24);
        cell.setCellValue(dataBean.getAccountofficer());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(25);
        cell.setCellValue(dataBean.getRemark());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(26);
        cell.setCellValue(dataBean.getMaker());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(27);
        cell.setCellValue(dataBean.getChecker());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(28);
        cell.setCellValue(dataBean.getCreatedtime());
        cell.setCellStyle(rowColumnCell);

        cell = row.createCell(29);
        cell.setCellValue(dataBean.getLastupdatedtime());
        cell.setCellStyle(rowColumnCell);

//        sheet.autoSizeColumn(9);
        return currrow;
    }

    private void createExcelBotomSection(SXSSFWorkbook workbook, int currrow, long count, Date date) throws Exception {

        CellStyle fontBoldedCell = ExcelCommon.getFontBoldedCell(workbook);
        Sheet sheet = workbook.getSheetAt(0);

        currrow++;
        Row row = sheet.createRow(currrow++);
        Cell cell = row.createCell(0);
        cell.setCellValue("Summary");
        cell.setCellStyle(fontBoldedCell);

        row = sheet.createRow(currrow++);
        cell = row.createCell(0);
        cell.setCellValue("Total Record Count");
        cell = row.createCell(1);
        cell.setCellValue(count);
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));

        row = sheet.createRow(currrow++);
        cell = row.createCell(0);
        cell.setCellValue("Report Created Time and Date");
        cell = row.createCell(1);
        cell.setCellValue(date.toString().substring(0, 19));
        cell.setCellStyle(ExcelCommon.getAligneCell(workbook, null, XSSFCellStyle.ALIGN_RIGHT));
    }

    public List<CustomerSearchLimitBean> mobUserLimitListByUserID(String userid, int max, int first) throws Exception {

        List<CustomerSearchLimitBean> dataList = new ArrayList<CustomerSearchLimitBean>();
        Session session = null;

        try {

            long count = 0;

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id.userid) from SwtUserTxnLimit as u where id.userid=:userid ";
//            Query queryCount = session.createQuery(sqlCount).setBigDecimal("userid", new BigDecimal(userid));
            Query queryCount = session.createQuery(sqlCount).setString("userid", userid);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();
            System.out.println("count: " + count);

            if (count > 0) {

                String sqlSearch = "from SwtUserTxnLimit u where id.userid=:userid order by u.createdTime desc";
                Query querySearch = session.createQuery(sqlSearch).setString("userid", userid);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    CustomerSearchLimitBean limitBean = new CustomerSearchLimitBean();
                    SwtUserTxnLimit userLimits = (SwtUserTxnLimit) it.next();

                    try {
                        limitBean.setUserid(userLimits.getId().getUserid().toString());
                    } catch (NullPointerException npe) {
                        limitBean.setUserid("--");
                    }
                    try {
                        limitBean.setTxntype(userLimits.getId().getTxnLimitType());
//                        limitBean.setTxntypedes(userLimits.getSwtMtTxnLimitType().getDescription());
                    } catch (NullPointerException npe) {
                        limitBean.setTxntype("--");
//                        limitBean.setTxntypedes("--");
                    }
                    try {
//                        limitBean.setTxntype(userLimits.getId().getTxnLimitType());
                        limitBean.setTxntypedes(userLimits.getSwtMtTxnLimitType().getDescription());
                    } catch (Exception npe) {
//                        limitBean.setTxntype("--");
                        limitBean.setTxntypedes("--");
                    }

                    try {
                        limitBean.setCif(userLimits.getSwtMobileUser().getCif().toString());
                    } catch (NullPointerException npe) {
                        limitBean.setCif("--");
                    }

                    try {
                        Status st = (Status) session.get(Status.class, userLimits.getStatus());
                        limitBean.setStatus(userLimits.getStatus());
                        limitBean.setStatusdes((st.getDescription()));
                    } catch (Exception npe) {
                        limitBean.setStatus("--");
                        limitBean.setStatusdes("--");
                    }

                    try {
                        limitBean.setUserLimit((userLimits.getUserLimit().toString()));
                    } catch (NullPointerException npe) {
                        limitBean.setUserLimit("--");
                    }

                    try {
                        if (userLimits.getCreatedTime().toString() != null && !userLimits.getCreatedTime().toString().isEmpty()) {
                            limitBean.setCreatedTime(userLimits.getCreatedTime().toString().substring(0, 19));
                        } else {
                            limitBean.setCreatedTime("--");
                        }
                    } catch (Exception npe) {
                        limitBean.setCreatedTime("--");
                    }
                    try {
                        if (userLimits.getLastUpdatedTime().toString() != null && !userLimits.getLastUpdatedTime().toString().isEmpty()) {
                            limitBean.setLastUpdatedTime(userLimits.getLastUpdatedTime().toString().substring(0, 19));
                        } else {
                            limitBean.setLastUpdatedTime("--");
                        }
                    } catch (Exception npe) {
                        limitBean.setLastUpdatedTime("--");
                    }

                    try {
                        limitBean.setMaker((userLimits.getMaker().toString()));
                    } catch (NullPointerException npe) {
                        limitBean.setMaker("--");
                    }

                    try {
                        limitBean.setChecker((userLimits.getChecker().toString()));
                    } catch (NullPointerException npe) {
                        limitBean.setChecker("--");
                    }

                    limitBean.setFullCount(count);

                    dataList.add(limitBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;

    }

    public SwtConfigBean getSwitchConfig() throws Exception {
        SwtMtConfiguration swtConfig = null;
        Session session = null;
        SwtConfigBean swtConfigBean = new SwtConfigBean();
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from SwtMtConfiguration as mb where mb.id=:id";
            Query query = session.createQuery(sql);
            query.setLong("id", CommonVarList.SWT_CONFIG_ID);

            swtConfig = (SwtMtConfiguration) query.list().get(0);

            swtConfigBean.setId(String.valueOf(swtConfig.getId()));
            swtConfigBean.setNtbRestBaseUrl(swtConfig.getNtbRestBaseUrl());
            swtConfigBean.setRestAutorization(swtConfig.getRestAutorization());
            swtConfigBean.setRestContentType(swtConfig.getRestContentType());

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return swtConfigBean;
    }

    public String getChannelTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("2")) {
            des = "Internet Banking";
        } else if (code.equals("1")) {
            des = "Mobile Banking";
        } else {
            des = code;
        }
        return des;

    }

    public String getOnBordedTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("1")) {
            des = "Account";
        } else if (code.equals("2")) {
            des = "Card";
        } else if (code.equals("3")) {
            des = "Migrated User";
        } else {
            des = code;
        }
        return des;

    }

    public String getDefaultTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("1")) {
            des = "Account";
        } else if (code.equals("2")) {
            des = "Card";
        } else {
            des = code;
        }
        return des;

    }

    public String getLoginStatusDesByCode(String code) {
        String des = "--";
        if (code.equals("0")) {
            des = "Success (Set default Account)";
        } else if (code.equals("1")) {
            des = "First time login expected";
        } else {
            des = code;
        }
        return des;

    }

    public String toMaskCardNumber(String cardNumber) {

        StringBuilder maskCardNumber = new StringBuilder();
        String mask = "**********************";
        if (cardNumber.length() > 4) {
            int maskLength = cardNumber.length() - 4;
            maskCardNumber
                    .append(mask.substring(0, maskLength))
                    .append(cardNumber.substring(maskLength, cardNumber.length()));
        } else {
            maskCardNumber.append(cardNumber);
        }
        return maskCardNumber.toString();
    }

    public String getUserBlocktatus(Session session, String currentLoginAttampString) {
        String blockStatus = "";
        try {
            String hql = "from Passwordpolicy as m where m.passwordpolicyid=:passwordpolicyid";
            Query query = session.createQuery(hql).setString("passwordpolicyid", CommonVarList.MOB_USER_PAS_POLICY_ID);
            Passwordpolicy passwordpolicy = (Passwordpolicy) query.list().get(0);

            int invalidLoginAttamp = passwordpolicy.getNoofinvalidloginattempt();
            int currentLoginAttamp = Integer.parseInt(currentLoginAttampString);

            if (invalidLoginAttamp <= currentLoginAttamp) {
                blockStatus = CommonVarList.CUSTOMER_LOGIN_STATUS_YES;
            } else {
                blockStatus = CommonVarList.CUSTOMER_LOGIN_STATUS_NO;
            }
        } catch (Exception e) {
            blockStatus = "--";
        }
        return blockStatus;
    }

    public String getUserBlocktatusInAction(String currentLoginAttampString) {
        String blockStatus = "";
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String hql = "from Passwordpolicy as m where m.passwordpolicyid=:passwordpolicyid";
            Query query = session.createQuery(hql).setString("passwordpolicyid", CommonVarList.MOB_USER_PAS_POLICY_ID);
            Passwordpolicy passwordpolicy = (Passwordpolicy) query.list().get(0);

            int invalidLoginAttamp = passwordpolicy.getNoofinvalidloginattempt();
            int currentLoginAttamp = Integer.parseInt(currentLoginAttampString);

            if (invalidLoginAttamp <= currentLoginAttamp) {
                blockStatus = CommonVarList.CUSTOMER_LOGIN_STATUS_YES;
            } else {
                blockStatus = CommonVarList.CUSTOMER_LOGIN_STATUS_NO;
            }
        } catch (Exception e) {
            blockStatus = "--";
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return blockStatus;
    }

    public CustomerUpdateBean getPendOldNewValueOfCustomer(CustomerSearchInputBean inputBean) throws Exception {

        Session session = null;
        CustomerUpdateBean customerBean = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                customerBean = new CustomerUpdateBean();

                String[] penArray = null;
                if (u.getFields() != null) {
                    penArray = u.getFields().split("\\|");
                }
                customerBean.setTaskCode(u.getTask().getTaskcode());
                customerBean.setTaskDescription(u.getTask().getDescription());
                if (u.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_STATUS_CHANGE_TASK)) {
                    SwtMobileUser mobUser = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());
                    if (mobUser != null) {
                        customerBean.setUserid(mobUser.getId());

                        if (mobUser.getUsername() != null && !mobUser.getUsername().isEmpty()) {
                            customerBean.setUsername(mobUser.getUsername());
                        } else {
                            customerBean.setUsername("--");
                        }

                        if (mobUser.getCif() != null && !mobUser.getCif().isEmpty()) {
                            customerBean.setCid(mobUser.getCif());
                        } else {
                            customerBean.setCid("--");
                        }

                        if (mobUser.getNic() != null && !mobUser.getNic().isEmpty()) {
                            customerBean.setNic(mobUser.getNic());
                        } else {
                            customerBean.setNic("--");
                        }

                        //------------------------Old Value-----------------
                        if (mobUser.getStatus() != null) {
                            customerBean.setOldCustomerStatus(mobUser.getStatus().getDescription());
                        } else {
                            customerBean.setOldCustomerStatus("--");
                        }

                        if (mobUser.getWaveoff() != null) {
                            customerBean.setOldWaveOffStatus(mobUser.getWaveoff().getDescription());
                        } else {
                            customerBean.setOldWaveOffStatus("--");
                        }

                        if (mobUser.getChargeAmount() != null) {
                            customerBean.setOldChargeAmount(mobUser.getChargeAmount().toString());
                        } else {
                            customerBean.setOldChargeAmount("--");
                        }

                        //-------------------------New Value-----------------
                        Status statusNew = (Status) session.get(Status.class, penArray[1]);
                        if (statusNew != null) {
                            customerBean.setNewCustomerStatus(statusNew.getDescription());
                        } else {
                            customerBean.setNewCustomerStatus("--");
                        }

                        Status waiveOffStatusNew = (Status) session.get(Status.class, penArray[2]);
                        if (waiveOffStatusNew != null) {
                            customerBean.setNewWaveOffStatus(waiveOffStatusNew.getDescription());
                        } else {
                            customerBean.setNewWaveOffStatus("--");
                        }

                        if (penArray[3] != null && !penArray[3].isEmpty()) {
                            Double newchargeAmount = Double.valueOf(penArray[3].trim());
                            DecimalFormat df = new DecimalFormat("###.#");
                            customerBean.setNewChargeAmount(df.format(newchargeAmount));
                        } else {
                            customerBean.setNewChargeAmount("--");
                        }
                    }
                } else if (u.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_UPDATE_TASK)) {
                    SwtMobileUser mobUser = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());
                    if (mobUser != null) {
                        customerBean.setUserid(mobUser.getId());

                        if (mobUser.getUsername() != null && !mobUser.getUsername().isEmpty()) {
                            customerBean.setUsername(mobUser.getUsername());
                        } else {
                            customerBean.setUsername("--");
                        }

                        if (mobUser.getCif() != null && !mobUser.getCif().isEmpty()) {
                            customerBean.setCid(mobUser.getCif());
                        } else {
                            customerBean.setCid("--");
                        }

                        if (mobUser.getNic() != null && !mobUser.getNic().isEmpty()) {
                            customerBean.setNic(mobUser.getNic());
                        } else {
                            customerBean.setNic("--");
                        }
                    }

                    SwtUserDevice userDevice = (SwtUserDevice) session.get(SwtUserDevice.class, new BigDecimal(penArray[1]));
                    if (userDevice != null) {
                        customerBean.setDeviceId(userDevice.getId().toString());

                        if (userDevice.getDeviceNickName() != null && !userDevice.getDeviceNickName().isEmpty()) {
                            customerBean.setDeviceNickName(userDevice.getDeviceNickName());
                        } else {
                            customerBean.setDeviceNickName("--");
                        }

                        if (userDevice.getDeviceBuildNumber() != null && !userDevice.getDeviceBuildNumber().isEmpty()) {
                            customerBean.setDeviceBuildNumber(userDevice.getDeviceBuildNumber());
                        } else {
                            customerBean.setDeviceBuildNumber("--");
                        }

                        if (userDevice.getDeviceManufacture() != null && !userDevice.getDeviceManufacture().isEmpty()) {
                            customerBean.setDeviceManufacture(userDevice.getDeviceManufacture());
                        } else {
                            customerBean.setDeviceManufacture("--");
                        }

                        //---------------------------------Old Value--------------------------
                        if (userDevice.getStatus() != null) {
                            customerBean.setOldDeviceStatus(userDevice.getStatus().getDescription());
                        } else {
                            customerBean.setDeviceManufacture("--");
                        }

                        //-------------------------New Value-----------------
                        Status deviceStatusNew = (Status) session.get(Status.class, penArray[2]);
                        if (deviceStatusNew != null) {
                            customerBean.setNewDeviceStatus(deviceStatusNew.getDescription());
                        } else {
                            customerBean.setNewDeviceStatus("--");
                        }
                    }
                } else if (u.getTask().getTaskcode().equals(TaskVarList.CUS_BLOCK_APPLICATIONS)) {

                    SwtMobileUser mobUser = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());
                    if (mobUser != null) {
                        customerBean.setUserid(mobUser.getId());
                        if (mobUser.getUsername() != null && !mobUser.getUsername().isEmpty()) {
                            customerBean.setUsername(mobUser.getUsername());
                        } else {
                            customerBean.setUsername("--");
                        }

                        if (mobUser.getCif() != null && !mobUser.getCif().isEmpty()) {
                            customerBean.setCid(mobUser.getCif());
                        } else {
                            customerBean.setCid("--");
                        }
                        if (mobUser.getNic() != null && !mobUser.getNic().isEmpty()) {
                            customerBean.setNic(mobUser.getNic());
                        } else {
                            customerBean.setNic("--");
                        }

                        //---------------------------------Old Value--------------------------
                        if (mobUser.getStatusByMbStatus() != null) {
                            customerBean.setOldMobStatus(mobUser.getStatusByMbStatus().getDescription());
                        } else {
                            customerBean.setOldMobStatus("--");
                        }

                        if (mobUser.getStatusByIbStatus() != null) {
                            customerBean.setOldIbStatus(mobUser.getStatusByIbStatus().getDescription());
                        } else {
                            customerBean.setOldIbStatus("--");
                        }

                        //---------------------------------New Value--------------------------- 
                        Status statusMobNew = (Status) session.get(Status.class, penArray[1]);
                        if (penArray[1] != null && !penArray[1].isEmpty()) {
                            customerBean.setNewMobStatus(statusMobNew.getDescription());
                        } else {
                            customerBean.setNewMobStatus("--");
                        }
                        Status statusIbNew = (Status) session.get(Status.class, penArray[2]);
                        if (penArray[2] != null && !penArray[2].isEmpty()) {
                            customerBean.setNewIbStatus(statusIbNew.getDescription());
                        } else {
                            customerBean.setNewIbStatus("--");
                        }
                    }

                } else if (u.getTask().getTaskcode().equals(TaskVarList.CUS_PRIMARY_ACCOUNT_UPDATE_TASK)) {
                    SwtMobileUser mobUser = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());
                    if (mobUser != null) {
                        customerBean.setUserid(mobUser.getId());

                        if (mobUser.getUsername() != null && !mobUser.getUsername().isEmpty()) {
                            customerBean.setUsername(mobUser.getUsername());
                        } else {
                            customerBean.setUsername("--");
                        }

                        if (mobUser.getCif() != null && !mobUser.getCif().isEmpty()) {
                            customerBean.setCid(mobUser.getCif());
                        } else {
                            customerBean.setCid("--");
                        }

                        if (mobUser.getNic() != null && !mobUser.getNic().isEmpty()) {
                            customerBean.setNic(mobUser.getNic());
                        } else {
                            customerBean.setNic("--");
                        }
                        //---------------------------------Old Value--------------------------
                        if (mobUser.getDefaultAccType() != null) {
                            String defaultAccoutTypeDesOld = this.getDefaultTypeDesByCode(mobUser.getDefaultAccType().toString());
                            customerBean.setOldDefaultAccType(defaultAccoutTypeDesOld);
                        } else {
                            customerBean.setOldDefaultAccType("--");
                        }

                        if (mobUser.getDefaultAccNo() != null && !mobUser.getDefaultAccNo().isEmpty()) {
                            customerBean.setOldDefaultAccNo(mobUser.getDefaultAccNo());
                        } else {
                            customerBean.setOldDefaultAccNo("--");
                        }

                        //---------------------------------New Value--------------------------
                        String defaultAccoutTypeDesNew = this.getDefaultTypeDesByCode(penArray[2]);
                        customerBean.setNewDefaultAccType(defaultAccoutTypeDesNew);

                        if (penArray[1] != null && !penArray[1].isEmpty()) {
                            customerBean.setNewDefaultAccNo(penArray[1]);
                        } else {
                            customerBean.setNewDefaultAccNo("--");
                        }
                    }

                } else if (u.getTask().getTaskcode().equals(TaskVarList.CUSTOMER_DATA_UPDATE_TASK)) {
                    SwtMobileUser mobUser = (SwtMobileUser) session.get(SwtMobileUser.class, penArray[0].trim());
                    if (mobUser != null) {
                        customerBean.setUserid(mobUser.getId());

                        if (mobUser.getUsername() != null && !mobUser.getUsername().isEmpty()) {
                            customerBean.setUsername(mobUser.getUsername());
                        } else {
                            customerBean.setUsername("--");
                        }

                        if (mobUser.getCif() != null && !mobUser.getCif().isEmpty()) {
                            customerBean.setCid(mobUser.getCif());
                        } else {
                            customerBean.setCid("--");
                        }

                        if (mobUser.getNic() != null && !mobUser.getNic().isEmpty()) {
                            customerBean.setNic(mobUser.getNic());
                        } else {
                            customerBean.setNic("--");
                        }
                    }
                    //---------------------------------Old Value--------------------------
                    try {
                        customerBean.setOldCustomerName(mobUser.getCustomerName().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldCustomerName("--");
                    }
//                    try {
//                        customerBean.setOldStatus(mobUser.getStatus().getStatuscode().toString());
//                    } catch (Exception e) {
//                        customerBean.setOldStatus("--");
//                    }
                    try {
                        customerBean.setOldSegmentType(mobUser.getSegmentType().getSegmentcode().toString());
                    } catch (Exception e) {
                        customerBean.setOldSegmentType("--");
                    }
                    try {
                        customerBean.setOldCustomerCategory(mobUser.getCustomerCategory().toString());
                    } catch (Exception e) {
                        customerBean.setOldCustomerCategory("--");
                    }
                    try {
                        customerBean.setOldNic(mobUser.getNic().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldNic("--");
                    }
                    try {
                        customerBean.setOldDob(mobUser.getDob().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldDob("--");
                    }
                    try {
                        customerBean.setOldGender(mobUser.getGender().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldGender("--");
                    }
                    try {
                        customerBean.setOldMobileNumber(mobUser.getMobileNumber().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldMobileNumber("--");
                    }
                    try {
                        customerBean.setOldEmail(mobUser.getEmail().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldEmail("--");
                    }
                    try {
                        customerBean.setOldSecondaryMobile(mobUser.getSecondaryMobile().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldSecondaryMobile("--");
                    }
                    try {
                        customerBean.setOldSecondaryEmail(mobUser.getSecondaryEmail().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldSecondaryEmail("--");
                    }
                    try {
                        customerBean.setOldPermanentAdd(mobUser.getPermanentAddress().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldPermanentAdd("--");
                    }
                    try {
                        customerBean.setOldAccountofficer(mobUser.getActofficer().toString());
                    } catch (NullPointerException e) {
                        customerBean.setOldAccountofficer("--");
                    }
                    //---------------------------------New Value--------------------------
                    try {
                        if (penArray[1] != null && !penArray[1].trim().isEmpty()) {
                            customerBean.setNewCustomerName(penArray[1].trim());
                        } else {
                            customerBean.setNewCustomerName("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewCustomerName("--");
                    }
//                    try {
//                        if (penArray[2] != null && !penArray[2].trim().isEmpty()) {
//                            customerBean.setNewStatus(penArray[2].trim());
//                        }else{
//                            customerBean.setNewStatus("--");
//                        }
//                    } catch ( IndexOutOfBoundsException e) {
//                        customerBean.setNewStatus("--");
//                    }
                    try {
                        if (penArray[2] != null && !penArray[2].trim().isEmpty()) {
                            customerBean.setNewSegmentType(penArray[2].trim());
                        } else {
                            customerBean.setNewSegmentType("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewSegmentType("--");
                    }
                    try {
                        if (penArray[3] != null && !penArray[3].trim().isEmpty()) {
                            customerBean.setNewCustomerCategory(penArray[3].trim());
                        } else {
                            customerBean.setNewCustomerCategory("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewCustomerCategory("--");
                    }
                    try {
                        if (penArray[4] != null && !penArray[4].trim().isEmpty()) {
                            customerBean.setNewNic(penArray[4].trim());
                        } else {
                            customerBean.setNewNic("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewNic("--");
                    }
                    try {
                        if (penArray[5] != null && !penArray[5].trim().isEmpty()) {
                            customerBean.setNewDob(penArray[5].trim());
                        } else {
                            customerBean.setNewDob("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewDob("--");
                    }
                    try {
                        if (penArray[6] != null && !penArray[6].trim().isEmpty()) {
                            customerBean.setNewGender(penArray[6].trim());
                        } else {
                            customerBean.setNewGender("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewGender("--");
                    }
                    try {
                        if (penArray[7] != null && !penArray[7].trim().isEmpty()) {
                            customerBean.setNewMobileNumber(penArray[7].trim());
                        } else {
                            customerBean.setNewMobileNumber("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewMobileNumber("--");
                    }
                    try {
                        if (penArray[8] != null && !penArray[8].trim().isEmpty()) {
                            customerBean.setNewEmail(penArray[8].trim());
                        } else {
                            customerBean.setNewEmail("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewEmail("--");
                    }
                    try {
                        if (penArray[9] != null && !penArray[9].trim().isEmpty()) {
                            customerBean.setNewSecondaryMobile(penArray[9].trim());
                        } else {
                            customerBean.setNewSecondaryMobile("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewSecondaryMobile("--");
                    }
                    try {
                        if (penArray[10] != null && !penArray[10].trim().isEmpty()) {
                            customerBean.setNewSecondaryEmail(penArray[10].trim());
                        } else {
                            customerBean.setNewSecondaryEmail("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewSecondaryEmail("--");
                    }
                    try {
                        if (penArray[11] != null && !penArray[11].trim().isEmpty()) {
                            customerBean.setNewPermanentAdd(penArray[11].trim());
                        } else {
                            customerBean.setNewPermanentAdd("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewPermanentAdd("--");
                    }
                    try {
                        if (penArray[12] != null && !penArray[12].trim().isEmpty()) {
                            customerBean.setNewAccountofficer(penArray[12].trim());
                        } else {
                            customerBean.setNewAccountofficer("--");
                        }
                    } catch (IndexOutOfBoundsException e) {
                        customerBean.setNewAccountofficer("--");
                    }
                }

            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return customerBean;
    }

    /**
     *
     * @param passwordToHash
     * @param salt
     * @return
     * @throws Exception
     */
    public static String getSecurePassword(String passwordToHash, byte[] salt) throws Exception {
        String generatedString = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(salt);
            //Get the hash's bytes
            byte[] bytes = md.digest(passwordToHash.getBytes());
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedString = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw e;

        }
        return generatedString;
    }

    public String insertAlert(String mobile, String tempPass) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());
            SwtAlert alertInfo = new SwtAlert();

            AlertBean ab = new AlertBean();

            System.out.println("Mobile Number " + mobile);
            ab.setMobile_number(mobile);
            ab.setMessage_body("Dear Customer, Your temporary password is : " + tempPass);
            ab.setMode(0);

            JSONObject jsonObject = new JSONObject(ab);

            System.out.println("JSON String " + jsonObject.toString());

            alertInfo.setAlertData(jsonObject.toString());

            alertInfo.setStatus("PEND");
            alertInfo.setDateTime(timestampDate);

            alertInfo.setTxnType("999"); // 999
            alertInfo.setTxnMode(Long.parseLong("0"));

            alertInfo.setEmailStatus("DEACT"); //DEACT
            alertInfo.setSmsStatus("ACT");
            alertInfo.setPushStatus("DEACT"); //DEACT

            alertInfo.setIsSentEmail("PEND");
            alertInfo.setIsSentPush("PEND");
            alertInfo.setIsSentSms("PEND");
            alertInfo.setServerNode("1");

            alertInfo.setLastUpdateDateTime(timestampDate); //   ????

            session.save(alertInfo);

            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
}
