package com.epic.ndb.dao.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.usermanagement.SectionBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SectionInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SectionPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Section;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author jeevan
 */
public class SectionDao {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public String insertSection(SectionInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((Section) session.get(Section.class, inputBean.getSectionCode().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getSectionCode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getSectionCode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.SECTION_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }

            } else {
                message = MessageVarList.SECTION_CODE_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    //start newly changed
    public String activateSection(SectionInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();

            Date sysDate = CommonDAO.getSystemDate(session);

            Section u = (Section) session.get(Section.class, inputBean.getSectionCode().trim());
            if (u != null) {

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                u.setDescription(inputBean.getDescription().trim());
                u.setSortkey(new Byte(inputBean.getSortKey().trim()));

                //Change status to 'Activate'
                Status status = new Status();
                status.setStatuscode(CommonVarList.STATUS_ACTIVE);
                u.setStatus(status);

//                u.setUser(audit.getUser());
//                u.setLastupdatedtime(sysDate);
                session.save(audit);
                session.update(u);

                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    //end newly changed
    public Section findSectionById(String sectionCode) throws Exception {
        Section section = null;
        Session session = null;

        try {

            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Section as u where u.sectioncode=:sectioncode";
            Query query = session.createQuery(sql).setString("sectioncode", sectionCode);
            section = (Section) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return section;
    }

    public String deleteSection(SectionInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Section u = (Section) session.get(Section.class, inputBean.getSectionCode().trim());

            if (u != null) {

                long count = 0;
                long count2 = 0;

                String sqlCount = "select count(sectioncode) from Userrolesection as u where u.section.sectioncode=:sectioncode ";
                Query queryCount = session.createQuery(sqlCount).setString("sectioncode", inputBean.getSectionCode().trim());

                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();

                if (count > 0) {
                    message = MessageVarList.COMMON_NOT_DELETE;
                } else {
                    String sqlCount2 = "select count(sectioncode) from Sectionpage as u where u.section.sectioncode=:sectioncode ";
                    Query queryCount2 = session.createQuery(sqlCount2).setString("sectioncode", inputBean.getSectionCode().trim());

                    Iterator itCount2 = queryCount2.iterate();
                    count2 = (Long) itCount2.next();
                    if (count2 > 0) {
                        message = MessageVarList.COMMON_NOT_DELETE;
                    } else {

                        String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                        Query query = session.createQuery(sql)
                                .setString("PKey", inputBean.getSectionCode())
                                .setString("pagecode", audit.getPagecode());

                        if (query.list().isEmpty()) {

                            Section k = (Section) session.get(Section.class, inputBean.getSectionCode().trim());
                            if (k != null) {

                                audit.setNewvalue(k.getSectioncode() + "|" + k.getDescription() + "|" + k.getSortkey() + "|" + k.getStatus().getStatuscode());
                            }

                            Pendingtask pendingtask = new Pendingtask();

                            pendingtask.setPKey(inputBean.getSectionCode().trim());
                            pendingtask.setFields(audit.getNewvalue());

                            Task task = new Task();
                            task.setTaskcode(TaskVarList.DELETE_TASK);
                            pendingtask.setTask(task);

                            Status st = new Status();
                            st.setStatuscode(CommonVarList.STATUS_PENDING);
                            pendingtask.setStatus(st);

                            Page page = (Page) session.get(Page.class, PageVarList.SECTION_MGT_PAGE);
                            pendingtask.setPage(page);

                            pendingtask.setInputterbranch(sysUser.getBranch());

                            pendingtask.setCreatedtime(sysDate);
                            pendingtask.setLastupdatedtime(sysDate);
                            pendingtask.setCreateduser(audit.getLastupdateduser());

                            audit.setCreatetime(sysDate);
                            audit.setLastupdatedtime(sysDate);
                            audit.setLastupdateduser(audit.getLastupdateduser());

                            session.save(audit);
                            session.save(pendingtask);
                            txn.commit();
                        } else {
                            message = "pending available";
                        }

                    }
                }

//                String sql = "from Userrolesection as u where u.section.sectioncode=:sectioncode";
//                Query query = session.createQuery(sql).setString("sectioncode", inputBean.getSectionCode());
//                Userrolesection urs = (Userrolesection) query.list().get(0);
//                String sql2 = "from Usersectionpage as u where u.section.sectioncode=:sectioncode";
//                Query query2 = session.createQuery(sql2).setString("sectioncode", inputBean.getSectionCode());
//                Usersectionpage usp = (Usersectionpage) query2.list().get(0);
                //Change status to 'Delete'
//                Status status = new Status();
//                status.setStatuscode(CommonVarList.STATUS_DELETE);
//                u.setStatus(status);
//
//                u.setUser(audit.getUser());
//                u.setLastupdatedtime(sysDate);
//                session.update(u);
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

//    public String updateSection(SectionInputBean inputBean, Systemaudit audit, String statusVal) throws Exception {
    public String updateSection(SectionInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getSectionCode().trim())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getSectionCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.SECTION_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<SectionBean> getSearchList(SectionInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<SectionBean> dataList = new ArrayList<SectionBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(sectioncode) from Section as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Section u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    SectionBean sdblSection = new SectionBean();
                    Section section = (Section) it.next();

                    try {
                        sdblSection.setSectioncode(section.getSectioncode());
                    } catch (NullPointerException npe) {
                        sdblSection.setSectioncode("--");
                    }
                    try {
                        sdblSection.setDescription(section.getDescription());
                    } catch (NullPointerException npe) {
                        sdblSection.setDescription("--");
                    }
                    try {
                        sdblSection.setStatuscode(section.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        sdblSection.setStatuscode("--");
                    }
                    try {
                        sdblSection.setSortkey(section.getSortkey().toString());
                    } catch (NullPointerException npe) {
                        sdblSection.setSortkey("--");
                    }
                    try {
                        if (section.getCreatetime().toString() != null && !section.getCreatetime().toString().isEmpty()) {
                            sdblSection.setCreatetime(section.getCreatetime().toString().substring(0, 19));
                        } else {
                            sdblSection.setCreatetime("--");
                        }
                    } catch (NullPointerException npe) {
                        sdblSection.setCreatetime("--");
                    }

                    sdblSection.setFullCount(count);

                    dataList.add(sdblSection);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(SectionInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getSectionCodeSearch() != null && !inputBean.getSectionCodeSearch().trim().isEmpty()) {
            where += " and lower(u.sectioncode) like lower('%" + inputBean.getSectionCodeSearch().trim() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().trim().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch().trim() + "%')";
        }
        if (inputBean.getSortKeySearch() != null && !inputBean.getSortKeySearch().trim().isEmpty()) {
            where += " and u.sortkey = '" + inputBean.getSortKeySearch() + "'";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch().trim() + "'";
        }
        return where;
    }

    public List<SectionPendBean> getPendingSectionList(SectionInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<SectionPendBean> dataList = new ArrayList<SectionPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.SECTION_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.SECTION_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    SectionPendBean pendSectionBean = new SectionPendBean();
                    Pendingtask pendsection = (Pendingtask) it.next();

                    try {
                        pendSectionBean.setId(Long.toString(pendsection.getId()));
                    } catch (NullPointerException npe) {
                        pendSectionBean.setId("--");
                    }
                    try {
                        pendSectionBean.setOperation(pendsection.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        pendSectionBean.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pendsection.getFields() != null) {
                        penArray = pendsection.getFields().split("\\|");
                    }

                    try {
                        pendSectionBean.setSectioncode(penArray[0]);
                    } catch (NullPointerException npe) {
                        pendSectionBean.setSectioncode("--");
                    } catch (Exception ex) {
                        pendSectionBean.setSectioncode("--");
                    }
                    try {
                        pendSectionBean.setFields(pendsection.getFields());
                    } catch (NullPointerException npe) {
                        pendSectionBean.setFields("--");
                    } catch (Exception ex) {
                        pendSectionBean.setFields("--");
                    }
                    try {
                        pendSectionBean.setStatus(pendsection.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        pendSectionBean.setStatus("--");
                    }
                    try {
                        pendSectionBean.setCreateduser(pendsection.getCreateduser());
                    } catch (NullPointerException npe) {
                        pendSectionBean.setCreateduser("--");
                    } catch (Exception ex) {
                        pendSectionBean.setCreateduser("--");
                    }

                    pendSectionBean.setFullCount(count);

                    dataList.add(pendSectionBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String confirmSection(SectionInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    Section section = new Section();
                    section.setSectioncode(penArray[0]);
                    section.setDescription(penArray[1]);

                    Status st = (Status) session.get(Status.class, penArray[3].trim());
                    section.setStatus(st);

                    section.setSortkey(Byte.valueOf(penArray[2]));

                    section.setCreatetime(sysDate);
                    section.setLastupdatedtime(sysDate);
                    section.setLastupdateduser(audit.getLastupdateduser());

                    audit.setNewvalue(section.getSectioncode() + "|" + section.getDescription() + "|" + section.getSortkey() + "|" + section.getStatus().getDescription());
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on section ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    session.save(section);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Section u = (Section) session.get(Section.class, penArray[0]);

                    if (u != null) {

                        audit.setOldvalue(u.getSectioncode() + "|" + u.getDescription() + "|" + u.getSortkey() + "|" + u.getStatus().getDescription());

                        u.setDescription(penArray[1]);
                        u.setSortkey(new Byte(penArray[2]));
                        Status st = (Status) session.get(Status.class, penArray[3].trim());
                        u.setStatus(st);

                        u.setLastupdateduser(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getSortkey() + "|" + u.getStatus().getDescription());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on section ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    Section u = (Section) session.get(Section.class, penArray[0]);
                    if (u != null) {
                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getSortkey() + "|" + u.getStatus().getDescription());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on section ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectSection(SectionInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String sectioncode = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Section section = (Section) session.get(Section.class, fieldsArray[0]);

                    if (section != null) {

                        audit.setOldvalue(section.getSectioncode() + "|" + section.getDescription() + "|" + section.getSortkey() + "|" + section.getStatus().getStatuscode());
                    }
                }
//                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on section (code : " + sectioncode + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

}
