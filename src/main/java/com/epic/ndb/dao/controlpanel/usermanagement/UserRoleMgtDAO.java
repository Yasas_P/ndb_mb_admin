/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.usermanagement.UserRoleMgtBean;
import com.epic.ndb.bean.controlpanel.usermanagement.UserRoleMgtInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.UserRoleMgtPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.Userrole;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author asela
 */
public class UserRoleMgtDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    //start newly changed
    public String activateUrole(UserRoleMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();

            Date sysDate = CommonDAO.getSystemDate(session);

            Userrole u = (Userrole) session.get(Userrole.class, inputBean.getUserRoleCode().trim());
            if (u != null) {

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                u.setDescription(inputBean.getDescription().trim());

//                Userlevel userlevel = new Userlevel();
//                userlevel.setLevelid(Integer.parseInt(inputBean.getUserRoleLevel()));
//                u.setUserlevel(userlevel);
                //Change status to 'Activate'
                Status status = new Status();
                status.setStatuscode(CommonVarList.STATUS_ACTIVE);
                u.setStatus(status);

//                u.setUser(audit.getUser());
//                u.setLastupdatedtime(sysDate);
                session.save(audit);
                session.update(u);

                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    //end newly changed
    public String insertPage(UserRoleMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((Userrole) session.get(Userrole.class, inputBean.getUserRoleCode().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getUserRoleCode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUserRoleCode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.USER_ROLE_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

//    //update user
    public String updateUserRoleMgt(UserRoleMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUserRoleCode().trim())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getUserRoleCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.USER_ROLE_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

//            Userrole u = (Userrole) session.get(Userrole.class, inputBean.getUserRoleCode().trim());
//
//            if (u != null) {
//
//                String oldValue = u.getUserrolecode()
//                        + "|" + u.getDescription()
//                        + "|" + u.getStatus().getDescription();
//
//                u.setDescription(inputBean.getDescription().trim());
//
////                Userlevel userlevel = new Userlevel();
////                userlevel.setLevelid(Integer.parseInt(inputBean.getUserRoleLevel()));
////                u.setUserlevel(userlevel);
//                Status st = (Status) session.get(Status.class, inputBean.getStatus().trim());
//                u.setStatus(st);
//
//                u.setLastupdatedtime(sysDate);
//                u.setLastupdateduser(audit.getLastupdateduser());
//
//                String newValue = u.getUserrolecode()
//                        + "|" + u.getDescription()
//                        + "|" + u.getStatus().getDescription();
//
//                audit.setOldvalue(oldValue);
//                audit.setNewvalue(newValue);
//                audit.setCreatetime(sysDate);
//                audit.setLastupdatedtime(sysDate);
//
//                session.save(audit);
//                session.update(u);
//
//                txn.commit();
//            } else {
//                message = MessageVarList.COMMON_NOT_EXISTS;
//            }
//        } catch (Exception e) {
//            if (txn != null) {
//                txn.rollback();
//            }
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return message;
//    }
    //get search list
//    //delete section
    public String deleteUserRoleMgt(UserRoleMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Userrole u = (Userrole) session.get(Userrole.class, inputBean.getUserRoleCode().trim());
            if (u != null) {

                long count = 0;

                String sqlCount = "select count(userrolecode) from Userrolesection as u where u.userrole.userrolecode=:userrole";
                Query queryCount = session.createQuery(sqlCount).setString("userrole", inputBean.getUserRoleCode().trim());

                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();

                if (count > 0) {
                    message = MessageVarList.COMMON_NOT_DELETE;
                } else {
                    String sqlCount2 = "select count(userrolecode) from Sectionpage as u where u.userrole.userrolecode=:userrole";
                    Query queryCount2 = session.createQuery(sqlCount2).setString("userrole", inputBean.getUserRoleCode().trim());

                    Iterator itCount2 = queryCount2.iterate();
                    count = (Long) itCount2.next();
                    if (count > 0) {
                        message = MessageVarList.COMMON_NOT_DELETE;
                    } else {
                        String sqlCount3 = "select count(userrolecode) from Pagetask as u where u.userrole.userrolecode=:userrole";
                        Query queryCount3 = session.createQuery(sqlCount3).setString("userrole", inputBean.getUserRoleCode().trim());

                        Iterator itCount3 = queryCount3.iterate();
                        count = (Long) itCount3.next();
                        if (count > 0) {
                            message = MessageVarList.COMMON_NOT_DELETE;
                        } else {
                            String sqlCount4 = "select count(userrole) from Systemuser as u where u.userrole.userrolecode=:userrole";
                            Query queryCount4 = session.createQuery(sqlCount4).setString("userrole", inputBean.getUserRoleCode().trim());

                            Iterator itCount4 = queryCount4.iterate();
                            count = (Long) itCount4.next();
                            if (count > 0) {
                                message = MessageVarList.COMMON_NOT_DELETE;
                            } else {

                                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                                Query query = session.createQuery(sql)
                                        .setString("PKey", inputBean.getUserRoleCode())
                                        .setString("pagecode", audit.getPagecode());

                                if (query.list().isEmpty()) {

                                    Userrole k = (Userrole) session.get(Userrole.class, inputBean.getUserRoleCode().trim());
                                    if (k != null) {

                                        audit.setNewvalue(k.getUserrolecode() + "|" + k.getDescription() + "|" + k.getStatus().getStatuscode());
                                    }

                                    Pendingtask pendingtask = new Pendingtask();

                                    pendingtask.setPKey(inputBean.getUserRoleCode().trim());
                                    pendingtask.setFields(audit.getNewvalue());

                                    Task task = new Task();
                                    task.setTaskcode(TaskVarList.DELETE_TASK);
                                    pendingtask.setTask(task);

                                    Status st = new Status();
                                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                                    pendingtask.setStatus(st);

                                    Page page = (Page) session.get(Page.class, PageVarList.USER_ROLE_MGT_PAGE);
                                    pendingtask.setPage(page);

                                    pendingtask.setInputterbranch(sysUser.getBranch());

                                    pendingtask.setCreatedtime(sysDate);
                                    pendingtask.setLastupdatedtime(sysDate);
                                    pendingtask.setCreateduser(audit.getLastupdateduser());

                                    audit.setCreatetime(sysDate);
                                    audit.setLastupdatedtime(sysDate);
                                    audit.setLastupdateduser(audit.getLastupdateduser());

                                    session.save(audit);
                                    session.save(pendingtask);
                                    txn.commit();
                                } else {
                                    message = "pending available";
                                }

//                                audit.setCreatetime(sysDate);
//                                audit.setLastupdatedtime(sysDate);
//
//                                session.save(audit);
//                                session.delete(u);
//                                txn.commit();
                            }
                        }
                    }
                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

//    public List<UserRoleMgtBean> getSearchList(UserRoleMgtInputBean inputBean, int max, int first, String orderBy) throws Exception {
//
//        List<UserRoleMgtBean> dataList = new ArrayList<UserRoleMgtBean>();
//        Session session = null;
//        try {
//
//            if (orderBy.equals("") || orderBy == null) {
//                orderBy = " order by UR.CREATEDTIME desc ";
//            }
//
//            String where = this.makeWhereClause(inputBean);
//
//            BigDecimal count = new BigDecimal(0);
//            session = HibernateInit.sessionFactory.openSession();
//
//            String sqlCount = "select count(UR.USERROLECODE) from USERROLE UR where " + where;
//
//            Query queryCount = session.createSQLQuery(sqlCount);
//
//            List countList = queryCount.list();
//            count = (BigDecimal) countList.get(0);
//
//            if (count.longValue() > 0) {
//
////                String sqlSearch = " SELECT * from ( select UR.USERROLECODE, UR.DESCRIPTION AS ROLEDESC, UL.DESCRIPTION AS ULDESC, ST.DESCRIPTION AS STDESC, rownum r "
////                        + " from USERROLE UR,STATUS ST, USERLEVEL UL "
////                        + " where UR.STATUSCODE=ST.STATUSCODE AND UR.USERLEVEL=UL.LEVELID AND " 
////                        + where + orderBy + " ) where r > " + first + " and r<= " + max;
//                String sqlSearch = " SELECT * from ( select UR.USERROLECODE, UR.DESCRIPTION AS ROLEDESC, ST.DESCRIPTION AS STDESC, UR.CREATEDTIME , "
//                        + " row_number() over (" + orderBy + ") as r "
//                        + " from USERROLE UR,STATUS ST "
//                        + " where UR.STATUSCODE=ST.STATUSCODE AND "
//                        + where + " ) where r > " + first + " and r<= " + max;
//
//                List<Object[]> chequeList = (List<Object[]>) session.createSQLQuery(sqlSearch).list();
//
//                for (Object[] urBean : chequeList) {
//
//                    UserRoleMgtBean urMgtBean = new UserRoleMgtBean();
//
//                    try {
//                        urMgtBean.setUserRoleCode(urBean[0].toString());
//                    } catch (NullPointerException npe) {
//                        urMgtBean.setUserRoleCode("--");
//                    }
//                    try {
//                        urMgtBean.setDescription(urBean[1].toString());
//                    } catch (NullPointerException npe) {
//                        urMgtBean.setDescription("--");
//                    }
////                    try {
////                        urMgtBean.setUserRoleLevel(urBean[2].toString());
////                    } catch (NullPointerException npe) {
////                        urMgtBean.setUserRoleLevel("--");
////                    }
//                    try {
//                        urMgtBean.setStatus(urBean[2].toString());
//                    } catch (NullPointerException npe) {
//                        urMgtBean.setStatus("--");
//                    }
//                    try {
//                        if (urBean[3].toString() != null && !urBean[3].toString().isEmpty()) {
//                            urMgtBean.setCreatetime(urBean[3].toString().substring(0, 19));
//                        } else {
//                            urMgtBean.setCreatetime("--");
//                        }
//
//                    } catch (NullPointerException npe) {
//                        urMgtBean.setCreatetime("--");
//                    }
//
//                    urMgtBean.setFullCount(count.longValue());
//
//                    dataList.add(urMgtBean);
//                }
//            }
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//        return dataList;
//    }
//
//    private String makeWhereClause(UserRoleMgtInputBean inputBean) throws Exception {
//
//        String where = "1=1";
//
//        if (inputBean.getS_userrolecode() != null && !inputBean.getS_userrolecode().isEmpty()) {
//            where += " and lower(UR.USERROLECODE) like lower('%" + inputBean.getS_userrolecode() + "%')";
//        }
//        if (inputBean.getS_description() != null && !inputBean.getS_description().isEmpty()) {
//            where += " and lower(UR.DESCRIPTION) like lower('%" + inputBean.getS_description() + "%')";
//        }
//        if (inputBean.getS_status() != null && !inputBean.getS_status().isEmpty()) {
//            where += " and UR.STATUSCODE = '" + inputBean.getS_status() + "'";
//        }
//
//        return where;
//    }
    //find userrole by userrole code
    public Userrole findUserRoleById(String userRoleCode) throws Exception {
        Userrole userrole = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Userrole as u where u.userrolecode=:userrolecode";
            Query query = session.createQuery(sql).setString("userrolecode", userRoleCode);
            userrole = (Userrole) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userrole;

    }

    public List<UserRoleMgtBean> getSearchList(UserRoleMgtInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<UserRoleMgtBean> dataList = new ArrayList<UserRoleMgtBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(userrolecode) from Userrole as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Userrole u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    UserRoleMgtBean sdblUserrole = new UserRoleMgtBean();
                    Userrole userrole = (Userrole) it.next();

                    try {
                        sdblUserrole.setUserRoleCode(userrole.getUserrolecode());
                    } catch (NullPointerException npe) {
                        sdblUserrole.setUserRoleCode("--");
                    }
                    try {
                        sdblUserrole.setDescription(userrole.getDescription());
                    } catch (NullPointerException npe) {
                        sdblUserrole.setDescription("--");
                    }
                    try {
                        sdblUserrole.setStatus(userrole.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        sdblUserrole.setStatus("--");
                    }

                    try {
                        if (userrole.getCreatetime().toString() != null && !userrole.getCreatetime().toString().isEmpty()) {
                            sdblUserrole.setCreatetime(userrole.getCreatetime().toString().substring(0, 19));
                        } else {
                            sdblUserrole.setCreatetime("--");
                        }
                    } catch (NullPointerException npe) {
                        sdblUserrole.setCreatetime("--");
                    }

//                    try {
//                        sdblUserrole.setUpmWorkclass(userrole.getUserrolecode().toString());
//                    } catch (NullPointerException npe) {
//                        sdblUserrole.setUpmWorkclass("--");
//                    }
                    sdblUserrole.setFullCount(count);

                    dataList.add(sdblUserrole);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(UserRoleMgtInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getUserRoleCodeSearch() != null && !inputBean.getUserRoleCodeSearch().isEmpty()) {
            where += " and lower(u.userrolecode) like lower('%" + inputBean.getUserRoleCodeSearch().trim() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
//        if (inputBean.getUserLevelSearch() != null) {
//             where += " and u.userrolecode = '" + inputBean.getUserLevelSearch() + "'";
//        }
        return where;
    }

    public List<UserRoleMgtPendBean> getPendingUserRoleList(UserRoleMgtInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<UserRoleMgtPendBean> dataList = new ArrayList<UserRoleMgtPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.USER_ROLE_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.USER_ROLE_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    UserRoleMgtPendBean pendUserRoleBean = new UserRoleMgtPendBean();
                    Pendingtask penduserrole = (Pendingtask) it.next();

                    try {
                        pendUserRoleBean.setId(Long.toString(penduserrole.getId()));
                    } catch (NullPointerException npe) {
                        pendUserRoleBean.setId("--");
                    }
                    try {
                        pendUserRoleBean.setOperation(penduserrole.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        pendUserRoleBean.setOperation("--");
                    }

                    String[] penArray = null;
                    if (penduserrole.getFields() != null) {
                        penArray = penduserrole.getFields().split("\\|");
                    }

                    try {
                        pendUserRoleBean.setUserrolecode(penArray[0]);
                    } catch (NullPointerException npe) {
                        pendUserRoleBean.setUserrolecode("--");
                    } catch (Exception ex) {
                        pendUserRoleBean.setUserrolecode("--");
                    }
                    try {
                        pendUserRoleBean.setFields(penduserrole.getFields());
                    } catch (NullPointerException npe) {
                        pendUserRoleBean.setFields("--");
                    } catch (Exception ex) {
                        pendUserRoleBean.setFields("--");
                    }
                    try {
                        pendUserRoleBean.setStatus(penduserrole.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        pendUserRoleBean.setStatus("--");
                    }
                    try {
                        pendUserRoleBean.setCreateduser(penduserrole.getCreateduser());
                    } catch (NullPointerException npe) {
                        pendUserRoleBean.setCreateduser("--");
                    } catch (Exception ex) {
                        pendUserRoleBean.setCreateduser("--");
                    }

                    pendUserRoleBean.setFullCount(count);

                    dataList.add(pendUserRoleBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String confirmUserRole(UserRoleMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            String oldVal = "";
            String newVal = "";

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    Userrole ur = new Userrole();
                    ur.setUserrolecode(penArray[0]);
                    ur.setDescription(penArray[1]);

                    Status st = (Status) session.get(Status.class, penArray[2].trim());
                    ur.setStatus(st);

                    ur.setCreatetime(sysDate);
                    ur.setLastupdatedtime(sysDate);
                    ur.setLastupdateduser(audit.getLastupdateduser());

                    audit.setNewvalue(ur.getUserrolecode() + "|" + ur.getDescription() + "|" + ur.getStatus().getDescription());
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on user role ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    session.save(ur);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Userrole u = (Userrole) session.get(Userrole.class, penArray[0]);

                    if (u != null) {

                        audit.setOldvalue(u.getUserrolecode() + "|" + u.getDescription() + "|" + u.getStatus().getDescription());

                        u.setDescription(penArray[1]);
                        Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(st);

                        u.setLastupdateduser(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getStatus().getDescription());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on user role ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    Userrole u = (Userrole) session.get(Userrole.class, penArray[0]);
                    if (u != null) {
                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getStatus().getDescription());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on user role ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectUserRole(UserRoleMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String userrolecode = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Userrole userrole = (Userrole) session.get(Userrole.class, fieldsArray[0]);

                    if (userrole != null) {

                        audit.setOldvalue(userrole.getUserrolecode() + "|" + userrole.getDescription() + "|" + userrole.getStatus().getStatuscode());
                    }
                }
//                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on user role( code : " + userrolecode  + ")  inputted by " + u.getCreateduser() + " rejected "+ audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

}
