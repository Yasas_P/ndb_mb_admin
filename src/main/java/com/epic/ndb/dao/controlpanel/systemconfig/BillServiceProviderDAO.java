/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.BillServiceProviderBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BillServiceProviderInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BillServiceProviderPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.BillBillerType;
import com.epic.ndb.util.mapping.BillCustomField;
import com.epic.ndb.util.mapping.BillFieldType;
import com.epic.ndb.util.mapping.BillServiceProvider;
import com.epic.ndb.util.mapping.BillerCategory;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.ProductCurrency;
import com.epic.ndb.util.mapping.RegularExpression;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author prathibha_w
 */
public class BillServiceProviderDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<BillServiceProviderBean> getSearchList(BillServiceProviderInputBean inputBean, int max, int first, String orderBy) {
        List<BillServiceProviderBean> dataList = new ArrayList<BillServiceProviderBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.lastupdatedtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(providerId) from BillServiceProvider as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from BillServiceProvider u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    BillServiceProviderBean jcode = new BillServiceProviderBean();
                    BillServiceProvider jc = (BillServiceProvider) it.next();

                    try {
                        jcode.setProviderId(jc.getProviderId());
                    } catch (NullPointerException npe) {
                        jcode.setProviderId("--");
                    }
                    try {
                        jcode.setProviderName(jc.getProviderName());
                    } catch (NullPointerException npe) {
                        jcode.setProviderName("--");
                    }
                    try {
                        jcode.setDescription(jc.getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setDescription("--");
                    }
                    try {
                        jcode.setCategoryCode(jc.getBillCategory().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setCategoryCode("--");
                    }
                    try {
                        jcode.setImageUrl(jc.getImageUrl());
                    } catch (NullPointerException npe) {
                        jcode.setImageUrl("--");
                    }
                    try {
                        jcode.setDisplayName(jc.getDisplayName());
                    } catch (NullPointerException npe) {
                        jcode.setDisplayName("--");
                    }
                    try {
                        jcode.setCollectionAccount(jc.getCollectionAccount().toString());
                    } catch (NullPointerException npe) {
                        jcode.setCollectionAccount("--");
                    }
                    try {
                        jcode.setBankCode(jc.getBankCode().toString());
                    } catch (NullPointerException npe) {
                        jcode.setBankCode("--");
                    }
                    try {
                        jcode.setPayType(jc.getPayType());
                    } catch (NullPointerException npe) {
                        jcode.setPayType("--");
                    }
                    try {
                        jcode.setBillreTypeCode(jc.getBillBillerType().getBillerTypeDescription());
                    } catch (NullPointerException npe) {
                        jcode.setBillreTypeCode("--");
                    }
                    try {
                        jcode.setStatus(jc.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCurrency(jc.getCurrency().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setCurrency("--");
                    }
                    try {
                        jcode.setFeeCharge(jc.getChargeCode().trim());
                    } catch (Exception npe) {
                        jcode.setFeeCharge("--");
                    }
                    try {
                        if (jc.getProductType() != null) {
                            String productTypeDes = this.getProductTypeDesByCode(jc.getProductType().toString());
                            jcode.setProductType(productTypeDes);
                        } else {
                            jcode.setProductType("--");
                        }
                    } catch (Exception npe) {
                        jcode.setFeeCharge("--");
                    }
                    try {
                        jcode.setMobPrefix(jc.getMobPrefix().trim());
                    } catch (Exception npe) {
                        jcode.setMobPrefix("--");
                    }
                    try {
                        jcode.setBillerValCode(jc.getBillerValCode().trim());
                    } catch (Exception npe) {
                        jcode.setBillerValCode("--");
                    }
                    try {
                        jcode.setValidationStatus(jc.getValidationStatus().getDescription());
                    } catch (Exception npe) {
                        jcode.setValidationStatus("--");
                    }
//                    try {
//                        jcode.setPlaceHolder(jc.getPlaceHolder().trim());
//                    } catch (Exception npe) {
//                        jcode.setPlaceHolder("--");
//                    }
                    try {
                        jcode.setMaker(jc.getMaker());
                    } catch (NullPointerException npe) {
                        jcode.setMaker("--");
                    }
                    try {
                        jcode.setChecker(jc.getChecker());
                    } catch (NullPointerException npe) {
                        jcode.setChecker("--");
                    }
                    try {
                        jcode.setCreatedtime(jc.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setCreatedtime("--");
                    }
                    try {
                        jcode.setLastupdatedtime(jc.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setLastupdatedtime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(BillServiceProviderInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getProviderIdSearch() != null && !inputBean.getProviderIdSearch().isEmpty()) {
            where += " and lower(u.providerId) like lower('%" + inputBean.getProviderIdSearch().trim() + "%')";
        }
        if (inputBean.getProviderNameSearch() != null && !inputBean.getProviderNameSearch().isEmpty()) {
            where += " and lower(u.providerName) like lower('%" + inputBean.getProviderNameSearch().trim() + "%')";
        }
        if (inputBean.getCategoryCodeSearch() != null && !inputBean.getCategoryCodeSearch().isEmpty()) {
            where += " and lower(u.billCategory.billercatcode) like lower('%" + inputBean.getCategoryCodeSearch().trim() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch().trim() + "%')";
        }
        if (inputBean.getCollectionAccountSearch() != null && !inputBean.getCollectionAccountSearch().isEmpty()) {
            where += " and lower(u.collectionAccount) like lower('%" + inputBean.getCollectionAccountSearch().trim() + "%')";
        }
        if (inputBean.getBillerTypeCodeSearch() != null && !inputBean.getBillerTypeCodeSearch().isEmpty()) {
            where += " and lower(u.billBillerType.billerTypeCode) like lower('%" + inputBean.getBillerTypeCodeSearch().trim() + "%')";
        }
        if (inputBean.getDisplayNameSearch() != null && !inputBean.getDisplayNameSearch().isEmpty()) {
            where += " and lower(u.displayName) like lower('%" + inputBean.getDisplayNameSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        return where;
    }

    public List<BillServiceProviderPendBean> getPendingSPList(BillServiceProviderInputBean inputBean, int max, int first, String orderBy) {
        List<BillServiceProviderPendBean> dataList = new ArrayList<BillServiceProviderPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.BILLER_SERVICE_PROVIDER_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.BILLER_SERVICE_PROVIDER_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    BillServiceProviderPendBean jcode = new BillServiceProviderPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        jcode.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setOperation(pTask.getTask().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        jcode.setProviderId(penArray[0]);
                    } catch (NullPointerException npe) {
                        jcode.setProviderId("--");
                    } catch (Exception ex) {
                        jcode.setProviderId("--");
                    }
                    try {
                        jcode.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        jcode.setFields("--");
                    } catch (Exception ex) {
                        jcode.setFields("--");
                    }
                    try {
                        jcode.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertBillSP(BillServiceProviderInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((BillServiceProvider) session.get(BillServiceProvider.class, inputBean.getProviderId().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getProviderId())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getProviderId().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.BILLER_SERVICE_PROVIDER_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String updateBillSP(BillServiceProviderInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getProviderId())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                BillServiceProvider u = (BillServiceProvider) session.get(BillServiceProvider.class, inputBean.getProviderId().trim());
//                BillCustomField c = (BillCustomField) session.get(BillCustomField.class, Long.parseLong(inputBean.getProviderId().trim()));

                BillCustomField c = this.findCusFieldByProviderId(inputBean.getProviderId().trim());

                String cusfield = "";
                String chargeCode = "";
                String collectionAccount = "";
                String bankCode = "";
                String mobPrefix = "";
                String validationStatus = "";
                String billerValCode = "";

                if (c != null) {
                    cusfield = "|" + c.getFieldName() + "|"
                            + c.getBillFieldType().getName() + "|"
                            + c.getFieldValue() + "|"
                            + c.getFieldLength() + "|"
                            + c.getFieldValidation() + "|"
                            + c.getPlaceHolder();
                }

                if (u != null) {
                    if (u.getChargeCode() != null) {
                        chargeCode = u.getChargeCode();
                    }
                    if (u.getCollectionAccount() != null) {
                        collectionAccount = u.getCollectionAccount();
                    }
                    if (u.getBankCode() != null) {
                        bankCode = u.getBankCode();
                    }
                    if (u.getBillerValCode()!= null) {
                        billerValCode = u.getBillerValCode();
                    }
                    if (u.getValidationStatus()!= null) {
                        validationStatus = u.getValidationStatus().getStatuscode();
                    }
                    if (u.getMobPrefix()!= null) {
                        mobPrefix = u.getMobPrefix();
                    }
                    audit.setOldvalue(
                            u.getProviderId() + "|"
                            + u.getProviderName() + "|"
                            + u.getDescription() + "|"
                            + u.getBillCategory().getBillercatcode() + "|"
                            + u.getImageUrl() + "|"
                            + u.getDisplayName() + "|"
                            + collectionAccount + "|"
                            + bankCode + "|"
                            + u.getPayType() + "|"
                            + u.getBillBillerType().getBillerTypeCode() + "|"
                            + u.getStatus().getStatuscode() + "|"
                            + u.getCurrency().getCurrencyCode() + "|"
                            + chargeCode + "|"
                            + u.getProductType().toString() + "|"       
                            + mobPrefix+ "|"  
                            + billerValCode + "|"
                            + validationStatus+ "|"       
                            //                            + u.getPlaceHolder() + "|"
                            + u.getIsHavingCustomField()
                            + cusfield
                    );
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getProviderId().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.BILLER_SERVICE_PROVIDER_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteBillSP(BillServiceProviderInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getProviderId())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                BillServiceProvider u = (BillServiceProvider) session.get(BillServiceProvider.class, inputBean.getProviderId().trim());
//                BillCustomField c = (BillCustomField) session.get(BillCustomField.class, Long.parseLong(inputBean.getProviderId().trim()));

                BillCustomField c = this.findCusFieldByProviderId(inputBean.getProviderId().trim());
                String cusfield = "";
                String fieldTypeOld = "";
                String fieldValidationOld = "";
                String fieldLengthOld = "";
                String fieldValueOld = "";
                String placeHolderOld = "";

                String chargeCode = "";
                String collectionAccount = "";
                String bankCode = "";
                String mobPrefix = "";
                String validationStatus = "";
                String billerValCode = "";
                if (c != null) {
                    if (c.getBillFieldType() != null) {
                        fieldTypeOld = String.valueOf(c.getBillFieldType().getId());
                    }
                    if (c.getBillFieldType().getId() == 3 || c.getBillFieldType().getId() == 2) {
                        fieldValueOld = c.getFieldValue();//for audit value
                    } else if (c.getBillFieldType().getId() == 1) {
                        fieldLengthOld = c.getFieldLength();//for audit  Value
                        placeHolderOld = c.getPlaceHolder();//for audit  Value
                        if (c.getFieldValidation() != null && !c.getFieldValidation().isEmpty()) {
                            fieldValidationOld = c.getFieldValidation();//for audit Value
                        }
                    }
                    cusfield = "|" + c.getFieldName() + "|"
                            + fieldTypeOld + "|"
                            + fieldValueOld + "|"
                            + fieldLengthOld + "|"
                            + fieldValidationOld + "|"
                            + placeHolderOld;
                }

                if (u != null) {
                    if (u.getChargeCode() != null) {
                        chargeCode = u.getChargeCode();
                    }
                    if (u.getCollectionAccount() != null) {
                        collectionAccount = u.getCollectionAccount();
                    }
                    if (u.getBankCode() != null) {
                        bankCode = u.getBankCode();
                    }
                    if (u.getBillerValCode()!= null) {
                        billerValCode = u.getBillerValCode();
                    }
                    if (u.getValidationStatus()!= null) {
                        validationStatus = u.getValidationStatus().getStatuscode();
                    }
                    if (u.getMobPrefix()!= null) {
                        mobPrefix = u.getMobPrefix();
                    }
                    audit.setNewvalue(
                            u.getProviderId() + "|"
                            + u.getProviderName() + "|"
                            + u.getDescription() + "|"
                            + u.getBillCategory().getBillercatcode() + "|"
                            + u.getImageUrl() + "|"
                            + u.getDisplayName() + "|"
                            + collectionAccount + "|"
                            + bankCode + "|" + "|"//for commet payType
                            //                            + u.getPayType() + "|"
                            + u.getBillBillerType().getBillerTypeCode() + "|"
                            + u.getStatus().getStatuscode() + "|"
                            + u.getCurrency().getCurrencyCode() + "|"
                            + chargeCode + "|"
                            + u.getProductType().toString() + "|"
                            + mobPrefix + "|"       
                            + billerValCode + "|"       
                            + validationStatus + "|"       
                            //                            + u.getPlaceHolder() + "|"
                            + u.getIsHavingCustomField()
                            + cusfield
                    );
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getProviderId().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.BILLER_SERVICE_PROVIDER_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmBillSP(BillServiceProviderInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());

                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    BillServiceProvider billSpn = (BillServiceProvider) session.get(BillServiceProvider.class, penArray[0].trim());

                    if (billSpn == null) {
                        BillServiceProvider billSp = new BillServiceProvider();

                        BillCustomField billCf = new BillCustomField();

                        billSp.setProviderId(penArray[0]);
                        billSp.setProviderName(penArray[1]);
                        billSp.setDescription(penArray[2]);

                        BillerCategory bc = (BillerCategory) session.get(BillerCategory.class, penArray[3].trim());
                        billSp.setBillCategory(bc);

                        billSp.setImageUrl(penArray[4]);
                        billSp.setDisplayName(penArray[5]);
                        billSp.setCollectionAccount(penArray[6]);
                        billSp.setBankCode(penArray[7]);
                        billSp.setPayType("Y");

                        BillBillerType bt = (BillBillerType) session.get(BillBillerType.class, penArray[9].trim());
                        billSp.setBillBillerType(bt);

                        Status st = (Status) session.get(Status.class, penArray[10].trim());
                        billSp.setStatus(st);

                        ProductCurrency pc = (ProductCurrency) session.get(ProductCurrency.class, penArray[11].trim());
                        billSp.setCurrency(pc);

                        if (penArray[12] != null && !penArray[12].isEmpty()) {
                            billSp.setChargeCode(penArray[12].trim());
                        }

                        billSp.setProductType(new BigDecimal(penArray[13]));
                        billSp.setMobPrefix(penArray[14]);
                        billSp.setBillerValCode(penArray[15]);

                        if (penArray[16] != null && !penArray[16].isEmpty()) {
                            Status vst = (Status) session.get(Status.class, penArray[16].trim());
                            billSp.setValidationStatus(vst);
                        }
                        billSp.setIsHavingCustomField(Boolean.parseBoolean(penArray[17].trim()));

                        billSp.setCreatedtime(sysDate);
                        billSp.setLastupdatedtime(sysDate);
                        billSp.setMaker(pentask.getCreateduser());
                        billSp.setChecker(audit.getLastupdateduser());

//                        String customField = "";
                        if (penArray[17].equals("true")) {

                            billCf.setBillServiceProvider(billSp);
                            billCf.setFieldName(penArray[18]);

                            BillFieldType ft = (BillFieldType) session.get(BillFieldType.class, Integer.parseInt(penArray[19].trim()));
                            billCf.setBillFieldType(ft);
                            try {
                                billCf.setFieldValue(penArray[20]);
                            } catch (Exception e) {
                                billCf.setFieldValue("");
                            }
                            try {
                                billCf.setFieldLength(penArray[21]);
                            } catch (Exception e) {
                                billCf.setFieldLength("");
                            }
                            try {
                                billCf.setFieldValidation(penArray[22]);
                            } catch (Exception e) {
                                billCf.setFieldValidation("");
                            }
                            try {
                                billCf.setPlaceHolder(penArray[23]);
                            } catch (Exception e) {
                                billCf.setFieldValidation("");
                            }
                            session.save(billCf);

//                            customField = "|" + billCf.getFieldName() + "|"
//                                    + billCf.getBillFieldType().getName() + "|"
//                                    + billCf.getFieldValue() + "|"
//                                    + billCf.getFieldLength() + "|"
//                                    + billCf.getFieldValidation() + "|"
//                                    + billCf.getPlaceHolder();
                        }

//                        audit.setNewvalue(
//                                billSp.getProviderId() + "|"
//                                + billSp.getProviderName() + "|"
//                                + billSp.getDescription() + "|"
//                                + billSp.getBillCategory().getDescription() + "|"
//                                + billSp.getImageUrl() + "|"
//                                + billSp.getDisplayName() + "|"
//                                + billSp.getCollectionAccount() + "|"
//                                + billSp.getBankCode() + "|"
//                                + billSp.getPayType() + "|"
//                                + billSp.getBillBillerType().getBillerTypeDescription() + "|"
//                                + billSp.getStatus().getDescription() + "|"
//                                + billSp.getCurrency().getDescription() + "|"
//                                + billSp.getChargeCode() + "|"
//                                + billSp.getProductType().toString() + "|"
//                                //                                + billSp.getPlaceHolder() + "|"
//                                + billSp.getIsHavingCustomField()
//                                + customField
//                        );
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on bill service provider ( bill service provider : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.save(billSp);
                    } else {
                        message = MessageVarList.COMMON_ALREADY_EXISTS;
                    }

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    BillServiceProvider billSp = (BillServiceProvider) session.get(BillServiceProvider.class, penArray[0].trim());

                    BillCustomField billCf = this.findCusFieldByProviderId(penArray[0].trim());

                    String customField_old = "";
//                    String customField_new = "";
                    String fieldTypeOld = "";
                    String fieldValidationOld = "";
                    String fieldLengthOld = "";
                    String fieldValueOld = "";
                    String placeHolderOld = "";

                    String billerType = "";
                    String collectionAccount = "";
                    String bankCode = "";
                    String feeCharge = "";
                    String mobPrefix = "";
                    String validationStatus = "";
                    String billerValCode = "";

                    if (billSp != null) {
                        //-------------------------audit old Value start------------------------------
                        if (billCf != null) {

                            if (billCf.getBillFieldType() != null) {
                                fieldTypeOld = String.valueOf(billCf.getBillFieldType().getId());
                            }
                            if (billCf.getBillFieldType().getId() == 3 || billCf.getBillFieldType().getId() == 2) {
                                fieldValueOld = billCf.getFieldValue();//for old value
                            } else if (billCf.getBillFieldType().getId() == 1) {
                                placeHolderOld = billCf.getPlaceHolder();//for old value
                                fieldLengthOld = billCf.getFieldLength();//for old Value
                                if (billCf.getFieldValidation() != null && !billCf.getFieldValidation().isEmpty()) {
                                    fieldValidationOld = billCf.getFieldValidation();//for old Value
                                }
                            }
                            customField_old = "|" + billCf.getFieldName() + "|"
                                    + fieldTypeOld + "|"
                                    + fieldValueOld + "|"
                                    + fieldLengthOld + "|"
                                    + fieldValidationOld + "|"
                                    + placeHolderOld;
                        }
                        if (billSp.getBillBillerType() != null) {
                            billerType = billSp.getBillBillerType().getBillerTypeDescription();
                        }
                        if (billSp.getCollectionAccount() != null && !billSp.getCollectionAccount().isEmpty()) {
                            collectionAccount = billSp.getCollectionAccount();
                        }
                        if (billSp.getBankCode() != null && !billSp.getBankCode().isEmpty()) {
                            bankCode = billSp.getBankCode();
                        }
                        if (billSp.getChargeCode() != null && !billSp.getChargeCode().isEmpty()) {
                            feeCharge = billSp.getChargeCode();
                        }
                        if (billSp.getMobPrefix() != null && !billSp.getMobPrefix().isEmpty()) {
                            mobPrefix = billSp.getMobPrefix();
                        }
                        if (billSp.getBillerValCode()!= null && !billSp.getBillerValCode().isEmpty()) {
                            billerValCode = billSp.getBillerValCode();
                        }
                        if (billSp.getValidationStatus()!= null ) {
                            validationStatus = billSp.getValidationStatus().getStatuscode();
                        }
                        audit.setOldvalue(
                                billSp.getProviderId() + "|"
                                + billSp.getProviderName() + "|"
                                + billSp.getDescription() + "|"
                                + billSp.getBillCategory().getDescription() + "|"
                                + billSp.getImageUrl() + "|"
                                + billSp.getDisplayName() + "|"
                                + collectionAccount + "|"
                                + bankCode + "|"
                                + billSp.getPayType() + "|"
                                + billerType + "|"
                                + billSp.getStatus().getDescription() + "|"
                                + billSp.getCurrency().getDescription() + "|"
                                + feeCharge + "|"
                                + billSp.getProductType().toString() + "|"
                                + mobPrefix + "|"
                                + billerValCode + "|"
                                + validationStatus + "|"
                                //                                + billSp.getPlaceHolder() + "|"
                                + billSp.getIsHavingCustomField()
                                + customField_old
                        );

                        //-------------------------audit old Value end------------------------------
                        billSp.setProviderName(penArray[1]);
                        billSp.setDescription(penArray[2]);

                        BillerCategory bc = (BillerCategory) session.get(BillerCategory.class, penArray[3].trim());
                        billSp.setBillCategory(bc);

                        billSp.setImageUrl(penArray[4]);
                        billSp.setDisplayName(penArray[5]);
                        billSp.setCollectionAccount(penArray[6]);
                        billSp.setBankCode(penArray[7]);
                        billSp.setPayType("Y");

                        BillBillerType bt = (BillBillerType) session.get(BillBillerType.class, penArray[9].trim());
                        billSp.setBillBillerType(bt);

                        Status st = (Status) session.get(Status.class, penArray[10].trim());
                        billSp.setStatus(st);

                        ProductCurrency pc = (ProductCurrency) session.get(ProductCurrency.class, penArray[11].trim());
                        billSp.setCurrency(pc);

                        if (penArray[12] != null && !penArray[12].isEmpty()) {
                            billSp.setChargeCode(penArray[12].trim());
                        } else {
                            billSp.setChargeCode("");
                        }

                        billSp.setProductType(new BigDecimal(penArray[13]));
                        billSp.setMobPrefix(penArray[14]);
                        billSp.setBillerValCode(penArray[15]);

                        if (penArray[16] != null && !penArray[16].isEmpty()) {
                            Status vst = (Status) session.get(Status.class, penArray[16].trim());
                            billSp.setValidationStatus(vst);
                        }else{
                            billSp.setValidationStatus(null);
                        }

//                        if (penArray[14] != null) {
//                            billSp.setPlaceHolder(penArray[14]);
//                        }
                        billSp.setIsHavingCustomField(Boolean.parseBoolean(penArray[17].trim()));

                        billSp.setMaker(pentask.getCreateduser());
                        billSp.setChecker(audit.getLastupdateduser());
                        billSp.setLastupdatedtime(sysDate);

                        if (penArray[17].equals("true")) {
                            if (billCf == null) {//for new customer field
                                billCf = new BillCustomField();
                                billCf.setBillServiceProvider(billSp);
                            }
                            billCf.setFieldName(penArray[18]);

                            BillFieldType ft = (BillFieldType) session.get(BillFieldType.class, Integer.parseInt(penArray[19].trim()));
                            billCf.setBillFieldType(ft);

                            try {
                                billCf.setFieldValue(penArray[20]);
                            } catch (Exception e) {
                                billCf.setFieldValue("");
                            }
                            try {
                                billCf.setFieldLength(penArray[21]);
                            } catch (Exception e) {
                                billCf.setFieldLength("");
                            }
                            try {
                                billCf.setFieldValidation(penArray[22]);
                            } catch (Exception e) {
                                billCf.setFieldValidation("");
                            }
                            try {
                                billCf.setPlaceHolder(penArray[23]);
                            } catch (Exception e) {
                                billCf.setPlaceHolder("");
                            }
                            session.saveOrUpdate(billCf);

//                            if (billCf.getBillFieldType() != null) {
//                                fieldType = billCf.getBillFieldType().getName();
//                            }
//                            customField_new = "|" + billCf.getFieldName() + "|"
//                                    + fieldType + "|"
//                                    + billCf.getFieldValue() + "|"
//                                    + billCf.getFieldLength() + "|"
//                                    + billCf.getFieldValidation() + "|"
//                                    + billCf.getPlaceHolder();
                        }
//                        if (billSp.getBillBillerType() != null) {
//                            billerType = billSp.getBillBillerType().getBillerTypeDescription();
//                        }
//                        audit.setNewvalue(
//                                billSp.getProviderId() + "|"
//                                + billSp.getProviderName() + "|"
//                                + billSp.getDescription() + "|"
//                                + billSp.getBillCategory().getDescription() + "|"
//                                + billSp.getImageUrl() + "|"
//                                + billSp.getDisplayName() + "|"
//                                + billSp.getCollectionAccount() + "|"
//                                + billSp.getBankCode() + "|"
//                                + billSp.getPayType() + "|"
//                                + billerType + "|"
//                                + billSp.getStatus().getDescription() + "|"
//                                + billSp.getCurrency().getDescription() + "|"
//                                + billSp.getChargeCode() + "|"
//                                + billSp.getProductType().toString() + "|"
//                                //                                + billSp.getPlaceHolder() + "|"
//                                + billSp.getIsHavingCustomField()
//                                + customField_new
//                        );

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on bill service provider ( bill service provider : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.update(billSp);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    BillServiceProvider billSp = (BillServiceProvider) session.get(BillServiceProvider.class, penArray[0].trim());
                    BillCustomField billCf = this.findCusFieldByProviderId(penArray[0].trim());

                    if (billSp != null) {

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on bill service provider ( bill service provider : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(billSp);

                        if (billCf != null) {
                            session.delete(billCf);
                        }

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectBillSP(BillServiceProviderInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String code = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    BillServiceProvider billSp = (BillServiceProvider) session.get(BillServiceProvider.class, code.trim());

                    BillCustomField billCf = this.findCusFieldByProviderId(code.trim());

                    String customField_old = "";
//                    String customField_new = "";
                    String fieldTypeOld = "";
                    String fieldValidationOld = "";
                    String fieldLengthOld = "";
                    String fieldValueOld = "";
                    String placeHolderOld = "";

                    String billerType = "";
                    String collectionAccount = "";
                    String bankCode = "";
                    String feeCharge = "";
                    String mobPrefix = "";

                    if (billSp != null) {
                        //-------------------------audit old Value start------------------------------
                        if (billCf != null) {

                            if (billCf.getBillFieldType() != null) {
                                fieldTypeOld = String.valueOf(billCf.getBillFieldType().getId());
                            }
                            if (billCf.getBillFieldType().getId() == 3 || billCf.getBillFieldType().getId() == 2) {
                                fieldValueOld = billCf.getFieldValue();//for old value
                            } else if (billCf.getBillFieldType().getId() == 1) {
                                placeHolderOld = billCf.getPlaceHolder();//for old value
                                fieldLengthOld = billCf.getFieldLength();//for old Value
                                if (billCf.getFieldValidation() != null && !billCf.getFieldValidation().isEmpty()) {
                                    fieldValidationOld = billCf.getFieldValidation();//for old Value
                                }
                            }
                            customField_old = "|" + billCf.getFieldName() + "|"
                                    + fieldTypeOld + "|"
                                    + fieldValueOld + "|"
                                    + fieldLengthOld + "|"
                                    + fieldValidationOld + "|"
                                    + placeHolderOld;
                        }
                        if (billSp.getBillBillerType() != null) {
                            billerType = billSp.getBillBillerType().getBillerTypeDescription();
                        }
                        if (billSp.getCollectionAccount() != null && !billSp.getCollectionAccount().isEmpty()) {
                            collectionAccount = billSp.getCollectionAccount();
                        }
                        if (billSp.getBankCode() != null && !billSp.getBankCode().isEmpty()) {
                            bankCode = billSp.getBankCode();
                        }
                        if (billSp.getChargeCode() != null && !billSp.getChargeCode().isEmpty()) {
                            feeCharge = billSp.getChargeCode();
                        }
                        if (billSp.getMobPrefix() != null && !billSp.getMobPrefix().isEmpty()) {
                            mobPrefix = billSp.getMobPrefix();
                        }
                        audit.setOldvalue(
                                billSp.getProviderId() + "|"
                                + billSp.getProviderName() + "|"
                                + billSp.getDescription() + "|"
                                + billSp.getBillCategory().getDescription() + "|"
                                + billSp.getImageUrl() + "|"
                                + billSp.getDisplayName() + "|"
                                + collectionAccount + "|"
                                + bankCode + "|"
                                + billSp.getPayType() + "|"
                                + billerType + "|"
                                + billSp.getStatus().getDescription() + "|"
                                + billSp.getCurrency().getDescription() + "|"
                                + feeCharge + "|"
                                + billSp.getProductType().toString() + "|"
                                + mobPrefix + "|"
                                //                                + billSp.getPlaceHolder() + "|"
                                + billSp.getIsHavingCustomField()
                                + customField_old
                        );

                        //-------------------------audit old Value end------------------------------
                    }
                }
                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on bill service provider ( bill service provider : " + code + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public BillServiceProvider findBillSPById(String providerId) {
        BillServiceProvider billsp = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from BillServiceProvider as u where u.providerId=:providerId";
            Query query = session.createQuery(sql).setString("providerId", providerId);
            System.out.print(query);
            billsp = (BillServiceProvider) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return billsp;
    }

    public BillCustomField findCusFieldByProviderId(String providerId) {
        BillCustomField billcf = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from BillCustomField as u where u.billServiceProvider.providerId =:providerId";
            Query query = session.createQuery(sql).setString("providerId", providerId);
            System.out.print(query);

            if (query.list().size() > 0) {
                billcf = (BillCustomField) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return billcf;
    }

    public String viwPendBillSP(BillServiceProviderInputBean inputBean) throws Exception {

        Session session = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                String[] penArray = null;
                if (u.getFields() != null) {
                    penArray = u.getFields().split("\\|");
                }
                inputBean.setProviderId(penArray[0]);
                inputBean.setProviderName(penArray[1]);
                inputBean.setDescription(penArray[2]);

                BillerCategory billerCategory = (BillerCategory) session.get(BillerCategory.class, penArray[3]);
                inputBean.setCategoryCode(billerCategory.getDescription());

                inputBean.setImageUrl(penArray[4]);
                inputBean.setDisplayName(penArray[5]);

                if (penArray[6] != null && !penArray[6].isEmpty()) {
                    inputBean.setCollectionAccount(penArray[6].trim());
                } else {
                    inputBean.setCollectionAccount("--");
                }
                if (penArray[7] != null && !penArray[7].isEmpty()) {
                    inputBean.setBankCode(penArray[7].trim());
                } else {
                    inputBean.setBankCode("--");
                }

                BillBillerType billerType = (BillBillerType) session.get(BillBillerType.class, penArray[9]);
                inputBean.setBillerTypeCode(penArray[9]);
                inputBean.setBillerType(billerType.getBillerTypeDescription());

                Status status = (Status) session.get(Status.class, penArray[10]);
                inputBean.setStatus(status.getDescription());

                ProductCurrency pc = (ProductCurrency) session.get(ProductCurrency.class, penArray[11].trim());
                inputBean.setCurrency(pc.getDescription());

                if (penArray[12] != null && !penArray[12].isEmpty()) {
                    inputBean.setFeeCharge(penArray[12].trim());
                } else {
                    inputBean.setFeeCharge("--");
                }

                String productTypeDes = this.getProductTypeDesByCode(penArray[13]);
                inputBean.setProductType(productTypeDes);

                if (penArray[14] != null && !penArray[14].isEmpty()) {
                    inputBean.setMobPrefix(penArray[14]);
                } else {
                    inputBean.setMobPrefix("--");
                }

                if (penArray[15] != null && !penArray[15].isEmpty()) {
                    inputBean.setBillerValCode(penArray[15]);
                } else {
                    inputBean.setBillerValCode("--");
                }

                if (penArray[16] != null && !penArray[16].isEmpty()) {
                    Status Vstatus = (Status) session.get(Status.class, penArray[16]);
                    inputBean.setValidationStatus(Vstatus.getDescription());
                } else {
                    inputBean.setValidationStatus("--");
                }

//                if (penArray[14] != null && !penArray[14].isEmpty()) {
//                    inputBean.setPlaceHolder(penArray[14]);
//                } else {
//                    inputBean.setPlaceHolder("--");
//                }
                Boolean havingCustom = Boolean.valueOf(penArray[17]);
                inputBean.setHavingCustomField(havingCustom);

                if (havingCustom) {
                    inputBean.setFieldName(penArray[18]);

                    BillFieldType billFieldType = (BillFieldType) session.get(BillFieldType.class, Integer.parseInt(penArray[19]));
                    inputBean.setFieldIdType(penArray[19]);
                    inputBean.setFieldIdTypeDes(billFieldType.getName());
                    try {
                        if (penArray[20] != null && !penArray[20].isEmpty()) {
                            inputBean.setFieldVal(penArray[20]);
                        } else {
                            inputBean.setFieldVal("--");
                        }
                    } catch (Exception e) {
                        inputBean.setFieldVal("--");
                    }
                    try {
                        if (penArray[21] != null && !penArray[21].isEmpty()) {
                            inputBean.setFieldLen(penArray[21]);
                        } else {
                            inputBean.setFieldLen("--");
                        }
                    } catch (Exception e) {
                        inputBean.setFieldLen("--");
                    }
                    try {
                        if (penArray[23] != null && !penArray[23].isEmpty()) {
                            inputBean.setPlaceHolder(penArray[23]);
                        } else {
                            inputBean.setPlaceHolder("--");
                        }
                    } catch (Exception e) {
                        inputBean.setPlaceHolder("--");
                    }
//                    try {
//                        if (penArray[20] != null && !penArray[20].isEmpty()) {
//                            inputBean.setFieldValidation(penArray[20]);
//                        } else {
//                            inputBean.setFieldValidation("--");
//                        }
//                    } catch (Exception e) {
//                        inputBean.setFieldValidation("--");
//                    }
                    try {
                        //                    inputBean.setFieldValidation(cusfield.getFieldValidation());
                        if (penArray[22] != null && !penArray[22].isEmpty()) {
                            String fullFieldValidation = penArray[22];
                            int fieldValidationEndIndex = fullFieldValidation.indexOf("{");
                            int fieldMinLengthStartIndex = fullFieldValidation.indexOf("{") + 1;
                            int fieldMinLengthEndIndex = fullFieldValidation.indexOf(",");
                            String fieldValidation = fullFieldValidation.substring(0, fieldValidationEndIndex);
                            String fieldMinLength = fullFieldValidation.substring(fieldMinLengthStartIndex, fieldMinLengthEndIndex);

                            try {
                                String sql = "from RegularExpression as s where s.paramvalue=:paramvalue";
                                Query query = session.createQuery(sql).setString("paramvalue", fieldValidation);
                                RegularExpression regularExpression = (RegularExpression) query.list().get(0);
                                inputBean.setFieldValidation(regularExpression.getDescription());
                            } catch (Exception e) {
                                inputBean.setFieldValidation("--");
                            }
                            inputBean.setFieldLenMin(fieldMinLength);

                        } else {
                            inputBean.setFieldValidation("--");
                            inputBean.setFieldLenMin("--");
                        }
                    } catch (Exception e) {
                        inputBean.setFieldValidation("--");
                        inputBean.setFieldLenMin("--");
                    }

                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public StringBuffer makeCSVReport(BillServiceProviderInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String orderBy = "order by u.lastupdatedtime desc";
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from BillServiceProvider u where " + where + orderBy;
            String sqlBillerField = "from BillCustomField as u where u.billServiceProvider.providerId =:providerId";
            Query querySearch = session.createQuery(sqlSearch);

            Iterator it = querySearch.iterate();
            content = new StringBuffer();

            //write column headers to csv file
            content.append("Bill Provider ID");
            content.append(',');
            content.append("Bill Provider Name");
            content.append(',');
            content.append("Description");
            content.append(',');
            content.append("Bill Category");
            content.append(',');
            content.append("Display Name");
            content.append(',');
            content.append("Collection Account");
            content.append(',');
            content.append("Bank Code");
            content.append(',');
            content.append("Pay Type");
            content.append(',');
            content.append("Biller Type");
            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Currency");
            content.append(',');
            content.append("Fee Charge");
            content.append(',');
            content.append("Product Type");
            content.append(',');
            content.append("Mobile Prefix");
            content.append(',');
            content.append("Biller Value Code");
            content.append(',');
            content.append("Validation Status");
            content.append(',');
//            content.append("Place Holder");
//            content.append(',');
            content.append("Biller Custom Field");
            content.append(',');
            content.append("Field Name");
            content.append(',');
            content.append("Field Type");
            content.append(',');
            content.append("Field Length Min");
            content.append(',');
            content.append("Field Length Max");
            content.append(',');
            content.append("Field Validation");
            content.append(',');
            content.append("Place Holder");
            content.append(',');
            content.append("Field Value (JSON)");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Checker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');

            while (it.hasNext()) {

                BillServiceProvider serviceProvider = (BillServiceProvider) it.next();

                try {
                    content.append(serviceProvider.getProviderId());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getProviderName());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getDescription());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getBillCategory().getDescription());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getDisplayName());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getCollectionAccount().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getBankCode().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getPayType().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getBillBillerType().getBillerTypeDescription());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getStatus().getDescription());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getCurrency().getDescription());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getChargeCode().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    if (serviceProvider.getProductType() != null) {
                        String productTypeDes = this.getProductTypeDesByCode(serviceProvider.getProductType().toString());
                        content.append(productTypeDes);
                    } else {
                        content.append("--");
                    }
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(serviceProvider.getMobPrefix().toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(serviceProvider.getBillerValCode().toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getValidationStatus().getDescription());
                    content.append(',');
                } catch (Exception ex) {
                    content.append("--");
                    content.append(',');
                }
//                try {
//                    content.append(serviceProvider.getPlaceHolder().toString());
//                    content.append(',');
//                } catch (NullPointerException npe) {
//                    content.append("--");
//                    content.append(',');
//                }
                if (serviceProvider.getIsHavingCustomField()) {
                    content.append("Yes");
                    content.append(',');

                    Query queryBillerField = session.createQuery(sqlBillerField).setString("providerId", serviceProvider.getProviderId());
                    if (queryBillerField.list().size() > 0) {
                        BillCustomField billcf = (BillCustomField) queryBillerField.list().get(0);

                        try {
                            content.append(billcf.getFieldName().toString());
                            content.append(',');
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            content.append(billcf.getBillFieldType().getName().toString());
                            content.append(',');
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }

                        try {
                            if (billcf.getFieldValidation() != null && !billcf.getFieldValidation().isEmpty()) {
                                String fullFieldValidation = billcf.getFieldValidation();
                                int fieldValidationEndIndex = fullFieldValidation.indexOf("{");
                                int fieldMinLengthStartIndex = fullFieldValidation.indexOf("{") + 1;
                                int fieldMinLengthEndIndex = fullFieldValidation.indexOf(",");
                                String fieldValidation = fullFieldValidation.substring(0, fieldValidationEndIndex);
                                String fieldMinLength = fullFieldValidation.substring(fieldMinLengthStartIndex, fieldMinLengthEndIndex);

                                content.append(fieldMinLength);
                                content.append(',');
                                content.append(billcf.getFieldLength());
                                content.append(',');
                                try {
                                    String sql = "from RegularExpression as s where s.paramvalue=:paramvalue";
                                    Query query = session.createQuery(sql).setString("paramvalue", fieldValidation);
                                    RegularExpression regularExpression = (RegularExpression) query.list().get(0);

                                    content.append(regularExpression.getDescription());
                                    content.append(',');
                                } catch (Exception e) {
                                    content.append("--");
                                    content.append(',');
                                }

                            } else {
                                content.append("--");
                                content.append(',');
                                content.append("--");
                                content.append(',');
                                content.append("--");
                                content.append(',');
                            }
                            try {
                                content.append(billcf.getPlaceHolder().toString());
                                content.append(',');
                            } catch (NullPointerException npe) {
                                content.append("--");
                                content.append(',');
                            }
                        } catch (Exception e) {
                            content.append("--");
                            content.append(',');
                            content.append("--");
                            content.append(',');
                            content.append("--");
                            content.append(',');
                            content.append("--");
                            content.append(',');
                        }

                        try {
//                            content.append(Common.replaceCommaFieldUnderDoublequotation(billcf.getFieldValue().toString()));
                            content.append(Common.replaceCommaAndUnderDoubleFieldUnderDoublequotation(billcf.getFieldValue().toString()));
                            content.append(',');
                        } catch (NullPointerException npe) {
                            content.append("--");
                            content.append(',');
                        }
                    } else {
                        content.append("--");
                        content.append(',');
                        content.append("--");
                        content.append(',');
                        content.append("--");
                        content.append(',');
                        content.append("--");
                        content.append(',');
                        content.append("--");
                        content.append(',');
                        content.append("--");
                        content.append(',');
                        content.append("--");
                        content.append(',');
                    }
                } else {
                    content.append("No");
                    content.append(',');
                    content.append("--");
                    content.append(',');
                    content.append("--");
                    content.append(',');
                    content.append("--");
                    content.append(',');
                    content.append("--");
                    content.append(',');
                    content.append("--");
                    content.append(',');
                    content.append("--");
                    content.append(',');
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getChecker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getCreatedtime().toString().substring(0, 19));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(serviceProvider.getLastupdatedtime().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    content.append("--");
                }
                content.append('\n');
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    public String getProductTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("0")) {
            des = "Others";
        } else if (code.equals("1")) {
            des = "NDB";
        } else {
            des = code;
        }
        return des;

    }
}
