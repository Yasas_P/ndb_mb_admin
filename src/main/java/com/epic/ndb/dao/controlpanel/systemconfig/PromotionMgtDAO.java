/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.PromotionMgtBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PromotionMgtInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PromotionMgtPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.Notification;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Promotions;
import com.epic.ndb.util.mapping.PromotionsCategories;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.SwtMtNotificationType;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class PromotionMgtDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    private String FIRE_BASE_SQL = "select "
            + "g.API_KEY, " //0
            + "g.AUTH_DOMAIN, " //1
            + "g.DATABASE_URL, " //2
            + "g.PROJECT_ID , " //3
            + "g.STORAGE_BUCKET,  " //4
            + "g.MESSAGING_SENDER_ID, " //5
            + "g.APP_ID, " //6
            + "g.AUTH_EMAIL, " //7
            + "g.AUTH_PASSWORD, " //8
            + "g.ENVIRONMENT " //9
            + "from FIREBASE_CONFIG_PARAMETER g ";

    public List<PromotionMgtBean> getSearchList(PromotionMgtInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<PromotionMgtBean> dataList = new ArrayList<PromotionMgtBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
//                orderBy = "order by u.createdtime desc";
                orderBy = "order by LPAD(u.subregioncode,10) asc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(subregioncode) from Promotions as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Promotions u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    PromotionMgtBean promotionBean = new PromotionMgtBean();
                    Promotions promotion = (Promotions) it.next();

                    try {
                        promotionBean.setSubregioncode(promotion.getSubregioncode().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setSubregioncode("--");
                    }
                    try {
                        promotionBean.setStatus(promotion.getStatus().getDescription());
                    } catch (Exception npe) {
                        promotionBean.setStatus("--");
                    }
                    try {
                        promotionBean.setPromotionsCategories(promotion.getPromotionsCategories().getCatname());
                    } catch (Exception npe) {
                        promotionBean.setPromotionsCategories("--");
                    }
                    try {
                        promotionBean.setSubregionname(promotion.getSubregionname().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setSubregionname("--");
                    }
                    try {
                        promotionBean.setSubreginaddress(promotion.getSubreginaddress().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setSubreginaddress("--");
                    }
                    try {
                        promotionBean.setFreetext(promotion.getFreetext().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setFreetext("--");
                    }
                    try {
                        promotionBean.setInfo(promotion.getInfo().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setInfo("--");
                    }
                    try {
                        promotionBean.setTradinghours(promotion.getTradinghours().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setTradinghours("--");
                    }
                    try {
                        promotionBean.setLatitude(promotion.getLatitude().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setLatitude("--");
                    }
                    try {
                        promotionBean.setLongitude(promotion.getLongitude().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setLongitude("--");
                    }
                    try {
                        promotionBean.setPromoconditions(promotion.getPromoconditions().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setPromoconditions("--");
                    }
                    try {
                        promotionBean.setPhoneno(promotion.getPhoneno().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setPhoneno("--");
                    }
                    try {
                        promotionBean.setMerchantwebsite(promotion.getMerchantwebsite().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setMerchantwebsite("--");
                    }
                    try {
                        promotionBean.setImage(promotion.getImage().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setImage("--");
                    }
                    try {
                        promotionBean.setRegioncode(promotion.getRegioncode().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setRegioncode("--");
                    }
                    try {
                        promotionBean.setRegionname(promotion.getRegionname().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setRegionname("--");
                    }
                    try {
                        promotionBean.setMasterregoncode(promotion.getMasterregoncode().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setMasterregoncode("--");
                    }
                    try {
                        promotionBean.setMasterregionname(promotion.getMasterregionname().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setMasterregionname("--");
                    }
                    try {
                        promotionBean.setCardtype(promotion.getCardtype().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setCardtype("--");
                    }
                    try {
                        promotionBean.setCardimage(promotion.getCardimage().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setCardimage("--");
                    }
                    try {
                        promotionBean.setStartdate(sdf.format(promotion.getStartdate()).substring(0, 16));
                    } catch (Exception e) {
                        promotionBean.setStartdate("--");
                    }
                    try {
                        promotionBean.setEnddate(sdf.format(promotion.getEnddate()).substring(0, 16));
                    } catch (Exception e) {
                        promotionBean.setEnddate("--");
                    }
                    try {
                        promotionBean.setMaker(promotion.getMaker().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setMaker("--");
                    }
                    try {
                        promotionBean.setChecker(promotion.getChecker().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setChecker("--");
                    }
                    try {
                        promotionBean.setCreatetime(promotion.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        promotionBean.setCreatetime("--");
                    }
                    try {
                        promotionBean.setLastupdatedtime(promotion.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        promotionBean.setLastupdatedtime("--");
                    }

                    promotionBean.setFullCount(count);

                    dataList.add(promotionBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(PromotionMgtInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getSubregioncodeSearch() != null && !inputBean.getSubregioncodeSearch().isEmpty()) {
            where += " and lower(u.subregioncode) like lower('%" + inputBean.getSubregioncodeSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getPromotionsCategoriesSearch() != null && !inputBean.getPromotionsCategoriesSearch().isEmpty()) {
            where += " and u.promotionsCategories.catcode = '" + inputBean.getPromotionsCategoriesSearch().trim() + "'";
        }
        if (inputBean.getSubregionnameSearch() != null && !inputBean.getSubregionnameSearch().isEmpty()) {
            where += " and lower(u.subregionname) like lower('%" + inputBean.getSubregionnameSearch().trim() + "%')";
        }
        if (inputBean.getSubreginaddressSearch() != null && !inputBean.getSubreginaddressSearch().isEmpty()) {
            where += " and lower(u.subreginaddress) like lower('%" + inputBean.getSubreginaddressSearch().trim() + "%')";
        }
        if (inputBean.getFreetextSearch() != null && !inputBean.getFreetextSearch().isEmpty()) {
            where += " and lower(u.freetext) like lower('%" + inputBean.getFreetextSearch().trim() + "%')";
        }
        if (inputBean.getInfoSearch() != null && !inputBean.getInfoSearch().isEmpty()) {
            where += " and lower(u.info) like lower('%" + inputBean.getInfoSearch().trim() + "%')";
        }
        if (inputBean.getLongitudeSearch() != null && !inputBean.getLongitudeSearch().isEmpty()) {
            where += " and lower(u.longitude) like lower('%" + inputBean.getLongitudeSearch().trim() + "%')";
        }
        if (inputBean.getLatitudeSearch() != null && !inputBean.getLatitudeSearch().isEmpty()) {
            where += " and lower(u.latitude) like lower('%" + inputBean.getLatitudeSearch().trim() + "%')";
        }
        if (inputBean.getPromoconditionsSearch() != null && !inputBean.getPromoconditionsSearch().isEmpty()) {
            where += " and lower(u.promoconditions) like lower('%" + inputBean.getPromoconditionsSearch().trim() + "%')";
        }
        if (inputBean.getPhonenoSearch() != null && !inputBean.getPhonenoSearch().isEmpty()) {
            where += " and lower(u.phoneno) like lower('%" + inputBean.getPhonenoSearch().trim() + "%')";
        }
        if (inputBean.getMerchantwebsiteSearch() != null && !inputBean.getMerchantwebsiteSearch().isEmpty()) {
            where += " and lower(u.merchantwebsite) like lower('%" + inputBean.getMerchantwebsiteSearch().trim() + "%')";
        }

        return where;
    }

    public List<PromotionMgtPendBean> getPendingPromotionList(PromotionMgtInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<PromotionMgtPendBean> dataList = new ArrayList<PromotionMgtPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.PROMOTION_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.PROMOTION_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    PromotionMgtPendBean promotion = new PromotionMgtPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        promotion.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        promotion.setId("--");
                    }
                    try {
                        promotion.setOperation(pTask.getTask().getDescription().toString());
                        promotion.setOperationcode(pTask.getTask().getTaskcode().toString());
                    } catch (NullPointerException npe) {
                        promotion.setOperationcode("--");
                        promotion.setOperation("--");
                    }

                    try {
                        promotion.setSubregioncode(pTask.getPKey().toString());
                    } catch (Exception ex) {
                        promotion.setSubregioncode("--");
                    }
                    try {
                        promotion.setFields(pTask.getFields().toString());
                    } catch (NullPointerException npe) {
                        promotion.setFields("--");
                    } catch (Exception ex) {
                        promotion.setFields("--");
                    }
                    try {
                        promotion.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        promotion.setStatus("--");
                    }
                    try {
                        promotion.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        promotion.setCreatetime("--");
                    }
                    try {
                        promotion.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        promotion.setCreateduser("--");
                    } catch (Exception ex) {
                        promotion.setCreateduser("--");
                    }

                    promotion.setFullCount(count);

                    dataList.add(promotion);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertPromotion(PromotionMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((Promotions) session.get(Promotions.class, inputBean.getSubregioncode().trim()) == null) {
                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getSubregioncode())
                        .setString("pagecode", audit.getPagecode());
                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getSubregioncode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PROMOTION_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String uploadPromotion(PromotionMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        FileInputStream fileInputStream = null;

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

//            if ((Promotions) session.get(Promotions.class, inputBean.getSubregioncode().trim()) == null) {
//                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
//                Query query = session.createQuery(sql)
//                        .setString("PKey", inputBean.getSubregioncode())
//                        .setString("pagecode", audit.getPagecode());
//                if (query.list().isEmpty()) {
            txn = session.beginTransaction();

            Pendingtask pendingtask = new Pendingtask();

//                    pendingtask.setPKey(inputBean.getSubregioncode().trim());
//                    pendingtask.setFields(audit.getNewvalue());
            Task task = new Task();
            task.setTaskcode(TaskVarList.UPLOAD_TASK);
            pendingtask.setTask(task);

            Status st = new Status();
            st.setStatuscode(CommonVarList.STATUS_PENDING);
            pendingtask.setStatus(st);

            Page page = (Page) session.get(Page.class, PageVarList.PROMOTION_MGT_PAGE);
            pendingtask.setPage(page);

            pendingtask.setInputterbranch(sysUser.getBranch());

            try {
                if (inputBean.getConXL().length() != 0) {
                    File csvFile = inputBean.getConXL();
                    byte[] bCsvFile = new byte[(int) csvFile.length()];
                    try {
                        fileInputStream = new FileInputStream(csvFile);
                        fileInputStream.read(bCsvFile);
                        fileInputStream.close();
                        Blob blob = new javax.sql.rowset.serial.SerialBlob(bCsvFile);
                        pendingtask.setInputfile(blob);
                    } catch (Exception ex) {

                    }
                }
            } catch (NullPointerException ex) {

            } finally {

                if (fileInputStream != null) {
                    fileInputStream.close();
                }

            }

            pendingtask.setCreatedtime(sysDate);
            pendingtask.setLastupdatedtime(sysDate);
            pendingtask.setCreateduser(audit.getLastupdateduser());

            audit.setCreatetime(sysDate);
            audit.setLastupdatedtime(sysDate);
            audit.setLastupdateduser(audit.getLastupdateduser());

            session.save(audit);
            session.save(pendingtask);
            txn.commit();
//                } else {
//                    message = "pending available";
//                }
//            } else {
//                message = MessageVarList.COMMON_ALREADY_EXISTS;
//            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deletePromotion(PromotionMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getSubregioncode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Promotions u = (Promotions) session.get(Promotions.class, inputBean.getSubregioncode().trim());
                if (u != null) {
                    String status = "";
                    String promotionsCategories = "";
//                    String promotionsCards = "";
                    String tradinghours = "";
                    String startDate = "";
                    String endDate = "";
                    String ispushnot = "";
//                    String radious = "";
                    if (u.getStatus() != null) {
                        status = u.getStatus().getStatuscode();
                    }
                    if (u.getPromotionsCategories() != null) {
                        promotionsCategories = u.getPromotionsCategories().getCatcode();
                    }
//                    if (u.getPromotionsCards() != null) {
//                        promotionsCards = u.getPromotionsCards().getSubregioncode();
//                    }
                    if (u.getTradinghours() != null) {
                        tradinghours = u.getTradinghours();
                    }
                    try {
                        startDate = sdf.format(u.getStartdate()).substring(0, 16);
                    } catch (Exception e) {
                    }
                    try {
                        endDate = sdf.format(u.getEnddate()).substring(0, 16);
                    } catch (Exception e) {
                    }
                    if (u.getIspushnotification()) {
                        ispushnot = "1";
                    } else {
                        ispushnot = "";
                    }
//                    try {
//                        radious = u.getRadius().toString();
//                    } catch (Exception e) {
//                    }
                    audit.setNewvalue(u.getSubregioncode().toString() + "|" + status + "|" + promotionsCategories + "|" + u.getSubregionname()
                            + "|" + u.getSubreginaddress() + "|" + u.getFreetext() + "|" + u.getInfo() + "|" + tradinghours
                            + "|" + u.getLongitude() + "|" + u.getLatitude() + "|" + u.getPromoconditions() + "|" + u.getPhoneno() + "|" + u.getMerchantwebsite() + "|" + u.getImage()
                            + "|" + u.getRegioncode() + "|" + u.getRegionname() + "|" + u.getMasterregoncode() + "|" + u.getMasterregionname() + "|" + u.getCardtype() + "|" + u.getCardimage()
                            + "|" + startDate + "|" + endDate
                            + "|" + ispushnot
                    //                   + "|" + radious + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                    );
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getSubregioncode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.PROMOTION_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public Promotions findPromotionById(String subregioncode) throws Exception {
        Promotions promotion = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Promotions as u where u.subregioncode=:subregioncode";
            Query query = session.createQuery(sql).setString("subregioncode", subregioncode);
            promotion = (Promotions) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return promotion;

    }

    public String findDefaultImageURL() throws Exception {
        ParameterUserCommon parameterUserCommon = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();//PRO_IMG_URL

            String sql = "from ParameterUserCommon as u where u.paramcode=:paramcode";
            Query query = session.createQuery(sql).setString("paramcode", CommonVarList.PRO_IMG_URL_CODE);
            parameterUserCommon = (ParameterUserCommon) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return parameterUserCommon.getParamvalue();

    }

    public String updatePromotion(PromotionMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getSubregioncode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Promotions u = (Promotions) session.get(Promotions.class, inputBean.getSubregioncode().trim());
                if (u != null) {

                    String status = "";
                    String promotionsCategories = "";
                    String startDate = "";
                    String endDate = "";
//                    String promotionsCards = "";
                    String tradinghours = "";
                    String ispushnot = "";
//                    String radious = "";
                    if (u.getStatus() != null) {
                        status = u.getStatus().getStatuscode();
                    }
                    if (u.getPromotionsCategories() != null) {
                        promotionsCategories = u.getPromotionsCategories().getCatcode();
                    }
//                    if (u.getPromotionsCards() != null) {
//                        promotionsCards = u.getPromotionsCards().getSubregioncode();
//                    }
                    if (u.getTradinghours() != null) {
                        tradinghours = u.getTradinghours();
                    }
                    try {
                        startDate = sdf.format(u.getStartdate()).substring(0, 16);
                    } catch (Exception e) {
                    }
                    try {
                        endDate = sdf.format(u.getEnddate()).substring(0, 16);
                    } catch (Exception e) {
                    }
                    if (u.getIspushnotification()) {
                        ispushnot = "1";
                    } else {
                        ispushnot = "";
                    }
                    String website;
                    if (u.getMerchantwebsite() != null) {
                        website = u.getMerchantwebsite();
                    } else {
                        website = "";
                    }
//                    try {
//                        radious = u.getRadius().toString();
//                    } catch (Exception e) {
//                    }

                    audit.setOldvalue(u.getSubregioncode().toString() + "|" + status + "|" + promotionsCategories + "|" + u.getSubregionname()
                            + "|" + u.getSubreginaddress() + "|" + u.getFreetext() + "|" + u.getInfo() + "|" + tradinghours
                            + "|" + u.getLongitude() + "|" + u.getLatitude() + "|" + u.getPromoconditions() + "|" + u.getPhoneno() + "|" + website + "|" + u.getImage()
                            + "|" + u.getRegioncode() + "|" + u.getRegionname() + "|" + u.getMasterregoncode() + "|" + u.getMasterregionname() + "|" + u.getCardtype() + "|" + u.getCardimage()
                            + "|" + startDate + "|" + endDate
                            + "|" + ispushnot
                    //                  + "|" + radious  + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                    );

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getSubregioncode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PROMOTION_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String updateDefImg(PromotionMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            String sql = "from Pendingtask as u where u.task.taskcode=:taskcode and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("taskcode", TaskVarList.UPDATE_DEFAULT_IMG)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                ParameterUserCommon u = (ParameterUserCommon) session.get(ParameterUserCommon.class, CommonVarList.PRO_IMG_URL_CODE);
                if (u != null) {
                    audit.setOldvalue(u.getParamvalue().toString());

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_DEFAULT_IMG);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PROMOTION_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmPromotion(PromotionMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());

                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    Promotions promotion = new Promotions();

                    promotion.setSubregioncode(penArray[0]);

                    Status status = (Status) session.get(Status.class, penArray[1]);
                    promotion.setStatus(status);

                    PromotionsCategories promotionsCategory = (PromotionsCategories) session.get(PromotionsCategories.class, penArray[2]);
                    promotion.setPromotionsCategories(promotionsCategory);

                    promotion.setSubregionname(penArray[3]);
                    promotion.setSubreginaddress(penArray[4]);
                    promotion.setFreetext(penArray[5]);
                    promotion.setInfo(penArray[6]);
                    promotion.setTradinghours(penArray[7]);
                    promotion.setLongitude(new BigDecimal(penArray[8]));
                    promotion.setLatitude(new BigDecimal(penArray[9]));
                    promotion.setPromoconditions(penArray[10]);
                    promotion.setPhoneno(penArray[11]);
                    promotion.setMerchantwebsite(penArray[12]);
                    if (penArray[13] != null && !penArray[13].isEmpty()) {
                        promotion.setImage(penArray[13].trim());
                    } else {
                        ParameterUserCommon commonPromoImageUrl = (ParameterUserCommon) session.get(ParameterUserCommon.class, CommonVarList.PRO_IMG_URL);
                        promotion.setImage(commonPromoImageUrl.getParamvalue());
                    }
                    promotion.setRegioncode(penArray[14]);
                    promotion.setRegionname(penArray[15]);
                    promotion.setMasterregoncode(penArray[16]);
                    promotion.setMasterregionname(penArray[17]);
                    promotion.setCardtype(penArray[18]);
                    promotion.setCardimage(penArray[19]);

                    System.out.println("Start Date : " + penArray[20]);
                    String startDate = penArray[20];
                    String endDate = penArray[21];
                    Date start = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(startDate);
                    Date end = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(endDate);

                    promotion.setStartdate(start);
                    promotion.setEnddate(end);
                    try {
                        String ispushnotification = penArray[22];
//                        if (ispushnotification != null && ispushnotification.equals("1")) {
//                            promotion.setIspushnotification(Boolean.TRUE);

                        CommonDAO cmDAO = new CommonDAO();
                        SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
                        String formattedDate = formatTo.format(end);

                        Notification notification = new Notification();
                        try {

                            BigDecimal id = cmDAO.getIDByPromoID(session, penArray[0]);

                            notification = (Notification) session.get(Notification.class, id);
                            notification.setStatus(new Status(CommonVarList.STATUS_UNREAD));

                            notification.setMessage(promotion.getPromotionsCategories().getCatname() + "\n" + promotion.getSubregionname() + "  " + promotion.getInfo() + ". Valid through " + formattedDate);

                            notification.setLastUpdatedTime(sysDate);

                            if (ispushnotification != null && ispushnotification.equals("1")) {
                                promotion.setIspushnotification(Boolean.TRUE);
                                notification.setPushNotificationSent(BigDecimal.ZERO);
                            } else {
                                promotion.setIspushnotification(Boolean.FALSE);
                                notification.setPushNotificationSent(new BigDecimal("2"));
                            }
                            session.update(notification);

                        } catch (Exception ee) {

                            notification.setStatus(new Status(CommonVarList.STATUS_UNREAD));
                            notification.setSwtMtNotificationType(new SwtMtNotificationType(new BigDecimal(2)));

                            notification.setMessage(promotion.getPromotionsCategories().getCatname() + "\n" + promotion.getSubregionname() + "  " + promotion.getInfo() + ". Valid through " + formattedDate);

                            Promotions mb = new Promotions();
                            mb.setSubregioncode(penArray[0]);
                            notification.setMobPromotions(mb);

                            notification.setCreatedTime(sysDate);
                            notification.setLastUpdatedTime(sysDate);

                            if (ispushnotification != null && ispushnotification.equals("1")) {
                                promotion.setIspushnotification(Boolean.TRUE);
                                notification.setPushNotificationSent(BigDecimal.ZERO);
                            } else {
                                promotion.setIspushnotification(Boolean.FALSE);
                                notification.setPushNotificationSent(new BigDecimal("2"));
                            }
                            session.save(notification);
                        }

//                        } else {
//                            promotion.setIspushnotification(Boolean.FALSE);
//                        }
                    } catch (Exception e) {
                        promotion.setIspushnotification(Boolean.FALSE);
                    }
//                    promotion.setRadius(Double.valueOf(penArray[23]));
                    promotion.setCreatedtime(timestampDate);
                    promotion.setLastupdatedtime(timestampDate);
                    promotion.setMaker(pentask.getCreateduser());
                    promotion.setChecker(audit.getLastupdateduser());

                    session.saveOrUpdate(promotion);

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on promotion (Subregion code:  " + promotion.getSubregioncode().toString() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Promotions u = (Promotions) session.get(Promotions.class, penArray[0].trim());

                    if (u != null) {
                        //---------------------------audit old  value------------------------------------
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                        String startDateOld = "";
                        String endDateOld = "";
                        String pushNotificatinEnable = "";
                        String tradinghours = "";
                        String merchantwebsite = "";

                        try {
                            startDateOld = (sdf.format(u.getStartdate()).substring(0, 16));
                        } catch (Exception e) {
                            startDateOld = "";
                        }
                        try {
                            endDateOld = (sdf.format(u.getEnddate()).substring(0, 16));
                        } catch (Exception e) {
                            endDateOld = "";
                        }

                        if (u.getIspushnotification() != null && u.getIspushnotification().equals(Boolean.TRUE)) {
                            pushNotificatinEnable = "1";
                        } else {
                            pushNotificatinEnable = "false";
                        }
                        if (u.getTradinghours() != null && !u.getTradinghours().isEmpty()) {
                            tradinghours = u.getTradinghours();
                        }
                        if (u.getMerchantwebsite() != null && !u.getMerchantwebsite().isEmpty()) {
                            merchantwebsite = u.getMerchantwebsite();
                        }
                        if (u.getMerchantwebsite() != null && !u.getMerchantwebsite().isEmpty()) {
                            merchantwebsite = u.getMerchantwebsite();
                        }

                        String oldVal = u.getSubregioncode() + "|" + u.getStatus().getStatuscode() + "|" + u.getPromotionsCategories().getCatcode() + "|" + u.getSubregionname()
                                + "|" + u.getSubreginaddress() + "|" + u.getFreetext() + "|" + u.getInfo() + "|" + tradinghours
                                + "|" + u.getLongitude().toString() + "|" + u.getLatitude().toString() + "|" + u.getPromoconditions() + "|" + u.getPhoneno() + "|" + merchantwebsite + "|" + u.getImage()
                                + "|" + u.getRegioncode() + "|" + u.getRegionname() + "|" + u.getMasterregoncode() + "|" + u.getMasterregionname() + "|" + u.getCardtype() + "|" + u.getCardimage()
                                + "|" + startDateOld + "|" + endDateOld
                                + "|" + pushNotificatinEnable;

                        Status statusNew = (Status) session.get(Status.class, penArray[1]);
                        u.setStatus(statusNew);

                        PromotionsCategories promotionsCategoryNew = (PromotionsCategories) session.get(PromotionsCategories.class, penArray[2]);
                        u.setPromotionsCategories(promotionsCategoryNew);

                        u.setSubregionname(penArray[3]);
                        u.setSubreginaddress(penArray[4]);
                        u.setFreetext(penArray[5]);
                        u.setInfo(penArray[6]);
                        u.setTradinghours(penArray[7]);
                        u.setLongitude(new BigDecimal(penArray[8]));
                        u.setLatitude(new BigDecimal(penArray[9]));
                        u.setPromoconditions(penArray[10]);
                        u.setPhoneno(penArray[11]);
                        u.setMerchantwebsite(penArray[12]);
                        if (penArray[13] != null && !penArray[13].isEmpty()) {
                            u.setImage(penArray[13].trim());
                        } else {
                            ParameterUserCommon commonPromoImageUrl = (ParameterUserCommon) session.get(ParameterUserCommon.class, CommonVarList.PRO_IMG_URL);
                            u.setImage(commonPromoImageUrl.getParamvalue());
                        }

                        u.setRegioncode(penArray[14]);
                        u.setRegionname(penArray[15]);
                        u.setMasterregoncode(penArray[16]);
                        u.setMasterregionname(penArray[17]);
                        u.setCardtype(penArray[18]);
                        u.setCardimage(penArray[19]);

                        System.out.println("Start Date : " + penArray[20]);
                        String startDate = penArray[20];
                        String endDate = penArray[21];
                        Date start = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(startDate);
                        Date end = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(endDate);

                        u.setStartdate(start);
                        u.setEnddate(end);
                        try {
                            String ispushnotification = penArray[22];
//                            if (ispushnotification != null && ispushnotification.equals("1")) {
//                                u.setIspushnotification(Boolean.TRUE);

                            CommonDAO cmDAO = new CommonDAO();

                            //                    Notification notification = cmDAO.getIDByPromotionID(inputBean.getPromotionId());
                            Notification notification = new Notification();

                            SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
                            String formattedDate = formatTo.format(end);

                            try {

                                BigDecimal id = cmDAO.getIDByPromoID(session, penArray[0]);

                                notification = (Notification) session.get(Notification.class, id);
                                notification.setStatus(new Status(CommonVarList.STATUS_UNREAD));

                                notification.setMessage(u.getPromotionsCategories().getCatname() + "\n" + u.getSubregionname() + " " + u.getInfo() + ". Valid through " + formattedDate);

                                if (ispushnotification != null && ispushnotification.equals("1")) {
                                    u.setIspushnotification(Boolean.TRUE);
                                    notification.setPushNotificationSent(BigDecimal.ZERO);
                                } else {
                                    u.setIspushnotification(Boolean.FALSE);
                                    notification.setPushNotificationSent(new BigDecimal("2"));
                                }
//                                    notification.setCreatedTime(sysDate);
                                notification.setLastUpdatedTime(sysDate);
                                session.update(notification);

                            } catch (Exception ee) {

                                Promotions mb = new Promotions();
                                mb.setSubregioncode(penArray[0]);
                                notification.setMobPromotions(mb);
                                notification.setStatus(new Status(CommonVarList.STATUS_UNREAD));
                                notification.setSwtMtNotificationType(new SwtMtNotificationType(new BigDecimal(2)));

                                //                        notification.setMessage(inputBean.getBody());
                                notification.setMessage(u.getPromotionsCategories().getCatname() + "\n" + u.getSubregionname() + " " + u.getInfo() + ". Valid through " + formattedDate);

                                notification.setMobPromotions(mb);

                                if (ispushnotification != null && ispushnotification.equals("1")) {
                                    u.setIspushnotification(Boolean.TRUE);
                                    notification.setPushNotificationSent(BigDecimal.ZERO);
                                } else {
                                    u.setIspushnotification(Boolean.FALSE);
                                    notification.setPushNotificationSent(new BigDecimal("2"));
                                }

                                notification.setCreatedTime(sysDate);
                                notification.setLastUpdatedTime(sysDate);
                                session.save(notification);
                            }

//                            } else {
//                                u.setIspushnotification(Boolean.FALSE);
//                            }
                        } catch (Exception e) {
                            u.setIspushnotification(Boolean.FALSE);
                        }

//                        u.setRadius(Double.valueOf(penArray[23]));
                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        session.update(u);

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on promotion (Subregion code: " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPLOAD_TASK)) {
//                    String subregioncods = "";
                    Blob csvBlob = pentask.getInputfile();
                    if (csvBlob != null) {
                        isr = new InputStreamReader(csvBlob.getBinaryStream());
                        br = new BufferedReader(isr);

                        String[] parts = new String[0];
                        int countrecord = 1;
                        int succesrec = 0;
                        String thisLine = null;
                        boolean getline = false;
                        String token = "";
                        while ((thisLine = br.readLine()) != null) {
                            if (thisLine.trim().equals("")) {
                                continue;
                            } else {
                                if (getline) {
//                              token = content.nextLine();
                                    token = thisLine;

//                                    System.err.println(token);
                                    try {
                                        parts = token.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", 21);
                                        inputBean.setSubregioncode(Common.removeCsvDoubleQuotation(parts[0].trim()));
                                        inputBean.setSubregionname(Common.removeCsvDoubleQuotation(parts[1].trim()));
                                        inputBean.setPromotionsCategories(Common.removeCsvDoubleQuotation(parts[2].trim()));
                                        inputBean.setFreetext(Common.removeCsvDoubleQuotation(parts[3].trim()));
                                        inputBean.setInfo(Common.removeCsvDoubleQuotation(parts[4].trim()));
                                        inputBean.setTradinghours(Common.removeCsvDoubleQuotation(parts[5]));
                                        inputBean.setLatitude(Common.removeCsvDoubleQuotation(parts[6].trim()));
                                        inputBean.setLongitude(Common.removeCsvDoubleQuotation(parts[7].trim()));
//                                        inputBean.setRadius(Common.removeCsvDoubleQuotation(parts[8].trim()));
                                        inputBean.setPromoconditions(Common.removeCsvDoubleQuotation(parts[8].trim()));
                                        inputBean.setPhoneno(Common.removeCsvDoubleQuotation(parts[9].trim()));
                                        inputBean.setMerchantwebsite(Common.removeCsvDoubleQuotation(parts[10].trim()));
                                        inputBean.setRegioncode(Common.removeCsvDoubleQuotation(parts[11].trim()));
                                        inputBean.setRegionname(Common.removeCsvDoubleQuotation(parts[12].trim()));
                                        inputBean.setMasterregoncode(Common.removeCsvDoubleQuotation(parts[13].trim()));
                                        inputBean.setMasterregionname(Common.removeCsvDoubleQuotation(parts[14].trim()));
                                        inputBean.setCardtype(Common.removeCsvDoubleQuotation(parts[15].trim()));
                                        inputBean.setCardimage(Common.removeCsvDoubleQuotation(parts[16].trim()));
                                        inputBean.setStartdate(Common.removeCsvDoubleQuotation(parts[17].trim()));
                                        inputBean.setEnddate(Common.removeCsvDoubleQuotation(parts[18].trim()));
                                        inputBean.setIspushnotification(Common.removeCsvDoubleQuotation(parts[19].trim()));
                                        inputBean.setSubreginaddress(Common.removeCsvDoubleQuotation(parts[20].trim()));
                                        inputBean.setStatus(CommonVarList.STATUS_ACTIVE);

                                    } catch (Exception ee) {
                                        message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                                        break;
                                    }
                                    countrecord++;
                                    if (parts.length >= 21 && message.isEmpty()) {
//                                        message = this.validateInputsForCSV(inputBean);
//                                        if (message.isEmpty()) {
//                                            message = this.validateUpload(inputBean, session);
//                                            if (message.isEmpty()) {
                                        if ((Promotions) session.get(Promotions.class, inputBean.getSubregioncode().trim()) == null) {
                                            Promotions promotion = new Promotions();

                                            promotion.setSubregioncode(inputBean.getSubregioncode().trim());

                                            Status status = (Status) session.get(Status.class, inputBean.getStatus());
                                            promotion.setStatus(status);

                                            PromotionsCategories promotionsCategory = (PromotionsCategories) session.get(PromotionsCategories.class, inputBean.getPromotionsCategories());
                                            promotion.setPromotionsCategories(promotionsCategory);

                                            ParameterUserCommon commonPromoImageUrl = (ParameterUserCommon) session.get(ParameterUserCommon.class, CommonVarList.PRO_IMG_URL);
                                            promotion.setImage(commonPromoImageUrl.getParamvalue());

                                            //                    PromotionsCards promotionsCard = (PromotionsCards) session.get(PromotionsCards.class, penArray[2]);
                                            //                    promotion.setPromotionsCards(promotionsCard);
                                            promotion.setSubregionname(inputBean.getSubregionname());
                                            promotion.setSubreginaddress(inputBean.getSubreginaddress());
                                            promotion.setFreetext(inputBean.getFreetext());
                                            promotion.setInfo(inputBean.getInfo());
                                            promotion.setTradinghours(inputBean.getTradinghours());
                                            promotion.setLongitude(new BigDecimal(inputBean.getLongitude()));
                                            promotion.setLatitude(new BigDecimal(inputBean.getLatitude()));
//                                            promotion.setRadius(Double.valueOf(inputBean.getRadius()));
                                            promotion.setPromoconditions(inputBean.getPromoconditions());
                                            promotion.setPhoneno(inputBean.getPhoneno());
                                            promotion.setMerchantwebsite(inputBean.getMerchantwebsite());
//                                                    promotion.setImage(inputBean.getImage());
                                            promotion.setRegioncode(inputBean.getRegioncode());
                                            promotion.setRegionname(inputBean.getRegionname());
                                            promotion.setMasterregoncode(inputBean.getMasterregoncode());
                                            promotion.setMasterregionname(inputBean.getMasterregionname());
                                            promotion.setCardtype(inputBean.getCardtype());
                                            promotion.setCardimage(inputBean.getCardimage());

                                            System.out.println("Start Date : " + inputBean.getStartdate());
                                            String startDate = inputBean.getStartdate();
                                            String endDate = inputBean.getEnddate();
//                                            Date start = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(startDate);
//                                            Date end = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(endDate);
                                            Date start = new Date();
                                            Date end = new Date();
                                            try {
                                                start = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(startDate);
                                                end = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(endDate);
                                            } catch (Exception e) {
                                                start = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(startDate);
                                                end = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(endDate);
                                            }

                                            try {
                                                String ispushnotification = inputBean.getIspushnotification();
//                                                if (ispushnotification != null && ispushnotification.equals("1")) {
//                                                    promotion.setIspushnotification(Boolean.TRUE);

                                                Notification notification = new Notification();

                                                SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
                                                String formattedDate = formatTo.format(end);

                                                Promotions mb = new Promotions();
                                                mb.setSubregioncode(promotion.getSubregioncode());
                                                notification.setMobPromotions(mb);
                                                notification.setStatus(new Status(CommonVarList.STATUS_UNREAD));
                                                notification.setSwtMtNotificationType(new SwtMtNotificationType(new BigDecimal(2)));

                                                notification.setMessage(promotion.getPromotionsCategories().getCatname() + "\n" + promotion.getSubregionname() + " " + promotion.getInfo() + ". Valid through " + formattedDate);

                                                notification.setMobPromotions(mb);

                                                if (ispushnotification != null && ispushnotification.equals("1")) {
                                                    promotion.setIspushnotification(Boolean.TRUE);
                                                    notification.setPushNotificationSent(BigDecimal.ZERO);
                                                } else {
                                                    promotion.setIspushnotification(Boolean.FALSE);
                                                    notification.setPushNotificationSent(new BigDecimal("2"));
                                                }
                                                notification.setCreatedTime(sysDate);
                                                notification.setLastUpdatedTime(sysDate);
                                                session.saveOrUpdate(notification);

//                                                } else {
//                                                    promotion.setIspushnotification(Boolean.FALSE);
//                                                }
                                            } catch (Exception e) {
                                                promotion.setIspushnotification(Boolean.FALSE);
                                            }
                                            promotion.setStartdate(start);
                                            promotion.setEnddate(end);

                                            promotion.setCreatedtime(timestampDate);
                                            promotion.setLastupdatedtime(timestampDate);
                                            promotion.setMaker(pentask.getCreateduser());
                                            promotion.setChecker(audit.getLastupdateduser());

                                            session.save(promotion);

                                        } else {
                                            Promotions u = (Promotions) session.get(Promotions.class, inputBean.getSubregioncode().trim());

//                                                    Status statusNew = (Status) session.get(Status.class, inputBean.getStatus());
//                                                    u.setStatus(statusNew);
                                            PromotionsCategories promotionsCategoryNew = (PromotionsCategories) session.get(PromotionsCategories.class, inputBean.getPromotionsCategories());
                                            u.setPromotionsCategories(promotionsCategoryNew);

                                            //                        PromotionsCards promotionsCardNew = (PromotionsCards) session.get(PromotionsCards.class, penArray[3]);
                                            //                        u.setPromotionsCards(promotionsCardNew);
                                            u.setSubregionname(inputBean.getSubregionname());
                                            u.setSubreginaddress(inputBean.getSubreginaddress());
                                            u.setFreetext(inputBean.getFreetext());
                                            u.setInfo(inputBean.getInfo());
                                            u.setTradinghours(inputBean.getTradinghours());
                                            u.setLongitude(new BigDecimal(inputBean.getLongitude()));
                                            u.setLatitude(new BigDecimal(inputBean.getLatitude()));
//                                            u.setRadius(Double.valueOf(inputBean.getRadius()));
                                            u.setPromoconditions(inputBean.getPromoconditions());
                                            u.setPhoneno(inputBean.getPhoneno());
                                            u.setMerchantwebsite(inputBean.getMerchantwebsite());
//                                                    promotion.setImage(inputBean.getImage());
                                            u.setRegioncode(inputBean.getRegioncode());
                                            u.setRegionname(inputBean.getRegionname());
                                            u.setMasterregoncode(inputBean.getMasterregoncode());
                                            u.setMasterregionname(inputBean.getMasterregionname());
                                            u.setCardtype(inputBean.getCardtype());
                                            u.setCardimage(inputBean.getCardimage());

                                            System.out.println("Start Date : " + inputBean.getStartdate());
                                            String startDate = inputBean.getStartdate();
                                            String endDate = inputBean.getEnddate();
//                                            Date start = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(startDate);
//                                            Date end = new SimpleDateFormat("dd-MM-yyyy HH:mm").parse(endDate);
                                            Date start = new Date();
                                            Date end = new Date();
                                            try {
                                                start = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(startDate);
                                                end = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(endDate);
                                            } catch (Exception e) {
                                                start = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(startDate);
                                                end = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(endDate);
                                            }

                                            u.setStartdate(start);
                                            u.setEnddate(end);
                                            try {
                                                String ispushnotification = inputBean.getIspushnotification();
//                                                if (ispushnotification != null && ispushnotification.equals("1")) {
//                                                    u.setIspushnotification(Boolean.TRUE);

                                                CommonDAO cmDAO = new CommonDAO();

                                                Notification notification = new Notification();

                                                SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
                                                String formattedDate = formatTo.format(end);

                                                try {

                                                    BigDecimal id = cmDAO.getIDByPromoID(session, u.getSubregioncode());

                                                    notification = (Notification) session.get(Notification.class, id);
                                                    notification.setStatus(new Status(CommonVarList.STATUS_UNREAD));

                                                    notification.setMessage(u.getPromotionsCategories().getCatname() + "\n" + u.getSubregionname() + " " + u.getInfo() + ". Valid through " + formattedDate);

                                                    if (ispushnotification != null && ispushnotification.equals("1")) {
                                                        u.setIspushnotification(Boolean.TRUE);
                                                        notification.setPushNotificationSent(BigDecimal.ZERO);
                                                    } else {
                                                        u.setIspushnotification(Boolean.FALSE);
                                                        notification.setPushNotificationSent(new BigDecimal("2"));
                                                    }
//                                                        notification.setCreatedTime(sysDate);
                                                    notification.setLastUpdatedTime(sysDate);
                                                    session.update(notification);

                                                } catch (Exception ee) {

                                                    Promotions mb = new Promotions();
                                                    mb.setSubregioncode(u.getSubregioncode());
                                                    notification.setMobPromotions(mb);
                                                    notification.setStatus(new Status(CommonVarList.STATUS_UNREAD));
                                                    notification.setSwtMtNotificationType(new SwtMtNotificationType(new BigDecimal(2)));

                                                    //                        notification.setMessage(inputBean.getBody());
                                                    notification.setMessage(u.getPromotionsCategories().getCatname() + "\n" + u.getSubregionname() + " " + u.getInfo() + ". Valid through " + formattedDate);

                                                    notification.setMobPromotions(mb);

                                                    if (ispushnotification != null && ispushnotification.equals("1")) {
                                                        u.setIspushnotification(Boolean.TRUE);
                                                        notification.setPushNotificationSent(BigDecimal.ZERO);
                                                    } else {
                                                        u.setIspushnotification(Boolean.FALSE);
                                                        notification.setPushNotificationSent(new BigDecimal("2"));
                                                    }
                                                    notification.setCreatedTime(sysDate);
                                                    notification.setLastUpdatedTime(sysDate);
                                                    session.save(notification);
                                                }

//                                                } else {
//                                                    u.setIspushnotification(Boolean.FALSE);
//                                                }
                                            } catch (Exception e) {
                                                u.setIspushnotification(Boolean.FALSE);
                                            }
                                            u.setMaker(pentask.getCreateduser());
                                            u.setChecker(audit.getLastupdateduser());
                                            u.setLastupdatedtime(timestampDate);

                                            session.update(u);

                                        }
//                                        subregioncods = subregioncods + inputBean.getSubregioncode().trim() + ",";
                                        succesrec++;
//                                            } else {
//                                                message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                                break;
//                                            }
//                                        } else {
//                                            message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                            break;
//                                        }

                                    } else {
                                        message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                                    }
                                } else {
                                    getline = true;
//                            content.nextLine();
                                }
                            }
                        }
//                        if (!subregioncods.isEmpty() && subregioncods.length() > 0) {
//                            subregioncods = subregioncods.substring(0, subregioncods.length() - 1);
//                        }
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on promotion (Records count :" + succesrec + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    }

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_DEFAULT_IMG)) {
                    ParameterUserCommon u = (ParameterUserCommon) session.get(ParameterUserCommon.class, CommonVarList.PRO_IMG_URL_CODE);
                    if (u != null) {
                        //----------------------audit old value---------------------------
                        String oldVal = u.getParamvalue();
                        u.setParamvalue(pentask.getFields());
                        u.setLastupdatedtime(sysDate);
                        u.setLastupdateduser(audit.getLastupdateduser());

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on promotion " + "  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.update(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    Promotions u = (Promotions) session.get(Promotions.class, penArray[0]);
                    if (u != null) {
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on promotion (Subregion code: " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectPromotion(PromotionMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
//                String[] fieldsArray = u.getFields().split("\\|");
//
//                String promotionId = fieldsArray[0];
                String subregioncode = u.getPKey();
                if (subregioncode != null && !subregioncode.isEmpty()) {
                    if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                        Promotions promotion = (Promotions) session.get(Promotions.class, subregioncode.trim());

                        if (promotion != null) {
                            //---------------------------audit old  value------------------------------------
                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                            String startDateOld = "";
                            String endDateOld = "";
                            String pushNotificatinEnable = "";
                            String tradinghours = "";
                            String merchantwebsite = "";

                            try {
                                startDateOld = (sdf.format(promotion.getStartdate()).substring(0, 16));
                            } catch (Exception e) {
                                startDateOld = "";
                            }
                            try {
                                endDateOld = (sdf.format(promotion.getEnddate()).substring(0, 16));
                            } catch (Exception e) {
                                endDateOld = "";
                            }

                            if (promotion.getIspushnotification() != null && promotion.getIspushnotification().equals(Boolean.TRUE)) {
                                pushNotificatinEnable = "1";
                            } else {
                                pushNotificatinEnable = "false";
                            }
                            if (promotion.getTradinghours() != null && !promotion.getTradinghours().isEmpty()) {
                                tradinghours = promotion.getTradinghours();
                            }
                            if (promotion.getMerchantwebsite() != null && !promotion.getMerchantwebsite().isEmpty()) {
                                merchantwebsite = promotion.getMerchantwebsite();
                            }
                            if (promotion.getMerchantwebsite() != null && !promotion.getMerchantwebsite().isEmpty()) {
                                merchantwebsite = promotion.getMerchantwebsite();
                            }

                            String oldVal = promotion.getSubregioncode() + "|" + promotion.getStatus().getStatuscode() + "|" + promotion.getPromotionsCategories().getCatcode() + "|" + promotion.getSubregionname()
                                    + "|" + promotion.getSubreginaddress() + "|" + promotion.getFreetext() + "|" + promotion.getInfo() + "|" + tradinghours
                                    + "|" + promotion.getLongitude().toString() + "|" + promotion.getLatitude().toString() + "|" + promotion.getPromoconditions() + "|" + promotion.getPhoneno() + "|" + merchantwebsite + "|" + promotion.getImage()
                                    + "|" + promotion.getRegioncode() + "|" + promotion.getRegionname() + "|" + promotion.getMasterregoncode() + "|" + promotion.getMasterregionname() + "|" + promotion.getCardtype() + "|" + promotion.getCardimage()
                                    + "|" + startDateOld + "|" + endDateOld
                                    + "|" + pushNotificatinEnable;
                            audit.setOldvalue(oldVal);
                        }
                    }
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on promotion (Subregion code: " + subregioncode + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                } else {
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on promotion inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String pendPromotionCsvDownloade(PromotionMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getInputfile() != null) {
                    Blob blob = u.getInputfile();
                    int blobLength = (int) blob.length();
                    byte[] blobAsBytes = blob.getBytes(1, blobLength);
                    inputBean.setFileInputStream(u.getInputfile().getBinaryStream());
                    inputBean.setFileLength(blobAsBytes.length);

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    session.save(audit);
                    txn.commit();
                } else {
                    message = "File not found";
                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String viwPendPromotion(PromotionMgtInputBean inputBean) throws Exception {

        Session session = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                inputBean.setTaskCode(u.getTask().getTaskcode());
                inputBean.setTaskDescription(u.getTask().getDescription());
                String[] penArray = null;
                if (u.getFields() != null) {
                    penArray = u.getFields().split("\\|");
                }
                inputBean.setSubregioncode(penArray[0]);

                Status status = (Status) session.get(Status.class, penArray[1]);
                inputBean.setStatus(status.getDescription());

                PromotionsCategories promotionsCategory = (PromotionsCategories) session.get(PromotionsCategories.class, penArray[2]);
                inputBean.setPromotionsCategories(promotionsCategory.getCatname());

//                    PromotionsCards promotionsCard = (PromotionsCards) session.get(PromotionsCards.class, penArray[2]);
//                    promotion.setPromotionsCards(promotionsCard);
                inputBean.setSubregionname(penArray[3]);
                inputBean.setSubreginaddress(penArray[4]);
                inputBean.setFreetext(penArray[5]);
                inputBean.setInfo(penArray[6]);
                if (penArray[7] != null && !penArray[7].isEmpty()) {
                    inputBean.setTradinghours(penArray[7]);
                } else {
                    inputBean.setTradinghours("--");
                }
                inputBean.setLongitude(penArray[8]);
                inputBean.setLatitude(penArray[9]);
                inputBean.setPromoconditions(penArray[10]);
                inputBean.setPhoneno(penArray[11]);
                if (penArray[12] != null && !penArray[12].isEmpty()) {
                    inputBean.setMerchantwebsite(penArray[12]);
                } else {
                    inputBean.setMerchantwebsite("--");
                }
                if (penArray[13] != null && !penArray[13].isEmpty()) {
                    inputBean.setImage(penArray[13].trim());
                } else {
                    ParameterUserCommon commonPromoImageUrl = (ParameterUserCommon) session.get(ParameterUserCommon.class, CommonVarList.PRO_IMG_URL);
                    inputBean.setImage(commonPromoImageUrl.getParamvalue());
                }
                inputBean.setRegioncode(penArray[14]);
                inputBean.setRegionname(penArray[15]);
                inputBean.setMasterregoncode(penArray[16]);
                inputBean.setMasterregionname(penArray[17]);
                inputBean.setCardtype(penArray[18]);
                inputBean.setCardimage(penArray[19]);
                inputBean.setStartdate(penArray[20]);
                inputBean.setEnddate(penArray[21]);
                try {
                    if (penArray[22].equals("1")) {
                        inputBean.setIspushnotification("True");
                    } else {
                        inputBean.setIspushnotification("False");
                    }
                } catch (Exception e) {
                    inputBean.setIspushnotification("False");
                }
//                inputBean.setRadius(penArray[23]);
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    //-------------------------------0ld value---------------------------------
                    Promotions promotion = (Promotions) session.get(Promotions.class, penArray[0]);
                    PromotionMgtBean promotionBean = new PromotionMgtBean();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
                    try {
                        promotionBean.setSubregioncode(promotion.getSubregioncode().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setSubregioncode("--");
                    }
                    try {
                        promotionBean.setStatus(promotion.getStatus().getDescription());
                    } catch (Exception npe) {
                        promotionBean.setStatus("--");
                    }
                    try {
                        promotionBean.setPromotionsCategories(promotion.getPromotionsCategories().getCatname());
                    } catch (Exception npe) {
                        promotionBean.setPromotionsCategories("--");
                    }
                    try {
                        promotionBean.setSubregionname(promotion.getSubregionname().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setSubregionname("--");
                    }
                    try {
                        promotionBean.setSubreginaddress(promotion.getSubreginaddress().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setSubreginaddress("--");
                    }
                    try {
                        promotionBean.setFreetext(promotion.getFreetext().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setFreetext("--");
                    }
                    try {
                        promotionBean.setInfo(promotion.getInfo().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setInfo("--");
                    }
                    try {
                        promotionBean.setTradinghours(promotion.getTradinghours().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setTradinghours("--");
                    }
                    try {
                        promotionBean.setLatitude(promotion.getLatitude().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setLatitude("--");
                    }
                    try {
                        promotionBean.setLongitude(promotion.getLongitude().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setLongitude("--");
                    }
                    try {
                        promotionBean.setPromoconditions(promotion.getPromoconditions().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setPromoconditions("--");
                    }
                    try {
                        promotionBean.setPhoneno(promotion.getPhoneno().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setPhoneno("--");
                    }
                    try {
                        promotionBean.setMerchantwebsite(promotion.getMerchantwebsite().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setMerchantwebsite("--");
                    }
                    try {
                        promotionBean.setImage(promotion.getImage().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setImage("--");
                    }
                    try {
                        promotionBean.setRegioncode(promotion.getRegioncode().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setRegioncode("--");
                    }
                    try {
                        promotionBean.setRegionname(promotion.getRegionname().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setRegionname("--");
                    }
                    try {
                        promotionBean.setMasterregoncode(promotion.getMasterregoncode().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setMasterregoncode("--");
                    }
                    try {
                        promotionBean.setMasterregionname(promotion.getMasterregionname().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setMasterregionname("--");
                    }
                    try {
                        promotionBean.setCardtype(promotion.getCardtype().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setCardtype("--");
                    }
                    try {
                        promotionBean.setCardimage(promotion.getCardimage().toString());
                    } catch (NullPointerException npe) {
                        promotionBean.setCardimage("--");
                    }
                    try {
                        promotionBean.setStartdate(sdf.format(promotion.getStartdate()).substring(0, 16));
                    } catch (Exception e) {
                        promotionBean.setStartdate("--");
                    }
                    try {
                        promotionBean.setEnddate(sdf.format(promotion.getEnddate()).substring(0, 16));
                    } catch (Exception e) {
                        promotionBean.setEnddate("--");
                    }
                    try {
                        if (promotion.getIspushnotification()) {
                            promotionBean.setIspushnotification("True");
                        } else {
                            promotionBean.setIspushnotification("False");
                        }
                    } catch (Exception e) {
                        promotionBean.setIspushnotification("False");
                    }
                    inputBean.setPromotionOldVal(promotionBean);
                } else {

                    PromotionMgtBean promotionBean = new PromotionMgtBean();
                    promotionBean.setSubregioncode("--");
                    promotionBean.setStatus("--");
                    promotionBean.setPromotionsCategories("--");
                    promotionBean.setSubregionname("--");
                    promotionBean.setSubreginaddress("--");
                    promotionBean.setFreetext("--");
                    promotionBean.setInfo("--");
                    promotionBean.setTradinghours("--");
                    promotionBean.setLatitude("--");
                    promotionBean.setLongitude("--");
                    promotionBean.setPromoconditions("--");
                    promotionBean.setPhoneno("--");
                    promotionBean.setMerchantwebsite("--");
                    promotionBean.setImage("--");
                    promotionBean.setRegioncode("--");
                    promotionBean.setRegionname("--");
                    promotionBean.setMasterregoncode("--");
                    promotionBean.setMasterregionname("--");
                    promotionBean.setCardtype("--");
                    promotionBean.setCardimage("--");
                    promotionBean.setStartdate("--");
                    promotionBean.setEnddate("--");
                    inputBean.setPromotionOldVal(promotionBean);
                }
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String validateUpload(PromotionMgtInputBean inputBean, Session session) throws Exception {
        String message = "";

        if (inputBean.getSubregioncode().length() > 10) {
            message = MessageVarList.PROMOTION_MGT_INVALID_SUBREGIONCODE_LENGTH + "10";
        } else if (!Validation.isSpecailCharacter(inputBean.getSubregioncode())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_SUBREGIONCODE;
        } else if (inputBean.getSubregionname().length() > 255) {
            message = MessageVarList.PROMOTION_MGT_INVALID_SUBREGIONNAME_LENGTH + "255";
        } else if (inputBean.getSubreginaddress().length() > 512) {
            message = MessageVarList.PROMOTION_MGT_INVALID_SUBREGIONADDRESS_LENGTH + "512";
        } else if (inputBean.getFreetext().length() > 512) {
            message = MessageVarList.PROMOTION_MGT_INVALID_FREETEXT_LENGTH + "512";
        } else if (inputBean.getInfo().length() > 255) {
            message = MessageVarList.PROMOTION_MGT_INVALID_INFO_LENGTH + "255";
        } else if (inputBean.getTradinghours() != null && !inputBean.getTradinghours().isEmpty() && inputBean.getTradinghours().length() > 20) {//
            message = MessageVarList.PROMOTION_MGT_INVALID_TRADINGHOURS_LENGTH + "20";
        } else if (inputBean.getLatitude().length() > 20) {
            message = MessageVarList.PROMOTION_MGT_INVALID_LATITUDE_LENGTH + "20";
        } else if (inputBean.getLongitude().length() > 20) {
            message = MessageVarList.PROMOTION_MGT_INVALID_LONGITUDE_LENGTH + "20";
//        } else if (inputBean.getRadius().length() > 20) {
//            message = MessageVarList.PROMOTION_MGT_INVALID_LATITUDE_LENGTH + "20";
        } else if (inputBean.getPromoconditions().length() > 95) {
            message = MessageVarList.PROMOTION_MGT_INVALID_PROMOCONDITION_LENGTH + "95";
        } else if (inputBean.getPhoneno().length() > 33) {
            message = MessageVarList.PROMOTION_MGT_INVALID_PHONENO_LENGTH + "33";
        } else if (inputBean.getMerchantwebsite().length() > 255) {
            message = MessageVarList.PROMOTION_MGT_INVALID_MERCHANTWEBSITE_LENGTH + "255";
        } else if (inputBean.getRegioncode().length() > 10) {
            message = MessageVarList.PROMOTION_MGT_INVALID_REGIONCODE_LENGTH + "10";
        } else if (inputBean.getRegionname().length() > 50) {
            message = MessageVarList.PROMOTION_MGT_INVALID_REGIONNAME_LENGTH + "50";
        } else if (inputBean.getMasterregoncode().length() > 10) {
            message = MessageVarList.PROMOTION_MGT_INVALID_MASTERREGIONCODE_LENGTH + "10";
        } else if (inputBean.getMasterregionname().length() > 50) {
            message = MessageVarList.PROMOTION_MGT_INVALID_MASTERREGIONNAME_LENGTH + "50";
        } else if (inputBean.getCardtype().length() > 50) {
            message = MessageVarList.PROMOTION_MGT_INVALID_CARDTYPE_LENGTH + "50";
        } else if (inputBean.getCardimage().length() > 100) {
            message = MessageVarList.PROMOTION_MGT_INVALID_CARDIMAGE_LENGTH + "100";
        } else if ((PromotionsCategories) session.get(PromotionsCategories.class, inputBean.getPromotionsCategories().trim()) == null) {
            message = MessageVarList.PROMOTION_MGT_INVALID_PROMOTIONCATEGORY;
        }

        return message;
    }

    public String validateInputsForCSV(PromotionMgtInputBean inputBean) {
        String message = "";
//        if (inputBean.getPromotionsCards() == null || inputBean.getPromotionsCards().trim().isEmpty()) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_PROMOTIONCARD;
        if (inputBean.getSubregioncode() == null || inputBean.getSubregioncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONCODE;
        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_STATUS;
        } else if (inputBean.getPromotionsCategories() != null && inputBean.getPromotionsCategories().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PROMOTIONCATEGORY;
        } else if (inputBean.getSubregionname() == null || inputBean.getSubregionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONNAME;
        } else if (inputBean.getSubreginaddress() == null || inputBean.getSubreginaddress().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_SUBREGIONADDRESS;
        } else if (inputBean.getFreetext() == null || inputBean.getFreetext().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_FREETEXT;
        } else if (inputBean.getInfo() == null || inputBean.getInfo().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_INFO;
//        } else if (inputBean.getTradinghours()== null || inputBean.getTradinghours().trim().isEmpty()) {
//            message = MessageVarList.BRANCH_MGT_EMPTY_MOBILE;
        } else if (inputBean.getLatitude() == null || inputBean.getLatitude().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_LATITUDE;
        } else if ((inputBean.getLatitude() != null || !inputBean.getLatitude().trim().isEmpty()) && !Validation.isValidFloatValue(inputBean.getLatitude())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_LATITUDE;
        } else if (inputBean.getLongitude() == null || inputBean.getLongitude().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_LONGITUDE;
        } else if ((inputBean.getLongitude() != null || !inputBean.getLongitude().trim().isEmpty()) && !Validation.isValidFloatValue(inputBean.getLongitude())) {
            message = MessageVarList.PROMOTION_MGT_INVALID_LONGITUDE;
//        } else if (inputBean.getRadius() == null || inputBean.getRadius().trim().isEmpty()) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_RADIOUS;
//        } else if (!Validation.isValidFloatValue(inputBean.getRadius().trim())) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_RADIOUS;
        } else if (inputBean.getPromoconditions() == null || inputBean.getPromoconditions().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PROMOCONDITION;
        } else if (inputBean.getPhoneno() == null || inputBean.getPhoneno().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_PHONENO;
//        } else if (inputBean.getMerchantwebsite() == null || inputBean.getMerchantwebsite().trim().isEmpty()) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_MERCHANTWEBSITE;
//        } else if (inputBean.getImage() == null || inputBean.getImage().trim().isEmpty()) {
//            message = MessageVarList.PROMOTION_MGT_EMPTY_IMAGE;
        } else if (inputBean.getRegioncode() == null || inputBean.getRegioncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_REGIONCODE;
        } else if (inputBean.getRegionname() == null || inputBean.getRegionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_REGIONNAME;
        } else if (inputBean.getMasterregoncode() == null || inputBean.getMasterregoncode().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_MASTERREGIONCODE;
        } else if (inputBean.getMasterregionname() == null || inputBean.getMasterregionname().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_MASTERREGIONNAME;
        } else if (inputBean.getCardtype() == null || inputBean.getCardtype().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_CARDTYPE;
        } else if (inputBean.getCardimage() == null || inputBean.getCardimage().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_CARDIMAGE;
        } else if (inputBean.getStartdate() == null || inputBean.getStartdate().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_STARTDATE;
        } else if (inputBean.getEnddate() == null || inputBean.getEnddate().trim().isEmpty()) {
            message = MessageVarList.PROMOTION_MGT_EMPTY_ENDDATE;
        } else if ((inputBean.getStartdate() != null || !inputBean.getStartdate().trim().isEmpty())
                && (inputBean.getEnddate() != null || !inputBean.getEnddate().trim().isEmpty())) {

            Date fromDate = new Date();
            Date toDate = new Date();
            if (message.isEmpty()) {
                try {
                    fromDate = Common.convertStringtoDate3(inputBean.getStartdate());
                } catch (Exception ex) {
                    message = MessageVarList.PROMOTION_MGT_INVALID_STARTDATE;
                }
            }
            if (message.isEmpty()) {
                try {
                    toDate = Common.convertStringtoDate3(inputBean.getEnddate());
                } catch (Exception ex) {
                    message = MessageVarList.PROMOTION_MGT_INVALID_ENDDATE;
                }
            }

            if (message.isEmpty() && fromDate.after(toDate)) {
                message = MessageVarList.PROMOTION_MGT_INVALID_DATERANGE;
            }
        }
        if (message.isEmpty()) {
            if (inputBean.getIspushnotification() == null || inputBean.getIspushnotification().trim().isEmpty()) {
                message = MessageVarList.PROMOTION_MGT_EMPTY_PUSH_NOTIFICATION_ENABLE;
            } else if (!inputBean.getIspushnotification().trim().equals("1") && !inputBean.getIspushnotification().trim().equals("0")) {
                message = MessageVarList.PROMOTION_MGT_INVALID_PUSH_NOTIFICATION_ENABLE;
            }
        }
        return message;
    }

    public StringBuffer makeCSVReport(PromotionMgtInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String orderby = " order by LPAD(u.id.subregioncode, 10) ";
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from Promotions u where " + where + orderby;
            Query querySearch = session.createQuery(sqlSearch);

            Iterator it = querySearch.iterate();
            content = new StringBuffer();

            //write column headers to csv file
            content.append("Subregion Code");
            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Promotions Category");
            content.append(',');
            content.append("Subregion Name");
            content.append(',');
            content.append("Subregion Address");
            content.append(',');
            content.append("Free Text");
            content.append(',');
            content.append("Information");
            content.append(',');
            content.append("Trading Hours");
            content.append(',');
            content.append("Longitude");
            content.append(',');
            content.append("Latitude");
            content.append(',');
//            content.append("Radius[In kilometer(km)]");
//            content.append(',');
            content.append("Promotion Conditions");
            content.append(',');
            content.append("Phone Number");
            content.append(',');
            content.append("Merchant Web Site");
            content.append(',');
            content.append("Region Code");
            content.append(',');
            content.append("Region Name");
            content.append(',');
            content.append("Master Region Code");
            content.append(',');
            content.append("Master Region Name");
            content.append(',');
            content.append("Card Type");
            content.append(',');
            content.append("Card Image");
            content.append(',');
            content.append("Push Notification Enabled(1/0)");
            content.append(',');
            content.append("Mobile Img - Firebase URL");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Checker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');

            while (it.hasNext()) {

                PromotionMgtBean promotionBean = new PromotionMgtBean();
                Promotions promotion = (Promotions) it.next();

                try {
                    content.append(promotion.getSubregioncode().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(promotion.getStatus().getStatuscode());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getPromotionsCategories().getCatcode()));
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getSubregionname().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getSubreginaddress().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getFreetext().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getInfo().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getTradinghours().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(promotion.getLatitude().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(promotion.getLongitude().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
//                try {
//                    if (promotion.getRadius() != null) {
//                        content.append(String.format("%.2f", promotion.getRadius()).toString());
//                    } else {
//                        content.append("--");
//                    }
//                    content.append(',');
//                } catch (NullPointerException npe) {
//                    content.append("--");
//                    content.append(',');
//                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getPromoconditions().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getPhoneno().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getMerchantwebsite().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getRegioncode().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getRegionname().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    promotionBean.setRegionname("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getMasterregoncode().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getMasterregionname().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getCardtype().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getCardimage().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    if (promotion.getIspushnotification()) {
                        content.append("1");
                    } else {
                        content.append("0");
                    }
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("0");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(promotion.getImage().toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(promotion.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(promotion.getChecker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(promotion.getCreatedtime().toString().substring(0, 19));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(promotion.getLastupdatedtime().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    content.append("--");
                }
                content.append('\n');
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    public String setFirebaseParam(PromotionMgtInputBean inputBean) throws Exception {
        Session session = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = this.FIRE_BASE_SQL;
            Query query = session.createSQLQuery(sql);
            List<Object[]> objectArrList = (List<Object[]>) query.list();
            if (objectArrList.size() > 0) {
                Object[] objArr = objectArrList.get(0);
                try {
                    inputBean.setApiKey(objArr[0].toString());
                } catch (Exception e) {
                    inputBean.setApiKey("");
                }
                try {
                    inputBean.setAuthDomain(objArr[1].toString());
                } catch (Exception e) {
                    inputBean.setAuthDomain("");
                }
                try {
                    inputBean.setDatabaseUrl(objArr[2].toString());
                } catch (Exception e) {
                    inputBean.setDatabaseUrl("");
                }
                try {
                    inputBean.setProjectId(objArr[3].toString());
                } catch (Exception e) {
                    inputBean.setProjectId("");
                }
                try {
                    inputBean.setStorageBucket(objArr[4].toString());
                } catch (Exception e) {
                    inputBean.setStorageBucket("");
                }
                try {
                    inputBean.setMessagingSenderId(objArr[5].toString());
                } catch (Exception e) {
                    inputBean.setMessagingSenderId("");
                }
                try {
                    inputBean.setAppId(objArr[6].toString());
                } catch (Exception e) {
                    inputBean.setAppId("");
                }
                try {
                    inputBean.setAuthEmail(objArr[7].toString());
                } catch (Exception e) {
                    inputBean.setAuthEmail("");
                }
                try {
                    inputBean.setAuthPassword(objArr[8].toString());
                } catch (Exception e) {
                    inputBean.setAuthPassword("");
                }
                try {
                    System.out.println("Faire base environment for " + objArr[9].toString());
                } catch (Exception e) {
                    System.out.println("Faire base environment not given");
                }

            } else {
                message = "Firbase detail not found";
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return message;
    }
}
