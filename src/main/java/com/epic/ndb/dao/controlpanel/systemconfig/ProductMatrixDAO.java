/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductMatrixBean;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductMatrixInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductMatrixPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.ProductCurrency;
import com.epic.ndb.util.mapping.ProductMatrix;
import com.epic.ndb.util.mapping.ProductMatrixCurrency;
import com.epic.ndb.util.mapping.ProductMatrixCurrencyId;
import com.epic.ndb.util.mapping.ProductMatrixId;
import com.epic.ndb.util.mapping.ProductType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class ProductMatrixDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<ProductMatrixBean> getSearchList(ProductMatrixInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<ProductMatrixBean> dataList = new ArrayList<ProductMatrixBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(u.id.debitProductType) from ProductMatrix as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from ProductMatrix u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    ProductMatrixBean productmatrixBean = new ProductMatrixBean();
                    ProductMatrix productmatrix = (ProductMatrix) it.next();

                    try {
                        productmatrixBean.setDebitProductType(productmatrix.getId().getDebitProductType().toString());
                        productmatrixBean.setDebitProductTypeDes(productmatrix.getProductTypeByDebitProductType().getProductName());
                    } catch (Exception npe) {
                        productmatrixBean.setDebitProductType("--");
                        productmatrixBean.setDebitProductTypeDes("--");
                    }
                    try {
                        productmatrixBean.setCreditProductType(productmatrix.getId().getCreditProductType().toString());
                        productmatrixBean.setCreditProductTypeDes(productmatrix.getProductTypeByCreditProductType().getProductName());
                    } catch (Exception npe) {
                        productmatrixBean.setCreditProductType("--");
                        productmatrixBean.setCreditProductTypeDes("--");
                    }
                    try {
                        productmatrixBean.setStatus(productmatrix.getStatus().getDescription());
                    } catch (Exception npe) {
                        productmatrixBean.setStatus("--");
                    }
//                    try {
//                        productmatrixBean.setDebitCurrency(productmatrix.getProductCurrency().getDescription().toString());
//                    } catch (Exception npe) {
//                        productmatrixBean.setDebitCurrency("--");
//                    }
                    try {
                        productmatrixBean.setMaker(productmatrix.getMaker().toString());
                    } catch (NullPointerException npe) {
                        productmatrixBean.setMaker("--");
                    }
                    try {
                        productmatrixBean.setChecker(productmatrix.getChecker().toString());
                    } catch (NullPointerException npe) {
                        productmatrixBean.setChecker("--");
                    }
                    try {
                        productmatrixBean.setCreatedtime(productmatrix.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        productmatrixBean.setCreatedtime("--");
                    }
                    try {
                        productmatrixBean.setLastupdatedtime(productmatrix.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        productmatrixBean.setLastupdatedtime("--");
                    }

                    productmatrixBean.setFullCount(count);

                    dataList.add(productmatrixBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(ProductMatrixInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getCreditProductTypeSearch() != null && !inputBean.getCreditProductTypeSearch().isEmpty()) {
            where += " and u.id.creditProductType = '" + inputBean.getCreditProductTypeSearch().trim() + "'";
        }
        if (inputBean.getDebitProductTypeSearch() != null && !inputBean.getDebitProductTypeSearch().isEmpty()) {
            where += " and u.id.debitProductType = '" + inputBean.getDebitProductTypeSearch().trim() + "'";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getDebitCurrencySearch() != null && !inputBean.getDebitCurrencySearch().isEmpty()) {
            where += " and u.productCurrency.currencyCode = '" + inputBean.getDebitCurrencySearch().trim() + "'";
        }

        return where;
    }

    public List<ProductMatrixPendBean> getPendingProductMatrixList(ProductMatrixInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<ProductMatrixPendBean> dataList = new ArrayList<ProductMatrixPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.PRODUCT_MATRIX_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.PRODUCT_MATRIX_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    ProductMatrixPendBean pendData = new ProductMatrixPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        pendData.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        pendData.setId("--");
                    }
                    try {
                        pendData.setOperation(pTask.getTask().getDescription().toString());
                        pendData.setOperationcode(pTask.getTask().getTaskcode().toString());
                    } catch (NullPointerException npe) {
                        pendData.setOperation("--");
                        pendData.setOperationcode("--");
                    }

//                    String[] penArray = null;
//                    if (pTask.getFields() != null) {
//                        penArray = pTask.getFields().split("\\|");
//                    }
//
//                    try {
//                        cardCenter.setBankcode(penArray[0]);
//                    } catch (NullPointerException npe) {
//                        cardCenter.setBankcode("--");
//                    } catch (Exception ex) {
//                        cardCenter.setBankcode("--");
//                    }
                    String[] pKeyArray = null;
                    if (pTask.getPKey() != null && !pTask.getPKey().isEmpty()) {
                        pKeyArray = pTask.getPKey().split("\\|");
                        try {
                            ProductType productType = (ProductType) session.get(ProductType.class, pKeyArray[0]);
                            pendData.setDebitProductType(productType.getProductName());
                        } catch (Exception ex) {
                            pendData.setDebitProductType("--");
                        }
                        try {
                            ProductType productType = (ProductType) session.get(ProductType.class, pKeyArray[1]);
                            pendData.setCreditProductType(productType.getProductName());
                        } catch (Exception ex) {
                            pendData.setCreditProductType("--");
                        }
                    } else {
                        pendData.setDebitProductType("--");
                        pendData.setCreditProductType("--");
                    }
                    try {
                        pendData.setFields(pTask.getFields().toString());
                    } catch (NullPointerException npe) {
                        pendData.setFields("--");
                    } catch (Exception ex) {
                        pendData.setFields("--");
                    }
                    try {
                        pendData.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        pendData.setStatus("--");
                    }
                    try {
                        pendData.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        pendData.setCreatetime("--");
                    }
                    try {
                        pendData.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        pendData.setCreateduser("--");
                    } catch (Exception ex) {
                        pendData.setCreateduser("--");
                    }

                    pendData.setFullCount(count);

                    dataList.add(pendData);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public void findProductCurrencyList(ProductMatrixInputBean bean) throws Exception {

        List<ProductCurrency> newList = new ArrayList<ProductCurrency>();

        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            Query query = session.createQuery("from ProductCurrency as t where t.status.statuscode=:status order by t.description asc").setString("status", CommonVarList.STATUS_ACTIVE);

            newList = (List<ProductCurrency>) query.list();

            ProductCurrency productCurrency;

            for (Iterator<ProductCurrency> it = newList.iterator(); it.hasNext();) {

                productCurrency = it.next();

                CommonKeyVal databean = new CommonKeyVal();
                databean.setValue(productCurrency.getDescription());
                databean.setKey(productCurrency.getCurrencyCode());
                bean.getNewCreditCurrencyList().add(databean);
                bean.getNewDebitCurrencyList().add(databean);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
    }

    public String insertProductMatrix(ProductMatrixInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            ProductMatrixId id = new ProductMatrixId();
            id.setCreditProductType(inputBean.getCreditProductType());
            id.setDebitProductType(inputBean.getDebitProductType());

            if ((ProductMatrix) session.get(ProductMatrix.class, id) == null) {

                String pKey = inputBean.getDebitProductType().trim() + "|" + inputBean.getCreditProductType().trim();

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", pKey)
                        .setString("pagecode", audit.getPagecode());
                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(pKey);
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PRODUCT_MATRIX_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public ProductMatrix findProductMatrixById(String debitProductType, String creditProductType) throws Exception {
        ProductMatrix productMatrix = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from ProductMatrix as u where u.id.debitProductType=:debitProductType and u.id.creditProductType=:creditProductType";
            Query query = session.createQuery(sql).setString("debitProductType", debitProductType).setString("creditProductType", creditProductType);
            productMatrix = (ProductMatrix) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return productMatrix;

    }

//    public void getProductCurrencyListById(ProductMatrixInputBean inputBean) throws Exception {
//
//        String debitProductType = inputBean.getDebitProductType();
//        String creditProductType = inputBean.getCreditProductType();
//        List<ProductCurrency> newList = new ArrayList<ProductCurrency>();
//        List<ProductCurrency> currentList = new ArrayList<ProductCurrency>();
//        Session session = null;
//        try {
//            session = HibernateInit.sessionFactory.openSession();
//
//            String sql1 = "from ProductCurrency as c where c.status.statuscode=:status and c.currencyCode in (select mc.id.creditCurrencyCode from ProductCreditCurrency as mc where mc.id.debitProductType=:debitProductType and mc.id.creditProductType=:creditProductType)";
//            String sql2 = "from ProductCurrency as c where c.status.statuscode=:status and c.currencyCode not in (select mc.id.creditCurrencyCode from ProductCreditCurrency as mc where mc.id.debitProductType=:debitProductType and mc.id.creditProductType=:creditProductType)";
//
//            Query query1 = session.createQuery(sql1).setString("status", CommonVarList.STATUS_ACTIVE).setString("debitProductType", debitProductType).setString("creditProductType", creditProductType);
//            Query query2 = session.createQuery(sql2).setString("status", CommonVarList.STATUS_ACTIVE).setString("debitProductType", debitProductType).setString("creditProductType", creditProductType);
//
//            currentList = (List<ProductCurrency>) query1.list();
//            newList = (List<ProductCurrency>) query2.list();
//
//            for (Iterator<ProductCurrency> it = newList.iterator(); it.hasNext();) {
//                ProductCurrency currency = it.next();
//                CommonKeyVal databean = new CommonKeyVal();
//                databean.setValue(currency.getDescription());
//                databean.setKey(currency.getCurrencyCode());
//                inputBean.getNewCreditCurrencyList().add(databean);
//            }
//
//            for (Iterator<ProductCurrency> it = currentList.iterator(); it.hasNext();) {
//                ProductCurrency currency = it.next();
//                CommonKeyVal databean = new CommonKeyVal();
//                databean.setValue(currency.getDescription());
//                databean.setKey(currency.getCurrencyCode());
//                inputBean.getCurrentCreditCurrencyList().add(databean);
//            }
//        } catch (Exception e) {
//            throw e;
//        } finally {
//            try {
//                session.flush();
//                session.close();
//            } catch (Exception e) {
//                throw e;
//            }
//        }
//    }
    public void getProductCurrencyListById(ProductMatrixInputBean inputBean) throws Exception {

        String debitProductType = inputBean.getDebitProductType();
        String creditProductType = inputBean.getCreditProductType();
        List<ProductCurrency> newList = new ArrayList<ProductCurrency>();
        List<ProductCurrency> currentList = new ArrayList<ProductCurrency>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql1 = "from ProductCurrency as c where c.status.statuscode=:status and c.currencyCode in (select mc.id.currencyCode from ProductMatrixCurrency as mc where mc.id.debitProductType=:debitProductType and mc.id.creditProductType=:creditProductType and mc.id.txnType=:txnType)";
            String sql2 = "from ProductCurrency as c where c.status.statuscode=:status and c.currencyCode not in (select mc.id.currencyCode from ProductMatrixCurrency as mc where mc.id.debitProductType=:debitProductType and mc.id.creditProductType=:creditProductType and mc.id.txnType=:txnType)";

            Query query1 = session.createQuery(sql1).setString("status", CommonVarList.STATUS_ACTIVE).setString("debitProductType", debitProductType).setString("creditProductType", creditProductType).setString("txnType", CommonVarList.CREDIT_TXN_TYPE);
            Query query2 = session.createQuery(sql2).setString("status", CommonVarList.STATUS_ACTIVE).setString("debitProductType", debitProductType).setString("creditProductType", creditProductType).setString("txnType", CommonVarList.CREDIT_TXN_TYPE);
            Query query3 = session.createQuery(sql1).setString("status", CommonVarList.STATUS_ACTIVE).setString("debitProductType", debitProductType).setString("creditProductType", creditProductType).setString("txnType", CommonVarList.DEBIT_TXN_TYPE);
            Query query4 = session.createQuery(sql2).setString("status", CommonVarList.STATUS_ACTIVE).setString("debitProductType", debitProductType).setString("creditProductType", creditProductType).setString("txnType", CommonVarList.DEBIT_TXN_TYPE);

            //--------------------------------Credit Currency New,Current List------------------------//
            currentList = (List<ProductCurrency>) query1.list();
            newList = (List<ProductCurrency>) query2.list();

            for (Iterator<ProductCurrency> it = newList.iterator(); it.hasNext();) {
                ProductCurrency currency = it.next();
                CommonKeyVal databean = new CommonKeyVal();
                databean.setValue(currency.getDescription());
                databean.setKey(currency.getCurrencyCode());
                inputBean.getNewCreditCurrencyList().add(databean);
            }

            for (Iterator<ProductCurrency> it = currentList.iterator(); it.hasNext();) {
                ProductCurrency currency = it.next();
                CommonKeyVal databean = new CommonKeyVal();
                databean.setValue(currency.getDescription());
                databean.setKey(currency.getCurrencyCode());
                inputBean.getCurrentCreditCurrencyList().add(databean);
            }

            //--------------------------------Debit Currency New,Current List------------------------//
            currentList = (List<ProductCurrency>) query3.list();
            newList = (List<ProductCurrency>) query4.list();

            for (Iterator<ProductCurrency> it = newList.iterator(); it.hasNext();) {
                ProductCurrency currency = it.next();
                CommonKeyVal databean = new CommonKeyVal();
                databean.setValue(currency.getDescription());
                databean.setKey(currency.getCurrencyCode());
                inputBean.getNewDebitCurrencyList().add(databean);
            }

            for (Iterator<ProductCurrency> it = currentList.iterator(); it.hasNext();) {
                ProductCurrency currency = it.next();
                CommonKeyVal databean = new CommonKeyVal();
                databean.setValue(currency.getDescription());
                databean.setKey(currency.getCurrencyCode());
                inputBean.getCurrentDebitCurrencyList().add(databean);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
    }

    public String updateProductMatrix(ProductMatrixInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            String pKey = inputBean.getDebitProductType().trim() + "|" + inputBean.getCreditProductType().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                ProductMatrixId id = new ProductMatrixId();
                id.setCreditProductType(inputBean.getCreditProductType());
                id.setDebitProductType(inputBean.getDebitProductType());

                ProductMatrix u = (ProductMatrix) session.get(ProductMatrix.class, id);
                if (u != null) {

//                    StringBuilder stringBuilderOldVal = new StringBuilder();
//                    stringBuilderOldVal.append(u.getId().getDebitProductType())
//                            .append("|").append(u.getId().getCreditProductType())
//                            .append("|").append(u.getStatus().getStatuscode())
//                            //                            .append("|").append(u.getProductCurrency().getCurrencyCode())
//                            .append("|");
//                    String sql2 = "select mc.id.currencyCode from ProductMatrixCurrency as mc where mc.id.debitProductType=:debitProductType and mc.id.creditProductType=:creditProductType and mc.id.creditProductType=:creditProductType and mc.id.txnType=:txnType ";
//                    Query query2 = session.createQuery(sql2)
//                            .setString("debitProductType", inputBean.getDebitProductType())
//                            .setString("creditProductType", inputBean.getCreditProductType())
//                            .setString("txnType", CommonVarList.DEBIT_TXN_TYPE);
//                    Query query3 = session.createQuery(sql2)
//                            .setString("debitProductType", inputBean.getDebitProductType())
//                            .setString("creditProductType", inputBean.getCreditProductType())
//                            .setString("txnType", CommonVarList.CREDIT_TXN_TYPE);
//                    List<String> debitCurrencyList = query2.list();
//                    List<String> creditCurrencyList = query3.list();
//                    String stringSeparator = "";
//
//                    for (String debitCurrency : debitCurrencyList) {
//                        stringBuilderOldVal.append(stringSeparator).append(debitCurrency);
//                        stringSeparator = ",";
//                    }
//
//                    stringBuilderOldVal.append("|");
//                    stringSeparator = "";
//                    for (String creditCurrency : creditCurrencyList) {
//                        stringBuilderOldVal.append(stringSeparator).append(creditCurrency);
//                        stringSeparator = ",";
//                    }
//
//                    audit.setOldvalue(stringBuilderOldVal.toString());
                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(pKey.trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PRODUCT_MATRIX_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteProductMatrix(ProductMatrixInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String pKey = inputBean.getDebitProductType().trim() + "|" + inputBean.getCreditProductType().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                ProductMatrixId id = new ProductMatrixId();
                id.setCreditProductType(inputBean.getCreditProductType());
                id.setDebitProductType(inputBean.getDebitProductType());

                ProductMatrix u = (ProductMatrix) session.get(ProductMatrix.class, id);
                if (u != null) {

                    StringBuilder stringBuilderNewVal = new StringBuilder();
                    stringBuilderNewVal.append(u.getId().getDebitProductType())
                            .append("|").append(u.getId().getCreditProductType())
                            .append("|").append(u.getStatus().getStatuscode())
                            //                            .append("|").append(u.getProductCurrency().getCurrencyCode())
                            .append("|");

                    String sql2 = "select mc.id.currencyCode from ProductMatrixCurrency as mc where mc.id.debitProductType=:debitProductType and mc.id.creditProductType=:creditProductType and mc.id.creditProductType=:creditProductType and mc.id.txnType=:txnType ";
                    Query query2 = session.createQuery(sql2)
                            .setString("debitProductType", inputBean.getDebitProductType())
                            .setString("creditProductType", inputBean.getCreditProductType())
                            .setString("txnType", CommonVarList.DEBIT_TXN_TYPE);
                    Query query3 = session.createQuery(sql2)
                            .setString("debitProductType", inputBean.getDebitProductType())
                            .setString("creditProductType", inputBean.getCreditProductType())
                            .setString("txnType", CommonVarList.CREDIT_TXN_TYPE);

                    List<String> debitCurrencyList = query2.list();
                    List<String> creditCurrencyList = query3.list();

                    String stringSeparator = "";

                    for (String debitCurrency : debitCurrencyList) {
                        stringBuilderNewVal.append(stringSeparator).append(debitCurrency);
                        stringSeparator = ",";
                    }

                    stringBuilderNewVal.append("|");
                    stringSeparator = "";
                    for (String creditCurrency : creditCurrencyList) {
                        stringBuilderNewVal.append(stringSeparator).append(creditCurrency);
                        stringSeparator = ",";
                    }

                    audit.setNewvalue(stringBuilderNewVal.toString());

                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(pKey.trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.PRODUCT_MATRIX_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmProductMatrix(ProductMatrixInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            txn = session.beginTransaction();

            InputStreamReader isr = null;
            BufferedReader br = null;

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                String[] penCreditCurrencyArray = null;
                String[] penDebitCurrencyArray = null;
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());

                    penArray = pentask.getFields().split("\\|");
                    if (penArray.length >= 5) {
                        penDebitCurrencyArray = penArray[3].split("\\,");
                        penCreditCurrencyArray = penArray[4].split("\\,");
                    }
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    ProductMatrixId id = new ProductMatrixId();

                    id.setDebitProductType(penArray[0]);
                    id.setCreditProductType(penArray[1]);

                    ProductMatrix u = (ProductMatrix) session.get(ProductMatrix.class, id);
                    if (u == null) {

                        ProductMatrix productMatrix = new ProductMatrix();

                        productMatrix.setId(id);

                        Status st = (Status) session.get(Status.class, penArray[2]);
                        productMatrix.setStatus(st);

//                        ProductCurrency debitCurrency = (ProductCurrency) session.get(ProductCurrency.class, penArray[3]);
//                        productMatrix.setProductCurrency(debitCurrency);
                        if (penDebitCurrencyArray != null) {
                            for (String debitCurrency : penDebitCurrencyArray) {

                                ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                                productMatrixCurrencyId.setDebitProductType(penArray[0]);
                                productMatrixCurrencyId.setCreditProductType(penArray[1]);
                                productMatrixCurrencyId.setCurrencyCode(debitCurrency);
                                productMatrixCurrencyId.setTxnType(CommonVarList.DEBIT_TXN_TYPE);

                                ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                                productMatrixCurrency.setId(productMatrixCurrencyId);
//                                productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                                productMatrixCurrency.setStatus(penArray[2]);

                                productMatrixCurrency.setCreatedtime(timestampDate);
                                productMatrixCurrency.setLastupdatedtime(timestampDate);
                                productMatrixCurrency.setMaker(pentask.getCreateduser());
                                productMatrixCurrency.setChecker(audit.getLastupdateduser());

                                session.save(productMatrixCurrency);

                            }
                        }
                        if (penCreditCurrencyArray != null) {
                            for (String creditCurrency : penCreditCurrencyArray) {

                                ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                                productMatrixCurrencyId.setDebitProductType(penArray[0]);
                                productMatrixCurrencyId.setCreditProductType(penArray[1]);
                                productMatrixCurrencyId.setCurrencyCode(creditCurrency);
                                productMatrixCurrencyId.setTxnType(CommonVarList.CREDIT_TXN_TYPE);

                                ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                                productMatrixCurrency.setId(productMatrixCurrencyId);
//                                productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                                productMatrixCurrency.setStatus(penArray[2]);

                                productMatrixCurrency.setCreatedtime(timestampDate);
                                productMatrixCurrency.setLastupdatedtime(timestampDate);
                                productMatrixCurrency.setMaker(pentask.getCreateduser());
                                productMatrixCurrency.setChecker(audit.getLastupdateduser());

                                session.saveOrUpdate(productMatrixCurrency);

//                                ProductCreditCurrencyId creditCurrencyId = new ProductCreditCurrencyId();
//
//                                creditCurrencyId.setDebitProductType(penArray[0]);
//                                creditCurrencyId.setCreditProductType(penArray[1]);
//                                creditCurrencyId.setCreditCurrencyCode(creditCurrency);
//
//                                ProductCreditCurrency productCreditCurrency = new ProductCreditCurrency();
//
//                                productCreditCurrency.setId(creditCurrencyId);
//                                productCreditCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
//
//                                productCreditCurrency.setCreatedtime(timestampDate);
//                                productCreditCurrency.setLastupdatedtime(timestampDate);
//                                productCreditCurrency.setMaker(pentask.getCreateduser());
//                                productCreditCurrency.setChecker(audit.getLastupdateduser());
//
//                                session.save(productCreditCurrency);
                            }
                        }
                        productMatrix.setCreatedtime(timestampDate);
                        productMatrix.setLastupdatedtime(timestampDate);
                        productMatrix.setMaker(pentask.getCreateduser());
                        productMatrix.setChecker(audit.getLastupdateduser());

                        session.save(productMatrix);
                    } else {
                        Status st = (Status) session.get(Status.class, penArray[2]);
                        u.setStatus(st);

//                        ProductCurrency debitCurrency = (ProductCurrency) session.get(ProductCurrency.class, penArray[3]);
//                        u.setProductCurrency(debitCurrency);
                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        String sql2 = "from ProductMatrixCurrency b where b.id.debitProductType=:debitProductType and b.id.creditProductType=:creditProductType and b.id.txnType=:txnType ";

//----------------------------------- multiple debit currency selection-----------------------------------------------//
                        Query query2 = session.createQuery(sql2).setString("debitProductType", penArray[0]).setString("creditProductType", penArray[1]).setString("txnType", CommonVarList.DEBIT_TXN_TYPE);

                        List<ProductMatrixCurrency> dbList = query2.list();
                        List<String> debitCurrencyList = new ArrayList<String>(Arrays.asList(penDebitCurrencyArray));

                        for (ProductMatrixCurrency pt : dbList) {

                            if (debitCurrencyList.contains(String.valueOf(pt.getId().getCurrencyCode()))) {
                                //                                pt.setStatus(CommonVarList.STATUS_ACTIVE);//Is it needed
                                pt.setStatus(penArray[2]);//Is it needed
                                pt.setLastupdatedtime(timestampDate);
                                pt.setMaker(pentask.getCreateduser());
                                pt.setChecker(audit.getLastupdateduser());
                                session.update(pt);
                                debitCurrencyList.remove(String.valueOf(pt.getId().getCurrencyCode()));
                            } else {
                                session.delete(pt);
                            }
                        }

                        for (String val : debitCurrencyList) {
                            ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                            productMatrixCurrencyId.setDebitProductType(penArray[0]);
                            productMatrixCurrencyId.setCreditProductType(penArray[1]);
                            productMatrixCurrencyId.setCurrencyCode(val);
                            productMatrixCurrencyId.setTxnType(CommonVarList.DEBIT_TXN_TYPE);

                            ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                            productMatrixCurrency.setId(productMatrixCurrencyId);
//                            productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                            productMatrixCurrency.setStatus(penArray[2]);

                            productMatrixCurrency.setCreatedtime(timestampDate);
                            productMatrixCurrency.setLastupdatedtime(timestampDate);
                            productMatrixCurrency.setMaker(pentask.getCreateduser());
                            productMatrixCurrency.setChecker(audit.getLastupdateduser());
                            session.save(productMatrixCurrency);

                        }

//---------------------------------------multiple credit currency selection----------------------------------------------//
                        Query query3 = session.createQuery(sql2).setString("debitProductType", penArray[0]).setString("creditProductType", penArray[1]).setString("txnType", CommonVarList.CREDIT_TXN_TYPE);

                        List<ProductMatrixCurrency> dbList2 = query3.list();
                        List<String> creditCurrencyList = new ArrayList<String>(Arrays.asList(penCreditCurrencyArray));

                        for (ProductMatrixCurrency pt : dbList2) {

                            if (creditCurrencyList.contains(String.valueOf(pt.getId().getCurrencyCode()))) {
                                //                                pt.setStatus(CommonVarList.STATUS_ACTIVE);//Is it needed
                                pt.setStatus(penArray[2]);//Is it needed
                                pt.setLastupdatedtime(timestampDate);
                                pt.setMaker(pentask.getCreateduser());
                                pt.setChecker(audit.getLastupdateduser());
                                session.update(pt);
                                creditCurrencyList.remove(String.valueOf(pt.getId().getCurrencyCode()));
                            } else {
                                session.delete(pt);
                            }
                        }

                        for (String val : creditCurrencyList) {
                            ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                            productMatrixCurrencyId.setDebitProductType(penArray[0]);
                            productMatrixCurrencyId.setCreditProductType(penArray[1]);
                            productMatrixCurrencyId.setCurrencyCode(val);
                            productMatrixCurrencyId.setTxnType(CommonVarList.CREDIT_TXN_TYPE);

                            ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                            productMatrixCurrency.setId(productMatrixCurrencyId);
//                            productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                            productMatrixCurrency.setStatus(penArray[2]);

                            productMatrixCurrency.setCreatedtime(timestampDate);
                            productMatrixCurrency.setLastupdatedtime(timestampDate);
                            productMatrixCurrency.setMaker(pentask.getCreateduser());
                            productMatrixCurrency.setChecker(audit.getLastupdateduser());
                            session.save(productMatrixCurrency);

                        }

                        session.update(u);
                    }
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on product matrix (  debit product type: " + penArray[0] + ", credit product type: " + penArray[1] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    ProductMatrixId id = new ProductMatrixId();

                    id.setDebitProductType(penArray[0]);
                    id.setCreditProductType(penArray[1]);

                    ProductMatrix u = (ProductMatrix) session.get(ProductMatrix.class, id);

                    if (u != null) {
                        //------------------------audit old value start-------------------------------
                        StringBuilder stringBuilderOld = new StringBuilder();
                        stringBuilderOld.append(penArray[0])
                                .append("|").append(penArray[1])
                                .append("|").append(u.getStatus().getStatuscode())
                                .append("|");
                        //------------------------audit old value end-------------------------------

                        Status st = (Status) session.get(Status.class, penArray[2]);
                        u.setStatus(st);

//                        ProductCurrency debitCurrency = (ProductCurrency) session.get(ProductCurrency.class, penArray[3]);
//                        u.setProductCurrency(debitCurrency);
                        String sql2 = "from ProductMatrixCurrency b where b.id.debitProductType=:debitProductType and b.id.creditProductType=:creditProductType and b.id.txnType=:txnType ";

//----------------------------------- multiple debit currency selection-----------------------------------------------//
                        Query query2 = session.createQuery(sql2).setString("debitProductType", penArray[0]).setString("creditProductType", penArray[1]).setString("txnType", CommonVarList.DEBIT_TXN_TYPE);

                        List<ProductMatrixCurrency> dbList = query2.list();
                        List<String> debitCurrencyList = new ArrayList<String>(Arrays.asList(penDebitCurrencyArray));

                        String stringSeparator = "";
                        for (ProductMatrixCurrency pt : dbList) {

                            //------------------------audit old value start-------------------------------
                            stringBuilderOld.append(stringSeparator).append(pt.getId().getCurrencyCode());
                            stringSeparator = ",";
                            //------------------------audit old value end-------------------------------

                            if (debitCurrencyList.contains(String.valueOf(pt.getId().getCurrencyCode()))) {
                                //                                pt.setStatus(CommonVarList.STATUS_ACTIVE);//Is it needed
                                pt.setStatus(penArray[2]);//Is it needed
                                pt.setLastupdatedtime(timestampDate);
                                pt.setMaker(pentask.getCreateduser());
                                pt.setChecker(audit.getLastupdateduser());
                                session.update(pt);
                                debitCurrencyList.remove(String.valueOf(pt.getId().getCurrencyCode()));
                            } else {
                                session.delete(pt);
                            }
                        }

                        for (String val : debitCurrencyList) {
                            ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                            productMatrixCurrencyId.setDebitProductType(penArray[0]);
                            productMatrixCurrencyId.setCreditProductType(penArray[1]);
                            productMatrixCurrencyId.setCurrencyCode(val);
                            productMatrixCurrencyId.setTxnType(CommonVarList.DEBIT_TXN_TYPE);

                            ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                            productMatrixCurrency.setId(productMatrixCurrencyId);
//                            productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                            productMatrixCurrency.setStatus(penArray[2]);

                            productMatrixCurrency.setCreatedtime(timestampDate);
                            productMatrixCurrency.setLastupdatedtime(timestampDate);
                            productMatrixCurrency.setMaker(pentask.getCreateduser());
                            productMatrixCurrency.setChecker(audit.getLastupdateduser());
                            session.save(productMatrixCurrency);

                        }
                        //------------------------audit old value start-------------------------------
                        stringBuilderOld.append("|");
                        //------------------------audit old value end-------------------------------

//---------------------------------------multiple credit currency selection----------------------------------------------//
                        Query query3 = session.createQuery(sql2).setString("debitProductType", penArray[0]).setString("creditProductType", penArray[1]).setString("txnType", CommonVarList.CREDIT_TXN_TYPE);

                        List<ProductMatrixCurrency> dbList2 = query3.list();
                        List<String> creditCurrencyList = new ArrayList<String>(Arrays.asList(penCreditCurrencyArray));

                        stringSeparator = "";
                        for (ProductMatrixCurrency pt : dbList2) {

                            //------------------------audit old value start-------------------------------
                            stringBuilderOld.append(stringSeparator).append(pt.getId().getCurrencyCode());
                            stringSeparator = ",";
                            //------------------------audit old value end-------------------------------

                            if (creditCurrencyList.contains(String.valueOf(pt.getId().getCurrencyCode()))) {
                                //                                pt.setStatus(CommonVarList.STATUS_ACTIVE);//Is it needed
                                pt.setStatus(penArray[2]);//Is it needed
                                pt.setLastupdatedtime(timestampDate);
                                pt.setMaker(pentask.getCreateduser());
                                pt.setChecker(audit.getLastupdateduser());
                                session.update(pt);
                                creditCurrencyList.remove(String.valueOf(pt.getId().getCurrencyCode()));
                            } else {
                                session.delete(pt);
                            }
                        }

                        for (String val : creditCurrencyList) {
                            ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                            productMatrixCurrencyId.setDebitProductType(penArray[0]);
                            productMatrixCurrencyId.setCreditProductType(penArray[1]);
                            productMatrixCurrencyId.setCurrencyCode(val);
                            productMatrixCurrencyId.setTxnType(CommonVarList.CREDIT_TXN_TYPE);

                            ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                            productMatrixCurrency.setId(productMatrixCurrencyId);
//                            productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                            productMatrixCurrency.setStatus(penArray[2]);

                            productMatrixCurrency.setCreatedtime(timestampDate);
                            productMatrixCurrency.setLastupdatedtime(timestampDate);
                            productMatrixCurrency.setMaker(pentask.getCreateduser());
                            productMatrixCurrency.setChecker(audit.getLastupdateduser());
                            session.save(productMatrixCurrency);

                        }

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        session.update(u);

                        audit.setOldvalue(stringBuilderOld.toString());

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on product matrix (  debit product type: " + penArray[0] + ", credit product type: " + penArray[1] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPLOAD_TASK)) {
//                    String productmatrixid = "";
                    Blob csvBlob = pentask.getInputfile();
                    if (csvBlob != null) {
                        isr = new InputStreamReader(csvBlob.getBinaryStream());
                        br = new BufferedReader(isr);

                        String[] parts = new String[0];
                        int countrecord = 1;
                        int succesrec = 0;
                        String thisLine = null;
                        boolean getline = false;
                        String token = "";
                        while ((thisLine = br.readLine()) != null) {
                            if (thisLine.trim().equals("")) {
                                continue;
                            } else {
                                if (getline) {
//                              token = content.nextLine();
                                    token = thisLine;

//                                    System.err.println(token);
                                    try {
                                        parts = token.split("\\,", 4);
                                        inputBean.setDebitProductType(parts[0].trim());
                                        inputBean.setCreditProductType(parts[1].trim());
//                                        inputBean.setDebitCurrency(parts[2].trim());

                                        penDebitCurrencyArray = parts[2].replace(" ", "").split("\\/");
                                        penCreditCurrencyArray = parts[3].replace(" ", "").split("\\/");
                                        List<String> debitCurrencyList = new ArrayList<String>(Arrays.asList(penDebitCurrencyArray));
                                        List<String> creditCurrencyList = new ArrayList<String>(Arrays.asList(penCreditCurrencyArray));
                                        inputBean.setCurrentDebitCurrencyBox(this.getuniqueList(debitCurrencyList));
                                        inputBean.setCurrentCreditCurrencyBox(this.getuniqueList(creditCurrencyList));

                                        inputBean.setStatus(CommonVarList.STATUS_ACTIVE);

                                    } catch (Exception ee) {
                                        message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                                        break;
                                    }
                                    countrecord++;
                                    if (parts.length >= 4 && message.isEmpty()) {
//                                        message = this.validateInputsForCSV(inputBean);
//                                        if (message.isEmpty()) {
//                                            message = this.validateUpload(inputBean, session);
//                                            if (message.isEmpty()) {
                                        ProductMatrixId id = new ProductMatrixId();

                                        id.setDebitProductType(inputBean.getDebitProductType());
                                        id.setCreditProductType(inputBean.getCreditProductType());

                                        ProductMatrix u = (ProductMatrix) session.get(ProductMatrix.class, id);
                                        if (u == null) {
                                            ProductMatrix productMatrix = new ProductMatrix();

                                            productMatrix.setId(id);

                                            Status st = (Status) session.get(Status.class, inputBean.getStatus());
                                            productMatrix.setStatus(st);

//                                                    ProductCurrency debitCurrency = (ProductCurrency) session.get(ProductCurrency.class, inputBean.getDebitCurrency());
//                                                    productMatrix.setProductCurrency(debitCurrency);
                                            if (inputBean.getCurrentDebitCurrencyBox() != null) {
                                                for (String debitCurrency : inputBean.getCurrentDebitCurrencyBox()) {

                                                    ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                                                    productMatrixCurrencyId.setDebitProductType(inputBean.getDebitProductType());
                                                    productMatrixCurrencyId.setCreditProductType(inputBean.getCreditProductType());
                                                    productMatrixCurrencyId.setCurrencyCode(debitCurrency);
                                                    productMatrixCurrencyId.setTxnType(CommonVarList.DEBIT_TXN_TYPE);

                                                    ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                                                    productMatrixCurrency.setId(productMatrixCurrencyId);
//                                                    productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                                                    productMatrixCurrency.setStatus(inputBean.getStatus());

                                                    productMatrixCurrency.setCreatedtime(timestampDate);
                                                    productMatrixCurrency.setLastupdatedtime(timestampDate);
                                                    productMatrixCurrency.setMaker(pentask.getCreateduser());
                                                    productMatrixCurrency.setChecker(audit.getLastupdateduser());

                                                    session.save(productMatrixCurrency);

                                                }
                                            }
                                            if (inputBean.getCurrentCreditCurrencyBox() != null) {
                                                for (String creditCurrency : inputBean.getCurrentCreditCurrencyBox()) {

                                                    ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                                                    productMatrixCurrencyId.setDebitProductType(inputBean.getDebitProductType());
                                                    productMatrixCurrencyId.setCreditProductType(inputBean.getCreditProductType());
                                                    productMatrixCurrencyId.setCurrencyCode(creditCurrency);
                                                    productMatrixCurrencyId.setTxnType(CommonVarList.CREDIT_TXN_TYPE);

                                                    ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                                                    productMatrixCurrency.setId(productMatrixCurrencyId);
//                                                    productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                                                    productMatrixCurrency.setStatus(inputBean.getStatus());

                                                    productMatrixCurrency.setCreatedtime(timestampDate);
                                                    productMatrixCurrency.setLastupdatedtime(timestampDate);
                                                    productMatrixCurrency.setMaker(pentask.getCreateduser());
                                                    productMatrixCurrency.setChecker(audit.getLastupdateduser());

                                                    session.save(productMatrixCurrency);

                                                }
                                            }
//                                                    if (inputBean.getCurrentCreditCurrencyBox() != null) {
//                                                        for (String creditCurrency : inputBean.getCurrentCreditCurrencyBox()) {
//
//                                                            ProductCreditCurrencyId creditCurrencyId = new ProductCreditCurrencyId();
//
//                                                            creditCurrencyId.setDebitProductType(inputBean.getDebitProductType());
//                                                            creditCurrencyId.setCreditProductType(inputBean.getCreditProductType());
//                                                            creditCurrencyId.setCreditCurrencyCode(creditCurrency.trim());
//
//                                                            ProductCreditCurrency productCreditCurrency = new ProductCreditCurrency();
//
//                                                            productCreditCurrency.setId(creditCurrencyId);
//                                                            productCreditCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
//
//                                                            productCreditCurrency.setCreatedtime(timestampDate);
//                                                            productCreditCurrency.setLastupdatedtime(timestampDate);
//                                                            productCreditCurrency.setMaker(pentask.getCreateduser());
//                                                            productCreditCurrency.setChecker(audit.getLastupdateduser());
//
//                                                            session.save(productCreditCurrency);
//                                                        }
//                                                    }
                                            productMatrix.setCreatedtime(timestampDate);
                                            productMatrix.setLastupdatedtime(timestampDate);
                                            productMatrix.setMaker(pentask.getCreateduser());
                                            productMatrix.setChecker(audit.getLastupdateduser());

                                            session.save(productMatrix);

                                        } else {

//                                                    Status st = (Status) session.get(Status.class, inputBean.getStatus());
//                                                    u.setStatus(st);
//                                                    ProductCurrency debitCurrency = (ProductCurrency) session.get(ProductCurrency.class, inputBean.getDebitCurrency());
//                                                    u.setProductCurrency(debitCurrency);
                                            String sql2 = "from ProductMatrixCurrency b where b.id.debitProductType=:debitProductType and b.id.creditProductType=:creditProductType and b.id.txnType=:txnType ";

//----------------------------------------------------------- multiple debit currency selection-----------------------------------------------//
                                            Query query2 = session.createQuery(sql2).setString("debitProductType", inputBean.getDebitProductType()).setString("creditProductType", inputBean.getCreditProductType()).setString("txnType", CommonVarList.DEBIT_TXN_TYPE);

                                            List<ProductMatrixCurrency> dbList = query2.list();
                                            List<String> debitCurrencyList = inputBean.getCurrentDebitCurrencyBox();

                                            for (ProductMatrixCurrency pt : dbList) {

                                                if (debitCurrencyList.contains(String.valueOf(pt.getId().getCurrencyCode()))) {
                                                    //                                pt.setStatus(CommonVarList.STATUS_ACTIVE);//Is it needed
                                                    pt.setStatus(u.getStatus().getStatuscode());//Is it needed
                                                    pt.setLastupdatedtime(timestampDate);
                                                    pt.setMaker(pentask.getCreateduser());
                                                    pt.setChecker(audit.getLastupdateduser());
                                                    session.update(pt);
                                                    debitCurrencyList.remove(String.valueOf(pt.getId().getCurrencyCode()));
                                                } else {
                                                    session.delete(pt);
                                                }
                                            }

                                            for (String val : debitCurrencyList) {
                                                ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                                                productMatrixCurrencyId.setDebitProductType(inputBean.getDebitProductType());
                                                productMatrixCurrencyId.setCreditProductType(inputBean.getCreditProductType());
                                                productMatrixCurrencyId.setCurrencyCode(val);
                                                productMatrixCurrencyId.setTxnType(CommonVarList.DEBIT_TXN_TYPE);

                                                ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                                                productMatrixCurrency.setId(productMatrixCurrencyId);
//                                                productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                                                productMatrixCurrency.setStatus(u.getStatus().getStatuscode());

                                                productMatrixCurrency.setCreatedtime(timestampDate);
                                                productMatrixCurrency.setLastupdatedtime(timestampDate);
                                                productMatrixCurrency.setMaker(pentask.getCreateduser());
                                                productMatrixCurrency.setChecker(audit.getLastupdateduser());
                                                session.save(productMatrixCurrency);

                                            }

//----------------------------------------------------------------------multiple credit currency selection----------------------------------------------//
                                            Query query3 = session.createQuery(sql2).setString("debitProductType", inputBean.getDebitProductType()).setString("creditProductType", inputBean.getCreditProductType()).setString("txnType", CommonVarList.CREDIT_TXN_TYPE);

                                            List<ProductMatrixCurrency> dbList2 = query3.list();
                                            List<String> creditCurrencyList = inputBean.getCurrentCreditCurrencyBox();

                                            for (ProductMatrixCurrency pt : dbList2) {

                                                if (creditCurrencyList.contains(String.valueOf(pt.getId().getCurrencyCode()))) {
                                                    //                                pt.setStatus(CommonVarList.STATUS_ACTIVE);//Is it needed
                                                    pt.setStatus(u.getStatus().getStatuscode());//Is it needed
                                                    pt.setLastupdatedtime(timestampDate);
                                                    pt.setMaker(pentask.getCreateduser());
                                                    pt.setChecker(audit.getLastupdateduser());
                                                    session.update(pt);
                                                    creditCurrencyList.remove(String.valueOf(pt.getId().getCurrencyCode()));
                                                } else {
                                                    session.delete(pt);
                                                }
                                            }

                                            for (String val : creditCurrencyList) {
                                                ProductMatrixCurrencyId productMatrixCurrencyId = new ProductMatrixCurrencyId();

                                                productMatrixCurrencyId.setDebitProductType(inputBean.getDebitProductType());
                                                productMatrixCurrencyId.setCreditProductType(inputBean.getCreditProductType());
                                                productMatrixCurrencyId.setCurrencyCode(val);
                                                productMatrixCurrencyId.setTxnType(CommonVarList.CREDIT_TXN_TYPE);

                                                ProductMatrixCurrency productMatrixCurrency = new ProductMatrixCurrency();

                                                productMatrixCurrency.setId(productMatrixCurrencyId);
//                                                productMatrixCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
                                                productMatrixCurrency.setStatus(u.getStatus().getStatuscode());

                                                productMatrixCurrency.setCreatedtime(timestampDate);
                                                productMatrixCurrency.setLastupdatedtime(timestampDate);
                                                productMatrixCurrency.setMaker(pentask.getCreateduser());
                                                productMatrixCurrency.setChecker(audit.getLastupdateduser());
                                                session.save(productMatrixCurrency);

                                            }
//                                                    // multiple credit currency selection
//                                                    String sql2 = "from ProductCreditCurrency b where b.id.debitProductType=:debitProductType and b.id.creditProductType=:creditProductType ";
//                                                    Query query2 = session.createQuery(sql2).setString("debitProductType", inputBean.getDebitProductType()).setString("creditProductType", inputBean.getCreditProductType());
//
//                                                    List<ProductCreditCurrency> dbList = query2.list();
//                                                    List<String> creditCurrencyList = inputBean.getCurrentCreditCurrencyBox();
//
//                                                    for (ProductCreditCurrency pt : dbList) {
//
//                                                        if (creditCurrencyList.contains(String.valueOf(pt.getId().getCreditCurrencyCode()))) {
//                                                            //                                pt.setStatus(CommonVarList.STATUS_ACTIVE);//Is it needed
//                                                            pt.setLastupdatedtime(timestampDate);
//                                                            pt.setMaker(pentask.getCreateduser());
//                                                            pt.setChecker(audit.getLastupdateduser());
//                                                            session.update(pt);
//                                                            creditCurrencyList.remove(String.valueOf(pt.getId().getCreditCurrencyCode()));
//                                                        } else {
//                                                            session.delete(pt);
//                                                        }
//                                                    }
//
//                                                    for (String val : creditCurrencyList) {
//                                                        ProductCreditCurrencyId creditCurrencyId = new ProductCreditCurrencyId();
//
//                                                        creditCurrencyId.setDebitProductType(inputBean.getDebitProductType());
//                                                        creditCurrencyId.setCreditProductType(inputBean.getCreditProductType());
//                                                        creditCurrencyId.setCreditCurrencyCode(val);
//
//                                                        ProductCreditCurrency productCreditCurrency = new ProductCreditCurrency();
//
//                                                        productCreditCurrency.setId(creditCurrencyId);
//                                                        productCreditCurrency.setStatus(CommonVarList.STATUS_ACTIVE);
//
//                                                        productCreditCurrency.setCreatedtime(timestampDate);
//                                                        productCreditCurrency.setLastupdatedtime(timestampDate);
//                                                        productCreditCurrency.setMaker(pentask.getCreateduser());
//                                                        productCreditCurrency.setChecker(audit.getLastupdateduser());
//                                                        session.save(productCreditCurrency);
//
//                                                    }

                                            u.setMaker(pentask.getCreateduser());
                                            u.setChecker(audit.getLastupdateduser());
                                            u.setLastupdatedtime(timestampDate);

                                            session.update(u);

                                        }
//                                                productmatrixid = productmatrixid + "(" + inputBean.getDebitProductType().trim() + "," + inputBean.getCreditProductType() + "),";
                                        succesrec++;
//                                            } else {
//                                                message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                                break;
//                                            }
//                                        } else {
//                                            message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                            break;
//                                        }

                                    } else {
                                        message = message + "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                                        break;
                                    }
                                } else {
                                    getline = true;
//                            content.nextLine();
                                }
                            }
                        }
//                        if (!productmatrixid.isEmpty() && productmatrixid.length() > 0) {
//                            productmatrixid = productmatrixid.substring(0, productmatrixid.length() - 1);
//                        }
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on product matrix (Records count :" + succesrec + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
                    ProductMatrixId id = new ProductMatrixId();

                    id.setDebitProductType(penArray[0]);
                    id.setCreditProductType(penArray[1]);

                    ProductMatrix u = (ProductMatrix) session.get(ProductMatrix.class, id);
                    if (u != null) {
                        // multiple credit currency selection
                        String sql2 = "from ProductMatrixCurrency b where b.id.debitProductType=:debitProductType and b.id.creditProductType=:creditProductType ";
                        Query query2 = session.createQuery(sql2).setString("debitProductType", penArray[0]).setString("creditProductType", penArray[1]);
                        List<ProductMatrixCurrency> productMatrixCurrencyList = query2.list();
                        for (ProductMatrixCurrency productMatrixCurrency : productMatrixCurrencyList) {
                            session.delete(productMatrixCurrency);
                        }
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on product matrix (  debit product type: " + penArray[0] + ", credit product type: " + penArray[1] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectProductMatrix(ProductMatrixInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                String[] pKeyArray = null;
                String debitProductType = "";
                String creditProductType = "";

                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                if (u.getPKey() != null && !u.getPKey().isEmpty()) {
                    pKeyArray = u.getPKey().split("\\|");
                    try {
                        if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                            ProductMatrixId id = new ProductMatrixId();

                            id.setDebitProductType(pKeyArray[0]);
                            id.setCreditProductType(pKeyArray[1]);

                            ProductMatrix proMat = (ProductMatrix) session.get(ProductMatrix.class, id);

                            if (u != null) {
                                //------------------------audit old value start-------------------------------
                                StringBuilder stringBuilderOld = new StringBuilder();
                                stringBuilderOld.append(pKeyArray[0])
                                        .append("|").append(pKeyArray[1])
                                        .append("|").append(proMat.getStatus().getStatuscode())
                                        .append("|");
                                //------------------------audit old value end-------------------------------
                                String sql2 = "from ProductMatrixCurrency b where b.id.debitProductType=:debitProductType and b.id.creditProductType=:creditProductType and b.id.txnType=:txnType ";

                                //----------------------------------- multiple debit currency selection-----------------------------------------------//
                                Query query2 = session.createQuery(sql2).setString("debitProductType", pKeyArray[0]).setString("creditProductType", pKeyArray[1]).setString("txnType", CommonVarList.DEBIT_TXN_TYPE);
                                List<ProductMatrixCurrency> dbList = query2.list();
                                String stringSeparator = "";
                                for (ProductMatrixCurrency pt : dbList) {

                                    //------------------------audit old value start-------------------------------
                                    stringBuilderOld.append(stringSeparator).append(pt.getId().getCurrencyCode());
                                    stringSeparator = ",";
                                    //------------------------audit old value end-------------------------------
                                }
                                //------------------------audit old value start-------------------------------
                                stringBuilderOld.append("|");
                                //------------------------audit old value end-------------------------------
                                //-----------------------------------multiple credit currency selection----------------------------------------------//
                                Query query3 = session.createQuery(sql2).setString("debitProductType", pKeyArray[0]).setString("creditProductType", pKeyArray[1]).setString("txnType", CommonVarList.CREDIT_TXN_TYPE);

                                List<ProductMatrixCurrency> dbList2 = query3.list();

                                stringSeparator = "";
                                for (ProductMatrixCurrency pt : dbList2) {

                                    //------------------------audit old value start-------------------------------
                                    stringBuilderOld.append(stringSeparator).append(pt.getId().getCurrencyCode());
                                    stringSeparator = ",";
                                    //------------------------audit old value end-------------------------------
                                }
                                audit.setOldvalue(stringBuilderOld.toString());
                            }
                        }
                        debitProductType = pKeyArray[0];
                        creditProductType = pKeyArray[1];
                        audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on  product matrix (  debit product type: " + debitProductType + ", credit product type: " + creditProductType + ") inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                    } catch (Exception e) {
                        audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on  product matrix inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                    }
                } else {
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on  product matrix inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String uploadProductMatrix(ProductMatrixInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        FileInputStream fileInputStream = null;

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            Pendingtask pendingtask = new Pendingtask();

//                    pendingtask.setPKey(inputBean.getDebitProductType().trim() + "|" + inputBean.getCreditProductType().trim());
//                    pendingtask.setFields(audit.getNewvalue());
            Task task = new Task();
            task.setTaskcode(TaskVarList.UPLOAD_TASK);
            pendingtask.setTask(task);

            Status st = new Status();
            st.setStatuscode(CommonVarList.STATUS_PENDING);
            pendingtask.setStatus(st);

            Page page = (Page) session.get(Page.class, PageVarList.PRODUCT_MATRIX_MGT_PAGE);
            pendingtask.setPage(page);

            pendingtask.setInputterbranch(sysUser.getBranch());

            try {
                if (inputBean.getConXL().length() != 0) {
                    File csvFile = inputBean.getConXL();
                    byte[] bCsvFile = new byte[(int) csvFile.length()];
                    try {
                        fileInputStream = new FileInputStream(csvFile);
                        fileInputStream.read(bCsvFile);
                        fileInputStream.close();
                        Blob blob = new javax.sql.rowset.serial.SerialBlob(bCsvFile);
                        pendingtask.setInputfile(blob);
                    } catch (Exception ex) {

                    }
                }
            } catch (NullPointerException ex) {

            } finally {
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            }

            pendingtask.setCreatedtime(sysDate);
            pendingtask.setLastupdatedtime(sysDate);
            pendingtask.setCreateduser(audit.getLastupdateduser());

            audit.setCreatetime(sysDate);
            audit.setLastupdatedtime(sysDate);
            audit.setLastupdateduser(audit.getLastupdateduser());

            session.save(audit);
            session.save(pendingtask);
            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String validateInputsForCSV(ProductMatrixInputBean inputBean) {

        String message = "";
        if (inputBean.getDebitProductType() == null || inputBean.getDebitProductType().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_DEBIT_PRODUCT_TYPE;
        } else if (inputBean.getCreditProductType() == null || inputBean.getCreditProductType().trim().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_CREDIT_PRODUCT_TYPE;
//        } else if (inputBean.getDebitCurrency() == null || inputBean.getDebitCurrency().trim().isEmpty()) {
//            message = MessageVarList.PRODUCT_MATRIX_EMPTY_DEBIT_CURRENCY;
        } else if (inputBean.getCurrentDebitCurrencyBox() == null || inputBean.getCurrentDebitCurrencyBox().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_DEBIT_CURRENCY;
        } else if (inputBean.getCurrentCreditCurrencyBox() == null || inputBean.getCurrentCreditCurrencyBox().isEmpty()) {
            message = MessageVarList.PRODUCT_MATRIX_EMPTY_CREDIT_CURRENCY;
        }

        return message;
    }

    public String pendProductMatrixCsvDownloade(ProductMatrixInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getInputfile() != null) {
                    Blob blob = u.getInputfile();
                    int blobLength = (int) blob.length();
                    byte[] blobAsBytes = blob.getBytes(1, blobLength);
                    inputBean.setFileInputStream(u.getInputfile().getBinaryStream());
                    inputBean.setFileLength(blobAsBytes.length);

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    session.save(audit);
                    txn.commit();
                } else {
                    message = "File not found";
                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String validateUpload(ProductMatrixInputBean inputBean, Session session) throws Exception {
        String message = "";

        if ((ProductType) session.get(ProductType.class, inputBean.getDebitProductType().trim()) == null) {
            message = MessageVarList.PRODUCT_MATRIX_INVALID_DEBIT_PRODUCT_TYPE;
        } else if ((ProductType) session.get(ProductType.class, inputBean.getCreditProductType().trim()) == null) {
            message = MessageVarList.PRODUCT_MATRIX_INVALID_CREDIT_PRODUCT_TYPE;
//        } else if ((ProductCurrency) session.get(ProductCurrency.class, inputBean.getDebitCurrency().trim()) == null) {
//            message = MessageVarList.PRODUCT_MATRIX_INVALID_DEBIT_CURRENCY;
        } else {

            for (String debitCurrency : inputBean.getCurrentDebitCurrencyBox()) {
                if ((ProductCurrency) session.get(ProductCurrency.class, debitCurrency.trim()) == null) {
                    message = MessageVarList.PRODUCT_MATRIX_INVALID_DEBIT_CURRENCY;
                    break;
                }
            }
            if (message.isEmpty()) {
                for (String creditCurrency : inputBean.getCurrentCreditCurrencyBox()) {
                    if ((ProductCurrency) session.get(ProductCurrency.class, creditCurrency.trim()) == null) {
                        message = MessageVarList.PRODUCT_MATRIX_INVALID_CREDIT_CURRENCY;
                        break;
                    }
                }
            }
        }

        return message;
    }

    public List<String> getuniqueList(List<String> duplicateList) {
        List<String> uniqueList = new ArrayList<String>();
        for (String value : duplicateList) {
            if (!uniqueList.contains(value)) {
                uniqueList.add(value);
            }
        }
        System.out.println("-------" + uniqueList.toString());
        return uniqueList;
    }

    public StringBuffer makeCSVReport(ProductMatrixInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            List<String> creditCurrencyList = new ArrayList<String>();
            List<String> debitCurrencyList = new ArrayList<String>();
            String orderby = " order by u.createdtime desc ";
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from ProductMatrix u where " + where + orderby;
            Query querySearch = session.createQuery(sqlSearch);

            Iterator it = querySearch.iterate();
            content = new StringBuffer();

            //write column headers to csv file
            content.append("Debit Product Type");
            content.append(',');
            content.append("Credit Product Type");
            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Debit Currency");
            content.append(',');
            content.append("Credit Currency");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Checker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');

            while (it.hasNext()) {

                ProductMatrix productMatrix = (ProductMatrix) it.next();

                try {
                    content.append(productMatrix.getProductTypeByDebitProductType().getProductName().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productMatrix.getProductTypeByCreditProductType().getProductName().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productMatrix.getStatus().getDescription());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }

                //----------------------------------get credit debit mutiple currency-------------------------------
                String currencyListSql = "select mc.id.currencyCode from ProductMatrixCurrency as mc where mc.id.debitProductType=:debitProductType and mc.id.creditProductType=:creditProductType and mc.id.txnType=:txnType";

                Query creditCurrencyquery = session.createQuery(currencyListSql).setString("debitProductType", productMatrix.getId().getDebitProductType()).setString("creditProductType", productMatrix.getId().getCreditProductType()).setString("txnType", CommonVarList.CREDIT_TXN_TYPE);
                Query debitCurrencyquery = session.createQuery(currencyListSql).setString("debitProductType", productMatrix.getId().getDebitProductType()).setString("creditProductType", productMatrix.getId().getCreditProductType()).setString("txnType", CommonVarList.DEBIT_TXN_TYPE);
                try {
                    creditCurrencyList = creditCurrencyquery.list();
                    debitCurrencyList = debitCurrencyquery.list();

                    StringBuilder stringBuilderCreditCurrencyList = new StringBuilder();
                    StringBuilder stringBuilderDebitCurrencyList = new StringBuilder();

                    String stringSeparator = "";
                    for (String creditCurrency : creditCurrencyList) {
                        stringBuilderCreditCurrencyList.append(stringSeparator).append(creditCurrency);
                        stringSeparator = "/";
                    }

                    stringSeparator = "";
                    for (String debitCurrency : debitCurrencyList) {
                        stringBuilderDebitCurrencyList.append(stringSeparator).append(debitCurrency);
                        stringSeparator = "/";
                    }

                    if (stringBuilderDebitCurrencyList != null && !stringBuilderDebitCurrencyList.toString().equals("")) {
                        content.append(stringBuilderDebitCurrencyList.toString());
                        content.append(',');
                    } else {
                        content.append("--");
                        content.append(',');
                    }

                    if (stringBuilderCreditCurrencyList != null && !stringBuilderCreditCurrencyList.toString().equals("")) {
                        content.append(stringBuilderCreditCurrencyList.toString());
                        content.append(',');
                    } else {
                        content.append("--");
                        content.append(',');
                    }

                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                    content.append("--");
                    content.append(',');
                }

                //---------------------------end------------------------------------
                try {
                    content.append(productMatrix.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productMatrix.getChecker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productMatrix.getCreatedtime().toString().substring(0, 19));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productMatrix.getLastupdatedtime().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    content.append("--");
                }
                content.append('\n');
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    public List<String> getProductCurrencyListByTxnType(String txnType, ProductMatrixInputBean inputBean) throws Exception {

        String debitProductType = inputBean.getDebitProductType();
        String creditProductType = inputBean.getCreditProductType();
        List<String> currentList = new ArrayList<String>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql1 = "select c.currencyCode from ProductCurrency as c where c.status.statuscode=:status and c.currencyCode in (select mc.id.currencyCode from ProductMatrixCurrency as mc where mc.id.debitProductType=:debitProductType and mc.id.creditProductType=:creditProductType and mc.id.txnType=:txnType)";

            Query query1 = session.createQuery(sql1).setString("status", CommonVarList.STATUS_ACTIVE).setString("debitProductType", debitProductType).setString("creditProductType", creditProductType).setString("txnType", txnType);

            currentList = query1.list();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return currentList;
    }
}
