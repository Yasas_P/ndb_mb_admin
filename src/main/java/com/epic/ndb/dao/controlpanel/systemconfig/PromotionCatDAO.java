/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.PromotionCatBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PromotionCatInpuBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PromotionCatPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.PromotionsCategories;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author yasas_p
 */
public class PromotionCatDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<PromotionCatBean> getSearchList(PromotionCatInpuBean inputBean, int max, int first, String orderBy) throws Exception {
        List<PromotionCatBean> dataList = new ArrayList<PromotionCatBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = " order by TT.LASTUPDATEDTIME desc ";
            }

            String where = this.makeWhereClause(inputBean);

            BigDecimal count = new BigDecimal(0);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(TT.CATCODE) from PROMOTIONS_CATEGORIES TT where " + where;

            Query queryCount = session.createSQLQuery(sqlCount);

            List countList = queryCount.list();
            count = (BigDecimal) countList.get(0);

            if (count.longValue() > 0) {

                String sqlSearch = " SELECT * from ( select TT.CATCODE , TT.CATNAME, ST.DESCRIPTION AS STATUSDESC , TT.MAKER , TT.CHECKER , TT.CREATEDTIME , TT.LASTUPDATEDTIME, "
                        + " row_number() over (" + orderBy + ") as r "
                        + " from PROMOTIONS_CATEGORIES TT,STATUS ST "
                        + " where TT.STATUS=ST.STATUSCODE  AND " + where + ") where r > " + first + " and r<= " + max;

                System.out.println("sqlSearch : "+sqlSearch);
                
                List<Object[]> chequeList = (List<Object[]>) session.createSQLQuery(sqlSearch).list();

                for (Object[] ttBean : chequeList) {

                    PromotionCatBean promotionCatBean = new PromotionCatBean();

                    try {
                        promotionCatBean.setCatcode(ttBean[0].toString());
                    } catch (NullPointerException npe) {
                        promotionCatBean.setCatcode("--");
                    }
                    try {
                        promotionCatBean.setCatname(ttBean[1].toString());
                    } catch (NullPointerException npe) {
                        promotionCatBean.setCatname("--");
                    }
                    try {
                        promotionCatBean.setStatus(ttBean[2].toString());
                    } catch (NullPointerException npe) {
                        promotionCatBean.setStatus("--");
                    }
                    try {
                        promotionCatBean.setMaker(ttBean[3].toString());
                    } catch (NullPointerException npe) {
                        promotionCatBean.setMaker("--");
                    }
                    try {
                        promotionCatBean.setChecker(ttBean[4].toString());
                    } catch (NullPointerException npe) {
                        promotionCatBean.setChecker("--");
                    }
                    try {
                        promotionCatBean.setCreatedtime(ttBean[5].toString().substring(0, 19));
                    } catch (Exception npe) {
                        promotionCatBean.setCreatedtime("--");
                    }
                    try {
                        promotionCatBean.setLastupdatedtime(ttBean[6].toString().substring(0, 19));
                    } catch (Exception npe) {
                        promotionCatBean.setLastupdatedtime("--");
                    }

                    promotionCatBean.setFullCount(count.longValue());

                    dataList.add(promotionCatBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(PromotionCatInpuBean inputBean) throws Exception {

        String where = "1=1";

        if ((inputBean.getS_catcode() == null || inputBean.getS_catcode().isEmpty())
                && (inputBean.getS_description() == null || inputBean.getS_description().isEmpty())
                && (inputBean.getS_status() == null || inputBean.getS_status().isEmpty())) {

        } else {

            if (inputBean.getS_catcode() != null && !inputBean.getS_catcode().isEmpty()) {
                where += " and lower(TT.CATCODE )like lower('%" + inputBean.getS_catcode()+ "%')";           
            }
            if (inputBean.getS_description() != null && !inputBean.getS_description().isEmpty()) {
                where += " and lower(TT.CATNAME) like lower('%" + inputBean.getS_description() + "%')";
            }
            if (inputBean.getS_status() != null && !inputBean.getS_status().isEmpty()) {
                where += " and TT.STATUS='" + inputBean.getS_status() + "'";
            }

        }
        return where;
    }

    public String insertPromotionCategory(PromotionCatInpuBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((PromotionsCategories) session.get(PromotionsCategories.class, inputBean.getCatcode().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getCatcode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getCatcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PROMOTION_CAT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();

                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<PromotionCatPendBean> getPendingPromotionCategory(PromotionCatInpuBean inputBean, int max, int first, String orderBy) throws Exception {

        List<PromotionCatPendBean> dataList = new ArrayList<PromotionCatPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.PROMOTION_CAT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.PROMOTION_CAT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    PromotionCatPendBean jcode = new PromotionCatPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        jcode.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        jcode.setCatcode(penArray[0]);
                    } catch (NullPointerException npe) {
                        jcode.setCatcode("--");
                    } catch (Exception ex) {
                        jcode.setCatcode("--");
                    }
                    try {
                        jcode.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        jcode.setFields("--");
                    } catch (Exception ex) {
                        jcode.setFields("--");
                    }
                    try {
                        jcode.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    
    public PromotionsCategories findPromotionCategoryById(String catcode) throws Exception {
        PromotionsCategories tt = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from PromotionsCategories as u where u.catcode=:catcode";
            Query query = session.createQuery(sql).setString("catcode", catcode);
            tt = (PromotionsCategories) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return tt;

    }

    public String updatePromotionCategory(PromotionCatInpuBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getCatcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getCatcode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.PROMOTION_CAT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deletePromotionCategory(PromotionCatInpuBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getCatcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                PromotionsCategories u = (PromotionsCategories) session.get(PromotionsCategories.class, inputBean.getCatcode().trim());
                if (u != null) {

                    audit.setNewvalue(u.getCatcode()+ "|" + u.getCatname() + "|" + u.getStatus().getStatuscode());
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getCatcode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.PROMOTION_CAT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmPromotionCategory(PromotionCatInpuBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    PromotionsCategories jcode = new PromotionsCategories();
                    jcode.setCatcode(penArray[0]);
                    jcode.setCatname(penArray[1]);

                    Status st = (Status) session.get(Status.class, penArray[2].trim());
                    jcode.setStatus(st);
                    
                    jcode.setCreatedtime(timestampDate);
                    jcode.setLastupdatedtime(timestampDate);
                    jcode.setMaker(pentask.getCreateduser());
                    jcode.setChecker(audit.getLastupdateduser());

                    audit.setNewvalue(jcode.getCatcode() + "|" + jcode.getCatname() + "|" + jcode.getStatus().getStatuscode() );
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on promotion category (Code:  " + jcode.getCatcode()+ ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    session.save(jcode);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    PromotionsCategories u = (PromotionsCategories) session.get(PromotionsCategories.class, penArray[0].trim());

                    if (u != null) {

                        audit.setOldvalue(u.getCatcode()+ "|" + u.getCatname()+ "|" + u.getStatus().getStatuscode());

                        u.setCatname(penArray[1].trim());

                        Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(st);
                      
                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getCatname()+ "|" + u.getStatus().getStatuscode());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on promotion category (Code: " + penArray[0] + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    PromotionsCategories u = (PromotionsCategories) session.get(PromotionsCategories.class, penArray[0]);
                    if (u != null) {
                        audit.setNewvalue(penArray[0] + "|" + u.getCatname()+ "|" + u.getStatus().getStatuscode());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on promotion category (Code: " + penArray[0] + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectPromotionCategory(PromotionCatInpuBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String code = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    PromotionsCategories cusCategory = (PromotionsCategories) session.get(PromotionsCategories.class, code.trim());

                    if (cusCategory != null) {
                        audit.setOldvalue(cusCategory.getCatcode()+ "|" + cusCategory.getCatname()+ "|" + cusCategory.getStatus().getStatuscode());
                    }
                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on promotion category (Code: " + code + ") inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

}
