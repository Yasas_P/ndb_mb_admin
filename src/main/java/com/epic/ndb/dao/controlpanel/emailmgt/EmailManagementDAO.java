/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.emailmgt;

import com.epic.ndb.bean.controlpanel.emailmgt.EmailManagementBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailManagementInputBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailManagementPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.InboxAttachment;
import com.epic.ndb.util.mapping.InboxAttachmentTemp;
import com.epic.ndb.util.mapping.InboxMessage;
import com.epic.ndb.util.mapping.InboxMessageTemp;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.FilenameUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author prathibha_w
 */
public class EmailManagementDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<EmailManagementBean> getSearchList(EmailManagementInputBean inputBean, int max, int first, String orderBy) throws Exception {
        List<EmailManagementBean> dataList = new ArrayList<EmailManagementBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
//                orderBy = "order by u.adminStatus desc, u.createdDate asc";
                orderBy = "order by u.createdDate desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from InboxMessage as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from InboxMessage as u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    EmailManagementBean mailBean = new EmailManagementBean();
                    InboxMessage inboxmsg = (InboxMessage) it.next();

                    try {
                        mailBean.setId(Long.toString(inboxmsg.getId()));
                    } catch (NullPointerException npe) {
                        mailBean.setId("--");
                    }
                    try {
                        mailBean.setMessage(inboxmsg.getMessage());
                    } catch (NullPointerException npe) {
                        mailBean.setMessage("--");
                    }
                    try {
                        mailBean.setRefId(inboxmsg.getRefId());
                    } catch (NullPointerException npe) {
                        mailBean.setRefId("--");
                    }
                    try {
                        mailBean.setServiceDes(inboxmsg.getInboxServiceCategory().getService());
                    } catch (NullPointerException npe) {
                        mailBean.setServiceDes("--");
                    }
                    try {
                        mailBean.setStatus(inboxmsg.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mailBean.setStatus("--");
                    }
                    try {
                        mailBean.setSubject(inboxmsg.getSubject());
                    } catch (NullPointerException npe) {
                        mailBean.setSubject("--");
                    }
                    try {
                        mailBean.setReadStatus(inboxmsg.getReadStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mailBean.setReadStatus("--");
                    }
                    try {
                        mailBean.setMailStatus(inboxmsg.getMailStatus().getStatuscode());
                    } catch (NullPointerException npe) {
                        mailBean.setMailStatus("--");
                    }
                    try {
                        mailBean.setMailStatusDes(inboxmsg.getMailStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mailBean.setMailStatusDes("--");
                    }
                    try {
                        mailBean.setCreatedDate(inboxmsg.getCreatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        mailBean.setCreatedDate("--");
                    }
                    try {
                        mailBean.setLastUpdatedDate(inboxmsg.getLastUpdatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        mailBean.setLastUpdatedDate("--");
                    }
                    try {
                        mailBean.setUserId(inboxmsg.getUserId());
                    } catch (NullPointerException npe) {
                        mailBean.setUserId("--");
                    }
                    try {
                        mailBean.setUserName(inboxmsg.getSwtMobileUser().getUsername());
                    } catch (NullPointerException npe) {
                        mailBean.setUserName("--");
                    }
                    try {
                        mailBean.setCif(inboxmsg.getSwtMobileUser().getCif());
                    } catch (NullPointerException npe) {
                        mailBean.setCif("--");
                    }
                    try {
                        mailBean.setSubject(inboxmsg.getSubject());
                    } catch (NullPointerException npe) {
                        mailBean.setSubject("--");
                    }
                    try {
                        mailBean.setReplierUsername(inboxmsg.getReplierUsername());
                    } catch (NullPointerException npe) {
                        mailBean.setReplierUsername("--");
                    }
                    try {
                        mailBean.setAdminStatus(inboxmsg.getAdminStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mailBean.setAdminStatus("--");
                    }
//                    try {
//                        InboxAttachment ia = new InboxAttachment();
//                        ia = this.findAttacgedImgWithSession(session,Long.toString(inboxmsg.getId()));
//
//                        if (ia != null) {
//                            mailBean.setAttachmentHave("YES");
//                        } else {
//                            mailBean.setAttachmentHave("NO");
//                        }
//                    } catch (NullPointerException npe) {
//                        mailBean.setAttachmentHave("NO");
//                    }

                    try {
                        mailBean.setMaker(inboxmsg.getMaker().toString());
                    } catch (NullPointerException npe) {
                        mailBean.setMaker("--");
                    }
                    try {
                        mailBean.setChecker(inboxmsg.getChecker().toString());
                    } catch (NullPointerException npe) {
                        mailBean.setChecker("--");
                    }
                    try {
                        mailBean.setChannelType(this.getChannelTypeDesByCode(inboxmsg.getChannelType().toString()));
                    } catch (NullPointerException npe) {
                        mailBean.setChannelType("--");
                    }

                    mailBean.setFullCount(count);

                    dataList.add(mailBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(EmailManagementInputBean inputBean) throws ParseException {
        String where = "1=1";
        if (inputBean.getFdate_s() != null && !inputBean.getFdate_s().isEmpty()) {
            where += " and u.createdDate >= to_date( '" + inputBean.getFdate_s() + "' , 'yy-mm-dd')";

        }

        if (inputBean.getTodate_s() != null && !inputBean.getTodate_s().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getTodate_s());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and u.createdDate <= to_date( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getCif_s() != null && !inputBean.getCif_s().isEmpty()) {
            where += " and lower(u.swtMobileUser.cif) like lower('%" + inputBean.getCif_s().trim() + "%') ";
        }
        if (inputBean.getUserName_s() != null && !inputBean.getUserName_s().isEmpty()) {
            where += " and lower(u.swtMobileUser.username) like lower('%" + inputBean.getUserName_s().trim() + "%')";
        }
        if (inputBean.getStatus_s() != null && !inputBean.getStatus_s().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatus_s() + "'";
        }
        if (inputBean.getAdminStatus_s() != null && !inputBean.getAdminStatus_s().isEmpty()) {
            where += " and u.adminStatus.statuscode = '" + inputBean.getAdminStatus_s() + "'";
        }
        if (inputBean.getMobileStatus_s() != null && !inputBean.getMobileStatus_s().isEmpty()) {
            where += " and u.readStatus.statuscode = '" + inputBean.getMobileStatus_s() + "'";
        }
        if (inputBean.getMailStatus_s() != null && !inputBean.getMailStatus_s().isEmpty()) {
            where += " and u.mailStatus.statuscode = '" + inputBean.getMailStatus_s() + "'";
        }
        if (inputBean.getChannelType_s()!= null && !inputBean.getChannelType_s().isEmpty()) {
            where += " and u.channelType = '" + inputBean.getChannelType_s() + "'";
        }
        return where;
    }

    public List<EmailManagementPendBean> getPendingEmailList(EmailManagementInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<EmailManagementPendBean> dataList = new ArrayList<EmailManagementPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from InboxMessageTemp as u where u.maker!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("currentUser", sysUser.getUsername()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from InboxMessageTemp u where u.maker!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("currentUser", sysUser.getUsername()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    EmailManagementPendBean mailBean = new EmailManagementPendBean();
                    InboxMessageTemp inboxmsg = (InboxMessageTemp) it.next();

                    try {
                        mailBean.setId(Long.toString(inboxmsg.getId()));
                    } catch (NullPointerException npe) {
                        mailBean.setId("--");
                    }
                    try {
                        mailBean.setMessage(inboxmsg.getMessage());
                    } catch (NullPointerException npe) {
                        mailBean.setMessage("--");
                    }
                    try {
                        mailBean.setRefId(inboxmsg.getRefId());
                    } catch (NullPointerException npe) {
                        mailBean.setRefId("--");
                    }
                    try {
                        mailBean.setServiceDes(inboxmsg.getInboxServiceCategory().getService());
                    } catch (NullPointerException npe) {
                        mailBean.setServiceDes("--");
                    }
                    try {
                        mailBean.setStatus(inboxmsg.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mailBean.setStatus("--");
                    }
                    try {
                        mailBean.setSubject(inboxmsg.getSubject());
                    } catch (NullPointerException npe) {
                        mailBean.setSubject("--");
                    }
                    try {
                        mailBean.setReadStatus(inboxmsg.getReadStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mailBean.setReadStatus("--");
                    }
                    try {
                        mailBean.setCreatedDate(inboxmsg.getCreatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        mailBean.setCreatedDate("--");
                    }
                    try {
                        mailBean.setUserId(inboxmsg.getUserId());
                    } catch (NullPointerException npe) {
                        mailBean.setUserId("--");
                    }
                    try {
                        mailBean.setUserName(inboxmsg.getSwtMobileUser().getUsername());
                    } catch (NullPointerException npe) {
                        mailBean.setUserName("--");
                    }
                    try {
                        mailBean.setCif(inboxmsg.getSwtMobileUser().getCif());
                    } catch (NullPointerException npe) {
                        mailBean.setCif("--");
                    }
                    try {
                        mailBean.setSubject(inboxmsg.getSubject());
                    } catch (NullPointerException npe) {
                        mailBean.setSubject("--");
                    }
                    try {
                        mailBean.setReplierUsername(inboxmsg.getReplierUsername());
                    } catch (NullPointerException npe) {
                        mailBean.setReplierUsername("--");
                    }
                    try {
                        mailBean.setAdminStatus(inboxmsg.getAdminStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mailBean.setAdminStatus("--");
                    }
                    try {
                        mailBean.setMessageId(inboxmsg.getMessageId().toString());
                    } catch (NullPointerException npe) {
                        mailBean.setMessageId("--");
                    }
                    try {
                        mailBean.setMailStatus(inboxmsg.getMailStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mailBean.setMailStatus("--");
                    }
                    try {
                        mailBean.setOperation(inboxmsg.getTask().getTaskcode().toString());
                    } catch (NullPointerException npe) {
                        mailBean.setOperation("");
                    }
                    try {
                        mailBean.setOperationDes(inboxmsg.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        mailBean.setOperationDes("--");
                    }
                    try {
                        mailBean.setCreateduser(inboxmsg.getMaker());
                    } catch (NullPointerException npe) {
                        mailBean.setCreateduser("--");
                    }
                    try {
                        InboxAttachmentTemp ia = new InboxAttachmentTemp();
                        ia = this.findAttacgedImgTemp(Long.toString(inboxmsg.getId()));

                        if (ia != null) {
                            mailBean.setAttachmentHave("YES");
                        } else {
                            mailBean.setAttachmentHave("NO");
                        }
                    } catch (NullPointerException npe) {
                        mailBean.setAttachmentHave("NO");
                    }

                    mailBean.setFullCount(count);

                    dataList.add(mailBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public InboxAttachment findAttacgedImg(String id) {
        InboxAttachment IBA = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxAttachment as u where u.inboxMessage.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                IBA = (InboxAttachment) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBA;
    }
    
    public InboxAttachment findAttacgedImgWithSession(Session session,String id) {
        InboxAttachment IBA = null;
        try {
            String sql = "from InboxAttachment as u where u.inboxMessage.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                IBA = (InboxAttachment) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
        }
        return IBA;
    }

    public InboxAttachmentTemp findAttacgedImgTemp(String id) {
        InboxAttachmentTemp IBA = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxAttachmentTemp as u where u.inboxMessageTemp.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                IBA = (InboxAttachmentTemp) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBA;
    }

    public InboxAttachment getfile(String id) {
        InboxAttachment IBA = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxAttachment as u where u.id=:id";
            Query query = session.createQuery(sql).setBigDecimal("id", new BigDecimal(id.trim()));

            if (query.list().size() > 0) {
                IBA = (InboxAttachment) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBA;
    }

    public InboxAttachmentTemp getfileTemp(String id) {
        InboxAttachmentTemp IBA = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxAttachmentTemp as u where u.id=:id";
            Query query = session.createQuery(sql).setBigDecimal("id", new BigDecimal(id.trim()));

            if (query.list().size() > 0) {
                IBA = (InboxAttachmentTemp) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBA;
    }

    public List<InboxAttachment> findAttacgedImgList(String id) {
        List<InboxAttachment> IBA = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxAttachment as u where u.inboxMessage.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                IBA = (List<InboxAttachment>) query.list();
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBA;
    }

    public List<InboxAttachmentTemp> findAttacgedImgTempList(String id) {
        List<InboxAttachmentTemp> IBAT = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxAttachmentTemp as u where u.inboxMessageTemp.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                IBAT = (List<InboxAttachmentTemp>) query.list();
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBAT;
    }

    public InboxMessage findInboxMsg(String id) {
        InboxMessage IBM = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMessage as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                IBM = (InboxMessage) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBM;
    }

    public EmailManagementBean findInboxMsgDetailById(String id) {
        InboxMessage IBM = null;
        List<InboxAttachment> IBA = null;
        EmailManagementBean inboxDetailbean = new EmailManagementBean();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMessage as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                IBM = (InboxMessage) query.list().get(0);
                try {
                    inboxDetailbean.setId(Long.toString(IBM.getId()));
                } catch (NullPointerException npe) {
                    inboxDetailbean.setId("--");
                }
                try {
                    inboxDetailbean.setMessage(IBM.getMessage());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setMessage("--");
                }
                try {
                    inboxDetailbean.setRefId(IBM.getRefId());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setRefId("--");
                }
                try {
                    inboxDetailbean.setServiceDes(IBM.getInboxServiceCategory().getService());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setServiceDes("--");
                }
                try {
                    inboxDetailbean.setStatus(IBM.getStatus().getDescription());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setStatus("--");
                }
                try {
                    inboxDetailbean.setSubject(IBM.getSubject());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setSubject("--");
                }
                try {
                    inboxDetailbean.setReadStatus(IBM.getReadStatus().getDescription());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setReadStatus("--");
                }
                try {
                    inboxDetailbean.setMailStatus(IBM.getMailStatus().getStatuscode());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setMailStatus("");
                }
                try {
                    inboxDetailbean.setMailStatusDes(IBM.getMailStatus().getDescription());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setMailStatusDes("--");
                }
                try {
                    inboxDetailbean.setCreatedDate(IBM.getCreatedDate().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    inboxDetailbean.setCreatedDate("--");
                }
                try {
                    inboxDetailbean.setLastUpdatedDate(IBM.getLastUpdatedDate().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    inboxDetailbean.setLastUpdatedDate("--");
                }
                try {
                    inboxDetailbean.setUserId(IBM.getUserId());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setUserId("--");
                }
                try {
                    inboxDetailbean.setUserName(IBM.getSwtMobileUser().getUsername());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setUserName("--");
                }
                try {
                    inboxDetailbean.setCif(IBM.getSwtMobileUser().getCif());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setCif("--");
                }
                try {
                    inboxDetailbean.setSubject(IBM.getSubject());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setSubject("--");
                }
                try {
                    inboxDetailbean.setReplierUsername(IBM.getReplierUsername());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setReplierUsername("--");
                }
                try {
                    inboxDetailbean.setAdminStatus(IBM.getAdminStatus().getDescription());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setAdminStatus("--");
                }
//                    try {
//                        InboxAttachment ia = new InboxAttachment();
//                        ia = this.findAttacgedImg(Long.toString(IBM.getId()));
//
//                        if (ia != null) {
//                            mailBean.setAttachmentHave("YES");
//                        } else {
//                            mailBean.setAttachmentHave("NO");
//                        }
//                    } catch (NullPointerException npe) {
//                        mailBean.setAttachmentHave("NO");
//                    }

                try {
                    inboxDetailbean.setMaker(IBM.getMaker().toString());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setMaker("--");
                }
                try {
                    inboxDetailbean.setChecker(IBM.getChecker().toString());
                } catch (NullPointerException npe) {
                    inboxDetailbean.setChecker("--");
                }
                try {
                    inboxDetailbean.setChannelType(this.getChannelTypeDesByCode(IBM.getChannelType().toString()));
                } catch (NullPointerException npe) {
                    inboxDetailbean.setChannelType("--");
                }

            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return inboxDetailbean;
    }

    public InboxMessageTemp findInboxMsgTemp(String id) {
        InboxMessageTemp IBMT = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMessageTemp as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                IBMT = (InboxMessageTemp) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBMT;
    }

    public String getUnreadMsgCount() {
        String count = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMessage as u where u.adminStatus.statuscode=:adminStatus";
            Query query = session.createQuery(sql).setString("adminStatus", "6");

            if (query.list().size() > 0) {
                count = query.list().size() + "";
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return count;
    }

    public List<InboxMessage> findInboxMsgListByRefID(String refId) {
        List<InboxMessage> IBM = new ArrayList<InboxMessage>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMessage as u where u.refId=:refId order by u.createdDate asc";
            Query query = session.createQuery(sql).setString("refId", refId);

            if (query.list().size() > 0) {
                IBM = (List<InboxMessage>) query.list();
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBM;
    }

    public InboxMessageTemp findInboxMsgTempByRefID(String refId) {
        InboxMessageTemp IBMT = new InboxMessageTemp();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMessageTemp as u where u.refId=:refId order by u.createdDate asc";
            Query query = session.createQuery(sql).setString("refId", refId);

            if (query.list().size() > 0) {
                IBMT = (InboxMessageTemp) query.list().get(0);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return IBMT;
    }

    public boolean isInprogressInboxMsgExistForRefID(String refId) {
        boolean flag = false;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "select count(u.id) from InboxMessage as u where u.refId=:refId and u.mailStatus.statuscode=:status";
            Query query = session.createQuery(sql).setString("refId", refId).setString("status", CommonVarList.STATUS_INBOX_INPROGRESS);
            long count = ((Number) query.uniqueResult()).longValue();
            if (count > 0) {
                flag = true;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return flag;
    }

    public String updateReadStatus(String id, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            InboxMessage u = (InboxMessage) session.get(InboxMessage.class, Long.parseLong(id));

            if (u != null) {
                if (!u.getAdminStatus().getStatuscode().equals("5")) {
                    String oldV = u.getAdminStatus().getDescription();

                    Status st = (Status) session.get(Status.class, "5");
                    u.setAdminStatus(st);

                    //                u.setReplierUsername(audit.getLastupdateduser());
                    u.setLastUpdatedDate(sysDate);

                    String newV = u.getAdminStatus().getDescription();

                    audit.setOldvalue(oldV);
                    audit.setNewvalue(newV);
                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    session.save(audit);
                    session.update(u);
                }
                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String sendReply(String replyMessage, String id, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            InboxMessage u = (InboxMessage) session.get(InboxMessage.class, Long.parseLong(id));

            if (u != null) {

                Status st = (Status) session.get(Status.class, "6");
                u.setReadStatus(st);

                u.setReplierUsername(audit.getLastupdateduser());
                u.setLastUpdatedDate(sysDate);
                u.setCreatedDate(sysDate);
                String newV = u.getAdminStatus().getDescription();

                audit.setNewvalue(newV);
                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

            }

            InboxMessage reply = new InboxMessage();
            reply.setMessage(replyMessage);
            reply.setRefId(u.getRefId());
            reply.setInboxServiceCategory(u.getInboxServiceCategory());

            Status st = (Status) session.get(Status.class, "15");

            reply.setStatus(st);
            reply.setSubject(u.getSubject());
            reply.setReadStatus(u.getReadStatus());
            reply.setCreatedDate(sysDate);
            reply.setLastUpdatedDate(sysDate);
            reply.setUserId(u.getUserId());
            reply.setReplierUsername(u.getReplierUsername());
            reply.setAdminStatus(u.getAdminStatus());
            reply.setSwtMobileUser(u.getSwtMobileUser());

            session.save(audit);
            session.save(reply);

            for (InboxAttachment inboxAttachment : u.getInboxAttachments()) {

                InboxAttachment newattach = new InboxAttachment();
                newattach.setAttachmentFile(inboxAttachment.getAttachmentFile());
                newattach.setCreatedDate(inboxAttachment.getCreatedDate());
                newattach.setFileFormat(inboxAttachment.getFileFormat());
                newattach.setFileName(inboxAttachment.getFileName());
                newattach.setInboxMessage(reply);
                session.save(newattach);
            }

            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String[] sendReply2(EmailManagementInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        String fileId = "";
        String[] messageList = new String[2];
        FileInputStream fileInputStream = null;

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            InboxMessage u = (InboxMessage) session.get(InboxMessage.class, Long.parseLong(inputBean.getId()));

            if (u != null) {
                   //-------------------------------------update mail status from pending to complete---------------------------
                    List<String> excludeMailStatusList = new ArrayList<String>();
                    excludeMailStatusList.add(CommonVarList.STATUS_INBOX_NOREPLY);
                    excludeMailStatusList.add(CommonVarList.STATUS_INBOX_COMPLETE);

                    String sql1 = "from InboxMessage as u where u.refId=:refId and u.mailStatus.statuscode not in(:excludeMailStatusList) order by u.createdDate asc";
                    Query query1 = session.createQuery(sql1).setString("refId", u.getRefId()).setParameterList("excludeMailStatusList", excludeMailStatusList);
                    List<InboxMessage> inboxMessageList = query1.list();
                    if (!inboxMessageList.isEmpty()) {
                        for (InboxMessage inboxMessage : inboxMessageList) {
                            Status mailStatus = (Status) session.get(Status.class, CommonVarList.STATUS_INBOX_COMPLETE);
                            inboxMessage.setMailStatus(mailStatus);

                            inboxMessage.setMaker(audit.getLastupdateduser());
                            inboxMessage.setChecker(audit.getLastupdateduser());
                            inboxMessage.setLastUpdatedDate(sysDate);

                            session.update(inboxMessage);
                        }
                    }
                    //-----------------------------------------------------------------------------------------------
                    InboxMessage reply = new InboxMessage();
                    reply.setMessage(inputBean.getMessageEmailReply());
                    reply.setRefId(u.getRefId());
                    reply.setInboxServiceCategory(u.getInboxServiceCategory());

                    Status st = (Status) session.get(Status.class, "14");
                    reply.setStatus(st);
                    reply.setSubject(u.getSubject());

                    Status readSt = (Status) session.get(Status.class, "6");
                    reply.setReadStatus(readSt);//reply Mobile Read Status Unread

                    reply.setMaker(audit.getLastupdateduser());
                    reply.setChecker(audit.getLastupdateduser());
                    reply.setCreatedDate(sysDate);
                    reply.setLastUpdatedDate(sysDate);
                    reply.setUserId(u.getUserId());
                    reply.setReplierUsername(audit.getLastupdateduser());
                    reply.setAdminStatus(u.getAdminStatus());
                    reply.setSwtMobileUser(u.getSwtMobileUser());
                    
                    Status mailStatusInProgress = (Status) session.get(Status.class, CommonVarList.STATUS_INBOX_INPROGRESS);
                    reply.setMailStatus(mailStatusInProgress);

                    session.save(reply);
                    
                    fileId = Long.toString(reply.getId());

                    if (inputBean.getFilesUploadFileName().size() > 0) {
                        int i = 0;
                        for (File file : inputBean.getFilesUpload()) {
                            File anyFile = file;
                            byte[] banyFile = new byte[(int) anyFile.length()];

                            try {
                                fileInputStream = new FileInputStream(anyFile);
                                fileInputStream.read(banyFile);

                                InboxAttachment newattach = new InboxAttachment();

                                byte[] fileEncoded = java.util.Base64.getEncoder().encode(banyFile);
                                newattach.setAttachmentFile(new String(fileEncoded));

                                newattach.setCreatedDate(sysDate);
                                newattach.setFileFormat(FilenameUtils.getExtension(inputBean.getFilesUploadFileName().get(i)));
                                newattach.setFileName(inputBean.getFilesUploadFileName().get(i));
                                newattach.setInboxMessage(reply);
                                session.save(newattach);
                            } catch (Exception ex) {
                                throw ex;
                            } finally {
                                if (fileInputStream != null) {
                                    fileInputStream.close();
                                }
                            }
                            i++;
                        }
                    }
                    
                    //------------------------------------audit new value-----------------------------
                    audit.setNewvalue(reply.getId() + "|" + reply.getMessage());
                    
                //for dual auth
//                String sql = "from InboxMessageTemp as u where u.refId=:refId order by u.createdDate asc";
//                Query query = session.createQuery(sql).setString("refId", u.getRefId());
//
//                if (query.list().isEmpty()) {
////                    u.setReplierUsername(audit.getLastupdateduser());
//                    u.setLastUpdatedDate(sysDate);
//
//                    InboxMessageTemp replyTemp = new InboxMessageTemp();
//                    replyTemp.setMessage(inputBean.getMessageEmailReply());
//                    replyTemp.setRefId(u.getRefId());
//                    replyTemp.setInboxServiceCategory(u.getInboxServiceCategory());
//
//                    Status st = (Status) session.get(Status.class, "14");
//                    replyTemp.setStatus(st);
//                    replyTemp.setSubject(u.getSubject());
//
//                    Status readSt = (Status) session.get(Status.class, "6");
//                    replyTemp.setReadStatus(readSt);//reply Mobile Read Status Unread
//
//                    Task task = new Task();
//                    task.setTaskcode(TaskVarList.REPLY_TASK);
//                    replyTemp.setTask(task);
//
//                    replyTemp.setInputterbranch(sysUser.getBranch());
//                    replyTemp.setMaker(audit.getLastupdateduser());
//                    replyTemp.setCreatedDate(sysDate);
//                    replyTemp.setLastUpdatedDate(sysDate);
//                    replyTemp.setUserId(u.getUserId());
//                    replyTemp.setReplierUsername(audit.getLastupdateduser());
//                    replyTemp.setAdminStatus(u.getAdminStatus());
//                    replyTemp.setSwtMobileUser(u.getSwtMobileUser());
//
//                    Status mailStatus = (Status) session.get(Status.class, CommonVarList.STATUS_INBOX_INPROGRESS);
//                    replyTemp.setMailStatus(mailStatus);
//
//                    session.save(replyTemp);
//
//                    fileId = Long.toString(replyTemp.getId());
//
//                    if (inputBean.getFilesUploadFileName().size() > 0) {
//                        int i = 0;
//                        for (File file : inputBean.getFilesUpload()) {
//                            File anyFile = file;
//                            byte[] banyFile = new byte[(int) anyFile.length()];
//
//                            try {
//                                fileInputStream = new FileInputStream(anyFile);
//                                fileInputStream.read(banyFile);
//
//                                InboxAttachmentTemp newattachTemp = new InboxAttachmentTemp();
//
//                                byte[] fileEncoded = java.util.Base64.getEncoder().encode(banyFile);
//                                newattachTemp.setAttachmentFile(new String(fileEncoded));
//
//                                newattachTemp.setCreatedDate(sysDate);
//                                newattachTemp.setFileFormat(FilenameUtils.getExtension(inputBean.getFilesUploadFileName().get(i)));
//                                newattachTemp.setFileName(inputBean.getFilesUploadFileName().get(i));
//                                newattachTemp.setInboxMessageTemp(replyTemp);
//                                session.save(newattachTemp);
//                            } catch (Exception ex) {
//                                System.out.println(ex);
//                            } finally {
//                                if (fileInputStream != null) {
//                                    fileInputStream.close();
//                                }
//                            }
//                            i++;
//                        }
//                    }
//
//                    String newV = replyTemp.getId() + "|" + replyTemp.getMessage();
//                    audit.setNewvalue(newV);
//                    audit.setCreatetime(sysDate);
//                    audit.setLastupdatedtime(sysDate);
//
//                    session.save(audit);
//                } else {
//                    message = "E4";//Pending available
//                }
            } else {
                message = "E5";//record not found
            }
            if (message.isEmpty()) {
                txn.commit();
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            message = "E1";
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            messageList[0] = message;
            messageList[1] = fileId;

        }
        return messageList;
    }

    public String updateMessage(EmailManagementInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            InboxMessage u = (InboxMessage) session.get(InboxMessage.class, Long.parseLong(inputBean.getId()));
            if (u != null) {
                List<String> excludeMailStatusList = new ArrayList<String>();
                excludeMailStatusList.add(CommonVarList.STATUS_INBOX_NOREPLY);
                excludeMailStatusList.add(CommonVarList.STATUS_INBOX_COMPLETE);

                String sql = "from InboxMessage as u where u.refId=:refId and u.mailStatus.statuscode not in(:excludeMailStatusList) order by u.createdDate asc";
                Query query = session.createQuery(sql).setString("refId", u.getRefId()).setParameterList("excludeMailStatusList", excludeMailStatusList);
                List<InboxMessage> inboxMessageList = query.list();
                if (!inboxMessageList.isEmpty()) {
                    for (InboxMessage inboxMessage : inboxMessageList) {
                        Status mailStatus = (Status) session.get(Status.class, inputBean.getMailStatus());
                        inboxMessage.setMailStatus(mailStatus);

                        inboxMessage.setMaker(audit.getLastupdateduser());
                        inboxMessage.setChecker(audit.getLastupdateduser());
                        inboxMessage.setLastUpdatedDate(sysDate);

                        session.update(inboxMessage);
                    }
                    session.save(audit);
                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
                //for dual auth
//                String sql = "from InboxMessageTemp as u where u.refId=:refId order by u.createdDate asc";
//                Query query = session.createQuery(sql).setString("refId", u.getRefId());
//
//                if (query.list().isEmpty()) {
//                    InboxMessageTemp inboxTemp = new InboxMessageTemp();
//                    inboxTemp.setMessage(u.getMessage());
//                    inboxTemp.setRefId(u.getRefId());
//                    inboxTemp.setInboxServiceCategory(u.getInboxServiceCategory());
//                    inboxTemp.setStatus(u.getStatus());
//                    inboxTemp.setSubject(u.getSubject());
//                    inboxTemp.setReadStatus(u.getReadStatus());//reply Mobile Read Status Unread
//                    inboxTemp.setMessageId(String.valueOf(u.getId()));//refere primary key
//                    
//                    Task task = new Task();
//                    task.setTaskcode(TaskVarList.STATUS_UPDATE_TASK);
//                    inboxTemp.setTask(task);
//
//                    inboxTemp.setInputterbranch(sysUser.getBranch());
//                    
//                    Status mailStatus = (Status) session.get(Status.class, inputBean.getMailStatus());
//                    inboxTemp.setMailStatus(mailStatus);
//                    
//                    inboxTemp.setMaker(audit.getLastupdateduser());
//                    inboxTemp.setCreatedDate(sysDate);
//                    inboxTemp.setLastUpdatedDate(sysDate);
//                    inboxTemp.setUserId(u.getUserId());
//                    inboxTemp.setReplierUsername(u.getReplierUsername());
//                    inboxTemp.setAdminStatus(u.getAdminStatus());
//                    inboxTemp.setSwtMobileUser(u.getSwtMobileUser());
//                    
//                    audit.setCreatetime(sysDate);
//                    audit.setLastupdatedtime(sysDate);
//
//                    session.save(inboxTemp);
//                    session.save(audit);
//
//                    txn.commit();
//                }else{
//                    message = "Pending available";
//                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmInboxMsg(EmailManagementInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            InboxMessageTemp pentask = (InboxMessageTemp) session.get(InboxMessageTemp.class, Long.parseLong(inputBean.getId().trim()));

            if (pentask != null) {
                if (pentask.getTask().getTaskcode().equals(TaskVarList.REPLY_TASK)) {

                    //-------------------------------------update mail status from pending to complete---------------------------
                    List<String> excludeMailStatusList = new ArrayList<String>();
                    excludeMailStatusList.add(CommonVarList.STATUS_INBOX_NOREPLY);
                    excludeMailStatusList.add(CommonVarList.STATUS_INBOX_COMPLETE);

                    String sql1 = "from InboxMessage as u where u.refId=:refId and u.mailStatus.statuscode not in(:excludeMailStatusList) order by u.createdDate asc";
                    Query query1 = session.createQuery(sql1).setString("refId", pentask.getRefId()).setParameterList("excludeMailStatusList", excludeMailStatusList);
                    List<InboxMessage> inboxMessageList = query1.list();
                    if (!inboxMessageList.isEmpty()) {
                        for (InboxMessage inboxMessage : inboxMessageList) {
                            Status mailStatus = (Status) session.get(Status.class, CommonVarList.STATUS_INBOX_COMPLETE);
                            inboxMessage.setMailStatus(mailStatus);

                            inboxMessage.setMaker(pentask.getMaker());
                            inboxMessage.setChecker(audit.getLastupdateduser());
                            inboxMessage.setLastUpdatedDate(sysDate);

                            session.update(inboxMessage);
                        }
                    }
                    //-----------------------------------------------------------------------------------------------

                    InboxMessage reply = new InboxMessage();
                    reply.setMessage(pentask.getMessage());
                    reply.setRefId(pentask.getRefId());
                    reply.setInboxServiceCategory(pentask.getInboxServiceCategory());

                    Status st = (Status) session.get(Status.class, "14");
                    reply.setStatus(st);
                    reply.setSubject(pentask.getSubject());

                    Status readSt = (Status) session.get(Status.class, "6");
                    reply.setReadStatus(readSt);//reply Mobile Read Status Unread

                    reply.setMaker(pentask.getMaker());
                    reply.setChecker(audit.getLastupdateduser());
                    reply.setCreatedDate(sysDate);
                    reply.setLastUpdatedDate(sysDate);
                    reply.setUserId(pentask.getUserId());
                    reply.setReplierUsername(pentask.getReplierUsername());
                    reply.setAdminStatus(pentask.getAdminStatus());
                    reply.setSwtMobileUser(pentask.getSwtMobileUser());
                    reply.setMailStatus(pentask.getMailStatus());

                    session.save(reply);

                    String sql = "from InboxAttachmentTemp as u where u.inboxMessageTemp.id=:id";
                    Query query = session.createQuery(sql).setLong("id", pentask.getId());
                    List<InboxAttachmentTemp> attachmentTempList = query.list();
                    for (InboxAttachmentTemp attachmentTemp : attachmentTempList) {
                        InboxAttachment newattach = new InboxAttachment();

                        newattach.setAttachmentFile(attachmentTemp.getAttachmentFile());

                        newattach.setCreatedDate(sysDate);
                        newattach.setFileFormat(attachmentTemp.getFileFormat());
                        newattach.setFileName(attachmentTemp.getFileName());
                        newattach.setInboxMessage(reply);

                        session.save(newattach);
//                        session.delete(attachmentTemp);
                    }
                    //------------------------------------audit new value-----------------------------
                    audit.setNewvalue(reply.getId() + "|" + reply.getMessage());

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on Email management (ID:  " + reply.getId() + ") inputted by " + pentask.getMaker() + " approved " + audit.getDescription());
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.STATUS_UPDATE_TASK)) {

                    List<String> excludeMailStatusList = new ArrayList<String>();
                    excludeMailStatusList.add(CommonVarList.STATUS_INBOX_NOREPLY);
                    excludeMailStatusList.add(CommonVarList.STATUS_INBOX_COMPLETE);

                    String sql = "from InboxMessage as u where u.refId=:refId and u.mailStatus.statuscode not in(:excludeMailStatusList) order by u.createdDate asc";
                    Query query = session.createQuery(sql).setString("refId", pentask.getRefId()).setParameterList("excludeMailStatusList", excludeMailStatusList);
                    List<InboxMessage> inboxMessageList = query.list();
                    if (!inboxMessageList.isEmpty()) {
                        for (InboxMessage inboxMessage : inboxMessageList) {

                            inboxMessage.setMailStatus(pentask.getMailStatus());

                            inboxMessage.setMaker(pentask.getMaker());
                            inboxMessage.setChecker(audit.getLastupdateduser());
                            inboxMessage.setLastUpdatedDate(sysDate);

                            session.update(inboxMessage);
                        }
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }
                if (message.isEmpty()) {
                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.delete(pentask);
                }
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectInboxMsg(EmailManagementInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            InboxMessageTemp u = (InboxMessageTemp) session.get(InboxMessageTemp.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                //------------------------------------audit new value-----------------------------
                audit.setNewvalue(u.getId() + "|" + u.getMessage());

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on Email management (ID : " + u.getId() + ") inputted by " + u.getMaker() + " rejected " + audit.getDescription());
//                String sql = "Delete InboxAttachmentTemp as u where u.inboxMessageTemp.id=:id";
//                Query query = session.createQuery(sql).setLong("id", u.getId());
//                query.executeUpdate();

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
    
    public String getChannelTypeDesByCode(String code) {
        String des = "--";
        if (code.equals("2")) {
            des = "Internet Banking";
        } else if (code.equals("1")) {
            des = "Mobile Banking";
        } else {
            des = code;
        }
        return des;

    }
    public StringBuffer makeCSVReport(EmailManagementInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String orderBy = "order by u.createdDate desc";
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from InboxMessage as u where " + where + orderBy;
            Query querySearch = session.createQuery(sqlSearch);
            Iterator it = querySearch.iterate();
            content = new StringBuffer();
            int count=0;
            //write column headers to csv file
            content.append("Message ID");
            content.append(',');
            content.append("User Name");
            content.append(',');
            content.append("CID");
            content.append(',');
            content.append("Subject");
            content.append(',');
//            content.append("Attachment");
//            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Admin Read Status");
            content.append(',');
            content.append("Custometr Read Status");
            content.append(',');
            content.append("Mail Status");
            content.append(',');
            content.append("Channel Type");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Created Time");
            content.append(',');
            content.append("Last Updated Time");
            content.append(',');
            content.append("Message");

            content.append('\n');

            while (it.hasNext()) {
                count++;
                InboxMessage inboxMessage = (InboxMessage) it.next();
                  
                try {
                    content.append(inboxMessage.getId());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getSwtMobileUser().getUsername());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getSwtMobileUser().getCif());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getSubject().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
//                try {
//                    InboxAttachment ia = new InboxAttachment();
//                    ia = this.findAttacgedImgWithSession(session,Long.toString(inboxMessage.getId()));
//
//                    if (ia != null) {
//                       content.append("YES");
//                    } else {
//                        content.append("NO");
//                    }
//                } catch (NullPointerException npe) {
//                    content.append("NO");
//                }
//                content.append(',');
                try {
                    content.append(inboxMessage.getStatus().getDescription());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getAdminStatus().getDescription());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getReadStatus().getDescription());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getMailStatus().getDescription());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(this.getChannelTypeDesByCode(inboxMessage.getChannelType().toString()));
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getCreatedDate().toString().substring(0, 19));
                    content.append(',');
                } catch (ArrayIndexOutOfBoundsException | NullPointerException aoe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(inboxMessage.getLastUpdatedDate().toString().substring(0, 19));
                    content.append(',');
                } catch (ArrayIndexOutOfBoundsException | NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }try {
                    content.append("\""+Common.DoubleFieldUnderDoublequotation(inboxMessage.getMessage().toString())+"\"");
                    content.append(',');
                } catch (NullPointerException e) {
                    content.append("--");
                    content.append(',');
                }
                
                content.append('\n');
                
            }
            content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFdate_s()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getTodate_s()));
                    content.append('\n');

                    content.append("User Name :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getUserName_s()));
                    content.append('\n');
                    
                    content.append("CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif_s()));
                    content.append('\n');

                    content.append("Status :");
                    if (inputBean.getStatus_s()!= null && !inputBean.getStatus_s().isEmpty()) {
                        Status status = (Status) session.get(Status.class, inputBean.getStatus_s());
                        content.append(Common.replaceEmptyorNullStringToALL(status.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getStatus_s()));
                    }
                    content.append('\n');
                    
                    content.append("Admin Read Status :");
                    if (inputBean.getAdminStatus_s()!= null && !inputBean.getAdminStatus_s().isEmpty()) {
                        Status adminStatus = (Status) session.get(Status.class, inputBean.getAdminStatus_s());
                        content.append(Common.replaceEmptyorNullStringToALL(adminStatus.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getAdminStatus_s()));
                    }
                    content.append('\n');
                    
                    content.append("Customer Read Status :");
                    if (inputBean.getMobileStatus_s()!= null && !inputBean.getMobileStatus_s().isEmpty()) {
                        Status customerStatus = (Status) session.get(Status.class, inputBean.getMobileStatus_s());
                        content.append(Common.replaceEmptyorNullStringToALL(customerStatus.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getMobileStatus_s()));
                    }
                    content.append('\n');
                    
                    content.append("Mail Status :");
                    if (inputBean.getMailStatus_s()!= null && !inputBean.getMailStatus_s().isEmpty()) {
                        Status mailStatus = (Status) session.get(Status.class, inputBean.getMailStatus_s());
                        content.append(Common.replaceEmptyorNullStringToALL(mailStatus.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getMailStatus_s()));
                    }
                    content.append('\n');
                    
                    content.append("Channel Type :");
                    if (inputBean.getChannelType_s()!= null && !inputBean.getChannelType_s().isEmpty()) {
                        String channelTypeDes = getChannelTypeDesByCode(inputBean.getChannelType_s());
                        content.append(Common.replaceEmptyorNullStringToALL(channelTypeDes));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getChannelType_s()));
                    }
                    content.append('\n');
                    
                    content.append('\n');
                    content.append("Summary");
                    content.append('\n');
                    content.append("Total Record Count :");
                    content.append(count);
                    content.append('\n');
                    
                    Date createdTime = CommonDAO.getSystemDate(session);

                    content.append("Report Created Time and Date :");
                    content.append(createdTime.toString().substring(0, 19));
                    content.append('\n');
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
    public StringBuffer makeCSVReportSql(EmailManagementInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String inboxMessageSql="SELECT I.ID ," +
                    "U.USERNAME  ," +
                    "U.CIF ," +
                    "I.SUBJECT ," +
                    "S.DESCRIPTION  status ," +
                    "AD.DESCRIPTION adminStatus , " +
                    "MB.DESCRIPTION mobileStatus ," +
                    "MA.DESCRIPTION mailStatus, " +
                    "I.CHANNEL_TYPE ," +
                    "I.MAKER ," +
                    "I.CREATED_DATE ," +
                    "I.LAST_UPDATED_DATE ," +
                    "I.MESSAGE " +

                    "FROM INBOX_MESSAGE I " +
                    "LEFT OUTER JOIN STATUS S ON S.STATUSCODE = I.STATUS " +
                    "LEFT OUTER JOIN STATUS AD ON AD.STATUSCODE = I.ADMIN_STATUS " +
                    "LEFT OUTER JOIN STATUS MB ON MB.STATUSCODE = to_char(I.READ_STATUS) " +
                    "LEFT OUTER JOIN STATUS MA ON MA.STATUSCODE = I.MAIL_STATUS " +
                    "LEFT OUTER JOIN SWT_MOBILE_USER U ON U.ID = I.USER_ID " + 
                    "WHERE  " ;
            String orderBy = " ORDER BY I.LAST_UPDATED_DATE DESC";
            String where = this.makeWhereClauseSql(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            
            String sqlSearch = inboxMessageSql + where + orderBy;
            Query querySearch = session.createSQLQuery(sqlSearch);
            List<Object[]> objectArrList = (List<Object[]>)querySearch.list();
            content = new StringBuffer();
            int count=0;
            //write column headers to csv file
            content.append("Message ID");
            content.append(',');
            content.append("User Name");
            content.append(',');
            content.append("CID");
            content.append(',');
            content.append("Subject");
            content.append(',');
//            content.append("Attachment");
//            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Admin Read Status");
            content.append(',');
            content.append("Custometr Read Status");
            content.append(',');
            content.append("Mail Status");
            content.append(',');
            content.append("Channel Type");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");
            content.append(',');
            content.append("Message");

            content.append('\n');

            for (Object[] objArr : objectArrList) {
                count++;
                  
                try {
                    content.append(objArr[0]).toString();
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[1].toString());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[2].toString());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[3].toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
//                try {
//                    InboxAttachment ia = new InboxAttachment();
//                    ia = this.findAttacgedImgWithSession(session,Long.toString(inboxMessage.getId()));
//
//                    if (ia != null) {
//                       content.append("YES");
//                    } else {
//                        content.append("NO");
//                    }
//                } catch (NullPointerException npe) {
//                    content.append("NO");
//                }
//                content.append(',');
                try {
                    content.append(objArr[4].toString());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[5].toString());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[6].toString());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[7].toString());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(this.getChannelTypeDesByCode(objArr[8].toString()));
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[9].toString());
                    content.append(',');
                } catch (NullPointerException e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[10].toString().substring(0, 19));
                    content.append(',');
                } catch (ArrayIndexOutOfBoundsException | NullPointerException aoe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[11].toString().substring(0, 19));
                    content.append(',');
                } catch (ArrayIndexOutOfBoundsException | NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }try {
                    content.append("\""+Common.DoubleFieldUnderDoublequotation(objArr[12].toString())+"\"");
                    content.append(',');
                } catch (NullPointerException e) {
                    content.append("--");
                    content.append(',');
                }
                
                content.append('\n');
                
            }
            content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFdate_s()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getTodate_s()));
                    content.append('\n');

                    content.append("User Name :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getUserName_s()));
                    content.append('\n');
                    
                    content.append("CID :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif_s()));
                    content.append('\n');

                    content.append("Status :");
                    if (inputBean.getStatus_s()!= null && !inputBean.getStatus_s().isEmpty()) {
                        Status status = (Status) session.get(Status.class, inputBean.getStatus_s());
                        content.append(Common.replaceEmptyorNullStringToALL(status.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getStatus_s()));
                    }
                    content.append('\n');
                    
                    content.append("Admin Read Status :");
                    if (inputBean.getAdminStatus_s()!= null && !inputBean.getAdminStatus_s().isEmpty()) {
                        Status adminStatus = (Status) session.get(Status.class, inputBean.getAdminStatus_s());
                        content.append(Common.replaceEmptyorNullStringToALL(adminStatus.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getAdminStatus_s()));
                    }
                    content.append('\n');
                    
                    content.append("Customer Read Status :");
                    if (inputBean.getMobileStatus_s()!= null && !inputBean.getMobileStatus_s().isEmpty()) {
                        Status customerStatus = (Status) session.get(Status.class, inputBean.getMobileStatus_s());
                        content.append(Common.replaceEmptyorNullStringToALL(customerStatus.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getMobileStatus_s()));
                    }
                    content.append('\n');
                    
                    content.append("Mail Status :");
                    if (inputBean.getMailStatus_s()!= null && !inputBean.getMailStatus_s().isEmpty()) {
                        Status mailStatus = (Status) session.get(Status.class, inputBean.getMailStatus_s());
                        content.append(Common.replaceEmptyorNullStringToALL(mailStatus.getDescription()));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getMailStatus_s()));
                    }
                    content.append('\n');
                    
                    content.append("Channel Type :");
                    if (inputBean.getChannelType_s()!= null && !inputBean.getChannelType_s().isEmpty()) {
                        String channelTypeDes = getChannelTypeDesByCode(inputBean.getChannelType_s());
                        content.append(Common.replaceEmptyorNullStringToALL(channelTypeDes));
                    } else {
                        content.append(Common.replaceEmptyorNullStringToALL(inputBean.getChannelType_s()));
                    }
                    content.append('\n');
                    
                    content.append('\n');
                    content.append("Summary");
                    content.append('\n');
                    content.append("Total Record Count :");
                    content.append(count);
                    content.append('\n');
                    
                    Date createdTime = CommonDAO.getSystemDate(session);

                    content.append("Report Created Time and Date :");
                    content.append(createdTime.toString().substring(0, 19));
                    content.append('\n');
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
    
     private String makeWhereClauseSql(EmailManagementInputBean inputBean) throws ParseException {
        String where = "1=1";
        if (inputBean.getFdate_s() != null && !inputBean.getFdate_s().isEmpty()) {
            where += " and I.CREATED_DATE >= to_date( '" + inputBean.getFdate_s() + "' , 'yy-mm-dd')";

        }

        if (inputBean.getTodate_s() != null && !inputBean.getTodate_s().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getTodate_s());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and I.CREATED_DATE <= to_date( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getCif_s() != null && !inputBean.getCif_s().isEmpty()) {
            where += " and lower(U.CIF) like lower('%" + inputBean.getCif_s().trim() + "%') ";
        }
        if (inputBean.getUserName_s() != null && !inputBean.getUserName_s().isEmpty()) {
            where += " and lower(U.USERNAME) like lower('%" + inputBean.getUserName_s().trim() + "%')";
        }
        if (inputBean.getStatus_s() != null && !inputBean.getStatus_s().isEmpty()) {
            where += " and I.STATUS = '" + inputBean.getStatus_s() + "'";
        }
        if (inputBean.getAdminStatus_s() != null && !inputBean.getAdminStatus_s().isEmpty()) {
            where += " and I.ADMIN_STATUS = '" + inputBean.getAdminStatus_s() + "'";
        }
        if (inputBean.getMobileStatus_s() != null && !inputBean.getMobileStatus_s().isEmpty()) {
            where += " and I.READ_STATUS = '" + inputBean.getMobileStatus_s() + "'";
        }
        if (inputBean.getMailStatus_s() != null && !inputBean.getMailStatus_s().isEmpty()) {
            where += " and I.MAIL_STATUS = '" + inputBean.getMailStatus_s() + "'";
        }
        if (inputBean.getChannelType_s()!= null && !inputBean.getChannelType_s().isEmpty()) {
            where += " and I.CHANNEL_TYPE = '" + inputBean.getChannelType_s() + "'";
        }
        return where;
    }
}
