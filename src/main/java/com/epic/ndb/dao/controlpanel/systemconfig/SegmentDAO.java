/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.SegmentBean;
import com.epic.ndb.bean.controlpanel.systemconfig.SegmentInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.SegmentPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author jayathissa_d
 */
public class SegmentDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<SegmentBean> getSearchList(SegmentInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<SegmentBean> dataList = new ArrayList<SegmentBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.lastupdatedtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(segmentcode) from SegmentType as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from SegmentType u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    SegmentBean jcode = new SegmentBean();
                    SegmentType jc = (SegmentType) it.next();

                    try {
                        jcode.setSegmentcode(jc.getSegmentcode());
                    } catch (NullPointerException npe) {
                        jcode.setSegmentcode("--");
                    }
                    try {
                        jcode.setDescription(jc.getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setDescription("--");
                    }
                    try {
                        Status u = (Status) session.get(Status.class, jc.getStatus());
                        jcode.setStatus(u.getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setWaveoff(jc.getWaveoff().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setWaveoff("--");
                    }
                    try {
                        jcode.setChargeAmount(jc.getChargeAmount().toString());
                    } catch (Exception npe) {
                        jcode.setChargeAmount("--");
                    }
                    try {
                        jcode.setMaker(jc.getMaker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setMaker("--");
                    }
                    try {
                        jcode.setChecker(jc.getChecker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setChecker("--");
                    }
                    try {
                        jcode.setCreatedtime(jc.getCreatetime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setCreatedtime("--");
                    }
                    try {
                        jcode.setLastupdatedtime(jc.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setLastupdatedtime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(SegmentInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getSegmentcodeSearch() != null && !inputBean.getSegmentcodeSearch().isEmpty()) {
            where += " and lower(u.segmentcode) like lower('%" + inputBean.getSegmentcodeSearch().trim() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status = '" + inputBean.getStatusSearch() + "'";
        }
        return where;
    }

    public List<SegmentPendBean> getPendingSegmentList(SegmentInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<SegmentPendBean> dataList = new ArrayList<SegmentPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.SEGMEMNT_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.SEGMEMNT_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    SegmentPendBean jcode = new SegmentPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        jcode.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        jcode.setSegmentcode(penArray[0]);
                    } catch (NullPointerException npe) {
                        jcode.setSegmentcode("--");
                    } catch (Exception ex) {
                        jcode.setSegmentcode("--");
                    }
                    try {
                        jcode.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        jcode.setFields("--");
                    } catch (Exception ex) {
                        jcode.setFields("--");
                    }
                    try {
                        jcode.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertSegment(SegmentInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((SegmentType) session.get(SegmentType.class, inputBean.getSegmentcode().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getSegmentcode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getSegmentcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.SEGMEMNT_MGT_PAGE);
                    pendingtask.setPage(page);
                    
                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteSegment(SegmentInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getSegmentcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                String sql1 = "select count(*) from TransactionLimit as u where u.segmentType.segmentcode =:segmentcode";
                Query query1 = session.createQuery(sql1).setString("segmentcode", inputBean.getSegmentcode().trim());
                Long count1 = (Long) query1.iterate().next();

                if (count1 != 0) {
                    message = MessageVarList.COMMON_ALREADY_IN_USE;
                } else {
                    String sql2 = "select count(*) from SwtMobileUser as u where u.segmentType.segmentcode =:segmentcode";
                    Query query2 = session.createQuery(sql2).setString("segmentcode", inputBean.getSegmentcode().trim());
                    Long count2 = (Long) query2.iterate().next();

                    if (count2 != 0) {
                        message = MessageVarList.COMMON_ALREADY_IN_USE;
                    } else {
                        SegmentType u = (SegmentType) session.get(SegmentType.class, inputBean.getSegmentcode().trim());
                        if (u != null) {

                            audit.setNewvalue(u.getSegmentcode() + "|" + u.getDescription() + "|" + u.getStatus() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());
                        }

                        Pendingtask pendingtask = new Pendingtask();

                        pendingtask.setPKey(inputBean.getSegmentcode().trim());
                        pendingtask.setFields(audit.getNewvalue());

                        Task task = new Task();
                        task.setTaskcode(TaskVarList.DELETE_TASK);
                        pendingtask.setTask(task);

                        Status st = new Status();
                        st.setStatuscode(CommonVarList.STATUS_PENDING);
                        pendingtask.setStatus(st);

                        Page page = (Page) session.get(Page.class, PageVarList.SEGMEMNT_MGT_PAGE);
                        pendingtask.setPage(page);
                        
                        pendingtask.setInputterbranch(sysUser.getBranch());

                        pendingtask.setCreatedtime(sysDate);
                        pendingtask.setLastupdatedtime(sysDate);
                        pendingtask.setCreateduser(audit.getLastupdateduser());

                        audit.setCreatetime(sysDate);
                        audit.setLastupdatedtime(sysDate);
                        audit.setLastupdateduser(audit.getLastupdateduser());

                        session.save(audit);
                        session.save(pendingtask);
                        txn.commit();
                    }
                }
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public SegmentType findSegmentById(String segmentcode) throws Exception {
        SegmentType branch = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from SegmentType as u where u.segmentcode=:segmentcode";
            Query query = session.createQuery(sql).setString("segmentcode", segmentcode);
            System.out.print(query);
            branch = (SegmentType) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return branch;

    }

    public String updateSegment(SegmentInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getSegmentcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

//                SegmentType u = (SegmentType) session.get(SegmentType.class, inputBean.getSegmentcode().trim());
//                if (u != null) {
//
//                    audit.setOldvalue(u.getSegmentcode() + "|" + u.getDescription() + "|" + u.getStatus() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());
//                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getSegmentcode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.SEGMEMNT_MGT_PAGE);
                pendingtask.setPage(page);
                
                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmSegment(SegmentInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    SegmentType jcode = new SegmentType();
                    jcode.setSegmentcode(penArray[0]);
                    jcode.setDescription(penArray[1]);

                    //Status st = (Status) session.get(Status.class, penArray[2].trim());
                    jcode.setStatus(penArray[2].trim());

                    Status wa = (Status) session.get(Status.class, penArray[3].trim());
                    jcode.setWaveoff(wa);

                    jcode.setChargeAmount(new BigDecimal(penArray[4].trim()));

                    jcode.setCreatetime(sysDate);
                    jcode.setLastupdatedtime(sysDate);
                    jcode.setMaker(pentask.getCreateduser());
                    jcode.setChecker(audit.getLastupdateduser());

                    audit.setNewvalue(jcode.getSegmentcode() + "|" + jcode.getDescription() + "|" + jcode.getStatus() + "|" + jcode.getWaveoff().getStatuscode() + "|" + jcode.getChargeAmount().toString());
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on segment (Segment code:  " + jcode.getSegmentcode()+ ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    session.save(jcode);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    SegmentType u = (SegmentType) session.get(SegmentType.class, penArray[0].trim());

                    if (u != null) {

                        audit.setOldvalue(u.getSegmentcode() + "|" + u.getDescription() + "|" + u.getStatus() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());

                        u.setDescription(penArray[1].trim());

                        // Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(penArray[2].trim());

                        Status wa = (Status) session.get(Status.class, penArray[3].trim());
                        u.setWaveoff(wa);

                        u.setChargeAmount(new BigDecimal(penArray[4].trim()));

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getStatus() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on segment (Segment code: " + penArray[0] + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    SegmentType u = (SegmentType) session.get(SegmentType.class, penArray[0]);
                    if (u != null) {
                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getStatus() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on segment (Segment code: " + penArray[0] + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectSegment(SegmentInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String code = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    SegmentType segmentType = (SegmentType) session.get(SegmentType.class, code.trim());

                    if (segmentType != null) {

                        audit.setOldvalue(segmentType.getSegmentcode() + "|" + segmentType.getDescription() + "|" + segmentType.getStatus() + "|" + segmentType.getWaveoff().getStatuscode() + "|" + segmentType.getChargeAmount().toString());
                    }
                }
//                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on segment (Segment code: " + code + ") inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

}
