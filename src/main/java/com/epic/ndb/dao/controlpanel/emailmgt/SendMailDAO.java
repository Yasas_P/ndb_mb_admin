/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.emailmgt;

import com.epic.ndb.bean.controlpanel.emailmgt.SendMailBean;
import com.epic.ndb.bean.controlpanel.emailmgt.SendMailInputBean;
import com.epic.ndb.bean.controlpanel.emailmgt.SendMailPendBean;
import com.epic.ndb.bean.customermanagement.KeyValueBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.InboxMsgCon;
import com.epic.ndb.util.mapping.InboxMsgConCus;
import com.epic.ndb.util.mapping.InboxMsgConCusId;
import com.epic.ndb.util.mapping.InboxMsgConCusTemp;
import com.epic.ndb.util.mapping.InboxMsgConCusTempId;
import com.epic.ndb.util.mapping.InboxMsgConSeg;
import com.epic.ndb.util.mapping.InboxMsgConSegId;
import com.epic.ndb.util.mapping.InboxMsgConSegTemp;
import com.epic.ndb.util.mapping.InboxMsgConSegTempId;
import com.epic.ndb.util.mapping.InboxMsgConTemp;
import com.epic.ndb.util.mapping.InboxServiceCategory;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.SwtMobileUser;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class SendMailDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<SendMailBean> getSearchList(SendMailInputBean inputBean, int max, int first, String orderBy) throws Exception {
        List<SendMailBean> dataList = new ArrayList<SendMailBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdDate desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from InboxMsgCon as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from InboxMsgCon as u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    SendMailBean sendBean = new SendMailBean();
                    InboxMsgCon inboxmsgCon = (InboxMsgCon) it.next();

                    try {
                        sendBean.setId(Long.toString(inboxmsgCon.getId()));
                    } catch (NullPointerException npe) {
                        sendBean.setId("--");
                    }
                    try {
                        sendBean.setMessage(inboxmsgCon.getMessage());
                    } catch (NullPointerException npe) {
                        sendBean.setMessage("--");
                    }
                    try {
                        sendBean.setServiceDes(inboxmsgCon.getInboxServiceCategory().getService());
                    } catch (NullPointerException npe) {
                        sendBean.setServiceDes("--");
                    }
                    try {
                        sendBean.setSubject(inboxmsgCon.getSubject());
                    } catch (NullPointerException npe) {
                        sendBean.setSubject("--");
                    }
                    try {
                        sendBean.setToUser(this.getRecipientsDesByCode(inboxmsgCon.getToUser()));
                    } catch (NullPointerException npe) {
                        sendBean.setToUser("--");
                    }
                    try {
                        sendBean.setStatus(inboxmsgCon.getStatusByStatus().getDescription());
                    } catch (NullPointerException npe) {
                        sendBean.setStatus("--");
                    }
                    try {
                        sendBean.setProcessingStatus(this.findProssingState(inboxmsgCon.getId(), session));
                    } catch (NullPointerException npe) {
                        sendBean.setProcessingStatus("--");
                    }
                    try {
                        sendBean.setCreatedDate(inboxmsgCon.getCreatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        sendBean.setCreatedDate("--");
                    }
                    try {
                        sendBean.setLastUpdatedDate(inboxmsgCon.getLastUpdatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        sendBean.setLastUpdatedDate("--");
                    }
                    try {
                        sendBean.setMaker(inboxmsgCon.getMaker().toString());
                    } catch (NullPointerException npe) {
                        sendBean.setMaker("--");
                    }
                    try {
                        sendBean.setChecker(inboxmsgCon.getChecker().toString());
                    } catch (NullPointerException npe) {
                        sendBean.setChecker("--");
                    }

                    sendBean.setFullCount(count);

                    dataList.add(sendBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(SendMailInputBean inputBean) throws ParseException {
        String where = "1=1";
        
        if (inputBean.getToUser_s()!= null && !inputBean.getToUser_s().isEmpty()) {
            where += " and u.toUser = '" + inputBean.getToUser_s() + "'";
            if(inputBean.getToUser_s().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT) && inputBean.getSegment_s()!= null && !inputBean.getSegment_s().isEmpty()){
                where += " and u.id in ( select s.id.id from InboxMsgConSeg as s where s.id.segmentcode= '" + inputBean.getSegment_s() + "' )";
            }else if(inputBean.getToUser_s().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL) && inputBean.getCif_s()!= null && !inputBean.getCif_s().isEmpty()){
                where += " and u.id in ( select c.id.id from InboxMsgConCus as c where lower(c.cif) like lower('%" + inputBean.getCif_s() + "%') )";
            }
        }
        if (inputBean.getFdate_s() != null && !inputBean.getFdate_s().isEmpty()) {
            where += " and u.createdDate >= to_date( '" + inputBean.getFdate_s() + "' , 'yy-mm-dd')";

        }

        if (inputBean.getTodate_s() != null && !inputBean.getTodate_s().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getTodate_s());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and u.createdDate <= to_date( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        return where;
    }

    public List<SendMailPendBean> getPendingSendEmailList(SendMailInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<SendMailPendBean> dataList = new ArrayList<SendMailPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from InboxMsgConTemp as u where u.maker!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("currentUser", sysUser.getUsername()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from InboxMsgConTemp u where u.maker!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("currentUser", sysUser.getUsername()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    SendMailPendBean sendBean = new SendMailPendBean();
                    InboxMsgConTemp sendmsg = (InboxMsgConTemp) it.next();

                    try {
                        sendBean.setId(Long.toString(sendmsg.getId()));
                    } catch (NullPointerException npe) {
                        sendBean.setId("--");
                    }
                    try {
                        sendBean.setMessage(sendmsg.getMessage());
                    } catch (NullPointerException npe) {
                        sendBean.setMessage("--");
                    }
                    try {
                        sendBean.setServiceDes(sendmsg.getInboxServiceCategory().getService());
                    } catch (NullPointerException npe) {
                        sendBean.setServiceDes("--");
                    }
                    try {
                        sendBean.setSubject(sendmsg.getSubject());
                    } catch (NullPointerException npe) {
                        sendBean.setSubject("--");
                    }
                    try {
                        sendBean.setToUser(this.getRecipientsDesByCode(sendmsg.getToUser()));
                    } catch (NullPointerException npe) {
                        sendBean.setToUser("--");
                    }
                    try {
                        sendBean.setStatus(sendmsg.getStatusByStatus().getDescription());
                    } catch (NullPointerException npe) {
                        sendBean.setStatus("--");
                    }
                    try {
                        sendBean.setCreatedDate(sendmsg.getCreatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        sendBean.setCreatedDate("--");
                    }
                    try {
                        sendBean.setOperation(sendmsg.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        sendBean.setOperation("--");
                    }
                    try {
                        sendBean.setMaker(sendmsg.getMaker());
                    } catch (NullPointerException npe) {
                        sendBean.setMaker("--");
                    }

                    sendBean.setFullCount(count);

                    dataList.add(sendBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String sendMessage(SendMailInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            StringBuilder newVal = new StringBuilder(inputBean.getServiceId() + "|" + inputBean.getToUser() + inputBean.getMessageEmail() + "|");

            InboxMsgCon send = new InboxMsgCon();
            send.setMessage(inputBean.getMessageEmail());
            InboxServiceCategory serviceCategory = (InboxServiceCategory) session.get(InboxServiceCategory.class, Short.parseShort(inputBean.getServiceId()));
            send.setInboxServiceCategory(serviceCategory);

            Status st = (Status) session.get(Status.class, "14");
            
            send.setStatusByStatus(st);
            send.setSubject(serviceCategory.getService());

            Status readSt = (Status) session.get(Status.class, "6");
            send.setStatusByReadStatus(readSt);//reply Mobile Read Status Unread

            Status adminSt = (Status) session.get(Status.class, "5");
            send.setStatusByAdminStatus(adminSt);//reply Admin Read Status read
            
//            send.setMapId(pentask.getId());
            send.setMapPointer(Common.getUUID());
            send.setSendStatus("0");

            send.setToUser(inputBean.getToUser());
            send.setMaker(audit.getLastupdateduser());
            send.setChecker(audit.getLastupdateduser());
            send.setCreatedDate(sysDate);
            send.setLastUpdatedDate(sysDate);

            session.save(send);
            
            if (inputBean.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT)) {
                for (String segment : inputBean.getCurrentSegmentBox()) {
                    InboxMsgConSegId conSegId = new InboxMsgConSegId();
                    conSegId.setId(send.getId());
                    conSegId.setSegmentcode(segment);

                    InboxMsgConSeg conSeg = new InboxMsgConSeg();
                    conSeg.setId(conSegId);

                    session.save(conSeg);
                    newVal.append(segment + ",");
                }
            } else if (inputBean.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL)) {
                for (KeyValueBean user : inputBean.getCidList()) {
                    InboxMsgConCusId conCusId = new InboxMsgConCusId();
                    conCusId.setId(send.getId());
                    conCusId.setUserId(user.getKey());

                    InboxMsgConCus conCus = new InboxMsgConCus();
                    conCus.setId(conCusId);
                    conCus.setCif(user.getValue());

                    session.save(conCus);
                    newVal.append(user.getValue() + ",");
                }
            }

            audit.setNewvalue(newVal.toString());
            //dual Auth
//            InboxMsgConTemp sendTemp = new InboxMsgConTemp();
//
//            sendTemp.setMessage(inputBean.getMessageEmail());
//
//            Task task = new Task();
//            task.setTaskcode(TaskVarList.SEND_TASK);
//            sendTemp.setTask(task);
//
//            InboxServiceCategory serviceCategory = (InboxServiceCategory) session.get(InboxServiceCategory.class, Short.parseShort(inputBean.getServiceId()));
//            sendTemp.setInboxServiceCategory(serviceCategory);
//
//            sendTemp.setSubject(serviceCategory.getService());
//            sendTemp.setToUser(inputBean.getToUser());
//            sendTemp.setCreatedDate(sysDate);
//            sendTemp.setInputterbranch(sysUser.getBranch());
//            sendTemp.setMaker(audit.getLastupdateduser());
//
//            session.save(sendTemp);
//
//            if (inputBean.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT)) {
//                for (String segment : inputBean.getCurrentSegmentBox()) {
//                    InboxMsgConSegTempId conSegTempId = new InboxMsgConSegTempId();
//                    conSegTempId.setId(sendTemp.getId());
//                    conSegTempId.setSegmentcode(segment);
//
//                    InboxMsgConSegTemp conSegTemp = new InboxMsgConSegTemp();
//                    conSegTemp.setId(conSegTempId);
//
//                    session.save(conSegTemp);
//                    newVal.append(segment + ",");
//                }
//            } else if (inputBean.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL)) {
//                for (KeyValueBean user : inputBean.getCidList()) {
//                    InboxMsgConCusTempId conCusTempId = new InboxMsgConCusTempId();
//                    conCusTempId.setId(sendTemp.getId());
//                    conCusTempId.setUserId(user.getKey());
//
//                    InboxMsgConCusTemp conCusTemp = new InboxMsgConCusTemp();
//                    conCusTemp.setId(conCusTempId);
//                    conCusTemp.setCif(user.getValue());
//
//                    session.save(conCusTemp);
//                    newVal.append(user.getValue() + ",");
//                }
//            }

            audit.setNewvalue(newVal.toString());
            audit.setCreatetime(sysDate);
            audit.setLastupdatedtime(sysDate);
            session.save(audit);
            txn.commit();

            message = this.updateSendStatus(send.getId());
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public Object getUseridCidBean(String cif) {

        Object messageOrBean = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from SwtMobileUser as u where u.cif=:cif";
            Query query = session.createQuery(sql).setString("cif", cif.trim());

            if (query.list().size() > 0) {
                KeyValueBean bean = new KeyValueBean();
                SwtMobileUser user = (SwtMobileUser) query.list().get(0);
                bean.setKey(user.getId());
                bean.setValue(user.getCif());
                messageOrBean = bean;
            } else {
                messageOrBean = "Invalid CID";
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return messageOrBean;
    }

    public String findProssingState(long id, Session session) {
        String prossingStatus = "--";
        try {
            String sql = "select u.status from InboxMsgSentStatus as u where u.id=:id";
            Query query = session.createQuery(sql).setLong("id", id);

            if (query.list().size() > 0) {
                prossingStatus = (String) query.list().get(0);
            } else {
                prossingStatus = "INPROGRESS";
            }

        } catch (Exception e) {
            throw e;
        }
        return prossingStatus;
    }

    public String confirmSendMsg(SendMailInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            InboxMsgConTemp pentask = (InboxMsgConTemp) session.get(InboxMsgConTemp.class, Long.parseLong(inputBean.getId().trim()));

            if (pentask != null) {
                if (pentask.getTask().getTaskcode().equals(TaskVarList.SEND_TASK)) {
                    StringBuilder newVal = new StringBuilder(pentask.getInboxServiceCategory().getId() + "|" + pentask.getSubject() + "|" + pentask.getToUser() + pentask.getMessage() + "|");
                    InboxMsgCon send = new InboxMsgCon();
                    send.setMessage(pentask.getMessage());
                    send.setInboxServiceCategory(pentask.getInboxServiceCategory());

                    Status st = (Status) session.get(Status.class, "14");
                    send.setStatusByStatus(st);
                    send.setSubject(pentask.getSubject());

                    Status readSt = (Status) session.get(Status.class, "6");
                    send.setStatusByReadStatus(readSt);//reply Mobile Read Status Unread

                    Status adminSt = (Status) session.get(Status.class, "5");
                    send.setStatusByAdminStatus(adminSt);//reply Admin Read Status read

                    send.setMapId(pentask.getId());
                    
                    send.setToUser(pentask.getToUser());
                    send.setMaker(pentask.getMaker());
                    send.setChecker(audit.getLastupdateduser());
                    send.setCreatedDate(sysDate);
                    send.setLastUpdatedDate(sysDate);

                    session.save(send);

                    if (send.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT)) {
                        String sql = "from InboxMsgConSegTemp as u where u.id.id=:id";
                        Query query = session.createQuery(sql).setLong("id", pentask.getId());
                        List<InboxMsgConSegTemp> conSegTempList = query.list();
                        for (InboxMsgConSegTemp conSegTemp : conSegTempList) {
                            InboxMsgConSegId conSegId = new InboxMsgConSegId();
                            conSegId.setId(send.getId());
                            conSegId.setSegmentcode(conSegTemp.getId().getSegmentcode());

                            newVal.append(conSegTemp.getId().getSegmentcode() + ",");//audit new value

                            InboxMsgConSeg conSeg = new InboxMsgConSeg();
                            conSeg.setId(conSegId);

                            session.save(conSeg);
                            session.delete(conSegTemp);
                        }
                    } else if (send.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL)) {
                        String sql = "from InboxMsgConCusTemp as u where u.id.id=:id";
                        Query query = session.createQuery(sql).setLong("id", pentask.getId());
                        List<InboxMsgConCusTemp> conCusTempList = query.list();
                        for (InboxMsgConCusTemp conCusTemp : conCusTempList) {
                            InboxMsgConCusId conCusId = new InboxMsgConCusId();
                            conCusId.setId(send.getId());
                            conCusId.setUserId(conCusTemp.getId().getUserId());

                            newVal.append(conCusTemp.getId().getUserId() + ",");//audit new value

                            InboxMsgConCus conCus = new InboxMsgConCus();
                            conCus.setId(conCusId);
                            conCus.setCif(conCusTemp.getCif());

                            session.save(conCus);
                            session.delete(conCusTemp);
                        }
                    }
                    audit.setNewvalue(newVal.toString());
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on send messger (ID:  " + send.getId() + ") inputted by " + pentask.getMaker() + " approved " + audit.getDescription());
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectSendMsg(SendMailInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            InboxMsgConTemp u = (InboxMsgConTemp) session.get(InboxMsgConTemp.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                //------------------------------------audit new value-----------------------------
                StringBuilder newVal = new StringBuilder(u.getInboxServiceCategory().getId() + "|" + u.getSubject() + "|" + u.getToUser() + u.getMessage() + "|");
                if (u.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT)) {
                    String sql = "Delete InboxMsgConSegTemp as u where u.id.id=:id";
                    Query query = session.createQuery(sql).setLong("id", u.getId());
                    query.executeUpdate();
                } else if (u.getToUser().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL)) {
                    String sql = "Delete InboxMsgConCusTemp as u where u.id.id=:id";
                    Query query = session.createQuery(sql).setLong("id", u.getId());
                    query.executeUpdate();
                }
                audit.setNewvalue(newVal.toString());
                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on send message inputted by " + u.getMaker() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public SendMailBean getSendMsgByID(String id) {
        InboxMsgCon msgcon = null;
        Session session = null;
        SendMailBean sendBean = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMsgCon as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                msgcon = (InboxMsgCon) query.list().get(0);
                 sendBean = new SendMailBean();
                try {
                    sendBean.setId(Long.toString(msgcon.getId()));
                } catch (NullPointerException npe) {
                    sendBean.setId("--");
                }
                try {
                    sendBean.setMessage(msgcon.getMessage());
                } catch (NullPointerException npe) {
                    sendBean.setMessage("--");
                }
                try {
                    sendBean.setServiceDes(msgcon.getInboxServiceCategory().getService());
                } catch (NullPointerException npe) {
                    sendBean.setServiceDes("--");
                }
                try {
                    sendBean.setSubject(msgcon.getSubject());
                } catch (NullPointerException npe) {
                    sendBean.setSubject("--");
                }
                try {
                    sendBean.setToUserId(msgcon.getToUser());
                    sendBean.setToUser(this.getRecipientsDesByCode(msgcon.getToUser()));
                } catch (NullPointerException npe) {
                    sendBean.setToUser("--");
                }
                try {
                        sendBean.setProcessingStatus(this.findProssingState(msgcon.getId(), session));
                    } catch (NullPointerException npe) {
                        sendBean.setProcessingStatus("--");
                    }
                    try {
                        sendBean.setCreatedDate(msgcon.getCreatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        sendBean.setCreatedDate("--");
                    }
                    try {
                        sendBean.setLastUpdatedDate(msgcon.getLastUpdatedDate().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        sendBean.setLastUpdatedDate("--");
                    }
                    try {
                        sendBean.setMaker(msgcon.getMaker().toString());
                    } catch (NullPointerException npe) {
                        sendBean.setMaker("--");
                    }
                    try {
                        sendBean.setChecker(msgcon.getChecker().toString());
                    } catch (NullPointerException npe) {
                        sendBean.setChecker("--");
                    }
                    try {
                        sendBean.setMapPointer(msgcon.getMapPointer().toString());
                    } catch (NullPointerException npe) {
                        sendBean.setMapPointer("--");
                    }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return sendBean;
    }
    
    public SendMailPendBean getSendMsgTempByID(String id) {
        InboxMsgConTemp msgcontemp = null;
        Session session = null;
        SendMailPendBean sendBean = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMsgConTemp as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            if (query.list().size() > 0) {
                msgcontemp = (InboxMsgConTemp) query.list().get(0);
                 sendBean = new SendMailPendBean();
                try {
                    sendBean.setId(Long.toString(msgcontemp.getId()));
                } catch (NullPointerException npe) {
                    sendBean.setId("--");
                }
                try {
                    sendBean.setMessage(msgcontemp.getMessage());
                } catch (NullPointerException npe) {
                    sendBean.setMessage("--");
                }
                try {
                    sendBean.setServiceDes(msgcontemp.getInboxServiceCategory().getService());
                } catch (NullPointerException npe) {
                    sendBean.setServiceDes("--");
                }
                try {
                    sendBean.setSubject(msgcontemp.getSubject());
                } catch (NullPointerException npe) {
                    sendBean.setSubject("--");
                }
                try {
                    sendBean.setToUserId(msgcontemp.getToUser());
                    sendBean.setToUser(this.getRecipientsDesByCode(msgcontemp.getToUser()));
                } catch (NullPointerException npe) {
                    sendBean.setToUser("--");
                }
                try {
                    sendBean.setStatus(msgcontemp.getStatusByStatus().getDescription());
                } catch (NullPointerException npe) {
                    sendBean.setStatus("--");
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return sendBean;
    }
    
    public List<KeyValueBean> getSegmentListById(String id) {
        Session session = null;
         List<KeyValueBean> dataList = new ArrayList<KeyValueBean>();
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMsgConSeg as u where u.id.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            List<InboxMsgConSeg> conSegList = query.list();
            for (InboxMsgConSeg conseg : conSegList) {
                KeyValueBean bean =new KeyValueBean();
                bean.setKey(conseg.getSegmentType().getSegmentcode());
                bean.setValue(conseg.getSegmentType().getDescription());
                dataList.add(bean);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }
    
    public String getSegmentsById(String id) {
        Session session = null;
        String segments=""; 
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "select u.segmentType.description from InboxMsgConSeg as u where u.id.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            List<String> conSegList = query.list();
            if(conSegList.size()>0){
                segments=conSegList.toString();
                //segments=segments.substring(1, segments.length()-1);
            }else{
                segments = "--";
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return segments;
    }
    
    public List<KeyValueBean> getTempSegmentListById(String id) {
        Session session = null;
         List<KeyValueBean> dataList = new ArrayList<KeyValueBean>();
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMsgConSegTemp as u where u.id.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            List<InboxMsgConSegTemp> conSegTempList = query.list();
            for (InboxMsgConSegTemp consegTemp : conSegTempList) {
                KeyValueBean bean =new KeyValueBean();
                bean.setKey(consegTemp.getSegmentType().getSegmentcode());
                bean.setValue(consegTemp.getSegmentType().getDescription());
                dataList.add(bean);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }
    
    public List<KeyValueBean> getCusListById(String id) {
        Session session = null;
         List<KeyValueBean> dataList = new ArrayList<KeyValueBean>();
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMsgConCus as u where u.id.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            List<InboxMsgConCus> conCusList = query.list();
            for (InboxMsgConCus conCus : conCusList) {
                KeyValueBean bean =new KeyValueBean();
                bean.setKey(conCus.getId().getUserId());
                bean.setValue(conCus.getCif());
                dataList.add(bean);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }
    
    public String getCustomersById(String id) {
        Session session = null;
        String customers="";
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "select u.cif from InboxMsgConCus as u where u.id.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            List<String> conCusList = query.list();
            if(conCusList.size()>0){
                customers=conCusList.toString();
                //customers=customers.substring(1, customers.length()-1);
            }else{
                customers = "--";
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return customers;
    }
    public String getCifByMapPointer(String mapPointer) {
        Session session = null;
        String customers="";
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "SELECT U.CIF FROM SWT_MOBILE_USER U WHERE U.ID IN (SELECT I.USER_ID FROM INBOX_MESSAGE I WHERE I.MAP_POINTER = :MAP_POINTER)";
            Query query = session.createSQLQuery(sql).setString("MAP_POINTER", mapPointer.trim());

            List<String> conCusList = query.list();
            if(conCusList.size()>0){
                customers=conCusList.toString();
                //customers=customers.substring(1, customers.length()-1);
            }else{
                customers = "--";
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return customers;
    }
    
    public List<KeyValueBean> getTempCusListById(String id) {
        Session session = null;
         List<KeyValueBean> dataList = new ArrayList<KeyValueBean>();
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxMsgConCusTemp as u where u.id.id=:id";
            Query query = session.createQuery(sql).setString("id", id.trim());

            List<InboxMsgConCusTemp> conCusTempList = query.list();
            for (InboxMsgConCusTemp conCusTemp : conCusTempList) {
                KeyValueBean bean =new KeyValueBean();
                bean.setKey(conCusTemp.getId().getUserId());
                bean.setValue(conCusTemp.getCif());
                dataList.add(bean);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }
    
    public boolean isCustomerExistForSegmentList(List<String> segmentList) {
        boolean flag = false;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "select count(u.id) from SwtMobileUser as u where u.segmentType.segmentcode in (:segmentlist) ";
            Query query = session.createQuery(sql).setParameterList("segmentlist",segmentList);
            long count = ((Number) query.uniqueResult()).longValue();
            if(count>0){
                flag=true;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return flag;
    }

    public String getRecipientsDesByCode(String code) {
        String des = "--";
        if (code.equals(CommonVarList.SEND_MESSAGE_TO_USER_ALL)) {
            des = "All";
        } else if (code.equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL)) {
            des = "Individual customers";
        } else if (code.equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT)) {
            des = "Segments";
        }
        return des;

    }
    
    public StringBuffer makeCSVReportSql(SendMailInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String inboxMessageSql="SELECT I.ID ," +
                    "S.SERVICE  ," +
                    "I.TO_USER ," +
                    "P.DESCRIPTION  proState ," +
                    "I.MAKER ," +
                    "I.CREATED_DATE ," +
                    "I.LAST_UPDATED_DATE ," +
                    "I.MESSAGE " +

                    "FROM INBOX_MSG_CON I " +
                    "LEFT OUTER JOIN STATUS P ON P.STATUSCODE = I.PROCESSING_STATUS " +
                    "LEFT OUTER JOIN INBOX_SERVICE_CATEGORY S ON S.ID = I.SERVICE_ID " +
                    "WHERE  " ;
            String orderBy = " ORDER BY I.LAST_UPDATED_DATE DESC";
            String where = this.makeWhereClauseSql(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            
            String sqlSearch = inboxMessageSql + where + orderBy;
            Query querySearch = session.createSQLQuery(sqlSearch);
            System.out.println("----------------"+sqlSearch);
            List<Object[]> objectArrList = (List<Object[]>)querySearch.list();
            content = new StringBuffer();
            int count=0;
            //write column headers to csv file
            content.append("Message ID");
            content.append(',');
            content.append("Subject");
            content.append(',');
            content.append("Recipients");
            content.append(',');
            content.append("Processing Status");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");
            content.append(',');
            content.append("Message");

            content.append('\n');

            for (Object[] objArr : objectArrList) {
                count++;
                  
                try {
                    content.append(objArr[0].toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[1].toString());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(getRecipientsDesByCode(objArr[2].toString()));
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(this.findProssingState(Long.parseLong(objArr[0].toString()), session));
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[4].toString());
                    content.append(',');
                } catch (Exception e) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[5].toString().substring(0, 19));
                    content.append(',');
                } catch (StringIndexOutOfBoundsException | NullPointerException aoe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(objArr[6].toString().substring(0, 19));
                    content.append(',');
                } catch (StringIndexOutOfBoundsException | NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }try {
                    content.append("\""+Common.DoubleFieldUnderDoublequotation(objArr[7].toString())+"\"");
                    content.append(',');
                } catch (NullPointerException e) {
                    content.append("--");
                    content.append(',');
                }
                
                content.append('\n');
                
            }
            content.append('\n');
                    //write column top to csv file
                    content.append("From Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getFdate_s()));
                    content.append('\n');

                    content.append("To Date :");
                    content.append(Common.replaceEmptyorNullStringToALL(inputBean.getTodate_s()));
                    content.append('\n');

                    content.append("Recipients :");
                    if(inputBean.getToUser_s()!=null && !inputBean.getToUser_s().isEmpty()){
                        content.append(Common.replaceEmptyorNullStringToALL(getRecipientsDesByCode(inputBean.getToUser_s())));
                        content.append('\n');
                        
                        if(inputBean.getToUser_s().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT) && inputBean.getSegment_s()!= null && !inputBean.getSegment_s().isEmpty()){
                            content.append("Segment :");
                            SegmentType segmentType = (SegmentType) session.get(SegmentType.class, inputBean.getSegment_s());
                            content.append(Common.replaceEmptyorNullStringToALL(segmentType.getDescription()));
                            content.append('\n');
                        }else if(inputBean.getToUser_s().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL) && inputBean.getCif_s()!= null && !inputBean.getCif_s().isEmpty()){
                            content.append("CID :");
                            content.append(Common.replaceEmptyorNullStringToALL(inputBean.getCif_s()));
                            content.append('\n');
                        }
                    }else{
                        content.append("-ALL-");
                        content.append('\n');
                    }
                    
                    
                    content.append('\n');
                    
                    content.append('\n');
                    content.append("Summary");
                    content.append('\n');
                    content.append("Total Record Count :");
                    content.append(count);
                    content.append('\n');
                    
                    Date createdTime = CommonDAO.getSystemDate(session);

                    content.append("Report Created Time and Date :");
                    content.append(createdTime.toString().substring(0, 19));
                    content.append('\n');
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
    
     private String makeWhereClauseSql(SendMailInputBean inputBean) throws ParseException {
        String where = "1=1";
        if (inputBean.getFdate_s() != null && !inputBean.getFdate_s().isEmpty()) {
            where += " and I.CREATED_DATE >= to_date( '" + inputBean.getFdate_s() + "' , 'yy-mm-dd')";

        }

        if (inputBean.getTodate_s() != null && !inputBean.getTodate_s().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date d = sdf.parse(inputBean.getTodate_s());
            int da = d.getDate() + 1;
            d.setDate(da);
            String sqlDate = sdf.format(d);
            where += " and I.CREATED_DATE <= to_date( '" + sqlDate + "' , 'yy-mm-dd')";
        }
        if (inputBean.getToUser_s()!= null && !inputBean.getToUser_s().isEmpty()) {
            where += " and I.TO_USER = '" + inputBean.getToUser_s() + "'";
            if(inputBean.getToUser_s().equals(CommonVarList.SEND_MESSAGE_TO_USER_SEGMENT) && inputBean.getSegment_s()!= null && !inputBean.getSegment_s().isEmpty()){
                where += " and I.ID in ( SELECT SI.ID FROM INBOX_MSG_CON_SEG SI WHERE SI.SEGMENTCODE= '" + inputBean.getSegment_s() + "' )";
            }else if(inputBean.getToUser_s().equals(CommonVarList.SEND_MESSAGE_TO_USER_INDIVIDUAL) && inputBean.getCif_s()!= null && !inputBean.getCif_s().isEmpty()){
                where += " and I.ID in ( SELECT IC.ID FROM INBOX_MSG_CON_CUS IC WHERE lower(IC.CIF) like lower('%" + inputBean.getCif_s() + "%') )";
            }
        }
        return where;
    }
     
      public String updateSendStatus(long id) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();

            InboxMsgCon u = (InboxMsgCon) session.get(InboxMsgCon.class, id);

            if (u != null) {
                u.setSendStatus("1");
                session.update(u);
                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
            
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
}
