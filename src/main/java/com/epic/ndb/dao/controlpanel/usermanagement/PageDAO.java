/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.usermanagement.PageBean;
import com.epic.ndb.bean.controlpanel.usermanagement.PageInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.PagePendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author chalitha
 */
public class PageDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public String insertPage(PageInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((Page) session.get(Page.class, inputBean.getPageCode().trim()) == null) {
                txn = session.beginTransaction();

                Page Page = new Page();
                Page.setPagecode(inputBean.getPageCode().trim());
                Page.setDescription(inputBean.getDescription().trim());
                Page.setSortkey(new Byte(inputBean.getSortKey().trim()));
                Page.setUrl(inputBean.getUrl().trim());

                Status st = (Status) session.get(Status.class, inputBean.getStatus().trim());
                Page.setStatus(st);

                Page.setCreatetime(sysDate);
                Page.setLastupdatedtime(sysDate);
                Page.setLastupdateduser(audit.getLastupdateduser());

                String newValue = Page.getPagecode()
                        + "|" + Page.getDescription()
                        + "|" + Page.getUrl()
                        + "|" + Page.getSortkey()
                        + "|" + Page.getStatus().getDescription();

                audit.setNewvalue(newValue);
                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.save(Page);

                txn.commit();
                //start newly changed
            } else {

                long count = 0;

                String sqlCount = "select count(pagecode) from Page as u where u.status=:statuscode AND u.pagecode=:pagecode ";
                Query queryCount = session.createQuery(sqlCount).setString("statuscode", CommonVarList.STATUS_DELETE)
                        .setString("pagecode", inputBean.getPageCode().trim());

                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();

                if (count > 0) {
                    message = "$" + inputBean.getPageCode().trim();
                } else {
                    message = MessageVarList.COMMON_ALREADY_EXISTS;
                }
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    // This method only for activate the existing (DEL) records
    public String activatePage(PageInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();

            Date sysDate = CommonDAO.getSystemDate(session);

            Page u = (Page) session.get(Page.class, inputBean.getPageCode().trim());
            if (u != null) {

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                u.setDescription(inputBean.getDescription().trim());
                u.setSortkey(new Byte(inputBean.getSortKey().trim()));
                u.setUrl(inputBean.getUrl().trim());

                //Change status to 'Activate'
                Status status = new Status();
                status.setStatuscode(CommonVarList.STATUS_ACTIVE);
//                u.setStatus(status);

//                u.setUser(audit.getUser());
//                u.setLastupdatedtime(sysDate);
                session.save(audit);
                session.update(u);

                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String updatePage(PageInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getPageCode().trim())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getPageCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.PAGE_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deletePage(PageInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Page u = (Page) session.get(Page.class, inputBean.getPageCode().trim());
            if (u != null) {

                long count = 0;

                String sqlCount = "select count(pagecode) from Sectionpage as u where u.page.pagecode=:pagecode ";
                Query queryCount = session.createQuery(sqlCount).setString("pagecode", inputBean.getPageCode().trim());

                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();

                if (count > 0) {
                    message = MessageVarList.COMMON_NOT_DELETE;
                } else {
                    String sqlCount2 = "select count(pagecode) from Pagetask as u where u.page.pagecode=:pagecode ";
                    Query queryCount2 = session.createQuery(sqlCount2).setString("pagecode", inputBean.getPageCode().trim());

                    Iterator itCount2 = queryCount2.iterate();
                    count = (Long) itCount2.next();
                    if (count > 0) {
                        message = MessageVarList.COMMON_NOT_DELETE;
                    } else {
                        audit.setCreatetime(sysDate);
                        audit.setLastupdatedtime(sysDate);

                        session.save(audit);
                        session.delete(u);
                        txn.commit();
                    }
                }

//                audit.setCreatedtime(sysDate);
//                audit.setLastupdatedtime(sysDate);
//                
//                //Change status to 'Delete'
//                Status status = new Status();
//                status.setStatuscode(CommonVarList.STATUS_DELETE);
//                u.setStatus(status);
//                
//                u.setUser(audit.getUser());
//                u.setLastupdatedtime(sysDate);
//                
//                session.save(audit);
//                session.update(u);                
////                session.delete(u);
//
//                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public Page findPageById(String pageCode) throws Exception {
        Page Page = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Page as u where u.pagecode=:pagecode";
            Query query = session.createQuery(sql).setString("pagecode", pageCode);
            Page = (Page) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return Page;

    }

    public List<PageBean> getSearchList(PageInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<PageBean> dataList = new ArrayList<PageBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(pagecode) from Page as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Page u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    PageBean sdblPage = new PageBean();
                    Page Page = (Page) it.next();

                    try {
                        sdblPage.setPageCode(Page.getPagecode());
                    } catch (NullPointerException npe) {
                        sdblPage.setPageCode("--");
                    }
                    try {
                        sdblPage.setDescription(Page.getDescription());
                    } catch (NullPointerException npe) {
                        sdblPage.setDescription("--");
                    }
                    try {
                        sdblPage.setStatus(Page.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        sdblPage.setStatus("--");
                    }
                    try {
                        sdblPage.setSortKey(Page.getSortkey().toString());
                    } catch (NullPointerException npe) {
                        sdblPage.setSortKey("--");
                    }
                    try {
                        sdblPage.setUrl(Page.getUrl().toString());
                    } catch (NullPointerException npe) {
                        sdblPage.setUrl("--");
                    }
                    try {
                        if (Page.getCreatetime().toString() != null && !Page.getCreatetime().toString().isEmpty()) {
                            sdblPage.setCreatetime(Page.getCreatetime().toString().substring(0, 19));
                        } else {
                            sdblPage.setCreatetime("--");
                        }
                    } catch (NullPointerException npe) {
                        sdblPage.setCreatetime("--");
                    }

                    sdblPage.setFullCount(count);

                    dataList.add(sdblPage);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(PageInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getPageCodeSearch() != null && !inputBean.getPageCodeSearch().isEmpty()) {
            where += " and lower(u.pagecode) like lower('%" + inputBean.getPageCodeSearch() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch() + "%')";
        }
        if (inputBean.getUrlSearch() != null && !inputBean.getUrlSearch().isEmpty()) {
            where += " and lower(u.url) like lower('%" + inputBean.getUrlSearch() + "%')";
        }
        if (inputBean.getSortKeySearch() != null && !inputBean.getSortKeySearch().isEmpty()) {
            where += " and u.sortkey = '" + inputBean.getSortKeySearch() + "'";
        }
        if (inputBean.getStatussearch() != null && !inputBean.getStatussearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatussearch() + "'";
        }
        return where;
    }

    public Page findPageById22(String pageCode) throws Exception {
        Page task = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Page as u where u.pagecode=:pagecode";
            Query query = session.createQuery(sql).setString("pagecode", pageCode);
            task = (Page) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return task;

    }

    public List<PagePendBean> getPendingPageList(PageInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<PagePendBean> dataList = new ArrayList<PagePendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.PAGE_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.PAGE_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    PagePendBean pendPageBean = new PagePendBean();
                    Pendingtask pendpage = (Pendingtask) it.next();

                    try {
                        pendPageBean.setId(Long.toString(pendpage.getId()));
                    } catch (NullPointerException npe) {
                        pendPageBean.setId("--");
                    }
                    try {
                        pendPageBean.setOperation(pendpage.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        pendPageBean.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pendpage.getFields() != null) {
                        penArray = pendpage.getFields().split("\\|");
                    }

                    try {
                        pendPageBean.setPagecode(penArray[0]);
                    } catch (NullPointerException npe) {
                        pendPageBean.setPagecode("--");
                    } catch (Exception ex) {
                        pendPageBean.setPagecode("--");
                    }
                    try {
                        pendPageBean.setFields(pendpage.getFields());
                    } catch (NullPointerException npe) {
                        pendPageBean.setFields("--");
                    } catch (Exception ex) {
                        pendPageBean.setFields("--");
                    }
                    try {
                        pendPageBean.setStatus(pendpage.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        pendPageBean.setStatus("--");
                    }
                    try {
                        pendPageBean.setCreateduser(pendpage.getCreateduser());
                    } catch (NullPointerException npe) {
                        pendPageBean.setCreateduser("--");
                    } catch (Exception ex) {
                        pendPageBean.setCreateduser("--");
                    }

                    pendPageBean.setFullCount(count);

                    dataList.add(pendPageBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String confirmPage(PageInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Page u = (Page) session.get(Page.class, penArray[0]);

                    if (u != null) {

                        audit.setOldvalue(u.getPagecode() + "|" + 
                                u.getDescription() + "|" + 
                                u.getSortkey() + "|" + 
                                u.getUrl() + "|" + 
                                u.getStatus().getDescription());

                        u.setDescription(penArray[1]);
                        u.setSortkey(new Byte(penArray[2]));
                        u.setUrl(penArray[3]);
                        Status st = (Status) session.get(Status.class, penArray[4].trim());
                        u.setStatus(st);

                        u.setLastupdateduser(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getSortkey() + "|" + u.getUrl() + "|" + u.getStatus().getDescription());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on page ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectPage(PageInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String sectioncode = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Page page = (Page) session.get(Page.class, fieldsArray[0]);

                    if (page != null) {

                        audit.setOldvalue(page.getPagecode() + "|" + 
                                page.getDescription() + "|" + 
                                page.getSortkey() + "|" + 
                                page.getUrl() + "|" + 
                                page.getStatus().getStatuscode());
                    }
                }
//                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on page ( code : " + sectioncode + ")  inputted by " + u.getCreateduser() + " rejected "  + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

}
