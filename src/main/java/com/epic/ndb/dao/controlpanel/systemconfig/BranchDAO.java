package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.BranchBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BranchInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BranchPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Branch;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.TaskVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author jayathissa_d
 */
public class BranchDAO {

    public List<BranchBean> getSearchList(BranchInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<BranchBean> dataList = new ArrayList<BranchBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.branchcode desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(branchcode) from Branch as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Branch u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    BranchBean branch = new BranchBean();
                    Branch bch = (Branch) it.next();

                    try {
                        branch.setBranchcode(bch.getBranchcode());
                    } catch (NullPointerException npe) {
                        branch.setBranchcode("--");
                    }
                    try {
                        branch.setBranchname(bch.getBranchname());
                    } catch (NullPointerException npe) {
                        branch.setBranchname("--");
                    }
                    try {
                        branch.setStatus(bch.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        branch.setStatus("--");
                    }
                    try {
                        branch.setManagername(bch.getManagername());
                    } catch (NullPointerException npe) {
                        branch.setManagername("--");
                    }
                    try {
                        branch.setMobile(bch.getMobile());
                    } catch (NullPointerException npe) {
                        branch.setMobile("--");
                    }
                    try {
                        branch.setMaker(bch.getMaker().toString());
                    } catch (NullPointerException npe) {
                        branch.setMaker("--");
                    }
                    try {
                        branch.setChecker(bch.getChecker().toString());
                    } catch (NullPointerException npe) {
                        branch.setChecker("--");
                    }
                    try {
                        branch.setCreatetime(bch.getCreatetime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        branch.setCreatetime("--");
                    }
                    try {
                        branch.setLastupdatedtime(bch.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        branch.setLastupdatedtime("--");
                    }

                    branch.setFullCount(count);

                    dataList.add(branch);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
           if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return dataList;
    }

    private String makeWhereClause(BranchInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getBranchCodeSearch() != null && !inputBean.getBranchCodeSearch().isEmpty()) {
            where += " and lower(u.branchcode) like lower('%" + inputBean.getBranchCodeSearch().trim() + "%')";
        }
        if (inputBean.getBranchNameSearch() != null && !inputBean.getBranchNameSearch().isEmpty()) {
            where += " and lower(u.branchname) like lower('%" + inputBean.getBranchNameSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getManagernameSearch() != null && !inputBean.getManagernameSearch().isEmpty()) {
            where += " and lower(u.managername) like lower('%" + inputBean.getManagernameSearch().trim() + "%')";
        }
        if (inputBean.getMobileSearch() != null && !inputBean.getMobileSearch().isEmpty()) {
            where += " and lower(u.mobile) like lower('%" + inputBean.getMobileSearch().trim() + "%')";
        }

        return where;
    }

    public List<BranchPendBean> getPendingBranchList(BranchInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<BranchPendBean> dataList = new ArrayList<BranchPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.BRANCH_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.BRANCH_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    BranchPendBean branch = new BranchPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        branch.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        branch.setId("--");
                    }
                    try {
                        branch.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        branch.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        branch.setBranchcode(penArray[0]);
                    } catch (NullPointerException npe) {
                        branch.setBranchcode("--");
                    } catch (Exception ex) {
                        branch.setBranchcode("--");
                    }
                    try {
                        branch.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        branch.setFields("--");
                    } catch (Exception ex) {
                        branch.setFields("--");
                    }
                    try {
                        branch.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        branch.setStatus("--");
                    }
                    try {
                        branch.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        branch.setCreatetime("--");
                    }
                    try {
                        branch.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        branch.setCreateduser("--");
                    } catch (Exception ex) {
                        branch.setCreateduser("--");
                    }

                    branch.setFullCount(count);

                    dataList.add(branch);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertBranch(BranchInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((Branch) session.get(Branch.class, inputBean.getBranchCode().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getBranchCode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getBranchCode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.BRANCH_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String deleteBranch(BranchInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql1 = "select count(*) from CustomerinfopendTemp as u where u.branch.branchcode =:branchcode";
            Query query1 = session.createQuery(sql1).setString("branchcode", inputBean.getBranchCode().trim());

            Long count1 = (Long) query1.iterate().next();

            if (count1 != 0) {
                message = MessageVarList.COMMON_ALREADY_IN_USE;
            } else {
                String sql2 = "select count(*) from Customerinfo as u where u.branch.branchcode =:branchcode";
                Query query2 = session.createQuery(sql2).setString("branchcode", inputBean.getBranchCode().trim());

                Long count2 = (Long) query2.iterate().next();

                if (count2 != 0) {
                    message = MessageVarList.COMMON_ALREADY_IN_USE;
                } else {
                    String sql3 = "select count(*) from Systemuser as u where u.branch.branchcode =:branchcode";
                    Query query3 = session.createQuery(sql3).setString("branchcode", inputBean.getBranchCode().trim());

                    Long count3 = (Long) query3.iterate().next();

                    if (count3 != 0) {
                        message = MessageVarList.COMMON_ALREADY_IN_USE;
                    } else {
                        String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                        Query query = session.createQuery(sql)
                                .setString("PKey", inputBean.getBranchCode())
                                .setString("pagecode", audit.getPagecode());

                        if (query.list().isEmpty()) {

                            Branch u = (Branch) session.get(Branch.class, inputBean.getBranchCode().trim());
                            if (u != null) {
                                audit.setNewvalue(u.getBranchcode() + "|" + u.getBranchname() + "|" + u.getStatus().getDescription() + "|" + u.getManagername() + "|" + u.getMobile());
                            }

                            Pendingtask pendingtask = new Pendingtask();

                            pendingtask.setPKey(inputBean.getBranchCode().trim());
                            pendingtask.setFields(audit.getNewvalue());

                            Task task = new Task();
                            task.setTaskcode(TaskVarList.DELETE_TASK);
                            pendingtask.setTask(task);

                            Status st = new Status();
                            st.setStatuscode(CommonVarList.STATUS_PENDING);
                            pendingtask.setStatus(st);

                            Page page = (Page) session.get(Page.class, PageVarList.BRANCH_MGT_PAGE);
                            pendingtask.setPage(page);

                            pendingtask.setCreatedtime(sysDate);
                            pendingtask.setLastupdatedtime(sysDate);
                            pendingtask.setCreateduser(audit.getLastupdateduser());

                            audit.setCreatetime(sysDate);
                            audit.setLastupdatedtime(sysDate);
                            audit.setLastupdateduser(audit.getLastupdateduser());

                            session.save(audit);
                            session.save(pendingtask);
                            txn.commit();
                        } else {
                            message = "pending available";
                        }
                    }
                }
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public Branch findBranchById(String branchCode) throws Exception {
        Branch branch = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Branch as u where u.branchcode=:branchcode";
            Query query = session.createQuery(sql).setString("branchcode", branchCode);
            branch = (Branch) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
           if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return branch;

    }

    public String updateBranch(BranchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getBranchCode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getBranchCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.BRANCH_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String confirmBranch(BranchInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    Branch branch = new Branch();
                    branch.setBranchcode(penArray[0]);
                    branch.setBranchname(penArray[1]);
                    branch.setManagername(penArray[2]);
                    branch.setMobile(penArray[3]);

                    Status st = (Status) session.get(Status.class, penArray[4].trim());
                    branch.setStatus(st);

                    branch.setCreatetime(sysDate);
                    branch.setLastupdatedtime(sysDate);
                    branch.setMaker(pentask.getCreateduser());
                    branch.setChecker(audit.getLastupdateduser());

                    audit.setNewvalue(branch.getBranchcode() + "|" + branch.getBranchname() + "|" + branch.getStatus().getDescription() + "|" + branch.getManagername() + "|" + branch.getMobile());
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on branch :  " + branch.getBranchcode() + audit.getDescription());

                    session.save(branch);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Branch u = (Branch) session.get(Branch.class, penArray[0].trim());

                    if (u != null) {

                        audit.setOldvalue(u.getBranchcode() + "|" + u.getBranchname() + "|" + u.getStatus().getDescription() + "|" + u.getManagername() + "|" + u.getMobile());

                        u.setBranchname(penArray[1].trim());
                        Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(st);
                        u.setManagername(penArray[3].trim());
                        u.setMobile(penArray[4].trim());

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getBranchname() + "|" + u.getStatus().getDescription() + "|" + u.getManagername() + "|" + u.getMobile());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on branch : " + penArray[0] + audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
                    
                    String sql1 = "select count(*) from CustomerinfopendTemp as u where u.branch.branchcode =:branchcode";
                    Query query1 = session.createQuery(sql1).setString("branchcode", penArray[0].trim());

                    Long count1 = (Long) query1.iterate().next();

                    if (count1 != 0) {
                        message = MessageVarList.COMMON_ALREADY_IN_USE;
                    } else {
                        String sql2 = "select count(*) from Customerinfo as u where u.branch.branchcode =:branchcode";
                        Query query2 = session.createQuery(sql2).setString("branchcode", penArray[0].trim());

                        Long count2 = (Long) query2.iterate().next();

                        if (count2 != 0) {
                            message = MessageVarList.COMMON_ALREADY_IN_USE;
                        } else {
                            String sql3 = "select count(*) from Systemuser as u where u.branch.branchcode =:branchcode";
                            Query query3 = session.createQuery(sql3).setString("branchcode", penArray[0].trim());

                            Long count3 = (Long) query3.iterate().next();

                            if (count3 != 0) {
                                message = MessageVarList.COMMON_ALREADY_IN_USE;
                            } else {
                                Branch u = (Branch) session.get(Branch.class, penArray[0]);
                                if (u != null) {
                                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on branch : " + penArray[0] + audit.getDescription());
                                    session.delete(u);
                                    session.delete(pentask);
                                } else {
                                    message = MessageVarList.COMMON_NOT_EXISTS;
                                }
                            }
                        }
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String rejectBranch(BranchInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                String[] fieldsArray = u.getFields().split("\\|");

                String branch = fieldsArray[0];
//                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on branch : " + branch + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

}
