/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.systemconfig.BranchListBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SystemUserDataBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SystemUserInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.SystemUserPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.MBankLocator;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.mapping.Passwordpolicy;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.Userrole;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author asitha_l
 */
public class SystemUserDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    private String makeWhereClause(SystemUserInputBean inputBean, String username) {
        String where = "u.username!='" + username + "' ";

        if (inputBean.getUsername() != null && !inputBean.getUsername().trim().isEmpty()) {
            where += " and lower(u.username) like lower('%" + inputBean.getUsername().trim() + "%')";
        }
        if (inputBean.getServiceid() != null && !inputBean.getServiceid().trim().isEmpty()) {
            where += " and lower(u.empid) like lower('%" + inputBean.getServiceid().trim() + "%')";
        }
        if (inputBean.getNic() != null && !inputBean.getNic().trim().isEmpty()) {
            where += " and lower(u.nic) like lower('%" + inputBean.getNic().trim() + "%')";
        }
        if (inputBean.getContactno() != null && !inputBean.getContactno().trim().isEmpty()) {
            where += " and lower(u.mobile) like lower('%" + inputBean.getContactno().trim() + "%')";
        }
        if (inputBean.getEmail() != null && !inputBean.getEmail().trim().isEmpty()) {
            where += " and lower(u.email) like lower('%" + inputBean.getEmail().trim() + "%')";
        }
        if (inputBean.getStatus() != null && !inputBean.getStatus().trim().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatus().trim() + "'";
        }
//        if (inputBean.getBranch() != null && !inputBean.getBranch().trim().isEmpty()) {
//            where += " and u.branch.branchcode = '" + inputBean.getBranch().trim() + "'";
//        }
        if (inputBean.getBranch() != null && !inputBean.getBranch().trim().isEmpty()) {
            where += " and u.branch = '" + inputBean.getBranch().trim() + "'";
        }
        if (inputBean.getUserrole() != null && !inputBean.getUserrole().trim().isEmpty()) {
            where += " and u.userrole.userrolecode = '" + inputBean.getUserrole().trim() + "'";
        }
        where += " and u.userrole.userrolecode != '" + "MERCH" + "'";
        where += " and u.userrole.userrolecode != '" + "MERCUS" + "'";

        if (inputBean.getFullname() != null && !inputBean.getFullname().trim().isEmpty()) {
            where += " and lower(u.fullname) like lower('%" + inputBean.getFullname().trim() + "%')";
        }
        return where;
    }

    public List<SystemUserDataBean> getSearchList(SystemUserInputBean inputBean, int max, int first, String orderBy, HttpServletRequest request) throws Exception {
        List<SystemUserDataBean> dataList = new ArrayList<SystemUserDataBean>();
        Session session = null;
        try {
            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createtime desc";
            }
            long count = 0;
            Systemuser sysUser = (Systemuser) request.getSession().getAttribute(SessionVarlist.SYSTEMUSER);
            String where = this.makeWhereClause(inputBean, sysUser.getUsername());
//            System.err.println("where "+where);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(username) from Systemuser as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {
                String sqlSearch = "from Systemuser u where " + where + orderBy;

                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();
                while (it.hasNext()) {
                    SystemUserDataBean systemUser = new SystemUserDataBean();
                    Systemuser user = (Systemuser) it.next();

                    try {
                        systemUser.setUsername(user.getUsername().toString());
                    } catch (NullPointerException e) {
                        systemUser.setUsername("--");
                    }
                    try {
                        systemUser.setUsernameUserrole(user.getUsername() + "|" + user.getUserrole().getDescription().toString());
                    } catch (NullPointerException npe) {
                        systemUser.setUsernameUserrole("--");
                    }

                    try {
                        systemUser.setStatus(user.getStatus().getDescription().toString());
                    } catch (NullPointerException e) {
                        systemUser.setStatus("--");
                    }

                    try {
//                        systemUser.setBranch(user.getBranch().getBranchname());
                        String sqlSearch2 = "from MBankLocator u where u.locatorcode=:locatorcode";
                        Query querySearch2 = session.createQuery(sqlSearch2).setString("locatorcode", user.getBranch());
                        MBankLocator mBankLocator = (MBankLocator) querySearch2.list().get(0);
                        systemUser.setBranch(mBankLocator.getName());
                    } catch (Exception e) {
                        systemUser.setBranch("--");
                    }
                    try {
                        systemUser.setUserrole(user.getUserrole().getDescription().toString());
                    } catch (NullPointerException e) {
                        systemUser.setStatus("--");
                    }

                    try {
                        systemUser.setNic(user.getNic().toString());
                    } catch (NullPointerException e) {
                        systemUser.setNic("--");
                    }

                    try {
                        systemUser.setContactNo(user.getMobile().toString());
                    } catch (NullPointerException e) {
                        systemUser.setContactNo("--");
                    }
                    try {
                        systemUser.setFullname(user.getFullname().toString());
                    } catch (NullPointerException e) {
                        systemUser.setFullname("--");
                    }

                    try {
                        systemUser.setEmail(user.getEmail().toString());
                    } catch (NullPointerException e) {
                        systemUser.setEmail("--");
                    }
                    try {
                        systemUser.setServiceId(user.getEmpid().toString());
                    } catch (NullPointerException e) {
                        systemUser.setServiceId("--");
                    }
                    try {
                        if (user.getCreatetime().toString() != null && !user.getCreatetime().toString().isEmpty()) {
                            systemUser.setCreatetime(user.getCreatetime().toString().substring(0, 19));
                        } else {
                            systemUser.setCreatetime("--");
                        }
                    } catch (NullPointerException e) {
                        systemUser.setCreatetime("--");
                    }

                    systemUser.setFullCount(count);
                    dataList.add(systemUser);
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    //start newly changed
    public String activateUser(SystemUserInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();

            Date sysDate = CommonDAO.getSystemDate(session);

//            User u = (User) session.get(User.class, inputBean.getUsername().trim());
            Systemuser u = this.getSystemUserByUserName2(inputBean.getUsername(), session);
            if (u != null) {

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                //modified (3/7/2014)
//                u.setPasswordexpirydate(formatter.parse(inputBean.getExpirydate()));

                Userrole ur = new Userrole();
                ur.setUserrolecode(inputBean.getUserrole());
                u.setUserrole(ur);

//                Systemuser dualAuthUSer = new Systemuser();
//                dualAuthUSer.setUsername(inputBean.getDualauthuser());
                u.setDualauthuserrole(inputBean.getDualauthuser());

                //Change status to 'Activate'
                Status status = new Status();
                status.setStatuscode(CommonVarList.STATUS_ACTIVE);
                u.setStatus(status);

                u.setNoofinvlidattempt("0");

                //if 'Active', change noofinvalidattempt to 0
                if ((inputBean.getStatus()).equals(CommonVarList.STATUS_ACTIVE)) {
//                u.setNoofinvlidattempt(0);
                }

                u.setFullname(inputBean.getFullname());
                u.setEmpid(inputBean.getServiceid());
                u.setAddress(inputBean.getAddress1());
//                u.setAddress2(inputBean.getAddress2());
//                u.setCity(inputBean.getCity());
                u.setMobile(inputBean.getContactno());
                u.setEmail(inputBean.getEmail());

//                Branch branch = new Branch();
//                branch.setBranchcode(inputBean.getBranch());           
                u.setBranch(inputBean.getBranch());

//                u.setNic(inputBean.getNic());
//                u.setDateofbirth(formatter.parse(inputBean.getDateofbirth()));
                u.setLastupdatedtime(sysDate);

                session.save(audit);
                session.update(u);

                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    //end newly changed
    public String insertSystemUser(SystemUserInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        Calendar cal = Calendar.getInstance();
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if (!isSystemUserExist(inputBean.getUsername())) {
                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getUsername())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getUsername().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.SYSTEM_USER);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {

                long count = 0;

                String sqlCount = "select count(username) from Systemuser as u where u.status.statuscode=:statuscode AND u.username=:username";
                Query queryCount = session.createQuery(sqlCount).setString("statuscode", CommonVarList.STATUS_DELETE)
                        .setString("username", inputBean.getUsername().trim());

                Iterator itCount = queryCount.iterate();
                count = (Long) itCount.next();

                if (count > 0) {
                    message = "$" + inputBean.getUsername().trim();
                } else {
                    message = MessageVarList.COMMON_ALREADY_EXISTS;
                }
//                message = MessageVarList.COMMON_ALREADY_EXISTS;
//                txn = session.beginTransaction();
//
//                User user = this.getSystemUserByUserName2(inputBean.getUsername(), session);
//
//                user.setPassword(Common.mpiMd5(inputBean.getPassword()));
//                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                user.setPasswordexpirydate(formatter.parse(inputBean.getExpirydate()));
//
//                Userrole ur = new Userrole();
//                ur.setUserrole(inputBean.getUserrole());
//                user.setUserrole(ur);
//
//                User dualAuthUSer = new User();
//                dualAuthUSer.setUsername(inputBean.getDualauthuser());
//                user.setUserByDualauthuser(dualAuthUSer);
//
//                Status st = new Status();
//                st.setStatuscode(CommonVarList.STATUS_ACTIVE);
//                user.setStatus(st);
//
//                //Status is 'Active', change noofinvalidattempt to 0
//                user.setNoofinvlidattempt(0);
//
//
//                user.setFullname(inputBean.getFullname());
//                user.setServiceid(inputBean.getServiceid());
//                user.setAddress1(inputBean.getAddress1());
//                user.setAddress2(inputBean.getAddress2());
//                user.setCity(inputBean.getCity());
//                user.setContactnumber(inputBean.getContactno());
//                user.setEmailaddress(inputBean.getEmail());
//                user.setNic(inputBean.getNic());
//                user.setDateofbirth(formatter.parse(inputBean.getDateofbirth()));
//
//                audit.setCreatedtime(sysDate);
//                audit.setLastupdatedtime(sysDate);
//
//                User lastUpdatedUser = new User();
//                lastUpdatedUser.setUsername(audit.getUser().getUsername());
//                user.setUserByLastupdateduser(lastUpdatedUser);
//
//                user.setLastupdatedtime(sysDate);
//                session.save(audit);
//                session.update(user);
//
//                txn.commit();

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public Passwordpolicy getPasswordPolicyDetails() throws Exception {
        Passwordpolicy passwordpolicy = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Passwordpolicy.class);
            passwordpolicy = (Passwordpolicy) criteria.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return passwordpolicy;
    }

    private boolean isSystemUserExist(String username) throws Exception {
        List<Systemuser> userList = new ArrayList<Systemuser>();
        Session session = null;
        boolean userCheckStatus = false;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Systemuser.class);
            criteria.add(Restrictions.eq("username", username));
            userList = (List<Systemuser>) criteria.list();

            for (Systemuser user : userList) {
                userCheckStatus = true;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userCheckStatus;
    }

    public Systemuser getSystemUserByUserName(String username) throws Exception {
        Systemuser user = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Systemuser as su where su.username=:username";

            Query query = session.createQuery(sql);
            query.setString("username", username);

            user = (Systemuser) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return user;
    }

    public Systemuser getSystemUserByUserName2(String username, Session session) throws Exception {//call via update method
        Systemuser user = null;
        try {

            String sql = "from Systemuser as su where su.username=:username";

            Query query = session.createQuery(sql);
            query.setString("username", username);

            user = (Systemuser) query.list().get(0);

        } catch (Exception e) {
            throw e;
        }
        return user;
    }

    public void findUsersByUserRole(SystemUserInputBean inputBean, int currUserLevel) throws Exception {
        List<Systemuser> userList = new ArrayList<Systemuser>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Systemuser.class);
            criteria.createAlias("userrole", "ur")
                    .createAlias("ur.userlevel", "ul");
            criteria.add(Restrictions.le("ul.levelid", currUserLevel));
            userList = (List<Systemuser>) criteria.list();

            for (Systemuser user : userList) {
                inputBean.getDualAuthUserMap().put(user.getUsername(), user.getUsername());
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
    }

    public int getCurrUsersUserLevel(String userrole) throws Exception {
        Session session = null;
        Userrole userRole = null;
        int userLevel = 8;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Userrole.class);
            criteria.add(Restrictions.eq("userrolecode", userrole));
            userRole = (Userrole) criteria.list().get(0);
            userLevel = userRole.getUserlevel().getLevelid();

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userLevel;
    }

    public ParameterUserCommon setTooltip(String sectionCode) throws Exception {
        ParameterUserCommon section = null;
        Session session = null;

        try {

            session = HibernateInit.sessionFactory.openSession();

            String sql = "from ParameterUserCommon as u where u.paramcode=:code";
            Query query = session.createQuery(sql).setString("code", sectionCode);
            section = (ParameterUserCommon) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return section;
    }

    public String updateSystemUser(SystemUserInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUsername().trim())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getUsername().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.SYSTEM_USER);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteSystemUser(SystemUserInputBean inputBean, Systemuser currentUser, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();

            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getUsername())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Systemuser u = (Systemuser) session.get(Systemuser.class, inputBean.getUsername().trim());

                if (u != null) {
                    if (u.getUsername().equals(currentUser.getUsername())) {
                        message = u.getUsername() + MessageVarList.SYSUSER_DEL_INVALID;
                    } else {
                        if (u.getAddress() == null) {
                            u.setAddress("");
                        }
                        if (u.getCity() == null) {
                            u.setCity("");
                        }
                        if (u.getEmpid() == null) {
                            u.setEmpid("");
                        }
                        if (u.getEmail() == null) {
                            u.setEmail("");
                        }

                        String newValue = u.getUsername() + "|"
                                + u.getFullname() + "|"
                                + u.getUserrole().getUserrolecode() + "|"
                                + u.getStatus().getStatuscode() + "|"
                                + u.getEmail() + "|"
                                + u.getAddress() + "|"
                                + u.getCity() + "|"
                                + u.getEmpid();

                        audit.setNewvalue(newValue);
//                        String newV = u.getUsername() + "|"
//                                + u.getFullname() + "|"
//                                + u.getNic() + "|"
//                                + u.getUserrole() + "|"
//                                + u.getStatus() + "|"
//                                + u.getMobile() + "|"
//                                + u.getEmail() + "|"
//                                + u.getAddress() + "|"
//                                + u.getCity() + "|"
//                                + u.getEmpid() + "|"
//                                + u.getExpirydate() + "|"
//                                + u.getDateofbirth() + "|"
//                                + u.getBranch();
//
//                        audit.setNewvalue(newV);

                        Pendingtask pendingtask = new Pendingtask();

                        pendingtask.setPKey(inputBean.getUsername().trim());
                        pendingtask.setFields(audit.getNewvalue());

                        Task task = new Task();
                        task.setTaskcode(TaskVarList.DELETE_TASK);
                        pendingtask.setTask(task);

                        Status st = new Status();
                        st.setStatuscode(CommonVarList.STATUS_PENDING);
                        pendingtask.setStatus(st);

                        Page page = (Page) session.get(Page.class, PageVarList.SYSTEM_USER);
                        pendingtask.setPage(page);

                        pendingtask.setInputterbranch(sysUser.getBranch());

                        pendingtask.setCreatedtime(sysDate);
                        pendingtask.setLastupdatedtime(sysDate);
                        pendingtask.setCreateduser(audit.getLastupdateduser());

                        audit.setCreatetime(sysDate);
                        audit.setLastupdatedtime(sysDate);
                        audit.setLastupdateduser(audit.getLastupdateduser());

                        session.save(audit);
                        session.save(pendingtask);
                        txn.commit();
                    }
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String updatePasswordReset(SystemUserInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Systemuser u = (Systemuser) session.get(Systemuser.class, inputBean.getHusername().trim());

            if (u != null) {

                u.setPassword(inputBean.getCrenewpwd());
//                Systemuser lastUpdatedUser = new Systemuser();
//                lastUpdatedUser.setUsername(audit.getLastupdateduser().getUsername());
                u.setLastupdateduser(audit.getLastupdateduser());
                u.setLastupdatedtime(sysDate);

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.update(u);

                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<SystemUserPendBean> getPendingSystemUserList(SystemUserInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<SystemUserPendBean> dataList = new ArrayList<SystemUserPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.SYSTEM_USER).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.SYSTEM_USER).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    SystemUserPendBean user = new SystemUserPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        user.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        user.setId("--");
                    }
                    try {
                        user.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        user.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        user.setUsername(penArray[0]);
                    } catch (NullPointerException npe) {
                        user.setUsername("--");
                    } catch (Exception ex) {
                        user.setUsername("--");
                    }
                    try {
                        user.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        user.setFields("--");
                    } catch (Exception ex) {
                        user.setFields("--");
                    }
                    try {
                        user.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        user.setStatus("--");
                    }
                    try {
                        user.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        user.setCreateduser("--");
                    } catch (Exception ex) {
                        user.setCreateduser("--");
                    }

                    user.setFullCount(count);

                    dataList.add(user);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String confirmSystemUser(SystemUserInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Systemuser user = this.getSystemUserByUserName2(penArray[0], session);

                    if (user != null) {
                        if (user.getAddress() == null) {
                            user.setAddress("");
                        }
                        if (user.getCity() == null) {
                            user.setCity("");
                        }
                        if (user.getEmpid() == null) {
                            user.setEmpid("");
                        }
                        if (user.getEmail() == null) {
                            user.setEmail("");
                        }

                        String oldValue = user.getUsername() + "|"
                                + user.getFullname() + "|"
                                + user.getUserrole().getDescription() + "|"
                                + user.getStatus().getDescription() + "|"
                                + user.getEmail() + "|"
                                + user.getAddress() + "|"
                                + user.getCity() + "|"
                                + user.getEmpid();

                        if (user.getBranch() != null) {
                            oldValue = oldValue + "|" + user.getBranch();//user.getBranch().getBranchname()
                        } else {
                            oldValue += "";
                        };

                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");

                        user.setDualauthuserrole("admin");
                        Status st = (Status) session.get(Status.class, penArray[3]);
                        user.setStatus(st);

                        Userrole urole = (Userrole) session.get(Userrole.class, penArray[2]);
                        user.setUserrole(urole);

                        if ((penArray[3]).equals(CommonVarList.STATUS_ACTIVE)) {
                            user.setNoofinvlidattempt("0");
                            user.setLoggeddate(sysDate);
                        }

                        user.setFullname(penArray[1]);

                        if (!penArray[4].equals("")) {
                            user.setEmail(penArray[4].trim());
                        } else {
                            user.setEmail(null);
                        }
                        if (!penArray[5].equals("")) {
                            user.setAddress(penArray[5]);
                        } else {
                            user.setAddress(null);
                        }
                        if (!penArray[6].equals("")) {
                            user.setCity(penArray[6]);
                        } else {
                            user.setCity(null);
                        }
                        if (!penArray[7].equals("")) {
                            user.setEmpid(penArray[7]);
                        } else {
                            user.setEmpid(null);
                        }

                        user.setBranch(penArray[8]);

                        user.setLastupdateduser(audit.getLastupdateduser());
                        user.setLastupdatedtime(sysDate);

                        String newValue = user.getUsername() + "|"
                                + user.getFullname() + "|"
                                + user.getUserrole().getDescription() + "|"
                                + user.getStatus().getDescription() + "|"
                                + user.getEmail() + "|"
                                + user.getAddress() + "|"
                                + user.getCity() + "|"
                                + user.getEmpid() + "|"
                                + user.getBranch();

                        Systemuser lastUpdatedUser = new Systemuser();
                        lastUpdatedUser.setUsername(audit.getLastupdateduser());
                        user.setLastupdateduser(audit.getLastupdateduser());

                        audit.setOldvalue(oldValue);
                        audit.setNewvalue(newValue);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on system user ( username : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.update(user);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    Systemuser user = new Systemuser();

                    user.setUsername(penArray[0].trim());

                    Userrole urole = (Userrole) session.get(Userrole.class, penArray[2].trim());
                    user.setUserrole(urole);
                    user.setDualauthuserrole("admin");

                    Status st = (Status) session.get(Status.class, penArray[3].trim());
                    user.setStatus(st);

                    user.setBranch(penArray[8].trim());

                    user.setFullname(penArray[1].trim());

                    String address = "";
                    String city = "";
                    String email = "";

                    if (!penArray[7].equals("")) {
                        user.setEmpid(penArray[7]);
                    } else {
                        user.setEmpid(null);
                    }
                    if (!penArray[5].equals("")) {
                        user.setAddress(penArray[5].trim());
                        address = penArray[5].trim();
                    } else {
                        address = "";
                        user.setAddress(null);
                    }
                    if (!penArray[6].equals("")) {
                        user.setCity(penArray[6].trim());
                        city = penArray[6].trim();
                    } else {
                        city = "";
                        user.setCity(null);
                    }
                    if (!penArray[4].equals("")) {
                        user.setEmail(penArray[4].trim());
                        email = penArray[4].trim();
                    } else {
                        user.setEmail(null);
                        email = "";
                    }
                    user.setNoofinvlidattempt("0");//edited

                    String newValue = user.getUsername() + "|"
                            + user.getFullname() + "|"
                            + user.getUserrole().getDescription() + "|"
                            + user.getStatus().getDescription() + "|"
                            + email + "|"
                            + address + "|"
                            + city + "|"
                            + user.getEmpid() + "|"
                            + user.getBranch();

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setNewvalue(newValue);

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on system user ( username : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    user.setLastupdateduser(audit.getLastupdateduser());

                    user.setLastupdatedtime(sysDate);
                    user.setLoggeddate(sysDate);
                    user.setInitialloginstatus("0");
                    user.setCreatetime(sysDate);

                    session.save(user);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    Systemuser u = (Systemuser) session.get(Systemuser.class, penArray[0]);

                    if (u != null) {

                        if (u.getAddress() == null) {
                            u.setAddress("");
                        }
                        if (u.getCity() == null) {
                            u.setCity("");
                        }
                        if (u.getEmpid() == null) {
                            u.setEmpid("");
                        }
                        if (u.getEmail() == null) {
                            u.setEmail("");
                        }

                        String newValue = u.getUsername() + "|"
                                + u.getFullname() + "|"
                                + u.getUserrole().getDescription() + "|"
                                + u.getStatus().getDescription() + "|"
                                + u.getEmail() + "|"
                                + u.getAddress() + "|"
                                + u.getCity() + "|"
                                + u.getEmpid();

                        audit.setNewvalue(newValue);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on system user ( username : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectSystemUser(SystemUserInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }

                String[] fieldsArray = u.getFields().split("\\|");

                String username = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Systemuser user = this.getSystemUserByUserName2(fieldsArray[0], session);

                    if (user != null) {
                        if (user.getAddress() == null) {
                            user.setAddress("");
                        }
                        if (user.getCity() == null) {
                            user.setCity("");
                        }
                        if (user.getEmpid() == null) {
                            user.setEmpid("");
                        }
                        if (user.getEmail() == null) {
                            user.setEmail("");
                        }

                        String oldValue = user.getUsername() + "|"
                                + user.getFullname() + "|"
                                + user.getUserrole().getUserrolecode() + "|"
                                + user.getStatus().getStatuscode() + "|"
                                + user.getEmail() + "|"
                                + user.getAddress() + "|"
                                + user.getCity() + "|"
                                + user.getEmpid();

                        if (user.getBranch() != null) {
                            oldValue = oldValue + "|" + user.getBranch();//user.getBranch().getBranchname()
                        } else {
                            oldValue += "";
                        }
                        audit.setOldvalue(oldValue);
                    }
                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    Systemuser user = this.getSystemUserByUserName2(fieldsArray[0], session);

                    if (user != null) {
                        if (user.getAddress() == null) {
                            user.setAddress("");
                        }
                        if (user.getCity() == null) {
                            user.setCity("");
                        }
                        if (user.getEmpid() == null) {
                            user.setEmpid("");
                        }
                        if (user.getEmail() == null) {
                            user.setEmail("");
                        }

                        String newValue = user.getUsername() + "|"
                                + user.getFullname() + "|"
                                + user.getUserrole().getUserrolecode() + "|"
                                + user.getStatus().getStatuscode() + "|"
                                + user.getEmail() + "|"
                                + user.getAddress() + "|"
                                + user.getCity() + "|"
                                + user.getEmpid();

                        if (user.getBranch() != null) {
                            newValue = newValue + "|" + user.getBranch();//user.getBranch().getBranchname()
                        } else {
                            newValue += "";
                        }
                        audit.setNewvalue(newValue);
                    }
                }

//                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                }
                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on system user ( username : " + username + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<BranchListBean> getBranchBeanList() throws Exception {

        List<BranchListBean> branchList = new ArrayList<BranchListBean>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from MBankLocator as s where lower(s.channelType.channelType) like lower('%BRANCH%') order by s.name asc";
            Query query = session.createQuery(sql);
            Iterator it = query.iterate();
            while (it.hasNext()) {
                BranchListBean branch = new BranchListBean();
                MBankLocator mBankLocator = (MBankLocator) it.next();

                branch.setBranchcode(mBankLocator.getLocatorcode());
                branch.setBranchname(mBankLocator.getName());

                branchList.add(branch);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return branchList;
    }

}
