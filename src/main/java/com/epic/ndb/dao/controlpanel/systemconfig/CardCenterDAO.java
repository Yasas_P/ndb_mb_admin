/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.CardCenterBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CardCenterInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CardCenterPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.OtherBank;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class CardCenterDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<CardCenterBean> getSearchList(CardCenterInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<CardCenterBean> dataList = new ArrayList<CardCenterBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.bankcode asc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(bankcode) from OtherBank as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from OtherBank u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    CardCenterBean cardCenterBean = new CardCenterBean();
                    OtherBank cardCenterDetail = (OtherBank) it.next();

                    try {
                        cardCenterBean.setBankcode(cardCenterDetail.getBankcode().toString());
                    } catch (NullPointerException npe) {
                        cardCenterBean.setBankcode("--");
                    }
                    try {
                        cardCenterBean.setStatus(cardCenterDetail.getStatus().getDescription());
                    } catch (Exception npe) {
                        cardCenterBean.setStatus("--");
                    }
                    try {
                        cardCenterBean.setBankname(cardCenterDetail.getBankname().toString());
                    } catch (Exception npe) {
                        cardCenterBean.setBankname("--");
                    }
                    try {
                        cardCenterBean.setCardCodeCenter(cardCenterDetail.getCardCodeCenter().toString());
                    } catch (Exception npe) {
                        cardCenterBean.setCardCodeCenter("--");
                    }
                    try {
                        cardCenterBean.setFundTranMode(cardCenterDetail.getFundTranMode().toString());
                    } catch (Exception npe) {
                        cardCenterBean.setFundTranMode("--");
                    }
                    try {
                        cardCenterBean.setShortCode(cardCenterDetail.getShortCode().toString());
                    } catch (Exception npe) {
                        cardCenterBean.setShortCode("--");
                    }
                    try {
                        cardCenterBean.setMaker(cardCenterDetail.getMaker().toString());
                    } catch (NullPointerException npe) {
                        cardCenterBean.setMaker("--");
                    }
                    try {
                        cardCenterBean.setChecker(cardCenterDetail.getChecker().toString());
                    } catch (NullPointerException npe) {
                        cardCenterBean.setChecker("--");
                    }
                    try {
                        cardCenterBean.setCreatedtime(cardCenterDetail.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        cardCenterBean.setCreatedtime("--");
                    }
                    try {
                        cardCenterBean.setLastupdatedtime(cardCenterDetail.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        cardCenterBean.setLastupdatedtime("--");
                    }

                    cardCenterBean.setFullCount(count);

                    dataList.add(cardCenterBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return dataList;
    }

    private String makeWhereClause(CardCenterInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getBankcodeSearch() != null && !inputBean.getBankcodeSearch().isEmpty()) {
            where += " and lower(u.bankcode) like lower('%" + inputBean.getBankcodeSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getBanknameSearch() != null && !inputBean.getBanknameSearch().isEmpty()) {
            where += " and lower(u.bankname) like lower('%" + inputBean.getBanknameSearch().trim() + "%')";
        }
        if (inputBean.getFundTranModeSearch() != null && !inputBean.getFundTranModeSearch().isEmpty()) {
            where += " and lower(u.fundTranMode) like lower('%" + inputBean.getFundTranModeSearch().trim() + "%')";
        }
        if (inputBean.getCardCodeCenterSearch() != null && !inputBean.getCardCodeCenterSearch().isEmpty()) {
            where += " and lower(u.cardCodeCenter) like lower('%" + inputBean.getCardCodeCenterSearch().trim() + "%')";
        }

        return where;
    }

    public List<CardCenterPendBean> getPendingCardCenterList(CardCenterInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<CardCenterPendBean> dataList = new ArrayList<CardCenterPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.CARD_CENTER_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.CARD_CENTER_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    CardCenterPendBean cardCenter = new CardCenterPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        cardCenter.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        cardCenter.setId("--");
                    }
                    try {
                        cardCenter.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        cardCenter.setOperation("--");
                    }

                    try {
                        cardCenter.setBankcode(pTask.getPKey().toString());
                    } catch (Exception ex) {
                        cardCenter.setBankcode("--");
                    }
                    try {
                        cardCenter.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        cardCenter.setFields("--");
                    } catch (Exception ex) {
                        cardCenter.setFields("--");
                    }
                    try {
                        cardCenter.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        cardCenter.setStatus("--");
                    }
                    try {
                        cardCenter.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        cardCenter.setCreatetime("--");
                    }
                    try {
                        cardCenter.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        cardCenter.setCreateduser("--");
                    } catch (Exception ex) {
                        cardCenter.setCreateduser("--");
                    }

                    cardCenter.setFullCount(count);

                    dataList.add(cardCenter);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();           
            }
        }
        return dataList;
    }

    public String insertCardCenter(CardCenterInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((OtherBank) session.get(OtherBank.class, inputBean.getBankcode().trim()) == null) {
                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getBankcode())
                        .setString("pagecode", audit.getPagecode());
                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getBankcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CARD_CENTER_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());
                    
                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
          if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String deleteCardCenter(CardCenterInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getBankcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                OtherBank u = (OtherBank) session.get(OtherBank.class, inputBean.getBankcode().trim());
                if (u != null) {
                    String cardCenterCode = "";
                    if (u.getCardCodeCenter() != null && !u.getCardCodeCenter().isEmpty()) {
                        cardCenterCode = u.getCardCodeCenter();
                    }
                    audit.setNewvalue(u.getBankcode() + "|" + u.getBankname() + "|" + u.getStatus().getStatuscode() + "|" + u.getFundTranMode() + "|" + u.getShortCode() + "|" + cardCenterCode
                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                    );
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getBankcode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.CARD_CENTER_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());
                
                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public OtherBank findCardCenterById(String bankcode) throws Exception {
        OtherBank mBankLocator = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from OtherBank as u where u.bankcode=:bankcode";
            Query query = session.createQuery(sql).setString("bankcode", bankcode);
            mBankLocator = (OtherBank) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
           if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return mBankLocator;

    }

    public String updateCardCenter(CardCenterInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getBankcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                OtherBank u = (OtherBank) session.get(OtherBank.class, inputBean.getBankcode().trim());
                if (u != null) {

//                    audit.setOldvalue(u.getBankcode() + "|" + u.getBankname() + "|" + u.getStatus().getStatuscode() + "|" + u.getFundTranMode() + "|" + u.getShortCode() + "|" + u.getCardCodeCenter()
                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
//                    );

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getBankcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CARD_CENTER_PAGE);
                    pendingtask.setPage(page);
                    
                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmCardCenter(CardCenterInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    OtherBank cardCenter = new OtherBank();

                    cardCenter.setBankcode(penArray[0]);
                    cardCenter.setBankname(penArray[1]);

                    Status st = (Status) session.get(Status.class, penArray[2]);
                    cardCenter.setStatus(st);
                    cardCenter.setFundTranMode(penArray[3]);
                    cardCenter.setShortCode(penArray[4]);
                    try {
                        cardCenter.setCardCodeCenter(penArray[5]);
                    } catch (Exception e) {

                    }

                    cardCenter.setCreatedtime(timestampDate);
                    cardCenter.setLastupdatedtime(timestampDate);
                    cardCenter.setMaker(pentask.getCreateduser());
                    cardCenter.setChecker(audit.getLastupdateduser());

                    session.save(cardCenter);

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on other bank (Bank code: " + cardCenter.getBankcode() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    OtherBank u = (OtherBank) session.get(OtherBank.class, penArray[0].trim());

                    if (u != null) {
                    //-------------------audit old value-------------------
                        String cardCenterCode = "";
                        if (u.getCardCodeCenter() != null && !u.getCardCodeCenter().isEmpty()) {
                            cardCenterCode = u.getCardCodeCenter();
                        }

                        String oldvalue = u.getBankcode() + "|"
                            + u.getBankname() + "|"
                            + u.getStatus().getStatuscode() + "|"
                            + u.getFundTranMode() + "|"
                            + u.getShortCode() + "|"
                            + cardCenterCode;

                        u.setBankname(penArray[1]);

                        Status st = (Status) session.get(Status.class, penArray[2]);
                        u.setStatus(st);

                        u.setFundTranMode(penArray[3]);
                        u.setShortCode(penArray[4]);
                        try {
                            u.setCardCodeCenter(penArray[5]);
                        } catch (Exception e) {
                            u.setCardCodeCenter("");
                        }

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        session.update(u);

                        audit.setOldvalue(oldvalue);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on other bank (Bank code: " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    OtherBank u = (OtherBank) session.get(OtherBank.class, penArray[0].trim());
                    if (u != null) {
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on other bank (Bank code: " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String rejectCardCenter(CardCenterInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    OtherBank otherBranch = (OtherBank) session.get(OtherBank.class,  u.getPKey());

                    if (otherBranch != null) {
                    //-------------------audit old value-------------------
                        String cardCenterCode = "";
                        if (otherBranch.getCardCodeCenter() != null && !otherBranch.getCardCodeCenter().isEmpty()) {
                            cardCenterCode = otherBranch.getCardCodeCenter();
                        }

                        String oldvalue = otherBranch.getBankcode() + "|"
                            + otherBranch.getBankname() + "|"
                            + otherBranch.getStatus().getStatuscode() + "|"
                            + otherBranch.getFundTranMode() + "|"
                            + otherBranch.getShortCode() + "|"
                            + cardCenterCode;
                        audit.setOldvalue(oldvalue);
                    }
                }
                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on other bank (Bank code: " + u.getPKey() + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public boolean isRecordUse(String bankcode) throws Exception {
        boolean bankCodeCheck = false;
        Session session = null;
        try {
            long count = 0;
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id.bankcode) from OtherBankBranch as u where u.id.bankcode=:bankcode";
            Query query = session.createQuery(sqlCount).setString("bankcode", bankcode);

            Iterator itCount = query.iterate();
            count = (Long) itCount.next();

            if (count > 0) {
                bankCodeCheck = true;

            } else {
                bankCodeCheck = false;
            }

        } catch (Exception e) {
            throw e;
        } finally {
           if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return bankCodeCheck;

    }

    public StringBuffer makeCSVReport(CardCenterInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String orderBy = "order by u.bankcode asc";
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from OtherBank u where " + where + orderBy;
            Query querySearch = session.createQuery(sqlSearch);

            Iterator it = querySearch.iterate();
            content = new StringBuffer();

            //write column headers to csv file
            content.append("Bank Code");
            content.append(',');
            content.append("Bank Name");
            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Fund Tran Mode");
            content.append(',');
            content.append("Short Code");
            content.append(',');
            content.append("Card Code Center");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Checker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');

            while (it.hasNext()) {

                OtherBank bank = (OtherBank) it.next();

                try {
                    content.append(bank.getBankcode().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getBankname().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getStatus().getDescription());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getFundTranMode().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getShortCode());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getCardCodeCenter().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getChecker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getCreatedtime().toString().substring(0, 19));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bank.getLastupdatedtime().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    content.append("--");
                }
                content.append('\n');
            }
        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

}
