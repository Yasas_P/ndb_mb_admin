/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.emailmgt;

import com.epic.ndb.bean.controlpanel.emailmgt.EmailSubjectBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailSubjectInputBean;
import com.epic.ndb.bean.controlpanel.emailmgt.EmailSubjectPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.InboxServiceCategory;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author yasas_p
 */
public class EmailSubjectDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public String insertMailSubject(EmailSubjectInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((InboxServiceCategory) session.get(InboxServiceCategory.class, Short.valueOf(inputBean.getSubjectid().trim())) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getSubjectid())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getSubjectid().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.MAIL_SUB_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();

                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<EmailSubjectBean> getSearchList(EmailSubjectInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<EmailSubjectBean> dataList = new ArrayList<EmailSubjectBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = " order by TT.LASTUPDATEDTIME desc ";
            }
            String where = this.makeWhereClause(inputBean);

            BigDecimal count = new BigDecimal(0);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(TT.ID) from INBOX_SERVICE_CATEGORY TT where " + where;

            Query queryCount = session.createSQLQuery(sqlCount);

            List countList = queryCount.list();
            count = (BigDecimal) countList.get(0);

            if (count.longValue() > 0) {

              String sqlSearch = " SELECT * from ( select TT.ID , TT.SERVICE, ST.DESCRIPTION AS STATUSDESC , TT.MAKER , TT.CHECKER , TT.CREATEDTIME , TT.LASTUPDATEDTIME, "
                        + " row_number() over (" + orderBy + ") as r "
        
                        + " from INBOX_SERVICE_CATEGORY TT,STATUS ST "
                        + " where TT.STATUS=ST.STATUSCODE AND " + where + ") where r > " + first + " and r<= " + max;
     
                List<Object[]> chequeList = (List<Object[]>) session.createSQLQuery(sqlSearch).list();

                for (Object[] ttBean : chequeList) {

                    EmailSubjectBean Bean = new EmailSubjectBean();

                    try {
                        Bean.setSubjectid(ttBean[0].toString());
                    } catch (NullPointerException npe) {
                        Bean.setSubjectid("--");
                    }
                    try {
                        Bean.setService(ttBean[1].toString());
                    } catch (NullPointerException npe) {
                        Bean.setService("--");
                    }
                    try {
                        Bean.setStatus(ttBean[2].toString());
                    } catch (NullPointerException npe) {
                        Bean.setStatus("--");
                    }
                    try {
                        Bean.setMaker(ttBean[3].toString());
                    } catch (NullPointerException npe) {
                        Bean.setMaker("--");
                    }
                    try {
                        Bean.setChecker(ttBean[4].toString());
                    } catch (NullPointerException npe) {
                        Bean.setChecker("--");
                    }
                    try {
                        Bean.setCreatedtime(ttBean[5].toString().substring(0, 19));
                    } catch (Exception npe) {
                        Bean.setCreatedtime("--");
                    }
                    try {
                        Bean.setLastupdatedtime(ttBean[6].toString().substring(0, 19));
                    } catch (Exception npe) {
                        Bean.setLastupdatedtime("--");
                    }

                    Bean.setFullCount(count.longValue());

                    dataList.add(Bean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(EmailSubjectInputBean inputBean) throws Exception {

        String where = "1=1";

        if ((inputBean.getS_id()== null || inputBean.getS_id().isEmpty())
                && (inputBean.getS_service() == null || inputBean.getS_service().isEmpty())
                && (inputBean.getS_status() == null || inputBean.getS_status().isEmpty())
               ) {

        } else {

            if (inputBean.getS_id() != null && !inputBean.getS_id().isEmpty()) {
                where += " and TT.ID like '%" + inputBean.getS_id() + "%'";
            }
            if (inputBean.getS_service() != null && !inputBean.getS_service().isEmpty()) {
                where += " and lower(TT.SERVICE) like lower('%" + inputBean.getS_service() + "%')";
            }
            if (inputBean.getS_status() != null && !inputBean.getS_status().isEmpty()) {
                where += " and TT.STATUS='" + inputBean.getS_status() + "'";
            }

        }
        return where;
    }

    public List<EmailSubjectPendBean> getPendingMailSubjectList(EmailSubjectInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<EmailSubjectPendBean> dataList = new ArrayList<EmailSubjectPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.MAIL_SUB_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.MAIL_SUB_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    EmailSubjectPendBean jcode = new EmailSubjectPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        jcode.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        jcode.setSubjectid(penArray[0]);
                    } catch (NullPointerException npe) {
                        jcode.setSubjectid("--");
                    } catch (Exception ex) {
                        jcode.setSubjectid("--");
                    }
                    try {
                        jcode.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        jcode.setFields("--");
                    } catch (Exception ex) {
                        jcode.setFields("--");
                    }
                    try {
                        jcode.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public InboxServiceCategory findMailSubjectById(String id) throws Exception {
        InboxServiceCategory tt = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from InboxServiceCategory as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", id);
            tt = (InboxServiceCategory) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return tt;

    }

    public String updateMailSubject(EmailSubjectInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getSubjectid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getSubjectid().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.MAIL_SUB_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());
                pendingtask.setInputterbranch(sysUser.getBranch());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteMailSubject(EmailSubjectInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getSubjectid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                
                String sql1 = "select count(*) from InboxMessage as u where u.inboxServiceCategory.id =:id";
                Query query1 = session.createQuery(sql1).setLong("id", Short.valueOf(inputBean.getSubjectid().trim()));
                Long count1 = (Long) query1.iterate().next();

                if (count1 != 0) {
                    message = MessageVarList.COMMON_ALREADY_IN_USE;
                } else {
                    String sql2 = "select count(*) from InboxMsgCon as u where u.inboxServiceCategory.id =:id";
                    Query query2 = session.createQuery(sql2).setLong("id", Short.valueOf(inputBean.getSubjectid().trim()));
                    Long count2 = (Long) query2.iterate().next();

                    if (count2 != 0) {
                        message = MessageVarList.COMMON_ALREADY_IN_USE;
                    } else {

                        InboxServiceCategory u = (InboxServiceCategory) session.get(InboxServiceCategory.class, Short.valueOf(inputBean.getSubjectid().trim()));
                        if (u != null) {
                            audit.setNewvalue(u.getId()+ "|" + u.getService()+ "|" + u.getStatus().getStatuscode());
                        }

                        Pendingtask pendingtask = new Pendingtask();

                        pendingtask.setPKey(inputBean.getSubjectid().trim());
                        pendingtask.setFields(audit.getNewvalue());

                        Task task = new Task();
                        task.setTaskcode(TaskVarList.DELETE_TASK);
                        pendingtask.setTask(task);

                        Status st = new Status();
                        st.setStatuscode(CommonVarList.STATUS_PENDING);
                        pendingtask.setStatus(st);

                        Page page = (Page) session.get(Page.class, PageVarList.MAIL_SUB_MGT_PAGE);
                        pendingtask.setPage(page);

                        pendingtask.setCreatedtime(sysDate);
                        pendingtask.setLastupdatedtime(sysDate);
                        pendingtask.setCreateduser(audit.getLastupdateduser());
                        
                        pendingtask.setInputterbranch(sysUser.getBranch());

                        audit.setCreatetime(sysDate);
                        audit.setLastupdatedtime(sysDate);
                        audit.setLastupdateduser(audit.getLastupdateduser());

                        session.save(audit);
                        session.save(pendingtask);
                        txn.commit();
                    }
                }
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmMailSubject(EmailSubjectInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getSubjectid());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    InboxServiceCategory jcode = new InboxServiceCategory();
                    jcode.setId(Short.valueOf(penArray[0]));
                    jcode.setService(penArray[1]);

                    Status st = (Status) session.get(Status.class, penArray[2].trim());
                    jcode.setStatus(st);

                    jcode.setCreatedtime(timestampDate);
                    jcode.setLastupdatedtime(timestampDate);
                    jcode.setMaker(pentask.getCreateduser());
                    jcode.setChecker(audit.getLastupdateduser());

                    audit.setNewvalue(jcode.getId()+ "|" + jcode.getService()+ "|" + jcode.getStatus().getDescription());
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on mail subject (ID: " + jcode.getId()+ ")  inputted by " + pentask.getCreateduser() + " approved "  + audit.getDescription());

                    session.save(jcode);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    InboxServiceCategory u = (InboxServiceCategory) session.get(InboxServiceCategory.class, Short.valueOf(penArray[0].trim()));

                    if (u != null) {

                        audit.setOldvalue(u.getId()+ "|" + u.getService()+ "|" + u.getStatus().getDescription());

                        u.setService(penArray[1].trim());

                         Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(st);

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getService()+ "|" + u.getStatus().getDescription());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on mail subject (ID: " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    InboxServiceCategory u = (InboxServiceCategory) session.get(InboxServiceCategory.class, Short.valueOf(penArray[0]));
                    if (u != null) {
                        audit.setNewvalue(penArray[0] + "|" + u.getService()+ "|" + u.getStatus().getDescription());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on mail subject (ID: " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectMailSubject(EmailSubjectInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getSubjectid().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }

                String[] fieldsArray = u.getFields().split("\\|");

                String code = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    InboxServiceCategory inboxServiceCategory = (InboxServiceCategory) session.get(InboxServiceCategory.class, Short.valueOf(code.trim()));

                    if (inboxServiceCategory != null) {

                        audit.setOldvalue(inboxServiceCategory.getId()+ "|" + inboxServiceCategory.getService()+ "|" + inboxServiceCategory.getStatus().getStatuscode());
                    }
                }
                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on mail subject (ID: " + code + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

}
