/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.TransactionLimitBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TransactionLimitInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TransactionLimitPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.SegmentType;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.TransactionLimit;
import com.epic.ndb.util.mapping.TransactionLimitId;
import com.epic.ndb.util.mapping.TransferType;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class TransactionLimitDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<TransactionLimitBean> getSearchList(TransactionLimitInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<TransactionLimitBean> dataList = new ArrayList<TransactionLimitBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(u.id.transfertype) from TransactionLimit as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from TransactionLimit u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    TransactionLimitBean txnLimitBean = new TransactionLimitBean();
                    TransactionLimit txnLimit = (TransactionLimit) it.next();

                    try {
                        txnLimitBean.setTransferType(txnLimit.getId().getTransfertype());
//                        txnLimitBean.setTransferTypeDes(txnLimit.getTransferType().getDescription());
                    } catch (Exception npe) {
                        txnLimitBean.setTransferType("--");
//                        txnLimitBean.setTransferTypeDes("--");
                    }
                    try {
//                        txnLimitBean.setTransferType(txnLimit.getId().getTransfertype());
                        txnLimitBean.setTransferTypeDes(txnLimit.getTransferType().getDescription());
                    } catch (Exception npe) {
//                        txnLimitBean.setTransferType("--");
                        txnLimitBean.setTransferTypeDes("--");
                    }
                    try {
                        txnLimitBean.setSegmentType(txnLimit.getId().getSegmtype());
                        txnLimitBean.setSegmentTypeDes(txnLimit.getSegmentType().getDescription());
                    } catch (Exception npe) {
                        txnLimitBean.setSegmentType("--");
                        txnLimitBean.setSegmentTypeDes("--");
                    }
                    try {
                        txnLimitBean.setStatus(txnLimit.getStatus().getDescription());
                    } catch (Exception npe) {
                        txnLimitBean.setStatus("--");
                    }
                    try {
                        txnLimitBean.setDefaultLimit(txnLimit.getDefaultLimit().toString());
                    } catch (Exception npe) {
                        txnLimitBean.setDefaultLimit("--");
                    }
                    try {
                        txnLimitBean.setTranLimitMin(txnLimit.getTranLimitMin().toString());
                    } catch (Exception npe) {
                        txnLimitBean.setTranLimitMin("--");
                    }
                    try {
                        txnLimitBean.setTranLimitMax(txnLimit.getTranLimitMax().toString());
                    } catch (Exception npe) {
                        txnLimitBean.setTranLimitMax("--");
                    }
                    try {
                        txnLimitBean.setDailyLimitMin(txnLimit.getDailyLimitMin().toString());
                    } catch (Exception npe) {
                        txnLimitBean.setDailyLimitMin("--");
                    }
                    try {
                        txnLimitBean.setDailyLimitMax(txnLimit.getDailyLimitMax().toString());
                    } catch (Exception npe) {
                        txnLimitBean.setDailyLimitMax("--");
                    }
                    try {
                        txnLimitBean.setMaker(txnLimit.getMaker().toString());
                    } catch (NullPointerException npe) {
                        txnLimitBean.setMaker("--");
                    }
                    try {
                        txnLimitBean.setChecker(txnLimit.getChecker().toString());
                    } catch (NullPointerException npe) {
                        txnLimitBean.setChecker("--");
                    }
                    try {
                        txnLimitBean.setCreatedtime(txnLimit.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        txnLimitBean.setCreatedtime("--");
                    }
                    try {
                        txnLimitBean.setLastupdatedtime(txnLimit.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        txnLimitBean.setLastupdatedtime("--");
                    }

                    txnLimitBean.setFullCount(count);

                    dataList.add(txnLimitBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(TransactionLimitInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getTransferTypeSearch() != null && !inputBean.getTransferTypeSearch().isEmpty()) {
            where += " and lower(u.id.transfertype) like lower('%" + inputBean.getTransferTypeSearch().trim() + "%')";
        }
        if (inputBean.getSegmentTypeSearch() != null && !inputBean.getSegmentTypeSearch().isEmpty()) {
            where += " and lower(u.id.segmtype) like lower('%" + inputBean.getSegmentTypeSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getDefaultLimitSearch() != null && !inputBean.getDefaultLimitSearch().isEmpty()) {
            where += " and lower(u.defaultLimit) like lower('%" + inputBean.getDefaultLimitSearch().trim() + "%')";
        }
        if (inputBean.getTranLimitMinSearch() != null && !inputBean.getTranLimitMinSearch().isEmpty()) {
            where += " and lower(u.tranLimitMin ) like lower('%" + inputBean.getTranLimitMinSearch().trim() + "%')";
        }
        if (inputBean.getTranLimitMaxSearch() != null && !inputBean.getTranLimitMaxSearch().isEmpty()) {
            where += " and lower(u.tranLimitMax) like lower('%" + inputBean.getTranLimitMaxSearch().trim() + "%')";
        }
        if (inputBean.getDailyLimitMinSearch() != null && !inputBean.getDailyLimitMinSearch().isEmpty()) {
            where += " and lower(u.dailyLimitMin) like lower('%" + inputBean.getDailyLimitMinSearch().trim() + "%')";
        }
        if (inputBean.getDailyLimitMaxSearch() != null && !inputBean.getDailyLimitMaxSearch().isEmpty()) {
            where += " and lower(u.dailyLimitMax) like lower('%" + inputBean.getDailyLimitMaxSearch().trim() + "%')";
        }

        return where;
    }

    public List<TransactionLimitPendBean> getPendingTransactionLimitList(TransactionLimitInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<TransactionLimitPendBean> dataList = new ArrayList<TransactionLimitPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.TRANSACTION_LIMIT_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.TRANSACTION_LIMIT_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    TransactionLimitPendBean pendData = new TransactionLimitPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        pendData.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        pendData.setId("--");
                    }
                    try {
                        pendData.setOperationcode(pTask.getTask().getTaskcode().toString());
                        pendData.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        pendData.setOperationcode("--");
                        pendData.setOperation("--");
                    }

//                    String[] penArray = null;
//                    if (pTask.getFields() != null) {
//                        penArray = pTask.getFields().split("\\|");
//                    }
//
//                    try {
//                        cardCenter.setBankcode(penArray[0]);
//                    } catch (NullPointerException npe) {
//                        cardCenter.setBankcode("--");
//                    } catch (Exception ex) {
//                        cardCenter.setBankcode("--");
//                    }
                    String[] pKeyArray = null;
                    if (pTask.getPKey() != null && !pTask.getPKey().isEmpty()) {
                        pKeyArray = pTask.getPKey().split("\\|");
                        try {
                            TransferType transferType = (TransferType) session.get(TransferType.class, pKeyArray[0]);
                            pendData.setTransferType(transferType.getDescription());
                        } catch (Exception ex) {
                            pendData.setTransferType("--");
                        }
                        try {
                            SegmentType segmentType = (SegmentType) session.get(SegmentType.class, pKeyArray[1]);
                            pendData.setSegmentType(segmentType.getDescription());
                        } catch (Exception ex) {
                            pendData.setSegmentType("--");
                        }
                    }
                    try {
                        pendData.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        pendData.setFields("--");
                    } catch (Exception ex) {
                        pendData.setFields("--");
                    }
                    try {
                        pendData.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        pendData.setStatus("--");
                    }
                    try {
                        pendData.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        pendData.setCreatetime("--");
                    }
                    try {
                        pendData.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        pendData.setCreateduser("--");
                    } catch (Exception ex) {
                        pendData.setCreateduser("--");
                    }

                    pendData.setFullCount(count);

                    dataList.add(pendData);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertTransactionLimit(TransactionLimitInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            TransactionLimitId id = new TransactionLimitId();
            id.setTransfertype(inputBean.getTransferType());
            id.setSegmtype(inputBean.getSegmentType());

            if ((TransactionLimit) session.get(TransactionLimit.class, id) == null) {

                String pKey = inputBean.getTransferType().trim() + "|" + inputBean.getSegmentType().trim();

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", pKey)
                        .setString("pagecode", audit.getPagecode());
                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(pKey);
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.TRANSACTION_LIMIT_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());
                    
                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteTransactionLimit(TransactionLimitInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String pKey = inputBean.getTransferType().trim() + "|" + inputBean.getSegmentType().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                TransactionLimitId id = new TransactionLimitId();
                id.setTransfertype(inputBean.getTransferType().trim());
                id.setSegmtype(inputBean.getSegmentType().trim());

                TransactionLimit u = (TransactionLimit) session.get(TransactionLimit.class, id);
                if (u != null) {

                    audit.setNewvalue(u.getId().getTransfertype() + "|" + u.getId().getSegmtype() + "|" + u.getStatus().getStatuscode()
                            + "|" + u.getDefaultLimit() + "|" + u.getTranLimitMin() + "|" + u.getTranLimitMax() + "|" + u.getDailyLimitMin() + "|" + u.getDailyLimitMax()
                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                    );
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(pKey.trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.TRANSACTION_LIMIT_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());
                
                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public TransactionLimit findTransactionLimitById(String transferType, String segmentType) throws Exception {
        TransactionLimit transactionLimit = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from TransactionLimit as u where u.id.transfertype=:transfertype and u.id.segmtype=:segmtype";
            Query query = session.createQuery(sql).setString("transfertype", transferType).setString("segmtype", segmentType);
            transactionLimit = (TransactionLimit) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return transactionLimit;

    }

    public String updateTransactionLimit(TransactionLimitInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            String pKey = inputBean.getTransferType().trim() + "|" + inputBean.getSegmentType().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                TransactionLimitId id = new TransactionLimitId();
                id.setTransfertype(inputBean.getTransferType().trim());
                id.setSegmtype(inputBean.getSegmentType().trim());

                TransactionLimit u = (TransactionLimit) session.get(TransactionLimit.class, id);
                if (u != null) {

//                    audit.setOldvalue(u.getId().getTransfertype() + "|" + u.getId().getSegmtype() + "|" + u.getStatus().getStatuscode()
//                            + "|" + u.getDefaultLimit() + "|" + u.getTranLimitMin() + "|" + u.getTranLimitMax() + "|" + u.getDailyLimitMin() + "|" + u.getDailyLimitMax()
//                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
//                    );

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(pKey.trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.TRANSACTION_LIMIT_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setInputterbranch(sysUser.getBranch());
                    
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmTransactionLimit(TransactionLimitInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());
                    
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    TransactionLimitId id = new TransactionLimitId();

                    id.setTransfertype(penArray[0]);
                    id.setSegmtype(penArray[1]);

                    TransactionLimit transactionLimit = new TransactionLimit();

                    transactionLimit.setId(id);

                    Status st = (Status) session.get(Status.class, penArray[2]);
                    transactionLimit.setStatus(st);

                    transactionLimit.setDefaultLimit(new BigDecimal(penArray[3]));
                    transactionLimit.setTranLimitMin(new BigDecimal(penArray[4]));
                    transactionLimit.setTranLimitMax(new BigDecimal(penArray[5]));
                    transactionLimit.setDailyLimitMin(new BigDecimal(penArray[6]));
                    transactionLimit.setDailyLimitMax(new BigDecimal(penArray[7]));

                    transactionLimit.setCreatedtime(sysDate);
                    transactionLimit.setLastupdatedtime(sysDate);
                    transactionLimit.setMaker(pentask.getCreateduser());
                    transactionLimit.setChecker(audit.getLastupdateduser());

                    session.save(transactionLimit);

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on teansaction limit (transfer type :  " + transactionLimit.getId().getTransfertype() + ", segment type : " + transactionLimit.getId().getSegmtype() + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    TransactionLimitId id = new TransactionLimitId();
                    id.setTransfertype(penArray[0]);
                    id.setSegmtype(penArray[1]);

                    TransactionLimit u = (TransactionLimit) session.get(TransactionLimit.class, id);

                    if (u != null) {
                        
                        //---------------------audit old value--------------
                        String oldVal =penArray[0] + "|"
                            + penArray[1] + "|"
                            + u.getStatus().getStatuscode() + "|"
                            + u.getDefaultLimit().toString() + "|"
                            + u.getTranLimitMin().toString() + "|"
                            + u.getTranLimitMax().toString() + "|"
                            + u.getDailyLimitMin().toString() + "|"
                            + u.getDailyLimitMax().toString();
                        
                        Status st = (Status) session.get(Status.class, penArray[2]);
                        u.setStatus(st);

                        u.setDefaultLimit(new BigDecimal(penArray[3]));
                        u.setTranLimitMin(new BigDecimal(penArray[4]));
                        u.setTranLimitMax(new BigDecimal(penArray[5]));
                        u.setDailyLimitMin(new BigDecimal(penArray[6]));
                        u.setDailyLimitMax(new BigDecimal(penArray[7]));

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        session.update(u);

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on teansaction limit (transfer type :  " + u.getId().getTransfertype() + ", segment type : " + u.getId().getSegmtype() + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
                    TransactionLimitId id = new TransactionLimitId();

                    id.setTransfertype(penArray[0]);
                    id.setSegmtype(penArray[1]);

                    TransactionLimit u = (TransactionLimit) session.get(TransactionLimit.class, id);
                    if (u != null) {
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on teansaction limit (transfer type :  " + u.getId().getTransfertype() + ", segment type : " + u.getId().getSegmtype() + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectTransactionLimit(TransactionLimitInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                String[] pKeyArray = null;
                String transferType = "";
                String segmentType = "";
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                if (u.getPKey() != null) {
                    pKeyArray = u.getPKey().split("\\|");
                }

                try {
                    transferType = pKeyArray[0];
                    segmentType = pKeyArray[1];
                    
                    //------------------------audit old value------------
                    if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                        TransactionLimitId id = new TransactionLimitId();
                        id.setTransfertype(pKeyArray[0]);
                        id.setSegmtype(pKeyArray[1]);

                        TransactionLimit txnLimit = (TransactionLimit) session.get(TransactionLimit.class, id);

                        if (txnLimit != null) {

                            String oldVal =pKeyArray[0] + "|"
                                + pKeyArray[1] + "|"
                                + txnLimit.getStatus().getStatuscode() + "|"
                                + txnLimit.getDefaultLimit().toString() + "|"
                                + txnLimit.getTranLimitMin().toString() + "|"
                                + txnLimit.getTranLimitMax().toString() + "|"
                                + txnLimit.getDailyLimitMin().toString() + "|"
                                + txnLimit.getDailyLimitMax().toString();
                            audit.setOldvalue(oldVal);
                        }
                    }
                } catch (Exception e) {

                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on  teansaction limit (transfer type :  " + transferType + ", segment type : " + segmentType + ") inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public StringBuffer makeCSVReport(TransactionLimitInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String orderby = " order by u.createdtime desc ";
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from TransactionLimit u where " + where + orderby;
            Query querySearch = session.createQuery(sqlSearch);

            Iterator it = querySearch.iterate();
            content = new StringBuffer();

            //write column headers to csv file
            content.append("Transfer Type");
            content.append(',');
            content.append("Segment Type");
            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Default Limit");
            content.append(',');
            content.append("Tran Limit Min");
            content.append(',');
            content.append("Tran Limit Max");
            content.append(',');
            content.append("Daily Limit Min");
            content.append(',');
            content.append("Daily Limit Max");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Checker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');

            while (it.hasNext()) {

                TransactionLimit transactionLimit = (TransactionLimit) it.next();

                try {
                    content.append(transactionLimit.getTransferType().getDescription().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getSegmentType().getDescription().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getStatus().getDescription());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getDefaultLimit().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getTranLimitMin().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getTranLimitMax().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getDailyLimitMin().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getDailyLimitMax().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getChecker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getCreatedtime().toString().substring(0, 19));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(transactionLimit.getLastupdatedtime().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    content.append("--");
                }
                content.append('\n');
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
    
    public String getPendOldNewValueOfTransactionLimit(TransactionLimitInputBean inputBean) throws Exception {
        Session session = null;
        String message ="";
        try {
            session = HibernateInit.sessionFactory.openSession();
             Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                
                inputBean.setTaskCode(u.getTask().getTaskcode());
                inputBean.setTaskDescription(u.getTask().getDescription());
                String[] penArray = null;
                if (u.getFields() != null) {
                    penArray = u.getFields().split("\\|");
                }
                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    if(penArray[0]!=null && !penArray[0].isEmpty()){
                        TransferType transferType =(TransferType)session.get(TransferType.class, penArray[0]);
                        if(transferType!=null){
                            inputBean.setTransferType(penArray[0].trim());
                        }else{
                            inputBean.setTransferType("--");
                        }
                    }else{
                        inputBean.setTransferType("--");
                    }
                    
                    if(penArray[1]!=null && !penArray[1].isEmpty()){
                        SegmentType segmentType=(SegmentType)session.get(SegmentType.class, penArray[1].trim());
                        if(segmentType!=null){
                            inputBean.setSegmentType(segmentType.getDescription());
                        }else{
                            inputBean.setSegmentType("--");
                        }
                    }else{
                        inputBean.setSegmentType("--");
                    }
                    //------------------------new value---------------
                    if(penArray[2]!=null && !penArray[2].isEmpty()){
                        Status st = (Status) session.get(Status.class, penArray[2]);
                        if(st!=null){
                            inputBean.setStatus(st.getDescription());
                        }else{
                            inputBean.setStatus("--");
                        }
                    }else{
                        inputBean.setStatus("--");
                    }
                    
                    if(penArray[3]!=null && !penArray[3].isEmpty()){
                        inputBean.setDefaultLimit(penArray[3].trim());
                    }else{
                        inputBean.setDefaultLimit("--");
                    }
                    
                    if(penArray[4]!=null && !penArray[4].isEmpty()){
                        inputBean.setTranLimitMin(penArray[4].trim());
                    }else{
                        inputBean.setTranLimitMin("--");
                    }
                    
                    if(penArray[5]!=null && !penArray[5].isEmpty()){
                        inputBean.setTranLimitMax(penArray[5].trim());
                    }else{
                        inputBean.setTranLimitMax("--");
                    }
                    
                    if(penArray[6]!=null && !penArray[6].isEmpty()){
                        inputBean.setDailyLimitMin(penArray[6].trim());
                    }else{
                        inputBean.setDailyLimitMin("--");
                    }
                    
                    if(penArray[7]!=null && !penArray[7].isEmpty()){
                        inputBean.setDailyLimitMax(penArray[7].trim());
                    }else{
                        inputBean.setDailyLimitMax("--");
                    }
                    
                    //---------------------------old value-----------------------
                    inputBean.setStatusOld("--");
                    inputBean.setDefaultLimitOld("--");
                    inputBean.setTranLimitMinOld("--");
                    inputBean.setTranLimitMaxOld("--");
                    inputBean.setDailyLimitMinOld("--");
                    inputBean.setDailyLimitMaxOld("--");
                    
                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    
                    if(penArray[0]!=null && !penArray[0].isEmpty()){
                        TransferType transferType =(TransferType)session.get(TransferType.class, penArray[0]);
                        if(transferType!=null){
                            inputBean.setTransferType(penArray[0].trim());
                        }else{
                            inputBean.setTransferType("--");
                        }
                    }else{
                        inputBean.setTransferType("--");
                    }
                    
                    if(penArray[1]!=null && !penArray[1].isEmpty()){
                        SegmentType segmentType=(SegmentType)session.get(SegmentType.class, penArray[1].trim());
                        if(segmentType!=null){
                            inputBean.setSegmentType(segmentType.getDescription());
                        }else{
                            inputBean.setSegmentType("--");
                        }
                    }else{
                        inputBean.setSegmentType("--");
                    }
                    //-----------------------------new value-------------------------------------//
                    
                    if(penArray[2]!=null && !penArray[2].isEmpty()){
                        Status st = (Status) session.get(Status.class, penArray[2]);
                        if(st!=null){
                            inputBean.setStatus(st.getDescription());
                        }else{
                            inputBean.setStatus("--");
                        }
                    }else{
                        inputBean.setStatus("--");
                    }
                    
                    if(penArray[3]!=null && !penArray[3].isEmpty()){
                        inputBean.setDefaultLimit(penArray[3].trim());
                    }else{
                        inputBean.setDefaultLimit("--");
                    }
                    
                    if(penArray[4]!=null && !penArray[4].isEmpty()){
                        inputBean.setTranLimitMin(penArray[4].trim());
                    }else{
                        inputBean.setTranLimitMin("--");
                    }
                    
                    if(penArray[5]!=null && !penArray[5].isEmpty()){
                        inputBean.setTranLimitMax(penArray[5].trim());
                    }else{
                        inputBean.setTranLimitMax("--");
                    }
                    
                    if(penArray[6]!=null && !penArray[6].isEmpty()){
                        inputBean.setDailyLimitMin(penArray[6].trim());
                    }else{
                        inputBean.setDailyLimitMin("--");
                    }
                    
                    if(penArray[7]!=null && !penArray[7].isEmpty()){
                        inputBean.setDailyLimitMax(penArray[7].trim());
                    }else{
                        inputBean.setDailyLimitMax("--");
                    }
                    //-----------------------------old value-------------------------------------//
                     TransactionLimitId id = new TransactionLimitId();
                    id.setTransfertype(penArray[0]);
                    id.setSegmtype(penArray[1]);

                    TransactionLimit txnLimit = (TransactionLimit) session.get(TransactionLimit.class, id);
                    
                    if(txnLimit.getStatus()!=null ){
                        inputBean.setStatusOld(txnLimit.getStatus().getDescription().trim());
                    }else{
                        inputBean.setStatusOld("--");
                    }
                    
                    if(txnLimit.getDefaultLimit()!=null ){
                        inputBean.setDefaultLimitOld(txnLimit.getDefaultLimit().toString());
                    }else{
                        inputBean.setDefaultLimitOld("--");
                    }
                    
                    if(txnLimit.getTranLimitMin()!=null ){
                        inputBean.setTranLimitMinOld(txnLimit.getTranLimitMin().toString());
                    }else{
                        inputBean.setTranLimitMinOld("--");
                    }
                    
                    if(txnLimit.getTranLimitMax()!=null ){
                        inputBean.setTranLimitMaxOld(txnLimit.getTranLimitMax().toString());
                    }else{
                        inputBean.setTranLimitMaxOld("--");
                    }
                    
                    if(txnLimit.getDailyLimitMin()!=null ){
                        inputBean.setDailyLimitMinOld(txnLimit.getDailyLimitMin().toString());
                    }else{
                        inputBean.setDailyLimitMinOld("--");
                    }
                    
                    if(txnLimit.getDailyLimitMax()!=null ){
                        inputBean.setDailyLimitMaxOld(txnLimit.getDailyLimitMax().toString());
                    }else{
                        inputBean.setDailyLimitMaxOld("--");
                    }
                }
            }else{
              message="Record not exist.";  
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }    
        return message;
    }
}
