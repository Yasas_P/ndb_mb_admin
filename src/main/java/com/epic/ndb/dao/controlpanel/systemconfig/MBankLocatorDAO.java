/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.MBankLocatorBean;
import com.epic.ndb.bean.controlpanel.systemconfig.MBankLocatorInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.MBankLocatorPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.ChannelType;
import com.epic.ndb.util.mapping.MBankLocator;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class MBankLocatorDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<MBankLocatorBean> getSearchList(MBankLocatorInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<MBankLocatorBean> dataList = new ArrayList<MBankLocatorBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(locatorid) from MBankLocator as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from MBankLocator u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    MBankLocatorBean mBankLocatorBean = new MBankLocatorBean();
                    MBankLocator mBankLocator = (MBankLocator) it.next();

                    try {
                        mBankLocatorBean.setLocatorid(mBankLocator.getLocatorid().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setLocatorid("--");
                    }
                    try {
                        mBankLocatorBean.setLocatorcode(mBankLocator.getLocatorcode().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setLocatorcode("--");
                    }
                    try {
                        Status status = (Status) session.get(Status.class, mBankLocator.getStatus());
                        mBankLocatorBean.setStatus(status.getDescription());
                    } catch (Exception npe) {
                        mBankLocatorBean.setStatus("--");
                    }
                    try {
                        mBankLocatorBean.setChannelType(mBankLocator.getChannelType().getChannelType().toString());
                    } catch (Exception npe) {
                        mBankLocatorBean.setChannelType("--");
                    }
//                    try {
//                        mBankLocatorBean.setChannelSubType(mBankLocator.getChannelSubType().toString());
//                    } catch (Exception npe) {
//                        mBankLocatorBean.setChannelSubType("--");
//                    }
                    try {
                        mBankLocatorBean.setCountryCode(mBankLocator.getCountryCode().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setCountryCode("--");
                    }
                    try {
                        mBankLocatorBean.setCountry(mBankLocator.getCountry().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setCountry("--");
                    }
                    try {
                        mBankLocatorBean.setMasterRegion(mBankLocator.getMasterRegion().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setMasterRegion("--");
                    }
//                    try {
//                        mBankLocatorBean.setRegion(mBankLocator.getRegion().toString());
//                    } catch (NullPointerException npe) {
//                        mBankLocatorBean.setRegion("--");
//                    }
                    try {
                        mBankLocatorBean.setSubRegion(mBankLocator.getSubRegion().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setSubRegion("--");
                    }
                    try {
                        mBankLocatorBean.setName(mBankLocator.getName().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setName("--");
                    }
                    try {
                        mBankLocatorBean.setAddress(mBankLocator.getAddress().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setAddress("--");
                    }
                    try {
                        mBankLocatorBean.setPostalCode(mBankLocator.getPostalCode().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setPostalCode("--");
                    }
                    try {
                        mBankLocatorBean.setContacts(mBankLocator.getContacts().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setContacts("--");
                    }
                    try {
                        mBankLocatorBean.setFax(mBankLocator.getFax().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setFax("--");
                    }
                    try {
                        mBankLocatorBean.setOpeningHsMonFri(mBankLocator.getOpeningHsMonFri().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setOpeningHsMonFri("--");
                    }
                    try {
                        mBankLocatorBean.setOpeningHsSat(mBankLocator.getOpeningHsSat().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setOpeningHsSat("--");
                    }
                    try {
                        mBankLocatorBean.setOpeningHsSunHol(mBankLocator.getOpeningHsSunHol().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setOpeningHsSunHol("--");
                    }
                    try {
                        mBankLocatorBean.setHolidayBanking(mBankLocator.getHolidayBanking().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setHolidayBanking("--");
                    }
                    try {
                        mBankLocatorBean.setLongitude(mBankLocator.getLongitude().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setLongitude("--");
                    }
                    try {
                        mBankLocatorBean.setLatitude(mBankLocator.getLatitude().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setLatitude("--");
                    }
                    try {
                        mBankLocatorBean.setLocationTag(mBankLocator.getLocationTag().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setLocationTag("--");
                    }
                    try {
                        mBankLocatorBean.setLanguage(mBankLocator.getLanguage().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setLanguage("--");
                    }
                    try {
                        mBankLocatorBean.setAtmOnLocation(mBankLocator.getAtmOnLocation().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setAtmOnLocation("--");
                    }
                    try {
                        mBankLocatorBean.setCrmOnLocation(mBankLocator.getCrmOnLocation().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setCrmOnLocation("--");
                    }
                    try {
                        mBankLocatorBean.setPawningOnLocation(mBankLocator.getPawningOnLocation().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setPawningOnLocation("--");
                    }
                    try {
                        mBankLocatorBean.setSafeDepositLockers(mBankLocator.getSafeDepositLockers().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setSafeDepositLockers("--");
                    }
                    try {
                        mBankLocatorBean.setLeasingDesk365(mBankLocator.getLeasingDesk365().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setLeasingDesk365("--");
                    }
                    try {
                        mBankLocatorBean.setPrvCentre(mBankLocator.getPrvCentre().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setPrvCentre("--");
                    }
                    try {
                        mBankLocatorBean.setBranchlessBanking(mBankLocator.getBranchlessBanking().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setBranchlessBanking("--");
                    }
                    try {
                        mBankLocatorBean.setMaker(mBankLocator.getMaker().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setMaker("--");
                    }
                    try {
                        mBankLocatorBean.setChecker(mBankLocator.getChecker().toString());
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setChecker("--");
                    }
                    try {
                        mBankLocatorBean.setCreatedtime(mBankLocator.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setCreatedtime("--");
                    }
                    try {
                        mBankLocatorBean.setLastupdatedtime(mBankLocator.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        mBankLocatorBean.setLastupdatedtime("--");
                    }

                    mBankLocatorBean.setFullCount(count);

                    dataList.add(mBankLocatorBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(MBankLocatorInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getLocatoridSearch() != null && !inputBean.getLocatoridSearch().isEmpty()) {
            where += " and lower(u.locatorid) like lower('%" + inputBean.getLocatoridSearch().trim() + "%')";
        }
        if (inputBean.getLocatorcodeSearch() != null && !inputBean.getLocatorcodeSearch().isEmpty()) {
            where += " and lower(u.locatorcode) like lower('%" + inputBean.getLocatorcodeSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getChannelTypeSearch() != null && !inputBean.getChannelTypeSearch().isEmpty()) {
            where += " and lower(u.channelType.channelType) like lower('%" + inputBean.getChannelTypeSearch().trim() + "%')";
        }
//        if (inputBean.getChannelSubTypeSearch() != null && !inputBean.getChannelSubTypeSearch().isEmpty()) {
//            where += " and lower(u.channelSubType) like lower('%" + inputBean.getChannelSubTypeSearch().trim() + "%')";
//        }
//        if (inputBean.getCountryCodeSearch() != null && !inputBean.getCountryCodeSearch().isEmpty()) {
//            where += " and lower(u.countryCode) like lower('%" + inputBean.getCountryCodeSearch().trim() + "%')";
//        }
//        if (inputBean.getCountrySearch() != null && !inputBean.getCountrySearch().isEmpty()) {
//            where += " and lower(u.country) like lower('%" + inputBean.getCountrySearch().trim() + "%')";
//        }
        if (inputBean.getMasterRegionSearch() != null && !inputBean.getMasterRegionSearch().isEmpty()) {
            where += " and lower(u.masterRegion) like lower('%" + inputBean.getMasterRegionSearch().trim() + "%')";
        }
//        if (inputBean.getRegionSearch() != null && !inputBean.getRegionSearch().isEmpty()) {
//            where += " and lower(u.region) like lower('%" + inputBean.getRegionSearch().trim() + "%')";
//        }
        if (inputBean.getSubRegionSearch() != null && !inputBean.getSubRegionSearch().isEmpty()) {
            where += " and lower(u.subRegion) like lower('%" + inputBean.getSubRegionSearch().trim() + "%')";
        }
        if (inputBean.getNameSearch() != null && !inputBean.getNameSearch().isEmpty()) {
            where += " and lower(u.name) like lower('%" + inputBean.getNameSearch().trim() + "%')";
        }
        if (inputBean.getAddressSearch() != null && !inputBean.getAddressSearch().isEmpty()) {
            where += " and lower(u.address) like lower('%" + inputBean.getAddressSearch().trim() + "%')";
        }
        if (inputBean.getPostalCodeSearch() != null && !inputBean.getPostalCodeSearch().isEmpty()) {
            where += " and lower(u.postalCode) like lower('%" + inputBean.getPostalCodeSearch().trim() + "%')";
        }
        if (inputBean.getContactsSearch() != null && !inputBean.getContactsSearch().isEmpty()) {
            where += " and lower(u.contacts) like lower('%" + inputBean.getContactsSearch().trim() + "%')";
        }
        if (inputBean.getOpeningHsMonFriSearch() != null && !inputBean.getOpeningHsMonFriSearch().isEmpty()) {
            where += " and lower(u.openingHsMonFri) like lower('%" + inputBean.getOpeningHsMonFriSearch().trim() + "%')";
        }
        if (inputBean.getOpeningHsSatSearch() != null && !inputBean.getOpeningHsSatSearch().isEmpty()) {
            where += " and lower(u.openingHsSat) like lower('%" + inputBean.getOpeningHsSatSearch().trim() + "%')";
        }
        if (inputBean.getOpeningHsSunHolSearch() != null && !inputBean.getOpeningHsSunHolSearch().isEmpty()) {
            where += " and lower(u.openingHsSunHol) like lower('%" + inputBean.getOpeningHsSunHolSearch().trim() + "%')";
        }
        if (inputBean.getLongitudeSearch() != null && !inputBean.getLongitudeSearch().isEmpty()) {
            where += " and lower(u.longitude) like lower('%" + inputBean.getLongitudeSearch().trim() + "%')";
        }
        if (inputBean.getLatitudeSearch() != null && !inputBean.getLatitudeSearch().isEmpty()) {
            where += " and lower(u.latitude) like lower('%" + inputBean.getLatitudeSearch().trim() + "%')";
        }
        if (inputBean.getLocationTagSearch() != null && !inputBean.getLocationTagSearch().isEmpty()) {
            where += " and lower(u.locationTag) like lower('%" + inputBean.getLocationTagSearch().trim() + "%')";
        }
        if (inputBean.getLanguageSearch() != null && !inputBean.getLanguageSearch().isEmpty()) {
            where += " and lower(u.language) like lower('%" + inputBean.getLanguageSearch().trim() + "%')";
        }

        return where;
    }

    public List<MBankLocatorPendBean> getPendingMBankLocatorList(MBankLocatorInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<MBankLocatorPendBean> dataList = new ArrayList<MBankLocatorPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.M_BANK_LOCATOR_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.M_BANK_LOCATOR_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    MBankLocatorPendBean mBankLocator = new MBankLocatorPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        mBankLocator.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        mBankLocator.setId("--");
                    }
                    try {
                        mBankLocator.setOperation(pTask.getTask().getDescription().toString());
                        mBankLocator.setOperationcode(pTask.getTask().getTaskcode().toString());
                    } catch (NullPointerException npe) {
                        mBankLocator.setOperation("--");
                        mBankLocator.setOperationcode("--");
                    }

//                    String[] penArray = null;
//                    if (pTask.getFields() != null) {
//                        penArray = pTask.getFields().split("\\|");
//                    }
//
//                    try {
//                        mBankLocator.setLocatorid(penArray[0]);
//                    } catch (NullPointerException npe) {
//                        mBankLocator.setLocatorid("--");
//                    } catch (Exception ex) {
//                        mBankLocator.setLocatorid("--");
//                    }
                    try {
                        mBankLocator.setLocatorid(pTask.getPKey().toString());
                    } catch (Exception ex) {
                        mBankLocator.setLocatorid("--");
                    }
                    try {
                        mBankLocator.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        mBankLocator.setFields("--");
                    } catch (Exception ex) {
                        mBankLocator.setFields("--");
                    }
                    try {
                        mBankLocator.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mBankLocator.setStatus("--");
                    }
                    try {
                        mBankLocator.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        mBankLocator.setCreatetime("--");
                    }
                    try {
                        mBankLocator.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        mBankLocator.setCreateduser("--");
                    } catch (Exception ex) {
                        mBankLocator.setCreateduser("--");
                    }

                    mBankLocator.setFullCount(count);

                    dataList.add(mBankLocator);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertMBankLocator(MBankLocatorInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            String sql = "from MBankLocator as u where u.locatorcode=:locatorcode ";
            Query query = session.createQuery(sql).setString("locatorcode", inputBean.getLocatorcode());
            if (query.list().isEmpty()) {
                String sql2 = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query2 = session.createQuery(sql2)
                        .setString("PKey", inputBean.getLocatorcode())
                        .setString("pagecode", audit.getPagecode());
                if (query2.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getLocatorcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.M_BANK_LOCATOR_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String uploadMBankLocator(MBankLocatorInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        FileInputStream fileInputStream = null;

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

//            if ((MBankLocator) session.get(MBankLocator.class, inputBean.getLocatorid().trim()) == null) {
//                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
//                Query query = session.createQuery(sql)
//                        .setString("PKey", inputBean.getLocatorid())
//                        .setString("pagecode", audit.getPagecode());
//                if (query.list().isEmpty()) {
            txn = session.beginTransaction();

            Pendingtask pendingtask = new Pendingtask();

//                    pendingtask.setPKey(inputBean.getLocatorid().trim());
//            pendingtask.setFields(audit.getNewvalue());
            Task task = new Task();
            task.setTaskcode(TaskVarList.UPLOAD_TASK);
            pendingtask.setTask(task);

            Status st = new Status();
            st.setStatuscode(CommonVarList.STATUS_PENDING);
            pendingtask.setStatus(st);

            Page page = (Page) session.get(Page.class, PageVarList.M_BANK_LOCATOR_PAGE);
            pendingtask.setPage(page);

            pendingtask.setInputterbranch(sysUser.getBranch());

            try {
                if (inputBean.getConXL().length() != 0) {
                    File csvFile = inputBean.getConXL();
                    byte[] bCsvFile = new byte[(int) csvFile.length()];
                    try {
                        fileInputStream = new FileInputStream(csvFile);
                        fileInputStream.read(bCsvFile);
                        fileInputStream.close();
                        Blob blob = new javax.sql.rowset.serial.SerialBlob(bCsvFile);
                        pendingtask.setInputfile(blob);
                    } catch (Exception ex) {

                    }
                }
            } catch (NullPointerException ex) {

            } finally {

                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            }

            pendingtask.setCreatedtime(sysDate);
            pendingtask.setLastupdatedtime(sysDate);
            pendingtask.setCreateduser(audit.getLastupdateduser());

            audit.setCreatetime(sysDate);
            audit.setLastupdatedtime(sysDate);
            audit.setLastupdateduser(audit.getLastupdateduser());

            session.save(audit);
            session.save(pendingtask);
            txn.commit();
//                } else {
//                    message = "pending available";
//                }
//            } else {
//                message = MessageVarList.COMMON_ALREADY_EXISTS;
//            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteMBankLocator(MBankLocatorInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            MBankLocator u = (MBankLocator) session.get(MBankLocator.class, new BigDecimal(inputBean.getLocatorid().trim()));
            if (u != null) {
                String status = "";
                if (u.getStatus() != null) {
                    status = u.getStatus();
                }
                String address = "";
                String contacts = "";
                String openingHsMonFri = "";
                String fax = "";
                String atmOnLocation = "";
                String crmOnLocation = "";
                String pawningOnLocation = "";
                String safeDepositLockers = "";
                String leasingDesk365 = "";
                String prvCentre = "";
                String branchlessBanking = "";
                String holidayBanking = "";

                String postalCode = "";
                String openingHsSat = "";
                String openingHsSunHol = "";
                String locationTag = "";

                if (u.getAddress() != null && !u.getAddress().isEmpty()) {
                    address = u.getAddress();
                }
                if (u.getContacts() != null && !u.getContacts().isEmpty()) {
                    contacts = u.getContacts();
                }
                if (u.getOpeningHsMonFri() != null && !u.getOpeningHsMonFri().isEmpty()) {
                    openingHsMonFri = u.getOpeningHsMonFri();
                }
                if (u.getPostalCode() != null) {
                    postalCode = u.getPostalCode();
                }
                if (u.getOpeningHsSat() != null) {
                    openingHsSat = u.getOpeningHsSat();
                }
                if (u.getOpeningHsSunHol() != null) {
                    openingHsSunHol = u.getOpeningHsSunHol();
                }
                if (u.getLocationTag() != null) {
                    locationTag = u.getLocationTag();
                }
                if (u.getFax() != null) {
                    fax = u.getFax();
                }
                if (u.getAtmOnLocation() != null) {
                    atmOnLocation = u.getAtmOnLocation();
                }
                if (u.getCrmOnLocation() != null) {
                    crmOnLocation = u.getCrmOnLocation();
                }
                if (u.getPawningOnLocation() != null) {
                    pawningOnLocation = u.getPawningOnLocation();
                }
                if (u.getSafeDepositLockers() != null) {
                    safeDepositLockers = u.getSafeDepositLockers();
                }
                if (u.getLeasingDesk365() != null) {
                    leasingDesk365 = u.getLeasingDesk365();
                }
                if (u.getPrvCentre() != null) {
                    prvCentre = u.getPrvCentre();
                }
                if (u.getBranchlessBanking() != null) {
                    branchlessBanking = u.getBranchlessBanking();
                }
                if (u.getHolidayBanking() != null) {
                    holidayBanking = u.getHolidayBanking();
                }

                audit.setNewvalue(u.getLocatorid().toString() + "|" + u.getLocatorcode().toString() + "|" + status + "|" + u.getChannelType().getChannelType() + "|" + u.getCountryCode()
                        + "|" + u.getCountry() + "|" + u.getMasterRegion() + "|" + u.getSubRegion()
                        + "|" + u.getName() + "|" + address + "|" + postalCode + "|" + contacts + "|" + fax + "|" + openingHsMonFri
                        + "|" + openingHsSat + "|" + openingHsSunHol + "|" + holidayBanking + "|" + u.getLongitude().toString() + "|" + u.getLatitude().toString() + "|" + locationTag + "|" + u.getLanguage()
                        + "|" + atmOnLocation + "|" + crmOnLocation + "|" + pawningOnLocation + "|" + safeDepositLockers + "|" + leasingDesk365 + "|" + prvCentre + "|" + branchlessBanking
                //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                );

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", u.getLocatorcode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(u.getLocatorcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.DELETE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.M_BANK_LOCATOR_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public MBankLocator findMBankLocatorById(String locatorid) throws Exception {
        MBankLocator mBankLocator = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from MBankLocator as u where u.locatorid=:locatorid";
            Query query = session.createQuery(sql).setBigDecimal("locatorid", new BigDecimal(locatorid));
            mBankLocator = (MBankLocator) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return mBankLocator;

    }

    public String updateMBankLocator(MBankLocatorInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getLocatorcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                MBankLocator u = (MBankLocator) session.get(MBankLocator.class, new BigDecimal(inputBean.getLocatorid().trim()));
                if (u != null) {

                    String status = "";
                    if (u.getStatus() != null) {
                        status = u.getStatus();
                    }
//                    String channelSubType = "";
//                    String region = "";
                    String postalCode = "";
                    String openingHsSat = "";
                    String openingHsSunHol = "";
                    String locationTag = "";

                    String fax = "";
                    String atmOnLocation = "";
                    String crmOnLocation = "";
                    String pawningOnLocation = "";
                    String safeDepositLockers = "";
                    String leasingDesk365 = "";
                    String prvCentre = "";
                    String branchlessBanking = "";
                    String holidayBanking = "";

                    if (u.getPostalCode() != null) {
                        postalCode = u.getPostalCode();
                    }
                    if (u.getOpeningHsSat() != null) {
                        openingHsSat = u.getOpeningHsSat();
                    }
                    if (u.getOpeningHsSunHol() != null) {
                        openingHsSunHol = u.getOpeningHsSunHol();
                    }
                    if (u.getLocationTag() != null) {
                        locationTag = u.getLocationTag();
                    }
                    if (u.getFax() != null) {
                        fax = u.getFax();
                    }
                    if (u.getAtmOnLocation() != null) {
                        atmOnLocation = u.getAtmOnLocation();
                    }
                    if (u.getCrmOnLocation() != null) {
                        crmOnLocation = u.getCrmOnLocation();
                    }
                    if (u.getPawningOnLocation() != null) {
                        pawningOnLocation = u.getPawningOnLocation();
                    }
                    if (u.getSafeDepositLockers() != null) {
                        safeDepositLockers = u.getSafeDepositLockers();
                    }
                    if (u.getLeasingDesk365() != null) {
                        leasingDesk365 = u.getLeasingDesk365();
                    }
                    if (u.getPrvCentre() != null) {
                        prvCentre = u.getPrvCentre();
                    }
                    if (u.getBranchlessBanking() != null) {
                        branchlessBanking = u.getBranchlessBanking();
                    }
                    if (u.getHolidayBanking() != null) {
                        holidayBanking = u.getHolidayBanking();
                    }

                    audit.setOldvalue(u.getLocatorid().toString() + "|" + u.getLocatorcode().toString() + "|" + status + "|" + u.getChannelType().getChannelType() + "|" + u.getCountryCode()
                            + "|" + u.getCountry() + "|" + u.getMasterRegion() + "|" + u.getSubRegion()
                            + "|" + u.getName() + "|" + u.getAddress() + "|" + postalCode + "|" + u.getContacts() + "|" + fax + "|" + u.getOpeningHsMonFri()
                            + "|" + openingHsSat + "|" + openingHsSunHol + "|" + holidayBanking + "|" + u.getLongitude().toString() + "|" + u.getLatitude().toString() + "|" + locationTag + "|" + u.getLanguage()
                            + "|" + atmOnLocation + "|" + crmOnLocation + "|" + pawningOnLocation + "|" + safeDepositLockers + "|" + leasingDesk365 + "|" + prvCentre + "|" + branchlessBanking
                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                    );

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getLocatorcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.M_BANK_LOCATOR_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmMBankLocator(MBankLocatorInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());

                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
                    String sql2 = "from MBankLocator as u where u.locatorcode=:locatorcode ";
                    Query query2 = session.createQuery(sql2).setString("locatorcode", penArray[0].trim());
                    if (query2.list().isEmpty()) {
                        MBankLocator mbankLocator = new MBankLocator();

                        mbankLocator.setLocatorcode(penArray[0]);
                        mbankLocator.setStatus(penArray[1]);

                        ChannelType channelType = (ChannelType) session.get(ChannelType.class, penArray[2]);
                        mbankLocator.setChannelType(channelType);

                        mbankLocator.setCountryCode(penArray[3]);
                        mbankLocator.setCountry(penArray[4]);
                        mbankLocator.setMasterRegion(penArray[5]);
                        mbankLocator.setSubRegion(penArray[6]);
                        mbankLocator.setName(penArray[7]);
                        mbankLocator.setAddress(penArray[8]);
                        mbankLocator.setPostalCode(penArray[9]);
                        mbankLocator.setContacts(penArray[10]);
                        mbankLocator.setFax(penArray[11]);
                        mbankLocator.setOpeningHsMonFri(penArray[12]);
                        mbankLocator.setOpeningHsSat(penArray[13]);
                        mbankLocator.setOpeningHsSunHol(penArray[14]);
                        mbankLocator.setHolidayBanking(penArray[15]);
                        mbankLocator.setLongitude(new BigDecimal(penArray[16]));
                        mbankLocator.setLatitude(new BigDecimal(penArray[17]));
                        mbankLocator.setLocationTag(penArray[18]);
                        mbankLocator.setLanguage(penArray[19]);
                        try {//empty value in insert boundry out of index
                            mbankLocator.setAtmOnLocation(penArray[20]);
                        } catch (Exception e) {
                            mbankLocator.setAtmOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setCrmOnLocation(penArray[21]);
                        } catch (Exception e) {
                            mbankLocator.setCrmOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setPawningOnLocation(penArray[22]);
                        } catch (Exception e) {
                            mbankLocator.setPawningOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setSafeDepositLockers(penArray[23]);
                        } catch (Exception e) {
                            mbankLocator.setSafeDepositLockers("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setLeasingDesk365(penArray[24]);
                        } catch (Exception e) {
                            mbankLocator.setLeasingDesk365("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setPrvCentre(penArray[25]);
                        } catch (Exception e) {
                            mbankLocator.setPrvCentre("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setBranchlessBanking(penArray[26]);
                        } catch (Exception e) {
                            mbankLocator.setBranchlessBanking("");
                        }

                        mbankLocator.setCreatedtime(timestampDate);
                        mbankLocator.setLastupdatedtime(timestampDate);
                        mbankLocator.setMaker(pentask.getCreateduser());
                        mbankLocator.setChecker(audit.getLastupdateduser());

                        session.save(mbankLocator);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on mobile bank locator(Locator code:  " + mbankLocator.getLocatorcode().toString() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        MBankLocator mbankLocator = (MBankLocator) query2.list().get(0);

                        mbankLocator.setStatus(penArray[1]);

                        ChannelType channelType = (ChannelType) session.get(ChannelType.class, penArray[2]);
                        mbankLocator.setChannelType(channelType);

                        mbankLocator.setCountryCode(penArray[3]);
                        mbankLocator.setCountry(penArray[4]);
                        mbankLocator.setMasterRegion(penArray[5]);
                        mbankLocator.setSubRegion(penArray[6]);
                        mbankLocator.setName(penArray[7]);
                        mbankLocator.setAddress(penArray[8]);
                        mbankLocator.setPostalCode(penArray[9]);
                        mbankLocator.setContacts(penArray[10]);
                        mbankLocator.setFax(penArray[11]);
                        mbankLocator.setOpeningHsMonFri(penArray[12]);
                        mbankLocator.setOpeningHsSat(penArray[13]);
                        mbankLocator.setOpeningHsSunHol(penArray[14]);
                        mbankLocator.setHolidayBanking(penArray[15]);
                        mbankLocator.setLongitude(new BigDecimal(penArray[16]));
                        mbankLocator.setLatitude(new BigDecimal(penArray[17]));
                        mbankLocator.setLocationTag(penArray[18]);
                        mbankLocator.setLanguage(penArray[19]);
                        try {//empty value in insert boundry out of index
                            mbankLocator.setAtmOnLocation(penArray[20]);
                        } catch (Exception e) {
                            mbankLocator.setAtmOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setCrmOnLocation(penArray[21]);
                        } catch (Exception e) {
                            mbankLocator.setCrmOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setPawningOnLocation(penArray[22]);
                        } catch (Exception e) {
                            mbankLocator.setPawningOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setSafeDepositLockers(penArray[23]);
                        } catch (Exception e) {
                            mbankLocator.setSafeDepositLockers("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setLeasingDesk365(penArray[24]);
                        } catch (Exception e) {
                            mbankLocator.setLeasingDesk365("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setPrvCentre(penArray[25]);
                        } catch (Exception e) {
                            mbankLocator.setPrvCentre("");
                        }
                        try {//empty value in insert boundry out of index
                            mbankLocator.setBranchlessBanking(penArray[26]);
                        } catch (Exception e) {
                            mbankLocator.setBranchlessBanking("");
                        }

                        mbankLocator.setCreatedtime(timestampDate);
                        mbankLocator.setLastupdatedtime(timestampDate);
                        mbankLocator.setMaker(pentask.getCreateduser());
                        mbankLocator.setChecker(audit.getLastupdateduser());

                        session.update(mbankLocator);
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    MBankLocator u = (MBankLocator) session.get(MBankLocator.class, new BigDecimal(penArray[0].trim()));

                    if (u != null) {
                        //------------------------audit old value------------------------------
                        String address = "";
                        String postalCode = "";
                        String contacts = "";
                        String fax = "";
                        String openingHsMonFri = "";
                        String openingHsSat = "";
                        String openingHsSunHol = "";
                        String holidayBanking = "";
                        String locationTag = "";
                        String atmOnLocation = "";
                        String crmOnLocation = "";
                        String pawningOnLocation = "";
                        String safeDepositLockers = "";
                        String leasingDesk365 = "";
                        String prvCentre = "";
                        String branchlessBanking = "";

                        if (u.getAddress() != null && !u.getAddress().isEmpty()) {
                            address = u.getAddress();
                        }
                        if (u.getPostalCode() != null && !u.getPostalCode().isEmpty()) {
                            postalCode = u.getPostalCode();
                        }
                        if (u.getContacts() != null && !u.getContacts().isEmpty()) {
                            contacts = u.getContacts();
                        }
                        if (u.getFax() != null && !u.getFax().isEmpty()) {
                            fax = u.getFax();
                        }
                        if (u.getOpeningHsMonFri() != null && !u.getOpeningHsMonFri().isEmpty()) {
                            openingHsMonFri = u.getOpeningHsMonFri();
                        }
                        if (u.getOpeningHsSat() != null && !u.getOpeningHsSat().isEmpty()) {
                            openingHsSat = u.getOpeningHsSat();
                        }
                        if (u.getOpeningHsSunHol() != null && !u.getOpeningHsSunHol().isEmpty()) {
                            openingHsSunHol = u.getOpeningHsSunHol();
                        }
                        if (u.getHolidayBanking() != null && !u.getHolidayBanking().isEmpty()) {
                            holidayBanking = u.getHolidayBanking();
                        }
                        if (u.getLocationTag() != null && !u.getLocationTag().isEmpty()) {
                            locationTag = u.getLocationTag();
                        }
                        if (u.getAtmOnLocation() != null && !u.getAtmOnLocation().isEmpty()) {
                            atmOnLocation = u.getAtmOnLocation();
                        }
                        if (u.getCrmOnLocation() != null && !u.getCrmOnLocation().isEmpty()) {
                            crmOnLocation = u.getCrmOnLocation();
                        }
                        if (u.getPawningOnLocation() != null && !u.getPawningOnLocation().isEmpty()) {
                            pawningOnLocation = u.getPawningOnLocation();
                        }
                        if (u.getSafeDepositLockers() != null && !u.getSafeDepositLockers().isEmpty()) {
                            safeDepositLockers = u.getSafeDepositLockers();
                        }
                        if (u.getLeasingDesk365() != null && !u.getLeasingDesk365().isEmpty()) {
                            leasingDesk365 = u.getLeasingDesk365();
                        }
                        if (u.getLeasingDesk365() != null && !u.getLeasingDesk365().isEmpty()) {
                            leasingDesk365 = u.getLeasingDesk365();
                        }
                        if (u.getPrvCentre() != null && !u.getPrvCentre().isEmpty()) {
                            prvCentre = u.getPrvCentre();
                        }
                        if (u.getBranchlessBanking() != null && !u.getBranchlessBanking().isEmpty()) {
                            branchlessBanking = u.getBranchlessBanking();
                        }

                        String oldVal = u.getLocatorid() + "|" + u.getLocatorcode() + "|" + u.getStatus() + "|" + u.getChannelType().getChannelType() + "|" + u.getCountryCode()
                                + "|" + u.getCountry() + "|" + u.getMasterRegion() + "|" + u.getSubRegion()
                                + "|" + u.getName() + "|" + address + "|" + postalCode + "|" + contacts + "|" + fax + "|" + openingHsMonFri
                                + "|" + openingHsSat + "|" + openingHsSunHol + "|" + holidayBanking + "|" + u.getLongitude().toString() + "|" + u.getLatitude().toString() + "|" + locationTag + "|" + u.getLanguage()
                                + "|" + atmOnLocation + "|" + crmOnLocation + "|" + pawningOnLocation + "|" + safeDepositLockers + "|" + leasingDesk365 + "|" + prvCentre + "|" + branchlessBanking;

                        u.setLocatorcode(penArray[1]);
                        u.setStatus(penArray[2]);

                        ChannelType channelType = (ChannelType) session.get(ChannelType.class, penArray[3]);
                        u.setChannelType(channelType);

                        u.setCountryCode(penArray[4]);
                        u.setCountry(penArray[5]);
                        u.setMasterRegion(penArray[6]);
                        u.setSubRegion(penArray[7]);
                        u.setName(penArray[8]);
                        u.setAddress(penArray[9]);
                        u.setPostalCode(penArray[10]);
                        u.setContacts(penArray[11]);
                        u.setFax(penArray[12]);
                        u.setOpeningHsMonFri(penArray[13]);
                        u.setOpeningHsSat(penArray[14]);
                        u.setOpeningHsSunHol(penArray[15]);
                        u.setHolidayBanking(penArray[16]);
                        u.setLongitude(new BigDecimal(penArray[17]));
                        u.setLatitude(new BigDecimal(penArray[18]));
                        u.setLocationTag(penArray[19]);
                        u.setLanguage(penArray[20]);
                        try {//empty value in insert boundry out of index
                            u.setAtmOnLocation(penArray[21]);
                        } catch (Exception e) {
                            u.setAtmOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            u.setCrmOnLocation(penArray[22]);
                        } catch (Exception e) {
                            u.setCrmOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            u.setPawningOnLocation(penArray[23]);
                        } catch (Exception e) {
                            u.setPawningOnLocation("");
                        }
                        try {//empty value in insert boundry out of index
                            u.setSafeDepositLockers(penArray[24]);
                        } catch (Exception e) {
                            u.setSafeDepositLockers("");
                        }
                        try {//empty value in insert boundry out of index
                            u.setLeasingDesk365(penArray[25]);
                        } catch (Exception e) {
                            u.setLeasingDesk365("");
                        }
                        try {//empty value in insert boundry out of index
                            u.setPrvCentre(penArray[26]);
                        } catch (Exception e) {
                            u.setPrvCentre("");
                        }
                        try {//empty value in insert boundry out of index
                            u.setBranchlessBanking(penArray[27]);
                        } catch (Exception e) {
                            u.setBranchlessBanking("");
                        }

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        session.update(u);

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on mobile bank locator (Locator code: " + penArray[1] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPLOAD_TASK)) {
//                    String locatorCodes = "";
                    Blob csvBlob = pentask.getInputfile();
                    if (csvBlob != null) {
                        isr = new InputStreamReader(csvBlob.getBinaryStream());
                        br = new BufferedReader(isr);

                        String[] parts = new String[0];
                        int countrecord = 1;
                        int succesrec = 0;
                        String thisLine = null;
                        boolean getline = false;
                        String token = "";
                        while ((thisLine = br.readLine()) != null) {
                            if (thisLine.trim().equals("")) {
                                continue;
                            } else {
                                if (getline) {
//                              token = content.nextLine();
                                    token = thisLine;

//                                    System.err.println(token);
                                    try {
                                        parts = token.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", 26);
                                        inputBean.setLocatorcode(Common.removeCsvDoubleQuotation(parts[0]));
                                        inputBean.setChannelType(Common.removeCsvDoubleQuotation(parts[1]));
                                        inputBean.setCountryCode(Common.removeCsvDoubleQuotation(parts[2]));
                                        inputBean.setCountry(Common.removeCsvDoubleQuotation(parts[3]));
                                        inputBean.setMasterRegion(Common.removeCsvDoubleQuotation(parts[4]));
                                        inputBean.setSubRegion(Common.removeCsvDoubleQuotation(parts[5]));
                                        inputBean.setName(Common.removeCsvDoubleQuotation(parts[6]));
                                        inputBean.setPostalCode(Common.removeCsvDoubleQuotation(parts[7]));
                                        inputBean.setContacts(Common.removeCsvDoubleQuotation(parts[8]));
                                        inputBean.setFax(Common.removeCsvDoubleQuotation(parts[9]));
                                        inputBean.setOpeningHsMonFri(Common.removeCsvDoubleQuotation(parts[10]));
                                        inputBean.setOpeningHsSat(Common.removeCsvDoubleQuotation(parts[11]));
                                        inputBean.setOpeningHsSunHol(Common.removeCsvDoubleQuotation(parts[12]));
                                        inputBean.setHolidayBanking(Common.removeCsvDoubleQuotation(parts[13]));
                                        inputBean.setLatitude(Common.removeCsvDoubleQuotation(parts[14]));
                                        inputBean.setLongitude(Common.removeCsvDoubleQuotation(parts[15]));
                                        inputBean.setLocationTag(Common.removeCsvDoubleQuotation(parts[16]));
                                        inputBean.setLanguage(Common.removeCsvDoubleQuotation(parts[17]));
                                        inputBean.setAtmOnLocation(Common.removeCsvDoubleQuotation(parts[18]));
                                        inputBean.setCrmOnLocation(Common.removeCsvDoubleQuotation(parts[19]));
                                        inputBean.setPawningOnLocation(Common.removeCsvDoubleQuotation(parts[20]));
                                        inputBean.setSafeDepositLockers(Common.removeCsvDoubleQuotation(parts[21]));
                                        inputBean.setLeasingDesk365(Common.removeCsvDoubleQuotation(parts[22]));
                                        inputBean.setPrvCentre(Common.removeCsvDoubleQuotation(parts[23]));
                                        inputBean.setBranchlessBanking(Common.removeCsvDoubleQuotation(parts[24]));
                                        inputBean.setAddress(Common.removeCsvDoubleQuotation(parts[25]));

                                        inputBean.setStatus(CommonVarList.STATUS_ACTIVE);

                                    } catch (Exception ee) {
                                        System.out.println("----" + ee.getMessage());
                                        message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                                        break;
                                    }
                                    countrecord++;
                                    if (parts.length >= 26 && message.isEmpty()) {
//                                        message = this.validateInputsForCSV(inputBean);
//                                        if (message.isEmpty()) {
//                                            message = this.validateUpload(inputBean, session);
//                                            if (message.isEmpty()) {
                                        String sql2 = "from MBankLocator as u where u.locatorcode=:locatorcode ";
                                        Query query2 = session.createQuery(sql2).setString("locatorcode", inputBean.getLocatorcode());
                                        if (query2.list().isEmpty()) {
                                            MBankLocator mbankLocator = new MBankLocator();

                                            mbankLocator.setLocatorcode(inputBean.getLocatorcode());
                                            mbankLocator.setStatus(inputBean.getStatus());

                                            ChannelType channelType = (ChannelType) session.get(ChannelType.class, inputBean.getChannelType());
                                            mbankLocator.setChannelType(channelType);

                                            mbankLocator.setCountryCode(inputBean.getCountryCode());
                                            mbankLocator.setCountry(inputBean.getCountry());
                                            mbankLocator.setMasterRegion(inputBean.getMasterRegion());
                                            mbankLocator.setSubRegion(inputBean.getSubRegion());
                                            mbankLocator.setName(inputBean.getName());
                                            mbankLocator.setAddress(inputBean.getAddress());
                                            mbankLocator.setPostalCode(inputBean.getPostalCode());
                                            mbankLocator.setContacts(inputBean.getContacts());
                                            mbankLocator.setFax(inputBean.getFax());
                                            mbankLocator.setOpeningHsMonFri(inputBean.getOpeningHsMonFri());
                                            mbankLocator.setOpeningHsSat(inputBean.getOpeningHsSat());
                                            mbankLocator.setOpeningHsSunHol(inputBean.getOpeningHsSunHol());
                                            mbankLocator.setHolidayBanking(inputBean.getHolidayBanking());
                                            mbankLocator.setLongitude(new BigDecimal(inputBean.getLongitude()));
                                            mbankLocator.setLatitude(new BigDecimal(inputBean.getLatitude()));
                                            mbankLocator.setLocationTag(inputBean.getLocationTag());
                                            mbankLocator.setLanguage(inputBean.getLanguage());
                                            try {//empty value in insert boundry out of index
                                                mbankLocator.setAtmOnLocation(inputBean.getAtmOnLocation());
                                            } catch (Exception e) {
                                                mbankLocator.setAtmOnLocation("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                mbankLocator.setCrmOnLocation(inputBean.getCrmOnLocation());
                                            } catch (Exception e) {
                                                mbankLocator.setCrmOnLocation("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                mbankLocator.setPawningOnLocation(inputBean.getPawningOnLocation());
                                            } catch (Exception e) {
                                                mbankLocator.setPawningOnLocation("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                mbankLocator.setSafeDepositLockers(inputBean.getSafeDepositLockers());
                                            } catch (Exception e) {
                                                mbankLocator.setSafeDepositLockers("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                mbankLocator.setLeasingDesk365(inputBean.getLeasingDesk365());
                                            } catch (Exception e) {
                                                mbankLocator.setLeasingDesk365("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                mbankLocator.setPrvCentre(inputBean.getPrvCentre());
                                            } catch (Exception e) {
                                                mbankLocator.setPrvCentre("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                mbankLocator.setBranchlessBanking(inputBean.getBranchlessBanking());
                                            } catch (Exception e) {
                                                mbankLocator.setBranchlessBanking("");
                                            }

                                            mbankLocator.setCreatedtime(timestampDate);
                                            mbankLocator.setLastupdatedtime(timestampDate);
                                            mbankLocator.setMaker(pentask.getCreateduser());
                                            mbankLocator.setChecker(audit.getLastupdateduser());

                                            session.save(mbankLocator);
//
                                        } else {
                                            MBankLocator u = (MBankLocator) query2.list().get(0);

                                            ChannelType channelType = (ChannelType) session.get(ChannelType.class, inputBean.getChannelType());
                                            u.setChannelType(channelType);

                                            u.setCountryCode(inputBean.getCountryCode());
                                            u.setCountry(inputBean.getCountry());
                                            u.setMasterRegion(inputBean.getMasterRegion());
                                            u.setSubRegion(inputBean.getSubRegion());
                                            u.setName(inputBean.getName());
                                            u.setAddress(inputBean.getAddress());
                                            u.setPostalCode(inputBean.getPostalCode());
                                            u.setContacts(inputBean.getContacts());
                                            u.setFax(inputBean.getFax());
                                            u.setOpeningHsMonFri(inputBean.getOpeningHsMonFri());
                                            u.setOpeningHsSat(inputBean.getOpeningHsSat());
                                            u.setOpeningHsSunHol(inputBean.getOpeningHsSunHol());
                                            u.setHolidayBanking(inputBean.getHolidayBanking());
                                            u.setLongitude(new BigDecimal(inputBean.getLongitude()));
                                            u.setLatitude(new BigDecimal(inputBean.getLatitude()));
                                            u.setLocationTag(inputBean.getLocationTag());
                                            u.setLanguage(inputBean.getLanguage());
                                            try {//empty value in insert boundry out of index
                                                u.setAtmOnLocation(inputBean.getAtmOnLocation());
                                            } catch (Exception e) {
                                                u.setAtmOnLocation("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                u.setCrmOnLocation(inputBean.getCrmOnLocation());
                                            } catch (Exception e) {
                                                u.setCrmOnLocation("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                u.setPawningOnLocation(inputBean.getPawningOnLocation());
                                            } catch (Exception e) {
                                                u.setPawningOnLocation("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                u.setSafeDepositLockers(inputBean.getSafeDepositLockers());
                                            } catch (Exception e) {
                                                u.setSafeDepositLockers("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                u.setLeasingDesk365(inputBean.getLeasingDesk365());
                                            } catch (Exception e) {
                                                u.setLeasingDesk365("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                u.setPrvCentre(inputBean.getPrvCentre());
                                            } catch (Exception e) {
                                                u.setPrvCentre("");
                                            }
                                            try {//empty value in insert boundry out of index
                                                u.setBranchlessBanking(inputBean.getBranchlessBanking());
                                            } catch (Exception e) {
                                                u.setBranchlessBanking("");
                                            }

                                            u.setMaker(pentask.getCreateduser());
                                            u.setChecker(audit.getLastupdateduser());
                                            u.setLastupdatedtime(timestampDate);

                                            session.update(u);
//
                                        }
//                                        locatorCodes = locatorCodes + inputBean.getLocatorcode().trim() + ",";
                                        succesrec++;
//                                            } else {
//                                                message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                                break;
//                                            }
//                                        } else {
//                                            message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                            break;
//                                        }

                                    } else {
                                        message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                                    }
                                } else {
                                    getline = true;
//                            content.nextLine();
                                }
                            }
                        }
//                        if (!locatorCodes.isEmpty() && locatorCodes.length() > 0) {
//                            locatorCodes = locatorCodes.substring(0, locatorCodes.length() - 1);
//                        }
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on mobile bank locator (Records count :" + succesrec + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    MBankLocator u = (MBankLocator) session.get(MBankLocator.class, new BigDecimal(penArray[0]));
                    if (u != null) {
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on mobile bank locator (Locator code: " + penArray[1] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectMBankLocator(MBankLocatorInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                String[] penArray = null;
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                    penArray = u.getFields().split("\\|");
                }

                String locatorCode = u.getPKey();
                if (locatorCode != null && !locatorCode.isEmpty()) {
                    if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                        MBankLocator mBankLocator = (MBankLocator) session.get(MBankLocator.class, new BigDecimal(penArray[0].trim()));

                        if (mBankLocator != null) {
                            //------------------------audit old value------------------------------
                            String address = "";
                            String postalCode = "";
                            String contacts = "";
                            String fax = "";
                            String openingHsMonFri = "";
                            String openingHsSat = "";
                            String openingHsSunHol = "";
                            String holidayBanking = "";
                            String locationTag = "";
                            String atmOnLocation = "";
                            String crmOnLocation = "";
                            String pawningOnLocation = "";
                            String safeDepositLockers = "";
                            String leasingDesk365 = "";
                            String prvCentre = "";
                            String branchlessBanking = "";

                            if (mBankLocator.getAddress() != null && !mBankLocator.getAddress().isEmpty()) {
                                address = mBankLocator.getAddress();
                            }
                            if (mBankLocator.getPostalCode() != null && !mBankLocator.getPostalCode().isEmpty()) {
                                postalCode = mBankLocator.getPostalCode();
                            }
                            if (mBankLocator.getContacts() != null && !mBankLocator.getContacts().isEmpty()) {
                                contacts = mBankLocator.getContacts();
                            }
                            if (mBankLocator.getFax() != null && !mBankLocator.getFax().isEmpty()) {
                                fax = mBankLocator.getFax();
                            }
                            if (mBankLocator.getOpeningHsMonFri() != null && !mBankLocator.getOpeningHsMonFri().isEmpty()) {
                                openingHsMonFri = mBankLocator.getOpeningHsMonFri();
                            }
                            if (mBankLocator.getOpeningHsSat() != null && !mBankLocator.getOpeningHsSat().isEmpty()) {
                                openingHsSat = mBankLocator.getOpeningHsSat();
                            }
                            if (mBankLocator.getOpeningHsSunHol() != null && !mBankLocator.getOpeningHsSunHol().isEmpty()) {
                                openingHsSunHol = mBankLocator.getOpeningHsSunHol();
                            }
                            if (mBankLocator.getHolidayBanking() != null && !mBankLocator.getHolidayBanking().isEmpty()) {
                                holidayBanking = mBankLocator.getHolidayBanking();
                            }
                            if (mBankLocator.getLocationTag() != null && !mBankLocator.getLocationTag().isEmpty()) {
                                locationTag = mBankLocator.getLocationTag();
                            }
                            if (mBankLocator.getAtmOnLocation() != null && !mBankLocator.getAtmOnLocation().isEmpty()) {
                                atmOnLocation = mBankLocator.getAtmOnLocation();
                            }
                            if (mBankLocator.getCrmOnLocation() != null && !mBankLocator.getCrmOnLocation().isEmpty()) {
                                crmOnLocation = mBankLocator.getCrmOnLocation();
                            }
                            if (mBankLocator.getPawningOnLocation() != null && !mBankLocator.getPawningOnLocation().isEmpty()) {
                                pawningOnLocation = mBankLocator.getPawningOnLocation();
                            }
                            if (mBankLocator.getSafeDepositLockers() != null && !mBankLocator.getSafeDepositLockers().isEmpty()) {
                                safeDepositLockers = mBankLocator.getSafeDepositLockers();
                            }
                            if (mBankLocator.getLeasingDesk365() != null && !mBankLocator.getLeasingDesk365().isEmpty()) {
                                leasingDesk365 = mBankLocator.getLeasingDesk365();
                            }
                            if (mBankLocator.getLeasingDesk365() != null && !mBankLocator.getLeasingDesk365().isEmpty()) {
                                leasingDesk365 = mBankLocator.getLeasingDesk365();
                            }
                            if (mBankLocator.getPrvCentre() != null && !mBankLocator.getPrvCentre().isEmpty()) {
                                prvCentre = mBankLocator.getPrvCentre();
                            }
                            if (mBankLocator.getBranchlessBanking() != null && !mBankLocator.getBranchlessBanking().isEmpty()) {
                                branchlessBanking = mBankLocator.getBranchlessBanking();
                            }

                            String oldVal = mBankLocator.getLocatorid() + "|" + mBankLocator.getLocatorcode() + "|" + mBankLocator.getStatus() + "|" + mBankLocator.getChannelType().getChannelType() + "|" + mBankLocator.getCountryCode()
                                    + "|" + mBankLocator.getCountry() + "|" + mBankLocator.getMasterRegion() + "|" + mBankLocator.getSubRegion()
                                    + "|" + mBankLocator.getName() + "|" + address + "|" + postalCode + "|" + contacts + "|" + fax + "|" + openingHsMonFri
                                    + "|" + openingHsSat + "|" + openingHsSunHol + "|" + holidayBanking + "|" + mBankLocator.getLongitude().toString() + "|" + mBankLocator.getLatitude().toString() + "|" + locationTag + "|" + mBankLocator.getLanguage()
                                    + "|" + atmOnLocation + "|" + crmOnLocation + "|" + pawningOnLocation + "|" + safeDepositLockers + "|" + leasingDesk365 + "|" + prvCentre + "|" + branchlessBanking;
                            audit.setOldvalue(oldVal);
                        }
                    }
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on mobile bank locator (Locator code: " + locatorCode + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                } else {
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on mobile bank locator inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String pendMBankLocatorCsvDownloade(MBankLocatorInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getInputfile() != null) {
                    Blob blob = u.getInputfile();
                    int blobLength = (int) blob.length();
                    byte[] blobAsBytes = blob.getBytes(1, blobLength);
                    inputBean.setFileInputStream(u.getInputfile().getBinaryStream());
                    inputBean.setFileLength(blobAsBytes.length);

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    session.save(audit);
                    txn.commit();
                } else {
                    message = "File not found";
                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String viwPendMBankLocator(MBankLocatorInputBean inputBean) throws Exception {

        Session session = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                String[] penArray = null;
                if (u.getFields() != null) {
                    penArray = u.getFields().split("\\|");
                }
                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
                    try {
                        if (penArray[0] != null && !penArray[0].isEmpty()) {
                            inputBean.setLocatorcode(penArray[0].toString());
                        } else {
                            inputBean.setLocatorcode("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLocatorcode("--");
                    }
                    try {
                        if (penArray[1] != null && !penArray[1].isEmpty()) {
                            inputBean.setStatus(penArray[1].toString());
                        } else {
                            inputBean.setStatus("--");
                        }
                    } catch (Exception e) {
                        inputBean.setStatus("--");
                    }
                    try {
                        if (penArray[2] != null && !penArray[2].isEmpty()) {
                            inputBean.setChannelType(penArray[2].toString());
                        } else {
                            inputBean.setChannelType("--");
                        }
                    } catch (Exception e) {
                        inputBean.setChannelType("--");
                    }
                    try {
                        if (penArray[3] != null && !penArray[3].isEmpty()) {
                            inputBean.setCountryCode(penArray[3].toString());
                        } else {
                            inputBean.setCountryCode("--");
                        }
                    } catch (Exception e) {
                        inputBean.setCountryCode("--");
                    }
                    try {
                        if (penArray[4] != null && !penArray[4].isEmpty()) {
                            inputBean.setCountry(penArray[4].toString());
                        } else {
                            inputBean.setCountry("--");
                        }
                    } catch (Exception e) {
                        inputBean.setCountry("--");
                    }
                    try {
                        if (penArray[5] != null && !penArray[5].isEmpty()) {
                            inputBean.setMasterRegion(penArray[5].toString());
                        } else {
                            inputBean.setMasterRegion("--");
                        }
                    } catch (Exception e) {
                        inputBean.setMasterRegion("--");
                    }
                    try {
                        if (penArray[6] != null && !penArray[6].isEmpty()) {
                            inputBean.setSubRegion(penArray[6].toString());
                        } else {
                            inputBean.setSubRegion("--");
                        }
                    } catch (Exception e) {
                        inputBean.setSubRegion("--");
                    }
                    try {
                        if (penArray[7] != null && !penArray[7].isEmpty()) {
                            inputBean.setName(penArray[7].toString());
                        } else {
                            inputBean.setName("--");
                        }
                    } catch (Exception e) {
                        inputBean.setName("--");
                    }
                    try {
                        if (penArray[8] != null && !penArray[8].isEmpty()) {
                            inputBean.setAddress(penArray[8].toString());
                        } else {
                            inputBean.setAddress("--");
                        }
                    } catch (Exception e) {
                        inputBean.setAddress("--");
                    }
                    try {
                        if (penArray[9] != null && !penArray[9].isEmpty()) {
                            inputBean.setPostalCode(penArray[9].toString());
                        } else {
                            inputBean.setPostalCode("--");
                        }
                    } catch (Exception e) {
                        inputBean.setPostalCode("--");
                    }
                    try {
                        if (penArray[10] != null && !penArray[10].isEmpty()) {
                            inputBean.setContacts(penArray[10].toString());
                        } else {
                            inputBean.setContacts("--");
                        }
                    } catch (Exception e) {
                        inputBean.setContacts("--");
                    }
                    try {
                        if (penArray[11] != null && !penArray[11].isEmpty()) {
                            inputBean.setFax(penArray[11].toString());
                        } else {
                            inputBean.setFax("--");
                        }
                    } catch (Exception e) {
                        inputBean.setFax("--");
                    }
                    try {
                        if (penArray[12] != null && !penArray[12].isEmpty()) {
                            inputBean.setOpeningHsMonFri(penArray[12].toString());
                        } else {
                            inputBean.setOpeningHsMonFri("--");
                        }
                    } catch (Exception e) {
                        inputBean.setOpeningHsMonFri("--");
                    }
                    try {
                        if (penArray[13] != null && !penArray[13].isEmpty()) {
                            inputBean.setOpeningHsSat(penArray[13].toString());
                        } else {
                            inputBean.setOpeningHsSat("--");
                        }
                    } catch (Exception e) {
                        inputBean.setOpeningHsSat("--");
                    }
                    try {
                        if (penArray[14] != null && !penArray[14].isEmpty()) {
                            inputBean.setOpeningHsSunHol(penArray[14].toString());
                        } else {
                            inputBean.setOpeningHsSunHol("--");
                        }
                    } catch (Exception e) {
                        inputBean.setOpeningHsSunHol("--");
                    }
                    try {
                        if (penArray[15] != null && !penArray[15].isEmpty()) {
                            inputBean.setHolidayBanking(penArray[15].toString());
                        } else {
                            inputBean.setHolidayBanking("--");
                        }
                    } catch (Exception e) {
                        inputBean.setHolidayBanking("--");
                    }
                    try {
                        if (penArray[16] != null && !penArray[16].isEmpty()) {
                            inputBean.setLongitude(penArray[16].toString());
                        } else {
                            inputBean.setLongitude("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLongitude("--");
                    }
                    try {
                        if (penArray[17] != null && !penArray[17].isEmpty()) {
                            inputBean.setLatitude(penArray[17].toString());
                        } else {
                            inputBean.setLatitude("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLatitude("--");
                    }
                    try {
                        if (penArray[18] != null && !penArray[18].isEmpty()) {
                            inputBean.setLocationTag(penArray[18].toString());
                        } else {
                            inputBean.setLocationTag("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLocationTag("--");
                    }
                    try {
                        if (penArray[19] != null && !penArray[19].isEmpty()) {
                            inputBean.setLanguage(penArray[19].toString());
                        } else {
                            inputBean.setLanguage("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLanguage("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[20] != null && !penArray[20].isEmpty()) {
                            inputBean.setAtmOnLocation(penArray[20].toString());
                        } else {
                            inputBean.setAtmOnLocation("--");
                        }
                    } catch (Exception e) {
                        inputBean.setAtmOnLocation("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[21] != null && !penArray[21].isEmpty()) {
                            inputBean.setCrmOnLocation(penArray[21].toString());
                        } else {
                            inputBean.setCrmOnLocation("--");
                        }
                    } catch (Exception e) {
                        inputBean.setCrmOnLocation("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[22] != null && !penArray[22].isEmpty()) {
                            inputBean.setPawningOnLocation(penArray[22].toString());
                        } else {
                            inputBean.setPawningOnLocation("--");
                        }
                    } catch (Exception e) {
                        inputBean.setPawningOnLocation("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[23] != null && !penArray[23].isEmpty()) {
                            inputBean.setSafeDepositLockers(penArray[23].toString());
                        } else {
                            inputBean.setSafeDepositLockers("--");
                        }
                    } catch (Exception e) {
                        inputBean.setSafeDepositLockers("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[24] != null && !penArray[24].isEmpty()) {
                            inputBean.setLeasingDesk365(penArray[24].toString());
                        } else {
                            inputBean.setLeasingDesk365("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLeasingDesk365("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[25] != null && !penArray[25].isEmpty()) {
                            inputBean.setPrvCentre(penArray[25].toString());
                        } else {
                            inputBean.setPrvCentre("--");
                        }
                    } catch (Exception e) {
                        inputBean.setPrvCentre("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[26] != null && !penArray[26].isEmpty()) {
                            inputBean.setBranchlessBanking(penArray[26].toString());
                        } else {
                            inputBean.setBranchlessBanking("--");
                        }
                    } catch (Exception e) {
                        inputBean.setBranchlessBanking("--");
                    }

                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK) || u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    try {
                        if (penArray[1] != null && !penArray[1].isEmpty()) {
                            inputBean.setLocatorcode(penArray[1].toString());
                        } else {
                            inputBean.setLocatorcode("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLocatorcode("--");
                    }
                    try {
                        if (penArray[2] != null && !penArray[2].isEmpty()) {
                            inputBean.setStatus(penArray[2].toString());
                        } else {
                            inputBean.setStatus("--");
                        }
                    } catch (Exception e) {
                        inputBean.setStatus("--");
                    }
                    try {
                        if (penArray[3] != null && !penArray[3].isEmpty()) {
                            inputBean.setChannelType(penArray[3].toString());
                        } else {
                            inputBean.setChannelType("--");
                        }
                    } catch (Exception e) {
                        inputBean.setChannelType("--");
                    }
                    try {
                        if (penArray[4] != null && !penArray[4].isEmpty()) {
                            inputBean.setCountryCode(penArray[4].toString());
                        } else {
                            inputBean.setCountryCode("--");
                        }
                    } catch (Exception e) {
                        inputBean.setCountryCode("--");
                    }
                    try {
                        if (penArray[5] != null && !penArray[5].isEmpty()) {
                            inputBean.setCountry(penArray[5].toString());
                        } else {
                            inputBean.setCountry("--");
                        }
                    } catch (Exception e) {
                        inputBean.setCountry("--");
                    }
                    try {
                        if (penArray[6] != null && !penArray[6].isEmpty()) {
                            inputBean.setMasterRegion(penArray[6].toString());
                        } else {
                            inputBean.setMasterRegion("--");
                        }
                    } catch (Exception e) {
                        inputBean.setMasterRegion("--");
                    }
                    try {
                        if (penArray[7] != null && !penArray[7].isEmpty()) {
                            inputBean.setSubRegion(penArray[7].toString());
                        } else {
                            inputBean.setSubRegion("--");
                        }
                    } catch (Exception e) {
                        inputBean.setSubRegion("--");
                    }
                    try {
                        if (penArray[8] != null && !penArray[8].isEmpty()) {
                            inputBean.setName(penArray[8].toString());
                        } else {
                            inputBean.setName("--");
                        }
                    } catch (Exception e) {
                        inputBean.setName("--");
                    }
                    try {
                        if (penArray[9] != null && !penArray[9].isEmpty()) {
                            inputBean.setAddress(penArray[9].toString());
                        } else {
                            inputBean.setAddress("--");
                        }
                    } catch (Exception e) {
                        inputBean.setAddress("--");
                    }
                    try {
                        if (penArray[10] != null && !penArray[10].isEmpty()) {
                            inputBean.setPostalCode(penArray[10].toString());
                        } else {
                            inputBean.setPostalCode("--");
                        }
                    } catch (Exception e) {
                        inputBean.setPostalCode("--");
                    }
                    try {
                        if (penArray[11] != null && !penArray[11].isEmpty()) {
                            inputBean.setContacts(penArray[11].toString());
                        } else {
                            inputBean.setContacts("--");
                        }
                    } catch (Exception e) {
                        inputBean.setContacts("--");
                    }
                    try {
                        if (penArray[12] != null && !penArray[12].isEmpty()) {
                            inputBean.setFax(penArray[12].toString());
                        } else {
                            inputBean.setFax("--");
                        }
                    } catch (Exception e) {
                        inputBean.setFax("--");
                    }
                    try {
                        if (penArray[13] != null && !penArray[13].isEmpty()) {
                            inputBean.setOpeningHsMonFri(penArray[13].toString());
                        } else {
                            inputBean.setOpeningHsMonFri("--");
                        }
                    } catch (Exception e) {
                        inputBean.setOpeningHsMonFri("--");
                    }
                    try {
                        if (penArray[14] != null && !penArray[14].isEmpty()) {
                            inputBean.setOpeningHsSat(penArray[14].toString());
                        } else {
                            inputBean.setOpeningHsSat("--");
                        }
                    } catch (Exception e) {
                        inputBean.setOpeningHsSat("--");
                    }
                    try {
                        if (penArray[15] != null && !penArray[15].isEmpty()) {
                            inputBean.setOpeningHsSunHol(penArray[15].toString());
                        } else {
                            inputBean.setOpeningHsSunHol("--");
                        }
                    } catch (Exception e) {
                        inputBean.setOpeningHsSunHol("--");
                    }
                    try {
                        if (penArray[16] != null && !penArray[16].isEmpty()) {
                            inputBean.setHolidayBanking(penArray[16].toString());
                        } else {
                            inputBean.setHolidayBanking("--");
                        }
                    } catch (Exception e) {
                        inputBean.setHolidayBanking("--");
                    }
                    try {
                        if (penArray[17] != null && !penArray[17].isEmpty()) {
                            inputBean.setLongitude(penArray[17].toString());
                        } else {
                            inputBean.setLongitude("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLongitude("--");
                    }
                    try {
                        if (penArray[18] != null && !penArray[18].isEmpty()) {
                            inputBean.setLatitude(penArray[18].toString());
                        } else {
                            inputBean.setLatitude("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLatitude("--");
                    }
                    try {
                        if (penArray[19] != null && !penArray[19].isEmpty()) {
                            inputBean.setLocationTag(penArray[19].toString());
                        } else {
                            inputBean.setLocationTag("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLocationTag("--");
                    }
                    try {
                        if (penArray[20] != null && !penArray[20].isEmpty()) {
                            inputBean.setLanguage(penArray[20].toString());
                        } else {
                            inputBean.setLanguage("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLanguage("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[21] != null && !penArray[21].isEmpty()) {
                            inputBean.setAtmOnLocation(penArray[21].toString());
                        } else {
                            inputBean.setAtmOnLocation("--");
                        }
                    } catch (Exception e) {
                        inputBean.setAtmOnLocation("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[22] != null && !penArray[22].isEmpty()) {
                            inputBean.setCrmOnLocation(penArray[22].toString());
                        } else {
                            inputBean.setCrmOnLocation("--");
                        }
                    } catch (Exception e) {
                        inputBean.setCrmOnLocation("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[23] != null && !penArray[23].isEmpty()) {
                            inputBean.setPawningOnLocation(penArray[23].toString());
                        } else {
                            inputBean.setPawningOnLocation("--");
                        }
                    } catch (Exception e) {
                        inputBean.setPawningOnLocation("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[24] != null && !penArray[24].isEmpty()) {
                            inputBean.setSafeDepositLockers(penArray[24].toString());
                        } else {
                            inputBean.setSafeDepositLockers("--");
                        }
                    } catch (Exception e) {
                        inputBean.setSafeDepositLockers("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[25] != null && !penArray[25].isEmpty()) {
                            inputBean.setLeasingDesk365(penArray[25].toString());
                        } else {
                            inputBean.setLeasingDesk365("--");
                        }
                    } catch (Exception e) {
                        inputBean.setLeasingDesk365("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[26] != null && !penArray[26].isEmpty()) {
                            inputBean.setPrvCentre(penArray[26].toString());
                        } else {
                            inputBean.setPrvCentre("--");
                        }
                    } catch (Exception e) {
                        inputBean.setPrvCentre("--");
                    }
                    try {//empty value in insert boundry out of index
                        if (penArray[27] != null && !penArray[27].isEmpty()) {
                            inputBean.setBranchlessBanking(penArray[27].toString());
                        } else {
                            inputBean.setBranchlessBanking("--");
                        }
                    } catch (Exception e) {
                        inputBean.setBranchlessBanking("--");
                    }

                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public StringBuffer makeCSVReport(MBankLocatorInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from MBankLocator u where " + where;
            Query querySearch = session.createQuery(sqlSearch);

            Iterator it = querySearch.iterate();
            content = new StringBuffer();

            //write column headers to csv file
            content.append("Locator ID");
            content.append(',');
            content.append("Locator Code");
            content.append(',');
            content.append("Channel Type");
            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Country Code");
            content.append(',');
            content.append("Country");
            content.append(',');
            content.append("Master Region");
            content.append(',');
            content.append("Subregion");
            content.append(',');
            content.append("Name");
            content.append(',');
            content.append("Address");
            content.append(',');
            content.append("Postal Code");
            content.append(',');
            content.append("Contacts");
            content.append(',');
            content.append("Fax");
            content.append(',');
            content.append("Opening Hours From Mon To Fri");
            content.append(',');
            content.append("Opening Hours At Sat");
            content.append(',');
            content.append("Opening Hours at Sun");
            content.append(',');
            content.append("Holiday Banking");
            content.append(',');
            content.append("Longitude");
            content.append(',');
            content.append("Latitude");
            content.append(',');
            content.append("Location Tag");
            content.append(',');
            content.append("Language");
            content.append(',');
            content.append("ATM On Location");
            content.append(',');
            content.append("CRM On Location");
            content.append(',');
            content.append("Pawning On Location");
            content.append(',');
            content.append("Safe Deposit Lockers");
            content.append(',');
            content.append("Leasing Desk 365 Days");
            content.append(',');
            content.append("PRV Centre");
            content.append(',');
            content.append("Branchless Banking");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Checker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');

            while (it.hasNext()) {

                MBankLocator mBankLocator = (MBankLocator) it.next();

                try {
                    content.append(mBankLocator.getLocatorid().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getLocatorcode().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getChannelType().getChannelType().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
//                    Status status = (Status) session.get(Status.class, mBankLocator.getStatus());
                    content.append(mBankLocator.getStatus());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
//                    try {
//                        mBankLocatorBean.setChannelSubType(mBankLocator.getChannelSubType().toString());
//                    } catch (Exception npe) {
//                        mBankLocatorBean.setChannelSubType("--");
//                    }
                try {
                    content.append(mBankLocator.getCountryCode().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getCountry().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getMasterRegion().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
//                    try {
//                        mBankLocatorBean.setRegion(mBankLocator.getRegion().toString());
//                    } catch (NullPointerException npe) {
//                        mBankLocatorBean.setRegion("--");
//                    }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getSubRegion().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getName().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getAddress().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getPostalCode().toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getContacts().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getFax().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getOpeningHsMonFri().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getOpeningHsSat().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getOpeningHsSunHol().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getHolidayBanking().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getLongitude().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getLatitude().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(mBankLocator.getLocationTag().replace("\n", " ").toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getLanguage().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getAtmOnLocation().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getCrmOnLocation().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getPawningOnLocation().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getSafeDepositLockers().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getLeasingDesk365().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getPrvCentre().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getBranchlessBanking().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getChecker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getCreatedtime().toString().substring(0, 19));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(mBankLocator.getLastupdatedtime().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    content.append("--");
                }
                content.append('\n');
            }
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    public String validateInputsForCSV(MBankLocatorInputBean inputBean) {
        String message = "";
        if (inputBean.getLocatorcode() == null || inputBean.getLocatorcode().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LOCATORCODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getLocatorcode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LOCATORCODE;
        } else if (inputBean.getChannelType() == null || inputBean.getChannelType().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CHANNELTYPE;
//        } else if (inputBean.getStatus() == null || inputBean.getStatus().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_STATUS;
//        } else if (inputBean.getChannelSubType()!= null && inputBean.getChannelSubType().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CHANNELSUBTYPE;
        } else if (inputBean.getCountryCode() == null || inputBean.getCountryCode().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_COUNTRYCODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getCountryCode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_COUNTRYCODE;
        } else if (inputBean.getCountry() == null || inputBean.getCountry().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_COUNTRY;
        } else if (!Validation.isSpecailCharacter(inputBean.getCountry())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_COUNTRY;
        } else if (inputBean.getMasterRegion() == null || inputBean.getMasterRegion().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_MASTERREGION;
        } else if (!Validation.isSpecailCharacter(inputBean.getMasterRegion())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_MASTERREGION;
//        } else if (inputBean.getRegion()== null || inputBean.getRegion().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_RERION;
        } else if (inputBean.getSubRegion() == null || inputBean.getSubRegion().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_SUBREGION;
        } else if (!Validation.isSpecailCharacter(inputBean.getSubRegion())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_SUBREGION;
        } else if (inputBean.getName() == null || inputBean.getName().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_NAME;
//        } else if (!Validation.isSpecailCharacter(inputBean.getName())) {
//            message = MessageVarList.M_BANK_LOCATOR_INVALID_NAME;
//        } else if (inputBean.getAddress() == null || inputBean.getAddress().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_ADDRESS;
//        } else if (inputBean.getPostalCode()== null || inputBean.getPostalCode().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_POSTALCODE;
        } else if (inputBean.getPostalCode() != null && !inputBean.getPostalCode().trim().isEmpty() && !Validation.isSpecailCharacter(inputBean.getPostalCode())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_POSTALCODE;
//        } else if (inputBean.getContacts() == null || inputBean.getContacts().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_CONTACTS;
//        } else if ( inputBean.getContacts()!= null && !inputBean.getContacts().trim().isEmpty() && !Validation.areMobileNumers(inputBean.getContacts().trim())) {
//            System.out.println("------"+inputBean.getContacts().trim());
//            message = MessageVarList.M_BANK_LOCATOR_INVALID_CONTACTS;
        } else if (inputBean.getFax() != null && !inputBean.getFax().trim().isEmpty() && !Validation.isFaxNumber(inputBean.getFax().trim())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_FAX;
//        } else if (inputBean.getOpeningHsMonFri() == null || inputBean.getOpeningHsMonFri().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_MON_FRI;
//        } else if (inputBean.getOpeningHsSat()== null || inputBean.getOpeningHsSat().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_SAT;
//        } else if (inputBean.getOpeningHsSunHol()== null || inputBean.getOpeningHsSunHol().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_OPENING_HS_SUN;
//        } else if (inputBean.getGeoCode() == null || inputBean.getGeoCode().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_GEOCODE;
        } else if (inputBean.getLatitude() == null || inputBean.getLatitude().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LATITUDE;
        } else if (Validation.isLongitudeLatitude(inputBean.getLatitude())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LATITUDE;
        } else if (inputBean.getLongitude() == null || inputBean.getLongitude().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LONGITUDE;
        } else if (Validation.isLongitudeLatitude(inputBean.getLongitude())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LONGITUDE;
//        } else if (inputBean.getLocationTag()== null || inputBean.getLocationTag().trim().isEmpty()) {
//            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LOCATIONTAG;
        } else if (inputBean.getLanguage() == null || inputBean.getLanguage().trim().isEmpty()) {
            message = MessageVarList.M_BANK_LOCATOR_EMPTY_LANGUAGE;
        } else if (!Validation.isSpecailCharacter(inputBean.getLanguage())) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LANGUAGE;
        }

        return message;
    }

    public String validateUpload(MBankLocatorInputBean inputBean, Session session) throws Exception {
        String message = "";

        if (inputBean.getLocatorcode().length() > 20) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LOCATORCODE_LENGTH + "20";
        } else if (inputBean.getCountryCode().length() > 10) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_COUNTRYCODE_LENGTH + "10";
        } else if (inputBean.getCountry().length() > 20) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_COUNTRY_LENGTH + "20";
        } else if (inputBean.getMasterRegion().length() > 20) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_MASTERREGION_LENGTH + "20";
        } else if (inputBean.getSubRegion().length() > 100) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_SUBREGION_LENGTH + "100";
        } else if (inputBean.getName().length() > 100) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_NAME_LENGTH + "100";
        } else if (inputBean.getAddress() != null && !inputBean.getAddress().isEmpty() && inputBean.getAddress().length() > 510) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_ADDRESS_LENGTH + "510";
        } else if (inputBean.getPostalCode() != null && !inputBean.getPostalCode().isEmpty() && inputBean.getPostalCode().length() > 10) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_POSTALCODE_LENGTH + "10";
        } else if (inputBean.getContacts() != null && !inputBean.getContacts().isEmpty() && inputBean.getContacts().length() > 100) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_CONTACTS_LENGTH + "100";
        } else if (inputBean.getFax() != null && !inputBean.getFax().isEmpty() && inputBean.getFax().length() > 100) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_FAX_LENGTH + "100";
        } else if (inputBean.getOpeningHsMonFri() != null && !inputBean.getOpeningHsMonFri().isEmpty() && inputBean.getOpeningHsMonFri().length() > 250) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_OPENING_HS_MON_FRI_LENGTH + "250";
        } else if (inputBean.getOpeningHsSat() != null && !inputBean.getOpeningHsSat().isEmpty() && inputBean.getOpeningHsSat().length() > 250) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_OPENING_HS_SAT_LENGTH + "250";
        } else if (inputBean.getOpeningHsSunHol() != null && !inputBean.getOpeningHsSunHol().isEmpty() && inputBean.getOpeningHsSunHol().length() > 250) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_OPENING_HS_SUN_LENGTH + "250";
        } else if (inputBean.getHolidayBanking() != null && !inputBean.getHolidayBanking().isEmpty() && inputBean.getHolidayBanking().length() > 250) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_HOLIDAY_BANKING_LENGTH + "250";
        } else if (inputBean.getLatitude().length() > 20) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LATITUDE_LENGTH + "20";
        } else if (inputBean.getLongitude().length() > 20) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LONGITUDE_LENGTH + "20";
        } else if (inputBean.getLocationTag() != null && !inputBean.getLocationTag().isEmpty() && inputBean.getLocationTag().length() > 100) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LOCATIONTAG_LENGTH + "100";
        } else if (inputBean.getLanguage().length() > 20) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LANGUAGE_LENGTH + "20";
        } else if (inputBean.getAtmOnLocation() != null && !inputBean.getAtmOnLocation().isEmpty() && inputBean.getAtmOnLocation().length() > 30) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_ATM_ON_LOCATION_LENGTH + "30";
        } else if (inputBean.getCrmOnLocation() != null && !inputBean.getCrmOnLocation().isEmpty() && inputBean.getCrmOnLocation().length() > 30) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_CRM_ON_LOCATION_LENGTH + "30";
        } else if (inputBean.getPawningOnLocation() != null && !inputBean.getPawningOnLocation().isEmpty() && inputBean.getPawningOnLocation().length() > 30) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_PAWNING_ON_LOCATION_LENGTH + "30";
        } else if (inputBean.getSafeDepositLockers() != null && !inputBean.getSafeDepositLockers().isEmpty() && inputBean.getSafeDepositLockers().length() > 30) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_SAFE_DEPOSIT_LOCKER_LENGTH + "30";
        } else if (inputBean.getLeasingDesk365() != null && !inputBean.getLeasingDesk365().isEmpty() && inputBean.getLeasingDesk365().length() > 30) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_LEASING_DESK_345_LENGTH + "30";
        } else if (inputBean.getPrvCentre() != null && !inputBean.getPrvCentre().isEmpty() && inputBean.getPrvCentre().length() > 30) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_PRV_CENTRE_LENGTH + "30";
        } else if (inputBean.getBranchlessBanking() != null && !inputBean.getBranchlessBanking().isEmpty() && inputBean.getBranchlessBanking().length() > 30) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_BRANCHLESS_BANKING_LENGTH + "30";
        } else if ((ChannelType) session.get(ChannelType.class, inputBean.getChannelType().trim()) == null) {
            message = MessageVarList.M_BANK_LOCATOR_INVALID_CHANNELTYPE;
        }

        return message;
    }

}
