/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.CommonKeyVal;
import com.epic.ndb.bean.controlpanel.systemconfig.PasswordPolicyInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.PasswordPolicyPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.mapping.Passwordpolicy;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.Userpassword;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author thushanth
 */
public class PasswordPolicyDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<PasswordPolicyPendBean> getPendingPasswordPolicyList(PasswordPolicyInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<PasswordPolicyPendBean> dataList = new ArrayList<PasswordPolicyPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.PAS_POLICY).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.PAS_POLICY).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    PasswordPolicyPendBean jcode = new PasswordPolicyPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        jcode.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        Passwordpolicy passwordpolicy = (Passwordpolicy) session.get(Passwordpolicy.class, Integer.parseInt(penArray[0].toString()));
                        jcode.setPasswordPolicy(passwordpolicy.getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setPasswordPolicy(penArray[0].toString());
                    } catch (Exception ex) {
                        jcode.setPasswordPolicy("--");
                    }
                    try {
                        jcode.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        jcode.setFields("--");
                    } catch (Exception ex) {
                        jcode.setFields("--");
                    }
                    try {
                        jcode.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public Passwordpolicy getPasswordPolicyDetails() throws Exception {
        Passwordpolicy ipgpasswordpolicy = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Passwordpolicy ";

            Query query = session.createQuery(sql);

            ipgpasswordpolicy = (Passwordpolicy) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return ipgpasswordpolicy;
    }

    public boolean isExistPasswordPolicy() throws Exception {
        long count = 0;
        boolean status = false;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "select count(passwordpolicyid) from Passwordpolicy";

            Query query = session.createQuery(sql);

            Iterator it = query.iterate();

            while (it.hasNext()) {
                count = (Long) it.next();
                if (count > 0) {
                    status = true;
                    break;
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return status;
    }

    public Passwordpolicy findPasswordPolicyById(String passwordpolicyid) throws Exception {
        Passwordpolicy passwordpolicy = new Passwordpolicy();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String hql = "from Passwordpolicy as m where m.passwordpolicyid=:passwordpolicyid";
            Query query = session.createQuery(hql).setString("passwordpolicyid", passwordpolicyid);
            passwordpolicy = (Passwordpolicy) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return passwordpolicy;
    }

    public String insertPasswordPolicy(PasswordPolicyInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((Passwordpolicy) session.get(Passwordpolicy.class, new Integer(inputBean.getPasswordpolicyid().trim())) == null) {
                txn = session.beginTransaction();

                Passwordpolicy i = new Passwordpolicy();

                /*
                 * private int minimumlength;
                 private int maximumlength;
                 private Integer minimumspecialcharacters;
                 private Integer minimumuppercasecharacters;
                 private Integer minimumnumericalcharacters;
                 private Integer minimumlowercasecharacters;
                 private Integer repeatcharactersallow;
                 private Integer initialpasswordexpirystatus;
                 private int passwordexpiryperiod;
                 private Integer noofhistorypassword;
                 private Integer minimumpasswordchangeperiod;
                 private Integer idleaccountexpiryperiod;
                 private int noofinvalidloginattempt;
                 * */
                i.setPasswordpolicyid(new Integer(inputBean.getPasswordpolicyid()));
                i.setMinimumlength(new Integer(inputBean.getMinimumlength()));
                i.setMaximumlength(new Integer(inputBean.getMaximumlength()));
                i.setMinimumspecialcharacters(new Integer(inputBean.getMinimumspecialcharacters()));
                i.setMinimumuppercasecharacters(new Integer(inputBean.getMinimumuppercasecharacters()));
                i.setMinimumnumericalcharacters(new Integer(inputBean.getMinimumnumericalcharacters()));
                i.setMinimumlowercasecharacters(new Integer(inputBean.getMinimumlowercasecharacters()));
                i.setRepeatcharactersallow(new Integer(inputBean.getRepeatcharactersallow()));
                i.setInitialpasswordexpirystatus("1");
                i.setPasswordexpiryperiod(new Integer(inputBean.getPasswordexpiryperiod()));
                i.setNoofhistorypassword(new Integer(inputBean.getNoofhistorypassword()));
                i.setMinimumpasswordchangeperiod(new Integer(inputBean.getMinimumpasswordchangeperiod()));
                i.setIdleaccountexpiryperiod(new Integer(inputBean.getIdleaccountexpiryperiod()));
                i.setNoofinvalidloginattempt(new Integer(inputBean.getNoofinvalidloginattempt()));

                i.setCreatetime(sysDate);
                i.setLastupdatedtime(sysDate);
                i.setLastupdateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.save(i);

                txn.commit();
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public void updateHistory(PasswordPolicyInputBean inputBean, Session session) throws Exception {
        List<Userpassword> kj = new ArrayList<Userpassword>();
        List<Object[]> uname = null;

        String hql = "select m.id.username as uname, count(m.id.username) as count from Userpassword as m "
                + "group by m.id.username "
                + "having count(m.id.username) >:noofrec ";

        Query query = session.createQuery(hql).setString("noofrec", inputBean.getNoofhistorypassword());
        uname = query.list();

        for (Object[] var : uname) {
            String hql2 = "from Userpassword as s where s.id.username =:username"
                    + " order by s.lastupdatedtime asc";

            Query query2 = session.createQuery(hql2).setString("username", (String) var[0]);
            query2.setFirstResult(0);
            query2.setMaxResults((int) (long) (Long) var[1] - new Integer(inputBean.getNoofhistorypassword()));
            kj.addAll(query2.list());
        }

//                 String hql2 = "from Userpassword as s where s.id.username in "
//                + "( select m.id.username from Userpassword as m "
//                + "group by m.id.username "
//                + "having count(m.id.username) > 1) "
//                + "order by s.lastupdatedtime asc";
        for (Userpassword j : kj) {
            session.delete(j);
            session.flush();
        }
    }

    public String updatePasswordPolicy(PasswordPolicyInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getPasswordpolicyid())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                Passwordpolicy u = (Passwordpolicy) session.get(Passwordpolicy.class, new Integer(inputBean.getPasswordpolicyid().trim()));
                if (u != null) {
                    String oldVal = u.getPasswordpolicyid() + "|"
                            + u.getMinimumlength() + "|"
                            + u.getMaximumlength() + "|"
                            + u.getMinimumspecialcharacters() + "|"
                            + u.getMinimumuppercasecharacters() + "|"
                            + u.getMinimumlowercasecharacters() + "|"
                            + u.getMinimumnumericalcharacters() + "|"
                            + u.getRepeatcharactersallow();

                    if (!inputBean.getPasswordpolicyid().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                        oldVal += "|" + u.getPasswordexpiryperiod() + "|"
                                + u.getMinimumpasswordchangeperiod() + "|"
                                + u.getNoofhistorypassword() + "|"
                                + u.getIdleaccountexpiryperiod() + "|"
                                + u.getNoofinvalidloginattempt();
                    }
                    audit.setOldvalue(oldVal);
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getPasswordpolicyid().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.PAS_POLICY);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }
//            Passwordpolicy i = (Passwordpolicy) session.get(Passwordpolicy.class, new Integer(inputBean.getPasswordpolicyid().trim()));
//
//            if (i != null) {
//                updateHistory(inputBean, session);
//
//                String oldV = i.getPasswordpolicyid() + "|"
//                        + i.getMinimumlength() + "|"
//                        + i.getMaximumlength() + "|"
//                        + i.getMinimumspecialcharacters() + "|"
//                        + i.getMinimumuppercasecharacters() + "|"
//                        + i.getMinimumlowercasecharacters() + "|"
//                        + i.getMinimumnumericalcharacters() + "|"
//                        + i.getRepeatcharactersallow() + "|"
//                        + i.getPasswordexpiryperiod() + "|"
//                        + i.getMinimumpasswordchangeperiod() + "|"
//                        + i.getNoofhistorypassword() + "|"
//                        + i.getIdleaccountexpiryperiod() + "|"
//                        + i.getNoofinvalidloginattempt();
//
//                i.setMinimumlength(new Integer(inputBean.getMinimumlength().trim()));
//                i.setMaximumlength(new Integer(inputBean.getMaximumlength().trim()));
//                i.setMinimumspecialcharacters(new Integer(inputBean.getMinimumspecialcharacters().trim()));
//                i.setMinimumuppercasecharacters(new Integer(inputBean.getMinimumuppercasecharacters().trim()));
//                i.setMinimumnumericalcharacters(new Integer(inputBean.getMinimumnumericalcharacters().trim()));
//                i.setMinimumlowercasecharacters(new Integer(inputBean.getMinimumlowercasecharacters().trim()));
//                i.setRepeatcharactersallow(new Integer(inputBean.getRepeatcharactersallow().trim()));
//                i.setInitialpasswordexpirystatus("1");
//                i.setPasswordexpiryperiod(new Integer(inputBean.getPasswordexpiryperiod().trim()));
//                i.setNoofhistorypassword(new Integer(inputBean.getNoofhistorypassword().trim()));
//                i.setMinimumpasswordchangeperiod(new Integer(inputBean.getMinimumpasswordchangeperiod().trim()));
//                i.setIdleaccountexpiryperiod(new Integer(inputBean.getIdleaccountexpiryperiod().trim()));
//                i.setNoofinvalidloginattempt(new Integer(inputBean.getNoofinvalidloginattempt().trim()));
//
//                i.setCreatetime(sysDate);
//                i.setLastupdatedtime(sysDate);
//                i.setLastupdateduser(audit.getLastupdateduser());
//
//                String newV = i.getPasswordpolicyid() + "|"
//                        + i.getMinimumlength() + "|"
//                        + i.getMaximumlength() + "|"
//                        + i.getMinimumspecialcharacters() + "|"
//                        + i.getMinimumuppercasecharacters() + "|"
//                        + i.getMinimumlowercasecharacters() + "|"
//                        + i.getMinimumnumericalcharacters() + "|"
//                        + i.getRepeatcharactersallow() + "|"
//                        + i.getPasswordexpiryperiod() + "|"
//                        + i.getMinimumpasswordchangeperiod() + "|"
//                        + i.getNoofhistorypassword() + "|"
//                        + i.getIdleaccountexpiryperiod() + "|"
//                        + i.getNoofinvalidloginattempt();
//
//                audit.setOldvalue(oldV);
//                audit.setNewvalue(newV);
//                audit.setCreatetime(sysDate);
//                audit.setLastupdatedtime(sysDate);
//
//                String tooltip = "";
//
//                for (int minx = 0; minx < i.getMinimumlength(); minx++) {
//
//                    for (int x = 0; x < i.getMinimumspecialcharacters(); x++) {
//                        if (tooltip.length() < i.getMinimumlength()) {
//                            tooltip = tooltip + (char) (42 + x + minx);
//                        } else {
//                            break;
//                        }
//                    }
//                    for (int x = 0; x < i.getMinimumuppercasecharacters(); x++) {
//                        if (tooltip.length() < i.getMinimumlength()) {
//                            tooltip = tooltip + (char) (65 + x + minx);
//                        } else {
//                            break;
//                        }
//                    }
//                    for (int x = 0; x < i.getMinimumlowercasecharacters(); x++) {
//                        if (tooltip.length() < i.getMinimumlength()) {
//                            tooltip = tooltip + (char) (97 + x + minx);
//                        } else {
//                            break;
//                        }
//                    }
//                    for (int x = 0; x < i.getMinimumnumericalcharacters(); x++) {
//                        if (tooltip.length() < i.getMinimumlength()) {
//                            tooltip = tooltip + (char) (48 + x + minx);
//                        } else {
//                            break;
//                        }
//                    }
//
//                }
//                System.err.println(tooltip);
//                System.err.println(this.generateToolTipMessage(i));
//                updateToolTip(tooltip);
//
//                session.save(audit);
//                session.update(i);
//
//                txn.commit();
//            } else {
//                message = MessageVarList.COMMON_NOT_EXISTS;
//            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String generateToolTipMessage(Passwordpolicy ppolicy) {

        String tooltip = null;
//                "Minimum length should be equal or greater than "+ppolicy.getMinimumlength()+"<br/>";
        tooltip = "Maximum " + ppolicy.getMaximumlength() + " character(s) <br/>";
        tooltip = tooltip + "At least " + ppolicy.getMinimumspecialcharacters() + " special character(s)<br/>";
        tooltip = tooltip + "At least " + ppolicy.getMinimumuppercasecharacters() + " upper case character(s)<br/>";
        tooltip = tooltip + "At least " + ppolicy.getMinimumlowercasecharacters() + " lower case character(s)<br/>";
        tooltip = tooltip + "At least " + ppolicy.getMinimumnumericalcharacters() + " numeric character(s)<br/>";
        if (ppolicy.getRepeatcharactersallow() < 2) {
            tooltip = tooltip + "Password must not contain repeat characters <br/>";
        } else {
            tooltip = tooltip + "Maximum  " + ppolicy.getRepeatcharactersallow() + " repeat character(s)<br/>";
//        System.err.println(tooltip);
        }
        return tooltip;
    }

    public void updateToolTip(String tooltip) throws Exception {
        Session session = null;
        Transaction txn = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            ParameterUserCommon u = (ParameterUserCommon) session.get(ParameterUserCommon.class, "PWTOOLTIP");
            if (u != null) {
                u.setParamvalue("Example : " + tooltip);
                session.update(u);
                txn.commit();
            } else {

            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }

    }

    public Status getStatusDes(Status statusCode) throws Exception {

        Status status = null;
        Session session = null;
        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "from Status as s where s.statuscode=:status";
            Query query = session.createQuery(sql).setString("status", statusCode.getStatuscode());
            status = (Status) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return status;
    }

    public List<CommonKeyVal> getPasswordPolicyList() {

        List<CommonKeyVal> passwordpolicyBeanList = new ArrayList<CommonKeyVal>();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
//            String sql = "from Passwordpolicy as s order by Upper(s.description) asc";
            String sql = "from Passwordpolicy ";
            Query query = session.createQuery(sql);
            List<Passwordpolicy> passwordpolicyList = query.list();
            for (Passwordpolicy passwordpolicy : passwordpolicyList) {
                CommonKeyVal passwordpolicyBean = new CommonKeyVal();
                passwordpolicyBean.setKey(String.valueOf(passwordpolicy.getPasswordpolicyid()));
                passwordpolicyBean.setValue(passwordpolicy.getDescription());
                passwordpolicyBeanList.add(passwordpolicyBean);
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return passwordpolicyBeanList;

    }

    public String confirmPasswordPolicy(PasswordPolicyInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }
                if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Passwordpolicy u = (Passwordpolicy) session.get(Passwordpolicy.class, Integer.parseInt(penArray[0].trim()));

                    if (u != null) {
                        String passwordpolicyid = String.valueOf(u.getPasswordpolicyid());

                        String oldVal = u.getPasswordpolicyid() + "|"
                                + u.getMinimumlength() + "|"
                                + u.getMaximumlength() + "|"
                                + u.getMinimumspecialcharacters() + "|"
                                + u.getMinimumuppercasecharacters() + "|"
                                + u.getMinimumlowercasecharacters() + "|"
                                + u.getMinimumnumericalcharacters() + "|"
                                + u.getRepeatcharactersallow();

                        if (!passwordpolicyid.equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                            oldVal += "|" + u.getPasswordexpiryperiod() + "|"
                                    + u.getMinimumpasswordchangeperiod() + "|"
                                    + u.getNoofhistorypassword() + "|"
                                    + u.getIdleaccountexpiryperiod() + "|"
                                    + u.getNoofinvalidloginattempt();
                        }
                        audit.setOldvalue(oldVal);

                        u.setMinimumlength(new Integer(penArray[1].trim()));
                        u.setMaximumlength(new Integer(penArray[2].trim()));
                        u.setMinimumspecialcharacters(new Integer(penArray[3].trim()));
                        u.setMinimumuppercasecharacters(new Integer(penArray[4].trim()));
                        u.setMinimumlowercasecharacters(new Integer(penArray[5].trim()));
                        u.setMinimumnumericalcharacters(new Integer(penArray[6].trim()));
                        u.setRepeatcharactersallow(new Integer(penArray[7].trim()));

                        if (!passwordpolicyid.equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                            u.setInitialpasswordexpirystatus("1");
                            u.setPasswordexpiryperiod(new Integer(penArray[8].trim()));
                            u.setMinimumpasswordchangeperiod(new Integer(penArray[9].trim()));
                            u.setNoofhistorypassword(new Integer(penArray[10].trim()));
                            u.setIdleaccountexpiryperiod(new Integer(penArray[11].trim()));
                            u.setNoofinvalidloginattempt(new Integer(penArray[12].trim()));
                        }
                        u.setCreatetime(sysDate);
                        u.setLastupdatedtime(sysDate);
                        u.setLastupdateduser(audit.getLastupdateduser());

                        String newVal = u.getPasswordpolicyid() + "|"
                                + u.getMinimumlength() + "|"
                                + u.getMaximumlength() + "|"
                                + u.getMinimumspecialcharacters() + "|"
                                + u.getMinimumuppercasecharacters() + "|"
                                + u.getMinimumlowercasecharacters() + "|"
                                + u.getMinimumnumericalcharacters() + "|"
                                + u.getRepeatcharactersallow();
                        if (!passwordpolicyid.equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                            newVal += "|" + u.getPasswordexpiryperiod() + "|"
                                    + u.getMinimumpasswordchangeperiod() + "|"
                                    + u.getNoofhistorypassword() + "|"
                                    + u.getIdleaccountexpiryperiod() + "|"
                                    + u.getNoofinvalidloginattempt();
                        }
                        audit.setNewvalue(newVal);

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on password policy ( password policy : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        if (passwordpolicyid.equals(CommonVarList.ADMIN_USER_PAS_POLICY_ID)) {
                            String tooltip = "";

                            for (int minx = 0; minx < u.getMinimumlength(); minx++) {

                                for (int x = 0; x < u.getMinimumspecialcharacters(); x++) {
                                    if (tooltip.length() < u.getMinimumlength()) {
                                        tooltip = tooltip + (char) (42 + x + minx);
                                    } else {
                                        break;
                                    }
                                }
                                for (int x = 0; x < u.getMinimumuppercasecharacters(); x++) {
                                    if (tooltip.length() < u.getMinimumlength()) {
                                        tooltip = tooltip + (char) (65 + x + minx);
                                    } else {
                                        break;
                                    }
                                }
                                for (int x = 0; x < u.getMinimumlowercasecharacters(); x++) {
                                    if (tooltip.length() < u.getMinimumlength()) {
                                        tooltip = tooltip + (char) (97 + x + minx);
                                    } else {
                                        break;
                                    }
                                }
                                for (int x = 0; x < u.getMinimumnumericalcharacters(); x++) {
                                    if (tooltip.length() < u.getMinimumlength()) {
                                        tooltip = tooltip + (char) (48 + x + minx);
                                    } else {
                                        break;
                                    }
                                }

                            }
                            System.err.println(tooltip);
                            System.err.println(this.generateToolTipMessage(u));
                            updateToolTip(tooltip);
                        }
                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectPasswordPolicy(PasswordPolicyInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String code = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Passwordpolicy passPolice = (Passwordpolicy) session.get(Passwordpolicy.class, Integer.parseInt(code.trim()));

                    if (passPolice!= null) {
                        String passwordpolicyid = String.valueOf(passPolice.getPasswordpolicyid());

                        String oldVal = passPolice.getPasswordpolicyid() + "|"
                                + passPolice.getMinimumlength() + "|"
                                + passPolice.getMaximumlength() + "|"
                                + passPolice.getMinimumspecialcharacters() + "|"
                                + passPolice.getMinimumuppercasecharacters() + "|"
                                + passPolice.getMinimumlowercasecharacters() + "|"
                                + passPolice.getMinimumnumericalcharacters() + "|"
                                + passPolice.getRepeatcharactersallow();

                        if (!passwordpolicyid.equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                            oldVal += "|" + passPolice.getPasswordexpiryperiod() + "|"
                                    + passPolice.getMinimumpasswordchangeperiod() + "|"
                                    + passPolice.getNoofhistorypassword() + "|"
                                    + passPolice.getIdleaccountexpiryperiod() + "|"
                                    + passPolice.getNoofinvalidloginattempt();
                        }
                        audit.setOldvalue(oldVal);
                    }
                }
                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on password policy ( password policy : " + code + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String viwPendPasswordPolicy(PasswordPolicyInputBean inputBean) throws Exception {

        Session session = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                String[] penArray = new String[10];
                if (u.getFields() != null) {
                    penArray = u.getFields().split("\\|");
                }
                inputBean.setPasswordpolicyid(penArray[0].trim());
                try {
                    Passwordpolicy passwordpolicy = (Passwordpolicy) session.get(Passwordpolicy.class, Integer.parseInt(penArray[0].toString()));
                    inputBean.setPasswordpolicydes(passwordpolicy.getDescription());
                } catch (NullPointerException npe) {
                    inputBean.setPasswordpolicydes("--");
                } catch (Exception ex) {
                    inputBean.setPasswordpolicydes("--");
                }
                inputBean.setMinimumlength(penArray[1].trim());
                inputBean.setMaximumlength(penArray[2].trim());
                inputBean.setMinimumspecialcharacters(penArray[3].trim());
                inputBean.setMinimumuppercasecharacters(penArray[4].trim());
                inputBean.setMinimumlowercasecharacters(penArray[5].trim());
                inputBean.setMinimumnumericalcharacters(penArray[6].trim());
                inputBean.setRepeatcharactersallow(penArray[7].trim());
                if (!penArray[0].trim().equals(CommonVarList.MOB_USER_NAME_POLICY_ID)) {
                    inputBean.setPasswordexpiryperiod(penArray[8].trim());
                    inputBean.setMinimumpasswordchangeperiod(penArray[9].trim());
                    inputBean.setNoofhistorypassword(penArray[10].trim());
                    inputBean.setIdleaccountexpiryperiod(penArray[11].trim());
                    inputBean.setNoofinvalidloginattempt(penArray[12].trim());
                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
}
