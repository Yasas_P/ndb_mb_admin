/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.FeesChargeBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FeesChargeInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FeesChargePendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.FeesCharges;
import com.epic.ndb.util.mapping.FeesChargesId;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.TransferType;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class FeesChargeDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<FeesChargeBean> getSearchList(FeesChargeInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<FeesChargeBean> dataList = new ArrayList<FeesChargeBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(u.id.chargeCode) from FeesCharges as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from FeesCharges u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    FeesChargeBean feesChargeBean = new FeesChargeBean();
                    FeesCharges feesChargeDetail = (FeesCharges) it.next();

                    try {
                        feesChargeBean.setChargeCode(feesChargeDetail.getId().getChargeCode().toString());
                    } catch (NullPointerException npe) {
                        feesChargeBean.setChargeCode("--");
                    }
                    try {
                        feesChargeBean.setTranTypeId(feesChargeDetail.getId().getTransferType().toString());
                    } catch (NullPointerException npe) {
                        feesChargeBean.setTranTypeId("--");
                    }
                    try {
                        feesChargeBean.setStatus(feesChargeDetail.getStatus().getDescription());
                    } catch (Exception npe) {
                        feesChargeBean.setStatus("--");
                    }
                    try {
                        feesChargeBean.setShortName(feesChargeDetail.getShortName().toString());
                    } catch (Exception npe) {
                        feesChargeBean.setShortName("--");
                    }
                    try {
                        feesChargeBean.setDescription(feesChargeDetail.getDescription().toString());
                    } catch (Exception npe) {
                        feesChargeBean.setDescription("--");
                    }
                    try {
                        feesChargeBean.setChargeAmount(feesChargeDetail.getChargeAmount().toString());
                    } catch (Exception npe) {
                        feesChargeBean.setChargeAmount("--");
                    }
                    try {
                        feesChargeBean.setTransferType(feesChargeDetail.getTransferType().getDescription());
                    } catch (Exception npe) {
                        feesChargeBean.setTransferType("--");
                    }
                    try {
                        feesChargeBean.setMaker(feesChargeDetail.getMaker().toString());
                    } catch (NullPointerException npe) {
                        feesChargeBean.setMaker("--");
                    }
                    try {
                        feesChargeBean.setChecker(feesChargeDetail.getChecker().toString());
                    } catch (NullPointerException npe) {
                        feesChargeBean.setChecker("--");
                    }
                    try {
                        feesChargeBean.setCreatedtime(feesChargeDetail.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        feesChargeBean.setCreatedtime("--");
                    }
                    try {
                        feesChargeBean.setLastupdatedtime(feesChargeDetail.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        feesChargeBean.setLastupdatedtime("--");
                    }

                    feesChargeBean.setFullCount(count);

                    dataList.add(feesChargeBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(FeesChargeInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getChargeCodeSearch() != null && !inputBean.getChargeCodeSearch().isEmpty()) {
            where += " and lower(u.id.chargeCode) like lower('%" + inputBean.getChargeCodeSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getTransferTypeSearch() != null && !inputBean.getTransferTypeSearch().isEmpty()) {
            where += " and u.transferType.transferId = '" + inputBean.getTransferTypeSearch() + "'";
        }
        if (inputBean.getShortNameSearch() != null && !inputBean.getShortNameSearch().isEmpty()) {
            where += " and lower(u.shortName) like lower('%" + inputBean.getShortNameSearch().trim() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch().trim() + "%')";
        }
        if (inputBean.getChargeAmountSearch() != null && !inputBean.getChargeAmountSearch().isEmpty()) {
            where += " and lower(u.chargeAmount) like lower('%" + inputBean.getChargeAmountSearch().trim() + "%')";
        }

        return where;
    }

    public List<FeesChargePendBean> getPendingFeesChargeList(FeesChargeInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<FeesChargePendBean> dataList = new ArrayList<FeesChargePendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.FEES_CHARGE_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.FEES_CHARGE_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    FeesChargePendBean feesCharge = new FeesChargePendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        feesCharge.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        feesCharge.setId("--");
                    }
                    try {
                        feesCharge.setOperationcode(pTask.getTask().getTaskcode().toString());
                        feesCharge.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        feesCharge.setOperationcode("--");
                        feesCharge.setOperation("--");
                    }

//                    String[] penArray = null;
//                    if (pTask.getFields() != null) {
//                        penArray = pTask.getFields().split("\\|");
//                    }
//
//                    try {
//                        cardCenter.setBankcode(penArray[0]);
//                    } catch (NullPointerException npe) {
//                        cardCenter.setBankcode("--");
//                    } catch (Exception ex) {
//                        cardCenter.setBankcode("--");
//                    }
                    String[] penKeyArray = null;
                    if (pTask.getPKey() != null) {
                        penKeyArray = pTask.getPKey().split("\\|");
                    }
                    try {
                        feesCharge.setChargeCode(penKeyArray[0].toString());
                    } catch (Exception ex) {
                        feesCharge.setChargeCode("--");
                    }
                    try {
                        TransferType transferType = (TransferType) session.get(TransferType.class, penKeyArray[1].toString());
                        feesCharge.setTransferType(transferType.getDescription());
                    } catch (Exception ex) {
                        feesCharge.setTransferType("--");
                    }
                    try {
                        feesCharge.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        feesCharge.setFields("--");
                    } catch (Exception ex) {
                        feesCharge.setFields("--");
                    }
                    try {
                        feesCharge.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        feesCharge.setStatus("--");
                    }
                    try {
                        feesCharge.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        feesCharge.setCreatetime("--");
                    }
                    try {
                        feesCharge.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        feesCharge.setCreateduser("--");
                    } catch (Exception ex) {
                        feesCharge.setCreateduser("--");
                    }

                    feesCharge.setFullCount(count);

                    dataList.add(feesCharge);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertFeesCharge(FeesChargeInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            FeesChargesId id = new FeesChargesId();
            id.setChargeCode(inputBean.getChargeCode().trim());
            id.setTransferType(inputBean.getTransferType().trim());

            if ((FeesCharges) session.get(FeesCharges.class, id) == null) {
                String pKey = inputBean.getChargeCode().trim() + "|" + inputBean.getTransferType().trim();
                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", pKey)
                        .setString("pagecode", audit.getPagecode());
                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(pKey);
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.FEES_CHARGE_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteFeesCharge(FeesChargeInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String pKey = inputBean.getChargeCode().trim() + "|" + inputBean.getTransferType().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                FeesChargesId id = new FeesChargesId();
                id.setChargeCode(inputBean.getChargeCode().trim());
                id.setTransferType(inputBean.getTransferType().trim());

                FeesCharges u = (FeesCharges) session.get(FeesCharges.class, id);
                if (u != null) {

                    audit.setNewvalue(u.getId().getChargeCode() + "|" + u.getId().getTransferType() + "|" + u.getShortName() + "|" + u.getStatus().getStatuscode() + "|" + u.getDescription() + "|" + u.getChargeAmount()
                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                    );
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(pKey);
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.FEES_CHARGE_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public FeesCharges findFeesChargeById(String chargeCode, String transferType) throws Exception {
        FeesCharges feesCharges = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from FeesCharges as u where u.id.chargeCode=:chargeCode and u.id.transferType=:transferType ";
            Query query = session.createQuery(sql).setString("chargeCode", chargeCode).setString("transferType", transferType);
            feesCharges = (FeesCharges) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return feesCharges;

    }

    public String updateFeesCharge(FeesChargeInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String pKey = inputBean.getChargeCode().trim() + "|" + inputBean.getTransferType().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                FeesChargesId id = new FeesChargesId();
                id.setChargeCode(inputBean.getChargeCode().trim());
                id.setTransferType(inputBean.getTransferType().trim());

                FeesCharges u = (FeesCharges) session.get(FeesCharges.class, id);
                if (u != null) {

//                    audit.setOldvalue(u.getId().getChargeCode() + "|" + u.getId().getTransferType() + "|" + u.getShortName() + "|" + u.getStatus().getStatuscode() + "|" + u.getDescription() + "|" + u.getChargeAmount()
//                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
//                    );

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(pKey);
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.FEES_CHARGE_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmFeesCharge(FeesChargeInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());
                    
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    FeesCharges feesCharges = new FeesCharges();
                    FeesChargesId feesChargesId = new FeesChargesId();

//                    feesCharges.setChargeCode(penArray[0]);
                    feesChargesId.setChargeCode(penArray[0]);
                    feesChargesId.setTransferType(penArray[1]);

                    feesCharges.setId(feesChargesId);

                    feesCharges.setShortName(penArray[2]);

                    Status st = (Status) session.get(Status.class, penArray[3]);
                    feesCharges.setStatus(st);

                    feesCharges.setDescription(penArray[4]);
                    feesCharges.setChargeAmount(new BigDecimal(penArray[5]));

//                    TransferType transferType = (TransferType) session.get(TransferType.class, penArray[5]);
//                    feesCharges.setTransferType(transferType);
                    feesCharges.setCreatedtime(timestampDate);
                    feesCharges.setLastupdatedtime(timestampDate);
                    feesCharges.setMaker(pentask.getCreateduser());
                    feesCharges.setChecker(audit.getLastupdateduser());

                    session.save(feesCharges);

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on fee charge( Charge code: " + penArray[0] + ", Transfer type: " + penArray[1] + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    FeesChargesId feesChargesId = new FeesChargesId();

                    feesChargesId.setChargeCode(penArray[0]);
                    feesChargesId.setTransferType(penArray[1]);

                    FeesCharges u = (FeesCharges) session.get(FeesCharges.class, feesChargesId);

                    if (u != null) {
                        //------------------------audit old value------------
                        String oldVal=penArray[0] + "|"
                            + penArray[1] + "|"
                            + u.getShortName() + "|"
                            + u.getStatus().getStatuscode() + "|"
                            + u.getDescription() + "|"
                            + u.getChargeAmount().toString();
                        
                        u.setShortName(penArray[2]);

                        Status st = (Status) session.get(Status.class, penArray[3]);
                        u.setStatus(st);

                        u.setDescription(penArray[4]);
                        u.setChargeAmount(new BigDecimal(penArray[5]));

//                        TransferType transferType = (TransferType) session.get(TransferType.class, penArray[5]);
//                        u.setTransferType(transferType);
                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        session.update(u);

                        audit.setOldvalue(oldVal);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on fee charge( Charge code: " + penArray[0] + ", Transfer type: " + penArray[1] + ") inputted by " + pentask.getCreateduser() + " approved "  + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
                    FeesChargesId feesChargesId = new FeesChargesId();

                    feesChargesId.setChargeCode(penArray[0]);
                    feesChargesId.setTransferType(penArray[1]);

                    FeesCharges u = (FeesCharges) session.get(FeesCharges.class, feesChargesId);
                    if (u != null) {
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on fee charge( Charge code: " + penArray[0] + ", Transfer type: " + penArray[1] + ") inputted by " + pentask.getCreateduser() + " approved "  + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectFeesCharge(FeesChargeInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] penKeyArray = new String[10];
                if (u.getPKey() != null) {
                    penKeyArray = u.getPKey().split("\\|");
                }
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    FeesChargesId feesChargesId = new FeesChargesId();

                    feesChargesId.setChargeCode(penKeyArray[0]);
                    feesChargesId.setTransferType(penKeyArray[1]);

                    FeesCharges feeCharge = (FeesCharges) session.get(FeesCharges.class, feesChargesId);

                    if (feeCharge != null) {
                        //------------------------audit old value------------
                        String oldVal=penKeyArray[0] + "|"
                            + penKeyArray[1] + "|"
                            + feeCharge.getShortName() + "|"
                            + feeCharge.getStatus().getStatuscode() + "|"
                            + feeCharge.getDescription() + "|"
                            + feeCharge.getChargeAmount().toString();
                        audit.setOldvalue(oldVal);
                    }
                }
                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on fee charge( Charge code: " + penKeyArray[0] + ", Transfer type: " + penKeyArray[1] +") inputted by " + u.getCreateduser() + " rejected "   + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
    
    public String getPendOldNewValueOfFeesCharge(FeesChargeInputBean inputBean) throws Exception {
        Session session = null;
        String message ="";
        try {
            session = HibernateInit.sessionFactory.openSession();
             Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                
                inputBean.setTaskCode(u.getTask().getTaskcode());
                inputBean.setTaskDescription(u.getTask().getDescription());
                String[] penArray = null;
                if (u.getFields() != null) {
                    penArray = u.getFields().split("\\|");
                }
                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
                    if(penArray[0]!=null && !penArray[0].isEmpty()){
                        inputBean.setChargeCode(penArray[0].trim());
                    }else{
                        inputBean.setChargeCode("--");
                    }
                    
                    if(penArray[1]!=null && !penArray[1].isEmpty()){
                        TransferType transferType=(TransferType)session.get(TransferType.class, penArray[1].trim());
                        if(transferType!=null){
                            inputBean.setTransferType(transferType.getDescription());
                        }else{
                            inputBean.setTransferType("--");
                        }
                    }else{
                        inputBean.setTransferType("--");
                    }
                    //------------------------new value---------------
                    if(penArray[2]!=null && !penArray[2].isEmpty()){
                        inputBean.setShortName(penArray[2].trim());
                    }else{
                        inputBean.setShortName("--");
                    }
                    
                    if(penArray[3]!=null && !penArray[3].isEmpty()){
                        Status st = (Status) session.get(Status.class, penArray[3]);
                        if(st!=null){
                            inputBean.setStatus(st.getDescription());
                        }else{
                            inputBean.setStatus("--");
                        }
                    }else{
                        inputBean.setStatus("--");
                    }
                    
                    if(penArray[4]!=null && !penArray[4].isEmpty()){
                        inputBean.setDescription(penArray[4].trim());
                    }else{
                        inputBean.setDescription("--");
                    }
                    
                    if(penArray[5]!=null && !penArray[5].isEmpty()){
                        inputBean.setChargeAmount(penArray[5].trim());
                    }else{
                        inputBean.setChargeAmount("--");
                    }
                    
                    //---------------------------old value-----------------------
                    inputBean.setShortNameOld("--");
                    inputBean.setStatusOld("--");
                    inputBean.setDescriptionOld("--");
                    inputBean.setChargeAmountOld("--");
                    
                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    if(penArray[0]!=null && !penArray[0].isEmpty()){
                        inputBean.setChargeCode(penArray[0].trim());
                    }else{
                        inputBean.setChargeCode("--");
                    }
                    
                    if(penArray[1]!=null && !penArray[1].isEmpty()){
                        TransferType transferType=(TransferType)session.get(TransferType.class, penArray[1].trim());
                        if(transferType!=null){
                            inputBean.setTransferType(transferType.getDescription());
                        }else{
                            inputBean.setTransferType("--");
                        }
                    }else{
                        inputBean.setTransferType("--");
                    }
                    //-----------------------------new value-------------------------------------//
                    
                    if(penArray[2]!=null && !penArray[2].isEmpty()){
                        inputBean.setShortName(penArray[2].trim());
                    }else{
                        inputBean.setShortName("--");
                    }
                    
                    if(penArray[3]!=null && !penArray[3].isEmpty()){
                        Status st = (Status) session.get(Status.class, penArray[3]);
                        if(st!=null){
                            inputBean.setStatus(st.getDescription());
                        }else{
                            inputBean.setStatus("--");
                        }
                    }else{
                        inputBean.setStatus("--");
                    }
                    
                    if(penArray[4]!=null && !penArray[4].isEmpty()){
                        inputBean.setDescription(penArray[4].trim());
                    }else{
                        inputBean.setDescription("--");
                    }
                    
                    if(penArray[5]!=null && !penArray[5].isEmpty()){
                        inputBean.setChargeAmount(penArray[5].trim());
                    }else{
                        inputBean.setChargeAmount("--");
                    }
                    //-----------------------------old value-------------------------------------//
                    FeesChargesId feesChargesId = new FeesChargesId();

                    feesChargesId.setChargeCode(penArray[0]);
                    feesChargesId.setTransferType(penArray[1]);

                    FeesCharges feecharge = (FeesCharges) session.get(FeesCharges.class, feesChargesId);
                    
                    if(feecharge.getShortName()!=null && !feecharge.getShortName().isEmpty()){
                        inputBean.setShortNameOld(feecharge.getShortName().trim());
                    }else{
                        inputBean.setShortNameOld("--");
                    }
                    
                    if(feecharge.getStatus()!=null ){
                        inputBean.setStatusOld(feecharge.getStatus().getDescription().trim());
                    }else{
                        inputBean.setStatusOld("--");
                    }
                    
                    if(feecharge.getDescription()!=null && !feecharge.getDescription().isEmpty()){
                        inputBean.setDescriptionOld(feecharge.getDescription().trim());
                    }else{
                        inputBean.setDescriptionOld("--");
                    }
                    
                    if(feecharge.getChargeAmount()!=null ){
                        inputBean.setChargeAmountOld(feecharge.getChargeAmount().toString());
                    }else{
                        inputBean.setChargeAmountOld("--");
                    }
                }
            }else{
              message="Record not exist.";  
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }    
        return message;
    }
}
