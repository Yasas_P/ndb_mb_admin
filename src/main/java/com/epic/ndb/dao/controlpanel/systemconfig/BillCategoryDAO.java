/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.BillCategoryBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BillCategoryInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.BillCategoryPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.BillerCategory;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author prathibha_w
 */
public class BillCategoryDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<BillCategoryBean> getSearchList(BillCategoryInputBean inputBean, int max, int first, String orderBy) {
        List<BillCategoryBean> dataList = new ArrayList<BillCategoryBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.lastupdatedtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(billercatcode) from BillerCategory as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from BillerCategory u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    BillCategoryBean jcode = new BillCategoryBean();
                    BillerCategory jc = (BillerCategory) it.next();

                    try {
                        jcode.setBillercatcode(jc.getBillercatcode());
                    } catch (NullPointerException npe) {
                        jcode.setBillercatcode("--");
                    }
                    try {
                        jcode.setName(jc.getName());
                    } catch (NullPointerException npe) {
                        jcode.setName("--");
                    }
                    try {
                        jcode.setDescription(jc.getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setDescription("--");
                    }
                    try {
                        Status u = (Status) session.get(Status.class, jc.getStatus());
                        jcode.setStatus(u.getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setMaker(jc.getMaker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setMaker("--");
                    }
                    try {
                        jcode.setChecker(jc.getChecker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setChecker("--");
                    }
                    try {
                        jcode.setCreatedtime(jc.getCreatetime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setCreatedtime("--");
                    }
                    try {
                        jcode.setLastupdatedtime(jc.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setLastupdatedtime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(BillCategoryInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getBillercatcodeSearch() != null && !inputBean.getBillercatcodeSearch().isEmpty()) {
            where += " and lower(u.billercatcode) like lower('%" + inputBean.getBillercatcodeSearch().trim() + "%')";
        }
        if (inputBean.getNameSearch() != null && !inputBean.getNameSearch().isEmpty()) {
            where += " and lower(u.name) like lower('%" + inputBean.getNameSearch().trim() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status = '" + inputBean.getStatusSearch() + "'";
        }
        return where;
    }

    public List<BillCategoryPendBean> getPendingBillCategoryList(BillCategoryInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<BillCategoryPendBean> dataList = new ArrayList<BillCategoryPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.BILLER_CATEGORY_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.BILLER_CATEGORY_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    BillCategoryPendBean jcode = new BillCategoryPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        jcode.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        jcode.setBillercatcode(penArray[0]);
                    } catch (NullPointerException npe) {
                        jcode.setBillercatcode("--");
                    } catch (Exception ex) {
                        jcode.setBillercatcode("--");
                    }
                    try {
                        jcode.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        jcode.setFields("--");
                    } catch (Exception ex) {
                        jcode.setFields("--");
                    }
                    try {
                        jcode.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
           if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return dataList;
    }

    public String insertBillCategory(BillCategoryInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((BillerCategory) session.get(BillerCategory.class, inputBean.getBillercatcode().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getBillercatcode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getBillercatcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.BILLER_CATEGORY_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String deleteBillCategory(BillCategoryInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getBillercatcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                /*---------------Is already use in parent tables----------------------*/
                String sql2 = "from BillServiceProvider as u where u.billCategory.billercatcode=:billercatcode";
                Query query2 = session.createQuery(sql2)
                        .setString("billercatcode", inputBean.getBillercatcode());

                if (query2.list().isEmpty()) {

                    BillerCategory u = (BillerCategory) session.get(BillerCategory.class, inputBean.getBillercatcode().trim());
                    if (u != null) {
                        audit.setNewvalue(u.getBillercatcode() + "|" + u.getName() + "|" + u.getDescription() + "|" + u.getStatus());
                    }

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getBillercatcode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.DELETE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.BILLER_CATEGORY_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_ALREADY_IN_USE;
                }
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String updateBillCategory(BillCategoryInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getBillercatcode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                BillerCategory u = (BillerCategory) session.get(BillerCategory.class, inputBean.getBillercatcode().trim());
                if (u != null) {
                    audit.setOldvalue(u.getBillercatcode() + "|" + u.getName() + "|" + u.getDescription() + "|" + u.getStatus());
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getBillercatcode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.BILLER_CATEGORY_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String confirmBillCategory(BillCategoryInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    BillerCategory jcode = new BillerCategory();
                    jcode.setBillercatcode(penArray[0]);
                    jcode.setName(penArray[1]);
                    jcode.setDescription(penArray[2]);
                    jcode.setStatus(penArray[3].trim());

                    jcode.setCreatetime(sysDate);
                    jcode.setLastupdatedtime(sysDate);
                    jcode.setMaker(pentask.getCreateduser());
                    jcode.setChecker(audit.getLastupdateduser());

                    audit.setNewvalue(jcode.getBillercatcode() + "|" + jcode.getName() + "|" + jcode.getDescription() + "|" + jcode.getStatus());
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on bill category ( bill category : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    session.save(jcode);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    BillerCategory u = (BillerCategory) session.get(BillerCategory.class, penArray[0].trim());

                    if (u != null) {

                        audit.setOldvalue(u.getBillercatcode() + "|" + u.getName() + "|" + u.getDescription() + "|" + u.getStatus());

                        u.setName(penArray[1].trim());
                        u.setDescription(penArray[2].trim());

                        u.setStatus(penArray[3].trim());

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getName() + "|" + u.getDescription() + "|" + u.getStatus());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on bill category ( bill category : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    BillerCategory u = (BillerCategory) session.get(BillerCategory.class, penArray[0]);

                    if (u != null) {

                        u.setName(penArray[1].trim());
                        u.setDescription(penArray[2].trim());
                        u.setStatus(penArray[3].trim());
                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getName() + "|" + u.getDescription() + "|" + u.getStatus());

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on bill category ( bill category : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            } 
        }
        return message;
    }

    public String rejectBillCategory(BillCategoryInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String code = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    BillerCategory billerCategory = (BillerCategory) session.get(BillerCategory.class, code.trim());

                    if (billerCategory != null) {

                        audit.setOldvalue(billerCategory.getBillercatcode() + "|" + billerCategory.getName() + "|" + billerCategory.getDescription() + "|" + billerCategory.getStatus());
                    }
                }
//                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on bill category (bill category : " + code + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public BillerCategory findBillCategoryById(String billercatcode) {
        BillerCategory bill = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from BillerCategory as u where u.billercatcode=:billercatcode";
            Query query = session.createQuery(sql).setString("billercatcode", billercatcode);
            System.out.print(query);
            bill = (BillerCategory) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return bill;
    }

    public boolean isContainBillServiceProviders(String billercatcode) {
        Boolean checker = false;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from BillServiceProvider as u where u.billCategory.billercatcode=:billercatcode";
            Query query = session.createQuery(sql).setString("billercatcode", billercatcode);
            if (!query.list().isEmpty()) {
                checker = true;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return checker;
    }

}
