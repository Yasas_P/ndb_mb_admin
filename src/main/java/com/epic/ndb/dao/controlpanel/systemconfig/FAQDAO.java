/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.FAQBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FAQInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FAQPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Faq;
import com.epic.ndb.util.mapping.FaqTemp;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class FAQDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<FAQBean> getSearchList(FAQInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<FAQBean> dataList = new ArrayList<FAQBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.lastupdatedtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Faq as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Faq u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    FAQBean jcode = new FAQBean();
                    Faq jc = (Faq) it.next();

                    try {
                        jcode.setId(jc.getId().toString());
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setQuestion(jc.getQuestion());
                    } catch (NullPointerException npe) {
                        jcode.setQuestion("--");
                    }
                    try {
                        jcode.setAnswer(jc.getAnswer());
                    } catch (NullPointerException npe) {
                        jcode.setAnswer("--");
                    }
                    try {
                        jcode.setStatus(jc.getStatus().getDescription());
                    } catch (Exception npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setMaker(jc.getMaker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setMaker("--");
                    }
                    try {
                        jcode.setChecker(jc.getChecker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setChecker("--");
                    }
                    try {
                        jcode.setCreatedtime(jc.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setCreatedtime("--");
                    }
                    try {
                        jcode.setLastupdatedtime(jc.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setLastupdatedtime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(FAQInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getIdSearch() != null && !inputBean.getIdSearch().isEmpty()) {
            where += " and lower(u.id) like lower('%" + inputBean.getIdSearch().trim() + "%')";
        }
        if (inputBean.getQuestionSearch() != null && !inputBean.getQuestionSearch().isEmpty()) {
            where += " and lower(u.question) like lower('%" + inputBean.getQuestionSearch().trim() + "%')";
        }
        if (inputBean.getAnswerSearch() != null && !inputBean.getAnswerSearch().isEmpty()) {
            where += " and lower(u.answer) like lower('%" + inputBean.getAnswerSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        return where;
    }

    public List<FAQPendBean> getPendingFAQList(FAQInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<FAQPendBean> dataList = new ArrayList<FAQPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from FaqTemp as u where u.maker!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from FaqTemp u where u.maker!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    FAQPendBean jcode = new FAQPendBean();
                    FaqTemp faqTemp = (FaqTemp) it.next();

                    try {
                        jcode.setId(faqTemp.getId().toString());
                        jcode.setFaqId(faqTemp.getId().toString());
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                        jcode.setFaqId("--");
                    }
                    try {
                        jcode.setOperation(faqTemp.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                    }

                    try {
                        jcode.setAnswer(faqTemp.getAnswer());
                    } catch (NullPointerException npe) {
                        jcode.setAnswer("--");
                    } catch (Exception ex) {
                        jcode.setAnswer("--");
                    }
                    try {
                        jcode.setQuestion(faqTemp.getQuestion());
                    } catch (NullPointerException npe) {
                        jcode.setQuestion("--");
                    } catch (Exception ex) {
                        jcode.setQuestion("--");
                    }
                    try {
                        jcode.setStatus(faqTemp.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(faqTemp.getMaker());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(faqTemp.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertFAQ(FAQInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((Faq) session.get(Faq.class, new BigDecimal(inputBean.getId().trim())) == null) {
                if ((FaqTemp) session.get(FaqTemp.class, new BigDecimal(inputBean.getId().trim())) == null) {
                    txn = session.beginTransaction();

                    FaqTemp faqTemp = new FaqTemp();
                    faqTemp.setId(new BigDecimal(inputBean.getId().trim()));
                    faqTemp.setQuestion(inputBean.getQuestion());
                    faqTemp.setAnswer(inputBean.getAnswer());

                    Status st = (Status) session.get(Status.class, inputBean.getStatus());
                    faqTemp.setStatus(st);

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    faqTemp.setTask(task);

                    faqTemp.setInputterbranch(sysUser.getBranch());

                    faqTemp.setCreatedtime(sysDate);
                    faqTemp.setLastupdatedtime(sysDate);
                    faqTemp.setMaker(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(faqTemp);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public Faq findFAQById(String faqId) throws Exception {
        Faq faq = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Faq as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", faqId);
            System.out.print(query);
            faq = (Faq) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return faq;

    }

    public FaqTemp findFAQTempById(String faqId) throws Exception {
        FaqTemp faqTemp = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from FaqTemp as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", faqId);
            System.out.print(query);
            faqTemp = (FaqTemp) query.list().get(0);
            faqTemp.getTask().getDescription();//don't remove
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return faqTemp;

    }

    public String updateFAQ(FAQInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            FaqTemp temp = (FaqTemp) session.get(FaqTemp.class, new BigDecimal(inputBean.getId().trim()));
            if (temp == null) {
                Faq u = (Faq) session.get(Faq.class, new BigDecimal(inputBean.getId().trim()));

                if (u != null) {
                    FaqTemp faqTemp = new FaqTemp();

                    faqTemp.setId(new BigDecimal(inputBean.getId().trim()));
                    faqTemp.setQuestion(inputBean.getQuestion());
                    faqTemp.setAnswer(inputBean.getAnswer());

                    Status st = (Status) session.get(Status.class, inputBean.getStatus());
                    faqTemp.setStatus(st);

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    faqTemp.setTask(task);

                    faqTemp.setMaker(audit.getLastupdateduser());
                    faqTemp.setCreatedtime(sysDate);
                    faqTemp.setLastupdatedtime(sysDate);

                    faqTemp.setInputterbranch(sysUser.getBranch());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    session.save(audit);
                    session.save(faqTemp);

                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }

            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteFAQ(FAQInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            FaqTemp temp = (FaqTemp) session.get(FaqTemp.class, new BigDecimal(inputBean.getId().trim()));
            if (temp == null) {
                Faq u = (Faq) session.get(Faq.class, new BigDecimal(inputBean.getId().trim()));
                if (u != null) {
                    FaqTemp faqTemp = new FaqTemp();

                    faqTemp.setId(u.getId());
                    faqTemp.setQuestion(u.getQuestion());
                    faqTemp.setAnswer(u.getAnswer());
                    faqTemp.setStatus(u.getStatus());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.DELETE_TASK);
                    faqTemp.setTask(task);

                    faqTemp.setMaker(audit.getLastupdateduser());
                    faqTemp.setCreatedtime(sysDate);
                    faqTemp.setLastupdatedtime(sysDate);

                    faqTemp.setInputterbranch(sysUser.getBranch());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    //------------------------------------audit new value-----------------------------
                    audit.setNewvalue(u.getId() + "|" + u.getStatus().getStatuscode() + "|" + u.getQuestion() + "|" + u.getAnswer());

                    session.save(faqTemp);
                    session.save(audit);
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }

            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmFAQ(FAQInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            FaqTemp pentask = (FaqTemp) session.get(FaqTemp.class, new BigDecimal(inputBean.getId().trim()));

            if (pentask != null) {
                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    Faq jcode = new Faq();
                    jcode.setId(pentask.getId());
                    jcode.setQuestion(pentask.getQuestion());
                    jcode.setAnswer(pentask.getAnswer());

                    jcode.setStatus(pentask.getStatus());

                    jcode.setCreatedtime(sysDate);
                    jcode.setLastupdatedtime(sysDate);
                    jcode.setMaker(pentask.getMaker());
                    jcode.setChecker(audit.getLastupdateduser());

                    //------------------------------------audit new value-----------------------------
                    audit.setNewvalue(jcode.getId() + "|" + jcode.getStatus().getStatuscode() + "|" + jcode.getQuestion() + "|" + jcode.getAnswer());

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on FAQ (ID:  " + jcode.getId() + ") inputted by " + pentask.getMaker() + " approved " + audit.getDescription());

                    session.save(jcode);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Faq u = (Faq) session.get(Faq.class, pentask.getId());

                    if (u != null) {

                        //------------------------------------audit old value-----------------------------
                        audit.setOldvalue(u.getId() + "|" + u.getStatus().getStatuscode() + "|" + u.getQuestion() + "|" + u.getAnswer());

                        u.setQuestion(pentask.getQuestion());
                        u.setAnswer(pentask.getAnswer());

                        // Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(pentask.getStatus());

                        u.setMaker(pentask.getMaker());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        //------------------------------------audit new value-----------------------------
                        audit.setNewvalue(u.getId() + "|" + u.getStatus().getStatuscode() + "|" + u.getQuestion() + "|" + u.getAnswer());

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on FAQ (ID : " + inputBean.getId().trim() + ") inputted by " + pentask.getMaker() + " approved " + audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    Faq u = (Faq) session.get(Faq.class, pentask.getId());
                    if (u != null) {
                        //------------------------------------audit new value-----------------------------
                        audit.setNewvalue(u.getId() + "|" + u.getStatus().getStatuscode() + "|" + u.getQuestion() + "|" + u.getAnswer());

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on FAQ (ID : " + inputBean.getId().trim() + ") inputted by " + pentask.getMaker() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectFAQ(FAQInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            FaqTemp u = (FaqTemp) session.get(FaqTemp.class, new BigDecimal(inputBean.getId().trim()));

            if (u != null) {
                //------------------------------------audit new value-----------------------------
                audit.setNewvalue(u.getId().toString() + "|" + u.getStatus().getStatuscode() + "|" + u.getQuestion() + "|" + u.getAnswer());

                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Faq faq = (Faq) session.get(Faq.class, u.getId());

                    if (faq != null) {

                        //------------------------------------audit old value-----------------------------
                        audit.setOldvalue(faq.getId() + "|" + faq.getStatus().getStatuscode() + "|" + faq.getQuestion() + "|" + faq.getAnswer());
                    }
                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on FAQ (ID : " + u.getId().toString() + ") inputted by " + u.getMaker() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
}
