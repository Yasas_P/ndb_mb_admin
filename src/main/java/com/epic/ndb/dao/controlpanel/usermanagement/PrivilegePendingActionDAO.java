/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.usermanagement.PendingActionBean;
import com.epic.ndb.bean.controlpanel.usermanagement.PrivilegePendingDataBean;
import com.epic.ndb.bean.controlpanel.usermanagement.PrivilegePendingInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.UserRolePrivilegeInputBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pagetask;
import com.epic.ndb.util.mapping.PagetaskId;
import com.epic.ndb.util.mapping.PendingPagetask;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Section;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Userrole;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.PageVarList;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author dilanka_w
 */
public class PrivilegePendingActionDAO {

    public List<PendingActionBean> getSearchList(PrivilegePendingInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<PendingActionBean> dataList = new ArrayList<PendingActionBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by lower(UR.DESCRIPTION) ASC";
            }

            BigDecimal count = new BigDecimal(0);
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "SELECT COUNT(*) FROM(SELECT COUNT(ID) "
                    + "FROM WEB_PENDINGTASK PT "
                    + "WHERE "
                    + "PT.CREATEDUSER!=:createduser AND "
                    + "PT.STATUS=:status AND "
                    + "PT.PAGECODE=:page AND "
                    + where + " "
                    + "GROUP BY PT.P_KEY,PT.CREATEDUSER)";
            System.out.println(sqlCount);
            Query queryCount = session.createSQLQuery(sqlCount)
                    .setParameter("createduser", inputBean.getSysusername())
                    .setParameter("status", CommonVarList.STATUS_PENDING)
                    .setParameter("page", PageVarList.USER_ROLE_PRIVILEGE_MGT);

            List countList = queryCount.list();
            System.out.println("--------"+countList.get(0));
            count = (BigDecimal) countList.get(0);

            System.err.println(sqlCount);
            System.err.println(count);

            if (count.longValue() > 0) {

                String sqlSearch = "SELECT "
                        + "PT.P_KEY, " //pkey(user role code) -0
                        + "PT.CREATEDUSER uname, " //created user -1
                        + "S.DESCRIPTION pkey, " //status des -2
                        + "UR.DESCRIPTION udes, "//user role des -3
                        + "row_number() over ( " + orderBy + ") as r "
                        + "FROM WEB_PENDINGTASK PT "
                        + "LEFT OUTER JOIN STATUS S ON S.STATUSCODE=PT.STATUS "
                        + "LEFT OUTER JOIN WEB_USERROLE UR ON UR.USERROLECODE=PT.P_KEY "
                        + "WHERE "
                        + "PT.CREATEDUSER!=:createduser AND "
                        + "PT.STATUS=:status AND "
                        + "PT.PAGECODE=:page AND "
                        + where + " "
                        + "GROUP BY PT.P_KEY,PT.CREATEDUSER,S.DESCRIPTION,UR.DESCRIPTION";

                String sqlQ = " SELECT * from (" + sqlSearch + ") where r > " + first + " and r<= " + max;

                System.err.println(sqlQ);
                List<Object[]> chequeList = (List<Object[]>) session.createSQLQuery(sqlQ)
                        .setParameter("createduser", inputBean.getSysusername())
                        .setParameter("status", CommonVarList.STATUS_PENDING)
                        .setParameter("page", PageVarList.USER_ROLE_PRIVILEGE_MGT)
                        .list();

                for (Object[] stmtBean : chequeList) {

                    PendingActionBean pendAction = new PendingActionBean();
                    try {
                        pendAction.setPkey(stmtBean[0].toString());
                    } catch (NullPointerException npe) {
                        pendAction.setPkey("--");
                    }

                    try {
                        pendAction.setCreateduser(stmtBean[1].toString());
                    } catch (NullPointerException npe) {
                        pendAction.setCreateduser("--");
                    }

                    try {
                        pendAction.setStatusdes(stmtBean[2].toString());
                    } catch (NullPointerException npe) {
                        pendAction.setStatusdes("--");
                    }

                    try {
                        pendAction.setPkeydes(stmtBean[3].toString());
                    } catch (NullPointerException npe) {
                        pendAction.setPkeydes("--");
                    }

                    pendAction.setFullCount(count.longValue());

                    dataList.add(pendAction);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(PrivilegePendingInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getUsernameSearch() != null && !inputBean.getUsernameSearch().isEmpty()) {
            where += " and lower(PT.CREATEDUSER) like  lower('%" + inputBean.getUsernameSearch() + "%')";
        }
        if (inputBean.getPkeySearch() != null && !inputBean.getPkeySearch().isEmpty()) {
            where += " and PT.P_KEY = '" + inputBean.getPkeySearch() + "'";
        }

        return where;
    }

    public List<PendingPagetask> getPendingPageTaskList(String fieldId) throws Exception {

        List<PendingPagetask> pendingPageTaskList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from PendingPagetask as p where p.fieldId=:fieldId order by Upper(p.task.description) asc";
            Query query = session.createQuery(sql).setString("fieldId", fieldId);
            pendingPageTaskList = query.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return pendingPageTaskList;
    }

    public List<PrivilegePendingDataBean> getPendingtaskListByPkeyAndCreatedUser(PrivilegePendingInputBean inputBean, String systemuser, int max, int first) throws Exception {
        List<PrivilegePendingDataBean> dataList = new ArrayList<PrivilegePendingDataBean>();
        Session session = null;
        try {

            String orderBy = "ORDER BY PT.CREATEDTIME DESC";

            BigDecimal count = new BigDecimal(0);
            String where = this.makeWhereClauseForView(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "SELECT "
                    + "COUNT(PT.P_KEY) "
                    + "FROM WEB_PENDINGTASK PT "
                    + "LEFT OUTER JOIN WEB_PENDING_PAGETASK PP ON PP.FIELD_ID=PT.FIELD_ID "
                    + "LEFT OUTER JOIN WEB_PAGE P ON P.PAGECODE=PT.P_KEY1 "
                    + "LEFT OUTER JOIN WEB_SECTION S ON S.SECTIONCODE=PT.FIELDS "
                    + "LEFT OUTER JOIN WEB_TASK T ON T.TASKCODE = PP.TASKCODE "
                    + "WHERE "
                    + "PT.CREATEDUSER!=:systemuser AND "
                    + "PT.CREATEDUSER=:createduser AND "
                    + "PT.STATUS=:status AND "
                    + "PT.PAGECODE=:page AND "
                    + "PT.P_KEY=:pkey AND "
                    + where;

            Query queryCount = session.createSQLQuery(sqlCount)
                    .setParameter("systemuser", systemuser)
                    .setParameter("createduser", inputBean.getCreateduser())
                    .setParameter("status", CommonVarList.STATUS_PENDING)
                    .setParameter("page", PageVarList.USER_ROLE_PRIVILEGE_MGT)
                    .setParameter("pkey", inputBean.getId());

            List countList = queryCount.list();
            count = (BigDecimal) countList.get(0);

            System.err.println(sqlCount);
            System.err.println(count);

            if (count.longValue() > 0) {

                String sqlSearch = "SELECT "
                        + "PP.ID, "//0
                        + "S.DESCRIPTION section, "//1
                        + "P.DESCRIPTION page, "//2
                        + "T.DESCRIPTION task, "//3
                        + "PT.CREATEDTIME, "//4
                        + "row_number() over ( " + orderBy + ") as r "
                        + "FROM WEB_PENDINGTASK PT "
                        + "LEFT OUTER JOIN WEB_PENDING_PAGETASK PP ON PP.FIELD_ID=PT.FIELD_ID "
                        + "LEFT OUTER JOIN WEB_USERROLE UR ON UR.USERROLECODE=PT.P_KEY "
                        + "LEFT OUTER JOIN WEB_PAGE P ON P.PAGECODE=PT.P_KEY1 "
                        + "LEFT OUTER JOIN WEB_SECTION S ON S.SECTIONCODE=PT.FIELDS "
                        + "LEFT OUTER JOIN WEB_TASK T ON T.TASKCODE = PP.TASKCODE "
                        + "WHERE "
                        + "PT.CREATEDUSER!=:systemuser AND "
                        + "PT.CREATEDUSER=:createduser AND "
                        + "PT.STATUS=:status AND "
                        + "PT.PAGECODE=:page AND "
                        + "PT.P_KEY=:pkey AND "
                        + where;

                String sqlQ = " SELECT * from (" + sqlSearch + ") where r > " + first + " and r<= " + max;

                System.err.println(sqlQ);
                List<Object[]> chequeList = (List<Object[]>) session.createSQLQuery(sqlQ)
                        .setParameter("systemuser", systemuser)
                        .setParameter("createduser", inputBean.getCreateduser())
                        .setParameter("status", CommonVarList.STATUS_PENDING)
                        .setParameter("page", PageVarList.USER_ROLE_PRIVILEGE_MGT)
                        .setParameter("pkey", inputBean.getId())
                        .list();

                for (Object[] stmtBean : chequeList) {

                    PrivilegePendingDataBean pendDataBean = new PrivilegePendingDataBean();
                    try {
                        pendDataBean.setId(stmtBean[0].toString());
                    } catch (NullPointerException npe) {
                        pendDataBean.setId("--");
                    }

                    try {
                        pendDataBean.setSection(stmtBean[1].toString());
                    } catch (NullPointerException npe) {
                        pendDataBean.setSection("--");
                    }

                    try {
                        pendDataBean.setPage(stmtBean[2].toString());
                    } catch (NullPointerException npe) {
                        pendDataBean.setPage("--");
                    }

                    try {
                        pendDataBean.setTask(stmtBean[3].toString());
                    } catch (NullPointerException npe) {
                        pendDataBean.setTask("--");
                    }

                    try {
                        if (stmtBean[4].toString() != null && !stmtBean[4].toString().isEmpty()) {
                            pendDataBean.setCreatedtime(stmtBean[4].toString().substring(0, 19));
                        } else {
                            pendDataBean.setCreatedtime("--");
                        }
                    } catch (NullPointerException e) {
                        pendDataBean.setCreatedtime("--");
                    }

                    pendDataBean.setFullCount(count.longValue());

                    dataList.add(pendDataBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClauseForView(PrivilegePendingInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getSectionSearchView() != null && !inputBean.getSectionSearchView().isEmpty()) {
            where += " and lower(S.DESCRIPTION) like  lower('%" + inputBean.getSectionSearchView() + "%')";
        }
        if (inputBean.getPageSearchView() != null && !inputBean.getPageSearchView().isEmpty()) {
            where += " and lower(P.DESCRIPTION) like lower('%" + inputBean.getPageSearchView() + "%')";
        }
        if (inputBean.getTaskSearchView() != null && !inputBean.getTaskSearchView().isEmpty()) {
            where += " and lower(T.DESCRIPTION) like lower('%" + inputBean.getTaskSearchView() + "%')";
        }

        return where;
    }

    public String assignTask(UserRolePrivilegeInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Page pg = (Page) session.get(Page.class, inputBean.getPage().trim());
            Userrole ur = (Userrole) session.get(Userrole.class, inputBean.getUserRole().trim());
            Section se = (Section) session.get(Section.class, inputBean.getSectionpage().trim());

            /**
             * for audit new value
             */
            StringBuilder stringBuilderNew = new StringBuilder();
            stringBuilderNew.append(ur.getDescription())
                    .append("|").append(se.getDescription())
                    .append("|").append(pg.getDescription())
                    .append("|").append(inputBean.getNewBox());

            String sql = "from Pagetask as pt where pt.id.userrolecode =:userrolecode and pt.id.pagecode =:pagecode ";
            Query query = session.createQuery(sql).setString("userrolecode", inputBean.getUserRole()).setString("pagecode", inputBean.getPage());

            List<Pagetask> userSectionList = query.list();
            List<String> currSectionCodeList = inputBean.getNewBox();

            for (Pagetask pt : userSectionList) {

                if (currSectionCodeList.contains(pt.getId().getTaskcode().toString())) {

                    pt.setLastupdatedtime(sysDate);
                    pt.setLastupdateduser(audit.getLastupdateduser());
                    session.update(pt);

                    currSectionCodeList.remove(pt.getId().getTaskcode().toString());

                } else {

                    session.delete(pt);
                    session.flush();
                }
            }

            for (String taskcode : currSectionCodeList) {

                PagetaskId ptId = new PagetaskId(inputBean.getUserRole(), inputBean.getPage(), taskcode);

                Pagetask pt = new Pagetask();
                pt.setId(ptId);
                pt.setCreatetime(sysDate);
                pt.setLastupdatedtime(sysDate);
                pt.setLastupdateduser(audit.getLastupdateduser());
                session.save(pt);

            }

            audit.setNewvalue(stringBuilderNew.toString());
            audit.setCreatetime(sysDate);
            audit.setLastupdatedtime(sysDate);

            session.save(audit);
            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String approvePrivilege(PrivilegePendingInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            BigDecimal count = new BigDecimal(0);

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            /**
             * get pending task count using page,status-PEND,primary key
             */
            String sqlCount = "SELECT "
                    + "COUNT(*) "
                    + "FROM WEB_PENDINGTASK "
                    + "WHERE "
                    + "PAGECODE=:page AND "
                    + "STATUS=:status AND "
                    + "P_KEY=:id AND "
                    + "CREATEDUSER=:createduser";

            Query queryCount = session.createSQLQuery(sqlCount)
                    .setParameter("page", PageVarList.USER_ROLE_PRIVILEGE_MGT)
                    .setParameter("status", CommonVarList.STATUS_PENDING)
                    .setParameter("id", inputBean.getId())
                    .setParameter("createduser", inputBean.getCreateduser());

            List countList = queryCount.list();
            count = (BigDecimal) countList.get(0);

            if (count.longValue() > 0) {

                String sqlpagelist = "SELECT "
                        + "ID, "//0
                        + "P_KEY1, "//1
                        + "FIELD_ID, "//2
                        + "FIELDS "//3
                        + "FROM WEB_PENDINGTASK "
                        + "WHERE "
                        + "PAGECODE=:page AND "
                        + "STATUS=:status AND "
                        + "P_KEY=:id AND "
                        + "CREATEDUSER=:createduser";

                System.err.println(sqlpagelist);
                List<Object[]> pageList = (List<Object[]>) session.createSQLQuery(sqlpagelist)
                        .setParameter("page", PageVarList.USER_ROLE_PRIVILEGE_MGT)
                        .setParameter("status", CommonVarList.STATUS_PENDING)
                        .setParameter("id", inputBean.getId())
                        .setParameter("createduser", inputBean.getCreateduser())
                        .list();

                /**
                 * for audit new value
                 */
                String userrole = this.getUserRoleDesBycode(session, inputBean.getId());
                StringBuilder stringBuilderNew = new StringBuilder();
                stringBuilderNew.append(userrole).append("|");

                for (Object[] page : pageList) {

                    //audit ------------------
                    stringBuilderNew.append("[").append(this.getDescriptionBySectionCode(session, page[3].toString()))
                            .append("|").append(this.getDescriptionByPageCode(session, page[1].toString()))
                            .append("[");

                    String sql = "from Pagetask as pt where pt.id.userrolecode =:userrolecode and pt.id.pagecode =:pagecode ";
                    Query query = session.createQuery(sql).setString("userrolecode", inputBean.getId()).setString("pagecode", page[1].toString());

                    List<Pagetask> userPageTaskList = query.list();

                    String newTaskListsql = "SELECT "
                            + "TASKCODE "
                            + "FROM "
                            + "WEB_PENDING_PAGETASK "
                            + "WHERE "
                            + "FIELD_ID=:fieldId";

                    List<String> currTaskCodeList = (List<String>) session.createSQLQuery(newTaskListsql)
                            .setParameter("fieldId", page[2])
                            .list();

                    for (Pagetask pt : userPageTaskList) {

                        if (currTaskCodeList.contains(pt.getId().getTaskcode().toString())) {

                            pt.setLastupdatedtime(sysDate);
                            pt.setLastupdateduser(audit.getLastupdateduser());
                            session.update(pt);

                            currTaskCodeList.remove(pt.getId().getTaskcode().toString());

                            //audit ------------------------
                            stringBuilderNew.append(",").append(pt.getId().getTaskcode().toString());

                        } else {

                            session.delete(pt);
                            session.flush();
                        }
                    }

                    for (String taskcode : currTaskCodeList) {

                        PagetaskId ptId = new PagetaskId(inputBean.getId(), page[1].toString(), taskcode);

                        Pagetask pt = new Pagetask();
                        pt.setId(ptId);
                        pt.setCreatetime(sysDate);
                        pt.setLastupdatedtime(sysDate);
                        pt.setLastupdateduser(audit.getLastupdateduser());

                        //audit ------------------                       
                        stringBuilderNew.append(",").append(taskcode);

                        session.save(pt);
                    }

                    //audit ------------------
                    stringBuilderNew.append("]]");

                    /**
                     * update pending task table record
                     * ---------------------------------------------------
                     */
                    String sqlpendtask = "from Pendingtask as u where u.id=:id";
                    Query querypendtask = session.createQuery(sqlpendtask).setString("id", page[0].toString());
                    Pendingtask pentask = (Pendingtask) querypendtask.list().get(0);

                    Status approveStatus = (Status) session.get(Status.class, CommonVarList.STATUS_PENDING_CONFIRMED);
                    pentask.setStatus(approveStatus);

                    try {
                        pentask.setComments(inputBean.getComment().toString());

                    } catch (NullPointerException ex) {
                    }

                    pentask.setConfirmeduser(audit.getLastupdateduser());
                    pentask.setLastupdatedtime(sysDate);

                    session.update(pentask);

                }

                /**
                 * for audit
                 */
                stringBuilderNew.append("|Comment - ").append(inputBean.getComment().toString());
                audit.setNewvalue(stringBuilderNew.toString());

                StringBuilder auditDescription = new StringBuilder();
                auditDescription.append("Approved privileges on userrole '").append(userrole).append("' created by ").append(inputBean.getCreateduser());
                audit.setDescription(auditDescription.toString());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectPrivilege(PrivilegePendingInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {

            BigDecimal count = new BigDecimal(0);

            CommonDAO commonDAO = new CommonDAO();

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = commonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sqlCount = "SELECT "
                    + "COUNT(*) "
                    + "FROM WEB_PENDINGTASK "
                    + "WHERE "
                    + "PAGECODE=:page AND "
                    + "STATUS=:status AND "
                    + "P_KEY=:id AND "
                    + "CREATEDUSER=:createduser";

            Query queryCount = session.createSQLQuery(sqlCount)
                    .setParameter("page", PageVarList.USER_ROLE_PRIVILEGE_MGT)
                    .setParameter("status", CommonVarList.STATUS_PENDING)
                    .setParameter("id", inputBean.getId())
                    .setParameter("createduser", inputBean.getCreateduser());

            List countList = queryCount.list();
            count = (BigDecimal) countList.get(0);

            if (count.longValue() > 0) {

                String sqlpagelist = "SELECT "
                        + "ID, "
                        + "P_KEY1, "
                        + "FIELD_ID "
                        + "FROM WEB_PENDINGTASK "
                        + "WHERE "
                        + "PAGECODE=:page AND "
                        + "STATUS=:status AND "
                        + "P_KEY=:id AND "
                        + "CREATEDUSER=:createduser";

                System.err.println(sqlpagelist);
                List<Object[]> pageList = (List<Object[]>) session.createSQLQuery(sqlpagelist)
                        .setParameter("page", PageVarList.USER_ROLE_PRIVILEGE_MGT)
                        .setParameter("status", CommonVarList.STATUS_PENDING)
                        .setParameter("id", inputBean.getId())
                        .setParameter("createduser", inputBean.getCreateduser())
                        .list();

                for (Object[] page : pageList) {

                    String sqlpendtask = "from Pendingtask as u where u.id=:id";
                    Query querypendtask = session.createQuery(sqlpendtask).setString("id", page[0].toString());
                    Pendingtask pentask = (Pendingtask) querypendtask.list().get(0);

                    Status approveStatus = (Status) session.get(Status.class, CommonVarList.STATUS_PENDING_REJECTED);
                    pentask.setStatus(approveStatus);

                    pentask.setComments(inputBean.getComment().toString());

                    pentask.setConfirmeduser(audit.getLastupdateduser());
                    pentask.setLastupdatedtime(sysDate);
                    session.update(pentask);

                }

                StringBuilder descriptionAudit = new StringBuilder();
                descriptionAudit.append("Rejected privileges on userrole '").append(this.getUserRoleDesBycode(session, inputBean.getId()))
                        .append("' created by ").append(inputBean.getCreateduser());

                audit.setDescription(descriptionAudit.toString());
                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    private String getDescriptionBySectionCode(Session session, String sectioncode) {
        String description = "";

        try {

            String sql = "select DESCRIPTION from WEB_SECTION where SECTIONCODE=:sectioncode";
            Query query = session.createSQLQuery(sql).setParameter("sectioncode", sectioncode);
            description = (String) query.list().get(0);

        } catch (Exception e) {

        }

        return description;
    }

    public String getDescriptionByPageCode(Session session, String pagecode) {

        String description = "";

        try {

            String sql = "select DESCRIPTION from WEB_PAGE where PAGECODE=:pagecode";
            Query query = session.createSQLQuery(sql).setParameter("pagecode", pagecode);
            description = (String) query.list().get(0);

        } catch (Exception e) {

        }

        return description;
    }

    public String getUserRoleDesBycode(Session session, String userrolecode) {

        String description = "";

        try {

            String sql = "select DESCRIPTION from WEB_USERROLE where USERROLECODE=:userrolecode";
            Query query = session.createSQLQuery(sql).setParameter("userrolecode", userrolecode);
            description = (String) query.list().get(0);

        } catch (Exception e) {

        }

        return description;
    }

}
