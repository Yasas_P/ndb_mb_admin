/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.CustomerCatBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CustomerCatInpuBean;
import com.epic.ndb.bean.controlpanel.systemconfig.CustomerCatPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.CustomerCategories;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author eranga_j
 */
public class CustomerCatDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<CustomerCatBean> getSearchList(CustomerCatInpuBean inputBean, int max, int first, String orderBy) throws Exception {
        List<CustomerCatBean> dataList = new ArrayList<CustomerCatBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = " order by TT.LASTUPDATEDTIME desc ";
            }
            String where = this.makeWhereClause(inputBean);

            BigDecimal count = new BigDecimal(0);
            session = HibernateInit.sessionFactory.openSession();

//            String sqlCount = "select count(TT.TRANSFER_ID) from TRANSFER_TYPE TT where " + where;
            String sqlCount = "select count(TT.CODE) from CUSTOMER_CATEGORIES TT where " + where;

            Query queryCount = session.createSQLQuery(sqlCount);

            List countList = queryCount.list();
            count = (BigDecimal) countList.get(0);

            if (count.longValue() > 0) {

                String sqlSearch = " SELECT * from ( select TT.CODE , TT.DESCRIPTION, ST.DESCRIPTION AS STATUSDESC , TT.MAKER , TT.CHECKER , TT.CREATEDTIME , TT.LASTUPDATEDTIME, WA.DESCRIPTION AS WAVEOFFDESC, TT.CHARGE_AMOUNT , "
                        + " row_number() over (" + orderBy + ") as r "
                        + " from CUSTOMER_CATEGORIES TT,STATUS ST,STATUS WA "
                        //                        + " from TRANSFER_TYPE TT,STATUS ST "
                        + " where TT.STATUS=ST.STATUSCODE AND WA.STATUSCODE = TT.WAIVEOFF AND " + where + ") where r > " + first + " and r<= " + max;

                List<Object[]> chequeList = (List<Object[]>) session.createSQLQuery(sqlSearch).list();

                for (Object[] ttBean : chequeList) {

                    CustomerCatBean customerCatBean = new CustomerCatBean();

                    try {
                        customerCatBean.setCode(ttBean[0].toString());
                    } catch (NullPointerException npe) {
                        customerCatBean.setCode("--");
                    }
                    try {
                        customerCatBean.setDescription(ttBean[1].toString());
                    } catch (NullPointerException npe) {
                        customerCatBean.setDescription("--");
                    }
                    try {
                        customerCatBean.setStatus(ttBean[2].toString());
                    } catch (NullPointerException npe) {
                        customerCatBean.setStatus("--");
                    }
                    try {
                        customerCatBean.setMaker(ttBean[3].toString());
                    } catch (NullPointerException npe) {
                        customerCatBean.setMaker("--");
                    }
                    try {
                        customerCatBean.setChecker(ttBean[4].toString());
                    } catch (NullPointerException npe) {
                        customerCatBean.setChecker("--");
                    }
                    try {
                        customerCatBean.setCreatedtime(ttBean[5].toString().substring(0, 19));
                    } catch (Exception npe) {
                        customerCatBean.setCreatedtime("--");
                    }
                    try {
                        customerCatBean.setLastupdatedtime(ttBean[6].toString().substring(0, 19));
                    } catch (Exception npe) {
                        customerCatBean.setLastupdatedtime("--");
                    }
                    try {
                        customerCatBean.setWaveoff(ttBean[7].toString());
                    } catch (Exception npe) {
                        customerCatBean.setWaveoff("--");
                    }
                    try {
                        customerCatBean.setChargeAmount(ttBean[8].toString());
                    } catch (Exception npe) {
                        customerCatBean.setChargeAmount("--");
                    }

                    customerCatBean.setFullCount(count.longValue());

                    dataList.add(customerCatBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(CustomerCatInpuBean inputBean) throws Exception {

        String where = "1=1";

        if ((inputBean.getS_Code() == null || inputBean.getS_Code().isEmpty())
                && (inputBean.getS_description() == null || inputBean.getS_description().isEmpty())
                && (inputBean.getS_status() == null || inputBean.getS_status().isEmpty())
                && (inputBean.getS_sortkey() == null || inputBean.getS_sortkey().isEmpty())) {

        } else {

            if (inputBean.getS_Code() != null && !inputBean.getS_Code().isEmpty()) {
                where += " and TT.CODE like '%" + inputBean.getS_Code() + "%'";
//                where += " and TT.TRANSFER_ID like '%" + inputBean.getS_transactiontypecode() + "%'";
            }
            if (inputBean.getS_description() != null && !inputBean.getS_description().isEmpty()) {
                where += " and lower(TT.DESCRIPTION) like lower('%" + inputBean.getS_description() + "%')";
            }
            if (inputBean.getS_status() != null && !inputBean.getS_status().isEmpty()) {
                where += " and TT.STATUS='" + inputBean.getS_status() + "'";
            }

        }
        return where;
    }

    public String insertCustomerCategory(CustomerCatInpuBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((CustomerCategories) session.get(CustomerCategories.class, inputBean.getCode().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getCode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getCode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_CATEGORY_PAGE);
                    pendingtask.setPage(page);
                    
                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();

                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<CustomerCatPendBean> getPendingCustomerCategory(CustomerCatInpuBean inputBean, int max, int first, String orderBy) throws Exception {

        List<CustomerCatPendBean> dataList = new ArrayList<CustomerCatPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.CUSTOMER_CATEGORY_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.CUSTOMER_CATEGORY_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    CustomerCatPendBean jcode = new CustomerCatPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        jcode.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        jcode.setCode(penArray[0]);
                    } catch (NullPointerException npe) {
                        jcode.setCode("--");
                    } catch (Exception ex) {
                        jcode.setCode("--");
                    }
                    try {
                        jcode.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        jcode.setFields("--");
                    } catch (Exception ex) {
                        jcode.setFields("--");
                    }
                    try {
                        jcode.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public CustomerCategories findCustomerCategoryById(String code) throws Exception {
        CustomerCategories tt = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from CustomerCategories as u where u.code=:code";
            Query query = session.createQuery(sql).setString("code", code);
            tt = (CustomerCategories) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return tt;

    }

    public String updateCustomerCategory(CustomerCatInpuBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getCode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

//                CustomerCategories u = (CustomerCategories) session.get(CustomerCategories.class, inputBean.getCode().trim());
//                if (u != null) {
//
//                    audit.setOldvalue(u.getCode() + "|" + u.getDescription() + "|" + u.getStatus().getStatuscode() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());
//                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
//                }
                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_CATEGORY_PAGE);
                pendingtask.setPage(page);
                
                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteCustomerCategory(CustomerCatInpuBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getCode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                CustomerCategories u = (CustomerCategories) session.get(CustomerCategories.class, inputBean.getCode().trim());
                if (u != null) {

                    audit.setNewvalue(u.getCode() + "|" + u.getDescription() + "|" + u.getStatus().getStatuscode() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.CUSTOMER_CATEGORY_PAGE);
                pendingtask.setPage(page);
                
                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmCustomerCategory(CustomerCatInpuBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = null;
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    CustomerCategories jcode = new CustomerCategories();
                    jcode.setCode(penArray[0]);
                    jcode.setDescription(penArray[1]);

                    Status st = (Status) session.get(Status.class, penArray[2].trim());
                    jcode.setStatus(st);

                    Status wa = (Status) session.get(Status.class, penArray[3].trim());
                    jcode.setWaveoff(wa);

                    jcode.setChargeAmount(new BigDecimal(penArray[4].trim()));

                    jcode.setCreatedtime(timestampDate);
                    jcode.setLastupdatedtime(timestampDate);
                    jcode.setMaker(pentask.getCreateduser());
                    jcode.setChecker(audit.getLastupdateduser());

                    audit.setNewvalue(jcode.getCode() + "|" + jcode.getDescription() + "|" + jcode.getStatus().getStatuscode() + "|" + jcode.getWaveoff().getStatuscode() + "|" + jcode.getChargeAmount().toString());
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer category (Code:  " + jcode.getCode() +") inputted by " + pentask.getCreateduser() + " approved "+ audit.getDescription());

                    session.save(jcode);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    CustomerCategories u = (CustomerCategories) session.get(CustomerCategories.class, penArray[0].trim());

                    if (u != null) {

                        audit.setOldvalue(u.getCode() + "|" + u.getDescription() + "|" + u.getStatus().getStatuscode() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());

                        u.setDescription(penArray[1].trim());

                        Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(st);

                        Status wa = (Status) session.get(Status.class, penArray[3].trim());
                        u.setWaveoff(wa);

                        u.setChargeAmount(new BigDecimal(penArray[4].trim()));

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getStatus().getStatuscode() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer category (Code: " + penArray[0] +") inputted by " + pentask.getCreateduser() + " approved "+ audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    CustomerCategories u = (CustomerCategories) session.get(CustomerCategories.class, penArray[0]);
                    if (u != null) {
                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getStatus().getStatuscode() + "|" + u.getWaveoff().getStatuscode() + "|" + u.getChargeAmount().toString());
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on customer category (Code: " + penArray[0] +") inputted by " + pentask.getCreateduser() + " approved "+ audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectCustomerCategory(CustomerCatInpuBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String code = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    CustomerCategories cusCategory = (CustomerCategories) session.get(CustomerCategories.class, code.trim());

                    if (cusCategory != null) {
                        audit.setOldvalue(cusCategory.getCode() + "|" + cusCategory.getDescription() + "|" + cusCategory.getStatus().getStatuscode() + "|" + cusCategory.getWaveoff().getStatuscode() + "|" + cusCategory.getChargeAmount().toString());
                    }
                }
//                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                } else {
//                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on txn type : " + txntype + audit.getDescription());
//                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on customer category (Code: " + code +") inputted by " + u.getCreateduser() + " rejected "+ audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

}
