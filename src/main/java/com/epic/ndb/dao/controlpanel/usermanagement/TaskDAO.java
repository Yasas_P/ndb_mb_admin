/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.usermanagement;

import com.epic.ndb.bean.controlpanel.usermanagement.TaskBean;
import com.epic.ndb.bean.controlpanel.usermanagement.TaskInputBean;
import com.epic.ndb.bean.controlpanel.usermanagement.TaskPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author chanuka
 */
public class TaskDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    //start newly changed
    public String activateTask(TaskInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();

            Date sysDate = CommonDAO.getSystemDate(session);

            Task u = (Task) session.get(Task.class, inputBean.getTaskCode().trim());
            if (u != null) {

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                u.setDescription(inputBean.getDescription().trim());
                u.setSortkey(new Byte(inputBean.getSortKey().trim()));

                //Change status to 'Activate'
                Status status = new Status();
                status.setStatuscode(CommonVarList.STATUS_ACTIVE);
                u.setStatus(status);

                session.save(audit);
                session.update(u);

                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }//end newly changed

    public String insertTask(TaskInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((Task) session.get(Task.class, inputBean.getTaskCode().trim()) == null) {

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getTaskCode())
                        .setString("pagecode", audit.getPagecode());

                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getTaskCode().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.TASK_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

//            txn = session.beginTransaction();
//
//            String sql = "from Task as u where lower(u.taskcode)=:taskcode ";
//            Query query = session.createQuery(sql).setString("taskcode", inputBean.getTaskCode().trim().toLowerCase());
//            if (query.list() == null || query.list().size() < 1) {
//
//                Task task = new Task();
//                task.setTaskcode(inputBean.getTaskCode().trim());
//                task.setDescription(inputBean.getDescription().trim());
////                task.setSortkey(new Byte(inputBean.getSortKey().trim()));
//
//                Status st = (Status) session.get(Status.class, inputBean.getStatus().trim());
//                task.setStatus(st);
//
//                task.setCreatetime(sysDate);
//                task.setLastupdatedtime(sysDate);
//                task.setLastupdateduser(audit.getLastupdateduser());
//
//                String newV = task.getTaskcode()
//                        + "|" + task.getDescription()
//                        //                        + "|" + task.getSortkey()
//                        + "|" + task.getStatus().getDescription();
//
//                audit.setNewvalue(newV);
//                audit.setCreatetime(sysDate);
//                audit.setLastupdatedtime(sysDate);
//
//                session.save(audit);
//                session.save(task);
//
//                txn.commit();
//            } else {
//
//                long count = 0;
//                String sqlCount = "select count(taskcode) from Task as u where u.status=:statuscode AND lower(u.taskcode)=:taskcode";
//                Query queryCount = session.createQuery(sqlCount).setString("statuscode", CommonVarList.STATUS_DELETE)
//                        .setString("taskcode", inputBean.getTaskCode().trim().toLowerCase());
//
//                Iterator itCount = queryCount.iterate();
//                count = (Long) itCount.next();
//
//                if (count > 0) {
//                    message = "$" + inputBean.getTaskCode().trim();
//                } else {
//                    message = MessageVarList.COMMON_ALREADY_EXISTS;
//                }
//            }
//            if ((Task) session.get(Task.class, inputBean.getTaskCode().trim()) == null) {
//                txn = session.beginTransaction();
//
//                Task task = new Task();
//                task.setTaskcode(inputBean.getTaskCode().trim());
//                task.setDescription(inputBean.getDescription().trim());
//                task.setSortkey(new Byte(inputBean.getSortKey().trim()));
//
//                Status st = new Status();
//                st.setStatuscode(CommonVarList.STATUS_ACTIVE);
//                task.setStatus(st);
////                task.setCreatetime(sysDate);
////                task.setLastupdatedtime(sysDate);
////                task.setUser(audit.getUser());
//
//                audit.setCreatetime(sysDate);
//                audit.setLastupdatedtime(sysDate);
//
//                session.save(audit);
//                session.save(task);
//
//                txn.commit();
//            } else {
//                long count = 0;
//
//                System.out.println("****************** " + inputBean.getTaskCode().trim().toLowerCase());
//                String sqlCount = "select count(taskcode) from Task as u where u.status=:statuscode AND lower(u.taskcode)=:taskcode";
//                Query queryCount = session.createQuery(sqlCount).setString("statuscode", CommonVarList.STATUS_DELETE)
//                        .setString("taskcode", inputBean.getTaskCode().trim().toLowerCase());
//
//                Iterator itCount = queryCount.iterate();
//                count = (Long) itCount.next();
//
//                if (count > 0) {
//                    message = "$" + inputBean.getTaskCode().trim();
//                } else {
//                    message = MessageVarList.COMMON_ALREADY_EXISTS;
//                }
//
////                message = MessageVarList.COMMON_ALREADY_EXISTS;
////                Task u = (Task) session.get(Task.class, inputBean.getTaskCode().trim());
////                txn = session.beginTransaction();
////                u.setDescription(inputBean.getDescription());
////                u.setSortkey(new Integer(inputBean.getSortKey()));
////                
////                Status st = new Status();
////                st.setStatuscode(CommonVarList.STATUS_ACTIVE);
////                u.setStatus(st);
////                
////                u.setUser(audit.getUser());
////                u.setLastupdatedtime(sysDate);
////
////                audit.setCreatedtime(sysDate);
////                audit.setLastupdatedtime(sysDate);
////
////                session.save(audit);
////                session.update(u);
////                
////                txn.commit();
//            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

//    //update user
    public String updateTask(TaskInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getTaskCode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getTaskCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.TASK_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }

//            Date sysDate = CommonDAO.getSystemDate(session);
//
//            Task u = (Task) session.get(Task.class, inputBean.getTaskCode().trim());
//
//            if (u != null) {
//
//                String oldValue = u.getTaskcode()
//                        + "|" + u.getDescription()
//                        //                        + "|" + u.getSortkey()
//                        + "|" + u.getStatus().getDescription();
//
//                u.setDescription(inputBean.getDescription().trim());
////                u.setSortkey(new Byte(inputBean.getSortKey().trim()));
//
//                Status st = (Status) session.get(Status.class, inputBean.getStatus().trim());
//                u.setStatus(st);
//
//                u.setLastupdatedtime(sysDate);
//                u.setLastupdateduser(audit.getLastupdateduser());
//
//                String newV = u.getTaskcode()
//                        + "|" + u.getDescription()
//                        //                        + "|" + u.getSortkey()
//                        + "|" + u.getStatus().getDescription();
//
//                audit.setOldvalue(oldValue);
//                audit.setNewvalue(newV);
//                audit.setCreatetime(sysDate);
//                audit.setLastupdatedtime(sysDate);
//
//                session.save(audit);
//                session.update(u);
//
//                txn.commit();
//            } else {
//                message = MessageVarList.COMMON_NOT_EXISTS;
//            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    //get search list
//    //delete section
    public String deleteTask(TaskInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getTaskCode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Task u = (Task) session.get(Task.class, inputBean.getTaskCode().trim());
                if (u != null) {

                    audit.setNewvalue(u.getTaskcode() + "|" + u.getDescription() + "|" + u.getStatus().getDescription());
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getTaskCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                pendingtask.setInputterbranch(sysUser.getBranch());

                Page page = (Page) session.get(Page.class, PageVarList.TASK_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

//            Task u = (Task) session.get(Task.class, inputBean.getTaskCode().trim());
//            if (u != null) {
//
//                long count = 0;
//
//                String sqlCount = "select count(taskcode) from Pagetask as u where u.task.taskcode=:taskcode";
//                Query queryCount = session.createQuery(sqlCount).setString("taskcode", inputBean.getTaskCode().trim());
//
//                Iterator itCount = queryCount.iterate();
//                count = (Long) itCount.next();
//
//                if (count > 0) {
//                    message = MessageVarList.COMMON_NOT_DELETE;
//                } else {
//                    audit.setCreatetime(sysDate);
//                    audit.setLastupdatedtime(sysDate);
//
//                    session.save(audit);
//                    session.delete(u);
//                    txn.commit();
//                }
//
////                audit.setCreatedtime(sysDate);
////                audit.setLastupdatedtime(sysDate);
////
////                //Change status to 'Delete'
////                Status status = new Status();
////                status.setStatuscode(CommonVarList.STATUS_DELETE);
////                u.setStatus(status);
////
////                u.setUser(audit.getUser());
////                u.setLastupdatedtime(sysDate);
////
////                session.save(audit);
////                session.update(u);
//////                session.delete(u);
////
////
////                txn.commit();
//            } else {
//                message = MessageVarList.COMMON_NOT_EXISTS;
//            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public List<TaskBean> getSearchList(TaskInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<TaskBean> dataList = new ArrayList<TaskBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(taskcode) from Task as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Task u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    TaskBean mpiTask = new TaskBean();
                    Task task = (Task) it.next();

                    try {
                        mpiTask.setTaskcode(task.getTaskcode());
                    } catch (NullPointerException npe) {
                        mpiTask.setTaskcode("--");
                    }
                    try {
                        mpiTask.setDescription(task.getDescription());
                    } catch (NullPointerException npe) {
                        mpiTask.setDescription("--");
                    }
                    try {
                        mpiTask.setStatuscode(task.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        mpiTask.setStatuscode("--");
                    }
                    try {
                        if (task.getCreatetime() != null && !task.getCreatetime().toString().isEmpty()) {
                            mpiTask.setCreatetime(task.getCreatetime().toString().substring(0, 19));
                        } else {
                            mpiTask.setCreatetime("--");
                        }

                    } catch (NullPointerException npe) {
                        mpiTask.setCreatetime("--");
                    }
//                    try {
//                        mpiTask.setSortkey(task.getSortkey().toString());
//                    } catch (NullPointerException npe) {
//                        mpiTask.setSortkey("--");
//                    }

                    mpiTask.setFullCount(count);

                    dataList.add(mpiTask);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    //find task by task code
    public Task findTaskById(String taskCode) throws Exception {
        Task task = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Task as u where u.taskcode=:taskcode";
            Query query = session.createQuery(sql).setString("taskcode", taskCode);
            task = (Task) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return task;

    }

    private String makeWhereClause(TaskInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getTaskCodeSearch() != null && !inputBean.getTaskCodeSearch().isEmpty()) {
            where += " and lower(u.taskcode) like lower('%" + inputBean.getTaskCodeSearch() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch() + "%')";
        }
//        if (inputBean.getSortKeySearch() != null && !inputBean.getSortKeySearch().isEmpty()) {
//            where += " and u.sortkey = '" + inputBean.getSortKeySearch() + "'";
//        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        return where;
    }

    public List<TaskPendBean> getPendingTaskList(TaskInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<TaskPendBean> dataList = new ArrayList<TaskPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.TASK_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.TASK_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    TaskPendBean task = new TaskPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        task.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        task.setId("--");
                    }
                    try {
                        task.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        task.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        task.setTaskCode(penArray[0]);
                    } catch (NullPointerException npe) {
                        task.setTaskCode("--");
                    } catch (Exception ex) {
                        task.setTaskCode("--");
                    }
                    try {
                        task.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        task.setFields("--");
                    } catch (Exception ex) {
                        task.setFields("--");
                    }
                    try {
                        task.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        task.setStatus("--");
                    }
                    try {
                        task.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        task.setCreateduser("--");
                    } catch (Exception ex) {
                        task.setCreateduser("--");
                    }

                    task.setFullCount(count);

                    dataList.add(task);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String confirmTask(TaskInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            String oldVal = "";
            String newVal = "";

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    //--------audit new value----------------
                    newVal = penArray[0].trim() + "|"
                            + penArray[1] + "|"
                            + penArray[2];

                    Task task = new Task();
                    task.setTaskcode(penArray[0]);
                    task.setDescription(penArray[1]);

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on task ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    Status st = (Status) session.get(Status.class, penArray[2].trim());
                    task.setStatus(st);

                    task.setCreatetime(sysDate);
                    task.setLastupdatedtime(sysDate);
                    task.setLastupdateduser(audit.getLastupdateduser());

                    session.save(task);

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    Task u = (Task) session.get(Task.class, penArray[0]);

                    if (u != null) {

                        newVal = penArray[0].trim() + "|"
                                + penArray[1] + "|"
                                + penArray[2];

                        oldVal = u.getTaskcode() + "|"
                                + u.getDescription() + "|"
                                + u.getStatus().getStatuscode();

                        u.setDescription(penArray[1]);
                        Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(st);
                        u.setLastupdateduser(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        session.update(u);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on task ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    newVal = penArray[0].trim() + "|"
                            + penArray[1] + "|"
                            + penArray[2];

                    Task u = (Task) session.get(Task.class, penArray[0]);
                    if (u != null) {
                        session.delete(u);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on task ( code : " + penArray[0] + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setOldvalue(oldVal);
                audit.setNewvalue(newVal);

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectTask(TaskInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] fieldsArray = u.getFields().split("\\|");

                String taskcode = fieldsArray[0];
                if (u.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on task ( code : "  + taskcode  + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                } else if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                     Task task = (Task) session.get(Task.class, fieldsArray[0]);

                    if (u != null) {
                        String oldval = task.getTaskcode() + "|"
                                + task.getDescription() + "|"
                                + task.getStatus().getStatuscode();
                        audit.setOldvalue(oldval);
                    }

                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on task ( code : "  + taskcode  + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                } else if (u.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on task ( code : "  + taskcode  + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                } else {
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on task ( code : "  + taskcode  + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

}
