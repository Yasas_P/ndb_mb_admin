/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.DelistedContractBean;
import com.epic.ndb.bean.controlpanel.systemconfig.DelistedContractInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.DelistedContractPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.DelistedCategory;
import com.epic.ndb.util.mapping.DelistedContract;
import com.epic.ndb.util.mapping.DelistedContractId;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class DelistedContractDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<DelistedContractBean> getSearchList(DelistedContractInputBean inputBean, int max, int first, String orderBy) {
        List<DelistedContractBean> dataList = new ArrayList<DelistedContractBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.lastupdatedtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(*) from DelistedContract as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from DelistedContract u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    DelistedContractBean jcode = new DelistedContractBean();
                    DelistedContract jc = (DelistedContract) it.next();

                    try {
                        jcode.setCid(jc.getId().getCid());
                    } catch (NullPointerException npe) {
                        jcode.setCid("--");
                    }
                    try {
                        jcode.setDelistedCategory(jc.getId().getDelistedCategory());
                    } catch (NullPointerException npe) {
                        jcode.setDelistedCategory("--");
                    }
                    try {
                        jcode.setDelistedCategoryDes(jc.getDelistedCategory().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setDelistedCategoryDes("--");
                    }
                    try {
                        jcode.setContractNumber(jc.getId().getContractNumber());
                    } catch (NullPointerException npe) {
                        jcode.setContractNumber("--");
                    }
                    try {
                        jcode.setMaker(jc.getMaker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setMaker("--");
                    }
                    try {
                        jcode.setChecker(jc.getChecker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setChecker("--");
                    }
                    try {
                        jcode.setCreatedtime(jc.getCreatetime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setCreatedtime("--");
                    }
                    try {
                        jcode.setLastupdatedtime(jc.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        jcode.setLastupdatedtime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(DelistedContractInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getCidSearch() != null && !inputBean.getCidSearch().isEmpty()) {
            where += " and lower(u.id.cid) like lower('%" + inputBean.getCidSearch().trim() + "%')";
        }
        if (inputBean.getDelistedCategorySearch() != null && !inputBean.getDelistedCategorySearch().isEmpty()) {
            where += " and u.id.delistedCategory = '" + inputBean.getDelistedCategorySearch() + "'";
        }
        if (inputBean.getContractNumberSearch() != null && !inputBean.getContractNumberSearch().isEmpty()) {
            where += " and lower(u.id.contractNumber) like lower('%" + inputBean.getContractNumberSearch().trim() + "%')";
        }
        return where;
    }

    public List<DelistedContractPendBean> getPendingDelistedContractList(DelistedContractInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<DelistedContractPendBean> dataList = new ArrayList<DelistedContractPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.DELISTED_CONTRACT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.DELISTED_CONTRACT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    DelistedContractPendBean jcode = new DelistedContractPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        jcode.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        jcode.setId("--");
                    }
                    try {
                        jcode.setOperationcode(pTask.getTask().getTaskcode().toString());
                        jcode.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        jcode.setOperation("--");
                        jcode.setOperationcode("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        jcode.setCid(penArray[0]);
                    } catch (NullPointerException npe) {
                        jcode.setCid("--");
                    } catch (Exception ex) {
                        jcode.setCid("--");
                    }
                    try {
                        DelistedCategory delistedCategory = (DelistedCategory) session.get(DelistedCategory.class, penArray[1]);
                        jcode.setDelistedCategory(delistedCategory.getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setDelistedCategory("--");
                    } catch (Exception ex) {
                        jcode.setDelistedCategory("--");
                    }
                    try {
                        jcode.setContractNumber(penArray[2]);
                    } catch (NullPointerException npe) {
                        jcode.setContractNumber("--");
                    } catch (Exception ex) {
                        jcode.setContractNumber("--");
                    }
                    try {
                        jcode.setFields(pTask.getFields().toString());
                    } catch (NullPointerException npe) {
                        jcode.setFields("--");
                    } catch (Exception ex) {
                        jcode.setFields("--");
                    }
                    try {
                        jcode.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public List<String> getDelistedContractNumberByCID(String cid)
            throws Exception {
        List<String> delistedContractNumList = new ArrayList<String>();
        List<DelistedContract> delistedContractList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            String sql = "from DelistedContract as s where s.id.cid =:cid and s.id.delistedCategory = :delistedCategory";
            Query query = session.createQuery(sql).setString("cid", cid).setString("delistedCategory", CommonVarList.DELISTED_CATEGORY_ACCOUNT_CODE);
            delistedContractList = query.list();
            for (DelistedContract delistedContract : delistedContractList) {
                delistedContractNumList.add(delistedContract.getId().getContractNumber());
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return delistedContractNumList;
    }

    public String insertDelistedContract(DelistedContractInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            String pKey = inputBean.getCid().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());
            if (query.list().isEmpty()) {
                txn = session.beginTransaction();

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(pKey);
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.ASSIGN_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.DELISTED_CONTRACT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteDelistedContract(DelistedContractInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String pKey = inputBean.getCid().trim() + "|" + inputBean.getDelistedCategory().trim() + "|" + inputBean.getContractNumber().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                DelistedContractId id = new DelistedContractId();
                id.setCid(inputBean.getCid());
                id.setDelistedCategory(inputBean.getDelistedCategory());
                id.setContractNumber(inputBean.getContractNumber());

                DelistedContract u = (DelistedContract) session.get(DelistedContract.class, id);
                if (u != null) {

                    StringBuilder stringBuilderNewVal = new StringBuilder();
                    stringBuilderNewVal.append(u.getId().getCid())
                            .append("|").append(u.getId().getDelistedCategory())
                            .append("|").append(u.getId().getContractNumber());

                    audit.setNewvalue(stringBuilderNewVal.toString());

                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(pKey.trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.DELISTED_CONTRACT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmDelistedContract(DelistedContractInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                String[] pencontractNumberArray = null;
                List<String> contractNumberList = null;
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                    if (penArray.length >= 3) {
                        pencontractNumberArray = penArray[2].split("\\,");
                    }
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ASSIGN_TASK)) {

                    // multiple credit currency selection
                    String sql2 = "from DelistedContract as s where s.id.cid =:cid and s.id.delistedCategory = :delistedCategory";
                    Query query2 = session.createQuery(sql2).setString("cid", pentask.getPKey()).setString("delistedCategory", CommonVarList.DELISTED_CATEGORY_ACCOUNT_CODE);

                    List<DelistedContract> dbList = query2.list();

                    if (pencontractNumberArray != null) {
                        contractNumberList = new ArrayList<String>(Arrays.asList(pencontractNumberArray));
                    } else {
                        contractNumberList = new ArrayList<String>();
                    }

                    for (DelistedContract pt : dbList) {

                        if (contractNumberList.contains(String.valueOf(pt.getId().getContractNumber()))) {
//                                pt.setStatus(CommonVarList.STATUS_ACTIVE);//Is it needed
                            pt.setLastupdatedtime(sysDate);
                            pt.setMaker(pentask.getCreateduser());
                            pt.setChecker(audit.getLastupdateduser());
                            session.update(pt);
                            contractNumberList.remove(String.valueOf(pt.getId().getContractNumber()));
                        } else {
                            session.delete(pt);
                        }
                    }

                    for (String val : contractNumberList) {
                        DelistedContractId delistedContractId = new DelistedContractId();

                        delistedContractId.setCid(penArray[0]);
                        delistedContractId.setDelistedCategory(CommonVarList.DELISTED_CATEGORY_ACCOUNT_CODE);
                        delistedContractId.setContractNumber(val);

                        DelistedContract delistedContract = new DelistedContract();

                        delistedContract.setId(delistedContractId);

                        delistedContract.setCreatetime(sysDate);
                        delistedContract.setLastupdatedtime(sysDate);
                        delistedContract.setMaker(pentask.getCreateduser());
                        delistedContract.setChecker(audit.getLastupdateduser());
                        session.save(delistedContract);

                    }
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on delisted contract(  CID : " + penArray[0] + ")" + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPLOAD_TASK)) {
//                    String delistedContractIds = "";
                    Blob csvBlob = pentask.getInputfile();
                    if (csvBlob != null) {
                        isr = new InputStreamReader(csvBlob.getBinaryStream());
                        br = new BufferedReader(isr);

                        String[] parts = new String[0];
                        int countrecord = 1;
                        int succesrec = 0;
                        String thisLine = null;
                        boolean getline = false;
                        String token = "";
                        while ((thisLine = br.readLine()) != null) {
                            if (thisLine.trim().equals("")) {
                                continue;
                            } else {
                                if (getline) {
//                              token = content.nextLine();
                                    token = thisLine;

//                                    System.err.println(token);
                                    try {
                                        parts = token.split("\\,", 3);
                                        inputBean.setCid(parts[0].trim());
                                        inputBean.setDelistedCategory(parts[1].trim());
                                        inputBean.setContractNumber(parts[2].trim());

                                    } catch (Exception ee) {
                                        System.out.println("----" + ee.getMessage());
                                        message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                                        break;
                                    }
                                    countrecord++;
                                    if (parts.length >= 3 && message.isEmpty()) {
//                                        message = this.validateInputsForCSV(inputBean);
//                                        if (message.isEmpty()) {
//                                            message = this.validateUpload(inputBean, session);
//                                            if (message.isEmpty()) {
                                        String sql2 = "from DelistedContract as s where s.id.cid =:cid and s.id.delistedCategory = :delistedCategory and s.id.contractNumber = :contractNumber";
                                        Query query2 = session.createQuery(sql2).setString("cid", inputBean.getCid()).setString("delistedCategory", inputBean.getDelistedCategory()).setString("contractNumber", inputBean.getContractNumber());
                                        if (query2.list().isEmpty()) {
                                            DelistedContractId delistedContractId = new DelistedContractId();

                                            delistedContractId.setCid(inputBean.getCid());
                                            delistedContractId.setDelistedCategory(inputBean.getDelistedCategory());
                                            delistedContractId.setContractNumber(inputBean.getContractNumber());

                                            DelistedContract delistedContract = new DelistedContract();

                                            delistedContract.setId(delistedContractId);

                                            delistedContract.setCreatetime(sysDate);
                                            delistedContract.setLastupdatedtime(sysDate);
                                            delistedContract.setMaker(pentask.getCreateduser());
                                            delistedContract.setChecker(audit.getLastupdateduser());
                                            session.save(delistedContract);
//
                                        } else {
                                            DelistedContract u = (DelistedContract) query2.list().get(0);

                                            u.setMaker(pentask.getCreateduser());
                                            u.setChecker(audit.getLastupdateduser());
                                            u.setLastupdatedtime(sysDate);

                                            session.update(u);
//
                                        }
//                                        delistedContractIds = delistedContractIds + "(" + inputBean.getCid() + "," + inputBean.getDelistedCategory() + "," + inputBean.getContractNumber() + "),";
                                        succesrec++;
//                                            } else {
//                                                message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                                break;
//                                            }
//                                        } else {
//                                            message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                            break;
//                                        }

                                    } else {
                                        message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                                    }
                                } else {
                                    getline = true;
//                            content.nextLine();
                                }
                            }
                        }
//                        if (!delistedContractIds.isEmpty() && delistedContractIds.length() > 0) {
//                            delistedContractIds = delistedContractIds.substring(0, delistedContractIds.length() - 1);
//                        }
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on delisted contract ( Records count :" + succesrec + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
                    DelistedContractId delistedContractId = new DelistedContractId();

                    delistedContractId.setCid(penArray[0]);
                    delistedContractId.setDelistedCategory(penArray[1]);
                    delistedContractId.setContractNumber(penArray[2]);

                    DelistedContract u = (DelistedContract) session.get(DelistedContract.class, delistedContractId);
                    if (u != null) {
                        // -------------------audit new value----------------------
                        StringBuilder stringBuilderNewVal = new StringBuilder();
                        stringBuilderNewVal.append(u.getId().getCid())
                                .append("|").append(u.getId().getDelistedCategory())
                                .append("|").append(u.getId().getContractNumber());

                        audit.setNewvalue(stringBuilderNewVal.toString());

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on delisted contract(  CID : " + penArray[0] + ", delisted category : " + penArray[1] + ", contract number : " + penArray[2] + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }

            if (message.isEmpty()) {
                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();
            } else {
                if (txn != null) {
                    txn.rollback();
                }
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectDelistedContract(DelistedContractInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask pentask = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (pentask != null) {
                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    //------------audit new value------------
                    audit.setNewvalue(pentask.getFields());
                    penArray = pentask.getFields().split("\\|");
                }
                if (pentask.getTask().getTaskcode().equals(TaskVarList.UPLOAD_TASK)) {
                    audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on delisted contract inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
                    audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on delisted contract(  CID : " + penArray[0] + ", delisted category : " + penArray[1] + ", contract number : " + penArray[2] + ") inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                } else {
                    audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on  delisted contract inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());
                }
                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(pentask);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String uploadDelistedContract(DelistedContractInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        FileInputStream fileInputStream = null;

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            Pendingtask pendingtask = new Pendingtask();

//                    pendingtask.setPKey(inputBean.getDebitProductType().trim() + "|" + inputBean.getCreditProductType().trim());
//                    pendingtask.setFields(audit.getNewvalue());
            Task task = new Task();
            task.setTaskcode(TaskVarList.UPLOAD_TASK);
            pendingtask.setTask(task);

            Status st = new Status();
            st.setStatuscode(CommonVarList.STATUS_PENDING);
            pendingtask.setStatus(st);

            pendingtask.setInputterbranch(sysUser.getBranch());

            Page page = (Page) session.get(Page.class, PageVarList.DELISTED_CONTRACT_PAGE);
            pendingtask.setPage(page);

            try {
                if (inputBean.getConXL().length() != 0) {
                    File csvFile = inputBean.getConXL();
                    byte[] bCsvFile = new byte[(int) csvFile.length()];
                    try {
                        fileInputStream = new FileInputStream(csvFile);
                        fileInputStream.read(bCsvFile);
                        fileInputStream.close();
                        Blob blob = new javax.sql.rowset.serial.SerialBlob(bCsvFile);
                        pendingtask.setInputfile(blob);
                    } catch (Exception ex) {

                    }
                }
            } catch (NullPointerException ex) {

            } finally {

                if (fileInputStream != null) {
                    fileInputStream.close();
                }

            }

            pendingtask.setCreatedtime(sysDate);
            pendingtask.setLastupdatedtime(sysDate);
            pendingtask.setCreateduser(audit.getLastupdateduser());

            audit.setCreatetime(sysDate);
            audit.setLastupdatedtime(sysDate);
            audit.setLastupdateduser(audit.getLastupdateduser());

            session.save(audit);
            session.save(pendingtask);
            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String pendDelistedContractCsvDownloade(DelistedContractInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getInputfile() != null) {
                    Blob blob = u.getInputfile();
                    int blobLength = (int) blob.length();
                    byte[] blobAsBytes = blob.getBytes(1, blobLength);
                    inputBean.setFileInputStream(u.getInputfile().getBinaryStream());
                    inputBean.setFileLength(blobAsBytes.length);

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    session.save(audit);
                    txn.commit();
                } else {
                    message = "File not found";
                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String validateInputsForCSV(DelistedContractInputBean inputBean) {
        String message = "";
        if (inputBean.getCid() == null || inputBean.getCid().trim().isEmpty()) {
            message = MessageVarList.DELISTED_CONTRACT_EMPTY_CID;
        } else if (inputBean.getDelistedCategory() == null || inputBean.getDelistedCategory().trim().isEmpty()) {
            message = MessageVarList.DELISTED_CONTRACT_EMPTY_DELISTED_CATEGORY;
        } else if (inputBean.getContractNumber() == null || inputBean.getContractNumber().trim().isEmpty()) {
            message = MessageVarList.DELISTED_CONTRACT_EMPTY_CONTRACT_NUMBER;
        }

        return message;
    }

    public String validateUpload(DelistedContractInputBean inputBean, Session session) throws Exception {
        String message = "";

        if (inputBean.getCid().length() > 20) {
            message = MessageVarList.DELISTED_CONTRACT_INVALID_CID_LENGTH + "20";
        } else if (inputBean.getContractNumber().length() > 20) {
            message = MessageVarList.DELISTED_CONTRACT_INVALID_CONTRACT_NUMBER_LENGTH + "20";
        } else if ((DelistedCategory) session.get(DelistedCategory.class, inputBean.getDelistedCategory().trim()) == null) {
            message = MessageVarList.DELISTED_CONTRACT_INVALID_DELISTED_CATEGORY;
        }

        return message;
    }
}
