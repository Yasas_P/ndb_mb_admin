/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.ProductTypeBean;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductTypeInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.ProductTypePendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.ProductType;
import com.epic.ndb.util.mapping.Productcategory;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class ProductTypeDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<ProductTypeBean> getSearchList(ProductTypeInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<ProductTypeBean> dataList = new ArrayList<ProductTypeBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(productType) from ProductType as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from ProductType u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    ProductTypeBean productTypeBean = new ProductTypeBean();
                    ProductType productTypeDetail = (ProductType) it.next();

                    try {
                        productTypeBean.setProductType(productTypeDetail.getProductType().toString());
                    } catch (NullPointerException npe) {
                        productTypeBean.setProductType("--");
                    }
                    try {
                        productTypeBean.setStatus(productTypeDetail.getStatus().getDescription());
                    } catch (Exception npe) {
                        productTypeBean.setStatus("--");
                    }
                    try {
                        productTypeBean.setProductName(productTypeDetail.getProductName().toString());
                    } catch (Exception npe) {
                        productTypeBean.setProductName("--");
                    }
                    try {
                        productTypeBean.setProductcategory(productTypeDetail.getProductcategory().getDescription().toString());
                    } catch (Exception npe) {
                        productTypeBean.setProductcategory("--");
                    }
                    try {
                        productTypeBean.setMaker(productTypeDetail.getMaker().toString());
                    } catch (NullPointerException npe) {
                        productTypeBean.setMaker("--");
                    }
                    try {
                        productTypeBean.setChecker(productTypeDetail.getChecker().toString());
                    } catch (NullPointerException npe) {
                        productTypeBean.setChecker("--");
                    }
                    try {
                        productTypeBean.setCreatedtime(productTypeDetail.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        productTypeBean.setCreatedtime("--");
                    }
                    try {
                        productTypeBean.setLastupdatedtime(productTypeDetail.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        productTypeBean.setLastupdatedtime("--");
                    }

                    productTypeBean.setFullCount(count);

                    dataList.add(productTypeBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(ProductTypeInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getProductTypeSearch() != null && !inputBean.getProductTypeSearch().isEmpty()) {
            where += " and lower(u.productType) like lower('%" + inputBean.getProductTypeSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getProductcategorySearch() != null && !inputBean.getProductcategorySearch().isEmpty()) {
            where += " and u.productcategory.code = '" + inputBean.getProductcategorySearch() + "'";
        }
        if (inputBean.getProductNameSearch() != null && !inputBean.getProductNameSearch().isEmpty()) {
            where += " and lower(u.productName) like lower('%" + inputBean.getProductNameSearch().trim() + "%')";
        }

        return where;
    }

    public List<ProductTypePendBean> getPendingProductTypeList(ProductTypeInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<ProductTypePendBean> dataList = new ArrayList<ProductTypePendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.PRODUCT_TYPE_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.PRODUCT_TYPE_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    ProductTypePendBean productType = new ProductTypePendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        productType.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        productType.setId("--");
                    }
                    try {
                        productType.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        productType.setOperation("--");
                    }

                    try {
                        productType.setProductType(pTask.getPKey().toString());
                    } catch (Exception ex) {
                        productType.setProductType("--");
                    }
                    try {
                        productType.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        productType.setFields("--");
                    } catch (Exception ex) {
                        productType.setFields("--");
                    }
                    try {
                        productType.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        productType.setStatus("--");
                    }
                    try {
                        productType.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        productType.setCreatetime("--");
                    }
                    try {
                        productType.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        productType.setCreateduser("--");
                    } catch (Exception ex) {
                        productType.setCreateduser("--");
                    }

                    productType.setFullCount(count);

                    dataList.add(productType);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertProductType(ProductTypeInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            if ((ProductType) session.get(ProductType.class, inputBean.getProductType().trim()) == null) {
                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", inputBean.getProductType())
                        .setString("pagecode", audit.getPagecode());
                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getProductType().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PRODUCT_TYPE_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteProductType(ProductTypeInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getProductType())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                String sql1 = "select count(*) from ProductMatrix as u where u.productTypeByDebitProductType.productType =:productType or u.productTypeByCreditProductType.productType =:productType";
                Query query1 = session.createQuery(sql1).setString("productType", inputBean.getProductType().trim());
                Long count1 = (Long) query1.iterate().next();

                if (count1 != 0) {
                    message = MessageVarList.COMMON_ALREADY_IN_USE;
                } else {
                    ProductType u = (ProductType) session.get(ProductType.class, inputBean.getProductType().trim());
                    if (u != null) {
                        String proCat = "";
                        if (u.getProductcategory() != null) {
                            proCat = u.getProductcategory().getCode();
                        }
                        audit.setNewvalue(u.getProductType() + "|" + u.getProductName() + "|" + u.getStatus().getStatuscode() + "|" + proCat
                        //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                        );
                    }

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getProductType().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.DELETE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PRODUCT_TYPE_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public ProductType findProductTypeById(String productTypeCode) throws Exception {
        ProductType productType = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from ProductType as u where u.productType=:productType";
            Query query = session.createQuery(sql).setString("productType", productTypeCode);
            productType = (ProductType) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return productType;

    }

    public String updateProductType(ProductTypeInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getProductType())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                ProductType u = (ProductType) session.get(ProductType.class, inputBean.getProductType().trim());
                if (u != null) {
//                    String proCat = "";
//                    if (u.getProductcategory() != null) {
//                        proCat = u.getProductcategory().getCode();
//                    }
//                    audit.setOldvalue(u.getProductType() + "|" + u.getProductName() + "|" + u.getStatus().getStatuscode() + "|" + proCat
//                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
//                    );

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(inputBean.getProductType().trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.PRODUCT_TYPE_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmProductType(ProductTypeInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());
                    
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    ProductType productType = new ProductType();

                    productType.setProductType(penArray[0]);
                    productType.setProductName(penArray[1]);

                    Status st = (Status) session.get(Status.class, penArray[2]);
                    productType.setStatus(st);

                    Productcategory pc = (Productcategory) session.get(Productcategory.class, penArray[3]);
                    productType.setProductcategory(pc);

                    productType.setCreatedtime(timestampDate);
                    productType.setLastupdatedtime(timestampDate);
                    productType.setMaker(pentask.getCreateduser());
                    productType.setChecker(audit.getLastupdateduser());

                    session.save(productType);

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on product type (Product Type:  " + productType.getProductType() + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    ProductType u = (ProductType) session.get(ProductType.class, penArray[0].trim());

                    if (u != null) {
                         //-------------------audit old value-------------------
                        String oldValue =penArray[0].trim() + "|"
                                + u.getProductName() + "|"
                                + u.getStatus().getStatuscode() + "|"
                                + u.getProductcategory().getCode();

                        u.setProductName(penArray[1]);

                        Status st = (Status) session.get(Status.class, penArray[2]);
                        u.setStatus(st);

                        Productcategory pc = (Productcategory) session.get(Productcategory.class, penArray[3]);
                        u.setProductcategory(pc);

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        session.update(u);
                        
                        audit.setOldvalue(oldValue);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on product type (Product Type: " + penArray[0] + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    ProductType u = (ProductType) session.get(ProductType.class, penArray[0].trim());
                    if (u != null) {
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on product type (Product Type: " + penArray[0] + ") inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectProductType(ProductTypeInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    ProductType productType = (ProductType) session.get(ProductType.class, u.getPKey());

                    if (productType != null) {
                         //-------------------audit old value-------------------
                         String proCat = "";
                        if (productType.getProductcategory() != null) {
                            proCat = productType.getProductcategory().getCode();
                        }
                        String oldValue =u.getPKey() + "|"
                                + productType.getProductName() + "|"
                                + productType.getStatus().getStatuscode() + "|"
                                + proCat;
                        audit.setOldvalue(oldValue);
                    }
                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on product type (Product Type:" + u.getPKey() + ") inputted by " + u.getCreateduser() + " rejected "  + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public StringBuffer makeCSVReport(ProductTypeInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String orderby = " order by u.createdtime desc ";
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from ProductType u where " + where + orderby;
            Query querySearch = session.createQuery(sqlSearch);

            Iterator it = querySearch.iterate();
            content = new StringBuffer();

            //write column headers to csv file
            content.append("Product Type");
            content.append(',');
            content.append("Product Name");
            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Product Category");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Checker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');

            while (it.hasNext()) {

                ProductType productType = (ProductType) it.next();

                try {
                    content.append(productType.getProductType().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(productType.getProductName().toString()));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productType.getStatus().getDescription());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productType.getProductcategory().getDescription().toString());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productType.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productType.getChecker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productType.getCreatedtime().toString().substring(0, 19));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(productType.getLastupdatedtime().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    content.append("--");
                }
                content.append('\n');
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }
}
