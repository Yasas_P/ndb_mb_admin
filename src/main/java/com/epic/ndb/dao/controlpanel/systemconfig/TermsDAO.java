/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.TermsBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TermsConditionPendBean;
import com.epic.ndb.bean.controlpanel.systemconfig.TermsInputBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.ParameterUserCommon;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.TermsCondition;
import com.epic.ndb.util.mapping.TermsConditionTemp;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class TermsDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<TermsConditionPendBean> getPendingTermsList(TermsInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<TermsConditionPendBean> dataList = new ArrayList<TermsConditionPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createtime desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(versionNo) from TermsConditionTemp as u where u.maker!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from TermsConditionTemp u where u.maker!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    TermsConditionPendBean jcode = new TermsConditionPendBean();
                    TermsConditionTemp pTermsCon = (TermsConditionTemp) it.next();

                    try {
                        jcode.setVersionNo(pTermsCon.getVersionNo());
                    } catch (NullPointerException npe) {
                        jcode.setVersionNo("--");
                    }
                    try {
                        jcode.setOperation(pTermsCon.getTask().getDescription().toString());
                    } catch (Exception npe) {
                        jcode.setOperation("--");
                    }

                    try {
                        jcode.setStatus(pTermsCon.getStatus().getDescription());
                    } catch (Exception npe) {
                        jcode.setStatus("--");
                    }
                    try {
                        jcode.setTerms(pTermsCon.getTerms().toString());
                    } catch (NullPointerException npe) {
                        jcode.setTerms("--");
                    }
                    try {
                        jcode.setCreateduser(pTermsCon.getMaker().toString());
                    } catch (NullPointerException npe) {
                        jcode.setCreateduser("--");
                    } catch (Exception ex) {
                        jcode.setCreateduser("--");
                    }
                    try {
                        jcode.setCreatetime(pTermsCon.getCreatetime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        jcode.setCreatetime("--");
                    }

                    jcode.setFullCount(count);

                    dataList.add(jcode);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public TermsBean findTermsById(String versionno) throws Exception {
        TermsBean dataBean = new TermsBean();
        Session session = null;

        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "select "
                    + "u.status.statuscode,"
                    + "u.terms, "
                    + "u.category, "
                    + "u.versionNo "
                    + "from TermsCondition u where u.versionNo='" + versionno + "'";

            Query query = session.createQuery(sql);

            List<Object[]> objectArrList = (List<Object[]>) query.list();
            if (objectArrList.size() > 0) {

                for (Object[] objArr : objectArrList) {

                    try {
                        dataBean.setStatus(objArr[0].toString());
                    } catch (NullPointerException npe) {
                        dataBean.setStatus("--");
                    }
                    try {
                        dataBean.setTerms(objArr[1].toString());
                    } catch (NullPointerException npe) {
                        dataBean.setTerms("--");
                    }
                    try {
                        dataBean.setCategory(objArr[2].toString());
                    } catch (NullPointerException npe) {
                        dataBean.setCategory("--");
                    }
                    try {
                        dataBean.setVersionno(objArr[3].toString());
                    } catch (NullPointerException npe) {
                        dataBean.setVersionno("--");
                    }
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataBean;

    }

    public TermsBean findTermsTempById(String versionno) throws Exception {
        TermsBean dataBean = new TermsBean();
        Session session = null;

        try {

            session = HibernateInit.sessionFactory.openSession();
            String sql = "select "
                    + "u.status.statuscode,"
                    + "u.terms, "
                    + "u.category, "
                    + "u.versionNo "
                    + "from TermsConditionTemp u where u.versionNo='" + versionno + "'";

            Query query = session.createQuery(sql);

            List<Object[]> objectArrList = (List<Object[]>) query.list();
            if (objectArrList.size() > 0) {

                for (Object[] objArr : objectArrList) {

                    try {
                        dataBean.setStatus(objArr[0].toString());
                    } catch (NullPointerException npe) {
                        dataBean.setStatus("--");
                    }
                    try {
                        dataBean.setTerms(objArr[1].toString());
                    } catch (NullPointerException npe) {
                        dataBean.setTerms("--");
                    }
                    try {
                        dataBean.setCategory(objArr[2].toString());
                    } catch (NullPointerException npe) {
                        dataBean.setCategory("--");
                    }
                    try {
                        dataBean.setVersionno(objArr[3].toString());
                    } catch (NullPointerException npe) {
                        dataBean.setVersionno("--");
                    }
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataBean;

    }

    public String updateTerms(TermsInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            TermsConditionTemp temp = (TermsConditionTemp) session.get(TermsConditionTemp.class, inputBean.getVersionno());
            if (temp == null) {
                TermsCondition u = (TermsCondition) session.get(TermsCondition.class, inputBean.getVersionno());

                /**
                 * for audit old value (adult)
                 */
                StringBuilder stringBuilderOld = new StringBuilder();
                stringBuilderOld.append(u.getVersionNo());

                try {
                    stringBuilderOld.append("|").append(u.getStatus().getStatuscode());
                } catch (Exception ex) {
                    stringBuilderOld.append("|");
                }

                if (u != null) {
                    TermsConditionTemp termsConditionTemp = new TermsConditionTemp();

                    termsConditionTemp.setVersionNo(u.getVersionNo());
                    termsConditionTemp.setTerms(inputBean.getDescription());

                    Status st = (Status) session.get(Status.class, inputBean.getStatus());
                    termsConditionTemp.setStatus(st);

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    termsConditionTemp.setTask(task);

                    termsConditionTemp.setMaker(audit.getLastupdateduser());
                    termsConditionTemp.setCreatetime(sysDate);
                    termsConditionTemp.setLastupdatedtime(sysDate);
                    
                    termsConditionTemp.setInputterbranch(sysUser.getBranch());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    /**
                     * for audit new value
                     */
                    StringBuilder stringBuilderNew = new StringBuilder();
                    stringBuilderNew.append(u.getVersionNo())
                            .append("|").append(u.getStatus().getStatuscode());

                    audit.setNewvalue(stringBuilderNew.toString());
                    audit.setOldvalue(stringBuilderNew.toString());
                    session.save(audit);
                    session.save(termsConditionTemp);

                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }

            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteTerms(TermsInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            TermsConditionTemp temp = (TermsConditionTemp) session.get(TermsConditionTemp.class, inputBean.getVersionno());
            if (temp == null) {
                TermsCondition u = (TermsCondition) session.get(TermsCondition.class, inputBean.getVersionno());
                if (u != null) {
                    String sql = "select "
                            + "u.paramvalue "
                            + "from ParameterUserCommon u where u.paramcode=:paramcode";
                    Query query = session.createQuery(sql).setString("paramcode", CommonVarList.COMMON_CONFIG_TERM_COND_VER_CODE);
                    List<Object[]> objectArrList = (List<Object[]>) query.list();

                    if (objectArrList.contains(inputBean.getVersionno())) {
                        message = "Tearms and conditions version currently in use";
                    } else {
                        TermsConditionTemp termsConditionTemp = new TermsConditionTemp();

                        termsConditionTemp.setVersionNo(u.getVersionNo());
                        termsConditionTemp.setTerms(u.getTerms());
                        termsConditionTemp.setStatus(u.getStatus());

                        Task task = new Task();
                        task.setTaskcode(TaskVarList.DELETE_TASK);
                        termsConditionTemp.setTask(task);

                        termsConditionTemp.setMaker(audit.getLastupdateduser());
                        termsConditionTemp.setCreatetime(sysDate);
                        termsConditionTemp.setLastupdatedtime(sysDate);
                        
                        termsConditionTemp.setInputterbranch(sysUser.getBranch());

                        audit.setCreatetime(sysDate);
                        audit.setLastupdatedtime(sysDate);

                        session.save(termsConditionTemp);
                        session.save(audit);
                    }

                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }
            } else {
                message = "pending available";
            }

            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String insertTerms(TermsInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();
            TermsConditionTemp temp = (TermsConditionTemp) session.get(TermsConditionTemp.class, inputBean.getVersionnoadd());
            if (temp == null) {
                TermsCondition u = (TermsCondition) session.get(TermsCondition.class, inputBean.getVersionnoadd());
                if (u == null) {
                    TermsConditionTemp termsConditionTemp = new TermsConditionTemp();
                    termsConditionTemp.setVersionNo(inputBean.getVersionnoadd().trim());
                    termsConditionTemp.setTerms(inputBean.getDescriptionadd().trim());

                    Status st = (Status) session.get(Status.class, inputBean.getStatusadd());
                    termsConditionTemp.setStatus(st);

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    termsConditionTemp.setTask(task);

                    /**
                     * for audit new value
                     */
                    StringBuilder stringBuilderNew = new StringBuilder();
                    stringBuilderNew.append(termsConditionTemp.getVersionNo())
                            .append("|").append(termsConditionTemp.getStatus().getStatuscode());

                    termsConditionTemp.setMaker(audit.getLastupdateduser());
                    termsConditionTemp.setCreatetime(sysDate);
                    termsConditionTemp.setLastupdatedtime(sysDate);
                    
                    termsConditionTemp.setInputterbranch(sysUser.getBranch());

                    audit.setNewvalue(stringBuilderNew.toString());
                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    session.save(audit);
                    session.save(termsConditionTemp);
                } else {
                    message = MessageVarList.COMMON_ALREADY_EXISTS;
                }
            } else {
                message = "pending available";
            }

            txn.commit();
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmTerms(TermsInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            TermsConditionTemp pentask = (TermsConditionTemp) session.get(TermsConditionTemp.class, inputBean.getVersionno().trim());

            if (pentask != null) {
                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {
                    
                    TermsCondition jcode = new TermsCondition();
                    jcode.setVersionNo(pentask.getVersionNo());
                    jcode.setTerms(pentask.getTerms());

//                    Status st = (Status) session.get(Status.class, penArray[2].trim());
                    jcode.setStatus(pentask.getStatus());

                    jcode.setCreatetime(sysDate);
                    jcode.setLastupdatedtime(sysDate);
                    jcode.setMaker(pentask.getMaker());
                    jcode.setChecker(audit.getLastupdateduser());
                    
                    //------------------------------------audit new value-----------------------------
                    audit.setNewvalue(jcode.getVersionNo()+ "|"+ jcode.getStatus().getStatuscode());
                    
                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on terms and conditions (Version number:  " + jcode.getVersionNo() + ") inputted by " + pentask.getMaker()+ " approved " + audit.getDescription());

                    session.save(jcode);

                    if (jcode.getStatus().getStatuscode().equals(CommonVarList.STATUS_ACTIVE)) {
                        String sqlStatusUpdate = "UPDATE TermsCondition m SET m.status=:newStatus WHERE versionNo !=:versionNo";
                        Query queryStatusUpdate = session.createQuery(sqlStatusUpdate).setString("newStatus", CommonVarList.STATUS_DEACTIVE).setString("versionNo", pentask.getVersionNo());
                        queryStatusUpdate.executeUpdate();
                    }

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    TermsCondition u = (TermsCondition) session.get(TermsCondition.class, inputBean.getVersionno().trim());

                    if (u != null) {
                        
                        //------------------------------------audit old value-----------------------------
                        audit.setOldvalue(u.getVersionNo()+ "|" + u.getStatus().getStatuscode());
                        
                        u.setTerms(pentask.getTerms());

                        // Status st = (Status) session.get(Status.class, penArray[2].trim());
                        u.setStatus(pentask.getStatus());

                        u.setMaker(pentask.getMaker());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        //------------------------------------audit new value-----------------------------
                        audit.setNewvalue(u.getVersionNo()+ "|"+ u.getStatus().getStatuscode());
                        
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on terms and conditions (Version Number : " + inputBean.getVersionno().trim() + ") inputted by " + pentask.getMaker()+ " approved " + audit.getDescription());

                        session.update(u);

                        if (u.getStatus().getStatuscode().equals(CommonVarList.STATUS_ACTIVE)) {
                            String sqlStatusUpdate = "UPDATE TermsCondition m SET m.status=:newStatus WHERE versionNo !=:versionNo";
                            Query queryStatusUpdate = session.createQuery(sqlStatusUpdate).setString("newStatus", CommonVarList.STATUS_DEACTIVE).setString("versionNo", pentask.getVersionNo());
                            queryStatusUpdate.executeUpdate();
                        }
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {

                    TermsCondition u = (TermsCondition) session.get(TermsCondition.class, inputBean.getVersionno().trim());
                    if (u != null) {
                        //------------------------------------audit new value-----------------------------
                        audit.setNewvalue(u.getVersionNo()+"|"+ u.getStatus().getStatuscode());
                        
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on terms and conditions (Version Number : " + inputBean.getVersionno().trim() + ") inputted by " + pentask.getMaker()+ " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectTerms(TermsInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            TermsConditionTemp u = (TermsConditionTemp) session.get(TermsConditionTemp.class, inputBean.getVersionno().trim());

            if (u != null) {
                //------------------------------------audit new value-----------------------------
                audit.setNewvalue(u.getVersionNo()+ "|"+ u.getStatus().getStatuscode());
                
                if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    TermsCondition termsCon = (TermsCondition) session.get(TermsCondition.class, inputBean.getVersionno().trim());

                    if (termsCon != null) {
                        
                        //------------------------------------audit old value-----------------------------
                        audit.setOldvalue(termsCon.getVersionNo()+ "|" + termsCon.getStatus().getStatuscode());
                    }
                }

                audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on terms and conditions(Version Number : " + u.getVersionNo() + ") inputted by " + u.getMaker()+ " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
}
