/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.FeeConfigBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FeeConfigInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.FeeConfigPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.FeeConfiguration;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class FeeConfigurationDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<FeeConfigBean> getSearchList(FeeConfigInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<FeeConfigBean> dataList = new ArrayList<FeeConfigBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.paramcode desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(paramcode) from FeeConfiguration as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from FeeConfiguration u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    FeeConfigBean feeConfigBean = new FeeConfigBean();
                    FeeConfiguration feeConfig = (FeeConfiguration) it.next();

                    try {
                        feeConfigBean.setParamCode(feeConfig.getParamcode());
                    } catch (NullPointerException npe) {
                        feeConfigBean.setParamCode("--");
                    }
                    try {
                        feeConfigBean.setDescription(feeConfig.getDescription());
                    } catch (NullPointerException npe) {
                        feeConfigBean.setDescription("--");
                    }
                    try {
                        feeConfigBean.setParamVal(feeConfig.getParamvalue());
                    } catch (NullPointerException npe) {
                        feeConfigBean.setParamVal("--");
                    }
                    try {
                        feeConfigBean.setMaker(feeConfig.getMarker().toString());
                    } catch (NullPointerException npe) {
                        feeConfigBean.setMaker("--");
                    }
                    try {
                        feeConfigBean.setChecker(feeConfig.getLastupdateduser().toString());
                    } catch (NullPointerException npe) {
                        feeConfigBean.setChecker("--");
                    }
                    try {
                        feeConfigBean.setCreatedtime(feeConfig.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        feeConfigBean.setCreatedtime("--");
                    }
                    try {
                        feeConfigBean.setLastupdatedtime(feeConfig.getLastupdatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        feeConfigBean.setLastupdatedtime("--");
                    }

                    feeConfigBean.setFullCount(count);

                    dataList.add(feeConfigBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(FeeConfigInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getParamCodeSearch() != null && !inputBean.getParamCodeSearch().isEmpty()) {
            where += " and lower(u.paramcode) like lower('%" + inputBean.getParamCodeSearch() + "%')";
        }
        if (inputBean.getDescriptionSearch() != null && !inputBean.getDescriptionSearch().isEmpty()) {
            where += " and lower(u.description) like lower('%" + inputBean.getDescriptionSearch() + "%')";
        }
        if (inputBean.getParamValSearch() != null && !inputBean.getParamValSearch().isEmpty()) {
            where += " and lower(u.paramvalue)  like lower('%" + inputBean.getParamValSearch() + "%')";
        }
        return where;
    }

    public List<FeeConfigPendBean> getPendingFeeConfigList(FeeConfigInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<FeeConfigPendBean> dataList = new ArrayList<FeeConfigPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.FEE_CONFIG_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.FEE_CONFIG_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    FeeConfigPendBean feeconfig = new FeeConfigPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        feeconfig.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        feeconfig.setId("--");
                    }
                    try {
                        feeconfig.setOperation(pTask.getTask().getDescription().toString());
                    } catch (NullPointerException npe) {
                        feeconfig.setOperation("--");
                    }

                    String[] penArray = null;
                    if (pTask.getFields() != null) {
                        penArray = pTask.getFields().split("\\|");
                    }

                    try {
                        feeconfig.setParamcode(penArray[0]);
                    } catch (NullPointerException npe) {
                        feeconfig.setParamcode("--");
                    } catch (Exception ex) {
                        feeconfig.setParamcode("--");
                    }
                    try {
                        feeconfig.setFields(pTask.getFields());
                    } catch (NullPointerException npe) {
                        feeconfig.setFields("--");
                    } catch (Exception ex) {
                        feeconfig.setFields("--");
                    }
                    try {
                        feeconfig.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        feeconfig.setStatus("--");
                    }
                    try {
                        feeconfig.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        feeconfig.setCreateduser("--");
                    } catch (Exception ex) {
                        feeconfig.setCreateduser("--");
                    }

                    feeconfig.setFullCount(count);

                    dataList.add(feeconfig);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public FeeConfiguration findFeeConfigById(String paramCode) throws Exception {
        FeeConfiguration feeconfig = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from FeeConfiguration as u where u.paramcode=:paramcode";
            Query query = session.createQuery(sql).setString("paramcode", paramCode);
            feeconfig = (FeeConfiguration) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return feeconfig;

    }

    public String updateFeeConfig(FeeConfigInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", inputBean.getParamCode())
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(inputBean.getParamCode().trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.UPDATE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.FEE_CONFIG_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);

                txn.commit();

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmFeeConfig(FeeConfigInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {

                    FeeConfiguration u = (FeeConfiguration) session.get(FeeConfiguration.class, penArray[0]);

                    if (u != null) {
                        //------------audit old value------------
                        audit.setOldvalue(u.getParamcode() + "|" + u.getDescription() + "|" + u.getParamvalue());

                        u.setDescription(penArray[1]);
                        u.setParamvalue(penArray[2]);

                        u.setMarker(pentask.getCreateduser());
                        u.setLastupdateduser(audit.getLastupdateduser());
                        u.setLastupdatedtime(sysDate);

                        //------------audit new value------------
                        audit.setNewvalue(penArray[0] + "|" + u.getDescription() + "|" + u.getParamvalue());

                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on fee configuration ( Parameter Code: " + u.getParamcode() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                        session.update(u);

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                if (message.isEmpty()) {
                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.delete(pentask);

                    txn.commit();
                }

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectFeeConfig(FeeConfigInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask pentask = (Pendingtask) session.get(Pendingtask.class, Long.parseLong(inputBean.getId().trim()));

            if (pentask != null) {

                String[] fieldsArray = pentask.getFields().split("\\|");
                //------------audit new value------------
                audit.setNewvalue(pentask.getFields());
                
                if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    FeeConfiguration u = (FeeConfiguration) session.get(FeeConfiguration.class, fieldsArray[0]);

                    if (u != null) {
                        //------------audit old value------------
                        audit.setOldvalue(u.getParamcode() + "|" + u.getDescription() + "|" + u.getParamvalue());
                    }
                }
                String paramcode = fieldsArray[0];

                audit.setDescription("Rejected performing  '" + pentask.getTask().getDescription() + "' operation on fee configuration ( Parameter Code: " + paramcode + ")  inputted by " + pentask.getCreateduser() + " rejected " + audit.getDescription());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(pentask);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
}
