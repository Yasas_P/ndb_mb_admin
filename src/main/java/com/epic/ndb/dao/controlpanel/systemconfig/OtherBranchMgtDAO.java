/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.controlpanel.systemconfig;

import com.epic.ndb.bean.controlpanel.systemconfig.OtherBranchMgtBean;
import com.epic.ndb.bean.controlpanel.systemconfig.OtherBranchMgtInputBean;
import com.epic.ndb.bean.controlpanel.systemconfig.OtherBranchMgtPendBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.Common;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.common.Validation;
import com.epic.ndb.util.mapping.OtherBank;
import com.epic.ndb.util.mapping.OtherBankBranch;
import com.epic.ndb.util.mapping.OtherBankBranchId;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Pendingtask;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import com.epic.ndb.util.varlist.PageVarList;
import com.epic.ndb.util.varlist.SessionVarlist;
import com.epic.ndb.util.varlist.TaskVarList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author sivaganesan_t
 */
public class OtherBranchMgtDAO {

    HttpServletRequest request = ServletActionContext.getRequest();
    HttpSession session = request.getSession(false);
    Systemuser sysUser = (Systemuser) session.getAttribute(SessionVarlist.SYSTEMUSER);

    public List<OtherBranchMgtBean> getSearchList(OtherBranchMgtInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<OtherBranchMgtBean> dataList = new ArrayList<OtherBranchMgtBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.createdtime desc";
            }

            long count = 0;
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(u.id.bankcode) from OtherBankBranch as u where " + where;
            Query queryCount = session.createQuery(sqlCount);

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from OtherBankBranch u where " + where + orderBy;
                Query querySearch = session.createQuery(sqlSearch);
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    OtherBranchMgtBean branchBean = new OtherBranchMgtBean();
                    OtherBankBranch bankBranch = (OtherBankBranch) it.next();

                    try {
                        branchBean.setBankcode(bankBranch.getId().getBankcode().toString());
                    } catch (NullPointerException npe) {
                        branchBean.setBankcode("--");
                    }
                    try {
                        branchBean.setBranchcode(bankBranch.getId().getBranchcode().toString());
                    } catch (NullPointerException npe) {
                        branchBean.setBranchcode("--");
                    }
                    try {
                        branchBean.setStatus(bankBranch.getStatus().getDescription());
                    } catch (Exception npe) {
                        branchBean.setStatus("--");
                    }
                    try {
                        branchBean.setBankname(bankBranch.getOtherBank().getBankname().toString());
                    } catch (Exception npe) {
                        branchBean.setBankname("--");
                    }
                    try {
                        branchBean.setBranchname(bankBranch.getBranchname().toString());
                    } catch (Exception npe) {
                        branchBean.setBranchname("--");
                    }
                    try {
                        branchBean.setMaker(bankBranch.getMaker().toString());
                    } catch (NullPointerException npe) {
                        branchBean.setMaker("--");
                    }
                    try {
                        branchBean.setChecker(bankBranch.getChecker().toString());
                    } catch (NullPointerException npe) {
                        branchBean.setChecker("--");
                    }
                    try {
                        branchBean.setCreatedtime(bankBranch.getCreatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        branchBean.setCreatedtime("--");
                    }
                    try {
                        branchBean.setLastupdatedtime(bankBranch.getLastupdatedtime().toString().substring(0, 19));
                    } catch (NullPointerException npe) {
                        branchBean.setLastupdatedtime("--");
                    }

                    branchBean.setFullCount(count);

                    dataList.add(branchBean);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    private String makeWhereClause(OtherBranchMgtInputBean inputBean) {
        String where = "1=1";

        if (inputBean.getBankSearch() != null && !inputBean.getBankSearch().isEmpty()) {
            where += " and lower(u.id.bankcode) like lower('%" + inputBean.getBankSearch().trim() + "%')";
        }
        if (inputBean.getBranchcodeSearch() != null && !inputBean.getBranchcodeSearch().isEmpty()) {
            where += " and lower(u.id.branchcode) like lower('%" + inputBean.getBranchcodeSearch().trim() + "%')";
        }
        if (inputBean.getStatusSearch() != null && !inputBean.getStatusSearch().isEmpty()) {
            where += " and u.status.statuscode = '" + inputBean.getStatusSearch() + "'";
        }
        if (inputBean.getBranchnameSearch() != null && !inputBean.getBranchnameSearch().isEmpty()) {
            where += " and lower(u.branchname) like lower('%" + inputBean.getBranchnameSearch().trim() + "%')";
        }

        return where;
    }

    public List<OtherBranchMgtPendBean> getPendingCardCenterList(OtherBranchMgtInputBean inputBean, int max, int first, String orderBy) throws Exception {

        List<OtherBranchMgtPendBean> dataList = new ArrayList<OtherBranchMgtPendBean>();
        Session session = null;
        try {

            if (orderBy.equals("") || orderBy == null) {
                orderBy = "order by u.id desc";
            }

            long count = 0;
            //String where = this.makeWhereClause(inputBean);
            session = HibernateInit.sessionFactory.openSession();

            String sqlCount = "select count(id) from Pendingtask as u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch ";
            Query queryCount = session.createQuery(sqlCount).setString("pagecode", PageVarList.OTHER_BRANCH_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());

            Iterator itCount = queryCount.iterate();
            count = (Long) itCount.next();

            if (count > 0) {

                String sqlSearch = "from Pendingtask u where u.page.pagecode=:pagecode and u.createduser!=:currentUser and u.inputterbranch=:branch " + orderBy;
                Query querySearch = session.createQuery(sqlSearch).setString("pagecode", PageVarList.OTHER_BRANCH_MGT_PAGE).setString("currentUser", inputBean.getCurrentUser()).setString("branch", sysUser.getBranch());
                querySearch.setMaxResults(max);
                querySearch.setFirstResult(first);

                Iterator it = querySearch.iterate();

                while (it.hasNext()) {

                    OtherBranchMgtPendBean cardCenter = new OtherBranchMgtPendBean();
                    Pendingtask pTask = (Pendingtask) it.next();

                    try {
                        cardCenter.setId(Long.toString(pTask.getId()));
                    } catch (NullPointerException npe) {
                        cardCenter.setId("--");
                    }
                    try {
                        cardCenter.setOperation(pTask.getTask().getDescription().toString());
                        cardCenter.setOperationcode(pTask.getTask().getTaskcode().toString());
                    } catch (NullPointerException npe) {
                        cardCenter.setOperation("--");
                        cardCenter.setOperationcode("--");
                    }

//                    String[] penArray = null;
//                    if (pTask.getFields() != null) {
//                        penArray = pTask.getFields().split("\\|");
//                    }
//
//                    try {
//                        cardCenter.setBankcode(penArray[0]);
//                    } catch (NullPointerException npe) {
//                        cardCenter.setBankcode("--");
//                    } catch (Exception ex) {
//                        cardCenter.setBankcode("--");
//                    }
                    String[] pKeyArray = null;

                    if (pTask.getPKey() != null && !pTask.getPKey().isEmpty()) {
                        pKeyArray = pTask.getPKey().split("\\|");

                        try {
                            cardCenter.setBankcode(pKeyArray[0]);
                            OtherBank otherBank = (OtherBank) session.get(OtherBank.class, pKeyArray[0]);
                            cardCenter.setBank(otherBank.getBankname());
                        } catch (Exception ex) {
                            cardCenter.setBankcode("--");
                            cardCenter.setBank("--");
                        }
                        try {
                            cardCenter.setBranchcode(pKeyArray[1]);
                        } catch (Exception ex) {
                            cardCenter.setBranchcode("--");
                        }
                    } else {
                        cardCenter.setBankcode("--");
                        cardCenter.setBank("--");
                        cardCenter.setBranchcode("--");
                    }

                    try {
                        cardCenter.setFields(pTask.getFields().trim());
                    } catch (NullPointerException npe) {
                        cardCenter.setFields("--");
                    } catch (Exception ex) {
                        cardCenter.setFields("--");
                    }
                    try {
                        cardCenter.setStatus(pTask.getStatus().getDescription());
                    } catch (NullPointerException npe) {
                        cardCenter.setStatus("--");
                    }
                    try {
                        cardCenter.setCreatetime(pTask.getCreatedtime().toString().substring(0, 19));
                    } catch (Exception npe) {
                        cardCenter.setCreatetime("--");
                    }
                    try {
                        cardCenter.setCreateduser(pTask.getCreateduser());
                    } catch (NullPointerException npe) {
                        cardCenter.setCreateduser("--");
                    } catch (Exception ex) {
                        cardCenter.setCreateduser("--");
                    }

                    cardCenter.setFullCount(count);

                    dataList.add(cardCenter);
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return dataList;
    }

    public String insertOtherBranch(OtherBranchMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            OtherBankBranchId id = new OtherBankBranchId();
            id.setBankcode(inputBean.getBank());
            id.setBranchcode(inputBean.getBranchcode());

            if ((OtherBankBranch) session.get(OtherBankBranch.class, id) == null) {

                String pKey = inputBean.getBank().trim() + "|" + inputBean.getBranchcode().trim();

                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
                Query query = session.createQuery(sql)
                        .setString("PKey", pKey)
                        .setString("pagecode", audit.getPagecode());
                if (query.list().isEmpty()) {
                    txn = session.beginTransaction();

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(pKey);
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.ADD_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.OTHER_BRANCH_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);
                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);
                    txn.commit();
                } else {
                    message = "pending available";
                }
            } else {
                message = MessageVarList.COMMON_ALREADY_EXISTS;
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String deleteOtherBranch(OtherBranchMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String pKey = inputBean.getBank().trim() + "|" + inputBean.getBranchcode().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {
                OtherBankBranchId id = new OtherBankBranchId();
                id.setBankcode(inputBean.getBank().trim());
                id.setBranchcode(inputBean.getBranchcode().trim());

                OtherBankBranch u = (OtherBankBranch) session.get(OtherBankBranch.class, id);
                if (u != null) {

                    audit.setNewvalue(u.getId().getBankcode() + "|" + u.getId().getBranchcode() + "|" + u.getStatus().getStatuscode() + "|" + u.getBranchname()
                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                    );
                }

                Pendingtask pendingtask = new Pendingtask();

                pendingtask.setPKey(pKey.trim());
                pendingtask.setFields(audit.getNewvalue());

                Task task = new Task();
                task.setTaskcode(TaskVarList.DELETE_TASK);
                pendingtask.setTask(task);

                Status st = new Status();
                st.setStatuscode(CommonVarList.STATUS_PENDING);
                pendingtask.setStatus(st);

                Page page = (Page) session.get(Page.class, PageVarList.OTHER_BRANCH_MGT_PAGE);
                pendingtask.setPage(page);

                pendingtask.setInputterbranch(sysUser.getBranch());

                pendingtask.setCreatedtime(sysDate);
                pendingtask.setLastupdatedtime(sysDate);
                pendingtask.setCreateduser(audit.getLastupdateduser());

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.save(pendingtask);
                txn.commit();
            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public OtherBankBranch findOtherBranchById(String bankcode, String branchcode) throws Exception {
        OtherBankBranch otherBranch = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from OtherBankBranch as u where u.id.bankcode=:bankcode and u.id.branchcode=:branchcode";
            Query query = session.createQuery(sql).setString("bankcode", bankcode).setString("branchcode", branchcode);
            otherBranch = (OtherBankBranch) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return otherBranch;

    }

    public String updateOtherBranch(OtherBranchMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);
            String pKey = inputBean.getBank().trim() + "|" + inputBean.getBranchcode().trim();

            String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
            Query query = session.createQuery(sql)
                    .setString("PKey", pKey)
                    .setString("pagecode", audit.getPagecode());

            if (query.list().isEmpty()) {

                OtherBankBranchId id = new OtherBankBranchId();
                id.setBankcode(inputBean.getBank().trim());
                id.setBranchcode(inputBean.getBranchcode().trim());

                OtherBankBranch u = (OtherBankBranch) session.get(OtherBankBranch.class, id);
                if (u != null) {

                    audit.setOldvalue(u.getId().getBankcode() + "|" + u.getId().getBranchcode() + u.getStatus().getStatuscode() + "|" + u.getBranchname()
                    //                    + "|" + u.getMaker() + "|" + u.getChecker() + "|" + u.getCreatedtime().toString()+u.getLastupdatedtime().toString()
                    );

                    Pendingtask pendingtask = new Pendingtask();

                    pendingtask.setPKey(pKey.trim());
                    pendingtask.setFields(audit.getNewvalue());

                    Task task = new Task();
                    task.setTaskcode(TaskVarList.UPDATE_TASK);
                    pendingtask.setTask(task);

                    Status st = new Status();
                    st.setStatuscode(CommonVarList.STATUS_PENDING);
                    pendingtask.setStatus(st);

                    Page page = (Page) session.get(Page.class, PageVarList.OTHER_BRANCH_MGT_PAGE);
                    pendingtask.setPage(page);

                    pendingtask.setCreatedtime(sysDate);
                    pendingtask.setLastupdatedtime(sysDate);

                    pendingtask.setInputterbranch(sysUser.getBranch());

                    pendingtask.setCreateduser(audit.getLastupdateduser());

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);
                    audit.setLastupdateduser(audit.getLastupdateduser());

                    session.save(audit);
                    session.save(pendingtask);

                    txn.commit();
                } else {
                    message = MessageVarList.COMMON_NOT_EXISTS;
                }

            } else {
                message = "pending available";
            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String pendPromotionCsvDownloade(OtherBranchMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {

                if (u.getInputfile() != null) {
                    Blob blob = u.getInputfile();
                    int blobLength = (int) blob.length();
                    byte[] blobAsBytes = blob.getBytes(1, blobLength);
                    inputBean.setFileInputStream(u.getInputfile().getBinaryStream());
                    inputBean.setFileLength(blobAsBytes.length);

                    audit.setCreatetime(sysDate);
                    audit.setLastupdatedtime(sysDate);

                    session.save(audit);
                    txn.commit();
                } else {
                    message = "File not found";
                }

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String confirmOtherBranch(OtherBranchMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        InputStreamReader isr = null;
        BufferedReader br = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);
            Timestamp timestampDate = new Timestamp(sysDate.getTime());

            txn = session.beginTransaction();

            String sql = "from Pendingtask as u where u.id=:id";
            Query query = session.createQuery(sql).setString("id", inputBean.getId());
            Pendingtask pentask = (Pendingtask) query.list().get(0);

            if (pentask != null) {

                String[] penArray = new String[10];
                if (pentask.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(pentask.getFields());

                    penArray = pentask.getFields().split("\\|");
                }

                if (pentask.getTask().getTaskcode().equals(TaskVarList.ADD_TASK)) {

                    OtherBankBranchId id = new OtherBankBranchId();

                    id.setBankcode(penArray[0]);
                    id.setBranchcode(penArray[1]);

                    OtherBankBranch otherBankBranch = new OtherBankBranch();

                    otherBankBranch.setId(id);

                    Status st = (Status) session.get(Status.class, penArray[2]);
                    otherBankBranch.setStatus(st);

                    otherBankBranch.setBranchname(penArray[3]);

                    otherBankBranch.setCreatedtime(timestampDate);
                    otherBankBranch.setLastupdatedtime(timestampDate);
                    otherBankBranch.setMaker(pentask.getCreateduser());
                    otherBankBranch.setChecker(audit.getLastupdateduser());

                    session.saveOrUpdate(otherBankBranch);

                    audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on other branch (Bank code:  " + otherBankBranch.getId().getBankcode() + ",Branch code: " + otherBankBranch.getId().getBranchcode() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                    OtherBankBranchId id = new OtherBankBranchId();
                    id.setBankcode(penArray[0]);
                    id.setBranchcode(penArray[1]);

                    OtherBankBranch u = (OtherBankBranch) session.get(OtherBankBranch.class, id);

                    if (u != null) {

                        //-------------------audit old value-------------------
                        String oldvalue = penArray[0] + "|"
                                + penArray[1] + "|"
                                + u.getStatus().getStatuscode() + "|"
                                + u.getBranchname();

                        Status st = (Status) session.get(Status.class, penArray[2]);
                        u.setStatus(st);

                        u.setBranchname(penArray[3]);

                        u.setMaker(pentask.getCreateduser());
                        u.setChecker(audit.getLastupdateduser());
                        u.setLastupdatedtime(timestampDate);

                        session.update(u);

                        audit.setOldvalue(oldvalue);
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on other branch (Bank code:  " + u.getId().getBankcode() + ",Branch code: " + u.getId().getBranchcode() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());

                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.UPLOAD_TASK)) {
//                    String bankBranch = "";
                    Blob csvBlob = pentask.getInputfile();
                    if (csvBlob != null) {
                        isr = new InputStreamReader(csvBlob.getBinaryStream());
                        br = new BufferedReader(isr);

                        String[] parts = new String[0];
                        int countrecord = 1;
                        int succesrec = 0;
                        String thisLine = null;
                        boolean getline = false;
                        String token = "";
                        while ((thisLine = br.readLine()) != null) {
                            if (thisLine.trim().equals("")) {
                                continue;
                            } else {
                                if (getline) {
//                              token = content.nextLine();
                                    token = thisLine;

//                                    System.err.println(token);
                                    try {
                                        parts = token.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)", 3);
                                        inputBean.setBank(Common.removeCsvDoubleQuotation(parts[0].trim()));
                                        inputBean.setBranchcode(Common.removeCsvDoubleQuotation(parts[1].trim()));
                                        inputBean.setBranchname(Common.removeCsvDoubleQuotation(parts[2].trim()));
                                        inputBean.setStatus(CommonVarList.STATUS_ACTIVE);

                                    } catch (Exception ee) {
                                        message = "File has incorrectly ordered records at line number " + (countrecord + 1) + ",success count :" + succesrec;
                                        break;
                                    }
                                    countrecord++;
                                    if (parts.length >= 3 && message.isEmpty()) {
//                                        message = this.validateInputsForCSV(inputBean);
//                                        if (message.isEmpty()) {
//                                            message = this.validateUpload(inputBean, session);
//                                            if (message.isEmpty()) {
                                        OtherBankBranchId id = new OtherBankBranchId();
                                        id.setBankcode(inputBean.getBank());
                                        id.setBranchcode(inputBean.getBranchcode());
                                        if ((OtherBankBranch) session.get(OtherBankBranch.class, id) == null) {
                                            OtherBankBranch otherBankBranch = new OtherBankBranch();

                                            otherBankBranch.setId(id);
                                            otherBankBranch.setBranchname(inputBean.getBranchname());

                                            Status status = (Status) session.get(Status.class, inputBean.getStatus());
                                            otherBankBranch.setStatus(status);

                                            otherBankBranch.setCreatedtime(timestampDate);
                                            otherBankBranch.setLastupdatedtime(timestampDate);
                                            otherBankBranch.setMaker(pentask.getCreateduser());
                                            otherBankBranch.setChecker(audit.getLastupdateduser());

                                            session.save(otherBankBranch);

                                        } else {
                                            OtherBankBranch u = (OtherBankBranch) session.get(OtherBankBranch.class, id);

//                                                    Status statusNew = (Status) session.get(Status.class, inputBean.getStatus());
//                                                    u.setStatus(statusNew);
                                            u.setBranchname(inputBean.getBranchname());

                                            u.setMaker(pentask.getCreateduser());
                                            u.setChecker(audit.getLastupdateduser());
                                            u.setLastupdatedtime(timestampDate);

                                            session.update(u);

                                        }
//                                                bankBranch = bankBranch + inputBean.getBank().trim() +"-"+inputBean.getBranchcode().trim() + ",";
                                        succesrec++;
//                                            } else {
//                                                message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                                break;
//                                            }
//                                        } else {
//                                            message = message + " at line number " + countrecord + ",success count :" + succesrec;
//                                            break;
//                                        }

                                    } else {
                                        message = "File has incorrectly ordered records at line number " + countrecord + ",success count :" + succesrec;
                                    }
                                } else {
                                    getline = true;
//                            content.nextLine();
                                }
                            }
                        }
//                        if (!bankBranch.isEmpty() && bankBranch.length() > 0) {
//                            bankBranch = bankBranch.substring(0, bankBranch.length() - 1);
//                        }
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on other branch (Records count: " + succesrec + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                    }
                } else if (pentask.getTask().getTaskcode().equals(TaskVarList.DELETE_TASK)) {
                    OtherBankBranchId id = new OtherBankBranchId();
                    id.setBankcode(penArray[0]);
                    id.setBranchcode(penArray[1]);

                    OtherBankBranch u = (OtherBankBranch) session.get(OtherBankBranch.class, id);
                    if (u != null) {
                        audit.setDescription("Approved performing  '" + pentask.getTask().getDescription() + "' operation on other branch (Bank code:  " + u.getId().getBankcode() + ",Branch code: " + u.getId().getBranchcode() + ")  inputted by " + pentask.getCreateduser() + " approved " + audit.getDescription());
                        session.delete(u);
                    } else {
                        message = MessageVarList.COMMON_NOT_EXISTS;
                    }
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);
                audit.setLastupdateduser(audit.getLastupdateduser());

                session.save(audit);
                session.delete(pentask);

                txn.commit();

            } else {

            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String rejectOtherBranch(OtherBranchMgtInputBean inputBean, Systemaudit audit) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Pendingtask u = (Pendingtask) session.get(Pendingtask.class,
                    Long.parseLong(inputBean.getId().trim()));

            if (u != null) {
                if (u.getFields() != null) {
                    //-------------------audit new value-------------------
                    audit.setNewvalue(u.getFields());
                }
                String[] pKeyArray = null;
                String bankcode = "";
                String branchcode = "";
                if (u.getPKey() != null && !u.getPKey().isEmpty()) {
                    pKeyArray = u.getPKey().split("\\|");
                }

                try {
                    if (pKeyArray != null) {
                        bankcode = pKeyArray[0];
                        branchcode = pKeyArray[1];
                        if (u.getTask().getTaskcode().equals(TaskVarList.UPDATE_TASK)) {
                            OtherBankBranchId id = new OtherBankBranchId();
                            id.setBankcode(pKeyArray[0]);
                            id.setBranchcode(pKeyArray[1]);

                            OtherBankBranch otherBankBranch = (OtherBankBranch) session.get(OtherBankBranch.class, id);

                            if (otherBankBranch != null) {

                                //-------------------audit old value-------------------
                                String oldvalue = pKeyArray[0] + "|"
                                        + pKeyArray[1] + "|"
                                        + otherBankBranch.getStatus().getStatuscode() + "|"
                                        + otherBankBranch.getBranchname();
                                audit.setOldvalue(oldvalue);
                            }
                        }

                        audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on on other branch (Bank code: " + bankcode + ",Branch code: " + branchcode + ")  inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                    } else {
                        audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on on other branch inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                    }
                } catch (Exception e) {
                    audit.setDescription("Rejected performing  '" + u.getTask().getDescription() + "' operation on on other branch inputted by " + u.getCreateduser() + " rejected " + audit.getDescription());
                }

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.delete(u);
                txn.commit();

            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public String uploadOtherBranch(OtherBranchMgtInputBean inputBean, Systemaudit audit) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        FileInputStream fileInputStream = null;

        try {

            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

//            if ((Promotions) session.get(Promotions.class, inputBean.getSubregioncode().trim()) == null) {
//                String sql = "from Pendingtask as u where u.PKey=:PKey and u.page.pagecode=:pagecode";
//                Query query = session.createQuery(sql)
//                        .setString("PKey", inputBean.getSubregioncode())
//                        .setString("pagecode", audit.getPagecode());
//                if (query.list().isEmpty()) {
            txn = session.beginTransaction();

            Pendingtask pendingtask = new Pendingtask();

//                    pendingtask.setPKey(inputBean.getSubregioncode().trim());
//                    pendingtask.setFields(audit.getNewvalue());
            Task task = new Task();
            task.setTaskcode(TaskVarList.UPLOAD_TASK);
            pendingtask.setTask(task);

            Status st = new Status();
            st.setStatuscode(CommonVarList.STATUS_PENDING);
            pendingtask.setStatus(st);

            Page page = (Page) session.get(Page.class, PageVarList.OTHER_BRANCH_MGT_PAGE);
            pendingtask.setPage(page);

            pendingtask.setInputterbranch(sysUser.getBranch());

            try {
                if (inputBean.getConXL().length() != 0) {
                    File csvFile = inputBean.getConXL();
                    byte[] bCsvFile = new byte[(int) csvFile.length()];
                    try {
                        fileInputStream = new FileInputStream(csvFile);
                        fileInputStream.read(bCsvFile);
                        fileInputStream.close();
                        Blob blob = new javax.sql.rowset.serial.SerialBlob(bCsvFile);
                        pendingtask.setInputfile(blob);
                    } catch (Exception ex) {

                    }
                }
            } catch (NullPointerException ex) {

            } finally {

                if (fileInputStream != null) {
                    fileInputStream.close();
                }

            }

            pendingtask.setCreatedtime(sysDate);
            pendingtask.setLastupdatedtime(sysDate);
            pendingtask.setCreateduser(audit.getLastupdateduser());

            audit.setCreatetime(sysDate);
            audit.setLastupdatedtime(sysDate);
            audit.setLastupdateduser(audit.getLastupdateduser());

            session.save(audit);
            session.save(pendingtask);
            txn.commit();
//                } else {
//                    message = "pending available";
//                }
//            } else {
//                message = MessageVarList.COMMON_ALREADY_EXISTS;
//            }

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public StringBuffer makeCSVReport(OtherBranchMgtInputBean inputBean) throws Exception {
        StringBuffer content = null;
        Session session = null;
        try {
            String orderby = " order by LPAD(u.id.bankcode, 10),LPAD(u.id.branchcode, 10) ";
            String where = this.makeWhereClause(inputBean);

            session = HibernateInit.sessionFactory.openSession();
            String sqlSearch = "from OtherBankBranch u where " + where + orderby;
            Query querySearch = session.createQuery(sqlSearch);

            Iterator it = querySearch.iterate();
            content = new StringBuffer();

            //write column headers to csv file
            content.append("Branch Code");
            content.append(',');
            content.append("Bank Code");
            content.append(',');
            content.append("Branch Name");
            content.append(',');
            content.append("Status");
            content.append(',');
            content.append("Maker");
            content.append(',');
            content.append("Checker");
            content.append(',');
            content.append("Created Date And Time");
            content.append(',');
            content.append("Last Updated Date And Time");

            content.append('\n');

            while (it.hasNext()) {

                OtherBankBranch bankBranch = (OtherBankBranch) it.next();

                try {
                    content.append(bankBranch.getId().getBranchcode().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bankBranch.getId().getBankcode().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(Common.replaceCommaFieldUnderDoublequotation(bankBranch.getBranchname().toString()));
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bankBranch.getStatus().getStatuscode());
                    content.append(',');
                } catch (Exception npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bankBranch.getMaker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bankBranch.getChecker().toString());
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bankBranch.getCreatedtime().toString().substring(0, 19));
                    content.append(',');
                } catch (NullPointerException npe) {
                    content.append("--");
                    content.append(',');
                }
                try {
                    content.append(bankBranch.getLastupdatedtime().toString().substring(0, 19));
                } catch (NullPointerException npe) {
                    content.append("--");
                }
                content.append('\n');
            }
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }

        }
        return content;
    }

    public String validateUpload(OtherBranchMgtInputBean inputBean, Session session) throws Exception {
        String message = "";

        if (inputBean.getBank().length() > 10) {
            message = MessageVarList.OTHER_BRANCH_INVALID_BANKCODE_LENGTH + "10";
        } else if (inputBean.getBranchcode().length() > 10) {
            message = MessageVarList.OTHER_BRANCH_INVALID_BRANCHCODE_LENGTH + "10";
        } else if (inputBean.getBranchname().length() > 100) {
            message = MessageVarList.OTHER_BRANCH_INVALID_BRANCHNAME_LENGTH + "100";
        } else if ((OtherBank) session.get(OtherBank.class, inputBean.getBank().trim()) == null) {
            message = MessageVarList.OTHER_BRANCH_INVALID_BANK;
        }

        return message;
    }

    public String validateInputsForCSV(OtherBranchMgtInputBean inputBean) {
        String message = "";
        if (inputBean.getBank() == null || inputBean.getBank().trim().isEmpty()) {
            message = MessageVarList.OTHER_BRANCH_EMPTY_BANK;
        } else if (!Validation.isSpecailCharacter(inputBean.getBank())) {
            message = MessageVarList.OTHER_BRANCH_INVALID_BANK;
        } else if (inputBean.getBranchcode() == null || inputBean.getBranchcode().trim().isEmpty()) {
            message = MessageVarList.OTHER_BRANCH_EMPTY_BRANCH_CODE;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchcode())) {
            message = MessageVarList.OTHER_BRANCH_INVALID_BRANCH_CODE;
        } else if (inputBean.getBranchname() == null || inputBean.getBranchname().trim().isEmpty()) {
            message = MessageVarList.OTHER_BRANCH_EMPTY_BRANCH_NAME;
        } else if (!Validation.isSpecailCharacter(inputBean.getBranchname())) {
            message = MessageVarList.OTHER_BRANCH_INVALID_BRANCH_NAME;
        }

        return message;
    }
}
