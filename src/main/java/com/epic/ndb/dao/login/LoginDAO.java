/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.epic.ndb.dao.login;

import com.epic.ndb.bean.login.LoginBean;
import com.epic.ndb.dao.common.CommonDAO;
import com.epic.ndb.util.common.HibernateInit;
import com.epic.ndb.util.mapping.Page;
import com.epic.ndb.util.mapping.Passwordpolicy;
import com.epic.ndb.util.mapping.Section;
import com.epic.ndb.util.mapping.Status;
import com.epic.ndb.util.mapping.Systemaudit;
import com.epic.ndb.util.mapping.Task;
import com.epic.ndb.util.mapping.Systemuser;
import com.epic.ndb.util.mapping.Pagetask;
import com.epic.ndb.util.mapping.Sectionpage;
import com.epic.ndb.util.mapping.Userrole;
import com.epic.ndb.util.varlist.CommonVarList;
import com.epic.ndb.util.varlist.MessageVarList;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author chanuka
 */
public class LoginDAO {

    public Systemuser findUserbyUsername(String username) throws Exception {
        Systemuser user = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

//            String sql = "from Systemuser as u join fetch u.status join fetch u.userrole where u.username =:username";
            String sql = "from Systemuser as u join fetch u.status join fetch u.userrole where lower(u.username) = lower(:username)";
            Query query = session.createQuery(sql).setString("username", username);

            user = (Systemuser) query.list().get(0);
        } catch (IndexOutOfBoundsException ibe) {
            user = null;
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return user;
    }

    public boolean updateUser(LoginBean inputBean, Systemaudit audit, boolean login) throws Exception {
        Session session = null;
        Transaction txn = null;
        boolean status = true;
        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            String sql = "from Systemuser as u where lower(u.username) =lower(:username)";
            Query query = session.createQuery(sql).setString("username", inputBean.getUsername().trim());

            Systemuser u = (Systemuser) query.list().get(0);

//            Mpisystemuser u = (Mpisystemuser) session.get(Mpisystemuser.class, inputBean.getUsername().trim());
            if (u != null) {

                if (login) {
                    u.setLoggeddate(sysDate);//set last logged date only in successfull login
                }
                Status s = new Status();
                s.setStatuscode(inputBean.getStatus());

                u.setStatus(s);
                u.setLastupdatedtime(sysDate);
                u.setNoofinvlidattempt(String.valueOf(inputBean.getAttempts()));

                audit.setCreatetime(sysDate);
                audit.setLastupdatedtime(sysDate);

                session.save(audit);
                session.update(u);

                txn.commit();
            } else {
                status = false;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return status;
    }

    public HashMap<Section, List<Page>> getSectionPages(String userrole) throws Exception {

        List<Sectionpage> sectionPList = null;
        HashMap<Section, List<Page>> secMap = new HashMap<Section, List<Page>>();// key : page code value : task list
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Sectionpage as u join fetch u.section join fetch u.page join fetch u.userrole where u.id.userrolecode =:userrole and u.page.pagecode !='LGPG' and u.section.status.statuscode=:statuscode and u.page.status.statuscode=:pstatuscode order by u.page.sortkey";
            Query query = session.createQuery(sql).setString("userrole", userrole).setString("statuscode", CommonVarList.STATUS_ACTIVE).setString("pstatuscode", CommonVarList.STATUS_ACTIVE);

            sectionPList = query.list();
            //set data to map
            for (Sectionpage bean : sectionPList) {
                List<Page> pageList = secMap.get(bean.getSection());

                if (pageList == null || pageList.isEmpty()) {
                    pageList = new ArrayList<Page>();
                    pageList.add(bean.getPage());
                    secMap.put(bean.getSection(), pageList);
                } else {
                    pageList.add(bean.getPage());
                    secMap.put(bean.getSection(), pageList);
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return secMap;
    }

    public HashMap<String, List<Task>> getPageTask(String userrole) throws Exception {

        List<Pagetask> pageList = null;

        HashMap<String, List<Task>> secMap = new HashMap<String, List<Task>>();// key : page code value : task list
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Pagetask as u join fetch u.page join fetch u.task where u.id.userrolecode =:userrolecode and u.task.status.statuscode=:statuscode";
            Query query = session.createQuery(sql).setString("userrolecode", userrole).setString("statuscode", CommonVarList.STATUS_ACTIVE);

            pageList = query.list();
            //set data to map
            for (Pagetask bean : pageList) {
                List<Task> taskList = secMap.get(bean.getPage().getPagecode());
                if (taskList == null || taskList.isEmpty()) {
                    taskList = new ArrayList<Task>();
                    taskList.add(bean.getTask());
                    secMap.put(bean.getPage().getPagecode(), taskList);
                } else {
                    taskList.add(bean.getTask());
                    secMap.put(bean.getPage().getPagecode(), taskList);
                }
            }

        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return secMap;
    }

    public Passwordpolicy findPasswordPolicy() throws Exception {
        Passwordpolicy passwordpolicy = new Passwordpolicy();
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String hql = "from Passwordpolicy as m";
            Query query = session.createQuery(hql);
            passwordpolicy = (Passwordpolicy) query.list().get(0);

        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return passwordpolicy;
    }
    
    
    //end newly changed
   
    private boolean isSystemUserExist(String username) throws Exception {
        List<Systemuser> userList = new ArrayList<Systemuser>();
        Session session = null;
        boolean userCheckStatus = false;
        try {
            session = HibernateInit.sessionFactory.openSession();
            session.beginTransaction();
            Criteria criteria = session.createCriteria(Systemuser.class);
            criteria.add(Restrictions.eq("username", username));
            userList = (List<Systemuser>) criteria.list();

            for (Systemuser user : userList) {
                userCheckStatus = true;
            }

        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return userCheckStatus;
    }

    public String AddUser(LoginBean inputBean) throws Exception {
        Session session = null;
        Transaction txn = null;
        String message = "";
        try {
            session = HibernateInit.sessionFactory.openSession();
            Date sysDate = CommonDAO.getSystemDate(session);

            txn = session.beginTransaction();

            Systemuser systemuser = new Systemuser();

            systemuser.setUsername(inputBean.getUsername());

            Userrole ur = new Userrole();
            ur.setUserrolecode("ADMIN");
            systemuser.setUserrole(ur);

            Status st = new Status();
            st.setStatuscode("ACT");
            systemuser.setStatus(st);
            
            systemuser.setInitialloginstatus("0");

//                acssystemuser.setEmpid(inputBean.getEmpid());
//                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                acssystemuser.setExpirydate(formatter.parse(inputBean.getExpirydate()));
//                acssystemuser.setFullname(inputBean.getFullname());
//                acssystemuser.setAddress1(inputBean.getAddress1());
//                acssystemuser.setAddress2(inputBean.getAddress2());
//                acssystemuser.setCity(inputBean.getCity());
//                acssystemuser.setMobile(inputBean.getMobile());
//                acssystemuser.setTelno(inputBean.getTelno());
//                acssystemuser.setFax(inputBean.getFax());
//                acssystemuser.setEmail(inputBean.getMail());
            //if 'Active', change noofinvalidattempt to 0 and loggeddate to now
//                if ((inputBean.getStatus()).equals(CommonVarList.STATUS_ACTIVE)) {
//                    acssystemuser.setNoofinvlidattempt(0);
//                    acssystemuser.setLoggeddate(sysDate);
//                }
//                acssystemuser.setLastupdateduser(audit.getLastupdateduser());
            systemuser.setLastupdatedtime(sysDate);
            systemuser.setCreatetime(sysDate);
//                session.save(audit);
            session.save(systemuser);

            txn.commit();

        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }

    public Userrole findUserRoleCodeByUPMWorkcCLs(String workclass) throws Exception {
        Userrole user = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();

            String sql = "from Userrole as u  where u.upmWorkclass =:upmWorkclass";
            Query query = session.createQuery(sql).setString("upmWorkclass", workclass);

            user = (Userrole) query.list().get(0);
        } catch (IndexOutOfBoundsException ibe) {
            user = null;
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return user;
    }

    public String updateUser(LoginBean inputBean) throws Exception {

        Session session = null;
        Transaction txn = null;
        String message = "";

        try {
            session = HibernateInit.sessionFactory.openSession();
            txn = session.beginTransaction();
            Date sysDate = CommonDAO.getSystemDate(session);

            Systemuser u = (Systemuser) session.get(Systemuser.class, inputBean.getUsername());

            if (u != null) {

//                u.setSystemuserid(new BigDecimal(inputBean.getSystemuserid()));
                u.setUsername(inputBean.getUsername());

                Userrole ur = new Userrole();
                ur.setUserrolecode("ADMIN");
                u.setUserrole(ur);

                Status st = new Status();
                st.setStatuscode("ACT");
                u.setStatus(st);

//                acssystemuser.setEmpid(inputBean.getEmpid());
//                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                acssystemuser.setExpirydate(formatter.parse(inputBean.getExpirydate()));
//                acssystemuser.setFullname(inputBean.getFullname());
//                acssystemuser.setAddress1(inputBean.getAddress1());
//                acssystemuser.setAddress2(inputBean.getAddress2());
//                acssystemuser.setCity(inputBean.getCity());
//                acssystemuser.setMobile(inputBean.getMobile());
//                acssystemuser.setTelno(inputBean.getTelno());
//                acssystemuser.setFax(inputBean.getFax());
//                acssystemuser.setEmail(inputBean.getMail());
                //if 'Active', change noofinvalidattempt to 0 and loggeddate to now
//                if ((inputBean.getStatus()).equals(CommonVarList.STATUS_ACTIVE)) {
//                    acssystemuser.setNoofinvlidattempt(0);
//                    acssystemuser.setLoggeddate(sysDate);
//                }
//                acssystemuser.setLastupdateduser(audit.getLastupdateduser());
                u.setLastupdatedtime(sysDate);
                u.setCreatetime(sysDate);
//                session.save(audit);
                session.saveOrUpdate(u);

                txn.commit();
            } else {
                message = MessageVarList.COMMON_NOT_EXISTS;
            }
        } catch (Exception e) {
            if (txn != null) {
                txn.rollback();
            }
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return message;
    }
    
     //Get section List
    public List<String> getSectionList(String userrole) throws Exception {
        
        List<String> secList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            
            String sql = "select distinct id.sectioncode from Userrolesection as u where u.id.userrolecode =:userrole";
            Query query = session.createQuery(sql).setString("userrole", userrole);
            
            secList = query.list();
            
        } catch (Exception e) {
            throw e;
        } finally {
             if (session != null) {
                session.flush();
                session.close();
            }
        }
        return secList;
    }

    //Get Page List
    public List<Page> getPageList(String userrole, String section) throws Exception {
        
        List<Page> pageList = null;
        Session session = null;
        try {
            session = HibernateInit.sessionFactory.openSession();
            
            String sql = "select u.page from Sectionpage as u where u.id.userrolecode =:userrole and u.id.sectioncode =:section ";
            Query query = session.createQuery(sql).setString("userrole", userrole).setString("section", section);
            pageList = query.list();
            
        } catch (Exception e) {
            throw e;
        } finally {
              if (session != null) {
                session.flush();
                session.close();
            }
        }
        return pageList;
    }
}
